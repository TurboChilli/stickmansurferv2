﻿using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class UnityEditorMenuItems
{
	[MenuItem("Tools/Clear PlayerPrefs")]
	private static void ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

	[MenuItem("Tools/ClearPrefsMarkTutorialCompleted")]
	private static void ClearPrefsMarkTutorialsCompleted()
	{
		PlayerPrefs.DeleteAll();

		GlobalSettings.firstTimePlayVideo = false;
		GlobalSettings.FlagTutorialAsCompleted();
		GlobalSettings.FlagSurfedPassMinTutorialDistanceCompleted();
		GameState.SharedInstance.TutorialStage = TutStageType.Finished;
		GameState.SharedInstance.HasPlayedHalfPipe = true;
	}

	[MenuItem("Tools/Complete Current Goals")]
	private static void CompleteCurrentGoals()
	{	
		Goal[] curGoals = GoalManager.SharedInstance.goals;
		for (int i = 0; i < 3; i++)
		{
			curGoals[i].complete = true;
			PlayerPrefs.SetInt("GM_Goal" + i + "_complete", 1);
		}
	}

	[MenuItem("Tools/Add 5000 Coins")]
	private static void Add5000Coins()
	{
		GameState.SharedInstance.AddCash(5000);
	}

    [MenuItem("Tools/Add 50 Coins")]
    private static void Add50Coins()
    {
        GameState.SharedInstance.AddCash(50);
    }

    [MenuItem("Tools/ClearPlayerPrefsAdd1MillionCoins")]
    private static void ClearPlayerPrefsAdd1MillionCoins()
    {
        PlayerPrefs.DeleteAll();
        GameState.SharedInstance.AddCash(1000000);
    }

    [MenuItem("Tools/ClearPlayerPrefsZeroCoins")]
    private static void ClearPlayerPrefsZeroCoins()
    {
        PlayerPrefs.DeleteAll();
        GameState.SharedInstance.AddCash(0);
    }

	[MenuItem("Tools/Clear Prefs and Mark Tutorials Till")]
	private static void ClearPrefsAndMarkTutorialsCompletedTill()
	{
		PlayerPrefs.DeleteAll();
		var values = Enum.GetValues(typeof(TutorialInfo)).Cast<TutorialInfo>();

		foreach(var value in values)
		{
			TutorialInfo stopAtTutInfoEnum = (TutorialInfo)value;
			if(stopAtTutInfoEnum == TutorialInfo.TutorialOverlayMexicoMagazineCoverReveal)
				return;
			
			string key = string.Format("ST_istc{0}", (int)value);
			PlayerPrefs.SetInt(key, 1);
		}
	}
}


public class UnityEditorLangDebug
{
	[MenuItem("LangDebug/English")]
	private static void English()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.English;
	}

	[MenuItem("LangDebug/Spanish")]
	private static void Spanish()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Spanish;
	}

	[MenuItem("LangDebug/ChineseSimplified")]
	private static void ChineseSimplified()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.ChineseSimplified;
	}

	[MenuItem("LangDebug/ChineseTrad")]
	private static void ChineseTrad()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.ChineseTraditional;
	}

	[MenuItem("LangDebug/French")]
	private static void French()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.French;
	}

	[MenuItem("LangDebug/Japanese")]
	private static void Japanese()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Japanese;
	}

	[MenuItem("LangDebug/German")]
	private static void German()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.German;
	}

	[MenuItem("LangDebug/Indo")]
	private static void Indo()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Indonesian;
	}

	[MenuItem("LangDebug/Portugese")]
	private static void Portugese()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Portuguese;
	}

	[MenuItem("LangDebug/Italian")]
	private static void Italian()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Italian;
	}

	[MenuItem("LangDebug/Korean")]
	private static void Korean()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Korean;
	}

	[MenuItem("LangDebug/Russian")]
	private static void Russian()
	{
		GameLanguage.SelectedLanguage = SystemLanguage.Russian;
	}
}
