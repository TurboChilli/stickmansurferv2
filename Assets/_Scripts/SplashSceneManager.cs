﻿using UnityEngine;
using System.Collections;
using Amazon;

public class SplashSceneManager : MonoBehaviour {

    Camera theCamera;
    public LoadingMenu theMenuLoader;

    void Awake()
    { 

    }

	// Use this for initialization
	void Start () 
	{
		Application.targetFrameRate = 60;


		#if UNITY_ANDROID
		Prime31.EtceteraAndroid.setAlertDialogTheme(5);
		#endif

        theCamera = Camera.main;
        AdjustCameraYForScreenAspect();

        if (GlobalSettings.TutorialCompleted())
        {
            theMenuLoader.loadingScene = SceneType.Menu;
        }
        else
        {
            theMenuLoader.loadingScene = SceneType.SurfMain;
        }

        
	}



	// Update is called once per frame
	void Update () {
	
	}

    public bool AdjustCameraYForScreenAspect()
    {
        float width = Mathf.Max(Screen.width, Screen.height);
        float height = Mathf.Min(Screen.width, Screen.height);

        float ratio = width / height;
        if(ratio > 1.7f)
        {
            // 16:9 Do nothing
        }
        else if(ratio > 1.45f)
        {
            // Iphone 4
            // camera x = -12
            theCamera.transform.position = new Vector3(-12, theCamera.transform.position.y, theCamera.transform.position.z);
        }
        else if(ratio > 1.3f)
        {
            // Ipad
            // camera x = 62
            theCamera.transform.position = new Vector3(62, theCamera.transform.position.y, theCamera.transform.position.z);
        }
        // i5,6 = 1.77777777 (16:9)
        // i4 = 1.5 (3:2)
        // Ipad = 1.3333 (4:3)
        if (width / height < 1.6f)
        {
            return false;            
        }
        else
        {
            return true;            
        }

    }
}
