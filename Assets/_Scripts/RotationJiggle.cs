﻿using UnityEngine;
using System.Collections;

public class RotationJiggle : MonoBehaviour {

	public float minAngle = -10.0f;
	public float maxAngle = 10.0f;
	public float jiggleSpeed = 1.0f;

	private Quaternion origRotation;

	void Start()
	{
		origRotation = transform.rotation;
	}

	void Update () 
	{
		float lerpValue = (Mathf.Sin( Time.time * jiggleSpeed) + 1) * 0.5f;
		float angle = Mathf.Lerp(minAngle, maxAngle, lerpValue);

		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward) * origRotation;
	}
}
