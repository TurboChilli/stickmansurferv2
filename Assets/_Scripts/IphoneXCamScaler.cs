﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IphoneXCamScaler : MonoBehaviour {

	public  Camera attachedCamera;

	public bool retroGameSubCamCheck = false;

	void Awake()
	{
		if (attachedCamera == null)
			attachedCamera = GetComponent<Camera>();

		if (Utils.IphoneXCheck())
		{
			attachedCamera.orthographicSize = attachedCamera.orthographicSize * 0.85f;

			if (retroGameSubCamCheck)
			{
				attachedCamera.rect = new Rect(new Vector2(0.04f, 0.035f), new Vector2(0.8f, 0.85f) );
			}
		}


	}

}
