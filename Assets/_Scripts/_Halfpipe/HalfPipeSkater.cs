﻿using UnityEngine;
using System.Collections;

public class HalfPipeSkater : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    GameObject coinHitObject = null;
    GameObject prizeHitObject = null;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Coin"))
        {
            coinHitObject = other.gameObject;
            other.gameObject.SetActive(false);
        }
        else if(other.tag.Equals("SpecialPickup"))
        {
            prizeHitObject = other.gameObject;
            other.gameObject.SetActive(false);
        }
        ////////Debug.Log("HIT ITEM WITH TAG:" + other.tag);
    }

    public GameObject GetHitPrizeObject()
    {
        GameObject returnVal = prizeHitObject;
        prizeHitObject = null;
        return returnVal;
    }

    public GameObject GetHitCoinObject()
    {
        GameObject returnVal = coinHitObject;
        coinHitObject = null;
        return returnVal;
    }

}
