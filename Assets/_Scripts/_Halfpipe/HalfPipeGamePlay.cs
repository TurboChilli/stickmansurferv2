﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;

public class HalfPipeGamePlay : MonoBehaviour {

    public Camera mainCamera;
	public Transform rotationLHSAxis;
	public Transform rotationRHSAxis;

	public Transform pumpZoneLHSXMin;
	public Transform pumpZoneLHSXMax;

	public Transform pumpZoneRHSXMin;
	public Transform pumpZoneRHSXMax;

	public Transform skaterTransform;
	public Transform skaterCollisionPoint;

	public Transform sceneryBackground;
	public Transform midGround;

	public Transform sessionTimeText;
	public Transform sessionTimeShadow;
	//public TextMesh sessionTimeTextMesh;
	//TextMesh sessionTimeTextMeshShadow;
	float SessionTimeSecs = 30f;

    public GameObject pausePanel;
    public GameObject playButton;
    public GameObject shackButton;
    public GameObject pauseButton;
    public AudioSource clickSound;

    public ParticleSystem coinParticles;

	bool movingLeft = false;
	bool pumping = false;
	float LHSRotationStart = 0;
	float RHSRotationStart = 0;
	float rollVelocity = 650f;
	float rockRollVelocity = 340f;
	float minRollVelocity = 900f;
	float maxRollVelocity = 1800f;
    float rollVelocityPerSpin = 60f;
	float gravity = 850f;
	float flatFriction = 100f;
	float slopeFriction = 1260f;
    float pumpForce = 50f;//110f;
	float startYPos = 0;
	float centerAdjustForce = 150f;
	//float maxCamRangeX = 140f; // iPhone 4
    float maxCamRangeX = 95f; // iPhone 4
	float minCamY = -20f;
	float sceneryYOffset = 20f;
	float backScrollSpeed = 0.4f;
	float midScrollSpeed = 8f;//4f;
	float midYOffset = 160f;
	float cameraYSkaterOffset = 50f;
	float pumpTimer = 0;
	float pumpDelay = 0.2f;
	bool rollingUpTransition = false;
	float frameTimer = 0;
	float frameDelay = 0.03f;
	int currentFrame = 0;
	int currentRollingFrame = 0;
	int currentTrick = 0;
	int prevTrick = -1;
	float lastTrickTime = 0f;
	float copingVelocityMaxTrigger = 400;
	//bool facingDirectionLeft = false;

	float frameFraction = 0.08984375f;//0.125f;//0.076171875f;//0.08984375f;//0.076171875f;//0.08984375f;//0.0625f;
	int framesAcross = 11; // 13
	
	float rockRollAwaySpeed = 90f;

	// BENS FRAMES
	int rightMethodHoldFrameIndex = 8;
	int leftMethodHoldFrameIndex = 8;

    int rightFakieMethodHoldFrameIndex = 6;
    int leftFakieMethodHoldFrameIndex = 6;

	int[] rightPumpFrames = {22};
	int[] leftPumpFrames = {23};
	int[] rightSlamFrames = {26};
	int[] leftSlamFrames = {27};
	int[] dropinFrames = {28,29,30,31,32};
	int[] rightRockRollFrames = {24};
	int[] leftRockRollFrames = {25};
	int[] rightRollingFrames = {0};
    int[] rightFakieRollingFrames = {6};
	int[] leftRollingFrames = {11};
    int[] leftFakieRollingFrames = {7};

	int[] right180Frames = {1,1,2,2,3,3,4,4};
	int[] left180Frames = {11,11,12,12,13,13,14,14};
	int[] rightMethodFrames =  {33,33,34,35,36,36,37,37,37,38,39,40};
    int[] leftFakieMethodFrames =  {40,39,38,37,36,36,35,34,33,33};
	int[] leftMethodFrames = {44,44,45,46,47,47,48,48,48,49,50,51};
    int[] rightFakieMethodFrames = {51,50,49,48,48,47,46,45,44,44};
	int[] right360SpinFrames = {1,1,2,2,3,3,4,4,5,5,11,11,12,12,13,13,14,14,15,15,16,16,0,0};
	int[] left360SpinFrames =  {12,12,13,13,14,14,15,15,16,16,0,0,1,1,2,2,3,3,4,4,5,5,11,11};
	int[] right360IndyFrames = {55,56,57,58,59,60,61,62,63,64,66,67,68,69,70,71,72,73,74,75};
	int[] left360IndyFrames =  {66,67,68,69,70,71,72,73,74,75,55,56,57,58,59,60,61,62,63,64};
	
	int spinRotationCount = 0;

	int[][] trickFrameIndexes;


	int TRICK_NONE = 0;
	int TRICK_PUMP = 1;
	int TRICK_ROLL_RIGHT = 2;
	int TRICK_ROLL_LEFT = 3;
	int TRICK_180_RIGHT = 4;
	int TRICK_180_LEFT = 5;
	int TRICK_METHOD_RIGHT = 6;
	int TRICK_METHOD_LEFT = 7;
	int TRICK_SPIN_RIGHT = 8;
	int TRICK_SPIN_LEFT = 9;
	int TRICK_INDY_RIGHT = 10;
	int TRICK_INDY_LEFT = 11;
	int TRICK_ROCK_ROLL_RIGHT = 12;
	int TRICK_ROCK_ROLL_LEFT = 13;
	int TRICK_DROP_IN = 14;
    int TRICK_FAKIE_ROLL_RIGHT = 15;
    int TRICK_FAKIE_ROLL_LEFT = 16;

    int TRICK_FAKIE_METHOD_RIGHT = 17;
    int TRICK_FAKIE_METHOD_LEFT = 18;

	int numberOfTricks = 19;

	Material skaterFramesMaterial;
	//public Transform tricksText;
	//public Transform tricksTextShadow;
    public TextMeshPro coinsAddedText;
	public TextMeshPro tricksTextMesh;
    public TextMeshPro sessionTimeTextMesh;
    public TextMeshPro messageTextMesh;
	//TextMesh tricksTextMeshShadow;
	bool didTrickThisAir = false;
	public Transform cameraTransform;

    public Coin coinMasterRHS;
    public Coin coinMasterLHS;

    public GameObject prizeLHS;
    public GameObject prizeRHS;
    private float coinVerticalGap = 50f;
    private int coinsHigh = 40;
    private int prizeCoinsHigh = 5;
    private int prizeCoinsHighMax = 25;
    GameObject coinHitObject;
    GameObject prizeHitObject;

    public CoinBar coinBar;
    public AudioSource collectCoinSound;
    public HalfPipeSkater theSkater;

    public AudioSource landEasyTrickSound;
    public AudioSource landMediumTrickSound;
    public AudioSource landHardTrickSound;
    public AudioSource rollingSound;
    public AudioSource pumpSound;
    public AudioSource specialPrizeSound;
    public AudioSource slidingSound;
    public AudioSource copingHitSound;
    public AudioSource crowdCheerSound;

    public AudioSource musicSound;

    private HalfPipeTimer halfpipeTimer;


    Coin[] lhsCoinsList;
    Coin[] rhsCoinsList;
    private void GenerateCoins()
    {
        if (doubleCoinMode)
        {
            coinMasterLHS.SetDoubleCoinModeManually(true);
            coinMasterRHS.SetDoubleCoinModeManually(true);
        }

        lhsCoinsList = new Coin[coinsHigh];
        rhsCoinsList = new Coin[coinsHigh];
        for(int i =0; i < coinsHigh; i++)
        {
            lhsCoinsList[i] = (Coin)Object.Instantiate(coinMasterLHS);
            lhsCoinsList[i].transform.position = new Vector3(coinMasterLHS.transform.position.x, coinMasterLHS.transform.position.y + (coinVerticalGap * (i+1)), coinMasterLHS.transform.position.z);

            rhsCoinsList[i] = (Coin)Object.Instantiate(coinMasterRHS);
            rhsCoinsList[i].transform.position = new Vector3(coinMasterRHS.transform.position.x, coinMasterRHS.transform.position.y + (coinVerticalGap * (i+1)), coinMasterRHS.transform.position.z);

            if (doubleCoinMode)
            {
                
                lhsCoinsList[i].SetDoubleCoinModeManually(true);
                rhsCoinsList[i].SetDoubleCoinModeManually(true);
            }
        }

        prizeLHS.transform.position = new Vector3(lhsCoinsList[prizeCoinsHigh - 1].transform.position.x, lhsCoinsList[prizeCoinsHigh - 1].transform.position.y + coinVerticalGap * 2, lhsCoinsList[prizeCoinsHigh - 1].transform.position.z);
        prizeRHS.transform.position = new Vector3(rhsCoinsList[prizeCoinsHigh - 1].transform.position.x, rhsCoinsList[prizeCoinsHigh - 1].transform.position.y + coinVerticalGap * 2, rhsCoinsList[prizeCoinsHigh - 1].transform.position.z);
    }

    bool droppingIn = true;

    int coinsInGame = 0;
    public void AddCoin(int numberOfCoins = 1, bool showTimerText = true)
    {
       
        coinsAddedThisTrick += numberOfCoins;

        if (showTimerText)
        {
            coinsAddedTextTimer = coinsAddedTextDelay;
        }

        coinBar.coinText.text = "" + coinsInGame;
        if(numberOfCoins <= 2)
        {
            
            GlobalSettings.PlaySound(collectCoinSound);
        }
        else if(numberOfCoins > 40)
        {
            //GlobalSettings.PlaySound(landHardTrickSound);
        }
        else if(numberOfCoins > 20)
        {
           // GlobalSettings.PlaySound(landMediumTrickSound);
        }
        /*
        if (doubleCoinMode)
        {
            numberOfCoins *= 2;
        }
        */
        coinsInGame += numberOfCoins;
    }

    bool fakieRolling = true;
    bool doubleCoinMode = false;
    private void ResetCoins(bool resetPrizes = true)
    {
        for(int i =0; i < coinsHigh; i++)
        {
            lhsCoinsList[i].gameObject.SetActive(true);
            rhsCoinsList[i].gameObject.SetActive(true);

            if (doubleCoinMode)
            {
                lhsCoinsList[i].SetDoubleCoinModeManually(true);
                rhsCoinsList[i].SetDoubleCoinModeManually(true);
            }
        }
        if (resetPrizes)
        {
            prizeLHS.transform.position = new Vector3(lhsCoinsList[prizeCoinsHigh - 1].transform.position.x, lhsCoinsList[prizeCoinsHigh - 1].transform.position.y + coinVerticalGap * 2, lhsCoinsList[prizeCoinsHigh - 1].transform.position.z);
            prizeRHS.transform.position = new Vector3(rhsCoinsList[prizeCoinsHigh - 1].transform.position.x, rhsCoinsList[prizeCoinsHigh - 1].transform.position.y + coinVerticalGap * 2, rhsCoinsList[prizeCoinsHigh - 1].transform.position.z);

            prizeLHS.SetActive(true);
            prizeRHS.SetActive(true);
        }
        if (doubleCoinMode)
        {
            coinMasterLHS.SetDoubleCoinModeManually(true);
            coinMasterRHS.SetDoubleCoinModeManually(true);
        }
    }

	public enum RampPosition
	{
		FLAT,
		LHS_CURVE,
		RHS_CURVE,
		LHS_AIR,
		RHS_AIR,
		LHS_COPING,
		RHS_COPING
	};

    public RampPosition currentRampPosition = RampPosition.LHS_AIR;
	GameCenterPluginManager gameCenterPluginManager = null;

	// Use this for initialization
	void Start () {


        SetMessageText(GameLanguage.LocalizedText("LETS SKATE!"), 2);


		GameState.SharedInstance.HasPlayedHalfPipe = true;

		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();


		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementSkatePro();

        if (GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.SkateGold).UpgradeCompleted())
        {
            doubleCoinMode = true;
        }

        if (doubleCoinMode)
        {
            prizeMultiplier *= 2;
        }
        int timerUpgradeStage = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.SkateDuration).CurrentUpgradeStage;

        SessionTimeSecs += timerUpgradeStage * 10f; 



		//Application.targetFrameRate = 60;
        tricksTextMesh.text = "";
        coinsAddedText.text = "";
        coinsAddedText.gameObject.SetActive(false);
        if (GlobalSettings.musicEnabled)
        {
			GlobalSettings.PlayMusicFromTimePoint(musicSound,35.7f);
        }

        coinBar.coinText.text = "0";
        GenerateCoins();

		skaterFramesMaterial = skaterTransform.GetComponent<Renderer>().material;
        startYPos = -32;
		trickFrameIndexes = new int[numberOfTricks][];
		Time.timeScale = 1f;

		trickFrameIndexes[TRICK_ROLL_RIGHT] = rightRollingFrames;
        trickFrameIndexes[TRICK_FAKIE_ROLL_RIGHT] = rightFakieRollingFrames;
		trickFrameIndexes[TRICK_ROLL_LEFT] = leftRollingFrames;
        trickFrameIndexes[TRICK_FAKIE_ROLL_LEFT] = leftFakieRollingFrames;
		trickFrameIndexes[TRICK_180_RIGHT] = right180Frames;
		trickFrameIndexes[TRICK_180_LEFT] = left180Frames;
		trickFrameIndexes[TRICK_METHOD_RIGHT] = rightMethodFrames;
		trickFrameIndexes[TRICK_METHOD_LEFT] = leftMethodFrames;
        trickFrameIndexes[TRICK_FAKIE_METHOD_RIGHT] = rightFakieMethodFrames;
        trickFrameIndexes[TRICK_FAKIE_METHOD_LEFT] = leftFakieMethodFrames;
		trickFrameIndexes[TRICK_SPIN_RIGHT] = right360SpinFrames;
		trickFrameIndexes[TRICK_SPIN_LEFT] = left360SpinFrames;
		trickFrameIndexes[TRICK_INDY_RIGHT] = right360IndyFrames;
		trickFrameIndexes[TRICK_INDY_LEFT] = left360IndyFrames;
		trickFrameIndexes[TRICK_ROCK_ROLL_RIGHT] = rightRockRollFrames;
		trickFrameIndexes[TRICK_ROCK_ROLL_LEFT] = leftRockRollFrames;
		trickFrameIndexes[TRICK_DROP_IN] = dropinFrames;

        //SetSkaterFrame(28);
        DropIn();

		//tricksTextMesh = (TextMesh)tricksText.GetComponent("TextMesh");
		//tricksTextMeshShadow = (TextMesh)tricksTextShadow.GetComponent("TextMesh");

		

		//sessionTimeTextMesh = (TextMesh)sessionTimeText.GetComponent("TextMesh");
		//sessionTimeTextMeshShadow = (TextMesh)sessionTimeShadow.GetComponent("TextMesh");
		SetSessionTimeRemainingText(((int)SessionTimeSecs).ToString());

        skaterTransform.RotateAround(lhsCopingMarker.position, new Vector3(0, 0, 1), 90);

        pausePanel.SetActive(false);

        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 30,30, pauseButton.transform, Camera.main);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 70,30, coinBar.transform, Camera.main);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 90,30, sessionTimeTextMesh.transform, Camera.main);
        // calculate the min camera x range

        Vector3 worldPointLHSX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0,0));
        Vector3 worldPointCenterX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2f, 0,0));

        float xHalfWidthWorld = worldPointCenterX.x - worldPointLHSX.x;
        maxCamRangeX = -450 + xHalfWidthWorld;

        //////Debug.Log("xmaxCamRangeX:" + maxCamRangeX);

        maxCamRangeX = Mathf.Abs(maxCamRangeX);

        GameObject halfPipeTimerObject = new GameObject("HalfPipeTimer");
		halfpipeTimer = halfPipeTimerObject.AddComponent<HalfPipeTimer>();
		halfpipeTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Halfpipe));
	}

    public Transform lhsCopingMarker;
	bool spinningTrickToggle = false;
    bool timeFreeze = false;
    float dropInTimer = 0;
    float dropInDelay = 1;
    float dropInSpeed = -50f;

    bool gamePaused = false;
    bool musicWasPlaying = false;

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(gamePaused == false)
			{
				PauseGame();
			}
			else
			{
                ResumeGame();
			}
		}
		#endif 
	}

    void ResumeGame()
    {
        pausePanel.SetActive(false);
        gamePaused = false;
        GlobalSettings.PlaySound(clickSound);
        if (musicWasPlaying)
        {
            musicSound.UnPause();

        }

        Time.timeScale = 1;
    }

	void PauseGame()
	{
		//playButton.transform.localScale = playLocalScale;
		pausePanel.SetActive(true);
		gamePaused = true;
		GlobalSettings.PlaySound(clickSound);
		if (musicSound.isPlaying)
		{
			musicWasPlaying = true;
			musicSound.Pause();
		}
		Time.timeScale = 0;
	}

	// Update is called once per frame
	void Update () {

		CheckAndroidBackButton();

        if (Input.GetMouseButtonDown(0))
        {
            //////Debug.Log("CHECKING BUTTON HIT HERE");
            if (UIHelpers.CheckButtonHit(pauseButton.transform, Camera.main))
            {
				PauseGame();
            }
            else if (UIHelpers.CheckButtonHit(playButton.transform, Camera.main))
            {
                //playButton.transform.localScale = playLocalScale * 0.7f;
                pausePanel.SetActive(false);
                gamePaused = false;
                GlobalSettings.PlaySound(clickSound);
                if (musicWasPlaying)
                {
                    musicSound.UnPause();

                }

                Time.timeScale = 1;
            }
            else if (UIHelpers.CheckButtonHit(shackButton.transform, Camera.main))
            {
                //pausePanel.SetActive(false);
                GameState.SharedInstance.HasPlayedHalfPipe = true;
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
                SceneNavigator.NavigateTo(SceneType.Shack);
                GlobalSettings.PlaySound(clickSound);
                Time.timeScale = 1;
            }
        }

        if (gamePaused)
        {

            return;
        }

        if (showingMessageText)
        {
            messageTimer += Time.deltaTime;
            if (messageTimer > messageDelay)
            {
                messageTimer = 0;
                showingMessageText = false;
                messageTextMesh.text = "";
            }
        }

        //Time.timeScale = 0.2f;
        if (droppingIn)
        {
           
            dropInTimer += Time.deltaTime;
            if (dropInTimer < dropInDelay)
            {
                UpdateCamera();
                return;
            }
            UpdateDropIn();
            dropInSpeed -= 450f * Time.deltaTime;
            //skaterTransform.Rotate(new Vector3(0,0,-50f * Time.deltaTime));
            skaterTransform.RotateAround(lhsCopingMarker.position, new Vector3(0, 0, 1), dropInSpeed * Time.deltaTime);
            ////////Debug.Log("ZROT:" + skaterTransform.rotation.eulerAngles.z);

            UpdateCamera();

            if((skaterTransform.rotation.eulerAngles.z < 270) && (skaterTransform.rotation.eulerAngles.z > 260))

            {
                
                skaterTransform.rotation = Quaternion.Euler(skaterTransform.rotation.eulerAngles.x, skaterTransform.rotation.eulerAngles.y, 270);
                droppingIn = false;
                movingLeft = false;
                slamming = false;
                SnapSkaterZRotationToValue(270);
                //Debug.Log ("CHANGING FROM LHS AIR TO LHS CURVE RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
                OnLandedOnRamp();
                currentRampPosition = RampPosition.LHS_CURVE;
                ////////Debug.Log("LHS CURVE AFTER AIR HERE");
                EnterRampOrNewDirection(true);
                // GlobalSettings.PlaySound(rollingSound);
                UpdateTransitionLHS();
                UpdateCamera(); 
                currentTrick = TRICK_NONE;
            }
            else
            {
                return;  
            }

        }

		UpdateSessionTimeRemaining();
		UpdateMovement();
        UpdateCoinsAddedText();


		#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (timeFreeze)
            {
                Time.timeScale = 0f;
                timeFreeze = false;
            }
            else
            {
                Time.timeScale = 1f;
                timeFreeze = true;
            }
        }
		#endif
       


        if(Input.GetMouseButton(0) && SessionTimeSecs > 0 && (!mouseDownDuringAir) && (!timesUp))
		{
            
			if(IsInAir() && (! didTrickThisAir))
			{
                mouseDownDuringAir = true;
				if(IsHighSpinAirVelocity(Mathf.Abs (rollVelocity)))
				{
					if(true)//spinningTrickToggle)
					{
						DoIndySpinAir();
					}
					else
					{
						DoSpinAir();
					}
					//DoSpinAir();
					spinningTrickToggle = !spinningTrickToggle;
				}
				else
				{
					DoMethodAir();
					spinningTrickToggle = !spinningTrickToggle;
				}
			}
			else if(!IsInAir())
			{
                
                //TryDoPump();

			}
		}

        if (Input.GetMouseButtonDown(0))
        {
            if(!IsInAir())
            {
                TryDoPump();
            }
        } 

		#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.O))
		{
			Time.timeScale -= 0.2f;
			Time.timeScale = Mathf.Max (Time.timeScale, 0);
		}
		else if(Input.GetKeyDown(KeyCode.P))
		{
			Time.timeScale += 0.2f;
			Time.timeScale = Mathf.Min (Time.timeScale, 1);
		}

		if(Input.GetKeyDown(KeyCode.A))
		{
			skaterTransform.Translate(0,40,0);
		}
		else if(Input.GetKeyDown(KeyCode.C))
		{
			TryDoPump();
		}
		
		else if(Input.GetKeyDown(KeyCode.D))
		{
			//////Debug.Log("DOING TRICK");
			DoMethodAir();
		}

		#endif
        /*
        if ((currentRampPosition == RampPosition.LHS_CURVE) || (currentRampPosition == RampPosition.RHS_CURVE))
        {
            TryDoPump();
        }
        */

		if(pumping)
		{
			pumpTimer += Time.deltaTime;
			if(pumpTimer > pumpDelay)
			{
				//pumping = false;
				if(currentTrick == TRICK_NONE)
				{
					if(movingLeft)
					{
                        //////Debug.Log("SETTING ROLLING FRAME HERE 2 PUMPING: fakie: " + fakieRolling);
						//SetSkaterFrame(leftRollingFrames[0]);
					}
					else
					{
                        //////Debug.Log("SETTING ROLLING FRAME HERE 3 PUMPING: fakie: " + fakieRolling);
						//SetSkaterFrame(rightRollingFrames[0]);
					}
				}
				pumpTimer = 0;
			}
		}

		UpdateSkaterFrames();

        coinHitObject = theSkater.GetHitCoinObject();
        if(coinHitObject != null)
        {
            // hit a coin
            coinHitObject.SetActive(false);
            if (doubleCoinMode)
            {
                AddCoin(2);
            }
            else
            {
                AddCoin();
            }
        }

        prizeHitObject = theSkater.GetHitPrizeObject();

        if(prizeHitObject != null)
        {
            // hit a coin
            prizeHitObject.SetActive(false);
            coinParticles.transform.position = prizeHitObject.transform.position;
            //coinParticles.transform.position = new Vector3(coinParticles.transform.position.x, coinParticles.transform.position.y-5, coinParticles.transform.position.z);

            prizeCounter++;
            coinParticles.Emit(50);
            AddCoin(prizeCounter * prizeMultiplier);

            prizeCoinsHigh += 7;
            prizeCoinsHigh = Mathf.Min(prizeCoinsHigh, prizeCoinsHighMax);
           // doubleCoinMode = true;
            ResetCoins(false);
            GlobalSettings.PlaySound(specialPrizeSound);
        }

		if(slamming)
		{
			UpdateSlam();
		}
		UpdateCamera();

		//Debug.Log ("ROLL VELOCITY:" + rollVelocity);
	}

    int prizeCounter = 0;
    int prizeMultiplier = 50;


    float coinsAddedTextTimer = 0;
    float coinsAddedTextDelay = 0.75f;
    int coinsAddedThisTrick = 0;
    float timerSinceLevelStarted = 0;
    float timerSinceLevelStartedDelay = 2;

    void UpdateCoinsAddedText()
    {
        if (timerSinceLevelStarted < timerSinceLevelStartedDelay)
        {
            if (coinsAddedThisTrick == 0)
                return;

            timerSinceLevelStarted += Time.deltaTime;
        }

        if(coinsAddedTextTimer < 0)
        {
            coinsAddedText.gameObject.SetActive(false);
        }
        else
        {
            coinsAddedText.gameObject.SetActive(true);
        }
        coinsAddedTextTimer -= Time .deltaTime;
        if(currentRampPosition == RampPosition.LHS_AIR || currentRampPosition == RampPosition.RHS_AIR)
        {
            coinsAddedText.text = "+" + coinsAddedThisTrick;
        }

    }

    void ResetCoinsAddedText()
    {
        coinsAddedThisTrick = 0;
    }
       
	/*
	void DoSuperman()
	{
		if(!IsOnLHSOfRamp())
		{
			DoTrick(TRICK_SPIN_LEFT);
		}
		else
		{
			DoTrick(TRICK_SPIN_RIGHT);
		}
	}
	*/
	void DoSpinAir()
	{
        onSafeLandingFrameDuringSpin = true;
		spinRotationCount = 0;

		SetTricksText("");
		if(!IsOnLHSOfRamp())
		{
			DoTrick(TRICK_SPIN_RIGHT);
		}
		else
		{
			DoTrick(TRICK_SPIN_LEFT);
		}
	}

	void DoIndySpinAir()
	{
        onSafeLandingFrameDuringSpin = true;
		spinRotationCount = 0;
		SetTricksText("");
		if(!IsOnLHSOfRamp())
		{
			DoTrick(TRICK_INDY_RIGHT);
		}
		else
		{
			DoTrick(TRICK_INDY_LEFT);
		}
	}

	void DoMethodAir()
	{
		SetTricksText("METHOD");
		if(!IsOnLHSOfRamp())
		{
            if (fakieRolling)
            {
                DoTrick(TRICK_FAKIE_METHOD_LEFT);
            }
            else
            {
                DoTrick(TRICK_METHOD_RIGHT);
            }
		}
		else
		{
            if (fakieRolling)
            {
                DoTrick(TRICK_FAKIE_METHOD_RIGHT);
            }
            else
            {
                DoTrick(TRICK_METHOD_LEFT);
            }
		}



	}

	bool IsOnLHSOfRamp()
	{
		if(skaterTransform.position.x < 0)
			return true;
		else
			return false;
	}

	void SetSkaterFrame(int newFrame)
	{
		//Debug.Log ("SETTING SKATER FRAME HERE:" + newFrame);
		int colPos = 0;
		if(newFrame >= framesAcross)
		{
			colPos = newFrame % framesAcross;	
		}
		else
		{
			colPos = newFrame;	
		}
		int rowPos = newFrame / framesAcross;
		
		////////Debug.Log("COL:" + colPos + " ROW:" + rowPos);
		float offsetX = colPos * frameFraction;
		float offsetY = rowPos * frameFraction;

		//Debug.Log ("OFFSET X:" + offsetX + " OFFSET Y:" + ((1f-frameFraction) - offsetY));

		skaterFramesMaterial.mainTextureOffset = new Vector2 (offsetX,(1f-frameFraction) - offsetY);
		skaterFramesMaterial.mainTextureScale = new Vector2 (frameFraction,frameFraction);

	}

    int prevSpinRotationCount = -1;

	void OnSpinTrickLanded()
	{
        UpdateTrickText(true);


        //AddCoin(spinRotationCount * 20);

		currentTrick = TRICK_NONE;
		currentFrame = 0;
		if(movingLeft)
		{
			//facingDirectionLeft = true;
            //////Debug.Log("SETTING ROLLING FRAME HERE 4: fakie: " + fakieRolling);

            if (fakieRolling)
            {
                SetSkaterFrame(leftFakieRollingFrames[0]);
            }
            else
            {
                SetSkaterFrame(leftRollingFrames[0]);
            }
			

            if (spinRotationCount > 0)
            {
                if (spinRotationCount >= prevSpinRotationCount)
                {
                    rollVelocity -= rollVelocityPerSpin * spinRotationCount;
                }
                else
                {
                    rollVelocity += rollVelocityPerSpin * (prevSpinRotationCount - spinRotationCount);
                }
            }

		}
		else
		{
			//facingDirectionLeft = false;
            if (fakieRolling)
            {
                SetSkaterFrame(rightFakieRollingFrames[0]);
            }
            else
            {
                SetSkaterFrame(rightRollingFrames[0]);
            }

			
            if (spinRotationCount > 0)
            {
                if (spinRotationCount >= prevSpinRotationCount)
                {
                    rollVelocity += rollVelocityPerSpin * spinRotationCount;
                }
                else
                {
                    rollVelocity -= rollVelocityPerSpin * (prevSpinRotationCount - spinRotationCount);
                }
            }
		}
        if (spinRotationCount <= 1)
        {
            // MSG HERE
            SetMessageText(GameLanguage.LocalizedText("MORE SPINS!"), 2);
        }
        //Debug.Log("Spin counter:" + spinRotationCount);
        prevSpinRotationCount = spinRotationCount;
	}

	bool IsSpinAirTrick(int trickNumber)
	{
		if(
            ( trickNumber == TRICK_SPIN_LEFT || 
            trickNumber == TRICK_SPIN_RIGHT ||
            trickNumber == TRICK_INDY_LEFT || 
            trickNumber == TRICK_INDY_RIGHT)
		  )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    int lastTrick = 0;

	void TrickFinished()
	{
		//Debug.Log ("------------------ TRICK FINISHED: " + currentTrick + " ------------------");
		//currentFrame = 0;
        if (!flippedDirectionThisAir)
        {
            if (currentTrick == TRICK_METHOD_RIGHT)
            {
                // Set frame to be the other frame SSSS
                SetSkaterFrame(leftRollingFrames[0]);
            }
            else if (currentTrick == TRICK_METHOD_LEFT)
            {
                // Set frame to be the other frame SSSS
                //Debug.Log("FLIPPING OTHER WAY HERE");
                SetSkaterFrame(rightRollingFrames[0]);
            }
            else if (currentTrick == TRICK_FAKIE_METHOD_LEFT)
            {
                // Set frame to be the other frame SSSS
                //Debug.Log("FLIPPING OTHER WAY HERE");
                SetSkaterFrame(rightRollingFrames[0]);
            }
            else if (currentTrick == TRICK_FAKIE_METHOD_RIGHT)
            {
                // Set frame to be the other frame SSSS
                //Debug.Log("FLIPPING OTHER WAY HERE");
                SetSkaterFrame(leftRollingFrames[0]);
            }
        }
		
		if(manualTriggeredTrick)
		{
			didTrickThisAir = true;
            //////Debug.Log("LAST TRICK = :" + currentTrick);
            lastTrick = currentTrick;
			manualTriggeredTrick = false;
		}
        if (!didTrickThisAir)
        {
            ////////Debug.Log("TOGGLING FACE LEFT DIRECTION FROM:" + facingDirectionLeft);
            //facingDirectionLeft = (!facingDirectionLeft);
        }
        else
        {
            if (currentTrick == TRICK_METHOD_RIGHT)
            {
                //////Debug.Log("DID METHOD FAKIE RIGHT");
            }


        }

		if(IsSpinAirTrick(currentTrick))
		{
			// dont reset the frame or the trick
			return;
		}
		else
		{
            //facingDirectionLeft = (!facingDirectionLeft);
			//if(facingDirectionLeft)
            if(movingLeft)
			{
				//currentTrick = TRICK_ROLL_LEFT;
				//Debug.Log ("FACING LEFT SO SETTING FRAME LEFT HERE ere1");
                //////Debug.Log("SETTING ROLLING FRAME HERE 1: fakie: " + fakieRolling);

                //SetSkaterFrame(leftRollingFrames[0]);

                if (flippedDirectionThisAir)
                {
                    if (fakieRolling)
                    {
                        //SetSkaterFrame(leftFakieRollingFrames[0]);
                        SetSkaterFrame(rightRollingFrames[0]);
                    }
                    else
                    {
                        SetSkaterFrame(leftRollingFrames[0]);
                    }
                }
				currentTrick = TRICK_NONE;
			}
			else
			{
				//currentTrick = TRICK_ROLL_RIGHT;
				//Debug.Log ("SETTING FRAME RIGHT HERE ere2");
				//Debug.Log ("FACING RIGHT SO SETTING FRAME RIGHT HERE ere1");
                if (flippedDirectionThisAir)
                {
                    if (fakieRolling)
                    {
                        //SetSkaterFrame(rightFakieRollingFrames[0]);
                        SetSkaterFrame(leftRollingFrames[0]);
                    }
                    else
                    {
                        SetSkaterFrame(rightRollingFrames[0]);
                    }
                }

			}
		}

		currentTrick = TRICK_NONE;
		currentFrame = 0;
	}

	void UpdateSkaterFrames()
	{  
		if(currentTrick != this.TRICK_NONE)
		{
			UpdateTrick(trickFrameIndexes[currentTrick]);
		}
	}

    int currentDropInFrame = 0;

    void UpdateDropIn()
    {
        frameTimer += Time.deltaTime;
        if(frameTimer >= frameDelay)
        {
            frameTimer = 0;
            currentDropInFrame++;
            if(currentDropInFrame < dropinFrames.Length)
            {
                SetSkaterFrame(dropinFrames[currentDropInFrame]);
            }
            else
            {
                SetSkaterFrame(0);
            }
        }
    }

	bool manualTriggeredTrick = false;

    void DropIn()
    {
        SetSkaterFrame(dropinFrames[0]);
    }

	void DoTrick(int trickType, bool manualTriggered = true)
	{
		manualTriggeredTrick = manualTriggered;
		//Debug.Log ("------------------ TRICK STARTED ------------------");
		prevTrick = currentTrick;
		currentTrick = trickType;
		lastTrickTime = Time.time;
	}

	void UpdateTrick(int[] trickFrames)
	{
		if(currentTrick != TRICK_NONE)
		{

			frameTimer += Time.deltaTime;
			if(frameTimer >= frameDelay)
			{
				frameTimer = 0;

				if(currentFrame >= trickFrames.Length)
				{
					TrickFinished();
				}
				else
				{
					SetSkaterFrame(trickFrames[currentFrame]);	
				}

				if(Input.GetMouseButton(0))
				{


					if(currentTrick == TRICK_METHOD_RIGHT && currentFrame == rightMethodHoldFrameIndex)
					{
						return;
					}
					else if(currentTrick == TRICK_METHOD_LEFT && currentFrame == leftMethodHoldFrameIndex)
					{
						return;
					}
                    else if(currentTrick == TRICK_FAKIE_METHOD_RIGHT && currentFrame == rightFakieMethodHoldFrameIndex)
                    {
                        return;
                    }
                    else if(currentTrick == TRICK_FAKIE_METHOD_LEFT && currentFrame == leftFakieMethodHoldFrameIndex)
                    {
                        return;
                    }

					currentFrame++;

					if(IsSpinAirTrick(currentTrick))
					{
                        if (currentFrame == trickFrames.Length - 1)
                        {
                            currentFrame = 0;
                        }

                        if (CheckSafeLandingFrame(currentTrick, currentFrame))
                        {
                            
                            if (!onSafeLandingFrameDuringSpin)
                            {
                                onSafeLandingFrameDuringSpin = true;
                                spinRotationCount++;
                                UpdateTrickText();
                            }

                        }
                        else
                        {
                            if (onSafeLandingFrameDuringSpin)
                            {
                                onSafeLandingFrameDuringSpin = false;
                            }
                        }
					}

				}
				else
				{
					// no mouse down
					if(IsSpinAirTrick(currentTrick))
					{
						TrickFinished();
					}
					else
					{
						currentFrame++;
					}
				}

			}
		}
	}




	void UpdateCamera()
	{
		cameraTransform.position = new Vector3(skaterTransform.position.x, skaterTransform.position.y - cameraYSkaterOffset, cameraTransform.position.z);
		if(cameraTransform.position.x < -this.maxCamRangeX)
		{
			cameraTransform.position = new Vector3(-this.maxCamRangeX, cameraTransform.position.y, cameraTransform.position.z);
		}
		else if(cameraTransform.position.x > this.maxCamRangeX)
		{
			cameraTransform.position = new Vector3(this.maxCamRangeX, cameraTransform.position.y, cameraTransform.position.z);
		}

		cameraTransform.position = new Vector3(cameraTransform.position.x, Mathf.Max (cameraTransform.position.y, this.minCamY), cameraTransform.position.z);

		midGround.position = new Vector3(midGround.position.x, cameraTransform.position.y / (1 + midScrollSpeed) + midYOffset, midGround.position.z);
		sceneryBackground.position = new Vector3(cameraTransform.position.x / (1 + backScrollSpeed), cameraTransform.position.y / (1 + backScrollSpeed) + sceneryYOffset, sceneryBackground.position.z);
	}

	float trickDisplayOffsetY = 0f;
	float trickRowOffsetAmount = 3.07f;
	float textRowsVisible = 2f;
	
	void UpdateScrollingTrickText()
	{
		//tricksTextMesh.transform.localPosition = Vector3.Lerp(tricksTextMesh.transform.localPosition, new Vector3(tricksTextStartPosition.x, tricksTextStartPosition.y + trickDisplayOffsetY, tricksTextStartPosition.z), 5f * currentDelta);
	}

	void UpdateTrickText(bool trickLanded = false)
	{
		if(IsSpinAirTrick(currentTrick))
		{
			//int degreeCount = spinRotationCount * 360 + 180;
            int degreeCount = spinRotationCount * 180;
			string captionRemark = "";
            string fakieRemark = "";
            //if (fakieRolling)
            //    fakieRemark = "FAKIE ";
            if (degreeCount >= 1500 && trickLanded)
            {
				SetMessageText(GameLanguage.LocalizedText("AWESOME!"), 1);
                GlobalSettings.PlaySound(landMediumTrickSound);
                //captionRemark = "<size=-26>AWESOME!</size>";
            }
            else if(degreeCount >= 900 && trickLanded)
            {
				SetMessageText(GameLanguage.LocalizedText("NICE!"), 1);
                GlobalSettings.PlaySound(landMediumTrickSound);
                //captionRemark = "\n<size=-26>GREAT!</size>";
            }
                
			if(currentTrick == TRICK_INDY_LEFT || currentTrick == TRICK_INDY_RIGHT)
			{
                SetTricksText("" +  fakieRemark + "" + degreeCount + "' " + captionRemark);
			}
			else
			{
                SetTricksText("" +  fakieRemark + "" + degreeCount + "' " + captionRemark);
			}
		}
	}

	void SetTricksText(string theText)
	{
		tricksTextMesh.text = theText;
		//tricksTextMeshShadow.text = theText;
	}

    bool showingMessageText = false;
    float messageTimer = 0;
    float messageDelay = 0;

    void SetMessageText(string theText, float timerForMessage = 2)
    {
        showingMessageText = true;
        messageTextMesh.text = theText;
        messageDelay = timerForMessage;
        //tricksTextMeshShadow.text = theText;
    }


	void SetSessionTimeRemainingText(string text)
	{
		sessionTimeTextMesh.text = text;
		//sessionTimeTextMeshShadow.text = text;
	}

	float timeoutCountdown = 1f;
    bool timesUp = false;

	void UpdateSessionTimeRemaining()
	{
        if (waitingToNavigate)
        {
            waitingToNavigateTimer += Time.deltaTime;
            if (waitingToNavigateTimer > waitingToNavigateDelay)
            {
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
                SceneNavigator.NavigateTo(SceneType.Shack);
            }
            return;
        }
		if(SessionTimeSecs > 0)
		{
			SessionTimeSecs -= Time.deltaTime;
			SetSessionTimeRemainingText(((int)SessionTimeSecs).ToString());

            if (SessionTimeSecs <= 0)
            {
                //Debug.Log("TIMES UP");
				this.SetMessageText(GameLanguage.LocalizedText("TIME'S UP!"), 10.0f);
                //SetSessionTimeRemainingText("TIME'S UP!!!");
            }
		}
		else
		{
            
            timesUp = true;
            if (Mathf.Abs(rollVelocity) < 50f)
            {
                if (timeoutCountdown > 0)
                    timeoutCountdown -= Time.deltaTime;
                else
                {
                    if (coinsInGame > 1000)
                    {
                        GlobalSettings.PlaySound(crowdCheerSound);
                        this.SetMessageText(GameLanguage.LocalizedText("AWESOME!"), 10.0f);
                    }
                    else
                    {
                        this.SetMessageText(GameLanguage.LocalizedText("TIME'S UP!"), 10.0f);
                        //Debug.Log("TIMES UP");
                    }
                    GameState.SharedInstance.AddCash(this.coinsInGame);
                    waitingToNavigate = true;




                }
            }
		}
	}

    bool waitingToNavigate = false;
    float waitingToNavigateTimer = 0;
    float waitingToNavigateDelay = 1;


	void AppendBailedTrickText()
	{
        tricksTextMesh.text += "\n<size=-26>BAILED!</size>";
		//tricksTextMeshShadow.text += "\nBAILED!";
	}



	void TryDoPump()
	{
        if (timesUp)
            return;
        if (pumping)
            return;
		if(slamming)
			return;

		if(CheckInPumpZone(skaterCollisionPoint))
		{
			

            pumping = true;
            //////Debug.Log("PUMPING!");
            GlobalSettings.PlaySound(pumpSound);
			if(movingLeft)
			{
				rollVelocity -= pumpForce;
               
                //if(!fakieRolling)
				//SetSkaterFrame(leftPumpFrames[0]);
			}
			else
			{
				rollVelocity += pumpForce;

				//SetSkaterFrame(rightPumpFrames[0]);

			}
            if (movingLeft)
            {
				#if UNITY_EDITOR
                //////Debug.Log("AFTER LEFT PUMP: rollVel:" + rollVelocity);
                #endif
            }
            else
            {
				#if UNITY_EDITOR
				//////Debug.Log("AFTER RIGHT PUMP: rollVel:" + rollVelocity);
				#endif
            }


		}
        /*
		else
		{
			pumping = false;
			if(movingLeft)
			{
				rollVelocity += pumpForce / 4f;
			}
			else
			{
				rollVelocity -= pumpForce / 4f;
				
			}
		}
  */      
	
	}

	bool CheckInPumpZone(Transform collisionPointTransform)
	{
		if((collisionPointTransform.position.x >= pumpZoneLHSXMin.position.x) && (collisionPointTransform.position.x <= pumpZoneLHSXMax.position.x) )
		{
			// in LHS PUMP ZONE
			////////Debug.Log("FOUND IN PUMP ZONE LHS!" + Time.time);
			return true;
		}
		else if((collisionPointTransform.position.x >= pumpZoneRHSXMin.position.x) && (collisionPointTransform.position.x <= pumpZoneRHSXMax.position.x) )
		{
			////////Debug.Log("FOUND IN PUMP ZONE RHS!" + Time.time);
			// in RHS PUMP ZONE
			return true;
		}

		return false;
	}


	void UpdateTransitionLHS()
	{
        if(timesUp)
        {
            //////Debug.Log("BREAKING LHS CURVE");
            //UpdateBreakingAtEnd(false);
        }
		skaterTransform.RotateAround(new Vector3(rotationLHSAxis.position.x, rotationLHSAxis.position.y, skaterTransform.position.z), new Vector3(0,0,1), rollVelocity * 0.3f * Time.deltaTime);
		ApplyGravity();
	}

	void UpdateTransitionRHSCoping()
	{
		if(doingLipTrick)
		{
			UpdateRHSCoping();
		}
		else if((skaterTransform.position.y >= rotationRHSAxis.position.y + 40f) && (!movingLeft))
		{
			OnStartLipTrickRHS();
		}
		else
		{
			skaterTransform.Translate(rollVelocity * Time.deltaTime, 0,0);
			//Debug.Log ("CHANGING FROM RHS CURVE TO RHS AIR RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
			ApplyGravity();
			/*
			if(rollVelocity < 100)
			{
				DoTrick (TRICK_180_LEFT);
				Debug.Log ("DOING EMERGENCY SLOW 180 RHS");
				rollVelocity = 0;
				currentRampPosition = RampPosition.RHS_CURVE;
				movingLeft = true;
			}
			*/
		}
	}


	void UpdateTransitionLHSCoping()
	{
		if(doingLipTrick)
		{
			UpdateLHSCoping();
		}
		else if((skaterTransform.position.y >= rotationLHSAxis.position.y + 40f) && (movingLeft))
		{
			OnStartLipTrickLHS();
		}
		else
		{
			skaterTransform.Translate(rollVelocity * Time.deltaTime, 0,0);
			ApplyGravity();
			/*
			if(rollVelocity > -100)
			{
				DoTrick (TRICK_180_LEFT);
				Debug.Log ("DOING EMERGENCY SLOW 180 LHS");
				rollVelocity = 0;
				currentRampPosition = RampPosition.LHS_CURVE;
				movingLeft = false;
			}
			*/
		}
	}

	void UpdateTransitionLHSAir()
	{
		skaterTransform.Translate(rollVelocity * Time.deltaTime, 0,0);
		ApplyGravity();
	}



	void UpdateTransitionRHS()
	{
        if(timesUp)
        {
            ////////Debug.Log("BREAKING RHS CURVE");
           // UpdateBreakingAtEnd(false);
        }
		skaterTransform.RotateAround(new Vector3(rotationRHSAxis.position.x, rotationRHSAxis.position.y, skaterTransform.position.z), new Vector3(0,0,1), rollVelocity * 0.3f * Time.deltaTime);
		ApplyGravity();
	}

	bool doingLipTrick = false;
	bool rockingBack = false;
	bool didLipTrickThisHalf = false;
	void UpdateRHSCoping()
	{
		if(rockingBack)
		{
			skaterTransform.Translate(-rockRollVelocity * Time.deltaTime, 0,0);

			skaterTransform.Rotate(0,0, rockRollVelocity * Time.deltaTime);
			//Debug.Log ("ROCKING BACK ROTATION Z:" + skaterTransform.rotation.eulerAngles.z);
			
			if(skaterTransform.rotation.eulerAngles.z >= 90)
			{
				doingLipTrick = false;
				didLipTrickThisHalf = true;
				currentRampPosition = RampPosition.RHS_CURVE;
				rollVelocity = -rockRollAwaySpeed;
				movingLeft = true;
				DoTrick (TRICK_180_RIGHT, false);
                //////Debug.Log("RHS CURVE AFTER LIP HERE");

                EnterRampOrNewDirection(false);
               // GlobalSettings.PlaySound(rollingSound);
			}

		}
		else
		{
			skaterTransform.Translate(rockRollVelocity * Time.deltaTime, 0,0);
			//Debug.Log ("ROCKING FORWARD ROTATION Z:" + skaterTransform.rotation.eulerAngles.z);
			skaterTransform.Rotate(0,0,-rockRollVelocity * Time.deltaTime);
			if(skaterTransform.rotation.eulerAngles.z > 350)
			{
				rockingBack = true;
			}
		}
	}

	void UpdateLHSCoping()
	{


		//Debug.Log ("UPDATING LHS COPING");
		if(rockingBack)
		{
			skaterTransform.Translate(rockRollVelocity * Time.deltaTime, 0,0);
			
			skaterTransform.Rotate(0,0, -rockRollVelocity * Time.deltaTime);
			//Debug.Log ("ROCKING BACK ROTATION Z:" + skaterTransform.rotation.eulerAngles.z);
			
			if(skaterTransform.rotation.eulerAngles.z <= 270)
			{
				doingLipTrick = false;
				didLipTrickThisHalf = true;
				currentRampPosition = RampPosition.LHS_CURVE;
                //////Debug.Log("LHS CURVE AFTER LIP HERE");
                EnterRampOrNewDirection(true);
                //GlobalSettings.PlaySound(rollingSound);
				rollVelocity = rockRollAwaySpeed;
				movingLeft = false;
				DoTrick (TRICK_180_LEFT, false);

			}
			
		}
		else
		{
			skaterTransform.Translate(-rockRollVelocity * Time.deltaTime, 0,0);
			//Debug.Log ("ROCKING FORWARD ROTATION Z:" + skaterTransform.rotation.eulerAngles.z);
			skaterTransform.Rotate(0,0,rockRollVelocity * Time.deltaTime);
			if(skaterTransform.rotation.eulerAngles.z >= 350)
			{
				rockingBack = true;
			}
		}
	}
	

	void UpdateTransitionRHSAir()
	{


		skaterTransform.Translate(rollVelocity * Time.deltaTime, 0,0);
		//Debug.Log ("CHANGING FROM RHS CURVE TO RHS AIR RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
		ApplyGravity();
	}

    bool stoppedAtEnd = false;
	void UpdateFlat()
	{
        if(timesUp)
        {
            //////Debug.Log("BREAKING FLAT");
            UpdateBreakingAtEnd();
        }

		skaterTransform.Translate(rollVelocity * Time.deltaTime, 0,0);

		if(skaterTransform.position.y != startYPos)
		{
			if(skaterTransform.position.y < startYPos)
			{
				skaterTransform.position = new Vector3(skaterTransform.position.x, skaterTransform.position.y + centerAdjustForce * Time.deltaTime, skaterTransform.position.z);
				if(skaterTransform.position.y > startYPos)
				{
					skaterTransform.position = new Vector3(skaterTransform.position.x, startYPos, skaterTransform.position.z);
				}
			}
			else if(skaterTransform.position.y > startYPos)
			{
				skaterTransform.position = new Vector3(skaterTransform.position.x, skaterTransform.position.y - centerAdjustForce * Time.deltaTime, skaterTransform.position.z);
				if(skaterTransform.position.y < startYPos)
				{
					skaterTransform.position = new Vector3(skaterTransform.position.x, startYPos, skaterTransform.position.z);
				}
			}
		}

		ApplyFriction();

        if (!timesUp)
        {
            if (movingLeft)
            {
                if (rollVelocity > -minRollVelocity)
                {
                    rollVelocity = -minRollVelocity;
                }
            }
            else
            {
                if (rollVelocity < minRollVelocity)
                {
                    rollVelocity = minRollVelocity;
                }
            }
        }
	}

	void ApplySlopeFriction()
	{


		if(movingLeft)
		{
			rollVelocity += (rollVelocity / 0.1f) * Time.deltaTime;
		}
		else
		{
			if(rollVelocity > 0)
			{
				rollVelocity -= (rollVelocity / 0.1f) * Time.deltaTime;
			}
			else
			{
				rollVelocity = 0;
			}
			
		}

	}

	void ApplyFriction()
	{
		float localFriction = flatFriction;
		if(!pumping)
		{
			localFriction *= 2;
		}

		if(movingLeft)
		{
			rollVelocity += localFriction * Time.deltaTime;
		}
		else
		{
			if(rollVelocity > 0)
			{
				rollVelocity -= localFriction * Time.deltaTime;
			}
			else
			{
				rollVelocity = 0;
			}
			
		}
	}

	bool IsOnCoping()
	{
		if(
			(currentRampPosition == RampPosition.LHS_COPING) || 
			(currentRampPosition == RampPosition.RHS_COPING)
			)
		{
			////////Debug.Log(" IN AIR - TRUE!!!");
			return true;
		}
		else
		{
			////////Debug.Log(" IN AIR - FALSE!!!");
			return false;
		}
	}

	bool IsInAir()
	{

		if(
			(currentRampPosition == RampPosition.LHS_AIR) || 
			(currentRampPosition == RampPosition.RHS_AIR)
			)
		{
			////////Debug.Log(" IN AIR - TRUE!!!");
            if (rollingSound.isPlaying)
            {
                rollingSound.Stop();
                //collectCoinSound.Play();
            }
			return true;
		}
		else
		{
			////////Debug.Log(" IN AIR - FALSE!!!");
			return false;
		}
	}

    bool flippedDirectionThisAir = false;
	void ApplyGravity()
	{
		if(
			(currentRampPosition == RampPosition.LHS_AIR) || 
			(currentRampPosition == RampPosition.LHS_CURVE) || 
			(currentRampPosition == RampPosition.LHS_COPING)
		)
		{

				rollVelocity += gravity * Time.deltaTime;
		}
		else if(
			(currentRampPosition == RampPosition.RHS_AIR) || 
			(currentRampPosition == RampPosition.RHS_CURVE) ||
			(currentRampPosition == RampPosition.RHS_COPING)
			)
		{
			
			rollVelocity -= gravity * Time.deltaTime;
		}

		if((rollVelocity < 0) && (!movingLeft))
		{
            if (IsInAir())
            {
                flippedDirectionThisAir = true;
            }

			movingLeft = true;
			if(currentTrick == TRICK_NONE)
			{
				if(!IsInAir() || (IsInAir() && !didTrickThisAir) || (IsOnCoping() && !didLipTrickThisHalf))
				{
					//Debug.Log ("DOING 180 KICKTURN RHS HERE");
					//DoTrick (TRICK_180_RIGHT, false);

                    //GlobalSettings.PlaySound(rollingSound);
					SetTricksText("");
					if(IsOnCoping())
					{
						currentRampPosition = RampPosition.RHS_CURVE;
                        //////Debug.Log("RHS CURVE FROM COPING HERE 2");
                        EnterRampOrNewDirection(false);
					}
                    else if (!IsInAir())
                    {
                        //////Debug.Log("RHS CURVE FROM FACE HERE");
                        fakieRolling = !fakieRolling;
                        EnterRampOrNewDirection(false);
                    }
				}
			}
			//Debug.Log ("--------------------------- MOVING LEFT HERE");
			rollVelocity = 0;
		}
		else if((rollVelocity > 0) && (movingLeft))
		{
            if (IsInAir())
            {
                flippedDirectionThisAir = true;
            }

			movingLeft = false;
			if(currentTrick == TRICK_NONE)
			{
				if(!IsInAir() || (IsInAir() && !didTrickThisAir) || (IsOnCoping() && !didLipTrickThisHalf))
				{
					//Debug.Log ("DOING 180 KICKTURN ON FACE HERE");


					//DoTrick (TRICK_180_LEFT, false);
					SetTricksText("");

					if(IsOnCoping())
					{
						currentRampPosition = RampPosition.LHS_CURVE;
                        //////Debug.Log("LHS CURVE FROM COPING HERE 2");
                        EnterRampOrNewDirection(true);
                       // GlobalSettings.PlaySound(rollingSound);
					}
                    else if (!IsInAir())
                    {
                        //////Debug.Log("LHS CURVE FROM FACE HERE");
                        fakieRolling = !fakieRolling;
                        EnterRampOrNewDirection(true);
                    }
				}
			}
			//Debug.Log ("--------------------------- MOVING RIGHT HERE");
			rollVelocity = 0;
		}

        if (movingLeft && (currentRampPosition == RampPosition.LHS_CURVE))
        {
            if (!pumping)
            {
                //ApplyFriction();
            }
        }
        else if ((!movingLeft) && (currentRampPosition == RampPosition.RHS_CURVE))
        {
            if (!pumping)
            {
                //ApplyFriction();
            }
        }


	}

    void EnterRampOrNewDirection(bool lhs = true)
    {
        if (skaterTransform.position.y > lhsCopingMarker.position.y)
        {
            return;
        }
        //////Debug.Log("ROLL VELO:" + Mathf.Abs(rollVelocity));
        if (Mathf.Abs(rollVelocity) > 1200f)
        {
            rollingSound.pitch = 1.6f;
        }
        else if (Mathf.Abs(rollVelocity) > 200f)
        {
            rollingSound.pitch = 1.2f;
        }
        else if (Mathf.Abs(rollVelocity) > 50f)
        {
            rollingSound.pitch = 0.9f;
        }
        else
        {
            rollingSound.pitch = 0.8f;
        }
        if (!slamming)
        {
            if (skaterTransform.position.y <= lhsCopingMarker.position.y)
            {
                GlobalSettings.PlaySound(rollingSound);
            }
        }
    }

	void OnStartLipTrickLHS()
	{
		rockingBack = false;
		doingLipTrick = true;
		SetSkaterFrame(leftRockRollFrames[0]);
		didLipTrickThisHalf = false;
		SetTricksText("ROCK N ROLL!");
        //AddCoin(10);
	}

	void OnStartLipTrickRHS()
	{
		rockingBack = false;
		doingLipTrick = true;
		SetSkaterFrame(rightRockRollFrames[0]);
		didLipTrickThisHalf = false;
		SetTricksText("ROCK N ROLL!");
       // AddCoin(10);
	}

    bool onSafeLandingFrameDuringSpin = true;

   

	bool CheckSafeLandingFrame(int theTrick, int theFrameNumber)
	{
		int[] trickFrames = trickFrameIndexes[theTrick];
        //////Debug.Log("CHECKING SAFE FOR INDEX FRAME:" + trickFrames[theFrameNumber]);

        if((trickFrames[theFrameNumber] == 63) || (trickFrames[theFrameNumber] == 64) || (trickFrames[theFrameNumber] == 66) || (trickFrames[theFrameNumber] == 67))
        {
            return true;
        }

        else if((trickFrames[theFrameNumber] == 74) || (trickFrames[theFrameNumber] == 75) || (trickFrames[theFrameNumber] == 55) || (trickFrames[theFrameNumber] == 56))
        {
            return true;
        }

       
        return false;
	}

    bool CheckFakieLanding(int theTrick, int theFrameNumber)
    {
        int[] trickFrames = trickFrameIndexes[theTrick];

        //////Debug.Log("CHECKING FAKIE FRAME FOR INDEX FRAME:" + trickFrames[theFrameNumber] +  " MOVINGLEFT:" + movingLeft);

        if (!movingLeft)
        {
            if((trickFrames[theFrameNumber] == 63) || (trickFrames[theFrameNumber] == 64) || (trickFrames[theFrameNumber] == 66) || (trickFrames[theFrameNumber] == 67))
            {
                return true;
            }
        }
        else
        {
            if((trickFrames[theFrameNumber] == 74) || (trickFrames[theFrameNumber] == 75) || (trickFrames[theFrameNumber] == 55) || (trickFrames[theFrameNumber] == 56))
            {
                return true;
            }

        }

        return false;
    }

	bool IsHighSpinAirVelocity(float velCheck)
	{
        if (velCheck > 550)
        {
            return true;
        }
        else if (theSkater.transform.position.y > 480)
        {
            return true;
        }
        else
        {
            return false;
        }
	}

	bool slamming = false;
    bool landedTrickLastAir = false;
    int lastTrickRotationCount = 0;

    bool mouseDownDuringAir = false;
    float rollVelocityMethod = 150f;

	void OnLandedOnRamp()
	{
        flippedDirectionThisAir = false;
        onSafeLandingFrameDuringSpin = true;
        //Debug.Log("LANNNNNNDING HERE");
        coinsAddedThisTrick = 0;
        mouseDownDuringAir = false;
        pumping = false;
        if (didTrickThisAir)
        {
            lastTrickRotationCount = spinRotationCount;
            landedTrickLastAir = true;
            //////Debug.Log("LANDED BACK ON RAMP AFTER AIR TRICK HERE --------------------- currentTrick:" + currentTrick + " lastTrick:" + lastTrick );

            if (
                (lastTrick == TRICK_METHOD_LEFT) ||
                (lastTrick == TRICK_METHOD_RIGHT) ||
                (lastTrick == TRICK_FAKIE_METHOD_LEFT) ||
                (lastTrick == TRICK_FAKIE_METHOD_RIGHT))
            {
                if (movingLeft)
                {
                    
                    rollVelocity -= rollVelocityMethod;
                    //Debug.Log("*********** METHOD BOOST LEFT" + rollVelocity);
                }
                else
                {
                    //Debug.Log("*********** METHOD BOOST RIGHT" + rollVelocity);
                    rollVelocity += rollVelocityMethod;

                }
                //////Debug.Log("METHOD ADDED FORCE !");
            }

        }
        else
        {
            // MSG HERE

            landedTrickLastAir = false;
            //////Debug.Log("LANDED NO TRICK" + currentTrick);

            fakieRolling = !fakieRolling;


            if (prevSpinRotationCount > -1)
            {
                if (movingLeft)
                {
                    rollVelocity = -minRollVelocity;//+= rollVelocityPerSpin * 5f;
                }
                else
                {
                    rollVelocity = minRollVelocity;//-= rollVelocityPerSpin * 5f;

                }
            }
        }



		didTrickThisAir = false;
		slamming = false;


        int[] trickFrames = trickFrameIndexes[currentTrick];
        if(IsSpinAirTrick(currentTrick))
		{
            ////////Debug.Log("SLAMMING + TRUE");
			slamming = true;
            ////////Debug.Log("TRICK FRAMES" + trickFrames + " lastTrick:" + lastTrick);
			if(trickFrames.Length > currentFrame)
			{
                //Debug.Log ("LANDED SPIN HERE");
                if (CheckSafeLandingFrame(currentTrick, currentFrame))
                {
                    //////Debug.Log("LANDED SAFE SPIN HERE trick:" + currentTrick + " FRAME:" + currentFrame);
                    if (CheckFakieLanding(currentTrick, currentFrame))
                    {
                        //////Debug.Log("LANDED FAKIE SPIN HERE");
                        fakieRolling = true;
                    }
                    else
                    {
                        fakieRolling = false;
                    }
                      
                    //////Debug.Log("REVERSING SLAM TRUE HERE");
                    slamming = false;
                    OnSpinTrickLanded();
                }
                else
                {
                    //Debug.Log ("SLAMMED AFTER SPIN HERE: currentTrick:" + currentTrick + " currentFrame:" + currentFrame);

                }

			}
			currentTrick = TRICK_NONE;

		}
		else
		{/*
			if(Input.GetMouseButton (0))
			{
				// doing a non spin air so we need to slam as they have not release
				slamming = true;
			}
        */
            
		}

        //Debug.Log("LANDED HERE WHY NO CODE: " + landedTrickLastAir + " spins:" + spinRotationCount + " timerSinceLevelStarted: " +timerSinceLevelStarted );
		if(slamming)
		{
			OnSlamStart();
		}
         
        else if (movingLeft)
        {
            if (!landedTrickLastAir)
            {
                
                if (timerSinceLevelStarted > timerSinceLevelStartedDelay)
                {
                    if (spinRotationCount <= 1)
                    {
                        SetMessageText(GameLanguage.LocalizedText("HOLD TO SPIN!"), 2);
                    }
                }
            }
            if (fakieRolling)
            {
                //////Debug.Log("SETTING FAKIE LEFT FRAME");
                SetSkaterFrame(leftFakieRollingFrames[0]);
            }
            else
            {
                SetSkaterFrame(leftRollingFrames[0]);
            }
        }
        else
        {
            if (!landedTrickLastAir)
            {
                if (timerSinceLevelStarted > timerSinceLevelStartedDelay)
                {
                    if (spinRotationCount <= 1)
                    {
                        SetMessageText(GameLanguage.LocalizedText("HOLD TO SPIN!"), 2);
                    }

                }
            }
            if (fakieRolling)
            {
                //////Debug.Log("SETTING FAKIE RIGHT FRAME");
                SetSkaterFrame(rightFakieRollingFrames[0]);
            }
            else
            {
                SetSkaterFrame(rightRollingFrames[0]);
            }
        }
	}

	void OnSlamStart()
	{
        if (skaterTransform.position.y < lhsCopingMarker.position.y)
        {
            GlobalSettings.PlaySound(slidingSound);
        }
		//Debug.Log ("SLAMMING HERE");
		AppendBailedTrickText();
        fakieRolling = false;
		if(movingLeft)
		{
			//facingDirectionLeft = true;

			SetSkaterFrame(this.leftSlamFrames[0]);
		}
		else
		{
			//facingDirectionLeft = false;

			SetSkaterFrame(this.rightSlamFrames[0]);
		}
        prevSpinRotationCount = 2;
	}

	float slamDragForce = 600f;
	void UpdateSlam()
	{
        if ((currentRampPosition == RampPosition.LHS_AIR) || (currentRampPosition == RampPosition.RHS_AIR) )
        {
            slidingSound.Stop();
        }
        else
        {
            if(!slidingSound.isPlaying)
            {
                if (skaterTransform.position.y < lhsCopingMarker.position.y)
                {
                    GlobalSettings.PlaySound(slidingSound);
                }

            }

        }
		if(currentRampPosition == RampPosition.FLAT)
		{
			if(movingLeft)
			{
				rollVelocity += slamDragForce * 2f * Time.deltaTime;
			}
			else
			{
				rollVelocity -= slamDragForce * 2f * Time.deltaTime;
			}
		}
		else
		{
			if(movingLeft)
			{
				rollVelocity += slamDragForce * Time.deltaTime;
			}
			else
			{
				rollVelocity -= slamDragForce * Time.deltaTime;
			}
		}

		if(movingLeft)
		{

			if(rollVelocity > -100)
			{
                if (skaterTransform.position.y <= lhsCopingMarker.position.y)
                {
                    GlobalSettings.PlaySound(rollingSound);
                }
                slidingSound.Stop();
				#if UNITY_EDITOR
				Debug.Log ("SLAM FINISHED moving left here");
				#endif
				rollVelocity = -100;
				slamming = false;
				SetTricksText("");
				#if UNITY_EDITOR
				//////Debug.Log("SETTING ROLLING FRAME HERE 5: fakie: " + fakieRolling);
				#endif
				SetSkaterFrame(leftRollingFrames[0]);
				currentFrame = 0;
				didTrickThisAir = false;
				currentTrick = TRICK_NONE;
			}
		}
		else
		{
			if(rollVelocity < 100)
			{
                if (skaterTransform.position.y <= lhsCopingMarker.position.y)
                {
                    GlobalSettings.PlaySound(rollingSound);
                }
                slidingSound.Stop();
				#if UNITY_EDITOR
				Debug.Log ("SLAM FINISHED moving right here");
				#endif
				rollVelocity = 100;
				slamming = false;
				SetTricksText("");
				#if UNITY_EDITOR
				//////Debug.Log("SETTING ROLLING FRAME HERE 6: fakie: " + fakieRolling);
				#endif
				SetSkaterFrame(rightRollingFrames[0]);
				currentFrame = 0;
				didTrickThisAir = false;
				currentTrick = TRICK_NONE;
			}
		}
	}

	void SnapSkaterZRotationToValue(float snapValue)
	{
		//return;
		skaterTransform.rotation = Quaternion.Euler(new Vector3(skaterTransform.rotation.eulerAngles.x, skaterTransform.rotation.eulerAngles.y, snapValue));
	}

    void UpdateBreakingAtEnd(bool useRollingFrame = true)
    {
        
        if (movingLeft)
        {
            rollVelocity += 3500f * Time.deltaTime;
            if (rollVelocity > -600)
            {
                if (useRollingFrame)
                {
                    rollingSound.Stop();
                    SetSkaterFrame(17);
                }
                else
                {
                    
                    //rollVelocity = Mathf.Min(rollVelocity, -300);
                }

            }


            ////////Debug.Log("LEFT ROLLVEL:" + rollVelocity);
            if(rollVelocity > 0)
            {
                rollVelocity = 0;
                stoppedAtEnd = true;
            }
        }
        else
        {
            rollVelocity -= 3500f * Time.deltaTime;
            if (rollVelocity < 600)
            {
                if (useRollingFrame)
                {
                    rollingSound.Stop();
                    SetSkaterFrame(30);
                }
                else
                {
                    //rollVelocity = Mathf.Max(rollVelocity, 300);
                }

            }
            ////////Debug.Log("RIGHT ROLLVEL:" + rollVelocity);
            if(rollVelocity < 0)
            {
                rollVelocity = 0;
                stoppedAtEnd = true;
            }
        }
    }
	void UpdateMovement()
	{
        if(true)//!timesUp)
        {
    		if(rollVelocity < -maxRollVelocity)
    		{
    			rollVelocity = -maxRollVelocity;
    		}
    		else if(rollVelocity > maxRollVelocity)
    		{
    			rollVelocity = maxRollVelocity;
    		}
        }
		if(movingLeft)
		{
			if(currentRampPosition == RampPosition.FLAT)
			{
				if(skaterCollisionPoint.position.x <= rotationLHSAxis.position.x)
				{
					currentRampPosition = RampPosition.LHS_CURVE;
                    ////////Debug.Log("LHS CURVE HERE 3");
					SnapSkaterZRotationToValue(0);
					//Debug.Log ("CHANGING FROM LHS FLAT TO LHS CURVE RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					UpdateTransitionLHS();
				}
				else
				{
					UpdateFlat();
				}
			}
			else if(currentRampPosition == RampPosition.LHS_CURVE)
			{
                if (timesUp)
                {
                    // xxxxxx
                    rollVelocity += 3500f * Time.deltaTime;
                    if(rollVelocity > 0)
                    {
                        rollVelocity = 0;
                        stoppedAtEnd = true;
                    }
                }

				if(skaterCollisionPoint.position.y <= rotationLHSAxis.position.y)
				{
                    ////////Debug.Log("Transition LHS");
					UpdateTransitionLHS();
				}
				else
				{
					SnapSkaterZRotationToValue(270);
					//Debug.Log ("CHANGING FROM LHS CURVE TO LHS AIR RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					if((rollVelocity > -copingVelocityMaxTrigger))// || (Input.GetMouseButton(0) && rollVelocity > -copingVelocityMaxTrigger - 300 ) )
					{
						currentRampPosition = RampPosition.LHS_COPING;
						UpdateTransitionLHSCoping();
					}
					else
					{
						currentRampPosition = RampPosition.LHS_AIR;
                        GlobalSettings.PlaySound(copingHitSound);
						UpdateTransitionLHSAir();
					}
				}
			}
			else if(currentRampPosition == RampPosition.LHS_AIR)
			{
				UpdateTransitionLHSAir();
			}
			else if(currentRampPosition == RampPosition.LHS_COPING)
			{
				UpdateTransitionLHSCoping();
			}
			else if(currentRampPosition == RampPosition.RHS_CURVE)
			{
                
				if(skaterCollisionPoint.position.x >= rotationRHSAxis.position.x)
				{
					UpdateTransitionRHS();
				}
				else
				{
					//Debug.Log ("CHANGING FROM RHS CURVE TO RHS FLAT RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					//Debug.Log ("SNAP HERE 2");
					SnapSkaterZRotationToValue(360);
					//Debug.Log ("AFTER ------- CHANGING FROM RHS CURVE TO RHS FLAT RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					currentRampPosition = RampPosition.FLAT;
                    pumping = false;
                    ResetCoins();
				}
			}
			else if(currentRampPosition == RampPosition.RHS_AIR)
			{
				if(skaterCollisionPoint.position.y >= rotationLHSAxis.position.y)
				{
					UpdateTransitionRHSAir();
				}
				else
				{
					SnapSkaterZRotationToValue(90);
					//Debug.Log ("CHANGING FROM RHS AIR TO RHS CURVE RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					currentRampPosition = RampPosition.RHS_CURVE;
                    //GlobalSettings.PlaySound(rollingSound);
					OnLandedOnRamp();
                    ////////Debug.Log("RHS CURVE AFTER AIR HERE");
                    EnterRampOrNewDirection(false);
					UpdateTransitionRHS();
				}
				
			}
		}
		else
		{
			if(currentRampPosition == RampPosition.FLAT)
			{
				if(skaterCollisionPoint.position.x >= rotationRHSAxis.position.x)
				{
					SnapSkaterZRotationToValue(0);
					//Debug.Log ("CHANGING FROM RHS FLAT TO RHS CURVE RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					currentRampPosition = RampPosition.RHS_CURVE;

					UpdateTransitionRHS();
				}
				else
				{
					UpdateFlat();
				}
			}
			else if(currentRampPosition == RampPosition.LHS_CURVE)
			{
                

				if(skaterCollisionPoint.position.x <= rotationLHSAxis.position.x)
				{
					UpdateTransitionLHS();
				}
				else
				{
					SnapSkaterZRotationToValue(0);
					//Debug.Log ("CHANGING FROM LHS CURVE TO LHS FLAT RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					currentRampPosition = RampPosition.FLAT;
                    pumping = false;
                    ResetCoins();
					UpdateFlat();
				}
			}
			else if(currentRampPosition == RampPosition.LHS_AIR)
			{
				if(skaterCollisionPoint.position.y >= rotationLHSAxis.position.y)
				{
					UpdateTransitionLHSAir();
				}
				else
				{
					SnapSkaterZRotationToValue(270);
					//Debug.Log ("CHANGING FROM LHS AIR TO LHS CURVE RotationEuler = :" + skaterTransform.rotation.eulerAngles.z);
					OnLandedOnRamp();
					currentRampPosition = RampPosition.LHS_CURVE;
                    ////////Debug.Log("LHS CURVE AFTER AIR HERE");
                    EnterRampOrNewDirection(true);
                   // GlobalSettings.PlaySound(rollingSound);
					UpdateTransitionLHS();
				}

			}

			else if(currentRampPosition == RampPosition.RHS_CURVE)
			{
				if(skaterCollisionPoint.position.y <= rotationRHSAxis.position.y)
				{
					//Debug.Log ("SNAP HERE 1");
					//
					UpdateTransitionRHS();
				}
				else
				{
					SnapSkaterZRotationToValue(90);

					//Debug.Log ("------------ RHS AIR TRANSITION: moveSpeed = " + this.rollVelocity);

					if((rollVelocity < copingVelocityMaxTrigger) )//|| (Input.GetMouseButton(0) && rollVelocity < copingVelocityMaxTrigger + 300 ) )
					{
						currentRampPosition = RampPosition.RHS_COPING;
						UpdateTransitionRHSCoping();
					}
					else
					{
						currentRampPosition = RampPosition.RHS_AIR;
						UpdateTransitionRHSAir();
                        GlobalSettings.PlaySound(copingHitSound);
					}
				}
			}
			else if(currentRampPosition == RampPosition.RHS_COPING)
			{
				UpdateTransitionRHSCoping();

			}

			else if(currentRampPosition == RampPosition.RHS_AIR)
			{
				UpdateTransitionRHSAir();
			}
		}

		//Debug.Log ("ROLL VELOCITY:" + rollVelocity);
	}

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            if(gamePaused == false)
            {
                PauseGame();
            }
        }
    }

}
