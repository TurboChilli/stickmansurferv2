﻿using UnityEngine;
using System.Collections;

public class GametimeTool : MonoBehaviour 
{
	float timer = 0.0f;
	bool timerActive = false;

	GUIStyle gametimeTextStyle = new GUIStyle();

	public void StartTimer()
	{
		timer = 0.0f;
		timerActive = true;
	}

	public void ResumeTimer()
	{
		timerActive = true;
	}

	public void EndTimer()
	{
		timerActive = false;
	}

	void Update () 
	{
		if (timerActive)
		{
			timer += Time.deltaTime;
		}
	}

	void OnGUI () 
	{
		gametimeTextStyle.normal.textColor = Color.magenta;
		gametimeTextStyle.fontSize = 30;
	
		GUI.Label(new Rect(Screen.width * 0.8f, Screen.height * 0.05f,Screen.width * 0.1f,Screen.height * 0.05f), timer.ToString("F2"), gametimeTextStyle);
	}
}