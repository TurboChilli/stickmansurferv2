﻿using UnityEngine;
using System.Collections;
using TMPro;

public class NumericDisplayBar : MonoBehaviour 
{
	public TMP_Text valueText = null;
	public int currentValue = 0;
	public bool openEnergyDrinkShopOnClick = false;
	public Transform backgroundDisplayPanelBar = null;

	private float timeToCount = 2.0f;
	private float curTimeToCount = 0.0f;

	private bool isCounting = false;
	private int previousValue = 0;
	private EnergyDrinkShopController energyDrinkShopController = null;
	private Camera mainCamera = null;

	void Start()
	{
		if(openEnergyDrinkShopOnClick)
		{
			GameObject camObject = GameObject.FindGameObjectWithTag("MainCamera");
			if(camObject != null)
			{
				mainCamera = camObject.GetComponent<Camera>();
			}

			if(energyDrinkShopController == null)
				energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();
		}
	}

	public void DisplayValue(int value, bool isAnimated = false)
	{
		if (isAnimated)
		{
			previousValue = currentValue;
			currentValue = value;

			isCounting = true;
			curTimeToCount = timeToCount;
		}
		else
		{
			previousValue = currentValue;
			currentValue = value;

			isCounting = false;
			valueText.text = string.Format("{0:#,0}", (int)currentValue);
		}
	}

	public void Update()
	{
		if (isCounting)
		{
			float lerpAmount = curTimeToCount / timeToCount;
			lerpAmount = 1.0f - Mathf.Pow(lerpAmount, 3.0f);

			int newValue = (int)Mathf.Lerp((float)previousValue, (float)currentValue, lerpAmount);

			curTimeToCount -= Time.deltaTime;
			if (curTimeToCount <= 0)
			{
				newValue = currentValue;
				isCounting = false;
			}

			valueText.text = string.Format("{0:#,0}", newValue);
		}


		if(Input.GetMouseButtonDown(0) && openEnergyDrinkShopOnClick && mainCamera != null && mainCamera.isActiveAndEnabled)
		{
			if(backgroundDisplayPanelBar != null && UIHelpers.CheckButtonHit(backgroundDisplayPanelBar, mainCamera))
			{
				if(energyDrinkShopController != null && mainCamera != null)
				{
					energyDrinkShopController.Show(mainCamera);
				}
			}
		}
	}


}
