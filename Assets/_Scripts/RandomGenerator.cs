﻿using System;

public class RandomGenerator
{
	private static readonly Random random = new Random();
	private static readonly object syncLock = new object();

	public static int RandomNumber(int inclusiveMin, int exclusiveMax)
	{
		lock(syncLock) {
			return random.Next(inclusiveMin, exclusiveMax);
		}
	}

	static int previous = -9999;
	public static int RandomNumberNotSameAsPrevious(int inclusiveMin, int exclusiveMax)
	{
		lock(syncLock) {
			int rand = random.Next(inclusiveMin, exclusiveMax);
			int tries = 0;
			while(rand == previous && tries < 3)
			{
				rand = random.Next(inclusiveMin, exclusiveMax);
				tries += 1;
			}

			previous = rand;
			return rand;
		}
	}
}
