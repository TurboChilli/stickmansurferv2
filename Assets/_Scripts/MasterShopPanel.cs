﻿using UnityEngine;
using System.Collections;

public class MasterShopPanel : MonoBehaviour {

    public AnimatingObject panelForeground;
    public GameObject selectButton;
    public GameObject selectedButton;
    public GameObject levelRequiredButton;
    public GameObject buyButton;
    public TextMesh vehicleText;
    public TextMesh priceText;
    public TextMesh levelRequiredText;
    public TextMesh gameLevelText;
    public GameObject coinImage;
    public Vehicle vehicleType;
    public int levelRequiredValue = -1;



    public enum PanelState
    {
        Buy,
        Select,
        Selected,
        NeedAnotherLevel
    }
    private PanelState currentPanelState = PanelState.Select;



    public PanelState CurrentPanelState
    {
        get
        {
            return currentPanelState;
        }
        set
        {
            currentPanelState = value;
        }
    }



    public void SetFrameIndex(int value)
    {
        panelForeground.DrawFrame(value);
    }



    public void SetPrice(int price)
    {
        priceText.text = "" + price;
    }



    public void SetItemName(string itemName)
    {
        vehicleText.text = itemName;
    }



    public string GetItemName()
    {
        return vehicleText.text;
    }



    public void SetLevelRequiredText(string levelRequirement)
    {
        levelRequiredText.text = levelRequirement;
    }



    public GameObject GetSelectButton()
    {
        return selectButton;
    }



    public GameObject GetSelectedButton()
    {
        return selectedButton;
    }



    public GameObject GetLevelRequiredButton()
    {
        return levelRequiredButton;
    }



    public GameObject GetBuyButton()
    {
        return buyButton;
    }



    public PanelState GetPanelState()
    {
        return currentPanelState;
    }



    public int LevelRequiredValue
    {
        get
        {
            return levelRequiredValue;
        }
        set
        {
            levelRequiredValue = value;
        }
    }



    public void SetPanelStateRequiresAnotherLevel(int requiresLevel)
    {
        LevelRequiredValue = requiresLevel;
        SetPanelState(PanelState.NeedAnotherLevel);
    }



    public void SetPanelState(PanelState newState)
    {
        selectButton.SetActive(false);
        selectedButton.SetActive(false);
        levelRequiredButton.SetActive(false);
        buyButton.SetActive(false);
        coinImage.SetActive(false);

        currentPanelState = newState;

        if(currentPanelState == PanelState.Buy)
        {           
            buyButton.SetActive(true);
            coinImage.SetActive(true);
        }
        else if(currentPanelState == PanelState.Select)
        {
            selectButton.SetActive(true);
            coinImage.SetActive(false);
            priceText.text = "";
        }
        else if(currentPanelState == PanelState.Selected)
        {
            selectedButton.SetActive(true);
            coinImage.SetActive(false);
            priceText.text = "";
        }
        else if(currentPanelState == PanelState.NeedAnotherLevel)
        {            
            levelRequiredButton.SetActive(true);
            levelRequiredText.text = "Requires\n level " + LevelRequiredValue;
        }

    }
}
