﻿using UnityEngine;
using System.Collections;

public class VehicleIconSelector : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetVehicleIcon(Vehicle vehicle)
	{
		var vehicleAnimatingSprite = this.GetComponent<AnimatingObject>();
		if(vehicle == Vehicle.Surfboard)
			vehicleAnimatingSprite.DrawFrame(0);
		else if(vehicle == Vehicle.Longboard)
			vehicleAnimatingSprite.DrawFrame(1);
		else if(vehicle == Vehicle.Bodyboard)
			vehicleAnimatingSprite.DrawFrame(2);
		else if(vehicle == Vehicle.Tube)
			vehicleAnimatingSprite.DrawFrame(3);
		else if(vehicle == Vehicle.Jetski)
			vehicleAnimatingSprite.DrawFrame(4);
		else if(vehicle == Vehicle.Boat)
			vehicleAnimatingSprite.DrawFrame(5);
		else
			vehicleAnimatingSprite.DrawFrame(8);
	}
}
