﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SnowboardGameManager : MonoBehaviour {

	public SnowboardGamePlayer player;
	public Camera snowboarderCamera;
	public Camera foregroundCamera;
	public Transform backgroundGameObjects;
	public GameCenterPluginManager gameCenter;

	public AudioSource startSound;
	public AudioSource errorSound;
	public AudioSource sfxFanAmbient;

	public ParticleSystem consoleSmoke;


	public TextMesh scoreText;
	public TextMesh highScoreText;
	public TextMesh lastScoreText;
	public TextMesh messageText;
	public GameObject titleObject;

	public GameObject consoleFan;
	public GameObject errorScreen;

	public bool gameStarted = false;
	public bool gameEnded = false;
	public bool gameErrored = false;

	private float curGameTimer;

	private float endGameTimer = 2.0f;
	private float curEndGameTimer;

	private float cliffPieceYOffset = 9.0f;
	private float lastCliffYPos = 0.0f;
	public GameObject[] cliffObjectsLeft;
	public GameObject[] cliffObjectsRight;

	private int cliffIndex = 0;

	public Sprite[] treeSprites;
	public Sprite[] rockSprites;
	public Sprite[] snowLumpSprites;

	public GameObject treePrefab;
	private GameObject[] treePool;

	public GameObject coinPrefab;
	private GameObject[] coinPool;

	public GameObject coinBetterPrefab;
	private GameObject[] betterCoinPool;

	public GameObject rockPrefab;
	private GameObject[] rockPool;

	public GameObject snowLumpPrefab;
	private GameObject[] snowLumpPool;

	private int coins = 0;

	private int treeIndex = 0;
	private int maxTrees = 30;

	private int coinIndex = 0;
	private int maxCoins = 30;

	private int betterCoinIndex = 0;
	private int maxBetterCoins = 10;

	private int rockIndex = 0;
	private int maxRocks = 30;

	private int snowLumpIndex = 0;
	private int maxSnowLumps = 25;

	private float playerCameraDistance = 0.0f;

	private float playerCameraGameOffset = -1.6f;
	private float playerCameraMenuOffset = 8f;
	private float playerCameraYOffset = -1.6f;
	private float curCameraTimer;
	private float cameraTimer = 0.5f;

	private Vector3 origPlayerPosition;

	private float lastCoinCheckYPos = 1.0f;
	private float lastTreeCheckYPos = 0.0f;
	private float lastLumpCheckYPos = 0.0f;

	private float lastCoinXPos;
	private float lastTreeXPos;
	private float lastSnowLumpXPos;
	private float spawnDistance = 8.0f;

	private float distanceCheckToSpawnTree = 2.0f;
	private float maxDistanceCheckToSpawnTree;
	private float minDistanceCheckToSpawnTree = 1.0f;

	private float distanceCheckToSpawnCoin = 2.0f;
	private float maxDistanceCheckToSpawnCoin;
	private float minDistanceCheckToSpawnCoin = 0.5f;

	private float distanceCheckToSpawnSnowLump = 12.0f;

	private float objectXSpawnWidth = 10f;
	private float objectMaxXDistance = 1.0f;

	private float curStartMessageTime;
	private float startMessageTime = 1.0f;
	private bool startMessageOn = true;

	private bool inputDown = true;
	private float inputCooldown = 0.05f;

	private float errorScreenTimer = 5.0f;

	private GameConsoleTimer gameConsoleTimer;

	void Start () 
	{
		if (gameCenter == null)
			gameCenter = FindObjectOfType<GameCenterPluginManager>();

		if (Utils.IphoneXCheck())
		{
			scoreText.transform.position += Vector3.down * 0.5f;
			scoreText.transform.position += Vector3.right * 0.25f;
		}

		GameState.SharedInstance.HasPlayedGameConsole = true;
			
		GameObject gameConsoleTimerObject = new GameObject("GameConsoleTimer");
		gameConsoleTimer = gameConsoleTimerObject.AddComponent<GameConsoleTimer>();
		gameConsoleTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.GameConsole));

		//Application.targetFrameRate = 60;

		UpgradeItem hasFan = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.RetroFan);
		bool hasFanTrue = false;
		if (hasFan != null)
			hasFanTrue = (hasFan.CurrentUpgradeStage != 0);

		if (hasFanTrue)
		{
			curGameTimer = GameServerSettings.SharedInstance.SurferSettings.GameConsoleMaxGameTime * 2.0f;
			consoleFan.SetActive(true);
			GlobalSettings.PlaySound(sfxFanAmbient);
		}
		else
		{
			curGameTimer = GameServerSettings.SharedInstance.SurferSettings.GameConsoleMaxGameTime;
			consoleFan.SetActive(false);
		}


		errorScreen.SetActive(false);

		consoleSmoke.Stop();
		consoleSmoke.gameObject.SetActive(false);


		treePool = new GameObject[maxTrees];
		for (int i = 0; i < maxTrees; i++)
		{
			treePool[i] = GameObject.Instantiate(treePrefab);
			SnowboardZSwapObject swapObject = treePool[i].GetComponent<SnowboardZSwapObject>();
			if (swapObject != null)
				swapObject.player = player;
			treePool[i].SetActive(false);
		}

		coinPool = new GameObject[maxCoins];
		for (int i = 0; i < maxCoins; i++)
		{
			coinPool[i] = GameObject.Instantiate(coinPrefab);
			SnowboardZSwapObject swapObject = coinPool[i].GetComponent<SnowboardZSwapObject>();
			if (swapObject != null)
				swapObject.player = player;
			coinPool[i].SetActive(false);
		}

		betterCoinPool = new GameObject[maxBetterCoins];
		for (int i = 0; i < maxBetterCoins; i++)
		{
			betterCoinPool[i] = GameObject.Instantiate(coinBetterPrefab);
			SnowboardZSwapObject swapObject = betterCoinPool[i].GetComponent<SnowboardZSwapObject>();
			if (swapObject != null)
				swapObject.player = player;
			betterCoinPool[i].SetActive(false);
		}

		rockPool = new GameObject[maxRocks];
		for (int i = 0; i < maxRocks; i++)
		{
			rockPool[i] = GameObject.Instantiate(rockPrefab);
			SnowboardZSwapObject swapObject = rockPool[i].GetComponent<SnowboardZSwapObject>();
			if (swapObject != null)
				swapObject.player = player;
			rockPool[i].SetActive(false);
		}

		snowLumpPool = new GameObject[maxSnowLumps];
		for (int i  = 0; i < maxSnowLumps; i++)
		{
			snowLumpPool[i] = GameObject.Instantiate(snowLumpPrefab);
			SnowboardZSwapObject swapObject = snowLumpPool[i].GetComponent<SnowboardZSwapObject>();
			if (swapObject != null)
				swapObject.player = player;
			snowLumpPool[i].SetActive(false);

			snowLumpPool[i].transform.position = new Vector3(snowLumpPool[i].transform.position.x , snowLumpPool[i].transform.position.y , player.transform.position.z + 1);
		}

		GlobalSettings.PlaySound(startSound);

		playerCameraDistance = player.transform.position.z - snowboarderCamera.transform.position.z;

		playerCameraYOffset = playerCameraMenuOffset;
		curCameraTimer = cameraTimer;

		snowboarderCamera.transform.position = player.transform.position + -Vector3.forward * playerCameraDistance;
		snowboarderCamera.transform.position += Vector3.up * playerCameraYOffset;

		maxDistanceCheckToSpawnTree = distanceCheckToSpawnTree;
		maxDistanceCheckToSpawnCoin = distanceCheckToSpawnCoin;

		scoreText.text = "0";
		scoreText.gameObject.SetActive(false);
		lastScoreText.gameObject.SetActive(false);

		if (PlayerPrefs.HasKey("SS_SnowBoardHighscore"))
			highScoreText.text = string.Format("Best: {0}", PlayerPrefs.GetInt("SS_SnowBoardHighscore", 0));
		else
			highScoreText.gameObject.SetActive(false);

		curStartMessageTime = startMessageTime;

		lastCliffYPos = -cliffPieceYOffset;

	}

	void Reset()
	{
		GlobalSettings.PlaySound(startSound);
	
		for (int i = 0; i < maxTrees; i++)
		{
			treePool[i].SetActive(false);
		}

		for (int i = 0; i < maxCoins; i++)
		{
			coinPool[i].SetActive(false);
		}

		for (int i = 0; i < maxRocks; i++)
		{
			rockPool[i].SetActive(false);
		}

		for (int i = 0; i < maxSnowLumps; i++)
		{
			snowLumpPool[i].SetActive(false);
		}
		for (int i = 0; i < maxBetterCoins; i++)
		{
			betterCoinPool[i].SetActive(false);
		}

		titleObject.SetActive(true);
		scoreText.text = "0";
		scoreText.gameObject.SetActive(false);

		playerCameraYOffset = playerCameraMenuOffset;
		curCameraTimer = cameraTimer;

		distanceCheckToSpawnTree = maxDistanceCheckToSpawnTree;
		distanceCheckToSpawnCoin = maxDistanceCheckToSpawnCoin;

		player.Reset();

		gameStarted = false;
		gameEnded = false;

		highScoreText.gameObject.SetActive(true);
		lastScoreText.gameObject.SetActive(true);

		highScoreText.text = string.Format("Best: {0}", PlayerPrefs.GetInt("SS_SnowBoardHighscore", 0)); ;
		lastScoreText.text = string.Format("Score: {0}", coins);

		coins = 0;

	}

	void Update () 
	{
		if (gameEnded)
		{
			player.PlayerUpdate();
		
			if (curEndGameTimer > 0)
				curEndGameTimer -= Time.deltaTime;
			else
				Reset();
		}
		else if (gameStarted)
		{
			if (curGameTimer > 0)
			{
				curGameTimer -= Time.deltaTime;
				if (curGameTimer <= 0)
				{
					consoleSmoke.gameObject.SetActive(true);
					consoleSmoke.Play();
				}
			}

			playerCameraYOffset = Mathf.Lerp(playerCameraMenuOffset, playerCameraGameOffset, 1.0f - curCameraTimer / cameraTimer);

			if (inputCooldown > 0)
			{
				inputCooldown -= Time.deltaTime;
			}
			else if (Input.GetMouseButton(0) && !inputDown)
			{	
				player.ChangeDirection();
				inputDown = true;
			}
			else if (!Input.GetMouseButton(0))
				inputDown = false;

			if (curCameraTimer > 0)
				curCameraTimer -= Time.deltaTime;
			else
			{
				player.PlayerUpdate();
			
				if (lastTreeCheckYPos - distanceCheckToSpawnTree > player.transform.position.y)
					SpawnObstacle();
				if (lastCoinCheckYPos - distanceCheckToSpawnCoin > player.transform.position.y)
					SpawnCoin();
			}
			distanceCheckToSpawnTree = Mathf.Max(minDistanceCheckToSpawnTree, distanceCheckToSpawnTree - Time.deltaTime * 0.02f);				
			distanceCheckToSpawnCoin = Mathf.Max(minDistanceCheckToSpawnTree, distanceCheckToSpawnTree - Time.deltaTime * 0.02f);				
		}
		else
		{
			player.PlayerUpdate();

			if (gameErrored)
			{
				if (errorScreenTimer > 0)
					errorScreenTimer -= Time.deltaTime;
				else
				{
					#if UNITY_ANDROID
						AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)foregroundCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

				        if(droidLoader != null)
				        {
				            droidLoader.Show();
				        }
			        #endif
					SceneNavigator.NavigateTo(SceneType.Shack);
				}

			}

			if (Input.GetMouseButton(0) && !inputDown)
			{
				if (inputCooldown > 0)
					inputCooldown -= Time.deltaTime;
				else
				{
					if (!gameErrored)
						StartGame();
					else
					{
						#if UNITY_ANDROID
							AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)foregroundCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

					        if(droidLoader != null)
					        {
					            droidLoader.Show();
					        }
				        #endif
						SceneNavigator.NavigateTo(SceneType.Shack);
					}
				}
					
			}
			else if (!Input.GetMouseButton(0))
				inputDown = false;

			curStartMessageTime -= Time.deltaTime;
			if (curStartMessageTime <= 0)
			{
				curStartMessageTime = startMessageTime;
				if (startMessageOn)
				{
					startMessageOn = false;
					messageText.gameObject.SetActive(false);
				}
				else
				{
					startMessageOn = true;
					messageText.gameObject.SetActive(true);
				}
			}
		}

		if (lastLumpCheckYPos - distanceCheckToSpawnSnowLump > player.transform.position.y)
			SpawnSnowLump();

		backgroundGameObjects.transform.position = new Vector3(backgroundGameObjects.transform.position.x, player.transform.position.y + playerCameraYOffset, backgroundGameObjects.transform.position.z);
			
		snowboarderCamera.transform.position = player.transform.position + -Vector3.forward * playerCameraDistance;
		snowboarderCamera.transform.position += Vector3.up * playerCameraYOffset;


		UpdateCliffs();
	}

	public void StartGame()
	{
		if (curGameTimer <= 0)
		{
			scoreText.gameObject.SetActive(false);
			highScoreText.gameObject.SetActive(false);
			lastScoreText.gameObject.SetActive(false);

			titleObject.SetActive(false);

			errorScreen.SetActive(true);
			GlobalSettings.PlaySound(errorSound);
			gameErrored = true;
			inputCooldown = 0.5f;
		}
		else if (!gameStarted)
		{
			gameStarted = true;
			player.gameStarted = true;

			scoreText.text = "0";
			scoreText.gameObject.SetActive(true);
			highScoreText.gameObject.SetActive(false);
			lastScoreText.gameObject.SetActive(false);

			titleObject.SetActive(false);
		}
	}


	public void AddCoin()
	{
		coins ++;
		scoreText.text = coins.ToString();
	}

	public void AddBetterCoin()
	{
		coins += 15;
		scoreText.text = coins.ToString();
	}

	void UpdateCliffs()
	{
		if (player.transform.position.y < lastCliffYPos)
		{
			cliffObjectsLeft[cliffIndex].transform.position += new Vector3(0, - cliffPieceYOffset * 3.0f ,0);
			cliffObjectsRight[cliffIndex].transform.position += new Vector3(0, - cliffPieceYOffset * 3.0f ,0);
			lastCliffYPos -= cliffPieceYOffset;

			cliffIndex = (cliffIndex + 1) % cliffObjectsLeft.Length;
		}
	}

	public void EndGame()
	{
		gameEnded = true;
		curEndGameTimer = endGameTimer;

		GameState.SharedInstance.AddCash(coins);
		gameCenter.SubmitHighScore( coins, LeaderboardType.SnowSurferBoard);

		if (PlayerPrefs.GetInt("SS_SnowBoardHighscore", 0) < coins)
			PlayerPrefs.SetInt("SS_SnowBoardHighscore", coins);
		

	}

	void SpawnSnowLump()
	{
		float lumpXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
		float lumpYPos = player.transform.position.y - spawnDistance;

		int checkers = 0;
		while (Mathf.Abs(lumpXPos - lastSnowLumpXPos) < objectMaxXDistance && checkers < 5)
		{
			lumpXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
			checkers++;
		}

		Vector3 lumpPosition = new Vector3( lumpXPos,
											lumpYPos + Random.Range(-0.5f, 0.5f),
											player.transform.position.z + 1 );
		lumpPosition += transform.position;

		SpriteRenderer spriteRenderer = snowLumpPool[snowLumpIndex].GetComponent<SpriteRenderer>();
		if (spriteRenderer != null)
			spriteRenderer.sprite = snowLumpSprites[Random.Range(0, snowLumpSprites.Length)];

		snowLumpPool[snowLumpIndex].transform.position = lumpPosition;
		snowLumpPool[snowLumpIndex].SetActive(true);
		lastSnowLumpXPos = lumpXPos;
		snowLumpIndex = (snowLumpIndex + 1) % maxSnowLumps;

		lastLumpCheckYPos = player.transform.position.y - distanceCheckToSpawnTree;
	}

	void SpawnObstacle()
	{
		int treesToSpawn = Random.Range(1, 3);

		float treeYPos = player.transform.position.y - spawnDistance;

		for (int i = 0; i < treesToSpawn; i++)
		{
			int randChance = Random.Range(0, 3);

			float treeXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
			int checkers = 0;
			while (Mathf.Abs(treeXPos - lastTreeXPos) < objectMaxXDistance && checkers < 5)
			{
				treeXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
				checkers++;
			}

			Vector3 treePosition = new Vector3( treeXPos,
													treeYPos + Random.Range(-0.5f, 0.5f),
													player.transform.position.z - 1);
			treePosition += transform.position;
													
			if (randChance > 0)
			{
				//tree
				SpriteRenderer spriteRenderer = treePool[treeIndex].GetComponent<SpriteRenderer>();
				if (spriteRenderer != null)
					spriteRenderer.sprite = treeSprites[Random.Range(0, treeSprites.Length)];

				treePool[treeIndex].transform.position = treePosition;
				treePool[treeIndex].SetActive(true);
					
				lastTreeXPos = treeXPos;
				treeIndex = (treeIndex + 1) % maxTrees;
			}
			else //rocks
			{
				SpriteRenderer spriteRenderer = rockPool[rockIndex].GetComponent<SpriteRenderer>();
				if (spriteRenderer != null)
					spriteRenderer.sprite = rockSprites[Random.Range(0, rockSprites.Length)];

				rockPool[rockIndex].transform.position = treePosition;
				rockPool[rockIndex].SetActive(true);
				lastTreeXPos = treeXPos;
				rockIndex = (rockIndex + 1) % maxRocks;
			}
		}
		lastTreeCheckYPos = player.transform.position.y - distanceCheckToSpawnTree;
	}

	void SpawnCoin()
	{
		float coinYPos = player.transform.position.y - spawnDistance;

		int coinsToSpawn = Random.Range(1, 3);

		for (int i = 0 ; i < coinsToSpawn; i++)
		{
			float coinXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
			int checkers = 0;
			while (Mathf.Abs(coinYPos - lastCoinXPos) < objectMaxXDistance && checkers < 5)
			{
				coinXPos = Random.Range(-objectXSpawnWidth * 0.5f, objectXSpawnWidth * 0.5f);
				checkers++;
			}

			Vector3 coinPosition = new Vector3( coinXPos,
												coinYPos + Random.Range(-0.5f, 0.5f),
												player.transform.position.z - 1.25f );

			coinPosition += transform.position;

			if (Random.Range(0, 5) == 0)
			{
				betterCoinPool[betterCoinIndex].transform.position = coinPosition;
				betterCoinPool[betterCoinIndex].SetActive(true);
				betterCoinIndex = (betterCoinIndex + 1) % maxBetterCoins;
			}
			else
			{
				coinPool[coinIndex].transform.position = coinPosition;
				coinPool[coinIndex].SetActive(true);
				coinIndex = (coinIndex + 1) % maxCoins;
			}
			lastCoinXPos = coinXPos;
		}

		lastCoinCheckYPos = player.transform.position.y - distanceCheckToSpawnCoin;
	}
}
