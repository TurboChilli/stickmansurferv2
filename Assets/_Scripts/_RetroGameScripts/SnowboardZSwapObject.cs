﻿using UnityEngine;
using System.Collections;

public class SnowboardZSwapObject : MonoBehaviour {

	public SnowboardGamePlayer player;

	private float zOffset = 0.5f;

	void Update () 
	{
		if (player == null)
			player = FindObjectOfType<SnowboardGamePlayer>();
		
		if (player.transform.position.y > transform.position.y)
			transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z - zOffset);
		else
			transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z + zOffset);

	}
}
