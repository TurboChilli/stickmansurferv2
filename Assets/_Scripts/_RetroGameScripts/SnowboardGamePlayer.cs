﻿using UnityEngine;
using System.Collections;

public class SnowboardGamePlayer : MonoBehaviour 
{
	SnowboardGameManager gameManager;

	public AudioSource crashSound;
	public AudioSource turnSound;
	public AudioSource fallSound;
	public AudioSource coinSound;
	public AudioSource betterCoinSound;

	public bool gameStarted = false;
	//false = left, true = right
	public bool playerDirection = false;
	public bool isCrashed = false;
	public bool isFalling = false;

	public Sprite normalSprite;
	public Sprite intermedieteSprite;
	public Sprite crashSprite;
	public Sprite[] fallSprites;

	private float timeToTurn = 0.08f;
	private float curTimeToTurn = 0.0f;


	private int fallingIndex = 0;
	private float fallingAnimTimer = 0.0f;

	private SpriteRenderer spriteRenderer;

	private float speed;
	private float maxSpeed = 10.0f;
	private float minSpeed = 6.5f;

	void Start () 
	{
		if (gameManager == null)
			gameManager = FindObjectOfType<SnowboardGameManager>();

		spriteRenderer = GetComponent<SpriteRenderer>();

		if (playerDirection)
			spriteRenderer.flipX = true;
		else 
			spriteRenderer.flipX = false;

		speed = minSpeed;
	}

	public void ChangeDirection()
	{
		if (!isCrashed)
		{
			playerDirection = !playerDirection;
			curTimeToTurn = timeToTurn;
			spriteRenderer.sprite = intermedieteSprite;

			GlobalSettings.PlaySound(turnSound);
		}
	}

	public void Crash(bool didFall = false)
	{
		isCrashed = true;
		gameManager.EndGame();

		if (didFall)
		{
			isFalling = true;
			GlobalSettings.PlaySound(fallSound);
		}
		else
		{
			spriteRenderer.sprite = crashSprite;
			GlobalSettings.PlaySound(crashSound);
		}
	}

	public void Reset()
	{
		speed = minSpeed;
		spriteRenderer.sprite = normalSprite;

		fallingIndex = 0;
		fallingAnimTimer = 0.0f;

		isCrashed = false;
		gameStarted = false;
		isFalling = false;
		transform.localPosition = new Vector3( 0.0f, transform.localPosition.y, transform.localPosition.z);
	}

	public void PlayerUpdate () 
	{
		Vector3 movementVector = Vector3.zero;

		if (!isCrashed)
		{
			if (gameStarted)
			{
				if (playerDirection)
					movementVector = new Vector3( 2, -1, 0);
				else
					movementVector = new Vector3(-2, -1, 0);

				speed = Mathf.Min( speed + Time.deltaTime * 0.25f, maxSpeed);
			}
			else
				movementVector = new Vector3(0, -1, 0);
		}

		if (curTimeToTurn > 0)
		{
			curTimeToTurn -= Time.deltaTime;

			if (curTimeToTurn <= 0)
			{
				spriteRenderer.sprite = normalSprite;
				if (playerDirection)
					spriteRenderer.flipX = true;
				else 
					spriteRenderer.flipX = false;
			}
		}

		if (isFalling)
		{
			float fps = 5.0f;

			while (fallingAnimTimer > 1.0f / fps && fallingIndex != fallSprites.Length)
			{
				fallingAnimTimer -= 1.0f / fps;
				spriteRenderer.sprite = fallSprites[fallingIndex];

				fallingIndex ++;
			}

			fallingAnimTimer += Time.deltaTime;
		}
		transform.position += movementVector.normalized * speed * Time.deltaTime;
	}

	public void OnTriggerEnter2D( Collider2D other)
	{
		if (other.tag == "Coin")
		{
			other.gameObject.SetActive(false);
			GlobalSettings.PlaySound(coinSound);
			gameManager.AddCoin();
		}
		else if (other.tag == "Boost")
		{
			other.gameObject.SetActive(false);
			GlobalSettings.PlaySound(betterCoinSound);
			gameManager.AddBetterCoin();
		}
		else if (other.tag == "Rock")
		{
			Crash();
		}
		else if (other.tag == "Cliff")
		{
			Crash(true);
		}
	}

}
