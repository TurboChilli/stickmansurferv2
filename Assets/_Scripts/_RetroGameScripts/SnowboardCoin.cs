﻿using UnityEngine;
using System.Collections;

public class SnowboardCoin : MonoBehaviour {

	public Sprite[] coinFrames;
	private SpriteRenderer spriteRenderer;
	private int index = 0;

	private float fps = 20.0f;
	private float fpsTimer = 0.0f;

	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	void Update () 
	{
		while (fpsTimer > fps / 60.0f)
		{
			fpsTimer -= fps / 60.0f;
			spriteRenderer.sprite = coinFrames[index];

			index = (index + 1) % coinFrames.Length;
		}
		fpsTimer += Time.deltaTime;
	}
}
