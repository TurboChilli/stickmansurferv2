﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class BackButton : MonoBehaviour 
{
	public Camera mainCamera;
	public bool leftAligned = false;

	public Action onBackButtonPress = null;

	void Start () 
	{
		if (leftAligned)
			UIHelpers.SetTransformRelativeToCorner( UIHelpers.GUICorner.CORNER_TOP_LEFT, 1.5f, 1.5f, transform, mainCamera);
		else
			UIHelpers.SetTransformRelativeToCorner( UIHelpers.GUICorner.CORNER_TOP_RIGHT, 1.5f, 1.5f, transform, mainCamera);
	}

	void Update () 
	{
		#if UNITY_ANDROID
		if (Input.GetKeyDown(KeyCode.Escape))
		{
	        AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

	        if(droidLoader != null)
	        {
	            droidLoader.Show();
	        }
		SceneNavigator.NavigateTo(SceneType.Shack);
		}
		#endif

		if (Input.GetMouseButton(0))
		{
			if (UIHelpers.CheckButtonHit( transform, mainCamera))
			{
				if (onBackButtonPress != null)
					onBackButtonPress();

				#if UNITY_ANDROID
					AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

			        if(droidLoader != null)
			        {
			            droidLoader.Show();
			        }
				#endif
		        
				SceneNavigator.NavigateTo(SceneType.Shack);
			}
		}
	}
}
