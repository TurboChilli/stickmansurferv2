﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TestSceneActivityScheduler : MonoBehaviour 
{
/*
	public InputField textOutput;
	Activity[] activities;

	void Start () 
	{
	}

	void Update () 
	{
	}

	public void GetActivitiesButtonClick()
	{
		bool ignoreTutorials = true;

		Scheduler.SharedInstance.SetIgnoreTutorials(ignoreTutorials);
		activities = Scheduler.SharedInstance.GetActivities();

		System.Text.StringBuilder sb = new System.Text.StringBuilder();

		foreach(var activity in activities)
		{
			sb.AppendFormat("ACTIVITY\nActivityType: {0}\tLocation: {1}", 
				(ActivityType)activity.Type,
				(Location)activity.Location);

			var activityDataSurf = activity.GetActivityData<ActivityDataSurf>();
			sb.AppendFormat("\n\nDATA SURF\nWaveType: {0}\tDifficulty: {1}\tStart Vehicle:{2}", 
				(WaveType)activityDataSurf.WaveType,
				(Difficulty)activityDataSurf.Difficulty,
				(Vehicle)activityDataSurf.VehicleAtStart);

			if(activity.Goals.Count > 0)
			{
				sb.Append("\n\nGOALS\n");
				foreach(var goal in activity.Goals)
				{
					sb.AppendFormat("{0} - {1}", 
						(ActivityGoalType)goal.Type,
						goal.GoalDescription());
				}
			}

			sb.Append("-------------------------------------------------------------\n\n");
		}

		textOutput.text = sb.ToString();
		//////Debug.Log(sb.ToString());
	}

	public void CompleteAllActivitiesButtonClick()
	{
		if(activities != null)
		{
			foreach(var activity in activities)
			{
				activity.MarkAsCompleted();
			}
		}
	}
	*/
}
