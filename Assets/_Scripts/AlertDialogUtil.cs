﻿using UnityEngine;
using System.Collections;

using System;
using Prime31;

public class AlertDialogUtil
{
	public static void RegisterAlertButtonClickHandler(Action<string> callbackEventHandler)
	{
		#if UNITY_IOS
		EtceteraManager.alertButtonClickedEvent += callbackEventHandler;
		#elif UNITY_ANDROID
		EtceteraAndroidManager.alertButtonClickedEvent += callbackEventHandler;
		#endif
	}

	public static void DeregisterAlertButtonClickHandler(Action<string> callbackEventHandler)
	{
		#if UNITY_IOS
		EtceteraManager.alertButtonClickedEvent -= callbackEventHandler;
		#elif UNITY_ANDROID
		EtceteraAndroidManager.alertButtonClickedEvent -= callbackEventHandler;
		#endif
	}

	public static void ShowAlert(string title, string message, string buttonText, string buttonText2 = null)
	{
		if(!string.IsNullOrEmpty(title))
			title = GameLanguage.LocalizedText(title);

		if(!string.IsNullOrEmpty(message))
			message = GameLanguage.LocalizedText(message);

		if(!string.IsNullOrEmpty(buttonText))
			buttonText = GameLanguage.LocalizedText(buttonText);

		if(!string.IsNullOrEmpty(buttonText2))
			buttonText2 = GameLanguage.LocalizedText(buttonText2);

		if(buttonText2 == null)
		{
			#if UNITY_IOS
			EtceteraBinding.showAlertWithTitleMessageAndButtons(title, message, new string[] { buttonText });
			#elif UNITY_ANDROID
			EtceteraAndroid.showAlert(title, message, buttonText);
			#elif UNITY_EDITOR
			Debug.LogFormat("AlertDialogUtil -> ShowAlert title: {0} message: {1} buttonText: {2}", title, message, buttonText);
			#endif
		}
		else
		{
			#if UNITY_IOS
			EtceteraBinding.showAlertWithTitleMessageAndButtons(title, message, new string[] { buttonText, buttonText2 });
			#elif UNITY_ANDROID
			EtceteraAndroid.showAlert(title, message, buttonText, buttonText2);
			#elif UNITY_EDITOR
			Debug.LogFormat("AlertDialogUtil -> ShowAlert title: {0} message: {1} buttonText: {2} buttonText2: {3}", title, message, buttonText, buttonText2);
			#endif
		}
	}
}
