﻿using UnityEngine;
using System.Collections;
using Prime31;
using System.IO;


public class PhotoAlbumManager : MonoBehaviour {

    private string imagePath = "";

    void OnDisable()
    {
        // Stop listening to the image picker event
        #if UNITY_IOS
        EtceteraManager.imagePickerChoseImageEvent -= ImagePickerChoseImage;
        #elif UNITY_ANDROID
		EtceteraAndroidManager.photoChooserSucceededEvent -= ImagePickerChoseImage;
		#endif
    }

    void Start()
    {
    	#if UNITY_IOS
        EtceteraManager.imagePickerChoseImageEvent += ImagePickerChoseImage;
        #elif UNITY_ANDROID
		EtceteraAndroidManager.photoChooserSucceededEvent += ImagePickerChoseImage;
        #endif

        if (PlayerPrefs.HasKey("CustomPic"))
        {
            Debug.Log("LOADING CUSTOM PIC FROM FILE HERE");
            LoadPictureFromSaveFile();
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void LoadPictureFromSaveFile()
    {
        byte[] imageBytes = File.ReadAllBytes(Application.persistentDataPath + "/currentPicture.png");

        Texture2D currentPic = (Texture2D)transform.GetComponent<Renderer>().material.mainTexture;
        currentPic.LoadImage(imageBytes);
    }

    public void LoadPictureFromAlbum()
    {
    	#if UNITY_IOS
        	EtceteraBinding.promptForPhoto( 0.25f, PhotoPromptType.CameraAndAlbum );
        #elif UNITY_ANDROID
        	EtceteraAndroid.promptToTakePhoto( "" );
        #endif
        // No need to resize because we asked for an image scaled from the picker but this is how we sould do it if we wanted to
        // Resize the image so that we dont end up trying to load a gigantic image
        //EtceteraBinding.resizeImageAtPath( imagePath, 256, 256 );

        // Add 'file://' to the imagePath so that it is accessible via the WWW class

    }

    void ImagePickerChoseImage( string imgPath )
    {

#if UNITY_IOS
        this.imagePath = imgPath;
        byte[] imageBytes = File.ReadAllBytes(imagePath);

        Texture2D currentPic = (Texture2D)transform.GetComponent<Renderer>().material.mainTexture;

        currentPic.LoadImage(imageBytes);
        textureLoaded(currentPic);
        //EtceteraBinding.promptForPhoto(0.25f, PhotoPromptType.CameraAndAlbum);//EtceteraManager.textureFromFileAtPath( "file://" + imagePath, textureLoaded, textureLoadFailed ) );
#elif UNITY_ANDROID
        		StartCoroutine( LoadPhotoTexture( "file://" + imagePath ));
                
#endif
    }


#if UNITY_ANDROID
    IEnumerator LoadPhotoTexture(string filePath)
    {
		Texture2D newTexture = EtceteraAndroid.textureFromFileAtPath(filePath);

		if (newTexture != null)
			textureLoaded(newTexture);
		else
			textureLoadFailed("");
		return null;
    }
#endif



        // Texture loading delegates
    public void textureLoaded( Texture2D texture )
    {
        transform.GetComponent<Renderer>().material.mainTexture = texture;
        byte[] imageBytes = texture.EncodeToPNG();
        Debug.Log("BEFORE WRITING FILE HERE");
        File.WriteAllBytes(Application.persistentDataPath + "/currentPicture.png", imageBytes);
        PlayerPrefs.SetInt("CustomPic", 1);
        Debug.Log("FINISHED WRITING FILE HERE");
    }

    public void textureLoadFailed( string error )
    {
		AlertDialogUtil.ShowAlert("Unable to load image", error, "OK");
    }
}
