﻿using UnityEngine;
using System.Collections;

public class RotationClickableJiggle : MonoBehaviour {

	private Quaternion origRotation = Quaternion.identity;
	private float timer = 0.0f;

	public float jiggleAngle = 7.5f;
	public float jiggleSpeed = 40.0f;

	public float breakTime = 0.5f;
	public float jiggleTime = 0.5f;
	private bool jiggling = false;

	void OnEnable () 
	{
		origRotation = transform.rotation;
		timer = Random.Range(0.0f, breakTime);
		jiggling = false;
	}

	void OnDisable()
	{
		transform.rotation = origRotation;
	}
	
	void Update () 
	{
		if (jiggling)
		{
			float jiggleWeight = Mathf.Sin( (timer / jiggleTime) * Mathf.PI);
			float angle = Mathf.Sin(Time.time * jiggleSpeed) * jiggleAngle;

			transform.rotation = origRotation * Quaternion.AngleAxis(angle * jiggleWeight, Vector3.forward);
		}

		timer += Time.deltaTime;

		if (timer > breakTime && !jiggling)
		{
			jiggling = true;
			timer = 0.0f;
		}
		else if (timer > jiggleTime & jiggling)
		{
			jiggling = false;
			timer = 0.0f;
		}
	}
}
