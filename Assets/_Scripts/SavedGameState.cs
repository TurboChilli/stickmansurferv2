﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

[DynamoDBTable("SavedGameState")]
public class SavedGameState
{
	[DynamoDBHashKey]   // Hash key (Primary key)
	public string SavedGameStateId;

	[DynamoDBProperty("psw")]
	public string Password;

	[DynamoDBProperty("sv")]
	public int Version;

	[DynamoDBProperty("cn")]
	public int Coins;

	[DynamoDBProperty("ed")]
	public int EnergyDrinks;

	[DynamoDBProperty("cl")]
	public int CurrentLevel;

	// Multi-valued (set type) attribute. 
	[DynamoDBProperty("shop")]
	public string ShopUpgradeItemsString;

	[DynamoDBIgnore]
	public int LastVersionSynced
	{
		get { return PlayerPrefs.GetInt("sgsLVS", 0); }
		set { PlayerPrefs.SetInt("sgsLVS", value); }
	}
}

