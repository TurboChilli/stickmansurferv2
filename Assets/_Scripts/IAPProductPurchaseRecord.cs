﻿using UnityEngine;
using System.Collections;

public class IAPProductPurchaseRecord
{
	public static string VerifiedPurchaseProductId;
	public static string PurchaseProductId;
	public static string CurrencyString;
	public static float Price;

	// Default at 1
	public const int Quantity = 1;
	public static int ResponseCode;
	public static string PurchaseTransactionData;
	public static string PurchaseTransactionIdentifier;

	public static void ClearPurchaseRecord()
	{
		VerifiedPurchaseProductId = null;
		PurchaseProductId = null;
		CurrencyString = null;
		Price = 0;
		ResponseCode = 0;
		PurchaseTransactionData = null;
		PurchaseTransactionIdentifier = null;
	}

	public static bool IAPMatchesVerifiedPurchase()
	{
		if(!string.IsNullOrEmpty(VerifiedPurchaseProductId) && !string.IsNullOrEmpty(VerifiedPurchaseProductId))
		{
			if(VerifiedPurchaseProductId.ToLowerInvariant() == VerifiedPurchaseProductId.ToLowerInvariant())
				return true;
		}

		return false;
	}
}
