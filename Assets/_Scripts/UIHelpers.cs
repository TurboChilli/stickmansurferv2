﻿using UnityEngine;
using System.Collections;

public class UIHelpers {
    
    public enum GUICorner
    {
		CORNER_TOP,
        CORNER_TOP_LEFT,
        CORNER_TOP_RIGHT,
        CORNER_BOTTOM_LEFT,
        CORNER_BOTTOM_RIGHT,
        SIDE_LEFT,
        SIDE_RIGHT
    };

    public static bool CheckButtonHit(Transform buttonCheck)
    {
		return CheckButtonHit(buttonCheck, Camera.main);
    }



	public static bool CheckButtonHit(Transform buttonCheck, Camera camera) 
	{        
		if(!camera.isActiveAndEnabled)
        {
			return false;
        }

		Vector2 touchPos = Input.mousePosition;
		RaycastHit hit;
		if (Physics.Raycast (camera.ScreenPointToRay(touchPos), out hit) )
		{ 
			if(hit.transform.Equals(buttonCheck))
			{
				return true;
			}
		}
		return false;
	}

    public static bool CheckButtonHitWithName(Transform buttonCheck, string buttonName)
    {
        Vector2 touchPos = Input.mousePosition;
        RaycastHit hit;
        if (Physics.Raycast (Camera.main.ScreenPointToRay(touchPos), out hit))
        {   
            if(hit.transform.Equals(buttonCheck) && 
                string.Equals(hit.transform.gameObject.name, buttonName, System.StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
        }
        return false;
    }
    
	public static bool IsIPadRes()
	{
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;

		if(screenWidth > 50 && screenHeight > 50)
		{
			float ratio = screenHeight > screenWidth ? (screenWidth / screenHeight) : (screenHeight / screenWidth);

			// Set custom orthographic size if iPad
			if(ratio >= 0.725f)
			{
				return true;
			}
		}

		return false;
	}

    public static void SetTransformRelativeToCorner(GUICorner theCorner, float xPaddingScreenPixels,
                                  float yPaddingScreenPixels, Transform theTransform, Camera theCamera)
    {
        SetTransformRelativeToCorner(theCorner, xPaddingScreenPixels, yPaddingScreenPixels, theTransform, theCamera, 0);
    }

	public static void SetTransformRelativeToCorner(GUICorner theCorner, float xPaddingUnits, float yPaddingUnits,
        											Transform theTransform, Camera theCamera, float positionXOffSet)
    {
        //Screenspace is defined in pixels. The bottom-left of the screen is (0,0); the right-top is (pixelWidth,pixelHeight).
        Vector3 transPos = new Vector3(0,0,0);
        float prevZPos = theTransform.position.z;
		float prevXPos = theTransform.position.x;

		if(theCorner == GUICorner.CORNER_TOP)
		{
			transPos = theCamera.ScreenToWorldPoint (new Vector3 (0, theCamera.pixelHeight, theCamera.nearClipPlane));
			transPos = new Vector3(prevXPos, transPos.y - yPaddingUnits, transPos.z);
		}
        else if(theCorner == GUICorner.CORNER_TOP_LEFT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (0, theCamera.pixelHeight, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet + xPaddingUnits, transPos.y - yPaddingUnits, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_TOP_RIGHT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth ,theCamera.pixelHeight, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet - xPaddingUnits, transPos.y - yPaddingUnits, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_BOTTOM_LEFT)
        {
            transPos = theCamera.ViewportToWorldPoint(new Vector3(0, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet + xPaddingUnits, transPos.y + yPaddingUnits, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_BOTTOM_RIGHT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet - xPaddingUnits, transPos.y + yPaddingUnits, transPos.z);
        }
        else if(theCorner == GUICorner.SIDE_LEFT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (0, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet + xPaddingUnits, theTransform.position.y, transPos.z);
        }
        else if(theCorner == GUICorner.SIDE_RIGHT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet - xPaddingUnits, theTransform.position.y, transPos.z);
        }

        theTransform.position = new Vector3(transPos.x, transPos.y, prevZPos); 
    }

	public static void SetTransformRelativeToCenter(float xPaddingUnits, float yPaddingUnits,
        											Transform theTransform, Camera theCamera, float positionXOffSet = 0.0f)
	{
		Vector3 transPos = new Vector3(0,0,0);
        float prevZPos = theTransform.position.z;

		transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth * 0.5f, theCamera.pixelHeight * 0.5f, theCamera.nearClipPlane));
        transPos = new Vector3(transPos.x + positionXOffSet - xPaddingUnits, theTransform.position.y, transPos.z);

		theTransform.position = new Vector3(transPos.x, transPos.y, prevZPos);     
	}

	public static void SetTransformToCenter(Transform theTransform, Camera theCamera)
	{
		float transPosZ = theTransform.position.z;
		Vector3 camPos = theCamera.transform.position;
		theTransform.position = new Vector3(camPos.x, camPos.y, transPosZ);     
	}
}
