﻿using UnityEngine;
using System.Collections;

public class InfoPopupPanel : MonoBehaviour {

    public GameObject infoPopupPanel;
    public GameObject noCoinInfoPopupPanel;
    public GameObject closeButton;
    public TextMesh theMessage;



    public void SetPopupPanelMessage(string itemName)
    {
        theMessage.text = itemName;
    }



    public void ShowHidePopupMessagePanel(GameObject theMessagePanel, bool state)
    {        
        theMessagePanel.SetActive(state);
    }



    public bool IsMessagePanelCloseButtonClicked()
    {
        bool result = false;
        if (UIHelpers.CheckButtonHit(closeButton.transform)) 
        {   
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }
}
