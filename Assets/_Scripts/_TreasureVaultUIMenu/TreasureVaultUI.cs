﻿using UnityEngine;
using System.Collections;
using TMPro;

public class TreasureVaultUI : MonoBehaviour {
    
    public TreasureVaultUITimer treasureVaultUITimer;
    public CoinBar coinbar;
    public NumericDisplayBar energyDrinkBar;
    public NumericDisplayBar currencyBarPlaceholder;

    public GameObject backButton;
    public GameObject backArrowhand;

	public GameObject coinStackSmall;
    public GameObject coinStackMedium;
	public GameObject coinStackLarge;
    
	public GameObject energyDrinkSmall;
	public GameObject energyDrinkMedium;
    public GameObject energyDrinkLarge;

    public GameObject llamaPlaceholder;
    public GameObject textPanel;
    public GameObject shinyBackground;
    public RotationClickableJiggle textPanelJiggle;

    public TextMeshPro timerText;
    public TextMeshPro openText;
    public TextMeshPro mainPanelText;

    public AudioSource swooshSound;
    public AudioSource kaChingSound;
    public AudioSource energyDrinkSound;
	public AudioSource chestOpenSound;
	public AudioSource chestCreak;
	public AudioSource drumroll;
	public AudioSource fanfareSound;

	public ParticleSystem coinExplosionParticles;

    public void SetTimeCountPanelText(string theTimerText)
    {
        timerText.text = theTimerText;
    }

	public void SetTimeCountOpenText(string theOpenText)
    {
		openText.text = theOpenText;
    }

    public void SetMainPanelText(string theMainPaneltext)
    {
        mainPanelText.text = theMainPaneltext;
    }
   
}
