﻿using UnityEngine;
using System.Collections;
using System;

public class TreasureVaultUITimer : MonoBehaviour {

    public TreasureVaultUIController treasureVaultUIController;
    private DateTime openDateTime;
    private TimeSpan timeDifference;
    private int timerInMinutes = 1;
    private bool timeIsUp = false;
    public TreasureVaultUI treasureVaultUI;
    public TreasureVault treasureVault;
    public AudioSource countDownSound;
    public AudioSource timeUpSound;
    private bool playTimesUpSound = false;
    private double totalSecs = 0;
    private int countdown = 5;
    public MainMenuController mainMenuController;
	private bool showingOpenText = false;

	private bool isInitialised = false;

    public bool TimeStatus
    {	
        get
        {
			if (!isInitialised)
				Initialise();

            return timeIsUp;
        }
        private set 
        {
            timeIsUp = value;
        }
    }

    public int TimerLength
    {
        get
        {
			if (!isInitialised)
				Initialise();

            return timerInMinutes;
        }
    }

    // Use this for initialization
    void Start () 
    {
		if (!isInitialised)
			Initialise();
    }

    void Initialise()
    {
		if (!isInitialised)
		{
			if ((int)GameState.SharedInstance.TutorialStage > (int)TutStageType.MainMenuChest)
        	{
				if (GameState.SharedInstance.TreasureVaultTimeStamp == "0")
		        {
					InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Treasure));
					Update();
		        }
		        else
		        {
		            GetOldTime();
		            Update();
		        }
	        }
	        else
	        {
				InitializeTimer(0);
				Update();
	        }

			isInitialised = true;
		}
    }

    // Update is called once per frame
    void Update () {

        timeDifference = openDateTime.Subtract(DateTime.UtcNow);
        totalSecs = timeDifference.TotalSeconds;

        if (totalSecs > 0)
        {
            string result = TimeStringFormatter(timeDifference);         
            treasureVaultUI.SetTimeCountPanelText(result);
			treasureVaultUI.SetTimeCountOpenText("");
            mainMenuController.SetTreasureButtonTimerText(result);
			mainMenuController.SetTreasureButtonNotification(false);
            

            if(((int)totalSecs == countdown || totalSecs < (double)countdown) && (int)totalSecs != 0)
            {
                GlobalSettings.PlaySound(countDownSound);
                playTimesUpSound = true;
                countdown--;
            }
            if((int)totalSecs == 0 && playTimesUpSound)
            {
                GlobalSettings.PlaySound(timeUpSound);
                playTimesUpSound = false;
                countdown = 5;
            }
        }
        else
        {
            timeIsUp = true;
        }
			
		if (timeIsUp && !showingOpenText)
        {
			showingOpenText = true;
            treasureVaultUI.SetTimeCountPanelText("");
			treasureVaultUI.SetTimeCountOpenText(GameLanguage.LocalizedText("OPEN"));
            mainMenuController.SetTreasureButtonTimerText("");
			mainMenuController.SetTreasureButtonNotification(true);
        }
    }

    public void HideText()
    {
		treasureVaultUI.timerText.gameObject.SetActive(false);
    }

    public void ShowText()
    {
		treasureVaultUI.timerText.gameObject.SetActive(true);
    }

    public void InitializeTimer(int minutes)
    {
        openDateTime = DateTime.UtcNow.AddSeconds(minutes);
		GameState.SharedInstance.TreasureVaultTimeStamp = openDateTime.ToBinary().ToString();
        timeIsUp = false;
		showingOpenText = false;
    }

    public DateTime GetOldTime()
    {
		long temp = Convert.ToInt64( GameState.SharedInstance.TreasureVaultTimeStamp);
        openDateTime = DateTime.FromBinary(temp);

        return openDateTime;
    }

    public string TimeStringFormatter(TimeSpan timeDifference)
    {
        string result = string.Format("{0:D1}:{1:D2}:{2:D2}",
            timeDifference.Hours,
            timeDifference.Minutes,
            timeDifference.Seconds);

        return result;
    }
}
