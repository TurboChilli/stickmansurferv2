﻿using UnityEngine;
using System.Collections;

public class TreasureVault : MonoBehaviour {

    public GameObject chestFull;
	public GameObject chestBottomBack;
    public GameObject chestBottomFront;
    public GameObject chestTopOpenHalf;
    public GameObject chestTopOpenFull;

	public GameObject chestBag1;
	public GameObject chestBag2;
    public GameObject chestBag3;

	private Vector3 chestBagOrigScale1;
	private Vector3 chestBagOrigScale2;
    private Vector3 chestBagOrigScale3;

    private float timeToDissapear = 0.10f;
    private float curTimeToDissapear = 0.0f;

    private float timeToOpen = 0.025f;
    private float curTimeToOpen = 0.0f;
    private bool opened = false;

    public enum VaultState
    {
    	Locked,
    	HalfOpened,
    	Opened,
    	TwoBags,
    	OneBag,
    	Empty
  
    }
    private VaultState currVaultState = VaultState.Locked;

    public VaultState VaultStateProperty
    {
        get
        {
            return currVaultState;
        }
        set
        {
            currVaultState = value;
        }
    }       

    public void Update()
    {
		if (curTimeToDissapear >= 0)
		{
			float lerpValue = curTimeToDissapear / timeToDissapear;
			curTimeToDissapear -= Time.deltaTime;
			if (curTimeToDissapear < 0)
				lerpValue = 0;
			switch (currVaultState)
	    	{
	    		case VaultState.TwoBags:
					chestBag1.transform.localScale = Vector3.Lerp(chestBagOrigScale1, Vector3.zero, 1.0f -  Mathf.Pow(lerpValue, 2.0f));
	    		break;
	    		case VaultState.OneBag:
					chestBag2.transform.localScale = Vector3.Lerp(chestBagOrigScale2, Vector3.zero, 1.0f -  Mathf.Pow(lerpValue, 2.0f));
	    		break;
	    		case VaultState.Empty:
					chestBag3.transform.localScale = Vector3.Lerp(chestBagOrigScale3, Vector3.zero, 1.0f - Mathf.Pow(lerpValue, 2.0f));
	    		break;
	    	};
    	}

		if (curTimeToOpen >= 0 && currVaultState == VaultState.Opened && !opened)
		{
			curTimeToOpen -= Time.deltaTime;
			if (curTimeToOpen < 0)
			{
				chestTopOpenHalf.SetActive(false);
				chestTopOpenFull.SetActive(true);
				opened = true;
			}
		}
    }


    public void DrawTreasureVaultComponents(VaultState vaultState)
    {        
		currVaultState = vaultState;

		if (chestBagOrigScale1 == Vector3.zero)
		{
			chestBagOrigScale1 = chestBag1.transform.localScale;
			chestBagOrigScale2 = chestBag2.transform.localScale;
		   	chestBagOrigScale3 = chestBag3.transform.localScale;
		}

    	switch (vaultState)
    	{
    		case VaultState.Locked:
    			chestFull.SetActive(true);
				chestBottomBack.SetActive(false);
				chestBottomFront.SetActive(false);
				chestTopOpenHalf.SetActive(false);
				chestTopOpenFull.SetActive(false);

				chestBag1.SetActive(false);
				chestBag2.SetActive(false);
				chestBag3.SetActive(false);

				chestBag1.transform.localScale = chestBagOrigScale1;
				chestBag2.transform.localScale = chestBagOrigScale2;
				chestBag3.transform.localScale = chestBagOrigScale3;

				opened = false;
    			break;

    		case VaultState.HalfOpened:
				chestFull.SetActive(false);
				chestBottomBack.SetActive(true);
				chestBottomFront.SetActive(true);
				chestTopOpenHalf.SetActive(true);
				chestTopOpenFull.SetActive(false);

				chestBag1.SetActive(true);
				chestBag2.SetActive(true);
				chestBag3.SetActive(true);

				chestBag1.transform.localScale = chestBagOrigScale1;
				chestBag2.transform.localScale = chestBagOrigScale2;
				chestBag3.transform.localScale = chestBagOrigScale3;

    			break;

    		case VaultState.Opened:
				chestFull.SetActive(false);
				chestBottomBack.SetActive(true);
				chestBottomFront.SetActive(true);

				if (opened)
				{
					chestTopOpenHalf.SetActive(false);
					chestTopOpenFull.SetActive(true);
				}
				else
				{
					chestTopOpenHalf.SetActive(true);
					chestTopOpenFull.SetActive(false);
				}

				chestBag1.SetActive(true);
				chestBag2.SetActive(true);
				chestBag3.SetActive(true);

				curTimeToOpen = timeToOpen;
    			break;

    		case VaultState.TwoBags:
				chestFull.SetActive(false);
				chestBottomBack.SetActive(true);
				chestBottomFront.SetActive(true);
				chestTopOpenHalf.SetActive(false);
				chestTopOpenFull.SetActive(true);

				curTimeToDissapear = timeToDissapear;
				chestBag1.SetActive(true);
				chestBag2.SetActive(true);
				chestBag3.SetActive(true);
    			break;

    		case VaultState.OneBag:
				chestFull.SetActive(false);
				chestBottomBack.SetActive(true);
				chestBottomFront.SetActive(true);
				chestTopOpenHalf.SetActive(false);
				chestTopOpenFull.SetActive(true);

				curTimeToDissapear = timeToDissapear;			
				chestBag1.SetActive(false);
				chestBag2.SetActive(true);
				chestBag3.SetActive(true);
    			break;

    		case VaultState.Empty:
				chestFull.SetActive(false);
				chestBottomBack.SetActive(true);
				chestBottomFront.SetActive(true);
				chestTopOpenHalf.SetActive(false);
				chestTopOpenFull.SetActive(true);

				curTimeToDissapear = timeToDissapear;
				chestBag1.SetActive(false);
				chestBag2.SetActive(false);
				chestBag3.SetActive(true);
    			break;
    	};

   	}
}
