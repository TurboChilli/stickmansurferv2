﻿using UnityEngine;
using System.Collections;

public class TreasureVaultUIController : MonoBehaviour {

    public Camera treasureVaultUICamera;
    public Camera mainCamera;
    public MainMenuController mainMenuController;
    public TreasureVaultUI treasureVaultUI;
    public TreasureVault treasureVault;
    public TreasureVault moneyBag;
    public TreasureVaultUITimer treasureVaultUITimer;

    private bool treasureChestOpen = false;

	private bool inputWasDown = false;
	private float inputCooldown = 0.5f;
	private float curInputCooldown = 0.0f;

    private float curTimeToMoveAssets = 0.0f;
    private float timeToMoveAssets = 0.37f;
    private float timeToOpenChest = 0.5f;
    private bool movingAssets = false;
    private bool revealedAssets = false;

    private Vector3 inactiveVaultPosition;
    private Vector3 lockedVaultPosition;
	private Vector3 unlockedVaultPosition;
	private Vector3 finalVaultPosition;

	private Vector3 origVaultScale;
	private Vector3 addedVaultScale;

	private Quaternion origVaultRotation;

	private Vector3 inactiveUIPosition;
	private Vector3 lockedUIPosition;
	private Vector3 unlockedUIPosition;
	private Vector3 animatedUIPosition;
	private Vector3 finalUIPosition;

	private Quaternion origUIRotation;
	private Quaternion animUIRotation;
	private Vector3 origUIScale;

    public enum VaultUIState
    {
        OpenNow,
        Opening,
        Open,
        RevealAsset,
        GoldCoins,
        EnergyDrinks,
        Llama,
        Reset,
        None
    }

    private VaultUIState currVaultUIState = VaultUIState.None;
    private VaultUIState nextVaultUIState = VaultUIState.None;

    private int assetsOpened = 0;

    public VaultUIState VaultUIStateProperty
    {
        get
        {
            return currVaultUIState;
        }
        set
        {
            currVaultUIState = value;
        }
    }  

    public int[] Rewards
    {
    	get 
    	{
			if (_rewards == null)
			{
				_rewards = new int[9];
				_rewards[0] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward50;
				_rewards[1] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward100;
				_rewards[2] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward200;
				_rewards[3] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward400;
				_rewards[4] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward1000;
				_rewards[5] = GameServerSettings.SharedInstance.SurferSettings.TreasureReward2000;
				_rewards[6] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardE1;
				_rewards[7] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardE2;
				_rewards[8] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardE3;
			}
		
			return _rewards;
    	}

    }

    public int[] RewardWeights
    {
		get {
			if (_rewardWeights == null)
			{
				_rewardWeights = new int[9];

				_rewardWeights[0] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward50;
				_rewardWeights[1] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward100  + _rewardWeights[0];
				_rewardWeights[2] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward200  + _rewardWeights[1];
				_rewardWeights[3] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward400  + _rewardWeights[2];
				_rewardWeights[4] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward1000 + _rewardWeights[3];
				_rewardWeights[5] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightReward2000 + _rewardWeights[4];
				_rewardWeights[6] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightRewardE1   + _rewardWeights[5];
				_rewardWeights[7] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightRewardE2   + _rewardWeights[6];
				_rewardWeights[8] = GameServerSettings.SharedInstance.SurferSettings.TreasureWeightRewardE3   + _rewardWeights[7];
				return _rewardWeights;
			}
			return _rewardWeights;
		}
    }

    public float[] RewardPercentages
    {
    	get 
    	{
			if (_rewardPercentages == null)
			{
				_rewardPercentages = new float[9];

				_rewardPercentages[0] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage50;	
				_rewardPercentages[1] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage100;	
				_rewardPercentages[2] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage200;	
				_rewardPercentages[3] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage400;	
				_rewardPercentages[4] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage1000;	
				_rewardPercentages[5] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentage2000;	
				_rewardPercentages[6] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentageE1;	
				_rewardPercentages[7] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentageE2;	
				_rewardPercentages[8] = GameServerSettings.SharedInstance.SurferSettings.TreasureRewardPrecentageE3;	
			}
			return _rewardPercentages;
    	}
    }

    public int[] FirstTimeRewards
    {
    	get 
    	{
			if (_firstTimeRewards == null)
			{
				StickSurferSetting surferSettings = GameServerSettings.SharedInstance.SurferSettings;
				_firstTimeRewards = new int[] {  surferSettings.FirstTreasureRewardsRandom1, surferSettings.FirstTreasureRewardsRandom2, surferSettings.FirstTreasureRewardsRandom3 };
			}

			return _firstTimeRewards;
    	}
    }


	private int[] _rewards = null;
    private int[] _firstTimeRewards = null;
	private int[] _rewardWeights = null;
	private float[] _rewardPercentages = null;

    private int nextReward = 0;

    private int coinsEarned = 0;
    private int energyDrinksEarned = 0;

    void Start () 
    {
        treasureVaultUI.coinbar.gameObject.SetActive(false);
        treasureVaultUI.energyDrinkBar.gameObject.SetActive(false);
        treasureVaultUI.currencyBarPlaceholder.gameObject.SetActive(false);

		treasureVaultUI.coinStackSmall.SetActive(false);
		treasureVaultUI.coinStackMedium.SetActive(false);
        treasureVaultUI.coinStackLarge.SetActive(false);

		treasureVaultUI.energyDrinkSmall.SetActive(false);
		treasureVaultUI.energyDrinkMedium.SetActive(false);
        treasureVaultUI.energyDrinkLarge.SetActive(false);

        treasureVaultUI.SetMainPanelText(" ");

		treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.Locked);

		//Vault Positions
		lockedVaultPosition = treasureVault.transform.position + Vector3.back * 40.0f;
		unlockedVaultPosition = new Vector3(-200, lockedVaultPosition.y - 40.0f, 
											treasureVault.transform.position.z);
		inactiveVaultPosition = lockedVaultPosition + Vector3.up * 500.0f;

		finalVaultPosition = unlockedVaultPosition + Vector3.left * 600.0f;

		origVaultScale = treasureVault.transform.localScale;
		addedVaultScale = origVaultScale + Vector3.one * 50;
		addedVaultScale.z = origVaultScale.z;
		origVaultRotation = treasureVault.transform.rotation;

		//UI Position
		lockedUIPosition = new Vector3(0, treasureVaultUI.transform.position.y, 
                                        treasureVaultUI.transform.position.z);
		unlockedUIPosition =  new Vector3(150, lockedUIPosition.y, 
										treasureVaultUI.transform.position.z - 1.0f);

		animatedUIPosition = unlockedVaultPosition;
		inactiveUIPosition = lockedUIPosition + Vector3.up * 500.0f;
		finalUIPosition = unlockedUIPosition + Vector3.right * 600.0f;

		treasureVaultUI.transform.position = inactiveUIPosition;

		origUIScale = treasureVaultUI.transform.localScale;

		origUIRotation = treasureVaultUI.transform.rotation;
		animUIRotation = origUIRotation * Quaternion.AngleAxis( 270.0f, Vector3.forward);

		//Back Button
		//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 80, 60, 
        //                                       treasureVaultUI.backArrow.transform, treasureVaultUICamera); 

    }

	bool ClosedClicked()
	{
		return PlayerPrefs.HasKey("TVUICcc");
	}

	void FlagClosedClicked()
	{
		if(!ClosedClicked())
		{
			PlayerPrefs.SetInt("TVUICcc", 1);
			NotificationManager.RegisterForNotifications();
		}
	}

    public void Initialise()
    {
		movingAssets = true;
		curTimeToMoveAssets = timeToMoveAssets;
		treasureVaultUI.transform.position = inactiveUIPosition;
		treasureVault.transform.position = inactiveVaultPosition;
		currVaultUIState = VaultUIState.OpenNow;
		inputWasDown = true;
		treasureVaultUI.shinyBackground.SetActive(false);

		treasureVaultUI.backArrowhand.SetActive(false);

		if (treasureVaultUITimer.TimeStatus)
		{
			treasureVaultUI.textPanelJiggle.enabled = true;
		}
		else
		{
			treasureVaultUI.textPanelJiggle.enabled = false;

			if (!ClosedClicked())
				treasureVaultUI.backArrowhand.SetActive(true);
		}

		curInputCooldown = inputCooldown;
    }

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.MainMenuTreasureChestShowing)
			{
				CloseTreasureChestButtonClick();
				DialogTrackingFlags.MainMenuIgnoreAndroidBackCheck = true;

			}
		}
		#endif 
	}

	void CloseTreasureChestButtonClick()
	{
		FlagClosedClicked();

		GlobalSettings.PlaySound(treasureVaultUI.swooshSound);
		if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChestClose)
		{
			GameState.SharedInstance.TutorialStage = TutStageType.MainMenuShop;
			mainMenuController.SetupTutorial();
		}
		NavigateToMainMenu();
	}

    // Update is called once per frame
    void Update () 
    {
		CheckAndroidBackButton();


    	//transition effects
		if (movingAssets && currVaultUIState != VaultUIState.None)
		{
			float lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / timeToMoveAssets);

			curTimeToMoveAssets -= Time.deltaTime;
			if (movingAssets && curTimeToMoveAssets <= 0)
			{
				lerpAmount = 0;
				movingAssets = false;
			}

			switch (currVaultUIState)
			{
				case VaultUIState.OpenNow:

					if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChest)
					{
						if (treasureVaultUI.backButton.activeSelf)
							treasureVaultUI.backButton.SetActive(false);
					}
					else 
					{
						if (treasureVaultUITimer.TimeStatus)
						{
							if (!treasureVaultUI.backButton.activeSelf)					
								treasureVaultUI.backButton.SetActive(true);
						}
					}


					treasureVaultUI.transform.position = Vector3.Lerp(inactiveUIPosition, lockedUIPosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					treasureVault.transform.position = Vector3.Lerp(inactiveVaultPosition, lockedVaultPosition, 1.0f - Mathf.Pow(lerpAmount, 2.0f)); //replace with bouncing function f'later

				break;
				case VaultUIState.Opening:

					lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / (timeToOpenChest));

					Quaternion newRotation = Quaternion.AngleAxis((Mathf.Sin(Time.time * 80.0f) * 4.0f), Vector3.forward ) * origVaultRotation;

					treasureVault.transform.rotation = Quaternion.Slerp(origVaultRotation, newRotation, Mathf.Pow(lerpAmount, 2.0f));
					treasureVault.transform.position = lockedVaultPosition + Random.insideUnitSphere * 4.0f * Mathf.Pow(1.0f - lerpAmount, 2.0f);

					if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChest)
						GameState.SharedInstance.TutorialStage = TutStageType.MainMenuChestClose;

					treasureVaultUI.transform.position = Vector3.Lerp(lockedUIPosition, inactiveUIPosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					treasureVault.transform.position = Vector3.Lerp(lockedVaultPosition, unlockedVaultPosition, 1.0f - Mathf.Pow(lerpAmount, 2.0f));
					treasureVault.transform.localScale = Vector3.Lerp(origVaultScale, addedVaultScale, 1.0f - Mathf.Pow(lerpAmount, 2.0f));

					if (treasureVaultUI.backButton.activeSelf)
						treasureVaultUI.backButton.SetActive(false);		

					if (!movingAssets)
					{
						bool isGoldReward = SetupNextReward();

						//GlobalSettings.PlaySound(treasureVaultUI.swooshSound);
						GlobalSettings.PlaySound(treasureVaultUI.chestOpenSound);
						GlobalSettings.PlaySound(treasureVaultUI.fanfareSound);

						if (isGoldReward)
							GlobalSettings.PlaySound(treasureVaultUI.kaChingSound);
						else 
							GlobalSettings.PlaySound(treasureVaultUI.energyDrinkSound);

						treasureVaultUI.coinExplosionParticles.Stop();
						treasureVaultUI.coinExplosionParticles.gameObject.SetActive(true);
						treasureVaultUI.coinExplosionParticles.Play();
						

						movingAssets = true;
						curTimeToMoveAssets = timeToMoveAssets;

						treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.TwoBags);
					}
				break;

				case VaultUIState.Open:
					if (!movingAssets)
					{

					}
		
				break;

				case VaultUIState.RevealAsset:
					treasureVaultUI.transform.position = Vector3.Lerp(animatedUIPosition + Vector3.back * 400.0f, unlockedUIPosition + Vector3.back * 400.0f, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					treasureVaultUI.transform.localScale = Vector3.Lerp(Vector3.zero, origUIScale * 0.9f, 1.0f - Mathf.Pow(lerpAmount, 3.0f) );
					treasureVaultUI.transform.rotation = Quaternion.Slerp(animUIRotation, origUIRotation, 1.0f - Mathf.Pow(lerpAmount, 3.0f));

					Quaternion jitterRotation = Quaternion.AngleAxis((Mathf.Sin(Time.time * 60.0f) * 4.0f), Vector3.forward );
					treasureVault.transform.rotation = Quaternion.Slerp(origVaultRotation, jitterRotation, Mathf.Pow(lerpAmount, 2.0f));

					if (!movingAssets)
					{
						if (treasureVault.VaultStateProperty == TreasureVault.VaultState.Opened)
							treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.TwoBags);

						currVaultUIState = nextVaultUIState;
						movingAssets = true;
						curTimeToMoveAssets = 0;
					}

				break;

				case VaultUIState.GoldCoins:
		
				

				break;
				case VaultUIState.EnergyDrinks:
					
				break;
				case VaultUIState.Reset:
					treasureVaultUI.transform.position = Vector3.Lerp(unlockedUIPosition, finalUIPosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					treasureVault.transform.position = Vector3.Lerp(unlockedVaultPosition, finalVaultPosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					treasureVault.transform.localScale = Vector3.Lerp(addedVaultScale, origVaultScale, 1.0f - Mathf.Pow(lerpAmount, 2.0f));

					AnalyticsAndCrossPromoManager.SharedInstance.LogTreasureOpened(coinsEarned, energyDrinksEarned);

					coinsEarned = 0;
					energyDrinksEarned = 0;

					if (!movingAssets)
					{
						curTimeToMoveAssets = timeToMoveAssets;
						movingAssets = true;
						currVaultUIState = VaultUIState.OpenNow;	

						treasureVaultUI.currencyBarPlaceholder.gameObject.SetActive(false);

		                treasureVaultUI.SetMainPanelText(" ");

						treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.Locked);

		                treasureVaultUI.textPanel.SetActive(true);

						treasureVaultUI.textPanelJiggle.enabled = false;
						treasureVaultUI.backButton.SetActive(true);

						if(!ClosedClicked())
						{
							treasureVaultUI.backArrowhand.SetActive(true);
						}

						treasureVaultUITimer.ShowText();

		                //treasureVaultUI.coinbar.DisplayCoins(0, false);
		                //treasureVaultUI.energyDrinkBar.DisplayValue(0, false);
		                //treasureVaultUI.currencyBarPlaceholder.DisplayValue(0, false);
		                //treasureVaultUI.shinyBackground.SetActive(false);
					}
				break;
			};
		}


		//idle effects
		if (Input.GetMouseButtonDown(0) && currVaultUIState != VaultUIState.None && !inputWasDown && curInputCooldown <= 0)
        {  
            if (UIHelpers.CheckButtonHit(treasureVaultUI.backButton.transform, treasureVaultUICamera))
            {
				CloseTreasureChestButtonClick();
				ServerSavedGameStateSync.Sync();

				FlagClosedClicked();

                GlobalSettings.PlaySound(treasureVaultUI.swooshSound);
				if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChestClose)
                {
                	GameState.SharedInstance.TutorialStage = TutStageType.MainMenuShop;
					mainMenuController.SetupTutorial();
                }
                NavigateToMainMenu();
            }
            else
            {
	            if (currVaultUIState == VaultUIState.OpenNow && treasureVaultUITimer.TimeStatus)
	            {
					
					currVaultUIState = VaultUIState.Opening;

					treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.Locked);

					treasureVaultUITimer.InitializeTimer(  TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Treasure) );
					treasureVaultUITimer.HideText();

					GlobalSettings.PlaySound(treasureVaultUI.chestCreak);
					GlobalSettings.PlaySound(treasureVaultUI.drumroll);
	                //GlobalSettings.PlaySound(treasureVaultUI.swooshSound);

	                treasureVaultUI.textPanel.SetActive(false);
	                treasureVaultUI.SetTimeCountPanelText("");
	                treasureVaultUI.SetTimeCountOpenText("");
	                
					curTimeToMoveAssets = timeToOpenChest;
					movingAssets = true;
					curInputCooldown = inputCooldown;
	            }
	            else if (treasureChestOpen && currVaultUIState == VaultUIState.GoldCoins)
	            {
					currVaultUIState = VaultUIState.RevealAsset;

					if (assetsOpened < 3)
					{
						bool isGoldReward = SetupNextReward();

						if (isGoldReward)
							GlobalSettings.PlaySound(treasureVaultUI.kaChingSound);
						else 
							GlobalSettings.PlaySound(treasureVaultUI.energyDrinkSound);

						if (assetsOpened == 2)
							treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.OneBag);
						else
							treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.Empty);
					}
					else
					{
						currVaultUIState = VaultUIState.Reset;
						nextVaultUIState = VaultUIState.Reset;
						ResetTreasureAssets();
						treasureVaultUI.transform.position = lockedUIPosition;
						treasureVaultUI.transform.localScale = origUIScale;
						treasureVaultUI.transform.localRotation = origUIRotation;
						assetsOpened = 0;
					}	

					//treasureVaultUI.coinbar.gameObject.SetActive(false);


	                GlobalSettings.PlaySound(treasureVaultUI.swooshSound);
					curTimeToMoveAssets = timeToMoveAssets;
					movingAssets = true;
					curInputCooldown = inputCooldown;

	            }
	            else if(treasureChestOpen && currVaultUIState == VaultUIState.EnergyDrinks)
	            {    
					currVaultUIState = VaultUIState.RevealAsset;
					if (assetsOpened < 3)
					{
						bool isGoldReward = SetupNextReward();

						if (isGoldReward)
							GlobalSettings.PlaySound(treasureVaultUI.kaChingSound);
						else 
							GlobalSettings.PlaySound(treasureVaultUI.energyDrinkSound);

						if (assetsOpened == 2)
							treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.OneBag);
						else
							treasureVault.DrawTreasureVaultComponents(TreasureVault.VaultState.Empty);
					}
					else
					{
						currVaultUIState = VaultUIState.Reset;
						nextVaultUIState = VaultUIState.Reset;
						ResetTreasureAssets();

						treasureVaultUI.transform.position = lockedUIPosition;
						treasureVaultUI.transform.localScale = origUIScale;
						treasureVaultUI.transform.localRotation = origUIRotation;
						assetsOpened = 0;
					}
	                
					GlobalSettings.PlaySound(treasureVaultUI.swooshSound);
					curTimeToMoveAssets = timeToMoveAssets;
					movingAssets = true;
					curInputCooldown = inputCooldown;
	            }
	        }
			inputWasDown = true;
        }
		else if (!Input.GetMouseButtonDown(0))
        {
			inputWasDown = false;
        }

		if (curInputCooldown > 0)
			curInputCooldown -= Time.deltaTime;
    }

	private void ResetTreasureAssets()
    {
		treasureVaultUI.SetMainPanelText("");
        treasureVaultUI.energyDrinkBar.gameObject.SetActive(false);
		treasureVaultUI.energyDrinkSmall.SetActive(false);
		treasureVaultUI.energyDrinkMedium.SetActive(false);
        treasureVaultUI.energyDrinkLarge.SetActive(false);

		treasureVaultUI.coinStackSmall.SetActive(false);
		treasureVaultUI.coinStackMedium.SetActive(false);
        treasureVaultUI.coinStackLarge.SetActive(false);

		treasureVaultUI.transform.position = animatedUIPosition;
		treasureVaultUI.transform.localScale = Vector3.zero;
		treasureVaultUI.transform.rotation = animUIRotation;
    }

    private void ShowGoldCoins()
    {
		if (!revealedAssets)
		{
			GlobalSettings.PlaySound(treasureVaultUI.kaChingSound);

            //treasureVaultUI.coinbar.gameObject.SetActive(true);

			//treasureVaultUI.coinbar.DisplayCoins(GameState.SharedInstance.Cash);
			//treasureVaultUI.coinbar.DisplayCoins(GameState.SharedInstance.Cash + nextReward, true);
			GameState.SharedInstance.AddCash(nextReward);
			mainMenuController.coinBarTotal.DisplayCoins(GameState.SharedInstance.Cash);
			

			if (nextReward <= 100)
			{
				treasureVaultUI.coinStackSmall.SetActive(true);
				treasureVaultUI.coinStackMedium.SetActive(false);
				treasureVaultUI.coinStackLarge.SetActive(false);
			}
			else if (nextReward <= 400)
			{
				treasureVaultUI.coinStackSmall.SetActive(false);
				treasureVaultUI.coinStackMedium.SetActive(true);
				treasureVaultUI.coinStackLarge.SetActive(false);
			}
			else
			{
				treasureVaultUI.coinStackSmall.SetActive(false);
				treasureVaultUI.coinStackMedium.SetActive(false);
				treasureVaultUI.coinStackLarge.SetActive(true);
			}

			treasureVaultUI.SetMainPanelText(string.Format("+{0}", nextReward));

            treasureChestOpen = true;
			revealedAssets = true;
        }
    }

    private void ShowEnergyDrinks()
    {
		if (!revealedAssets)
		{ 
            GlobalSettings.PlaySound(treasureVaultUI.kaChingSound);

            //treasureVaultUI.energyDrinkBar.gameObject.SetActive(true);
			//treasureVaultUI.energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
			//treasureVaultUI.energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink + (-nextReward), true);
			GameState.SharedInstance.AddEnergyDrink(-nextReward);

			mainMenuController.energyBar.DisplayValue(GameState.SharedInstance.EnergyDrink);

			if (nextReward == -1)
			{
				treasureVaultUI.energyDrinkSmall.SetActive(true);
				treasureVaultUI.energyDrinkMedium.SetActive(false);
				treasureVaultUI.energyDrinkLarge.SetActive(false);
			}
			else if (nextReward == -2)
			{
				treasureVaultUI.energyDrinkSmall.SetActive(false);
				treasureVaultUI.energyDrinkMedium.SetActive(true);
				treasureVaultUI.energyDrinkLarge.SetActive(false);
			}
			else
			{
				treasureVaultUI.energyDrinkSmall.SetActive(false);
				treasureVaultUI.energyDrinkMedium.SetActive(false);
				treasureVaultUI.energyDrinkLarge.SetActive(true);
			}

			treasureVaultUI.SetMainPanelText(string.Format("+{0}", -nextReward));

			treasureChestOpen = true;
			revealedAssets = true;
        }
    }

    public int GetWeightedReward()
    {
		int[] rewardWeights = RewardWeights;
		int randValue = Random.Range(0, rewardWeights[rewardWeights.Length - 1] + 1);

		for (int i = 0; i < Rewards.Length; i++)
		{
			if (randValue <= rewardWeights[i])
			{
				if (Rewards[i] > 0)
				{
					int percentageReward = GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem( RewardPercentages[i] );
					if (Rewards[i] > percentageReward)
						return Rewards[i];
					else 
						return percentageReward;
				}
				else
				{
					return Rewards[i];
				}	
			}
		}
		return Rewards[Rewards.Length - 1];
    }

    public bool SetupNextReward()
    {
		ResetTreasureAssets();

		currVaultUIState = VaultUIState.RevealAsset;

		int firstTreasureIndex = GameState.SharedInstance.FirstTreasureIndex;
		if (firstTreasureIndex <= 2)
		{
			nextReward = FirstTimeRewards[firstTreasureIndex];
			GameState.SharedInstance.FirstTreasureIndex ++;
		}
		else 
			nextReward = GetWeightedReward();

		assetsOpened++;
			
		if (nextReward < 0)
		{
			nextVaultUIState = VaultUIState.EnergyDrinks;
			energyDrinksEarned -= nextReward;
			revealedAssets = false;
			ShowEnergyDrinks();
			return false;
		}
		else
		{
			nextVaultUIState = VaultUIState.GoldCoins;
			coinsEarned += nextReward;
			revealedAssets = false;
			ShowGoldCoins();
			return true;
		}
    }

    public void NavigateToMainMenu()
    {
		DialogTrackingFlags.MainMenuTreasureChestShowing = false;
		GlobalSettings.CurrentMainMenuLocation = GlobalSettings.MainMenuLocation.MainMenu;
		mainCamera.enabled = false;
		treasureVaultUICamera.enabled = false;
		currVaultUIState = VaultUIState.None;
		mainCamera.enabled = true;
    }
}
