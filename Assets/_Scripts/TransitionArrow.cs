﻿using UnityEngine;
using System.Collections;

public class TransitionArrow : MonoBehaviour {

	private Vector3 origPosition = Vector3.one;
	private float timer = 0.0f;

	public Vector3 addedPosition = Vector3.right;

	public float breakTime = 0.5f;
	public float moveTime = 0.5f;
	private bool moving = false;
    private bool blinking = true;

    float blinkTimer = 0;
    float blinkDelay = 0.4f;

    bool blinkOn = true;
    Renderer currRenderer;

    void Start ()
    {
		if((GameState.SharedInstance.GetLocationVisits(Location.Cave) > 0) ||
        	(GameState.SharedInstance.GetLocationVisits(Location.WavePark) > 0))
        {
            this.gameObject.SetActive(false);
        }

        currRenderer = GetComponent<Renderer>();
    }

	public void ResetPosition()
	{
		origPosition = transform.position;
	}

	void OnEnable () 
	{
		origPosition = transform.localPosition;
		timer = Random.Range(0.0f, breakTime);
		moving = false;

		if((GameState.SharedInstance.GetLocationVisits(Location.Cave) > 0) ||
        	(GameState.SharedInstance.GetLocationVisits(Location.WavePark) > 0))
        {
            this.gameObject.SetActive(false);
        }

	}
	
	void Update () 
	{
       // //////Debug.Log("FUUUUUUUCJK");
		if (moving)
		{
			float positionWeight = Mathf.Sin( (timer / moveTime) * Mathf.PI);
			transform.localPosition = origPosition + addedPosition * positionWeight;
		}

        if (false)
        {
            blinkTimer += Time.deltaTime;
            if (blinkTimer > blinkDelay)
            {
                blinkTimer = 0;

                if (blinkOn)
                {
                    blinkOn = false;
                    currRenderer.enabled = false;
					#if UNITY_EDITOR
					//////Debug.Log("BLIKING HERE OFF");
					#endif
                }
                else
                {
                    blinkOn = true;
                    currRenderer.enabled = true;
					#if UNITY_EDITOR
					//////Debug.Log("BLIKING HERE ON");
					#endif
                }
            }
        }
		timer += Time.deltaTime;

		if (timer > breakTime && !moving)
		{
			moving = true;
			timer = 0.0f;
		}
		else if (timer > moveTime & moving)
		{
			moving = false;
			timer = 0.0f;
		}
	}
}
