﻿using UnityEngine;
using System.Collections;

public class PopUpDialogPanel : MonoBehaviour {

    public TextMesh titleBarText;
    public TextMesh mainPanelText;
    public GameObject closeButton;
    public GameObject mainButton;

    public delegate void PopupDialogOKDelegate(string okDetail);
    public event PopupDialogOKDelegate OnPopupDialogOK;

    public delegate void PopupDialogCloseDelegate();
    public event PopupDialogCloseDelegate OnPopupDialogClose;

    PopUpPanelState popupState;
    enum PopUpPanelState
    {
        IsActive,
        IsNotActive
    }



    void Start()
    {
        popupState = PopUpPanelState.IsNotActive;
		this.gameObject.SetActive(false);
    }



    void Update()
    {
        if(popupState == PopUpPanelState.IsActive && Input.GetMouseButton(0))
        {
			if(UIHelpers.CheckButtonHit(closeButton.transform))
            {
                NotifyCloseButtonHitEvent();
                CloseDialog();
            }
			else if(UIHelpers.CheckButtonHit(mainButton.transform))
            {
                NotifyOkButtonHitEvent();
                CloseDialog();
            }
        }
    }



	public void Show(Camera camera, string titleText, string mainText)
    {
        titleBarText.text = titleText;
        mainPanelText.text = mainText;
        popupState = PopUpPanelState.IsActive;

		this.gameObject.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -15f);
		this.gameObject.SetActive(true);
    }



    string okDetail = "";
	public void Show(Camera camera, string titleText, string mainText, string okDetailParam)
    {
        titleBarText.text = titleText;
        mainPanelText.text = mainText;
        popupState = PopUpPanelState.IsActive;
        okDetail = okDetailParam;

		this.gameObject.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -15f);
		this.gameObject.SetActive(true);
    }



    void CloseDialog()
    {
        titleBarText.text = "";
        mainPanelText.text = "";
        popupState = PopUpPanelState.IsNotActive;

		this.gameObject.SetActive(false);
		this.gameObject.transform.position = new Vector3(-9999f, this.gameObject.transform.position.y, -15f);
    }



    void NotifyOkButtonHitEvent()
    {
        if(OnPopupDialogOK != null)
        {
            OnPopupDialogOK(okDetail);
        }
    }



    void NotifyCloseButtonHitEvent()
    {
        if(OnPopupDialogClose != null)
        {
            OnPopupDialogClose();
        }
    }
}
