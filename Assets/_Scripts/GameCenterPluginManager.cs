﻿using System.Text;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using Prime31;
using PaperPlaneTools;

public class GameCenterPluginManager : MonoBehaviour {


	#if UNITY_IOS

	public const string leaderboardHighestSurfScoreID 		= "sticksurfer.leaderboard.highestSurfScore";
	public const string leaderboardHighestJeepScoreID 		= "sticksurfer.leaderboard.highestJeepDistance";
	public const string leaderboardHighestCredScoreID 		= "sticksurfer.leaderboard.highestCredLevel";
	public const string leaderboardHighestSnowboardScoreID 	= "sticksurfer.leaderboard.highestSnowboardScore";
	public const string leaderboardHighestCarGameScoreID 	= "sticksurfer.leaderboard.highestCarGameScore";

	public const string achievPestController = 		"stickmansurfer.pestcontroller";
	public const string achievFrequentFlyer = 		"stickmansurfer.frequentflyer";
	public const string achievHomebody = 			"stickmansurfer.homebody";
	public const string achievFourWheelEnthusiast = "stickmansurfer.fourwheelenthusiast";
	public const string achievLongDistanceSurfer =  "stickmansurfer.longdistancesurfer";

	public const string achievGrommit =				"stickmansurfer.grommit";
	public const string achievAmatuer =				"stickmansurfer.amatuer";
	public const string achievProSurfer =			"stickmansurfer.prosurfer";

	public const string achievSkatePro = 			"stickmansurfer.skatepro";
	public const string achievIdleHands = 			"stickmansurfer.idlehands";

	#elif UNITY_ANDROID

	public const string leaderboardHighestSurfScoreID = 		"CgkIxajPxLgGEAIQAA";
	public const string leaderboardHighestJeepScoreID = 		"CgkIxajPxLgGEAIQAQ";
	public const string leaderboardHighestCredScoreID = 		"CgkIxajPxLgGEAIQAg";
	public const string leaderboardHighestSnowboardScoreID = 	"CgkIxajPxLgGEAIQDw";
	public const string leaderboardHighestCarGameScoreID 	=   "CgkIxajPxLgGEAIQEA";

	private List<string> achievementsUnlocked = null;

	public const int androidPlayServicesAutoSignInMemoryMin = 512;
	public static bool androidCheckDeviceMemory = false;
	public static bool androidAutoSignInToPlayServices = true;


	public const string achievPestController = 		"CgkIxajPxLgGEAIQCw";
	public const string achievFrequentFlyer = 		"CgkIxajPxLgGEAIQCg";
	public const string achievHomebody = 			"CgkIxajPxLgGEAIQCQ";
	public const string achievFourWheelEnthusiast = "CgkIxajPxLgGEAIQCA";
	public const string achievLongDistanceSurfer =  "CgkIxajPxLgGEAIQBg";

	public const string achievGrommit =				"CgkIxajPxLgGEAIQAw";
	public const string achievAmatuer =				"CgkIxajPxLgGEAIQBA";
	public const string achievProSurfer =			"CgkIxajPxLgGEAIQBQ";

	public const string achievSkatePro = 			"CgkIxajPxLgGEAIQBQ";
	public const string achievIdleHands = 			"stickmansurfer.idlehands";


	#endif 

	private bool showAchievementBanners = false;

	void ActivateAchievementOnCountReaching(string achievementID, int onCountReaching)
	{
		if(!PlayerPrefs.HasKey(achievementID))
		{
			string countKey = string.Format("{0}.count", achievementID);

			PlayerPrefs.SetInt(countKey, PlayerPrefs.GetInt(countKey, 0) + 1);

			if(PlayerPrefs.GetInt(countKey, 0) >= onCountReaching && IsAuthenticated())
			{
				UnlockAchievement(achievementID);
				PlayerPrefs.SetInt(achievementID, 1);
			}
		}
	}

	public void TrackAndSubmitAchievementPestController()
	{
		ActivateAchievementOnCountReaching(achievPestController, 1);
	}

	public void TrackAndSubmitAchievementFrequentFlyer()
	{
		ActivateAchievementOnCountReaching(achievFrequentFlyer, 100);
	}

	public void TrackAndSubmitAchievementHomeBody()
	{
		ActivateAchievementOnCountReaching(achievHomebody, 200);
	}

	public void TrackAndSubmitAchievementFourWheelEnthusiast()
	{
		ActivateAchievementOnCountReaching(achievFourWheelEnthusiast, 50);
	}

	public void TrackAndSubmitAchievementLongDistanceSurfer(float distance)
	{
		string formatForceUnlockNextTime = string.Format("{0}.forceAchievement", achievLongDistanceSurfer);

		if(distance >= 2000f)
		{
			if(!UnlockAchievement(achievLongDistanceSurfer))
			{
				PlayerPrefs.SetInt(formatForceUnlockNextTime, 1);
			}
		}

		if(PlayerPrefs.GetInt(formatForceUnlockNextTime, 0) == 1)
		{
			if(UnlockAchievement(achievLongDistanceSurfer))
				PlayerPrefs.SetInt(formatForceUnlockNextTime, 0);
		}
	}

	public void TrackAndSubmitAchievementPlayedGames(int gamesPlayed)
	{
		if (gamesPlayed >= 200)
			ActivateAchievementOnCountReaching(achievProSurfer, 1);
		
		else if (gamesPlayed >= 75)
			ActivateAchievementOnCountReaching(achievAmatuer, 1);
		
		else if (gamesPlayed >= 25)
			ActivateAchievementOnCountReaching(achievGrommit, 1);
				
	}

	public void TrackAndSubmitAchievementSkatePro()
	{
		ActivateAchievementOnCountReaching(achievSkatePro, 500);
	}

	public void TrackAndSubmitAchivementFidgetSpinner()
	{
		ActivateAchievementOnCountReaching(achievIdleHands, 1);
	}

	//public void Track

	void Start()
	{
		#if UNITY_IOS //|| UNITY_EDITOR
		//Link all the callbacks
		Prime31.GameCenterManager.playerAuthenticatedEvent += playerAuthenticatedEvent;

		#endif

		#if UNITY_IOS || UNITY_EDITOR

		if (!GlobalSettings.firstTimePlayVideo)
		{
			AuthenticatePlayer();
		}
			
		#endif 
	}

	public void CheckAndroidGameServicesLogin()
	{
		#if UNITY_ANDROID
		if(androidAutoSignInToPlayServices)
		{
			if(!androidCheckDeviceMemory)
			{
				androidCheckDeviceMemory = true;
				int totalMemory	= SystemInfo.systemMemorySize;

				if(totalMemory < androidPlayServicesAutoSignInMemoryMin)
					androidAutoSignInToPlayServices = false;		
			}

			if(androidAutoSignInToPlayServices && !GlobalSettings.firstTimePlayVideo)
			{
				androidAutoSignInToPlayServices = false;
				AuthenticatePlayer();
			}
		}
		#endif
	}

	void OnDestroy()
	{
		#if UNITY_IOS //|| UNITY_EDITOR
		//Link all the callbacks
		Prime31.GameCenterManager.playerAuthenticatedEvent -= playerAuthenticatedEvent;
		#endif 
	}


	public void ShowLeaderboard(LeaderboardType leaderboardType)
	{
		//////Debug.Log("Attempting to show leaderboard");
	
		if(IsAuthenticated())
		{	
			#if UNITY_IOS //|| UNITY_EDITOR
				if (leaderboardType == LeaderboardType.SurfLeaderBoard)
					Prime31.GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, leaderboardHighestSurfScoreID);
				else if (leaderboardType == LeaderboardType.JeepLeaderBoard)
					Prime31.GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, leaderboardHighestJeepScoreID);
				else if (leaderboardType == LeaderboardType.CredLeaderBoard)
					Prime31.GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, leaderboardHighestCredScoreID);
				else if (leaderboardType == LeaderboardType.SnowSurferBoard)
					Prime31.GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, leaderboardHighestSnowboardScoreID);
				else if (leaderboardType == LeaderboardType.CarGameBoard)
					Prime31.GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(GameCenterLeaderboardTimeScope.AllTime, leaderboardHighestCarGameScoreID);
			#elif UNITY_ANDROID
				if (leaderboardType == LeaderboardType.SurfLeaderBoard)
					PlayGameServices.showLeaderboard(leaderboardHighestSurfScoreID);
				else if (leaderboardType == LeaderboardType.JeepLeaderBoard)
					PlayGameServices.showLeaderboard(leaderboardHighestJeepScoreID);
				else if (leaderboardType == LeaderboardType.CredLeaderBoard)
					PlayGameServices.showLeaderboard(leaderboardHighestCredScoreID);
				else if (leaderboardType == LeaderboardType.SnowSurferBoard)
					PlayGameServices.showLeaderboard(leaderboardHighestSnowboardScoreID);
				else if (leaderboardType == LeaderboardType.CarGameBoard)
					PlayGameServices.showLeaderboard(leaderboardHighestCarGameScoreID);
				
			#endif
		}
		else
		{
			AuthenticatePlayer();
		}
	}

	public void ShowAchievements()
	{
		//////Debug.Log("Attempt to show achievments");
	
		if(IsAuthenticated())
		{	
			#if UNITY_IOS //|| UNITY_EDITOR
			Prime31.GameCenterBinding.showAchievements();
			#elif UNITY_ANDROID
			PlayGameServices.showAchievements();
			#endif
		}
		else
		{
			AuthenticatePlayer();
		}
	}


	/*
	{
		var rateUrl = "";
#if UNITY_IOS
		var fver = 0f;
		float.TryParse(UnityEngine.iOS.Device.systemVersion.Split('.')[0], out fver);

		if (fver > 0 && fver < 8f)
			rateUrl = Instance.IOSRateURL7;
		else
			rateUrl = Instance.IOSRateURL;

		//rateUrl = rateUrl.Replace("1205391576", "769574372");
#elif UNITY_ANDROID
		rateUrl = Instance.AndtoidRateUrl;
#endif

		Debug.Log ("****** REDIRECTING TO RATE URL: " + rateUrl);
		Application.OpenURL(rateUrl);

		if (Instance) Instance.WasRateClicked = true;
	}
	*/

	private string IOSRateURL = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1151245201&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
	private string IOSRateURL7 = "itms-apps://itunes.apple.com/app/id1151245201";
	private string AndroidURL = "market://details?id=com.turbochilli.sticksurfer";

	public void RateGame()   
	{

#if UNITY_IOS
		float fver1 = 0.0f;
		float fver2  = 0.0f;
		float.TryParse(UnityEngine.iOS.Device.systemVersion.Split('.')[0], out fver1);
		float.TryParse(UnityEngine.iOS.Device.systemVersion.Split('.')[1], out fver2);

		if (fver1 > 0 && fver1 < 8f)
		{
			Debug.Log ("****** REDIRECTING TO RATE URL: " + IOSRateURL7);
			Application.OpenURL(IOSRateURL7);
		}
		else if (fver1 >= 10)
		{
			//if (fver1 == 10 && fver2 < 3)
			Application.OpenURL(IOSRateURL);

		}
		else
		{
			Debug.Log ("****** REDIRECTING TO RATE URL: " + IOSRateURL);
			Application.OpenURL(IOSRateURL);
		}
#elif UNITY_ANDROID
		Debug.Log ("****** REDIRECTING TO RATE URL: " + IOSRateURL);
		Application.OpenURL(AndroidURL);
#endif




	}

	public void ShowCompletionBannerForAchievements()
	{
		if(showAchievementBanners)
		{
			showAchievementBanners = false;
			#if UNITY_IOS //|| UNITY_EDITOR
			GameCenterBinding.showCompletionBannerForAchievements();
			#elif UNITY_ANDROID
			if(achievementsUnlocked != null && achievementsUnlocked.Count > 0)
			{
				foreach(string achievementID in achievementsUnlocked)
				{
					PlayGameServices.revealAchievement(achievementID);
				}

				achievementsUnlocked.Clear();
			}
			#endif
		}
	}

	public void SubmitHighScore(int score, LeaderboardType leaderboardType)
	{
		if(IsAuthenticated())
		{
			#if UNITY_IOS //|| UNITY_EDITOR
			
			if (leaderboardType == LeaderboardType.SurfLeaderBoard)
				GameCenterBinding.reportScore((int)score, leaderboardHighestSurfScoreID);
			else if (leaderboardType == LeaderboardType.JeepLeaderBoard)
				GameCenterBinding.reportScore((int)score, leaderboardHighestJeepScoreID);
			else if (leaderboardType == LeaderboardType.CredLeaderBoard)
				GameCenterBinding.reportScore((int)score, leaderboardHighestCredScoreID);
			else if (leaderboardType == LeaderboardType.SnowSurferBoard)
				GameCenterBinding.reportScore((int)score, leaderboardHighestSnowboardScoreID);
			else if (leaderboardType == LeaderboardType.CarGameBoard)
				GameCenterBinding.reportScore((int)score, leaderboardHighestCarGameScoreID);

			#elif UNITY_ANDROID

			if (leaderboardType == LeaderboardType.SurfLeaderBoard)
				PlayGameServices.submitScore(leaderboardHighestSurfScoreID, (int)score);
			else if (leaderboardType == LeaderboardType.JeepLeaderBoard)
				PlayGameServices.submitScore(leaderboardHighestJeepScoreID, (int)score);
			else if (leaderboardType == LeaderboardType.CredLeaderBoard)
				PlayGameServices.submitScore(leaderboardHighestCredScoreID, (int)score);
			else if (leaderboardType == LeaderboardType.SnowSurferBoard)
				PlayGameServices.submitScore(leaderboardHighestSnowboardScoreID, (int)score);
			else if (leaderboardType == LeaderboardType.CarGameBoard)
				PlayGameServices.submitScore(leaderboardHighestCarGameScoreID, (int)score);

			#endif
		}
	}



	public bool UnlockAchievement(string achievementID)
	{
		if(!IsAuthenticated())
			return false;

		string key = string.Format("GCPM_{0}", achievementID);
		if(!PlayerPrefs.HasKey(key))
		{
			PlayerPrefs.SetInt(key, 1);
			#if UNITY_IOS //|| UNITY_EDITOR
			GameCenterBinding.reportAchievement(achievementID, 100f);
			#elif UNITY_ANDROID
			PlayGameServices.unlockAchievement(achievementID, false);
			AddAchievementsUnlockedToReveal(achievementID);
			#endif 
			showAchievementBanners = true;
		}

		return true;
	}

	void AuthenticatePlayer()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			return;
		}

		#if UNITY_IOS //|| UNITY_EDITOR
		if(Prime31.GameCenterBinding.isPlayerAuthenticated() == false)
			Prime31.GameCenterBinding.authenticateLocalPlayer(true);
		#elif UNITY_ANDROID
		if(PlayGameServices.isSignedIn() == false)
			PlayGameServices.authenticate();
		#endif
	}

	bool IsAuthenticated()
	{
		#if UNITY_IOS //|| UNITY_EDITOR
		return Prime31.GameCenterBinding.isPlayerAuthenticated();
		#elif UNITY_ANDROID
		return PlayGameServices.isSignedIn();
		#endif
	}

	#if UNITY_ANDROID
	void AddAchievementsUnlockedToReveal(string achievementID)
	{
		if(achievementsUnlocked == null)
			achievementsUnlocked = new List<string>();

		achievementsUnlocked.Add(achievementID);
	}
	#endif 


	#if UNITY_IOS || UNITY_EDITOR
	// Fired when a player is logged in
	void playerAuthenticatedEvent()
	{
		
	}

	#endif
}

public enum LeaderboardType
{
	SurfLeaderBoard,
	JeepLeaderBoard,
	CredLeaderBoard,
	SnowSurferBoard,
    CarGameBoard
}