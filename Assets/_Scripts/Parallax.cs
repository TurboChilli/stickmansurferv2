﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour
{
	public GameObject TerrainMaster = null;
	public float TerrainWidthX;
	public int TerrainBufferSize;
	public float ScrollingSpeedX = 0f;
	public Transform ReferenceFocalPoint;
	public bool ScrollRight = false;
	public bool AutoInitialise = true;
	public GameObject TerrainDecals = null;

	int currentDisplayCounter;
	GameObject[] terrainPool = null;
	GameObject parentObject = null;
	bool initialised;
	public GameObject[] decals = null;

	public void UpdateFrame (bool scroll = true) {

		if(!initialised)
			return;

		if(terrainPool != null)
		{ 	
			if(scroll && ScrollingSpeedX > 0 && parentObject != null)
			{
				var parentObjectPos = parentObject.transform.position;
				float scrollX = ScrollingSpeedX * Time.deltaTime;

				if(ScrollRight)
				{
					parentObjectPos.x += scrollX;
				}
				else
				{
					parentObjectPos.x -= scrollX;
				}

				parentObject.transform.position = parentObjectPos;
			}


			if(ReferenceFocalPoint.position.x > (terrainPool[currentDisplayCounter].transform.position.x + TerrainWidthX))
			{
				var pos = terrainPool[currentDisplayCounter].transform.position;

				int lastAddedIndex = currentDisplayCounter - 1;
				if(lastAddedIndex < 0)
					lastAddedIndex = terrainPool.Length - 1;

				var lastAddedPos = terrainPool[lastAddedIndex].transform.position;
				lastAddedPos.x += TerrainWidthX;
				terrainPool[currentDisplayCounter].transform.position = lastAddedPos;

				PlaceRandomTerrainDecal(terrainPool[currentDisplayCounter]);
			
				currentDisplayCounter += 1;
				if(currentDisplayCounter >= terrainPool.Length)
				{
					currentDisplayCounter = 0;
				}
			}
		}
	}

	void Start () {

		if(AutoInitialise)
		{
			Initialise();
		}

	}

	public void Initialise(Location midgroundDecalLocation = Location.Undefined) 
	{

		if(TerrainDecals != null)
		{
			string midgroundPrefabName = string.Format("Prefabs/MidgroundDecals{0}", System.Enum.GetName(typeof(Location), midgroundDecalLocation));
			TerrainDecals = Instantiate(Resources.Load(midgroundPrefabName) as GameObject);

			if(TerrainDecals == null)
			{
				decals = null;
			}
			else
			{
				decals = new GameObject[TerrainDecals.transform.childCount];
				if(decals != null && decals.Length > 0)
				{
					for(int i = 0; i < decals.Length; i++)
					{
						decals[i] = TerrainDecals.transform.GetChild(i).gameObject;

						var pos = decals[i].transform.position;
						pos.x = -99999f;
						decals[i].transform.position = pos;
						decals[i].SetActive(false);
					}
				}
			}
		}

		if(TerrainMaster != null)
		{
			var masterPos = TerrainMaster.transform.position;

			if(ScrollingSpeedX > 0)
			{
				parentObject = new GameObject();
				parentObject.transform.position = masterPos;
			}

			terrainPool = new GameObject[TerrainBufferSize];
			for(int i = 0; i < TerrainBufferSize; i++)
			{
				if(i == 0)
				{
					terrainPool[i] = TerrainMaster;
				}
				else
				{
					masterPos.x += TerrainWidthX;
					terrainPool[i] = (GameObject)Instantiate(TerrainMaster, new Vector3(masterPos.x, masterPos.y, masterPos.z), TerrainMaster.transform.rotation);
				}

				if(parentObject != null)
					terrainPool[i].transform.parent = parentObject.transform;

				if(decals != null)
				{
					for(int j = 0; j < decals.Length; j++)
					{
						decals[j].transform.parent = terrainPool[i].transform;
					}

					PlaceRandomTerrainDecal(terrainPool[i]);
				}


			}

			initialised = true;
		}
	}

	public void UpdateAllTerrainPoolTexture(Texture texture)
	{
		if(terrainPool != null)
		{
			foreach(GameObject terrain in terrainPool)
			{
				terrain.GetComponent<Renderer>().material.mainTexture = texture;
			}
		}
	}

	public void ResetTerrainPoolPositionToReferenceFocalPoint()
	{
		if(terrainPool != null && terrainPool.Length > 0)
		{
			currentDisplayCounter = 0;
			Vector3 terrainPos = new Vector3(ReferenceFocalPoint.position.x, terrainPool[0].transform.position.y, terrainPool[0].transform.position.z);

			for(int i = 0; i < TerrainBufferSize; i++)
			{
				terrainPool[i].transform.position = terrainPos;
				terrainPos.x += TerrainWidthX;
			}
		}
	}
		
	void PlaceRandomTerrainDecal(GameObject terrain)
	{
		int decalCount = terrain.transform.childCount;
		if(decalCount > 0)
		{
			for(int i = 0; i < decalCount; i++)
			{
				terrain.transform.GetChild(i).gameObject.SetActive(false);
			}

			if((Random.Range(1, 3) % 2) == 0)
			{
				int decalIndex = Random.Range(0, decalCount);
				if(decalIndex < decalCount)
				{
					GameObject decal = terrain.transform.GetChild(decalIndex).gameObject;
					var decalPos = terrain.transform.position;

					int halfTerrainWidth = (int)TerrainWidthX/3;
					decalPos.x += Random.Range((halfTerrainWidth*-1), halfTerrainWidth);
					
					decal.transform.position = decalPos;
					decal.SetActive(true);
				}
			}
		}
	}

}
