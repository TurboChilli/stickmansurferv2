﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerupManager : MonoBehaviour {

	public Camera mainCamera = null;
	public ScoreBar scoreBar = null;
	public PlayerControl playerControl = null;

	public PlayerAnimation playerAnimation;
	public AudioSource soundExplode;
    public AudioSource soundPickup;

    // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
	public ParticleSystem jetskiExplodeParticles;
	public AnimatingObject bannerTitle;
	public Transform speedboatTransform;
	public AnimatingObject windSurfBoardTransform;
	public AnimatingObject longboardTransform;
    public AnimatingObject surfboardTransform;
	public AnimatingObject goldboardTransform;
	public AnimatingObject thrusterboardTransform;

	public GameObject smokeParticles;

	public GameMenuButtons gameMenuButtons = null;

	private float longboardStartZPos = 0;
    private float surfboardStartZPos = 0;
	private float goldboardStartZPos = 0;
	private float thrusterboardStartZPos = 0;
	private float windSurfBoardStartZPos = 0;

	private int powerUpCounter = -1;

	private bool frozenPowerUps = false;

	public bool magnetPowerupOn = false; 
	public bool vehicleMagnetPowerUpOn = false;

	public bool coinMulitplierPowerupOn = false; 
	public bool vehicleCoinMultiplierPowerUpOn = false;

	private Vehicle _currVehicle = Vehicle.None;
    private Vehicle currentVehicle
	{
		get 
		{
			if(_currVehicle == Vehicle.None)
			{
				if(GameState.SharedInstance.CurrentVehicle == Vehicle.Surfboard ||
					GameState.SharedInstance.CurrentVehicle == Vehicle.GoldSurfboard ||
					GameState.SharedInstance.CurrentVehicle == Vehicle.Thruster)
				{
					_currVehicle = GameState.SharedInstance.CurrentVehicle;
				}
				else
					_currVehicle = Vehicle.Surfboard;
			}

			return _currVehicle;
		}
		set { _currVehicle = value; }
	}
	private List<PowerUp> currentPowerups;
	private List<float> currentPowerupCooldowns;

	private float[] planeCooldownLevels = 
	{
		10.0f,
		15.0f,
		20.0f,
		25.0f,
		45.0f,
		60.0f
	};

	private float[] scoreCooldownLevels = 
	{
		10.0f,
		20.0f,
		30.0f,
		40.0f,
		50.0f,
		60.0f
	};

	private float[] multiplierCooldownLevels =
	{
		5.0f,
		7.5f,
		10.0f,
		12.5f,
		20.0f,
		30.0f
	};

	private float[] magnetCooldownLevels =
	{
		5.0f,
		10.0f,
		15.0f,
		20.0f,
		30.0f,
		45.0f
	};

	private float[] jetskiCooldownLevels = 
	{
		10.0f,
		15.0f,
		20.0f,
		25.0f,
		30.0f,
		45.0f
	};

	private float[] boatCooldownLevels = 
	{
		10.0f,
		15.0f,
		20.0f,
		25.0f,
		30.0f,
		45.0f
	};

	private float[] motorbikeCooldownLevels = 
	{
		10.0f,
		15.0f,
		20.0f,
		25.0f,
		30.0f,
		45.0f
	};

    private Vehicle[] firstTimeVehicles = 
    {
    	Vehicle.Jetski,
    	Vehicle.Longboard,
    	Vehicle.Bodyboard,
		Vehicle.WindSurfBoard,
    };

	private float curPlaneCooldown = 10.0f;
	private float curMultiplierCooldown = 5.0f;
	private float curMagnetCooldown = 5.0f;
	private float curScoreCooldown = 10.0f;
	private float curJetskiCooldown = 20.0f;
	private float curBoatCooldown = 20.0f;
	private float curMotorbikeCooldown = 10.0f;

	public FillBar magnetFillBar = null;
	public FillBar doubleCoinFillBar = null;
	public FillBar planeFillBar = null;
	public FillBar scoreFillBar = null;
	public FillBar jetskiFillBar = null;
	public FillBar boatFillBar = null;
	public FillBar motorbikeBar = null;

	List<FillBar> fillBars = null;

	void Start()
	{
		if (playerControl == null)
			playerControl = FindObjectOfType<PlayerControl>();
		if (gameMenuButtons == null)
			gameMenuButtons = FindObjectOfType<GameMenuButtons>();

		longboardStartZPos = longboardTransform.transform.position.z;
        surfboardStartZPos = surfboardTransform.transform.position.z;
		goldboardStartZPos = goldboardTransform.transform.position.z;
		thrusterboardStartZPos = thrusterboardTransform.transform.position.z;
		windSurfBoardStartZPos = windSurfBoardTransform.transform.position.z;

		currentPowerups = new List<PowerUp>();
		currentPowerupCooldowns = new List<float>();

		magnetFillBar.gameObject.SetActive(false);
		doubleCoinFillBar.gameObject.SetActive(false);
		planeFillBar.gameObject.SetActive(false);
		scoreFillBar.gameObject.SetActive(false);
		jetskiFillBar.gameObject.SetActive(false);
		boatFillBar.gameObject.SetActive(false);
		motorbikeBar.gameObject.SetActive(false);

		if (mainCamera == null)
			mainCamera = Camera.main;

        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 1f, 1f, magnetFillBar.gameObject.transform, mainCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 3f, 1f, doubleCoinFillBar.gameObject.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 5f, 1f, planeFillBar.gameObject.transform, mainCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 7f, 1f, scoreFillBar.gameObject.transform, mainCamera);
	}

	public void Update()
	{        
		for (int i = 0; i < currentPowerupCooldowns.Count; i++)
		{
			// Ensure array indexes exist.
			if(currentPowerups.Count <= i || currentPowerupCooldowns.Count <= i)
				continue;
			
			if (currentPowerupCooldowns[i] > 0.0f)
			{
				if (!frozenPowerUps)
				{
					if (currentPowerups[i] == PowerUp.CoinMagnet && !vehicleMagnetPowerUpOn)
						currentPowerupCooldowns[i] -= Time.deltaTime;
					else if (currentPowerups[i] == PowerUp.CoinMultiplier && !vehicleCoinMultiplierPowerUpOn)
						currentPowerupCooldowns[i] -= Time.deltaTime;
					else 
						currentPowerupCooldowns[i] -= Time.deltaTime;

					if (currentPowerups[i] == PowerUp.CoinMagnet)
						magnetFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curMagnetCooldown);
					else if (currentPowerups[i] == PowerUp.CoinMultiplier)
						doubleCoinFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curMultiplierCooldown);
					else if (currentPowerups[i] == PowerUp.ScoreMultiplier)
						scoreFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curScoreCooldown);

					else if (currentPowerups[i] == PowerUp.Plane)
					{
						planeFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curPlaneCooldown);
						if (currentPowerupCooldowns[i] / curPlaneCooldown < 0.25 && !smokeParticles.activeSelf)
							ShowSmokeParticles();
					}
					else if (currentPowerups[i] == PowerUp.Jetski)
					{
						jetskiFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curJetskiCooldown);
						if (currentPowerupCooldowns[i] / curJetskiCooldown < 0.25 && !smokeParticles.activeSelf)
							ShowSmokeParticles();
					}
					else if (currentPowerups[i] == PowerUp.Boat)
					{
						boatFillBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curBoatCooldown);
						if ( currentPowerupCooldowns[i] / curBoatCooldown < 0.25 && !smokeParticles.activeSelf)
							ShowSmokeParticles();
					}
					else if (currentPowerups[i] == PowerUp.Motorcycle)
					{
						motorbikeBar.SetVertBarAtValue(1.0f - currentPowerupCooldowns[i] / curMotorbikeCooldown);
						if ( currentPowerupCooldowns[i] / curMotorbikeCooldown < 0.25 && !smokeParticles.activeSelf)
							ShowSmokeParticles();
					}
				}
			}
			else
			{
				if (currentPowerups[i] == PowerUp.CoinMagnet)
				{
					magnetFillBar.gameObject.SetActive(false);
					magnetPowerupOn = false;
					RemoveCurrentPowerUp(PowerUp.CoinMagnet);

				}
				else if (currentPowerups[i] == PowerUp.CoinMultiplier)
				{
					doubleCoinFillBar.gameObject.SetActive(false);
					coinMulitplierPowerupOn = false;
					RemoveCurrentPowerUp(PowerUp.CoinMultiplier);
				}
				else if (currentPowerups[i] == PowerUp.ScoreMultiplier)
				{
					scoreFillBar.gameObject.SetActive(false);
					scoreBar.DeactivatePowerUp();
					RemoveCurrentPowerUp(PowerUp.ScoreMultiplier);
				}
				else if (currentPowerups[i] == PowerUp.Plane)
				{
					planeFillBar.gameObject.SetActive(false);
					HideSmokeParticles();
					RemoveCurrentPowerUp(PowerUp.Plane);
				}
				else if (currentPowerups[i] == PowerUp.Jetski)
				{
					jetskiFillBar.gameObject.SetActive(false);
					OnCrash(playerControl.transform.position);
					RemoveCurrentPowerUp(PowerUp.Jetski);
					HideSmokeParticles();
				}
				else if (currentPowerups[i] == PowerUp.Boat)
				{
					boatFillBar.gameObject.SetActive(false);
					OnCrash(playerControl.transform.position);
					RemoveCurrentPowerUp(PowerUp.Boat);
					HideSmokeParticles();
				}
				else if (currentPowerups[i] == PowerUp.Motorcycle)
				{
					motorbikeBar.gameObject.SetActive(false);
					OnCrash(playerControl.transform.position);
					HideSmokeParticles();

					RemoveCurrentPowerUp(PowerUp.Motorcycle);

					playerControl.HideSurferBikeTrail();
					playerControl.SetSurferBikeTrailActive(false);
				}

				/*if(currentPowerups.Count > i)
					currentPowerups.RemoveAt(i);

				if(currentPowerupCooldowns.Count > i)
					currentPowerupCooldowns.RemoveAt(i);
				*/
				////////Debug.Log("Powerup: " + currentPowerups[i] + " countdown: " + currentPowerupCooldowns[i] );


			}
		}
	}        

	public void ShowSmokeParticles()
	{
		if (currentVehicle == Vehicle.Boat)
			smokeParticles.transform.localPosition = new Vector3(-0.5f, -0.75f, 0.0f);
		else if (currentVehicle == Vehicle.Jetski)
			smokeParticles.transform.localPosition = new Vector3(-0.5f, -0.55f, 0.0f);
		else if (currentVehicle == Vehicle.Motorcycle)
			smokeParticles.transform.localPosition = new Vector3(0.0f, -0.4f, 0.0f);
		else 
			smokeParticles.transform.localPosition = Vector3.zero;
	
		if (!smokeParticles.activeSelf)
			smokeParticles.SetActive(true);
	}

	public void HideSmokeParticles()
	{
		if (smokeParticles.activeSelf)
			smokeParticles.SetActive(false);
	}

	public Vehicle GetCurrentVehicle()
	{
		return currentVehicle;
	}

	public bool CheckForPowerup(PowerUp powerUp)
	{
		for (int i = 0; i < currentPowerups.Count; i++)
		{
			if (currentPowerups[i] == powerUp)
				return true;
		}
		return false;
	}

	public void SetCurrentPowerUp(PowerUp powerUp)
	{
		float cooldown = 0.0f;
		if (powerUp == PowerUp.CoinMagnet)
		{
			magnetFillBar.gameObject.SetActive(true);
			magnetFillBar.SetVertBarAtValue(1.0f);
            GlobalSettings.PlaySound(soundPickup);

            int curMagnetLevel = GameState.SharedInstance.Upgrades.
            					 UpgradeItemByType(UpgradeItemType.PowerUpMagnetDuration).CurrentUpgradeStage;

			curMagnetCooldown = magnetCooldownLevels[curMagnetLevel];
			cooldown = curMagnetCooldown;

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
			#endif

			magnetPowerupOn = true;
		}
		else if (powerUp == PowerUp.CoinMultiplier)
		{
			doubleCoinFillBar.gameObject.SetActive(true);
			doubleCoinFillBar.SetVertBarAtValue(1.0f);
            GlobalSettings.PlaySound(soundPickup);

			int curCoinLevel = 	GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.PowerUpGoldDuration).CurrentUpgradeStage;

			curMultiplierCooldown = multiplierCooldownLevels[curCoinLevel];

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
			#endif

			cooldown = curMultiplierCooldown;
			coinMulitplierPowerupOn = true;
		}
		else if (powerUp == PowerUp.Plane)
		{
			RemoveCurrentPowerUp(PowerUp.Jetski);
			RemoveCurrentPowerUp(PowerUp.Boat);
			RemoveCurrentPowerUp(PowerUp.WindSurfBoard);

			planeFillBar.gameObject.SetActive(true);
			planeFillBar.SetVertBarAtValue(1.0f);
			GlobalSettings.PlaySound(soundPickup);

			int curPlaneLevel =	GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.PlaneFuel).CurrentUpgradeStage;

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
			#endif	

			curPlaneCooldown = planeCooldownLevels[curPlaneLevel];
			cooldown = curPlaneCooldown;
			HideSmokeParticles();

		}
		else if (powerUp == PowerUp.ScoreMultiplier)
		{
			scoreFillBar.gameObject.SetActive(true);
			scoreFillBar.SetVertBarAtValue(1.0f);
			GlobalSettings.PlaySound(soundPickup);

			int curScoreLevel =	GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.PlaneFuel).CurrentUpgradeStage;

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
           	#endif	

			curScoreCooldown = scoreCooldownLevels[curScoreLevel];
			cooldown = curScoreCooldown;

			scoreBar.ActivatePowerUp();
		}
		else if (powerUp == PowerUp.Jetski)
		{
			jetskiFillBar.gameObject.SetActive(true);
			jetskiFillBar.SetVertBarAtValue(1.0f);

			int jetskiLevel = GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.JetskiFuel).CurrentUpgradeStage;

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
			#endif

           	curJetskiCooldown = jetskiCooldownLevels[jetskiLevel];
           	cooldown = curJetskiCooldown;
			HideSmokeParticles();
           	
		}
		else if (powerUp == PowerUp.Boat)
		{
			boatFillBar.gameObject.SetActive(true);
			boatFillBar.SetVertBarAtValue(1.0f);

			int boatLevel = GameState.SharedInstance.Upgrades.
							UpgradeItemByType(UpgradeItemType.SpeedBoatFuel).CurrentUpgradeStage;

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
			#endif

			curBoatCooldown = boatCooldownLevels[boatLevel];
			cooldown = curBoatCooldown;
			HideSmokeParticles();
		}

		else if (powerUp == PowerUp.Motorcycle)
        {
        	motorbikeBar.gameObject.SetActive(true);
        	motorbikeBar.SetVertBarAtValue(1.0f);

        	playerControl.SetSurferBikeTrailActive(true);
        	playerControl.ShowSurferBikeTrail();

			#if UNITY_EDITOR
			//////Debug.Log(string.Format("SetCurrentPowerUp: {0}", PowerUpTitle.Get(powerUp)));
        	#endif

        	int bikeLevel = GameState.SharedInstance.Upgrades.
        					UpgradeItemByType(UpgradeItemType.MotorbikeFuel).CurrentUpgradeStage;
			curMotorbikeCooldown = motorbikeCooldownLevels[bikeLevel];
			cooldown = curMotorbikeCooldown;
			HideSmokeParticles();

        }

		if (currentPowerups.Count != 0)
		{
			bool alreadyActive = false;
			int checkIndex = 0;
			for (; checkIndex < currentPowerups.Count; checkIndex++)
			{
				if (currentPowerups[checkIndex] == powerUp)
				{
					alreadyActive = true;
					break;
				}
			}

			if (alreadyActive)
				currentPowerupCooldowns[checkIndex] = cooldown;
			else
			{
				currentPowerups.Add(powerUp);
				currentPowerupCooldowns.Add(cooldown);
			}
		}
		else
		{
			currentPowerups.Add(powerUp);
			currentPowerupCooldowns.Add(cooldown);
		}

		AlignFillBars();
	}

	public void ToggleFreezePowerUps(bool state)
	{
		frozenPowerUps = state;
	}

	public void RemoveCurrentPowerUp(PowerUp powerUp)
	{
		if (currentPowerups.Count != 0)
		{
			for (int checkIndex = 0; checkIndex < currentPowerups.Count; checkIndex++)
			{
				if (currentPowerups[checkIndex] == powerUp)
				{
					if (currentPowerups[checkIndex] == PowerUp.CoinMagnet)
					{
						magnetFillBar.gameObject.SetActive(false);
						magnetPowerupOn = false;
					}
					else if (currentPowerups[checkIndex] == PowerUp.CoinMultiplier)
					{
						doubleCoinFillBar.gameObject.SetActive(false);
						coinMulitplierPowerupOn = false;
					}
					else if (currentPowerups[checkIndex] == PowerUp.ScoreMultiplier)
					{
						scoreFillBar.gameObject.SetActive(false);
						scoreBar.DeactivatePowerUp();
					}

					else if (currentPowerups[checkIndex] == PowerUp.Plane)
					{
						planeFillBar.gameObject.SetActive(false);
						HideSmokeParticles();
						vehicleMagnetPowerUpOn = false;
						vehicleCoinMultiplierPowerUpOn = false;
					}
					else if (currentPowerups[checkIndex] == PowerUp.Jetski)
					{
						jetskiFillBar.gameObject.SetActive(false);
						vehicleMagnetPowerUpOn = false;
						vehicleCoinMultiplierPowerUpOn = false;
						//jetski stuff
					}
					else if (currentPowerups[checkIndex] == PowerUp.Boat)
					{
						boatFillBar.gameObject.SetActive(false);
						vehicleMagnetPowerUpOn = false;
						vehicleCoinMultiplierPowerUpOn = false;
						//boat stuff
					}
					else if (currentPowerups[checkIndex] == PowerUp.Motorcycle)
					{
						motorbikeBar.gameObject.SetActive(false);
						vehicleMagnetPowerUpOn = false;
						vehicleCoinMultiplierPowerUpOn = false;

						playerControl.HideSurferBikeTrail();
						playerControl.SetSurferBikeTrailActive(false);

					}

					#if UNITY_EDITOR
					//////Debug.Log(string.Format("Removing Current PowerUp: {0}", PowerUpTitle.Get(currentPowerups[checkIndex] )));
					#endif

					currentPowerups.RemoveAt(checkIndex);
					currentPowerupCooldowns.RemoveAt(checkIndex);
					break;
				}
			}

		
			AlignFillBars();
		}
	}

	public void RemoveAllPowerUps()
	{
		HideSmokeParticles();
	
		if (currentPowerups.Count != 0)
		{
			for (int checkIndex = 0; checkIndex < currentPowerups.Count; checkIndex++)
			{
				if (currentPowerups[checkIndex] == PowerUp.CoinMagnet)
				{
					magnetPowerupOn = false;
				}
				else if (currentPowerups[checkIndex] == PowerUp.CoinMultiplier)
				{
					coinMulitplierPowerupOn = false;
				}
				else if (currentPowerups[checkIndex] == PowerUp.ScoreMultiplier)
				{
					scoreBar.DeactivatePowerUp();
				}
				else if (currentPowerups[checkIndex] == PowerUp.Plane)
				{
					vehicleMagnetPowerUpOn = false;
					vehicleCoinMultiplierPowerUpOn = false;
				}
				else if (currentPowerups[checkIndex] == PowerUp.Jetski)
				{
					vehicleMagnetPowerUpOn = false;
					vehicleCoinMultiplierPowerUpOn = false;
				}
				else if (currentPowerups[checkIndex] == PowerUp.Boat)
				{
					vehicleMagnetPowerUpOn = false;
					vehicleCoinMultiplierPowerUpOn = false;
				}
                else if (currentPowerups[checkIndex] == PowerUp.Motorcycle)
                {                    
                    //  MOTORCYCLE STUFF
					vehicleMagnetPowerUpOn = false;
					vehicleCoinMultiplierPowerUpOn = false;

					playerControl.HideSurferBikeTrail();
					playerControl.SetSurferBikeTrailActive(false);

                }
				currentPowerups.RemoveAt(checkIndex);
				currentPowerupCooldowns.RemoveAt(checkIndex);
			}
		}

		magnetFillBar.gameObject.SetActive(false);
		doubleCoinFillBar.gameObject.SetActive(false);
		scoreFillBar.gameObject.SetActive(false);
		planeFillBar.gameObject.SetActive(false);
		boatFillBar.gameObject.SetActive(false);
		jetskiFillBar.gameObject.SetActive(false);
		motorbikeBar.gameObject.SetActive(false);
	}

	void AlignFillBars()
	{
		if(fillBars == null)
		{
			fillBars = new List<FillBar>
			{
				doubleCoinFillBar,
				magnetFillBar,
				planeFillBar,
				scoreFillBar,
				jetskiFillBar,
				boatFillBar,
				motorbikeBar
			};
		}

		fillBars.Sort(delegate(FillBar p1, FillBar p2)
			{
				return p1.gameObject.activeSelf.CompareTo(p2.gameObject.activeSelf);
			});

		fillBars.Sort(delegate(FillBar p1, FillBar p2)
			{
				return p2.CurrentVerticalBarValue.CompareTo(p1.CurrentVerticalBarValue);
			});
		

		float leftPad = 0.95f;
		foreach(var fillBar in fillBars)
		{
			if(fillBar.gameObject.activeSelf)
			{
				UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, leftPad, 0.95f, fillBar.gameObject.transform, mainCamera);
				leftPad += 1.70f;
			}
		}
	}
		
	static int prevPowerUpCounter = -999;
	public void SetRandomVehicle()
	{
		
		

		Vehicle goalVehicle = Vehicle.None;
		Goal[] curGoals = GoalManager.SharedInstance.goals;

		int curFirstVehicleIndex = GameState.SharedInstance.FirstTimeVehicleIndex;
		if (curFirstVehicleIndex >= firstTimeVehicles.Length)
		{
			for (int i = 0; i < 3; i++)
			{	
				if (!curGoals[i].complete)
				{
					switch (curGoals[i].goalType)
					{
						case GoalType.GetVehicle:
						case GoalType.GetTotalVehicle:
							goalVehicle = (Vehicle)curGoals[i].arg1;
						break;

						case GoalType.ReachDistanceVehicle:
						case GoalType.GetCoinsVehicle:
						case GoalType.ReachTotalDistanceVehicle:
							goalVehicle = (Vehicle)curGoals[i].arg2;
						break;
					}
				}
			}

			if (goalVehicle != Vehicle.None && Random.Range(0, 2) == 0)
			{
				powerUpCounter = ((int)goalVehicle - 2);
			}
			else
			{
				powerUpCounter = (int)VehicleHelper.GetWeightedRandomVehicle() - 2;
				while (powerUpCounter == prevPowerUpCounter)
				{
					powerUpCounter = (int)VehicleHelper.GetNextWeightedRandomVehicle( (Vehicle)(powerUpCounter + 2)  ) - 2;
				}
			}

			UpgradeItemType lastUpgradedItemType = GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased;

			if(lastUpgradedItemType != UpgradeItemType.None &&
				lastUpgradedItemType != UpgradeItemType.PowerUpGoldDuration &&
				lastUpgradedItemType != UpgradeItemType.PowerUpMagnetDuration)
			{
				switch(lastUpgradedItemType)
				{
				case UpgradeItemType.JetskiFuel:
				case UpgradeItemType.JetskiMagnet:
				case UpgradeItemType.JetskiGold:
					powerUpCounter = 0;
					break;
				case UpgradeItemType.BodyboardGold:
				case UpgradeItemType.BodyboardMagnet:
					powerUpCounter = 1;
					break;
				case UpgradeItemType.TubeGold:
				case UpgradeItemType.TubeMagnet:
					powerUpCounter = 2;
					break;
				case UpgradeItemType.SpeedBoatFuel:
				case UpgradeItemType.SpeedBoatGold:
				case UpgradeItemType.SpeedBoatMagnet:
					powerUpCounter = 3;
					break;
				case UpgradeItemType.LongboardGold:
				case UpgradeItemType.LongboardMagnet:
					powerUpCounter = 4;
					break;
				case UpgradeItemType.PlaneFuel:
				case UpgradeItemType.PlaneGold:
				case UpgradeItemType.PlaneMagnet:
					powerUpCounter = 5;
					break;
				case UpgradeItemType.ChilliGold:
				case UpgradeItemType.ChilliMagnet:
					powerUpCounter = 6;
					break;
				case UpgradeItemType.MotorbikeFuel:
				case UpgradeItemType.MotorbikeGold:
				case UpgradeItemType.MotorbikeMagnet:
					powerUpCounter = 7;
					break;
				case UpgradeItemType.WindSurfBoardGold:
				case UpgradeItemType.WindSurfBoardMagnet:
					powerUpCounter = 8;
					break;
				default:
					break;
				}
				GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased = UpgradeItemType.None;
			}
		}
		else
		{
			powerUpCounter = (int)firstTimeVehicles[curFirstVehicleIndex] - 2;
			GameState.SharedInstance.FirstTimeVehicleIndex += 1;
		}
	
		prevPowerUpCounter = powerUpCounter;

		RemoveCurrentPowerUp(PowerUp.Jetski);
		RemoveCurrentPowerUp(PowerUp.Boat);
		RemoveCurrentPowerUp(PowerUp.WindSurfBoard);
		RemoveCurrentPowerUp(PowerUp.Plane);
		RemoveCurrentPowerUp(PowerUp.Motorcycle);

		if(powerUpCounter == 0)
		{
			SetCurrentVehicle(Vehicle.Jetski);
		}
		else if(powerUpCounter == 1)
		{
			SetCurrentVehicle(Vehicle.Bodyboard);
		}
		else if(powerUpCounter == 2)
		{
			SetCurrentVehicle(Vehicle.Tube);
		}
		else if(powerUpCounter == 3)
		{
			SetCurrentVehicle(Vehicle.Boat);
		}
		else if(powerUpCounter == 4)
		{
			SetCurrentVehicle(Vehicle.Longboard);
		}
		else if (powerUpCounter == 5)
		{
			SetCurrentVehicle(Vehicle.Plane);
		}
        else if (powerUpCounter == 6)
        {
            SetCurrentVehicle(Vehicle.Chilli);
        }
        else if (powerUpCounter == 7)
        {
            SetCurrentVehicle(Vehicle.Motorcycle);
        }
		else if (powerUpCounter == 8)
		{
			SetCurrentVehicle(Vehicle.WindSurfBoard);
		}
        else
        {
			//////Debug.Log(string.Format("unrecognized powerup counter {0} ", powerUpCounter));
        }

	}

	public float GetGrindYOffset()
	{
		if(currentVehicle == Vehicle.Boat)
		{
			return 0.9f;
		}
		else
		{
			return 0.7f;
		}
	}

	public Vector3 GetTrailOffset()
	{	
		if (currentVehicle == Vehicle.Motorcycle)
			return new Vector3(-0.05f, -0.01f, 0.0f);
		else if (currentVehicle == Vehicle.Chilli)
			return new Vector3( 0.1f, 0.3f, 0.0f);
		else
			return Vector3.zero;
	}

    public void ShowBaseSurfboard()
    {
		if(currentVehicle == Vehicle.Thruster)
		{
			ShowLocalTransform(thrusterboardTransform.transform);
			thrusterboardTransform.transform.position = new Vector3(thrusterboardTransform.transform.position.x, thrusterboardTransform.transform.position.y, thrusterboardStartZPos + 0.03f);
		}
		else if(currentVehicle == Vehicle.GoldSurfboard)
		{
			ShowLocalTransform(goldboardTransform.transform);
			goldboardTransform.transform.position = new Vector3(goldboardTransform.transform.position.x, goldboardTransform.transform.position.y, goldboardStartZPos + 0.03f);
		}
		else
		{
			ShowLocalTransform(surfboardTransform.transform);
			surfboardTransform.transform.position = new Vector3(surfboardTransform.transform.position.x, surfboardTransform.transform.position.y, surfboardStartZPos + 0.03f);
		}
    }

	public void HideAllVehicleTransforms()
	{
		HideLocalTransform(speedboatTransform);
		HideLocalTransform(longboardTransform.transform);
        HideLocalTransform(surfboardTransform.transform);
		HideLocalTransform(goldboardTransform.transform);
		HideLocalTransform(thrusterboardTransform.transform);
		HideLocalTransform(windSurfBoardTransform.transform);
	}

	public void HideLocalTransform(Transform theTransform)
	{
		if(theTransform.localPosition.y > -999)
		{
			theTransform.localPosition = new Vector3(theTransform.localPosition.x, theTransform.localPosition.y - 99999f, theTransform.localPosition.z);
		}
	}

	public void ShowLocalTransform(Transform theTransform)
	{
		if(theTransform.localPosition.y < -999)
		{
			theTransform.localPosition = new Vector3(theTransform.localPosition.x, theTransform.localPosition.y + 99999f, theTransform.localPosition.z);
		}
	}

	public void SetPowerUpFrame(int frameIndex)
	{
		bool isGrinding = (playerControl.grindingRail);

		if(currentVehicle == Vehicle.Longboard)
		{
			longboardTransform.DrawFrame(frameIndex);
			if (!isGrinding)
			{
				if(frameIndex < 2)
				{
					longboardTransform.transform.position = new Vector3(longboardTransform.transform.position.x, longboardTransform.transform.position.y, longboardStartZPos + 0.03f);
				}
				else
				{
					longboardTransform.transform.position = new Vector3(longboardTransform.transform.position.x, longboardTransform.transform.position.y, longboardStartZPos - 0.03f);
				}
			}
		}
		else if(currentVehicle == Vehicle.WindSurfBoard)
		{
			windSurfBoardTransform.DrawFrame(frameIndex);
			if (!isGrinding)
			{
				if(frameIndex < 1)
				{
					windSurfBoardTransform.transform.position = new Vector3(windSurfBoardTransform.transform.position.x, windSurfBoardTransform.transform.position.y, windSurfBoardStartZPos + 0.03f);
				}
				else
				{
					windSurfBoardTransform.transform.position = new Vector3(windSurfBoardTransform.transform.position.x, windSurfBoardTransform.transform.position.y, windSurfBoardStartZPos - 0.03f);
				}
			}
		}
        else if(currentVehicle == Vehicle.Surfboard)
        {
            surfboardTransform.DrawFrame(frameIndex);
			if (!isGrinding)
			{
	            if(frameIndex < 2)
	            {
	                surfboardTransform.transform.position = new Vector3(surfboardTransform.transform.position.x, surfboardTransform.transform.position.y, surfboardStartZPos + 0.03f);
	            }
	            else
	            {
	                surfboardTransform.transform.position = new Vector3(surfboardTransform.transform.position.x, surfboardTransform.transform.position.y, surfboardStartZPos - 0.03f);
	            }
            }
        }
		else if(currentVehicle == Vehicle.Thruster)
		{
			thrusterboardTransform.DrawFrame(frameIndex);
			if (!isGrinding)
			{
				if(frameIndex < 2)
				{
					thrusterboardTransform.transform.position = new Vector3(thrusterboardTransform.transform.position.x, thrusterboardTransform.transform.position.y, thrusterboardStartZPos + 0.03f);
				}
				else
				{
					thrusterboardTransform.transform.position = new Vector3(thrusterboardTransform.transform.position.x, thrusterboardTransform.transform.position.y, thrusterboardStartZPos - 0.03f);
				}
			}
		}
		else if(currentVehicle == Vehicle.GoldSurfboard)
		{
			goldboardTransform.DrawFrame(frameIndex);
			if (!isGrinding)
			{
				if(frameIndex < 2)
				{
					goldboardTransform.transform.position = new Vector3(goldboardTransform.transform.position.x, goldboardTransform.transform.position.y, goldboardStartZPos + 0.03f);
				}
				else
				{
					goldboardTransform.transform.position = new Vector3(goldboardTransform.transform.position.x, goldboardTransform.transform.position.y, goldboardStartZPos - 0.03f);
				}
			}
		}
	}

	public void SetCurrentVehicle(Vehicle pow)
	{        
		//pow = PowerUp.LONGBOARD;
		HideAllVehicleTransforms();
		currentVehicle = pow;

		if(pow == Vehicle.Jetski)
		{
			//bannerTitle.DrawFrame(0);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			playerAnimation.DoAnimation(PlayerAnimation.Animation.JETSKI);
			SetCurrentPowerUp(PowerUp.Jetski);

			bool coinJetski = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.JetskiGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinJetski;

			bool magnetJetski = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.JetskiMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetJetski;

		}
		else if(pow == Vehicle.Bodyboard)
		{
			//bannerTitle.DrawFrame(2);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			playerAnimation.DoAnimation(PlayerAnimation.Animation.BODYBOARD);

			bool coinBodyBoard = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.BodyboardGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinBodyBoard;

			bool magnetBodyBoard = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.BodyboardMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetBodyBoard;

		}
		else if(pow == Vehicle.Tube)
		{
			//bannerTitle.DrawFrame(3);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			playerAnimation.DoAnimation(PlayerAnimation.Animation.TUBE);

			bool coinTube = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.TubeGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinTube;

			bool magnetTube = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.TubeMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetTube;

		}
		else if(pow == Vehicle.Longboard)
		{
			//bannerTitle.DrawFrame(4);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			ShowLocalTransform(longboardTransform.transform);
			longboardTransform.DrawFrame(1);

			playerAnimation.DoAnimation(PlayerAnimation.Animation.LONGBOARD_CARVE_RIGHT);

			bool coinLongboard = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.LongboardGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinLongboard;

			bool magnetLongboard = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.LongboardMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetLongboard;

		}
		else if(pow == Vehicle.Boat)
		{
			//bannerTitle.DrawFrame(1);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			ShowLocalTransform(speedboatTransform);
			playerAnimation.DoAnimation(PlayerAnimation.Animation.BOAT);
			SetCurrentPowerUp(PowerUp.Boat);

			bool coinBoat = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.SpeedBoatGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinBoat;

			bool magnetBoat = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.SpeedBoatMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetBoat;
		}
		else if(pow == Vehicle.WindSurfBoard)
		{
			//bannerTitle.DrawFrame(1);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));

			ShowLocalTransform(windSurfBoardTransform.transform);
			windSurfBoardTransform.DrawFrame(1);
			playerAnimation.DoAnimation(PlayerAnimation.Animation.WINDSURF_CARVE_RIGHT);


			SetCurrentPowerUp(PowerUp.WindSurfBoard);

			bool coinWindSurf = (GameState.SharedInstance.Upgrades.
				UpgradeItemByType(UpgradeItemType.WindSurfBoardGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinWindSurf;

			bool magnetWindSurf = (GameState.SharedInstance.Upgrades.
				UpgradeItemByType(UpgradeItemType.WindSurfBoardMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetWindSurf;
		}
		else if(pow == Vehicle.Plane)
		{
			//bannerTitle.DrawFrame(5);
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			playerAnimation.DoAnimation(PlayerAnimation.Animation.PLANE);
			SetCurrentPowerUp(PowerUp.Plane);

			bool coinPlane = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.PlaneGold).CurrentUpgradeStage != 0);

            vehicleCoinMultiplierPowerUpOn = coinPlane;

			bool magnetPlane = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.PlaneMagnet).CurrentUpgradeStage != 0);

			vehicleMagnetPowerUpOn = magnetPlane;

			HideLocalTransform(longboardTransform.transform);
			HideLocalTransform(speedboatTransform.transform);
			HideLocalTransform(windSurfBoardTransform.transform);
		}
		else if(pow == Vehicle.Surfboard)
		{
			//playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
            ShowLocalTransform(surfboardTransform.transform);
            surfboardTransform.DrawFrame(1);
            playerAnimation.DoAnimation(PlayerAnimation.Animation.ORANGEBOARD_CARVE_RIGHT);
			HideLocalTransform(longboardTransform.transform);
			HideLocalTransform(goldboardTransform.transform);
			HideLocalTransform(thrusterboardTransform.transform);
			HideLocalTransform(speedboatTransform.transform);
			HideLocalTransform(windSurfBoardTransform.transform);
			HideSmokeParticles();      
		}
		else if(pow == Vehicle.Thruster)
		{
			//playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			ShowLocalTransform(thrusterboardTransform.transform);
			thrusterboardTransform.DrawFrame(1);
			playerAnimation.DoAnimation(PlayerAnimation.Animation.THRUSTER_CARVE_RIGHT);
			HideLocalTransform(longboardTransform.transform);
			HideLocalTransform(speedboatTransform.transform);
			HideLocalTransform(windSurfBoardTransform.transform);
			HideLocalTransform(surfboardTransform.transform);
			HideLocalTransform(goldboardTransform.transform);
			HideSmokeParticles();       
		}
		else if(pow == Vehicle.GoldSurfboard)
		{
			//playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
			ShowLocalTransform(goldboardTransform.transform);
			goldboardTransform.DrawFrame(1);
			playerAnimation.DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_CARVE_RIGHT);
			HideLocalTransform(longboardTransform.transform);
			HideLocalTransform(speedboatTransform.transform);
			HideLocalTransform(windSurfBoardTransform.transform);
			HideLocalTransform(thrusterboardTransform.transform);
			HideLocalTransform(surfboardTransform.transform);
			HideSmokeParticles();           
		}
        else if(pow == Vehicle.Chilli)
        {
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
            playerAnimation.DoAnimation(PlayerAnimation.Animation.CHILLI);
            
			bool coinChilli = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.ChilliGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinChilli;

			bool magnetChilli = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.ChilliMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetChilli;

        }
        else if(pow == Vehicle.Motorcycle)
        {
			playerControl.SetVehicleBannerTitle(VehicleHelper.GetName(pow));
            playerAnimation.DoAnimation(PlayerAnimation.Animation.MOTORCYCLE);
			SetCurrentPowerUp(PowerUp.Motorcycle);
            
			bool coinBike = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.MotorbikeGold).CurrentUpgradeStage != 0);
			vehicleCoinMultiplierPowerUpOn = coinBike;

			bool magnetBike = (GameState.SharedInstance.Upgrades.
            					UpgradeItemByType(UpgradeItemType.MotorbikeMagnet).CurrentUpgradeStage != 0);
			vehicleMagnetPowerUpOn = magnetBike;

			playerControl.SetSurferBikeTrailActive(true);
			playerControl.ShowSurferBikeTrail();

			HideLocalTransform(longboardTransform.transform);
			HideLocalTransform(speedboatTransform.transform);
			HideLocalTransform(windSurfBoardTransform.transform);

        }
	}

	public void OnCrash(Vector3 playerCrashPos, bool showCrashParticles = true)
	{
		vehicleCoinMultiplierPowerUpOn = false;
		vehicleMagnetPowerUpOn = false;
        if (showCrashParticles)
        {

            jetskiExplodeParticles.gameObject.transform.position = playerCrashPos;
        }
        // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
        Debug.Log("CRASHING HITTING CRATE?");
        jetskiExplodeParticles.Play();
		//GlobalSettings.PlaySound(soundExplode);

		GlobalSettings.PlaySound(soundExplode);

		bool ensureGrindZIndex = false;
		if (currentVehicle == Vehicle.Boat)
		{
			RemoveCurrentPowerUp(PowerUp.Boat);
			ensureGrindZIndex = true;
		}
		else if (currentVehicle == Vehicle.WindSurfBoard)
		{
			RemoveCurrentPowerUp(PowerUp.WindSurfBoard);
			ensureGrindZIndex = true;
		}
		else if (currentVehicle == Vehicle.Motorcycle)
		{
			RemoveCurrentPowerUp(PowerUp.Motorcycle);
			ensureGrindZIndex = true;
		}
		else if (currentVehicle == Vehicle.Jetski)
		{
			RemoveCurrentPowerUp(PowerUp.Jetski);
			ensureGrindZIndex = true;
		}
		else if (currentVehicle == Vehicle.Plane)
		{
			RemoveCurrentPowerUp(PowerUp.Plane);
		}

		SetCurrentVehicle(playerControl.StartingVehicle);

		// This call ensures the right player z-index if on grind.
		if(ensureGrindZIndex && playerControl.grindingRail)
		{
			playerAnimation.OnUpdateAnimation();
		}
	}

	public float GetVehicleBoostY()
	{
		if(currentVehicle == Vehicle.WindSurfBoard)
			return 9f;

		return 8f;
	}

	public float GetVehicleBoostX()
	{
		if(currentVehicle == Vehicle.Jetski)
		{
			return 10f;
		}
		else if(currentVehicle == Vehicle.Bodyboard)
		{
			return 9f;
		}
		else if(currentVehicle == Vehicle.Longboard)
		{
			return 5f;
		}
		else if(currentVehicle == Vehicle.Boat)
		{
			return 10f;
		}
		else if(currentVehicle == Vehicle.WindSurfBoard)
		{
			return 10f;
		}
		else
		{
			return 7f;
		}
	}

	public float GetVehicleAirXVelocity()
	{
		if(currentVehicle == Vehicle.Jetski)
		{
			return 10f;
		}
		else if(currentVehicle == Vehicle.Bodyboard)
		{
			return 8f;
		}
		else if (currentVehicle == Vehicle.Longboard)
		{
			return 5f;
		}	
		else
		{
			return 7f;
		}
	}

	public float GetPowerUpCarveRotationSpeed(float carveRotationSpeed, bool carvingDown)
	{
		if(currentVehicle == Vehicle.Bodyboard)
		{
			carveRotationSpeed = carveRotationSpeed * 1.3f;
		}
		else if(currentVehicle == Vehicle.Boat)
		{
			carveRotationSpeed = carveRotationSpeed * 0.75f;
		}
		else if(currentVehicle == Vehicle.WindSurfBoard)
		{
			carveRotationSpeed = carveRotationSpeed * 0.85f;
		}
		else if(currentVehicle == Vehicle.Longboard)
		{
			carveRotationSpeed = carveRotationSpeed * 0.75f;
		}
		else if(currentVehicle == Vehicle.Motorcycle)
		{
			carveRotationSpeed = carveRotationSpeed * 1.1f;
		}

		return carveRotationSpeed;
	}

	public float GetVehicleXVelocity(float currentXVelocity)
	{
		if(currentVehicle == Vehicle.Jetski)
		{
			currentXVelocity += 120f * Time.deltaTime;
			if(currentXVelocity > 17)
			{
				currentXVelocity = 17f;
			}
		}
		else if(currentVehicle == Vehicle.Boat)
		{
			currentXVelocity += 250f * Time.deltaTime;
			if(currentXVelocity > 20)
			{
				currentXVelocity = 20f;
			}
		}
		else if(currentVehicle == Vehicle.WindSurfBoard)
		{
			currentXVelocity += 75f * Time.deltaTime;
			if(currentXVelocity > 15)
			{
				currentXVelocity = 15f;
				// tuning required?
			}
		}
		else if(currentVehicle == Vehicle.Bodyboard)
		{
			currentXVelocity += 30f * Time.deltaTime;
			if(currentXVelocity > 12)
			{
				currentXVelocity = 12f;
			}
		}
		else if(currentVehicle == Vehicle.Motorcycle)
		{
			currentXVelocity += 110f * Time.deltaTime;
			if(currentXVelocity > 15.5f)
			{
				currentXVelocity = 15.5f;
			}
		}

		return currentXVelocity;
	}
}