﻿using UnityEngine;
using System.Collections;
using Amazon;

public class ServerSavedGameStateSync : CommonDynamoDBContext {

	public static bool DataSyncedFromServer = false;
	bool debugOutput = false;
	static float? timeSinceLastSync = null; 

	public delegate void LoginWithEmailSuccessHandler();
	public event LoginWithEmailSuccessHandler OnLoginWithEmailSuccess;

	public delegate void LoginWithEmailFailedHandler(string errorMsg);
	public event LoginWithEmailFailedHandler OnLoginWithEmailFailed;

	public delegate void RegisterWithEmailSuccessHandler();
	public event RegisterWithEmailSuccessHandler OnRegisterWithEmailSuccess;

	public delegate void RegisterWithEmailFailedHandler(string errorMsg);
	public event RegisterWithEmailFailedHandler OnRegisterWithEmailFailed;


	public static string LoggedInSavedGameStateDataId
	{
		get 
		{
			return PlayerPrefs.GetString("ssfx", "");
		}
		set
		{
			PlayerPrefs.SetString("ssfx", value);

			if(!string.IsNullOrEmpty(value))
			{
				TutorialCompletionChecker.MarkTutorialCompleteAndRedirectToMenu = true;
			}
		}
	}

	public static void Sync()
	{

		ServerSavedGameStateSync syncher = FindObjectOfType<ServerSavedGameStateSync>();
		if(syncher != null)
		{
			syncher.SyncSavedGameState();
		}
	}

	void OnApplicationPause(bool paused)
	{
		if(paused)
		{
			SavedGameStateServerSettings.SharedInstance.UpdateGameStateDataToLocalStore(SavedGameStateServerSettings.SharedInstance.SavedGameStateData);
		}

		SyncSavedGameState();

		if(paused)
		{
			timeSinceLastSync = null;
		}
	}

	void OnApplicationQuit()
	{
		SavedGameStateServerSettings.SharedInstance.UpdateGameStateDataToLocalStore(SavedGameStateServerSettings.SharedInstance.SavedGameStateData);

		SyncSavedGameState();
	}

	void Start()
	{
		DontDestroyOnLoad(this);

		//AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWWW;

		SyncSavedGameState();
	}

	public static bool IsLoggedIn()
	{
		if(!string.IsNullOrEmpty(LoggedInSavedGameStateDataId))
			return true;
		else
			return false;
	}

	public static void LogOut()
	{
		FacebookManager.Logout();
		FacebookManager.FacebookUserId = "";
		SavedGameStateServerSettings.SharedInstance.SavedGameStateData.SavedGameStateId = "";
		SavedGameStateServerSettings.SharedInstance.FlagDataClean();
		LoggedInSavedGameStateDataId = "";

		SavedGameStateServerSettings.SharedInstance.UpdateGameStateDataToLocalStore(SavedGameStateServerSettings.SharedInstance.SavedGameStateData);
	}

	static bool ShouldSync()
	{
		if(string.IsNullOrEmpty(LoggedInSavedGameStateDataId))
			return false;
		
		if(!timeSinceLastSync.HasValue)
		{
			return true;
		}

		if((Time.time - timeSinceLastSync.Value) >= GameServerSettings.SharedInstance.SurferSettings.SecsBtwGameSavedSync)
		{
			return true;
		}

		return false;
	}

	public void LoginWithEmailAndPassword(string email, string password)
	{
		if(string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
		{
			if(OnLoginWithEmailFailed != null)
			{
				OnLoginWithEmailFailed(GameLanguage.LocalizedText("EMAIL OR PASSWORD IS INCORRECT."));
			}

			return;
		}
		else
		{
			if(debugOutput)
			{
				Debug.LogFormat("ServerSavedGameStateSync -> LoginWithEmailAndPassword (A): - Log in with email: {0}", email);
			}

			SavedGameState savedGameStateFromServer = null;

			Context.LoadAsync<SavedGameState>(email, (result)=>
				{
					if(result.Exception == null )
					{
						if(result.Result == null)
						{
							if(debugOutput)
							{
								Debug.LogFormat("ServerSavedGameStateSync -> LoginWithEmailAndPassword (B): - No record found with email: {0}", email);
							}

							if(OnLoginWithEmailFailed != null)
							{
								OnLoginWithEmailFailed(GameLanguage.LocalizedText("EMAIL OR PASSWORD IS INCORRECT."));
							}
						}
						else
						{
							savedGameStateFromServer = result.Result as SavedGameState;
							if(password.Equals(savedGameStateFromServer.Password))
							{
								if(debugOutput)
								{
									Debug.LogFormat("ServerSavedGameStateSync -> LoginWithEmailAndPassword (C): - Login with success with email: {0}", email);
								}

								LoggedInSavedGameStateDataId = savedGameStateFromServer.SavedGameStateId;

								SyncSavedGameState();

								if(OnLoginWithEmailSuccess != null)
								{
									OnLoginWithEmailSuccess();
								}
							}
							else
							{
								if(debugOutput)
								{
									Debug.Log("ServerSavedGameStateSync -> LoginWithEmailAndPassword (D): - Login with failed incorrect email or password.");
								}

								if(OnLoginWithEmailFailed != null)
								{
									OnLoginWithEmailFailed(GameLanguage.LocalizedText("EMAIL OR PASSWORD IS INCORRECT."));
								}
							}
						}
					}
					else
					{
						if(debugOutput)
						{
							Debug.LogFormat("ServerSavedGameStateSync -> LoginWithEmailAndPassword (E): - LoadAsync error: {0}", result.Exception.Message);
						}

						if(OnLoginWithEmailFailed != null)
						{
							OnLoginWithEmailFailed(result.Exception.Message);
						}
					}
				});
		}
	}

	public void RegisterWithEmailAndPassword(string email, string password)
	{
		if(string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
		{
			if(OnRegisterWithEmailFailed != null)
			{
				OnRegisterWithEmailFailed(GameLanguage.LocalizedText("EMAIL AND PASSWORD IS REQUIRED."));
			}

			return;
		}
		else
		{
			if(debugOutput)
			{
				Debug.LogFormat("ServerSavedGameStateSync -> RegisterWithEmailAndPassword (A): - Register with email: {0}", email);
			}

			Context.LoadAsync<SavedGameState>(email, (result)=>
				{
					if(result.Exception == null )
					{
						if(result.Result == null)
						{
							if(debugOutput)
							{
								Debug.LogFormat("ServerSavedGameStateSync -> RegisterWithEmailAndPassword (B): - No record found with email: {0}, registering now.", email);
							}
								
							LoggedInSavedGameStateDataId = email;
							SavedGameStateServerSettings.SharedInstance.SavedGameStateData.Password = password;
							SavedGameStateServerSettings.SharedInstance.FlagDataDirty();

							UpdateGameStateToServer();

							if(OnRegisterWithEmailSuccess != null)
							{
								OnRegisterWithEmailSuccess();	
							}
						}
						else
						{
							if(debugOutput)
							{
								Debug.Log("ServerSavedGameStateSync -> RegisterWithEmailAndPassword (C): - Email already registered.");
							}

							if(OnRegisterWithEmailFailed != null)
							{
								OnRegisterWithEmailFailed(GameLanguage.LocalizedText("EMAIL ALREADY REGISTERED. PLEASE LOG IN."));
							}
						}
					}
					else
					{
						if(debugOutput)
						{
							Debug.LogFormat("ServerSavedGameStateSync -> RegisterWithEmailAndPassword (D): - LoadAsync error: {0}", result.Exception.Message);
						}

						if(OnRegisterWithEmailFailed != null)
						{
							OnRegisterWithEmailFailed(GameLanguage.LocalizedText("EMAIL ALREADY REGISTERED. PLEASE LOG IN."));
						}
					}
				});
		}
	}

	public void SyncSavedGameState()
	{
		if(string.IsNullOrEmpty(LoggedInSavedGameStateDataId))
		{
			return;
		}

		if(debugOutput)
		{
			Debug.LogFormat("ServerSavedGameStateSync -> SyncSavedGameState (A) - LoggedInSavedGameStateDataId: {0}", LoggedInSavedGameStateDataId);
		}

		if(ShouldSync() == false)
		{
			return;
		}

		timeSinceLastSync = Time.time;

		SavedGameState savedGameStateFromServer = null;

		Context.LoadAsync<SavedGameState>(LoggedInSavedGameStateDataId, (result)=>
			{
				if(result.Exception == null )
				{
					savedGameStateFromServer = result.Result as SavedGameState;

					if(savedGameStateFromServer != null)
					{
						if(debugOutput)
						{
							Debug.LogFormat("ServerSavedGameStateSync -> SyncSavedGameState (B): - serverVersion: {0} localVersion: {1}"
								, savedGameStateFromServer.Version
								, SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced);
						}

						// Found record on server
						if(savedGameStateFromServer.Version > SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced)
						{
							if(debugOutput)
							{
								Debug.LogFormat("ServerSavedGameStateSync -> SyncSavedGameState (C): - serverCurrentLevel: {0} localCurrentLevel: {1}"
									, savedGameStateFromServer.CurrentLevel
									, SavedGameStateServerSettings.SharedInstance.SavedGameStateData.CurrentLevel);
							}

							if(savedGameStateFromServer.CurrentLevel >= SavedGameStateServerSettings.SharedInstance.SavedGameStateData.CurrentLevel)
							{
								SavedGameStateServerSettings.SharedInstance.SavedGameStateData = savedGameStateFromServer;
								SavedGameStateServerSettings.SharedInstance.UpdateLocalDataWithSavedGameState();
								SavedGameStateServerSettings.SharedInstance.UpdateGameStateDataToLocalStore(SavedGameStateServerSettings.SharedInstance.SavedGameStateData);

								DataSyncedFromServer = true;

								if(debugOutput)
								{
									Debug.Log("ServerSavedGameStateSync -> SyncSavedGameState (D):");
								}
							}

							SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced = savedGameStateFromServer.Version;
							SavedGameStateServerSettings.SharedInstance.FlagDataClean();
						}
						else if(SavedGameStateServerSettings.SharedInstance.DataIsDirty && SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced >= savedGameStateFromServer.Version)
						{
							if(debugOutput)
							{
								Debug.Log("ServerSavedGameStateSync -> SyncSavedGameState (E):");
							}

							UpdateGameStateToServer();
						}
					}
					else
					{
						if(debugOutput)
						{
							Debug.Log("ServerSavedGameStateSync -> SyncSavedGameState (F):");
						}

						// Never sync'ed before, push initial version up.
						UpdateGameStateToServer();
					}
				}
				else
				{
					if(debugOutput)
					{
						Debug.LogFormat("ServerSavedGameStateSync -> SyncSavedGameState (G): - LoadAsync error: {0}", result.Exception.Message);
					}
				}
			});
	}
	

	void UpdateGameStateToServer()
	{
		if(debugOutput)
		{
			Debug.LogFormat("ServerSavedGameStateSync -> UpdateGameStateToServer (A): - LoggedInSavedGameStateDataId: {0}", LoggedInSavedGameStateDataId);
		}

		if(string.IsNullOrEmpty(LoggedInSavedGameStateDataId))
		{
			return;
		}
			
		SavedGameStateServerSettings.SharedInstance.SavedGameStateData.SavedGameStateId = LoggedInSavedGameStateDataId;
		SavedGameStateServerSettings.SharedInstance.SavedGameStateData.Version = SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced + 1;
		SavedGameStateServerSettings.SharedInstance.RefreshDataWithLocalGameState();

		if(debugOutput)
		{
			Debug.Log("ServerSavedGameStateSync -> UpdateGameStateToServer (B)");
		}

		Context.SaveAsync<SavedGameState>(SavedGameStateServerSettings.SharedInstance.SavedGameStateData, (result) =>
			{
				if(result.Exception == null)
				{
					// Save success
					SavedGameStateServerSettings.SharedInstance.SavedGameStateData.LastVersionSynced = SavedGameStateServerSettings.SharedInstance.SavedGameStateData.Version;
					SavedGameStateServerSettings.SharedInstance.FlagDataClean();

					if(debugOutput)
					{
						Debug.Log("ServerSavedGameStateSync -> UpdateGameStateToServer (C)");
					}
				}
				else
				{
					if(debugOutput)
					{
						Debug.LogFormat("ServerSavedGameStateSync -> UpdateGameStateToServer (D): - SaveAsync error: {0}", result.Exception.Message);
					}
				}
			});
	}
}
