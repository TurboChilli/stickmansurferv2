﻿using UnityEngine;
using System.Collections;

public class InGameTextureManager : MonoBehaviour {

	public Renderer backgroundLayer;
	public Renderer midgroundLayer;
	public Renderer foregroundLayer;
	public Renderer waveRenderer;

	public TextureSlide backgroundtextureSlider;
	public TextureSlide midgroundTextureSlider;
	public TextureSlide foregroundTextureSlider;
	public WaveTextureSlide waveTextureSlider;

	private Texture backgroundTexture = null;
	private Texture midgroundTexture = null;
	private Texture foregroundTexture = null;
	private Texture waveTexture = null;

	public bool isIndoor = false;
	private bool loadedAssets = false;

	public void LoadBackgroundImages(Location location)
	{
		string locationName = "";

		switch (location)
		{
			case Location.MainBeach:
				locationName = "MainBeach";
				break;
			case Location.BigWaveReef:
				locationName = "BigWaveReef";
				break;
			case Location.TikiBay:
				locationName = "TikiBay";
				break;
			case Location.WavePark:
				locationName = "WavePark";
				break;
			case Location.Cave:
				locationName = "Cave";
				break;
			case Location.ShipWreck:
				locationName = "ShipWreck";
				break;
			case Location.Harbor:
				locationName = "Harbor";
				break;
			default:
				locationName = "MainBeach";
				break;
		}

		if (loadedAssets)
		{
			Resources.UnloadAsset(backgroundTexture);
			Resources.UnloadAsset(midgroundTexture);
			Resources.UnloadAsset(foregroundTexture);
			Resources.UnloadAsset(waveTexture);

			System.GC.Collect();
		}

		backgroundTexture = (Texture)Resources.Load(string.Format("Ingame/_Locations/_{0}/background", locationName), typeof(Texture));
		midgroundTexture = (Texture)Resources.Load(string.Format("Ingame/_Locations/_{0}/midground", locationName), typeof(Texture));
		foregroundTexture = (Texture)Resources.Load(string.Format("Ingame/_Locations/_{0}/foreground", locationName), typeof(Texture));
		waveTexture = (Texture)Resources.Load(string.Format("Ingame/_Locations/_{0}/wave", locationName), typeof(Texture));

		loadedAssets = true;

		ResetTextures();
	}
	public void StopScrollingTextures()
	{
		if(GlobalSettings.surfGamePaused || midgroundTextureSlider == null || foregroundTextureSlider == null)
			return;
			
		midgroundTextureSlider.Speed = 0;
		foregroundTextureSlider.Speed = 0;
	}

	public void UpdateScrollingTextures(Vector2 moveVector)
	{
		if(GlobalSettings.surfGamePaused || midgroundTextureSlider == null || foregroundTextureSlider == null)
			return;
		
		midgroundTextureSlider.Speed = moveVector.x/3f;
		foregroundTextureSlider.Speed = moveVector.x/2.7f;
	}

	public void UpdateWaveTextureOffset(float waveOffset)
	{
		waveTextureSlider.SetWaveSpeedForTextureOffset(waveOffset);
	}

	public void ResetTextures()
	{
		backgroundLayer.material.mainTexture = 	backgroundTexture;
		midgroundLayer.material.mainTexture = 	midgroundTexture;
		foregroundLayer.material.mainTexture = 	foregroundTexture;

		waveRenderer.materials[1].mainTexture = waveTexture;
		waveRenderer.materials[2].mainTexture = waveTexture;
	}

}
