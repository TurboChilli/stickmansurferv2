﻿using System;
using System.Text;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeManager
{
	List<UpgradeItem> _upgradeItems = null;

	public UpgradeManager()
	{
		StickSurferSetting surferSettings = GameServerSettings.SharedInstance.SurferSettings;

		_upgradeItems = new List<UpgradeItem>
		{
			new UpgradeItem(UpgradeItemType.SurfBoardDefault, 0, 0, Vehicle.Surfboard),
			new UpgradeItem(UpgradeItemType.SurfBoardThruster, 1, surferSettings.ThrusterBoardPrice, Vehicle.Thruster),
			new UpgradeItem(UpgradeItemType.SurfBoardGold, 1, surferSettings.GoldSurfBoardPrice, Vehicle.GoldSurfboard),

			new UpgradeItem(UpgradeItemType.JeepDefault, 0, 0, JeepType.Default),
			new UpgradeItem(UpgradeItemType.JeepBlack, 1, surferSettings.JeepBlackPrice, JeepType.Black),
			new UpgradeItem(UpgradeItemType.QuadBike, 1, surferSettings.QuadBikePrice, JeepType.QuadBike),

			new UpgradeItem(UpgradeItemType.RetroFan, 1, surferSettings.RetroFanPrice), 

			new UpgradeItem(UpgradeItemType.PowerUpGoldDuration, 5, surferSettings.PowerUpGoldDurationPrice),
			new UpgradeItem(UpgradeItemType.PowerUpMagnetDuration, 5, surferSettings.PowerUpMagnetDurationPrice),

			new UpgradeItem(UpgradeItemType.JeepFuel, 5, surferSettings.JeepFuelPrice),
			new UpgradeItem(UpgradeItemType.JeepEngine, 5, surferSettings.JeepEnginePrice),
			new UpgradeItem(UpgradeItemType.JeepWheels, 5, surferSettings.JeepWheelsPrice),

			new UpgradeItem(UpgradeItemType.JetskiFuel, 5, surferSettings.JetskiFuelPrice),
			new UpgradeItem(UpgradeItemType.JetskiGold, 1, surferSettings.JetskiGoldPrice),
			new UpgradeItem(UpgradeItemType.JetskiMagnet, 1, surferSettings.JetskiMagnetPrice),

			new UpgradeItem(UpgradeItemType.SkateGold, 1, surferSettings.SkateGoldPrice),
			new UpgradeItem(UpgradeItemType.SkateDuration, 5, surferSettings.SkateDurationPrice),

			new UpgradeItem(UpgradeItemType.BodyboardGold, 1, surferSettings.BodyboardGoldPrice),
			new UpgradeItem(UpgradeItemType.BodyboardMagnet, 1, surferSettings.BodyboardMagnetPrice),

			new UpgradeItem(UpgradeItemType.TubeGold, 1, surferSettings.TubeGoldPrice),
			new UpgradeItem(UpgradeItemType.TubeMagnet, 1, surferSettings.TubeMagnetPrice),

			new UpgradeItem(UpgradeItemType.SpeedBoatFuel, 5, surferSettings.SpeedBoatFuelPrice),
			new UpgradeItem(UpgradeItemType.SpeedBoatGold, 1, surferSettings.SpeedBoatGoldPrice),
			new UpgradeItem(UpgradeItemType.SpeedBoatMagnet, 1, surferSettings.SpeedBoatMagnetPrice),

			new UpgradeItem(UpgradeItemType.PlaneFuel, 5, surferSettings.PlaneFuelPrice),
			new UpgradeItem(UpgradeItemType.PlaneGold, 1, surferSettings.PlaneGoldPrice),
			new UpgradeItem(UpgradeItemType.PlaneMagnet, 1, surferSettings.PlaneMagnetPrice),

			new UpgradeItem(UpgradeItemType.LongboardGold, 1, surferSettings.LongboardGoldPrice),
			new UpgradeItem(UpgradeItemType.LongboardMagnet, 1, surferSettings.LongboardMagnetPrice),

			new UpgradeItem(UpgradeItemType.ChilliGold, 1, surferSettings.ChilliGoldPrice),
			new UpgradeItem(UpgradeItemType.ChilliMagnet, 1, surferSettings.ChilliMagnetPrice),

			new UpgradeItem(UpgradeItemType.MotorbikeFuel, 5, surferSettings.MotorbikeFuelPrice),
			new UpgradeItem(UpgradeItemType.MotorbikeGold, 1, surferSettings.MotorbikeGoldPrice),
			new UpgradeItem(UpgradeItemType.MotorbikeMagnet, 1, surferSettings.MotorbikeMagnetPrice),

			new UpgradeItem(UpgradeItemType.WindSurfBoardGold, 1, surferSettings.WindSurfBoardGoldPrice),
			new UpgradeItem(UpgradeItemType.WindSurfBoardMagnet, 1, surferSettings.WindSurfBoardMagnetPrice),

			new UpgradeItem(UpgradeItemType.HeadDefault, 0, 0, Cosmetic.None),
			new UpgradeItem(UpgradeItemType.HeadSticksonCap, 0, 0, Cosmetic.StickmanCap),
            new UpgradeItem(UpgradeItemType.HeadDisguise, 1, surferSettings.HeadDisguisePrice, Cosmetic.Disguise),
            new UpgradeItem(UpgradeItemType.HeadTopHat, 1, surferSettings.HeadTopHatPrice, Cosmetic.TopHat),
            new UpgradeItem(UpgradeItemType.HeadRastaHat, 1, surferSettings.HeadRastaHatPrice, Cosmetic.RastaHat),
            new UpgradeItem(UpgradeItemType.HeadMullet, 1, surferSettings.HeadMulletPrice, Cosmetic.Mullet),
            new UpgradeItem(UpgradeItemType.HeadPaperBag, 1, surferSettings.HeadPaperBagPrice, Cosmetic.PaperBag),
            new UpgradeItem(UpgradeItemType.HeadPumpkin, 1, surferSettings.HeadPumpkinPrice, Cosmetic.PumpkinHead),
            new UpgradeItem(UpgradeItemType.HeadPirateHat, 1, surferSettings.HeadPirateHatPrice, Cosmetic.PirateHat),
            new UpgradeItem(UpgradeItemType.HeadMohawk, 1, surferSettings.HeadMohawkPrice, Cosmetic.Mohawk),
            new UpgradeItem(UpgradeItemType.HeadCowboyHat, 1, surferSettings.HeadCowboyHatPrice, Cosmetic.CowboyHat),
            new UpgradeItem(UpgradeItemType.HeadEmoHair, 1, surferSettings.HeadEmoHairPrice, Cosmetic.EmoHair),

			new UpgradeItem(UpgradeItemType.HeadShark, 1, surferSettings.HeadSharkPrice, Cosmetic.Shark),
			new UpgradeItem(UpgradeItemType.HeadNewspaper, 1, surferSettings.HeadNewspaperPrice, Cosmetic.Newspaper),
			new UpgradeItem(UpgradeItemType.HeadCaptain, 1, surferSettings.HeadCaptainPrice, Cosmetic.Captain),
			new UpgradeItem(UpgradeItemType.HeadDuck, 1, surferSettings.HeadDuckPrice, Cosmetic.Duck),
			new UpgradeItem(UpgradeItemType.HeadSnorkel, 1, surferSettings.HeadSnorkelPrice, Cosmetic.Snorkel),
			new UpgradeItem(UpgradeItemType.HeadPineapple, 1, surferSettings.HeadPineapplePrice, Cosmetic.Pineapple),
			new UpgradeItem(UpgradeItemType.HeadElf, 1, surferSettings.HeadElfPrice, Cosmetic.Elf),
			new UpgradeItem(UpgradeItemType.HeadOctopus, 1, surferSettings.HeadOctopusPrice, Cosmetic.Octopus),
			new UpgradeItem(UpgradeItemType.HeadSwimcap, 1, surferSettings.HeadSwimcapPrice, Cosmetic.Swimcap),
			new UpgradeItem(UpgradeItemType.HeadNekoEars, 1, surferSettings.HeadNekoEarsPrice, Cosmetic.NekoEars),
			new UpgradeItem(UpgradeItemType.HeadCat, 1, surferSettings.HeadCatPrice, Cosmetic.Cat),

		};

		CheckAndStoreMinCostOfNextUpgrade();
	}

	public UpgradeItem UpgradeItemByType(UpgradeItemType itemType)
	{
		return _upgradeItems.SingleOrDefault(x => x.ItemType == itemType);
	}
		
	public string GetPurchasedUpgradeItemsSeparatedString()
	{
		StringBuilder result = new StringBuilder();

		foreach(UpgradeItem item in _upgradeItems)
		{
			if(item.CurrentUpgradeStage > 0)
			{
				result.AppendFormat("{0},{1}|", (int)item.ItemType, item.CurrentUpgradeStage);
			}
		}

		return result.ToString();
	}
		
	public void RestorePurchasedUpgradeItemFromSeparatedString(string separatedString)
	{
		if(string.IsNullOrEmpty(separatedString))
			return;

		string[] upgradeItemsString = separatedString.Split(new char[] { '|' });

		if(upgradeItemsString == null || upgradeItemsString.Length == 0)
			return;

		foreach(string str in upgradeItemsString)
		{
			string[] itemString = str.Split(new char[] { ',' });
			if(!string.IsNullOrEmpty(str) && itemString.Length == 2)
			{
				int upgradeItemTypeId;
				int serverCurrentUpgradeStage;

				if(Int32.TryParse(itemString[0], out upgradeItemTypeId) && Int32.TryParse(itemString[1], out serverCurrentUpgradeStage))
				{
					UpgradeItemType upgradeItemType = (UpgradeItemType)upgradeItemTypeId;
					UpgradeItem upgradeItem = UpgradeItemByType(upgradeItemType);

					if(upgradeItem != null && upgradeItem.CurrentUpgradeStage < serverCurrentUpgradeStage)
					{
						upgradeItem.RestoreToUpgradeStage(serverCurrentUpgradeStage);
					}
				}
			}
		}
	}

	public bool CanAffordItem()
	{
		int currentCash = GameState.SharedInstance.Cash;

		foreach(UpgradeItem item in _upgradeItems)
		{
			int? price = item.GetNextUgpradePrice();

			if(price.HasValue && currentCash >= price.Value)
				return true;
		}

		return false;
	}

	public UpgradeItemType LastUpgradeItemTypePurchased 
	{ 
		get
		{
			int itemId = PlayerPrefs.GetInt("upgrman_lastuitp", 0);
			return (UpgradeItemType)itemId;
		}
		set
		{
			UpgradeItemType itemType = value;
			PlayerPrefs.SetInt("upgrman_lastuitp", (int)itemType);

			if(itemType == UpgradeItemType.None)
				LastUpgradeItemCost = 0;
		}
	}

	public int LastUpgradeItemCost { get; set; }

	public void CheckAndStoreMinCostOfNextUpgrade()
	{
		if(_upgradeItems != null && _upgradeItems.Count > 0)
		{
			int minCost = 0;

			foreach(UpgradeItem item in _upgradeItems)
			{
				int? price = item.GetNextUgpradePrice();

				if(price.HasValue)
				{
					if(minCost == 0 || price.Value < minCost)
					{
						minCost = price.Value;
					}
				}
			}
				
			MinCostOfNextUpgradeItem = minCost;
		}
	}

	public int MinCostOfNextUpgradeItem
	{
		get 
		{
			return PlayerPrefs.GetInt("umMCONUI", 0);
		}
		private set
		{
			PlayerPrefs.SetInt("umMCONUI",value);
		}
	}

	public int GetNicePercentageCostOfNextMinUpgradeItem(float percent)
	{
		int result = (int)(percent * MinCostOfNextUpgradeItem);
		if(result == 0)
			result = 1;

		float roundedTo = 100f;
		if(result <= 2500)
			roundedTo = 10f;
		
		float roundedResult = ((float)result/roundedTo);
		result = (int)roundedResult*(int)roundedTo;


		if((UnityEngine.Random.Range(1,2) % 1) == 0)
			result += 50;

		return result;
	}


	static string upgradeItemTypeEnergyCost = "-1";
	static Dictionary<UpgradeItemType, int> upgradeItemTypeEnergyCostDict;

	public static int? UpgradeItemEnergyDrinkCost(UpgradeItemType upgradeItemType)
	{
		string upgradeItemTypeEnergyCostServerStr = GameServerSettings.SharedInstance.SurferSettings.UpgradeItemTypeIDEnergyDrinkCost;

		LoadUpgradeItemTypeValueDictionary(ref upgradeItemTypeEnergyCostDict, upgradeItemTypeEnergyCost, upgradeItemTypeEnergyCostServerStr);

		if(upgradeItemTypeEnergyCostDict != null && upgradeItemTypeEnergyCostDict.ContainsKey(upgradeItemType))
		{
			return upgradeItemTypeEnergyCostDict[upgradeItemType];
		}
		else
		{
			return null;
		}
	}

	static string upgradeItemTypeLevelReq = "-1";
	static Dictionary<UpgradeItemType, int> upgradeItemTypeLevelReqDict;

	public static int UpgradeItemLevelRequired(UpgradeItemType upgradeItemType)
	{
		string upgradeItemTypeLevelReqServerStr = GameServerSettings.SharedInstance.SurferSettings.UpgradeItemTypeIDLevelReq;

		LoadUpgradeItemTypeValueDictionary(ref upgradeItemTypeLevelReqDict, upgradeItemTypeLevelReq, upgradeItemTypeLevelReqServerStr);

		if(upgradeItemTypeLevelReqDict != null && upgradeItemTypeLevelReqDict.ContainsKey(upgradeItemType))
		{
			return upgradeItemTypeLevelReqDict[upgradeItemType];
		}
		else
		{
			return 0;
		}
	}

	static void LoadUpgradeItemTypeValueDictionary(ref Dictionary<UpgradeItemType, int> upgradeItemTypeValueDict, string commaDelimitedValueString, string commaDelimitedServerString)
	{
		if(upgradeItemTypeValueDict == null)
			upgradeItemTypeValueDict = new Dictionary<UpgradeItemType, int>();

		if(!string.IsNullOrEmpty(commaDelimitedServerString) && commaDelimitedServerString.Length > 1)
		{
			if(commaDelimitedValueString.ToLowerInvariant() != commaDelimitedServerString.ToLowerInvariant())
			{
				commaDelimitedValueString = commaDelimitedServerString;
				upgradeItemTypeValueDict.Clear();

				string[] upgradeItemIDCosts = commaDelimitedValueString.Split(new char[] { '|' });

				foreach(string item in upgradeItemIDCosts)
				{
					string[] itemIDCost = item.Split(new char[] { ',' });
					if(itemIDCost.Length == 2)
					{
						int id;
						int cost;
						if(Int32.TryParse(itemIDCost[0], out id) && Int32.TryParse(itemIDCost[1], out cost))
						{
							if(Enum.IsDefined(typeof(UpgradeItemType), id))
							{
								UpgradeItemType itemType = (UpgradeItemType)id;

								if(!upgradeItemTypeValueDict.ContainsKey(itemType))
								{
									upgradeItemTypeValueDict.Add(itemType, cost);
								}
							}
						}
					}
				}
			}
		}
	}
}
