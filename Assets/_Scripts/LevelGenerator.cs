﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;

#if UNITY_EDITOR

using UnityEditor;

[CustomEditor (typeof(LevelGenerator))]
public class LevelGeneratorEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		LevelGenerator levelGen = (LevelGenerator)target;

		levelGen.powerupManager = EditorGUILayout.ObjectField( "Power up Manager", levelGen.powerupManager, typeof(PowerupManager), true) as PowerupManager;
		levelGen.playerControl = EditorGUILayout.ObjectField( "Player Control", levelGen.playerControl,  typeof(PlayerControl), true) as PlayerControl;
		levelGen.goalManagerUI = EditorGUILayout.ObjectField( "Goal Manager UI", levelGen.goalManagerUI,  typeof(GoalUIManager), true) as GoalUIManager;
		levelGen.gameUI = EditorGUILayout.ObjectField( "Game UI", levelGen.gameUI,  typeof(GameUI), true) as GameUI;
		levelGen.coinBar = EditorGUILayout.ObjectField( "Coin Bar", levelGen.coinBar,  typeof(CoinBar), true) as CoinBar;

		levelGen.isUsingSeed = EditorGUILayout.Toggle( "Using Seed", levelGen.isUsingSeed);


		levelGen.levelLinker = EditorGUILayout.ObjectField( "Level Linker: ",
											levelGen.levelLinker , typeof(LevelLinker), true) as LevelLinker;

		//indoor transitions
		int indoorLocationNum = LocationHelper.GetIndoorNum();
		if (levelGen.indoorTransitionChunks == null)
			levelGen.indoorTransitionChunks = new GameObject[indoorLocationNum];
		if (levelGen.indoorTransitionChunks.Length != indoorLocationNum)
			levelGen.indoorTransitionChunks = new GameObject[indoorLocationNum];


		for (int i = 0, j = 0; i < (int)Location.Count; i++)
		{
			if (!LocationHelper.IsOutdoorLocation((Location)i))
			{
				levelGen.indoorTransitionChunks[j] = EditorGUILayout.ObjectField( LocationTitle.Get((Location)(i)) + " Transition: ",
														levelGen.indoorTransitionChunks[j] , typeof(GameObject), true) as GameObject;
				j++;
			}
		}

		levelGen.shipwreckExitChunk = EditorGUILayout.ObjectField( "Shipwreck Transition EXIT: ",
											levelGen.shipwreckExitChunk , typeof(GameObject), true) as GameObject;


		//Rock Materials
		if (levelGen.rockMaterials == null)
			levelGen.rockMaterials = new Material[(int)RockType.Count];
		if (levelGen.rockMaterials.Length != (int)RockType.Count)
			levelGen.rockMaterials = new Material[(int)RockType.Count];
		
		for (int i = 0; i < (int)RockType.Count; i++)
		{
			levelGen.rockMaterials[i] = EditorGUILayout.ObjectField( RockTitle.Get((RockType)i) + " material",
													levelGen.rockMaterials[i] , typeof(Material), true) as Material;
		}

		//Bird Objects
		if (levelGen.birdObjects == null)
			levelGen.birdObjects = new GameObject[(int)BirdType.Count];
		if (levelGen.birdObjects.Length != (int)BirdType.Count)
			levelGen.birdObjects = new GameObject[(int)BirdType.Count];
		
		for (int i = 0; i < (int)RockType.Count; i++)
		{
			levelGen.birdObjects[i] = EditorGUILayout.ObjectField( BirdTitle.Get((BirdType)i) + " object",
				levelGen.birdObjects[i], typeof(GameObject), true) as GameObject;
		}

		//slaloms
		levelGen.slalomBouyMaterial = EditorGUILayout.ObjectField( "Slalom buoy material",
				levelGen.slalomBouyMaterial , typeof(Material), true) as Material;
		levelGen.slalomTikiMaterial = EditorGUILayout.ObjectField( "Slalom tiki material",
				levelGen.slalomTikiMaterial , typeof(Material), true) as Material;

		//falling rocks
		levelGen.caveFallingRockMaterial = EditorGUILayout.ObjectField( "Cave falling rock material",
				levelGen.caveFallingRockMaterial , typeof(Material), true) as Material;
		levelGen.volcanoFallingRockMaterial = EditorGUILayout.ObjectField( "Volcano falling rock material",
				levelGen.volcanoFallingRockMaterial , typeof(Material), true) as Material;

		levelGen.caveFallingRockCropMaterial = EditorGUILayout.ObjectField( "Cave falling Cull rock material",
				levelGen.caveFallingRockCropMaterial , typeof(Material), true) as Material;
		levelGen.volcanoFallingRockCropMaterial = EditorGUILayout.ObjectField( "Volcano falling Cull rock material",
				levelGen.volcanoFallingRockCropMaterial , typeof(Material), true) as Material;

		levelGen.caveFallingRockShadowMaterial = EditorGUILayout.ObjectField( "Cave falling Shadow material",
				levelGen.caveFallingRockShadowMaterial , typeof(Material), true) as Material;
		levelGen.volcanoFallingRockShadowMaterial = EditorGUILayout.ObjectField( "Volcano falling Shadow material",
				levelGen.volcanoFallingRockShadowMaterial , typeof(Material), true) as Material;


        // shark master
        levelGen.sharkMaster = EditorGUILayout.ObjectField( "Shark Master",
            levelGen.sharkMaster , typeof(Shark), true) as Shark;
        
		//letter masters
		levelGen.levelMastersSize = EditorGUILayout.IntField("Level Masters Size: ", levelGen.levelMastersSize);
		levelGen.levelMastersSize = Mathf.Max(0, levelGen.levelMastersSize);

		if (levelGen.letterMasters == null)
			levelGen.letterMasters = new GameObject[levelGen.levelMastersSize];
		if (levelGen.letterMasters.Length != levelGen.levelMastersSize)
			levelGen.letterMasters = new GameObject[levelGen.levelMastersSize];

		for (int i = 0; i < levelGen.levelMastersSize; i++)
		{
			levelGen.letterMasters[i] = EditorGUILayout.ObjectField(string.Format("Letter {0} master", i),
				levelGen.letterMasters[i], typeof(GameObject), true) as GameObject;
		}

		levelGen.distancetextPopup = EditorGUILayout.ObjectField( "Distance TextPopup", levelGen.distancetextPopup, typeof(TextMeshPro), true) as TextMeshPro;

		levelGen.multiplierText = EditorGUILayout.ObjectField( "Multiplier Text", levelGen.multiplierText, typeof(GameObject), true) as GameObject;
		levelGen.multiplierParticles = EditorGUILayout.ObjectField( "Multiplier Text Particles", levelGen.multiplierParticles, typeof(ParticleSystem), true) as ParticleSystem;
	}
};

#endif

// AKA Carl
public class LevelGenerator : MonoBehaviour 
{
	public PowerupManager powerupManager = null;
	public PlayerControl playerControl = null;
	public GoalUIManager goalManagerUI = null;
	public GameUI gameUI = null;
	public CoinBar coinBar = null;

	public bool isUsingSeed = false;
	public int seed = 5;

	public bool isUsingPiers = false;
	public bool isUsingWoodGrind = false;
	public bool isUsingBoats = false;
	public bool isUsingFallingRocks = false;
	public bool isUsingSlalom = false;
	public bool isUsingFunboxs = false;
	public bool isOnlyUsingSlalom = false;

	public Difficulty curDifficulty = Difficulty.MEDIUM;

	public LevelLinker levelLinker;
	public ChunkPool chunkPool;
	public GameObject[] indoorTransitionChunks;
	public GameObject shipwreckExitChunk;

	public Location curLocation = Location.MainBeach;

	private Location prevIndoorLocation = Location.Undefined;
	private Location prevOutdoorLocation = Location.Undefined;

	public bool loadedAssets = false;
	public float loadProgress = 0.0f;

	// Letter collection
	const float letterMinimumGap = 50f;
	private float lastLetterXPos = -float.MaxValue;
	private int numberOfLettersToCollect = 4;

	public bool[] letterCollectStates;
	public int letterCollectCount = 0;

    public Shark sharkMaster;
	public int levelMastersSize = 4;
    public GameObject[] letterMasters;
    private GameObject[] letterInstances;
    public TextMeshPro distancetextPopup;

    public int timesSuperRewardCollected = 0;

    private float letterAnimTime = 1.0f;
    private float curLetterAnimTime = 0.0f;
    private float letterStayTime = 2.0f;
    private float curLetterStayTime = 0.0f;
	private bool isLettersShowing = false;
	private float inactiveLetterOffset = 5.0f;
	public Vector3[] letterActivePositions;

	public GameObject multiplierText;
	private Vector3 multiplierPosition;
	public ParticleSystem multiplierParticles;
	private bool isLetterCollectAnim = false;
	private float letterCollectAnimTime = 1.0f;
	private float curLetterCollectAnimTime = 0.0f;
	private Vector3[] letterVelocities;

	private float distanceAnimTime = 1.0f;
	private float curDistanceAnimTime = 0.0f;
	private float distanceStayTime = 2.0f;
	private float curDistanceStayTime = 0.0f;
	private bool isDistanceShowing = false;
	private float inactiveDistanceOffset = 5.0f;
	public Vector3 activeDistancePosition;

	private Difficulty difficulty;

	public float lastChunkFinishX = 20f;
	public float standardYScale = 4f;
	public float sectionYPos = 1.1f;

	public int chunksSinceSpecial = 1; //+2 for early start
	public int maxChunksBetweenSpecial = 3;

	public int chunksSinceSwitchLocation = 0;
    public const int minChunksBetweenSwitchLocations = 4;

	public bool isIndoorSection = false;
	public bool aboutToTransition = false;
	public Transform transitionObject = null;

	public int rewardChunksSpawned = 0;
	public float rewardChunkDistance = 300;

	public float generateNewTerrainXTriggerDistance = 10f;

	public const int maxChunksActive = 3;

	public float lastChopperXPos = -float.MaxValue;
	public float lastPowerupXPos = -float.MaxValue;
	public float lastSharkXPos = -float.MaxValue;

	private int currentLetterIndex = 0;

	private string currentSceneName;
	public bool levelCurrentlyLoaded = false;

	public Vector3 playerPosition = new Vector3(0,0,0);

	public float timeToTransition = 0.0f;

	public Material[] rockMaterials;

	public GameObject[] birdObjects; 
	public int maxBirdObjects = 6;
	public GameObject[,] birdObjectPool;
	public int[] birdIndexs;

	//private bool letter_waitingOnGoal = false;
	//private bool letter_waitingOnDistanceMilestone = false;

	//private bool distance_waitingOnGoal = false;
	//private bool distance_waitingOnSURF = false;

	public bool isImmediateTransition = false; 

	public Material slalomBouyMaterial;
	public Material slalomTikiMaterial;

	public Material caveFallingRockMaterial;
	public Material volcanoFallingRockMaterial;

	public Material caveFallingRockCropMaterial;
	public Material volcanoFallingRockCropMaterial;

	public Material caveFallingRockShadowMaterial;
	public Material volcanoFallingRockShadowMaterial;

	private Location _debugLocation = Location.Undefined;
	public Location debugLocation 
	{
		get{ return _debugLocation;}
		set{ _debugLocation = value; }
 	}

	public void LoadLevelAssets()
	{
		multiplierParticles.gameObject.SetActive(false);
		multiplierParticles.Stop();
		//create all pools needed from level linkers

		activeDistancePosition = distancetextPopup.transform.localPosition;
		if (Utils.IphoneXCheck())
			activeDistancePosition += Vector3.down * 0.65f;

		distancetextPopup.transform.localPosition = activeDistancePosition + Vector3.up * inactiveDistanceOffset;



		StartCoroutine("LoadScene");
		ResetLetters();
	}

	public IEnumerator LoadScene()
	{
		loadedAssets = false;

		if (gameUI == null)
			gameUI = FindObjectOfType<GameUI>();

		float lastTimeCheck = Time.time;

		chunkPool = new ChunkPool();
		chunkPool.InitIndexPools(maxChunksActive);

		if (isUsingSeed)// || GameState.IsFirstSession())
		{
			Random.seed = seed;
		}

		if (levelLinker != null)
		{
			chunkPool.FillLevelChunks(this, levelLinker);
			yield return null;
		}
	
        numberOfLettersToCollect = letterMasters.Length;
        letterCollectStates = new bool[letterMasters.Length];
		letterInstances = new GameObject[letterMasters.Length];
		letterActivePositions = new Vector3[letterMasters.Length];

		letterVelocities = new Vector3[letterMasters.Length];

        for (int i = 0; i < numberOfLettersToCollect; i++)
        {
			letterInstances[i] = GameObject.Instantiate(letterMasters[i]) as GameObject;
			letterInstances[i].SetActive(false);

			letterVelocities[i] = Vector3.zero;
			letterActivePositions[i] = letterMasters[i].transform.localPosition;
			if (Utils.IphoneXCheck())
				letterActivePositions[i] += Vector3.down * 0.6f;
        }

		birdObjectPool = new GameObject[ (int)BirdType.Count, maxBirdObjects];
		birdIndexs = new int[(int)BirdType.Count];
		for (int i = 0; i < (int)BirdType.Count; i++)
		{
			for (int j = 0; j < maxBirdObjects; j++)
			{
				birdObjectPool[i, j] = GameObject.Instantiate( birdObjects[i]);
				birdObjectPool[i, j].SetActive(false);
			}
		}

		#if UNITY_EDITOR
		//////Debug.Log("Asset Parse time taken: " + (Time.time - lastTimeCheck));
		#endif

		loadedAssets = true;
		levelCurrentlyLoaded = true;

		bool goalCheck = false;
		Goal[] goals = GoalManager.SharedInstance.goals;
		for (int i = 0; i < 3; i++)
		{
			if (!goals[i].complete)
			{
				if (goals[i].goalType == GoalType.VisitLocation || goals[i].goalType == GoalType.VisitLocation)
				{
					if (LocationHelper.IsOutdoorLocation((Location)goals[i].arg1 ))
					{
						goalCheck = (Random.Range(0, 2) == 0);
						break;
					}
				}
			}
		}

		isImmediateTransition = (Random.Range(0, 5) == 0) || goalCheck;

	}

	public void ResetGame()
	{
		ResetLetters();
		lastLetterXPos = -float.MaxValue;
		timesSuperRewardCollected = 0;
		chunkPool.spawnedIntroChunk = false;

		curLocation = Location.MainBeach;
		prevIndoorLocation = Location.Undefined; 
		prevOutdoorLocation = Location.MainBeach;
		isIndoorSection = false;

		lastChunkFinishX = 20.0f;
		chunksSinceSpecial = 2; //+2 for early start
		chunksSinceSwitchLocation = 0;
		rewardChunksSpawned = 0;

		chunkPool.ResetPool();

		for (int i = 0; i < (int)BirdType.Count; i++)
		{
			birdIndexs[i] = 0;
			for (int j = 0; j < maxBirdObjects; j++)
			{
				birdObjectPool[i, j].SetActive(false);
			}
		}

		for (int i = 0; i < indoorTransitionChunks.Length; i++)
			indoorTransitionChunks[i].SetActive(false);

		shipwreckExitChunk.SetActive(false);

		SetLocationConfigs(curLocation);

		lastChopperXPos = -float.MaxValue;
		lastPowerupXPos = -float.MaxValue;
		lastSharkXPos = -float.MaxValue;

		isImmediateTransition = (Random.Range(0, 5) == 0);

        
	}

	public void SwitchRandomLocation()
	{
		if (isIndoorSection)
			prevIndoorLocation = curLocation;
		else
			prevOutdoorLocation = curLocation;

		isIndoorSection = !isIndoorSection;

		if (debugLocation != Location.Undefined)
		{
			curLocation = debugLocation;
			SetLocationConfigs(curLocation);

			isIndoorSection = !LocationHelper.IsOutdoorLocation((Location)curLocation);
			#if UNITY_EDITOR
			//////Debug.Log(string.Format("DEBUG: Switching to {0}", LocationTitle.Get(curLocation)));
			#endif

			return;
		}

		Goal[] curGoals = GoalManager.SharedInstance.goals;
		Location goalLocation = Location.Undefined;
		for (int i = 0; i < 0; i++)
		{
			if (!curGoals[i].complete)
			{
				if (curGoals[i].goalType == GoalType.SlalomHitOverall ||
					//curGoals[i].goalType == GoalType.SlalomMissedOverall ||
					curGoals[i].goalType == GoalType.SlalomsHit)// ||
					//curGoals[i].goalType == GoalType.SlalomsMissed)
					goalLocation = Location.WavePark;

				if (curGoals[i].goalType == GoalType.VisitLocation ||
					curGoals[i].goalType == GoalType.VisitTotalLocation)
					goalLocation = (Location)curGoals[i].arg1;
			}
		}

		if (goalLocation != Location.Undefined && Random.Range(0, 2) == 0)
		{
			curLocation = goalLocation;
			SetLocationConfigs(curLocation);

			isIndoorSection = !LocationHelper.IsOutdoorLocation((Location)curLocation);
			//////Debug.Log(string.Format("Goal switch: Switching to {0}", LocationTitle.Get(curLocation) ));
			return;
		}


		int newLocation = -1;
		//newLocation = Random.Range(0, (int)Location.Count);

		List<Location> locationPool = new List<Location>();
		for (int i = 0; i < (int)Location.Count; i++)
		{
			Location thisLocation = (Location)i;
			bool isIndoor = !LocationHelper.IsOutdoorLocation(thisLocation);
			if (isIndoorSection && isIndoor && thisLocation != prevIndoorLocation)
			{
				locationPool.Add(thisLocation);
			}
			else if (!isIndoorSection && !isIndoor && thisLocation != prevOutdoorLocation)
			{
				locationPool.Add(thisLocation);
			}
		
		}
		newLocation = (int)locationPool[ Random.Range(0, locationPool.Count) ];

		#if UNITY_EDITOR
		//////Debug.Log( string.Format("Switching Location from {0} to {1}", LocationTitle.Get(curLocation), LocationTitle.Get((Location)newLocation)));
		#endif

		curLocation = (Location)newLocation;
		SetLocationConfigs(curLocation);

	}

	public void SetLocationConfigs(Location location)
	{
		switch(location)
		{
		case Location.MainBeach:
			isUsingSlalom = false;
			isOnlyUsingSlalom = false;
			isUsingPiers = true;
			isUsingWoodGrind = false;
			isUsingFallingRocks = false;
			isUsingBoats = false;
			isUsingFunboxs = false;
			break;

		case Location.BigWaveReef:
			isUsingSlalom = false;
			isOnlyUsingSlalom = false;
			isUsingPiers = false;
			isUsingWoodGrind = false;
			isUsingFallingRocks = false;
			isUsingBoats = false;
			isUsingFunboxs = false;
			break;

		case Location.Cave:
			isUsingSlalom = false;
			isOnlyUsingSlalom = false;
			isUsingPiers = false;
			isUsingWoodGrind = false;
			isUsingFallingRocks = true;
			isUsingBoats = false;
			isUsingFunboxs = false;
			break;

		case Location.WavePark:
			isUsingSlalom = true;
			isOnlyUsingSlalom = true;
			isUsingPiers = false;
			isUsingWoodGrind = false;
			isUsingFallingRocks = false;
			isUsingBoats = false;
			isUsingFunboxs = true;
			break;

		case Location.ShipWreck:
			isUsingSlalom = false;
			isOnlyUsingSlalom = false;
			isUsingPiers = false;
			isUsingWoodGrind = true;
			isUsingFallingRocks = false;
			isUsingBoats = false;
			isUsingFunboxs = false;
			break;

		case Location.Harbor:
			isUsingSlalom = false;
			isOnlyUsingSlalom = false;
			isUsingPiers = false;
			isUsingWoodGrind = false;
			isUsingFallingRocks = false;
			isUsingBoats = true;
			isUsingFunboxs = false;
			break;

		case Location.TikiBay:
			isUsingSlalom = true;
			isOnlyUsingSlalom = false;
			isUsingPiers = false;
			isUsingWoodGrind = false;
			isUsingFallingRocks = true;
			isUsingBoats = false;
			isUsingFunboxs = false;
			break;

		default:
			Debug.LogError( string.Format("Invalid Location Type passed to Location configs {0}", location));

			break;
		}
	}

    public Difficulty GetDifficulty()
    {	
		StickSurferSetting settings = GameServerSettings.SharedInstance.SurferSettings;

		if (playerPosition.x > settings.HardDifficultyDistance)
		{
			if(Random.value > 1.0f - settings.HardDifficulty_MediumChunkChance)
            {
				#if UNITY_EDITOR
				//////Debug.Log("HARD CHUNK");
				#endif

                return Difficulty.HARD;
            }
			#if UNITY_EDITOR
            //////Debug.Log("MEDIUM CHUNK");
            #endif

            return Difficulty.MEDIUM;
		}
		else if(playerPosition.x > settings.MediumDifficultyDistance)
        {
			if(Random.value > 1.0f - settings.MediumDifficulty_MediumChunkChance)
            {
				#if UNITY_EDITOR
                //////Debug.Log("MEDIUM CHUNK");
                #endif
                return Difficulty.MEDIUM;
            }
			if(Random.value > 1.0f - settings.MediumDifficulty_HardChunkChance)
            {
				#if UNITY_EDITOR
                //////Debug.Log("HARD CHUNK");
                #endif

                return Difficulty.HARD;
            }
			#if UNITY_EDITOR
			//////Debug.Log("EASY CHUNK");
			#endif
        	return Difficulty.EASY;
        }
		else if(playerPosition.x > settings.EasyDifficultyDistance)
        {
			if(Random.value > 1.0f - settings.EasyDifficulty_MediumChunkChance)
            {
				#if UNITY_EDITOR
                //////Debug.Log("MEDIUM CHUNK");
                #endif
                return Difficulty.MEDIUM;
            }
			else if(Random.value > 1.0f - settings.EasyDifficulty_HardChunkChance)
            {
				#if UNITY_EDITOR
                //////Debug.Log("HARD CHUNK");
                #endif
                return Difficulty.HARD;
            }
			#if UNITY_EDITOR
			//////Debug.Log("EASY CHUNK");
			#endif
        	return Difficulty.EASY;
        }
        else
        { 
			#if UNITY_EDITOR
        	//////Debug.Log("EASY CHUNK");
        	#endif
        	return Difficulty.EASY;
        }
    }

	public void ReplaceCoinsWithLettersIfRequired(float chunkStartX, Coin[] coins)
    {
    	if (!GlobalSettings.SurfedPassMinTutorialDistance())
    		return;

        if (letterCollectCount < letterMasters.Length)
        {
            if (chunkStartX - lastLetterXPos > letterMinimumGap)
            {
                if (coins.Length > 0)
                {
                    // disable the coin we are using as a marker
                    int randomIndex = Random.Range(0, coins.Length);
                    Vector3 markerPos = coins[randomIndex].transform.position;

                    // check if player already has this letter
                    for (int i = 0; i < letterCollectStates.Length; i++)
                    {
                        if (letterCollectStates[currentLetterIndex] == false)
                        {
                        	break;
                        }
                        currentLetterIndex = (currentLetterIndex + 1) % letterCollectStates.Length;
                    }

					letterInstances[currentLetterIndex].transform.position = markerPos;
					letterInstances[currentLetterIndex].transform.rotation = letterMasters[currentLetterIndex].transform.rotation;
					letterInstances[currentLetterIndex].SetActive(true);
					letterInstances[currentLetterIndex].GetComponent<Renderer>().enabled = true;

					coins[randomIndex].Hit();

					lastLetterXPos = letterInstances[currentLetterIndex].transform.position.x;
                    currentLetterIndex++;
                    if (currentLetterIndex >= letterMasters.Length)
                    {
                        currentLetterIndex = 0;
                    }
                }
            }
        }
    }

    public bool CheckAllLettersCollected()
    {
        for(int i = 0; i < letterCollectStates.Length; i++)
        {
            if (letterCollectStates[i] == false)
                return false;
        }
        return true;
    }

    public void DumpLetterCollectStates()
    {
		#if UNITY_EDITOR
        string statesText = "STATES:";
        for(int i = 0; i < letterCollectStates.Length; i++)
        {
            statesText += " " + letterCollectStates[i];
        }

		//////Debug.Log(statesText);
		#endif
    }

	public void CollectLetters()
	{
		if (!isLetterCollectAnim)
		{
			curLetterAnimTime = letterAnimTime;
			curLetterCollectAnimTime = letterCollectAnimTime;
			isLetterCollectAnim = true;
			isLettersShowing = true;

			curLetterAnimTime = letterAnimTime;
			curLetterStayTime = letterStayTime;

			//letter_waitingOnGoal = goalManagerUI.IsShowingGoalPopup();
			//letter_waitingOnDistanceMilestone = IsShowingDistancePopup();

			for (int i = 0; i < letterVelocities.Length; i++)
			{
				letterMasters[i].transform.localPosition = letterActivePositions[i] + Vector3.up * inactiveLetterOffset;
				letterVelocities[i] = (Vector3.up + Vector3.right * Random.Range(-1f, 1f)).normalized * 0.2f;
				letterMasters[i].transform.rotation = Quaternion.AngleAxis( Random.Range(-10.0f, 10.0f), Vector3.forward);
			}
		}

		GiveSuperCoinReward();
	}

	public void GiveSuperCoinReward()
	{
		int coinReward = GameState.SharedInstance.GetSuperRewardStepUp(timesSuperRewardCollected + 1);

		if(playerControl.StartingVehicle == Vehicle.GoldSurfboard)
			coinReward = coinReward * 2;

		gameUI.ShowSuperRewardText(SpecialReward.Coins, coinReward);
		playerControl.Coins += coinReward;

		gameUI.UpdateCoinTotal(playerControl.Coins);

		GameState.SharedInstance.AddCash(coinReward);
		GoalManager.SharedInstance.UpdateCoins(coinReward, powerupManager.GetCurrentVehicle());

		timesSuperRewardCollected++;
	}

	public void ResetLetters()
	{
		for (int i = 0; i < letterCollectStates.Length; i++)
        {
            letterCollectStates[i] = false;    
        }
		letterCollectCount = 0;

		for (int i = 0; i < letterMasters.Length; i++)
		{
			letterMasters[i].transform.rotation  = Quaternion.identity;
			letterMasters[i].gameObject.SetActive(false);
			letterMasters[i].gameObject.GetComponent<Collider>().enabled = true;
			letterMasters[i].GetComponent<Renderer>().enabled = true;

			if (letterInstances != null)
				if (letterInstances[i] != null)
					letterInstances[i].SetActive(false);

		}
	}

	public void UpdateLevel(Vector3 playerPosition)
	{
		if(!loadedAssets)
			return;
		
        this.playerPosition = playerPosition;
		if (playerPosition.x > lastChunkFinishX - generateNewTerrainXTriggerDistance)
		{
			chunkPool.GetChunk();
		}
	}

	public Transform GetTransitionChunk()
	{
		Location checkLocation;
		if (isIndoorSection)
		{
			checkLocation = curLocation;
		}
		else
		{
			checkLocation = prevIndoorLocation;
			if (checkLocation == Location.ShipWreck)
				return shipwreckExitChunk.transform;
		}
		

		if (debugLocation != Location.Undefined)
			return indoorTransitionChunks[0].transform;

		aboutToTransition = true;

		for (int i = 0, j = 0; i < (int)(Location.Count); i++)
		{
			if (!LocationHelper.IsOutdoorLocation((Location)i))
			{
				if ((Location)i == checkLocation)
				{
					return indoorTransitionChunks[j].transform;
				}
				j++;
			}
		}
		return null;
	}

	void Update()
	{

		#if UNITY_EDITOR
		if (Input.GetKey(KeyCode.Alpha1))
			debugLocation = Location.MainBeach;
		else if (Input.GetKey(KeyCode.Alpha2))
			debugLocation = Location.BigWaveReef;
		else if (Input.GetKey(KeyCode.Alpha3))
			debugLocation = Location.TikiBay;
		else if (Input.GetKey(KeyCode.Alpha4))
			debugLocation = Location.Cave;
		else if (Input.GetKey(KeyCode.Alpha5))
			debugLocation = Location.WavePark;
		else if (Input.GetKey(KeyCode.Alpha6))
			debugLocation = Location.ShipWreck;
		else if (Input.GetKey(KeyCode.Alpha7))
			debugLocation = Location.Harbor;
		#endif

		if (timeToTransition >= 0)
			timeToTransition -= Time.deltaTime;
		
		if (curLetterAnimTime > 0)
		{
			//if (!(letter_waitingOnGoal || letter_waitingOnDistanceMilestone) || !isLettersShowing)
			//{
				float lerpAmount = curLetterAnimTime / letterAnimTime;

				curLetterAnimTime -= Time.deltaTime;
				if (curLetterAnimTime <= 0)
					lerpAmount = 0;

				for (int i = 0; i < letterInstances.Length; i++)
				{
					if (letterMasters[i].activeSelf && letterCollectStates[i])
					{
						if (isLettersShowing)
						{
							letterMasters[i].transform.localPosition = Vector3.Lerp(letterActivePositions[i],
																					letterActivePositions[i] + Vector3.up * inactiveLetterOffset,
																					Mathf.Pow(lerpAmount, 3.0f));
						}
						else
						{
							letterMasters[i].transform.localPosition = Vector3.Lerp(letterActivePositions[i],
																					letterActivePositions[i] + Vector3.up * inactiveLetterOffset,
																					1.0f - Mathf.Pow(lerpAmount, 3.0f));
						}
					}
				}
			/*}
			else
			{
				if (letter_waitingOnGoal)
					letter_waitingOnGoal = goalManagerUI.IsShowingGoalPopup();

				if (letter_waitingOnDistanceMilestone)
					letter_waitingOnDistanceMilestone = IsShowingDistancePopup();
			}*/
		}
		else if (curLetterStayTime > 0 && !isLetterCollectAnim)
		{
			curLetterStayTime -= Time.deltaTime;
			if (curLetterStayTime <= 0 && isLettersShowing)
			{
				curLetterAnimTime = letterAnimTime;
				isLettersShowing = false;
			}
		}

		else if (curLetterCollectAnimTime > 0)
		{
			
			curLetterCollectAnimTime -= Time.deltaTime;
			if (curLetterCollectAnimTime <= 0)
			{
				ResetLetters();
				isLetterCollectAnim = false;

				/*
				multiplierParticles.gameObject.SetActive(true);
				multiplierParticles.Stop();
				multiplierParticles.time = 0;
				multiplierParticles.Play();
				*/
				//gameUI.AddMultiplier(1.0f);

				for (int i = 0; i < letterMasters.Length; i++)
				{
					letterMasters[i].SetActive(false);
				}
			}

			for (int i = 0; i < letterMasters.Length; i++)
			{
				letterVelocities[i] += Vector3.down * 1.0f * Time.deltaTime;
				letterMasters[i].transform.localPosition += letterVelocities[i];
			}

			/*lerpAmount = 1.0f - lerpAmount;

			int letterIndexToMove = Mathf.Min( (int)(lerpAmount * 4), 3);
			lerpAmount = lerpAmount * 4 - (float)letterIndexToMove;

			letterMasters[letterIndexToMove].transform.localPosition = Vector3.Lerp(letterActivePositions[letterIndexToMove],
																					multiplierPosition,
																					Mathf.Pow(lerpAmount, 3.0f));
			if (letterIndexToMove > 0)
			{
				if (letterMasters[letterIndexToMove -1 ].transform.localPosition != multiplierPosition)
					letterMasters[letterIndexToMove -1 ].transform.localPosition = multiplierPosition;
				else
					letterMasters[letterIndexToMove -1 ].SetActive(false);
			}*/
		}

		if (curDistanceAnimTime > 0)
		{	
			//do somthing later
			//if (!(distance_waitingOnGoal || distance_waitingOnSURF) || !isDistanceShowing)
			//{
				float lerpAmount = curDistanceAnimTime / distanceAnimTime;

				curDistanceAnimTime -= Time.deltaTime;
				if (curDistanceAnimTime <= 0)
					lerpAmount = 0;

				if (isDistanceShowing)
				{
					distancetextPopup.transform.localPosition = Vector3.Lerp(activeDistancePosition,
																			activeDistancePosition + Vector3.up * inactiveDistanceOffset,
																			Mathf.Pow(lerpAmount, 3.0f));
				}
				else
				{
					distancetextPopup.transform.localPosition = Vector3.Lerp(activeDistancePosition,
																			activeDistancePosition + Vector3.up * inactiveDistanceOffset,
																			1.0f - Mathf.Pow(lerpAmount, 3.0f));
				}
			/*}
			else
			{
				if (distance_waitingOnGoal)
					distance_waitingOnGoal = goalManagerUI.IsShowingGoalPopup();

				if (distance_waitingOnSURF)
					distance_waitingOnSURF = IsShowingLetterPopup();
			}*/
		}
		else if (curDistanceStayTime > 0)
		{
			curDistanceStayTime -= Time.deltaTime;
			if (curDistanceStayTime <= 0 && isDistanceShowing)
			{
				curDistanceAnimTime = distanceAnimTime;
				isDistanceShowing = false;
			}
		}
	}

	public void ShowLetters()
	{
		curLetterAnimTime = letterAnimTime;
		isLettersShowing = true;
		curLetterStayTime = letterStayTime;

		//letter_waitingOnGoal = goalManagerUI.IsShowingGoalPopup();
		//letter_waitingOnDistanceMilestone = IsShowingDistancePopup();
	}

	public void ActivateLetter(int letterIndex)
	{
		letterCollectStates[letterIndex] = true;
		letterMasters[letterIndex].gameObject.SetActive(true);
		letterMasters[letterIndex].transform.localPosition = letterActivePositions[letterIndex] + Vector3.up * inactiveLetterOffset;
	}

	public void ShowDistanceMarker(float distance)
	{
		curDistanceAnimTime = distanceAnimTime;
		isDistanceShowing = true;
		curDistanceStayTime = distanceStayTime;

		distancetextPopup.text = string.Format("{0}m", (int)distance);

		//distance_waitingOnGoal = goalManagerUI.IsShowingGoalPopup();
		//distance_waitingOnSURF = IsShowingLetterPopup();
	}

	public bool IsShowingLetterPopup()
	{
		return ( curLetterAnimTime > 0 || curLetterStayTime > 0);
	}

	public bool IsShowingDistancePopup()
	{
		return ( curDistanceAnimTime > 0 || curDistanceStayTime > 0);
	}
}

public class ChunkAssetFlags
{
	public bool hasChopper = false;
	public bool hasSpecialPickup = false;
	public bool hasShark = false;
}

public class ChunkPool
{
	private LevelGenerator levelGen = null;
	public bool spawnedIntroChunk = false;

	public Transform[] easyChunks;
	public Transform[] mediumChunks;
	public Transform[] hardChunks;

	public ChunkAssetFlags[] easyChunkFlags;
	public ChunkAssetFlags[] mediumChunkFlags;
	public ChunkAssetFlags[] hardChunkFlags;

	public Transform[] easySlalomChunks;
	public Transform[] mediumSlalomChunks;
	public Transform[] hardSlalomChunks;

	public ChunkAssetFlags[] easySlalomChunkFlags;
	public ChunkAssetFlags[] mediumSlalomChunkFlags;
	public ChunkAssetFlags[] hardSlalomChunkFlags;

	public Transform[] introChunks;
	public ChunkAssetFlags[] introChunkFlags;

	public Transform[] pierChunks;
	public Transform[] woodChunks;
	public Transform[] boatChunks;
	public Transform[] fallingRockChunks;
	public Transform[] funboxChunks;

	public ChunkAssetFlags[] pierChunkFlags;
	public ChunkAssetFlags[] woodChunkFlags;
	public ChunkAssetFlags[] boatChunkFlags;
	public ChunkAssetFlags[] fallingRockChunkFlags;
	public ChunkAssetFlags[] funboxChunkFlags;

	public Transform[] planeChunks;
	public Transform[] rewardChunks;
	public Transform[] superRewardChunks;

	public ChunkAssetFlags[] planeChunkFlags;
	public ChunkAssetFlags[] rewardChunkFlags;
	public ChunkAssetFlags[] superRewardChunkFlags;


	private int curIntroChunkIndex = 0;
	private int truncIntroChunkIndex = -1;
	private int[] chunkIntroIndexsActive = null;

	private int curEasyChunkIndex = 0;
	private int truncEasyChunkIndex = -1;
	private int[] chunkEasyIndexsActive = null;

	private int curMediumChunkIndex = 0;
	private int truncMediumChunkIndex = -1;
	private int[] chunkMediumIndexsActive = null;

	private int curHardChunkIndex = 0;
	private int truncHardChunkIndex = -1;
	private int[] chunkHardIndexsActive = null;

	private int curEasySlalomChunkIndex = 0;
	private int truncEasySlalomChunkIndex = -1;
	private int[] chunkEasySlalomIndexsActive = null;

	private int curMediumSlalomChunkIndex = 0;
	private int truncMediumSlalomChunkIndex = -1;
	private int[] chunkMediumSlalomIndexsActive = null;

	private int curHardSlalomChunkIndex = 0;
	private int truncHardSlalomChunkIndex = -1;
	private int[] chunkHardSlalomIndexsActive = null;

	private int curPierChunkIndex = 0;
	private int truncPierChunkIndex = -1;
	private int[] chunkPierIndexsActive = null;

	private int curBoatChunkIndex = 0;
	private int truncBoatChunkIndex = -1;
	private int[] chunkBoatIndexsActive = null;

	private int curWoodGrindChunkIndex = 0;
	private int truncWoodGrindChunkIndex = -1;
	private int[] chunkWoodGrindIndexsActive = null;

	private int curFallingRockChunkIndex = 0;
	private int truncFallingRockChunkIndex = -1;
	private int[] chunkFallingRockIndexsActive = null;

	private int curFunboxChunkIndex = 0;
	private int truncFunboxChunkIndex = -1;
	private int[] chunkFunboxIndexsActive = null;

	private int curRewardChunkIndex = 0;
	private int truncRewardChunkIndex = -1;
	private int[] chunkRewardIndexsActive = null;

	private int curSuperRewardChunkIndex = 0;
	private int truncSuperRewardChunkIndex = -1;
	private int[] chunkSuperRewardIndexsActive = null;

	private int curPlaneChunkIndex = 0;
	private int truncPlaneChunkIndex = -1;
	private int[] chunkPlaneIndexsActive = null;


	public void ResetPool(int maxChunksActive = 3)
	{
		if (easyChunks != null)
		{
			for (int i = 0; i < easyChunks.Length; i++)
				easyChunks[i].gameObject.SetActive(false);
		}

		if (mediumChunks != null)
		{
			for (int i = 0; i < mediumChunks.Length; i++)
				mediumChunks[i].gameObject.SetActive(false);
		}

		if (hardChunks != null)
		{
			for (int i = 0; i < hardChunks.Length; i++)
				hardChunks[i].gameObject.SetActive(false);
		}

		if (easySlalomChunks != null)
		{
			for (int i = 0; i < easySlalomChunks.Length; i++)
				easySlalomChunks[i].gameObject.SetActive(false);
		}

		if (mediumSlalomChunks != null)
		{
			for (int i = 0; i < mediumSlalomChunks.Length; i++)
				mediumSlalomChunks[i].gameObject.SetActive(false);
		}

		if (hardSlalomChunks != null)
		{
			for (int i = 0; i < hardSlalomChunks.Length; i++)
				hardSlalomChunks[i].gameObject.SetActive(false);
		}

		if (pierChunks != null)
		{
			for (int i = 0; i < pierChunks.Length; i++)
				pierChunks[i].gameObject.SetActive(false);
		}

		if (woodChunks != null)
		{
			for (int i = 0; i < woodChunks.Length; i++)
				woodChunks[i].gameObject.SetActive(false);
		}

		if (boatChunks != null)
		{
			for (int i = 0; i < boatChunks.Length; i++)
				boatChunks[i].gameObject.SetActive(false);
		}

		if (fallingRockChunks != null)
		{
			for (int i = 0; i < fallingRockChunks.Length; i++)
				fallingRockChunks[i].gameObject.SetActive(false);
		}

		if (funboxChunks != null)
		{
			for (int i = 0; i < funboxChunks.Length; i++)
				funboxChunks[i].gameObject.SetActive(false);
		}

		if (rewardChunks != null)
		{
			for (int i = 0; i < rewardChunks.Length; i++)
				rewardChunks[i].gameObject.SetActive(false);
		}

		if (superRewardChunks != null)
		{
			for (int i = 0; i < superRewardChunks.Length; i++)
				superRewardChunks[i].gameObject.SetActive(false);
		}

		if (introChunks != null)
		{
			for (int i = 0; i < introChunks.Length; i++)
				introChunks[i].gameObject.SetActive(false);
		}

		if (planeChunks != null)
		{
			for (int i = 0; i < planeChunks.Length; i++)
				planeChunks[i].gameObject.SetActive(false);
		}

			
		truncIntroChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkIntroIndexsActive[i] = -1;
		
		truncEasyChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkEasyIndexsActive[i] = -1;

		truncMediumChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkMediumIndexsActive[i] = -1;

		truncHardChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkHardIndexsActive[i] = -1;

		truncEasySlalomChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkEasySlalomIndexsActive[i] = -1;

		truncMediumSlalomChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkMediumSlalomIndexsActive[i] = -1;

		truncHardSlalomChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkHardSlalomIndexsActive[i] = -1;

		truncPierChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkPierIndexsActive[i] = -1;

		truncBoatChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkBoatIndexsActive[i] = -1;

		truncWoodGrindChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkWoodGrindIndexsActive[i] = -1;

		truncFallingRockChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkFallingRockIndexsActive[i] = -1;

		truncFunboxChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkFunboxIndexsActive[i] = -1;

		truncRewardChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkRewardIndexsActive[i] = -1;

		truncSuperRewardChunkIndex = -1;
		for (int i = 0; i < maxChunksActive; i++)
			chunkSuperRewardIndexsActive[i] = -1;
		
	}

	public void InitIndexPools(int maxChunksActive = 3)
	{
		if (chunkIntroIndexsActive == null)
		{
			chunkIntroIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkIntroIndexsActive[i] = -1;
		}

		if (chunkEasyIndexsActive == null)
		{
			chunkEasyIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkEasyIndexsActive[i] = -1;
		}

		if (chunkMediumIndexsActive == null)
		{
			chunkMediumIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkMediumIndexsActive[i] = -1;
		}

		if (chunkHardIndexsActive == null)
		{
			chunkHardIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkHardIndexsActive[i] = -1;
		}

		if (chunkEasySlalomIndexsActive == null)
		{
			chunkEasySlalomIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkEasySlalomIndexsActive[i] = -1;
		}

		if (chunkMediumSlalomIndexsActive == null)
		{
			chunkMediumSlalomIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkMediumSlalomIndexsActive[i] = -1;
		}

		if (chunkHardSlalomIndexsActive == null)
		{
			chunkHardSlalomIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkHardSlalomIndexsActive[i] = -1;
		}

		if (chunkPierIndexsActive == null)
		{
			chunkPierIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkPierIndexsActive[i] = -1;
		}

		if (chunkPierIndexsActive == null)
		{
			chunkPierIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkPierIndexsActive[i] = -1;
		}

		if (chunkBoatIndexsActive == null)
		{
			chunkBoatIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkBoatIndexsActive[i] = -1;
		}

		if (chunkWoodGrindIndexsActive == null)
		{
			chunkWoodGrindIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkWoodGrindIndexsActive[i] = -1;
		}

		if (chunkFallingRockIndexsActive == null)
		{
			chunkFallingRockIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkFallingRockIndexsActive[i] = -1;
		}

		if (chunkFunboxIndexsActive == null)
		{
			chunkFunboxIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkFunboxIndexsActive[i] = -1;
		}


		if (chunkPlaneIndexsActive == null)
		{
			chunkPlaneIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkPlaneIndexsActive[i] = -1;
		}

		if (chunkRewardIndexsActive == null)
		{
			chunkRewardIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkRewardIndexsActive[i] = -1;
		}

		if (chunkSuperRewardIndexsActive == null)
		{
			chunkSuperRewardIndexsActive = new int[maxChunksActive];
			for (int i = 0; i < maxChunksActive; i++)
				chunkSuperRewardIndexsActive[i] = -1;
		}

	}

	private ChunkAssetFlags CheckForAssets(GameObject chunk)
	{
		ChunkAssetFlags flags = new ChunkAssetFlags();
		if (chunk.GetComponentInChildren<Chopper>() != null)
			flags.hasChopper = true;
		if (chunk.GetComponentInChildren<SharkMarker>() != null)
			flags.hasShark = true;
		if (chunk.GetComponentInChildren<SpecialPickup>() != null)
			flags.hasSpecialPickup = true;

		return flags;
	}

	public void FillLevelChunks(LevelGenerator _levelGen, LevelLinker linker)
	{
		levelGen = _levelGen;

		///EASY CHUNKS
		if (linker.easyChunks != null)
		{	
			easyChunks = new Transform[linker.easyChunks.childCount];
			easyChunkFlags = new ChunkAssetFlags[linker.easyChunks.childCount];
			int easyChunkIndex = 0;
			foreach(Transform child in linker.easyChunks)
			{
				easyChunkFlags[easyChunkIndex] = CheckForAssets(child.gameObject);
				easyChunks[easyChunkIndex] = child;
				child.gameObject.SetActive(false);
				easyChunkIndex++;
			}
		}

		if (linker.slalomEasyChunks != null)
		{	
			easySlalomChunks = new Transform[linker.slalomEasyChunks.childCount];
			easySlalomChunkFlags = new ChunkAssetFlags[linker.slalomEasyChunks.childCount];

			int easyChunkSlalomIndex = 0;
			foreach(Transform child in linker.slalomEasyChunks)
			{
				easySlalomChunkFlags[easyChunkSlalomIndex] = CheckForAssets(child.gameObject);
				easySlalomChunks[easyChunkSlalomIndex] = child;
				child.gameObject.SetActive(false);
				easyChunkSlalomIndex++;

			}
		}

		///MEDIUM CHUNKS
		if (linker.mediumChunks != null)
		{
			mediumChunks = new Transform[linker.mediumChunks.childCount];
			mediumChunkFlags = new ChunkAssetFlags[linker.mediumChunks.childCount];

			int mediumChunkIndex = 0;
			foreach(Transform child in linker.mediumChunks)
			{
				mediumChunkFlags[mediumChunkIndex]  = CheckForAssets(child.gameObject);
				mediumChunks[mediumChunkIndex] = child;
				child.gameObject.SetActive(false);
				mediumChunkIndex++;
			}
		}

		if (linker.slalomMediumChunks != null)
		{
			mediumSlalomChunks = new Transform[linker.slalomMediumChunks.childCount];
			mediumSlalomChunkFlags = new ChunkAssetFlags[linker.slalomMediumChunks.childCount];

			 
			int mediumChunkSlalomIndex = 0;
			foreach(Transform child in linker.slalomMediumChunks)
			{
				mediumSlalomChunkFlags[mediumChunkSlalomIndex] = CheckForAssets(child.gameObject);
				mediumSlalomChunks[mediumChunkSlalomIndex] = child;
				child.gameObject.SetActive(false);
				mediumChunkSlalomIndex++;

			}
		}

		//HARD CHUNKS
		if (linker.hardChunks != null)
		{
			hardChunks = new Transform[linker.hardChunks.childCount];
			hardChunkFlags = new ChunkAssetFlags[linker.hardChunks.childCount];

			int hardChunkIndex = 0;
			foreach(Transform child in linker.hardChunks)
			{
				hardChunkFlags[hardChunkIndex] = CheckForAssets(child.gameObject);
				hardChunks[hardChunkIndex] = child;
				child.gameObject.SetActive(false);
				hardChunkIndex++;

			}
		}

		if (linker.slalomHardChunks != null)
		{
			hardSlalomChunks = new Transform[linker.slalomHardChunks.childCount];
			hardSlalomChunkFlags = new ChunkAssetFlags[linker.slalomHardChunks.childCount];

			int hardSlalomChunkIndex = 0;
			foreach(Transform child in linker.slalomHardChunks)
			{
				hardSlalomChunkFlags[hardSlalomChunkIndex] = CheckForAssets(child.gameObject);
				hardSlalomChunks[hardSlalomChunkIndex] = child;
				child.gameObject.SetActive(false);
				hardSlalomChunkIndex++;
			}
		}

		if (linker.introChunks != null)
		{
			introChunks = new Transform[linker.introChunks.childCount];
			introChunkFlags = new ChunkAssetFlags[linker.introChunks.childCount];

			int chunkIndex = 0;
			foreach (Transform child in linker.introChunks)
			{
				introChunkFlags[chunkIndex] = CheckForAssets(child.gameObject);
				introChunks[chunkIndex] = child;
				child.gameObject.SetActive(false);
				chunkIndex++;
			}
		}


		//SPECIAL CHUNKS
		if (linker.pierChunks != null)
		{
			pierChunks = new Transform[linker.pierChunks.childCount];
			pierChunkFlags = new ChunkAssetFlags[linker.pierChunks.childCount];

			int pierIndex = 0;
			foreach(Transform child in linker.pierChunks)
			{
				pierChunkFlags[pierIndex] = CheckForAssets(child.gameObject);
				pierChunks[pierIndex] = child;
				child.gameObject.SetActive(false);
				pierIndex++;
			}
		}
			
		if (linker.boatChunks != null)
		{
			boatChunks = new Transform[linker.boatChunks.childCount];
			boatChunkFlags = new ChunkAssetFlags[linker.boatChunks.childCount];

			int grindIndex = 0;
			foreach(Transform child in linker.boatChunks)
			{
				boatChunkFlags[grindIndex] = CheckForAssets(child.gameObject);
				boatChunks[grindIndex] = child;
				child.gameObject.SetActive(false);
				grindIndex++;
			}
		}

		if (linker.fallingRockChunks != null)
		{
			fallingRockChunks = new Transform[linker.fallingRockChunks.childCount];
			fallingRockChunkFlags = new ChunkAssetFlags[linker.fallingRockChunks.childCount];

			int grindIndex = 0;
			foreach(Transform child in linker.fallingRockChunks)
			{
				fallingRockChunkFlags[grindIndex] = CheckForAssets(child.gameObject);
				fallingRockChunks[grindIndex] = child;
				child.gameObject.SetActive(false);
				grindIndex++;
			}
		}

		if (linker.woodChunks != null)
		{
			woodChunks = new Transform[linker.woodChunks.childCount];
			woodChunkFlags = new ChunkAssetFlags[linker.woodChunks.childCount];
			int grindIndex = 0;
			foreach(Transform child in linker.woodChunks)
			{
				woodChunkFlags[grindIndex] = CheckForAssets(child.gameObject);
				woodChunks[grindIndex] = child;
				child.gameObject.SetActive(false);
				grindIndex++;
			}
		}

		if (linker.funboxChunks != null)
		{
			funboxChunks = new Transform[linker.funboxChunks.childCount];
			funboxChunkFlags = new ChunkAssetFlags[linker.funboxChunks.childCount];

			int grindIndex = 0;
			foreach(Transform child in linker.funboxChunks)
			{
				funboxChunkFlags[grindIndex] = CheckForAssets(child.gameObject);
				funboxChunks[grindIndex] = child;
				child.gameObject.SetActive(false);
				grindIndex++;
			}
		}


		if (linker.planeChunks != null)
		{
			planeChunks = new Transform[linker.planeChunks.childCount];
			planeChunkFlags = new ChunkAssetFlags[linker.planeChunks.childCount];

			int planeIndex = 0;
			foreach (Transform child in linker.planeChunks)
			{
				planeChunkFlags[planeIndex] = CheckForAssets(child.gameObject);
				planeChunks[planeIndex] = child;
				child.gameObject.SetActive(false);
				planeIndex++;
			}
		}

		//REWARD CHUNKS
		if (linker.rewardChunks != null)
		{
			rewardChunks = new Transform[linker.rewardChunks.childCount];
			rewardChunkFlags = new ChunkAssetFlags[linker.rewardChunks.childCount];

			int rewardIndex = 0;
			foreach (Transform child in linker.rewardChunks)
			{
				rewardChunkFlags[rewardIndex] = CheckForAssets(child.gameObject);
				rewardChunks[rewardIndex] = child;
				child.gameObject.SetActive(false);
				rewardIndex++;
			}
		}

		if (linker.superRewardChunks != null)
		{
			superRewardChunks = new Transform[linker.superRewardChunks.childCount];
			superRewardChunkFlags = new ChunkAssetFlags[linker.superRewardChunks.childCount];
			int rewardIndex = 0;
			foreach (Transform child in linker.superRewardChunks)
			{
				superRewardChunkFlags[rewardIndex] = CheckForAssets(child.gameObject);
				superRewardChunks[rewardIndex] = child;
				child.gameObject.SetActive(false);
				rewardIndex++;
			}
		}

	}

	public void GetChunk()
	{
		Transform newChunk;

		if (levelGen.powerupManager.CheckForPowerup(PowerUp.Plane))
			newChunk = GetPlaneChunk();
		else
			newChunk = GetNewChunk();

		//update x position
		Transform chunkLength = newChunk.Find("ChunkLength");
		float sectionWidth = chunkLength.localScale.x;

		if(chunkLength.localScale.y > levelGen.standardYScale)
			levelGen.sectionYPos = levelGen.sectionYPos + (chunkLength.transform.localScale.y - levelGen.standardYScale);

		float startX = levelGen.lastChunkFinishX + sectionWidth / 2.0f;
		levelGen.lastChunkFinishX += sectionWidth;

		chunkLength.gameObject.GetComponent<Renderer>().enabled = false;

		//place it and activate it
		newChunk.position = new Vector3(startX, levelGen.sectionYPos, newChunk.position.z);
		newChunk.gameObject.SetActive(true);


		//reset all assets on the chunks
		//birds
		BirdMarker[] birdMarkers = newChunk.gameObject.GetComponentsInChildren<BirdMarker>();
		for (int i = 0; i < birdMarkers.Length; i++)
		{
			//birds[i].ResetBird();

			birdMarkers[i].TurnOffRenderers();

			GameObject newBird  = null;
			int birdindex = 0;
			switch(levelGen.curLocation)
			{
				case Location.Harbor:
				case Location.ShipWreck:
					birdindex = (int)BirdType.Seagull;	break;
				case Location.WavePark:
					birdindex = (int)BirdType.Drone;	break;
				case Location.Cave:
					birdindex = (int)BirdType.Bat;	break;
				default:
					birdindex = (int)BirdType.Basic;	break;
			};

			newBird = levelGen.birdObjectPool[birdindex,  levelGen.birdIndexs[birdindex]];
			newBird.transform.position = birdMarkers[i].transform.position;
			newBird.GetComponent<Bird>().ResetBird();
			newBird.SetActive(true);

			levelGen.birdIndexs[birdindex] = (levelGen.birdIndexs[birdindex] + 1) % levelGen.maxBirdObjects;

		}

		Rock[] rocks = newChunk.gameObject.GetComponentsInChildren<Rock>();
		for (int i = 0; i < rocks.Length; i++)
		{
			rocks[i].ResetRock();

			switch(levelGen.curLocation)
			{
				case Location.Harbor:
					rocks[i].SetMaterial( levelGen.rockMaterials[(int)RockType.Barrel]);
					rocks[i].ToggleBobbing(true);
					break;
				case Location.ShipWreck:
					rocks[i].SetMaterial( levelGen.rockMaterials[(int)RockType.Flotsam]);
					rocks[i].ToggleBobbing(false);
					break;
				case Location.TikiBay:
					rocks[i].SetMaterial( levelGen.rockMaterials[(int)RockType.Volcanic]);
					rocks[i].ToggleBobbing(false);
					break;
				default:
					rocks[i].SetMaterial( levelGen.rockMaterials[(int)RockType.Basic]);
					rocks[i].ToggleBobbing(false);
					break;
			};
		}

        SharkMarker[] sharkMarkers = newChunk.gameObject.GetComponentsInChildren<SharkMarker>(true);
        if(sharkMarkers.Length > 0)
        {
			if (levelGen.playerPosition.x - levelGen.lastSharkXPos < GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenSharks)
			{
				for (int i = 0; i < sharkMarkers.Length; i++)
        		{
					sharkMarkers[0].gameObject.SetActive(false);
        		}
			}
			else
			{
				for (int i = 0; i < sharkMarkers.Length; i++)
        		{
		            if(!levelGen.sharkMaster.IsSharkVisibleOnScreen())
		            {
		                sharkMarkers[0].gameObject.SetActive(true);
		                levelGen.sharkMaster.ResetShark(levelGen.playerControl.transform.position, true);
						levelGen.lastSharkXPos = sharkMarkers[i].transform.position.x;
						levelGen.playerControl.playerAnimation.SetEmotion(Emotion.Mad, 5.0f);
		            }
		            else
		            {
		                sharkMarkers[0].gameObject.SetActive(false);
		            }
				}
            }
        }

		//coins
		Coin[] coins = newChunk.gameObject.GetComponentsInChildren<Coin>();
		for (int i = 0; i < coins.Length; i++)
		{
			coins[i].Initialise(levelGen.powerupManager, levelGen.playerControl);
			coins[i].ResetCoin();
		}

		//crates
		Crate[] crates = newChunk.gameObject.GetComponentsInChildren<Crate>();
		for (int i = 0; i < crates.Length; i++)
		{
			crates[i].ResetCrate();
		}

        /*
		//sharks
		Shark[] sharks = newChunk.gameObject.GetComponentsInChildren<Shark>();
		for (int i = 0; i < sharks.Length; i++)
		{
			sharks[i].ResetShark();
		}
        */

		//Slalom
		SlalomGate[] slaloms = newChunk.gameObject.GetComponentsInChildren<SlalomGate>();
		for (int i = 0; i < slaloms.Length; i++)
		{
			slaloms[i].ResetSlalom(levelGen.playerControl);

			if (levelGen.curLocation == Location.TikiBay)
			{
				slaloms[i].SetMaterial(levelGen.slalomTikiMaterial);
				slaloms[i].ResizeSlalomObjects(true);
			}
			else
			{
				slaloms[i].SetMaterial(levelGen.slalomBouyMaterial);
				slaloms[i].ResizeSlalomObjects(false);
			}
		}

		//Pickups
		SpecialPickup[] specialPickups = newChunk.gameObject.GetComponentsInChildren<SpecialPickup>(true);

		if (levelGen.playerPosition.x - levelGen.lastPowerupXPos < GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenPowerups)
		{
			for (int i = 0; i < specialPickups.Length; i++)
			{
				specialPickups[i].gameObject.SetActive(false);
			}
		}
		else
		{
			for (int i = 0; i < specialPickups.Length; i++)
			{
				specialPickups[i].ResetPickup();

				//replace me with a more intelligent system
				int rand = Random.Range(0,3);

				if(i == 0 && GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased != UpgradeItemType.None)
				{
					if(GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased == UpgradeItemType.PowerUpGoldDuration)
					{
						rand = 0;
						GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased = UpgradeItemType.None;
					}
					else if(GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased == UpgradeItemType.PowerUpMagnetDuration)
					{
						rand = 1;
						GameState.SharedInstance.Upgrades.LastUpgradeItemTypePurchased = UpgradeItemType.None;
					}
				}

				if (rand == 2)
					specialPickups[i].SetPowerUp(PowerUp.ScoreMultiplier);
				else if (rand == 1)
					specialPickups[i].SetPowerUp(PowerUp.CoinMagnet);
				else if (rand == 0)
					specialPickups[i].SetPowerUp(PowerUp.CoinMultiplier);

				levelGen.lastPowerupXPos = specialPickups[i].transform.position.x;
			}
		}

		SpecialRewardPickup[] specialRewardPickups = newChunk.gameObject.GetComponentsInChildren<SpecialRewardPickup>();
		for (int i = 0; i < specialRewardPickups.Length; i++)
		{
			specialRewardPickups[i].ResetSpecialReward();

			if (GameState.SharedInstance.FirstSuperRewardIndex < SpecialRewardHelper.firstSpecialReward.Length)
			{
				SpecialReward reward = SpecialRewardHelper.firstSpecialReward[GameState.SharedInstance.FirstSuperRewardIndex];
				specialRewardPickups[i].SetSpecialReward(reward);

				GameState.SharedInstance.FirstSuperRewardIndex += 1;
			}
			else
			{
				int[] randomWeights = new int[3];

				randomWeights[0] = GameServerSettings.SharedInstance.SurferSettings.SuperRewardWeightCoins;
				randomWeights[1] = GameServerSettings.SharedInstance.SurferSettings.SuperRewardWeightSingleEnergy + randomWeights[0];
				randomWeights[2] = GameServerSettings.SharedInstance.SurferSettings.SuperRewardWeightDoubleEnergy + randomWeights[1];

				int randVal = Random.Range(0, randomWeights[2] + 1);
				for (int j = 0; j < 3; j++)
				{
					if (randVal <= randomWeights[j])
					{
						if (j == 0)
							specialRewardPickups[i].SetSpecialReward(SpecialReward.Coins);
						else if (j == 1)
							specialRewardPickups[i].SetSpecialReward(SpecialReward.SingleEnergy);
						else if (j == 2)
							specialRewardPickups[i].SetSpecialReward(SpecialReward.DoubleEnergy);
						break;
					}
				}
			}
		}

		IndoorSwitchObject[] switchObjects = newChunk.gameObject.GetComponentsInChildren<IndoorSwitchObject>();
		for (int i = 0; i < switchObjects.Length; i++)
		{
			switchObjects[i].ResetIndoorSwitch();
			levelGen.playerControl.PrepareIndoorCameraSwitch(switchObjects[i].gameObject);
			levelGen.transitionObject = switchObjects[i].transform;
		}


		SplineGrind[] splineGrinds = newChunk.gameObject.GetComponentsInChildren<SplineGrind>();
		for (int i = 0; i < splineGrinds.Length; i++)
		{
			splineGrinds[i].Reset();
		}

		FallingRock[] fallingRocks = newChunk.gameObject.GetComponentsInChildren<FallingRock>();
		for (int i = 0; i < fallingRocks.Length; i++)
		{

			if (levelGen.curLocation == Location.Cave)
			{
				fallingRocks[i].transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);
				fallingRocks[i].SetMaterial(levelGen.caveFallingRockMaterial, levelGen.caveFallingRockCropMaterial, levelGen.caveFallingRockShadowMaterial);
				fallingRocks[i].ResizeRockObject(false);
				fallingRocks[i].isAnimated = false;
			}
			else
			{
				fallingRocks[i].transform.rotation = Quaternion.AngleAxis(-35.0f, Vector3.forward);
				fallingRocks[i].SetMaterial(levelGen.volcanoFallingRockMaterial, levelGen.volcanoFallingRockCropMaterial, levelGen.volcanoFallingRockShadowMaterial);
				fallingRocks[i].ResizeRockObject(true);
				fallingRocks[i].isAnimated = true;
			}

			fallingRocks[i].Reset();
		}

        //Choppers
        Chopper[] choppers = newChunk.gameObject.GetComponentsInChildren<Chopper>(true);
		if (levelGen.playerPosition.x - levelGen.lastChopperXPos < GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenChoppers)
        {
            for (int i = 0; i < choppers.Length; i++)
            {
                choppers[i].gameObject.SetActive(false);
            }
        }
        else if(choppers.Length > 0)
        {
            for (int i = 0; i < choppers.Length; i++)
            {
                choppers[i].gameObject.SetActive(true);
				levelGen.lastChopperXPos = choppers[i].transform.position.x;
				choppers[i].ResetChopper();
            }
        }

		DisableUnusedItems();

		levelGen.ReplaceCoinsWithLettersIfRequired(startX, coins);
        /*if (surfActivity.HasGoal(ActivityGoalType.CollectLetters))
        {
            ReplaceCoinsWithLettersIfRequired(startX, coins);
        }*/
	}

	private int GetNewIndex(int maxRange, ref int[] indexsActive,   ref int curIndexActive, ref int curTruncIndexActive, ChunkAssetFlags[] flags = null)
	{
		int returnIndex = -1;

		if (maxRange > 3)
		{
			returnIndex = Random.Range(0, maxRange);
			bool indexCheck = false;
			int specificChunkChunkCount = 0;
			while (!indexCheck)
			{
				indexCheck = true;
				if (flags != null && specificChunkChunkCount < 5)
				{
					bool wantedChunk = false;

					if (levelGen.playerPosition.x - levelGen.lastChopperXPos > GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenChoppers)
					{
						if (!flags[returnIndex].hasChopper)
						{
							indexCheck = false;
							returnIndex ++;
							returnIndex = returnIndex % (maxRange);
							specificChunkChunkCount++;
							continue;
						}
						else
							wantedChunk = true;	
					}

					if (!wantedChunk && levelGen.playerPosition.x - levelGen.lastSharkXPos > GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenSharks)
					{
						if (!flags[returnIndex].hasShark)
						{
							indexCheck = false;
							returnIndex ++;
							returnIndex = returnIndex % (maxRange);
							specificChunkChunkCount++;
							continue;
						}	
						else 
							wantedChunk = true;
					}

					if (!wantedChunk && levelGen.playerPosition.x - levelGen.lastPowerupXPos > GameServerSettings.SharedInstance.SurferSettings.MinDistanceBetweenPowerups)
					{
						if (!flags[returnIndex].hasSpecialPickup)
						{
							indexCheck = false;
							returnIndex ++;
							returnIndex = returnIndex % (maxRange);
							specificChunkChunkCount++;
							continue;
						}
					}


				}

				for (int i = 0; i < LevelGenerator.maxChunksActive; i++)
				{
					if (indexsActive[i] == returnIndex)
					{
						indexCheck = false;
						returnIndex ++;
						returnIndex = returnIndex % (maxRange);
						break;
					}	
				}
			}

			curTruncIndexActive = indexsActive[curIndexActive];
			indexsActive[curIndexActive] = returnIndex;
			curIndexActive = (curIndexActive + 1) % LevelGenerator.maxChunksActive;
		}
		else
		{
			curIndexActive = (curIndexActive + 1) % maxRange;
			returnIndex = curIndexActive;
			curTruncIndexActive = -1;
		}
		
		return returnIndex;
	}

	private Transform GetNewChunk()
	{
		Transform returnTransform = null;

        Difficulty diff = levelGen.GetDifficulty();

        //Intro Chunks
		if (!spawnedIntroChunk)
		{
			if(!GlobalSettings.TutorialCompleted())
			{
				returnTransform = introChunks[0];
				chunkIntroIndexsActive[0] = 0;
				curIntroChunkIndex = 1;
			}
			else
			{
				returnTransform = introChunks[GetNewIndex(introChunks.Length, ref chunkIntroIndexsActive, ref curIntroChunkIndex, ref truncIntroChunkIndex)];
				if (truncIntroChunkIndex != -1)
					introChunks[truncIntroChunkIndex].gameObject.SetActive(false);
			}

			if(GlobalSettings.SurfedPassMinTutorialDistance())
				spawnedIntroChunk = true;
		}
		//Super reward chunks
		else if ( levelGen.lastChunkFinishX > ( levelGen.rewardChunksSpawned + 1) * levelGen.rewardChunkDistance + 50 * levelGen.rewardChunksSpawned
					&& GlobalSettings.SurfedPassMinTutorialDistance()) 
        {
			if(Random.Range(0, 2) == 0)
			{
				returnTransform = superRewardChunks[GetNewIndex(superRewardChunks.Length, ref chunkSuperRewardIndexsActive, ref curSuperRewardChunkIndex, ref truncSuperRewardChunkIndex)];
				if (truncSuperRewardChunkIndex != -1)
					superRewardChunks[truncSuperRewardChunkIndex].gameObject.SetActive(false);
			}
			else
			{
				returnTransform = rewardChunks[GetNewIndex(rewardChunks.Length, ref chunkRewardIndexsActive, ref curRewardChunkIndex, ref truncRewardChunkIndex)];
				if (truncRewardChunkIndex != -1)
					rewardChunks[truncRewardChunkIndex].gameObject.SetActive(false);
			}
			levelGen.rewardChunksSpawned ++;
        }
        //Level switch
		else if ( (levelGen.chunksSinceSwitchLocation > LevelGenerator.minChunksBetweenSwitchLocations && GlobalSettings.SurfedPassMinTutorialDistance())
					 || levelGen.debugLocation != Location.Undefined 
					 || GlobalSettings.ForceDebugLocation != Location.Undefined
					 || levelGen.isImmediateTransition )
		{
			levelGen.isImmediateTransition = false;

			if(GlobalSettings.ForceDebugLocation != Location.Undefined)
				levelGen.debugLocation = GlobalSettings.ForceDebugLocation;

			if (levelGen.timeToTransition <= 0 || levelGen.debugLocation != Location.Undefined || GlobalSettings.ForceDebugLocation != Location.Undefined)
			{
				levelGen.chunksSinceSwitchLocation = 0;

				levelGen.SwitchRandomLocation();
				returnTransform = levelGen.GetTransitionChunk();
				levelGen.debugLocation = Location.Undefined;
				levelGen.timeToTransition = LocationTime.Get(levelGen.curLocation);
			}

			if(GlobalSettings.ForceDebugLocation != Location.Undefined)
			{
				GlobalSettings.ForceDebugLocation = Location.Undefined;
				levelGen.timeToTransition = 3600f; // stay in locaton debug for 60 mins.
			}
		}

		//Normal
		if (returnTransform == null && levelGen.chunksSinceSpecial < levelGen.maxChunksBetweenSpecial && GlobalSettings.SurfedPassMinTutorialDistance())
		{
			bool slalomCheck = ((Random.Range(0,4) == 0 && levelGen.isUsingSlalom) || (levelGen.isOnlyUsingSlalom));

           	//filter down if chunks needed are null
           	Difficulty origDiffiuclty = diff;
			if (slalomCheck)
            {
				if (diff == Difficulty.HARD)
				{
					if (hardSlalomChunks == null)
	            		diff = Difficulty.MEDIUM;
					else if (hardSlalomChunks.Length == 0)
						diff = Difficulty.MEDIUM;
            	}

				if (diff == Difficulty.MEDIUM)
				{	
					if (mediumSlalomChunks == null)
						diff = Difficulty.EASY;
					else if (mediumSlalomChunks.Length == 0)
	            		diff = Difficulty.EASY;
	            }

				if (diff == Difficulty.EASY)
				{
					if (easySlalomChunks == null)
					{
						diff = origDiffiuclty;
						levelGen.isUsingSlalom = false;
						slalomCheck = false;
	        		}
					else if (easySlalomChunks.Length == 0)
					{
						diff = origDiffiuclty;
						levelGen.isUsingSlalom = false;
						slalomCheck = false;
	        		}
        		}
            }

			if (!slalomCheck)
			{
				if (diff == Difficulty.HARD)
				{
					if (hardChunks == null)
						diff = Difficulty.MEDIUM;
					else if (hardChunks.Length == 0)
	        			diff = Difficulty.MEDIUM;
	        	}

	        	if (diff == Difficulty.MEDIUM)
	        	{
	        		if (mediumChunks == null)
						diff = Difficulty.EASY;
	        		else if (mediumChunks.Length == 0)
		        		diff = Difficulty.EASY;
		        }
        	}

			if (levelGen.powerupManager.GetCurrentVehicle() == Vehicle.Boat)
			{
				if ( slalomCheck )
				{
					returnTransform = easySlalomChunks[GetNewIndex(easySlalomChunks.Length, ref chunkEasySlalomIndexsActive, ref curEasySlalomChunkIndex, ref truncEasySlalomChunkIndex, easySlalomChunkFlags)];
					if (truncEasySlalomChunkIndex != -1)
						easySlalomChunks[truncEasySlalomChunkIndex].gameObject.SetActive(false);
				}
				else
				{
					returnTransform = easyChunks[GetNewIndex(easyChunks.Length, ref chunkEasyIndexsActive, ref curEasyChunkIndex, ref truncEasyChunkIndex, easyChunkFlags)];
					if (truncEasyChunkIndex != -1)
						easyChunks[truncEasyChunkIndex].gameObject.SetActive(false);
				}
			}
			else
			{
				switch (diff)//activityData.difficulty)
				{
					case Difficulty.EASY:
						if ( slalomCheck )
						{
							returnTransform = easySlalomChunks[GetNewIndex(easySlalomChunks.Length, ref chunkEasySlalomIndexsActive, ref curEasySlalomChunkIndex, ref truncEasySlalomChunkIndex, easySlalomChunkFlags)];
							if (truncEasySlalomChunkIndex != -1)
								easySlalomChunks[truncEasySlalomChunkIndex].gameObject.SetActive(false);
						}
						else
						{
							returnTransform = easyChunks[GetNewIndex(easyChunks.Length, ref chunkEasyIndexsActive, ref curEasyChunkIndex, ref truncEasyChunkIndex, easyChunkFlags)];
							if (truncEasyChunkIndex != -1)
								easyChunks[truncEasyChunkIndex].gameObject.SetActive(false);
						}
					break;

					case Difficulty.MEDIUM:
						if ( slalomCheck)
						{
						returnTransform = mediumSlalomChunks[GetNewIndex(mediumSlalomChunks.Length, ref chunkMediumSlalomIndexsActive, ref curMediumSlalomChunkIndex, ref truncMediumSlalomChunkIndex, mediumSlalomChunkFlags)];
							if (truncMediumSlalomChunkIndex != -1)
								mediumSlalomChunks[truncMediumSlalomChunkIndex].gameObject.SetActive(false);
						}
						else
						{
						returnTransform = mediumChunks[GetNewIndex(mediumChunks.Length, ref chunkMediumIndexsActive, ref curMediumChunkIndex, ref truncMediumChunkIndex, mediumChunkFlags)];
							if (truncMediumChunkIndex != -1)
								mediumChunks[truncMediumChunkIndex].gameObject.SetActive(false);
						}
					break;

					case Difficulty.HARD:
						if ( slalomCheck)
						{
							returnTransform = hardSlalomChunks[GetNewIndex(hardSlalomChunks.Length, ref chunkHardSlalomIndexsActive, ref curHardSlalomChunkIndex, ref truncHardSlalomChunkIndex, hardSlalomChunkFlags)];
							if (truncHardSlalomChunkIndex != -1)
								hardSlalomChunks[truncHardSlalomChunkIndex].gameObject.SetActive(false);
						}
						else
						{
							returnTransform = hardChunks[GetNewIndex(hardChunks.Length, ref chunkHardIndexsActive, ref curHardChunkIndex, ref truncHardChunkIndex, hardChunkFlags)];
							if (truncHardChunkIndex != -1)
								hardChunks[truncHardChunkIndex].gameObject.SetActive(false);
						}
					break;
				}
			}


			levelGen.chunksSinceSpecial++;
			levelGen.chunksSinceSwitchLocation++;
		}
		else if (returnTransform == null)
		{
			if (levelGen.isIndoorSection)
				levelGen.chunksSinceSpecial = 1;
			else 
				levelGen.chunksSinceSpecial = 0;

			List<int> randomPool = new List<int>();

			if (levelGen.isUsingPiers && pierChunks != null)
				randomPool.Add(0);
			if ( levelGen.isUsingBoats && boatChunks != null)
				randomPool.Add(1);
			if ( levelGen.isUsingWoodGrind && woodChunks != null)
				randomPool.Add(2);
			if ( levelGen.isUsingFallingRocks && fallingRockChunks != null)
				randomPool.Add(3);
			if ( levelGen.isUsingFunboxs && funboxChunks != null)
				randomPool.Add(4);
			
			if (randomPool.Count == 0)
				returnTransform = GetNewChunk();
			else
			{
				int specialType = randomPool[Random.Range(0, randomPool.Count)];
				if (specialType == 0)
				{
					returnTransform = pierChunks[ GetNewIndex(pierChunks.Length , ref chunkPierIndexsActive, ref curPierChunkIndex, ref truncPierChunkIndex)];
					if (truncPierChunkIndex != -1)
						pierChunks[truncPierChunkIndex].gameObject.SetActive(false);
					
				}
				if (specialType == 1) // boat
				{
					returnTransform = boatChunks[ GetNewIndex( boatChunks.Length, ref chunkBoatIndexsActive, ref curBoatChunkIndex, ref truncBoatChunkIndex)];
					if (truncBoatChunkIndex != -1)
						boatChunks[truncBoatChunkIndex].gameObject.SetActive(false);
				}
				if (specialType == 2) // wood grinds
				{
					returnTransform = woodChunks[ GetNewIndex( woodChunks.Length, ref chunkWoodGrindIndexsActive, ref curWoodGrindChunkIndex, ref truncWoodGrindChunkIndex)];
					if (truncWoodGrindChunkIndex != -1)
						woodChunks[truncWoodGrindChunkIndex].gameObject.SetActive(false);
				}
				if (specialType == 3) // falling rocks
				{
					returnTransform = fallingRockChunks[ GetNewIndex( fallingRockChunks.Length, ref chunkFallingRockIndexsActive, ref curFallingRockChunkIndex, ref truncFallingRockChunkIndex)];
					if (truncFallingRockChunkIndex != -1)
						fallingRockChunks[truncFallingRockChunkIndex].gameObject.SetActive(false);
				}
				if (specialType == 4) //funbox
				{
					returnTransform = funboxChunks[ GetNewIndex( funboxChunks.Length, ref chunkFunboxIndexsActive, ref curFunboxChunkIndex, ref truncFunboxChunkIndex)];
					if (truncFunboxChunkIndex != -1)
						funboxChunks[truncFunboxChunkIndex].gameObject.SetActive(false);
				}
			}
		}
		return returnTransform;
	}

	private Transform GetPlaneChunk()
	{
		Transform returnTransform = null;
		if (planeChunks != null)
		{
			 returnTransform = planeChunks[GetNewIndex(planeChunks.Length, ref chunkPlaneIndexsActive, ref curPlaneChunkIndex, ref truncPlaneChunkIndex)];
			 if (truncPlaneChunkIndex != -1)
			 	planeChunks[truncPlaneChunkIndex].gameObject.SetActive(false);
		}
		else
		{
			returnTransform = easyChunks[ GetNewIndex(easyChunks.Length, ref chunkEasyIndexsActive, ref curEasyChunkIndex, ref truncEasyChunkIndex)];
			if (truncEasyChunkIndex != -1)
				easyChunks[truncEasyChunkIndex].gameObject.SetActive(false);
		}
		return returnTransform;
	}

	private void DisableUnusedItems()
	{
	/*
        if (surfActivityData.DisablePiers)
		{
			GameObject[] piers = GameObject.FindGameObjectsWithTag("Pier");
			foreach (GameObject pier in piers)
			{
				//////Debug.Log("Disabling a pier here");
				pier.SetActive(false);
			}
		}

        if (surfActivityData.DisableBirds)
		{
			GameObject[] birds = GameObject.FindGameObjectsWithTag("Bird");
			foreach (GameObject bird in birds)
			{
				bird.SetActive(false);
			}
		}

        if (surfActivityData.DisableCrates)
		{
			GameObject[] crates = GameObject.FindGameObjectsWithTag("Crate");
			foreach (GameObject crate in crates)
			{
				crate.SetActive(false);
			}
		}

        if (surfActivityData.DisableBoostArrow)
		{
			GameObject[] arrows = GameObject.FindGameObjectsWithTag("Boost");
			foreach (GameObject arrow in arrows)
			{
				arrow.SetActive(false);
			}
		}

		if (surfActivityData.DisableSharks)
		{
			GameObject[] sharks = GameObject.FindGameObjectsWithTag("Shark");
			foreach (GameObject shark in sharks)
			{
				shark.SetActive(false);
			}
		}
		*/
	}
}
