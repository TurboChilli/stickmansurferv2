﻿using UnityEngine;
using System.Collections;
using System;

public class HalfPipeTimer : MonoBehaviour {

    private DateTime openDateTime;
    private TimeSpan timeDifference;
    private int timerInMinutes = 2;
    private bool timeIsUp = false;
    public AudioSource countDownSound;
    public AudioSource timeUpSound;
    private bool playTimesUpSound = false;
    private double totalSecs = 0;
    private int countdown = 5;
    public ShackController shackController;

    public bool TimeStatus
    {
        get
        {
            return timeIsUp;
        }
        set 
        {
            timeIsUp = value;
        }
    }

    public int TimerLength
    {
        get
        {
            return timerInMinutes;
        }
    }

    // Use this for initialization
    public void Start () 
    {
		if (shackController == null)
		{
			shackController = FindObjectOfType<ShackController>();
		}

    	if (!GameState.SharedInstance.HasPlayedHalfPipe)
    	{
			InitializeTimer(0);
            Update();
    	}
    	else
    	{
	        if (GameState.SharedInstance.HalfPipeTimeStamp == "0")
	        {
				InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Halfpipe));
	            Update();
	        }
	        else
	        {
	            GetOldTime();
	            Update();
	        }
        }
    }
	
    // Update is called once per frame
    void Update () {

        timeDifference = openDateTime.Subtract(DateTime.UtcNow);
        totalSecs = timeDifference.TotalSeconds;

        if (totalSecs > 0)
        {
            string result = TimeStringFormatter(timeDifference); 

            if (shackController != null)
	            shackController.SetHalfPipeTimerText(result);

            if(((int)totalSecs == countdown || totalSecs < (double)countdown) && (int)totalSecs != 0)
            {
            	if (countDownSound != null)
 	               GlobalSettings.PlaySound(countDownSound);
                playTimesUpSound = true;
                countdown--;
            }
            if((int)totalSecs == 0 && playTimesUpSound)
            {
				if (timeUpSound != null)
                	GlobalSettings.PlaySound(timeUpSound);
                playTimesUpSound = false;
                countdown = 5;
            }
        }
        else
        {
        	if (!timeIsUp)
        	{
        		if (shackController != null)
	        	{
					shackController.SetHalfPipeTimerText("");
		            shackController.EnableSkateboardActivityMarker();
					shackController.HideHalfpipeTimerDialog();
				}
				timeIsUp = true;
        	}
        }
    }

    public void InitializeTimer(int minutes)
    {
        openDateTime = DateTime.UtcNow.AddSeconds(minutes);
        GameState.SharedInstance.HalfPipeTimeStamp = openDateTime.ToBinary().ToString();
        timeIsUp = false;
    }

    public DateTime GetOldTime()
    {
        long temp = Convert.ToInt64(GameState.SharedInstance.HalfPipeTimeStamp);
        openDateTime = DateTime.FromBinary(temp);

        return openDateTime;
    }

    public string TimeStringFormatter(TimeSpan timeDifference)
    {
        string result = string.Format("  {0:D1}:{1:D2}:{2:D2}",
            timeDifference.Hours,
            timeDifference.Minutes,
            timeDifference.Seconds);

        return result;
    }
}
