﻿using UnityEngine;
using System.Collections;

public class FadeInAfterTimer : MonoBehaviour {

	public float timeToWait = 3.0f;
	public float timeToFadeIn = 0.5f;
	public bool startOnStart = false;
	private float curTimer;

	private bool timerStarted = false;
	private bool isFading = false;

	private bool initialised = false;

	private Material attachedMaterial;
	private Color origColor;
	private Color transparentColor;

	void Start () 
	{
		if (!initialised)
			Initialise();

		if (startOnStart)
			StartTimer();
	}

	void OnEnable()
	{
		if (!initialised)
			Initialise();

		if (startOnStart)
			StartTimer();
	}

	void Initialise()
	{
		MeshRenderer attachedMeshRenderer = GetComponent<MeshRenderer>();
		attachedMaterial = attachedMeshRenderer.material;
		origColor = attachedMaterial.color;

		transparentColor = origColor;
		transparentColor.a = 0.0f;
		attachedMaterial.color = transparentColor;

		initialised = true;
	}

	public void StartTimer()
	{
		if (!initialised)
			Initialise();

		timerStarted = true;
		attachedMaterial.color = transparentColor;
		curTimer = timeToWait;
	}

	void Update () 
	{
		if (timerStarted)
		{
			if (isFading)
			{	
				float lerpAmount = Mathf.Max( 0.0f, curTimer / timeToFadeIn);
				attachedMaterial.color = Color.Lerp(transparentColor, origColor, 1.0f - lerpAmount);

				if (curTimer <= 0)
					timerStarted = false;
			}
			else if (curTimer <= 0)
			{
				isFading = true;
				curTimer = timeToFadeIn;
			}

			curTimer -= Time.deltaTime;
		}

	}
}
