﻿using System;

public enum UpgradeItemType
{
	None = 0,

	PowerUpMagnetDuration = 1,
	PowerUpGoldDuration = 2,

	JetskiFuel = 11,
	JetskiMagnet = 12,
	JetskiGold = 13,

	BodyboardMagnet = 21,
	BodyboardGold = 22,

	TubeMagnet = 31,
	TubeGold = 32,

	LongboardMagnet = 41,
	LongboardGold = 42,

	SpeedBoatFuel = 51,
	SpeedBoatMagnet = 52,
	SpeedBoatGold = 53,

	PlaneFuel = 61,
	PlaneMagnet = 62,
	PlaneGold = 63,

	JeepEngine = 71,
	JeepFuel = 72,
	JeepWheels = 73,

	ChilliMagnet = 81,
	ChilliGold = 82,

	SkateGold = 101,
	SkateDuration = 102,

	MotorbikeFuel = 111,
	MotorbikeGold = 112,
	MotorbikeMagnet = 113,

	SurfBoardDefault = 120,
	SurfBoardThruster = 121,
	SurfBoardGold = 122,

	JeepDefault = 150,
	JeepBlack = 151,
	QuadBike = 152,

	RetroFan = 170,

	WindSurfBoardMagnet = 181,
	WindSurfBoardGold = 182,

	HeadDefault = 1000,
    HeadDisguise = 1001,
    HeadTopHat = 1002,
    HeadRastaHat = 1003,
    HeadMullet = 1004,
    HeadPaperBag = 1005,
    HeadPumpkin = 1006,
    HeadSticksonCap = 1007,
    HeadPirateHat = 1008,
    HeadMohawk = 1009,
    HeadCowboyHat = 1010,
    HeadEmoHair = 1011,
	HeadShark = 1012,
	HeadNewspaper = 1013,
	HeadCaptain = 1014,
	HeadDuck = 1015,
	HeadSnorkel = 1016,
	HeadPineapple = 1017,
	HeadElf = 1018,
	HeadOctopus = 1019,
	HeadSwimcap = 1020,
	HeadNekoEars = 1021,
	HeadCat = 1022,
}

public static class UpgradeItemTypeTitle
{
	public static string Get(UpgradeItemType type)
	{
		return Enum.GetName(typeof(UpgradeItemType), type);
	}
}	

