﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class EnergyDrinkShopController : MonoBehaviour {

	public Camera energyDrinkShopCamera;
	public GameObject closeButton;
	public NumericDisplayBar energyDrinkBar;
	public AudioSource soundClick;
	public AudioSource soundSwoosh;
	public AudioSource soundCashRegister;
	public AudioSource soundEnergyDrinkUse;
	public GameObject contactingServerOverlay;

	public EnergyDrinkPack pack1;
	public EnergyDrinkPack pack2;
	public EnergyDrinkPack pack3;
	public EnergyDrinkPack pack4;
	public EnergyDrinkPack pack5;
	public EnergyDrinkPack pack6;

	public GameObject shopTitle;

	public GameObject purchaseOverlayPanel;
	public Material purchaseOverlayMaterial;
	private Color origOverlayColor;

	public GameObject purchasePanel;
	public AnimatingObject purchasePackImage;
	public TextMeshPro purchaseTextMesh;

	private bool isShowingPanel = false;
	private bool isPausingPanel = false;
	private bool isMovingPanel = false;
	private bool isMovingPanelBack = false;

	private float curPanelTimer = 0.0f;
	private float moveTime = 0.35f;
	private float pauseTime = 1f;
	private Vector3 inactivePanelPosition;
	private Vector3 activePanelPosition;

	public GameObject shopTopBar;
	public GameObject shopBackground;

	bool initialised;
	bool showingContactingServerOverlay;
	float showingContactingServerOverlayTimeoutSecs = -1f;

	IAPManager iapManager;
	EnergyDrinkPack[] packObjects;
	Vector3[] packActivePosition;
	Vector3[] packInactivePosition;
	float baseTimeToMove = 0.35f;
	float[] curTimeToMove;
	float[] timeToMove;
	bool isMoving = false;

	public delegate void Close();
	public event Close OnClose;

	public delegate void PurchaseSuccessful(int energyDrinkAmount);
	public event PurchaseSuccessful OnPurchaseSuccessful;

	Camera launchingCameraReference;
	bool isActive;
	float clickDelay = -1f;

	public void Awake()
	{
		if (Utils.IphoneXCheck(energyDrinkShopCamera))
		{
			transform.localScale *= 0.9f;
			shopTitle.transform.position += Vector3.up * 1.0f;

			shopTopBar.transform.localScale *= 1.2f;
			shopTopBar.transform.position += Vector3.up * 1.2f;
			shopBackground.transform.localScale *= 1.1f;

            pack1.transform.position += Vector3.right * 1.2f;
            pack3.transform.position += Vector3.left * 1.2f;
            pack4.transform.position += Vector3.right * 1.2f;
            pack6.transform.position += Vector3.left * 1.2f;

            pack4.transform.position += Vector3.up * 0.8f;
            pack5.transform.position += Vector3.up * 0.8f;
            pack6.transform.position += Vector3.up * 0.8f;

            pack1.transform.localScale *= 1.2f;

            pack2.transform.localScale *= 1.2f;
            pack3.transform.localScale *= 1.2f;
            pack4.transform.localScale *= 1.2f;
            pack5.transform.localScale *= 1.2f;
            pack6.transform.localScale *= 1.2f;

		}
	}

	public void Show(Camera launchedFromCamera)
	{
		DialogTrackingFlags.EnergyDrinksShopShowing = true;

		RefreshEnergyDrinks();
		Initialise();

		launchingCameraReference = launchedFromCamera;

		launchingCameraReference.enabled = false;
        energyDrinkShopCamera.gameObject.SetActive(true);
		energyDrinkShopCamera.enabled = true;
		clickDelay = 0.2f;
		isActive = true;

		StartPanelAnimation(packObjects.Length);

		if (Utils.IphoneXCheck(energyDrinkShopCamera))
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 1.75f, 0.9f, closeButton.transform, energyDrinkShopCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 2.5f, 0.9f, energyDrinkBar.gameObject.transform, energyDrinkShopCamera);
		}
		else
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 1.75f, 1.2f, closeButton.transform, energyDrinkShopCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 2.5f, 1.2f, energyDrinkBar.gameObject.transform, energyDrinkShopCamera);
		}
	}

	public void Hide()
	{
		DialogTrackingFlags.EnergyDrinksShopShowing = false;

		if(OnClose != null)
			OnClose();

		energyDrinkShopCamera.enabled = false;

		if (launchingCameraReference != null)
			launchingCameraReference.enabled = true;

        energyDrinkShopCamera.gameObject.SetActive(false);
		isActive = false;
	}
		
	void InitialiseIAPManager()
	{
		if(iapManager == null)
		{
			iapManager = FindObjectOfType<IAPManager>();

			if(iapManager != null)
			{
				iapManager.OnProductPricesRefreshed += IAPManagerProductPricesRefreshedHandler;
				iapManager.OnPurchaseSuccessful += IAPManagerPurchaseSuccessfulHandler;
				iapManager.OnPurchaseFailed += IAPManagerPurchaseFailedHandler;
				iapManager.OnPurchaseCancelled += IAPManagerPurchaseCancelledHandler;
			}
		}
	}

	void OnDisable()
	{
		if(iapManager != null)
		{
			iapManager.OnProductPricesRefreshed -= IAPManagerProductPricesRefreshedHandler;
			iapManager.OnPurchaseSuccessful -= IAPManagerPurchaseSuccessfulHandler;
			iapManager.OnPurchaseFailed -= IAPManagerPurchaseFailedHandler;
			iapManager.OnPurchaseCancelled -= IAPManagerPurchaseCancelledHandler;
		}
	}

	void Start () 
	{
		HideContactingServerOverlay();

		activePanelPosition = Vector3.zero + Vector3.forward * purchasePanel.transform.localPosition.z;
		inactivePanelPosition = activePanelPosition + Vector3.right * 1500;
		purchasePanel.transform.localPosition = inactivePanelPosition;

		purchaseOverlayMaterial = purchaseOverlayPanel.GetComponent<MeshRenderer>().material;
		origOverlayColor = purchaseOverlayMaterial.color;
		purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);

		InitialiseIAPManager();
		RefreshEnergyDrinks();
		Initialise();
	}

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.EnergyDrinksShopShowing)
			{
				DialogTrackingFlags.FlagJustClosedPopupDialog();

				Hide();
			}
		}
		#endif 
	}

	void Update () {

		CheckAndroidBackButton();

		if(showingContactingServerOverlay && showingContactingServerOverlayTimeoutSecs >= 0)
		{
			showingContactingServerOverlayTimeoutSecs -= Time.deltaTime;
			if(showingContactingServerOverlayTimeoutSecs <= 0)
			{
				HideContactingServerOverlay();
			}
		}

		if(clickDelay >= 0)
			clickDelay -= Time.deltaTime;

		if (isShowingPanel)
		{
			if (isMovingPanel)
			{
				if (curPanelTimer >= 0)
				{
					float lerpAmount = 1.0f - Mathf.Pow( (curPanelTimer / moveTime), 3.0f);
					purchasePanel.transform.localPosition = Vector3.Lerp(inactivePanelPosition, activePanelPosition, lerpAmount);
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a * lerpAmount);
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					purchasePanel.transform.localPosition = activePanelPosition;
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a);

					curPanelTimer = pauseTime;
					isMovingPanel = false;
					isPausingPanel = true;
				}
			}
			else if (isPausingPanel)
			{
				if (curPanelTimer >= 0 && !Input.GetMouseButton(0))
				{
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					curPanelTimer = moveTime;
					isPausingPanel = false;
					isMovingPanelBack = true;

					GlobalSettings.PlaySound(soundSwoosh);
				}
			}
			else if (isMovingPanelBack)
			{
				if (curPanelTimer >= 0)
				{
					float lerpAmount = Mathf.Pow(1.0f - (curPanelTimer / moveTime), 3.0f);
					purchasePanel.transform.localPosition = Vector3.Lerp(-inactivePanelPosition, activePanelPosition, 1.0f - lerpAmount);
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a * (1.0f - lerpAmount));
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					purchasePanel.transform.localPosition = inactivePanelPosition;
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);

					curPanelTimer = pauseTime;
					isMovingPanelBack = false;
					isShowingPanel = false;
				}			
			}
		}

		if (isMoving)
		{
			bool[] animChecks = {false, false, false, false, false, false};
			for (int i = 0; i < packObjects.Length; i++)
			{
				if (curTimeToMove[i] >= 0)
				{
					curTimeToMove[i] -= Time.deltaTime;
					float lerpAmount = Mathf.Pow(Mathf.Max(0.0f, curTimeToMove[i] / timeToMove[i]), 3.0f);
					packObjects[i].transform.position = Vector3.Lerp(packInactivePosition[i], packActivePosition[i], 1.0f - lerpAmount);
					if (curTimeToMove[i] < 0)
						GlobalSettings.PlaySound(soundClick);
				}
				else
				{
					animChecks[i] = true;
				}
			}

			if (animChecks[0] && 
				animChecks[1] && 
				animChecks[2] && 
				animChecks[3] && 
				animChecks[4] && 
				animChecks[5])
			{
				isMoving = false;
			}
		}

		if( Input.GetMouseButtonUp(0))
		{
			if(isActive == false || clickDelay > 0 || energyDrinkShopCamera == null || !energyDrinkShopCamera.isActiveAndEnabled)
				return;

			if(UIHelpers.CheckButtonHit(closeButton.transform, energyDrinkShopCamera))
			{
				GlobalSettings.PlaySound(soundClick);
				Hide();
			}
			else if(UIHelpers.CheckButtonHit(pack1.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack1.PackId);
			}
			else if(UIHelpers.CheckButtonHit(pack2.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack2.PackId);
			}
			else if(UIHelpers.CheckButtonHit(pack3.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack3.PackId);
			}
			else if(UIHelpers.CheckButtonHit(pack4.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack4.PackId);
			}
			else if(UIHelpers.CheckButtonHit(pack5.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack5.PackId);
			}
			else if(UIHelpers.CheckButtonHit(pack6.PackButton, energyDrinkShopCamera))
			{
				BuyEnergyDrinks(pack6.PackId);
			}
		}
	}
		
	void Initialise()
	{
		if(!initialised)
		{
			initialised = true;

			InitialisePacks();

			int numberOfPacks = packObjects.Length;
			packActivePosition = new Vector3[numberOfPacks];
			packInactivePosition = new Vector3[numberOfPacks];
			curTimeToMove = new float[numberOfPacks];
			timeToMove = new float[numberOfPacks];

			for (int i = 0; i < numberOfPacks; i++)
			{
				packActivePosition[i] = packObjects[i].transform.position;
				packInactivePosition[i] = packActivePosition[i] + Vector3.down * 50f;
				curTimeToMove[i] = timeToMove[i] = baseTimeToMove + (float)i * 0.05f;
			}
		}
	}

	void InitialisePacks()
	{
		StickSurferSetting surferSettings = GameServerSettings.SharedInstance.SurferSettings;

		float pack1Price = 0f;
		float pack2Price = 0f;
		float pack3Price = 0f;
		float pack4Price = 0f;
		float pack5Price = 0f;
		float pack6Price = 0f;

		InitialiseIAPManager();

		if(iapManager != null)
		{
			pack1Price = iapManager.GetProductPrice(IAPManager.ProductIdPack1);
			pack2Price = iapManager.GetProductPrice(IAPManager.ProductIdPack2);
			pack3Price = iapManager.GetProductPrice(IAPManager.ProductIdPack3);
			pack4Price = iapManager.GetProductPrice(IAPManager.ProductIdPack4);
			pack5Price = iapManager.GetProductPrice(IAPManager.ProductIdPack5);
			pack6Price = iapManager.GetProductPrice(IAPManager.ProductIdPack6);
		}


		pack1.Initialise(IAPManager.ProductIdPack1, surferSettings.IAPEnergyPackAmount1, pack1Price, surferSettings.IAPEnergyPackAmount1, pack1Price);
		pack2.Initialise(IAPManager.ProductIdPack2, surferSettings.IAPEnergyPackAmount2, pack2Price, surferSettings.IAPEnergyPackAmount1, pack1Price);
		pack3.Initialise(IAPManager.ProductIdPack3, surferSettings.IAPEnergyPackAmount3, pack3Price, surferSettings.IAPEnergyPackAmount1, pack1Price);
		pack4.Initialise(IAPManager.ProductIdPack4, surferSettings.IAPEnergyPackAmount4, pack4Price, surferSettings.IAPEnergyPackAmount1, pack1Price);
		pack5.Initialise(IAPManager.ProductIdPack5, surferSettings.IAPEnergyPackAmount5, pack5Price, surferSettings.IAPEnergyPackAmount1, pack1Price);
		pack6.Initialise(IAPManager.ProductIdPack6, surferSettings.IAPEnergyPackAmount6, pack6Price, surferSettings.IAPEnergyPackAmount1, pack1Price);

		packObjects = new EnergyDrinkPack[] { pack6, pack5, pack4, pack3, pack2, pack1 };

		int bestValueIndex = -1;
		int lastBonusPercent = 0;
		for(int i = 0; i < packObjects.Length; i++)
		{
			if(packObjects[i].CalculatedBonusPercent > lastBonusPercent)
			{
				bestValueIndex = i;
				lastBonusPercent = packObjects[i].CalculatedBonusPercent;
			}
		}

		if(bestValueIndex != -1)
		{
			packObjects[bestValueIndex].ShowBestValueBanner();
			//don't show green tag, keep consistent red bonus //packObjects[bestValueIndex].ShowGreenBonusPercentTag(); 
		}

		if (Utils.IphoneXCheck(energyDrinkShopCamera))
		{
			pack1.transform.localScale *= 0.85f;
			pack2.transform.localScale *= 0.85f;
			pack3.transform.localScale *= 0.85f;
			pack4.transform.localScale *= 0.85f;
			pack5.transform.localScale *= 0.85f;
			pack6.transform.localScale *= 0.85f;
		}
	}

	void BuyEnergyDrinks(string packId)
	{
		GlobalSettings.PlaySound(soundClick);

		#if UNITY_EDITOR

		int packAmount = GetEnergyDrinkPackAmountByPackId(packId);
		GameState.SharedInstance.AddEnergyDrink(packAmount);
		RefreshEnergyDrinks();

		ShowEnergyDrinkPurchasePanel(packAmount);

		if(OnPurchaseSuccessful != null)
			OnPurchaseSuccessful(packAmount);

		GlobalSettings.PlaySound(soundCashRegister);
		GlobalSettings.PlaySound(soundEnergyDrinkUse);

		#else
		if(iapManager != null)
		{
			ShowContactingServerOverlay();
			iapManager.PurchaseProduct(packId);
		}
		#endif
	}

	public void ShowEnergyDrinkPurchasePanel(int energyDrinksPurchased, int packImageIndex = 0)
	{
		isShowingPanel = true;
		isMovingPanel = true;
		isPausingPanel = false;
		isMovingPanelBack = false;
		curPanelTimer = moveTime;

		purchaseTextMesh.text = string.Format("+{0}", energyDrinksPurchased);
		purchasePackImage.DrawFrame(packImageIndex);
		GlobalSettings.PlaySound(soundSwoosh);

		purchasePanel.transform.localPosition = inactivePanelPosition;
		purchaseOverlayPanel.transform.localPosition = activePanelPosition;
		purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);
	}

	int GetEnergyDrinkPackAmountByPackId(string packId)
	{
		foreach(EnergyDrinkPack pack in packObjects)
		{
			if(pack.PackId.ToLowerInvariant() == packId.ToLowerInvariant())
				return pack.PackAmount;
		}

		return 0;
	}

	void StartPanelAnimation(int numberOfPacks)
	{
		isMoving = true;
		for (int i = 0; i < numberOfPacks; i++)
		{
			curTimeToMove[i] = timeToMove[i];
			packObjects[i].transform.position = packInactivePosition[i];
		}
		GlobalSettings.PlaySound(soundSwoosh);
	}

	void RefreshEnergyDrinks()
	{
		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
	}

	void HideContactingServerOverlay()
	{
		showingContactingServerOverlay = false;
		showingContactingServerOverlayTimeoutSecs = -1f;
		contactingServerOverlay.SetActive(false);
	}

	void ShowContactingServerOverlay()
	{
		showingContactingServerOverlay = true;
		showingContactingServerOverlayTimeoutSecs = 30f;
		contactingServerOverlay.SetActive(true);
	}

	#region IAPManager Event Handlers

	void IAPManagerProductPricesRefreshedHandler()
	{
		InitialisePacks();
	}

	void IAPManagerPurchaseSuccessfulHandler(string productId)
	{
		HideContactingServerOverlay();

		if(!string.IsNullOrEmpty(productId) && IAPManager.ProductIdMatchesEnergyDrinkPackIds(productId))
		{
			GameState.SharedInstance.RemoveAds();

			GlobalSettings.PlaySound(soundCashRegister);
			GlobalSettings.PlaySound(soundEnergyDrinkUse);


			int packAmount = GetEnergyDrinkPackAmountByPackId(productId);
			GameState.SharedInstance.AddEnergyDrink(packAmount);

			RefreshEnergyDrinks();

			if(OnPurchaseSuccessful != null)
				OnPurchaseSuccessful(packAmount);

			ServerSavedGameStateSync.Sync();

			ShowEnergyDrinkPurchasePanel(packAmount);
		}
	}

	void IAPManagerPurchaseFailedHandler(string errorMsg)
	{
		HideContactingServerOverlay();
	}

	void IAPManagerPurchaseCancelledHandler(string msg)
	{
		HideContactingServerOverlay();
	}

	#endregion

}
