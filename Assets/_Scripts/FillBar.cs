﻿using UnityEngine;
using System.Collections;

public class FillBar : MonoBehaviour {
	
	public Renderer foreGroundRenderer;
	public Material foregroundMaterial;

	private float _currentHorizBarValue = 1.0f;
	private float prevHorizBarValue = 1.0f;
	private float curHorizAnimTime = 0.0f;
	private float horizAnimTime = 10.0f;

	private float _currentVertBarValue = 1.0f;
	private float prevVertBarValue = 1.0f;
	private float curVertAnimTime = 0.0f;
	private float vertAnimTime = 2.0f;

	private bool isHorizAnimating = false;
	private bool isVertAnimating = false;

	void Start () 
	{
		if (foregroundMaterial == null)
			foregroundMaterial = foreGroundRenderer.material;
	}

	public void SetHorizBarAtValue(float value, bool animated = false)
	{
		if (animated)
		{
			isHorizAnimating = true;
			curHorizAnimTime = horizAnimTime;
			prevHorizBarValue = _currentHorizBarValue;
			_currentHorizBarValue = value;
		}
		else
		{
			isHorizAnimating = false;
			_currentHorizBarValue = value;
			foregroundMaterial.SetFloat("_HorizontalCrop", value);
		}
	}

	public void SetVertBarAtValue(float value, bool animated = false)
	{
		if (animated)
		{
			isVertAnimating = true;
			curVertAnimTime = vertAnimTime;
			prevVertBarValue = _currentVertBarValue;
			_currentVertBarValue = value;
		}
		else
		{
			isVertAnimating = false;
			_currentVertBarValue = value;
			foregroundMaterial.SetFloat("_VerticalCrop", value);
		}
	}

	void Update () 
	{
		if (isHorizAnimating)
		{
			float lerpValue = curHorizAnimTime / horizAnimTime;

			curHorizAnimTime -= Time.deltaTime;
			if (curHorizAnimTime <= 0)
			{
				isHorizAnimating = false;
				lerpValue = 1.0f;
			}
			foregroundMaterial.SetFloat("_HorizontalCrop", Mathf.Lerp(prevHorizBarValue, _currentHorizBarValue, lerpValue));
		}

		if (isVertAnimating)
		{
			float lerpValue = curVertAnimTime / vertAnimTime;

			curVertAnimTime -= Time.deltaTime;
			if (curVertAnimTime <= 0)
			{
				isVertAnimating = false;
				lerpValue = 1.0f;
			}

			foregroundMaterial.SetFloat("_VerticalCrop", Mathf.Lerp(prevVertBarValue, _currentVertBarValue, lerpValue));
		}

	}

	public float CurrentVerticalBarValue
	{
		get{ return _currentVertBarValue;  }
	}

	public float CurrentHorizontalBarValue
	{
		get{ return _currentHorizBarValue;  }
	}
}
