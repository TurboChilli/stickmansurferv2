using UnityEngine;
using System.Collections;

public class GetVertices : MonoBehaviour {
	public Vector3[] initalVertexPositions;
	public Mesh mesh;
	public Vector3[] vertices;
	Vector3[] normals;

	public float Treshold = -4;
	public bool isWave = false;
	public int MenuID = 0;
	
	// FOAM WOBBLE SETTINGS
	public float Amplitude;
	public float Speed;
		
	// FOAM TEXTURE SETTINGS
	float scaleX = 1f;
	float scaleY = 1f;
	float scaleZ = 1f;
	
	public bool WawyGo = true;
	int i;
	GameObject handle;

	void InstantiateDummies() {
		i = 0;
		while (i < initalVertexPositions.Length/2) {
			////////Debug.Log("-> " + i + initalVertexPositions[i] + " -> " + (initalVertexPositions.Length / 2 + i) + initalVertexPositions[initalVertexPositions.Length / 2 + i]);
			handle = new GameObject("handle" + i);
			handle.AddComponent<Handler>();
			handle.transform.position = initalVertexPositions[i];
			handle.transform.parent = transform;
			handle.GetComponent<Handler>().VertexIndex = i;

			//for other side vertexes
			handle = new GameObject("handle" + (initalVertexPositions.Length / 2 + i));
			handle.AddComponent<Handler>();
			handle.transform.position = initalVertexPositions[initalVertexPositions.Length / 2 + i];
			handle.transform.parent = transform;
			handle.GetComponent<Handler>().VertexIndex = initalVertexPositions.Length / 2 + i;
			i++;
		}
	}

	void DoWavyMotion() {
		//normals = mesh.normals;
		i = 0;
		while (i < vertices.Length) {
			if (!isWave){
				if (initalVertexPositions[i].y > Treshold) {
					vertices[i] = initalVertexPositions[i] + new Vector3(0, Amplitude, 0) * Mathf.Sin(Time.time * Speed + i * 180 / Mathf.PI);
				}
			}
			else {
				if (i < vertices.Length / 2) {
					vertices[i] = initalVertexPositions[i] + new Vector3(0, Amplitude, 0) * Mathf.Sin(Time.time * Speed + i * 180 / Mathf.PI);
				}
				else {

					vertices[i] = initalVertexPositions[i] - new Vector3(0, Amplitude, 0) * Mathf.Sin(Time.time * Speed + i * 180 / Mathf.PI);
				}
			}
			i++;
		}
		mesh.vertices = vertices;
	}

	void Awake() { 
		mesh = gameObject.GetComponent<MeshFilter>().mesh;
		initalVertexPositions = mesh.vertices;
		vertices = initalVertexPositions;
		// FOAM WOBBLE SPEED
		Speed = 3f;
		
		// FOAM WOBBLE AMPLITUDE
		Amplitude = 0.1f;
		//InstantiateDummies();
	}

	// Update is called once per frame
	void Update () {
		vertices = mesh.vertices;
		//transform.localScale = new Vector3(scaleX, scaleY, scaleZ);

		if (WawyGo) {
			DoWavyMotion();
		}
	}



	void OnGUI() {
		
		/*
		if (GUIEnabler.foamEnabled == MenuID) {
			GUI.Label(new Rect(10, 5, 90, Screen.height * 0.05f), "Amplitude:");
			Amplitude = GUI.HorizontalSlider(new Rect(100, 10, Screen.width * 0.85f, Screen.height * 0.05f), Amplitude, 0f, 1f);
			GUI.Label(new Rect(10, Screen.height * 0.05f + 5, 90, Screen.height * 0.05f), "Speed:");
			Speed = GUI.HorizontalSlider(new Rect(100, Screen.height * 0.05f + 10, Screen.width * 0.85f, Screen.height * 0.05f), Speed, 0f, 10f);

			if (isWave) {
				//GUI.Label(new Rect(10, Screen.height * 0.1f + 5, 90, Screen.height * 0.05f), "TextureSpeed:");
				//TextureSpeed = GUI.HorizontalSlider(new Rect(100, Screen.height * 0.1f + 10, Screen.width * 0.85f, Screen.height * 0.05f), TextureSpeed, 0f, 5f);
			}
			else {
				GUI.Box(new Rect(Screen.width * 0.85f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.05f), "Treshold: " + Treshold.ToString("F2"));
				Treshold = GUI.VerticalSlider(new Rect(Screen.width * 0.95f, Screen.height * 0.3f, Screen.width * 0.05f, Screen.height * 0.3f), Treshold, 3, -4);
			}

			GUI.Box(new Rect(10, Screen.height * 0.8f, Screen.width * 0.2f, Screen.height * 0.09f), "Amplitude: " + Amplitude.ToString("F2"));
			GUI.Box(new Rect(10, Screen.height * 0.9f, Screen.width * 0.2f, Screen.height * 0.09f), "Speed: " + Speed.ToString("F2"));
			if (GUI.Button(new Rect(Screen.width * 0.2f + 20, Screen.height * 0.9f, Screen.width * 0.2f, Screen.height * 0.09f), (WawyGo ? "STOP" : "START"))) {
				WawyGo = !WawyGo;
			}


			GUI.Label(new Rect(Screen.width * 0.4f + 30, Screen.height * 0.8f, Screen.width * 0.1f, Screen.height * 0.05f), "X: " + scaleX.ToString("F2"));
			scaleX = GUI.HorizontalSlider(new Rect(Screen.width * 0.5f + 40, Screen.height * 0.8f, Screen.width * 0.3f, Screen.height * 0.05f), scaleX, 0f, 5f);
			GUI.Label(new Rect(Screen.width * 0.4f + 30, Screen.height * 0.86f, Screen.width * 0.1f, Screen.height * 0.05f), "Y: " + scaleY.ToString("F2"));
			scaleY = GUI.HorizontalSlider(new Rect(Screen.width * 0.5f + 40, Screen.height * 0.86f, Screen.width * 0.3f, Screen.height * 0.05f), scaleY, 0f, 5f);
			GUI.Label(new Rect(Screen.width * 0.4f + 30, Screen.height * 0.92f, Screen.width * 0.1f, Screen.height * 0.05f), "Z: " + scaleZ.ToString("F2"));
			scaleZ = GUI.HorizontalSlider(new Rect(Screen.width * 0.5f + 40, Screen.height * 0.92f, Screen.width * 0.3f, Screen.height * 0.05f), scaleZ, 0f, 5f);
		}
		*/
	}
}
