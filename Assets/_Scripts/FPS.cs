using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {
	public float updateInterval = 0.5F;

	private float accum = 0; // FPS accumulated over the interval
	private int frames = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval
	float fps = 0f;

	GUIStyle FPSTextStyle = new GUIStyle();


	void Awake() {
		//Application.targetFrameRate = 60;
	}

	void Start() {
		timeleft = updateInterval;
	}

	void Update () {
		timeleft -= Time.deltaTime;
		accum += Time.timeScale / Time.deltaTime;
		++frames;

		// Interval ended - update GUI text and start new interval
		if (timeleft <= 0.0) {
			fps = accum / frames;
			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
		}
	}

	void OnGUI () {
		if (fps < 10) {
			FPSTextStyle.normal.textColor = Color.red;
		}
		else {
			if (fps < 30) {
				FPSTextStyle.normal.textColor = Color.yellow;
			}
			else {
				FPSTextStyle.normal.textColor = Color.green;
			}
		}
		GUI.Label(new Rect(Screen.width * 0.95f,Screen.height * 0.01f,Screen.width * 0.1f,Screen.height * 0.05f),fps.ToString("F2"),FPSTextStyle);
	}
}