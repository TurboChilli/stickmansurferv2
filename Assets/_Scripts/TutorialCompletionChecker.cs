﻿using UnityEngine;
using System.Collections;

public class TutorialCompletionChecker 
{
	public static bool MarkTutorialCompleteAndRedirectToMenu
	{
		get 
		{ 
			return (PlayerPrefs.GetInt("SSFBM_markTutComplete", 0) == 1); 
		}
		set 
		{ 
			if(value)
				PlayerPrefs.SetInt("SSFBM_markTutComplete", 1);
			else
				PlayerPrefs.SetInt("SSFBM_markTutComplete", 0);
		}
	}

	public static void CheckMarkTutorialsCompleteAndRedirectToMenu()
	{
		if(MarkTutorialCompleteAndRedirectToMenu)
		{
			MarkTutorialCompleteAndRedirectToMenu = false;

			if(GameState.SharedInstance.TutorialStage != TutStageType.Finished)
			{
				GlobalSettings.firstTimePlayVideo = false;
				GlobalSettings.FlagTutorialAsCompleted();
				GlobalSettings.FlagSurfedPassMinTutorialDistanceCompleted();
				GameState.SharedInstance.TutorialStage = TutStageType.Finished;
				GameState.SharedInstance.HasPlayedHalfPipe = true;

				SceneNavigator.NavigateTo(SceneType.Menu);
			}
		}
	}

}
