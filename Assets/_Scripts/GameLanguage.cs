﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

/// <summary>
/// Supported Languages: English, Espanol, Chinese Simplified, Chinese Traditional, French, Japanese, German, Indonesian, Portugese, Italian, Korean, Russian.
/// </summary>
public class GameLanguage
{
	static TMP_FontAsset normalFont;

	public static TMP_FontAsset NormalFont
	{
		get
		{
			if ( normalFont == null ) 
				Initialise();
			
			return normalFont;
		}
	}

	static SystemLanguage selectedLanguage = SystemLanguage.Unknown;

	public static SystemLanguage SelectedLanguage 
	{
		get
		{
			if(!PlayerPrefs.HasKey("Localizer_defLang"))
				Initialise();
			else if(selectedLanguage == SystemLanguage.Unknown)
			{
				string langString = PlayerPrefs.GetString("Localizer_defLang");
				selectedLanguage = (SystemLanguage) Enum.Parse(typeof(SystemLanguage), langString, true);
			}

			return selectedLanguage;
		}
		set
		{
			selectedLanguage = value;
			PlayerPrefs.SetString("Localizer_defLang", Enum.GetName(typeof(SystemLanguage), selectedLanguage));
			RefreshFontSetting();
		}
	}

	public static void Initialise()
	{
		SystemLanguage sysLanguage = Application.systemLanguage;

		if(PlayerPrefs.HasKey("Localizer_defLang"))
			sysLanguage = SelectedLanguage;

		// Default to English if language not supported
		switch(sysLanguage)
		{
		case SystemLanguage.English:
		case SystemLanguage.Spanish:
		case SystemLanguage.French:
		case SystemLanguage.ChineseSimplified:
		case SystemLanguage.ChineseTraditional:
		case SystemLanguage.Japanese:
		case SystemLanguage.German:
		case SystemLanguage.Indonesian:
		case SystemLanguage.Portuguese:
		case SystemLanguage.Italian:
		case SystemLanguage.Korean:
		case SystemLanguage.Russian:
			break;
		case SystemLanguage.Chinese:
			sysLanguage = SystemLanguage.ChineseSimplified;
			break;
		default:
			sysLanguage = SystemLanguage.English;
			break;
		}

		SelectedLanguage = sysLanguage;

		RefreshFontSetting();
	}

	public static string LocalizedText(string englishValue)
	{
		if(SelectedLanguage == SystemLanguage.English)
			return englishValue;
		
		if(!localizedTexts.ContainsKey(englishValue))
		{
			#if UNITY_EDITOR
			Debug.LogWarningFormat("GameLanguage -> No localized entry for -> '{0}'", englishValue);
			#endif 

			return englishValue;
		}

		if(!localizedTexts[englishValue].ContainsKey(SelectedLanguage))
			return englishValue;

		#if UNITY_EDITOR
		if(string.IsNullOrEmpty(localizedTexts[englishValue][SelectedLanguage]))
			Debug.LogWarningFormat("GameLanguage -> No localized {0} term for -> '{1}'", Enum.GetName(typeof(SystemLanguage), SelectedLanguage), englishValue);
		#endif 

		return localizedTexts[englishValue][SelectedLanguage];
	}

	public static Dictionary<string, Dictionary<SystemLanguage, string>> GetDefaultLocalizedTexts()
	{
		return defaultLocalizedTexts;
	}

	static void RefreshFontSetting()
	{
		switch(SelectedLanguage)
		{
		case SystemLanguage.ChineseSimplified:
			normalFont = Resources.Load("Fonts/Chinese Simplified SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		case SystemLanguage.ChineseTraditional:
			normalFont = Resources.Load("Fonts/Chinese Traditional SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		case SystemLanguage.Chinese:
			normalFont = Resources.Load("Fonts/Chinese Simplified SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		case SystemLanguage.Japanese:
			normalFont = Resources.Load("Fonts/Japanese SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		case SystemLanguage.Korean:
			normalFont = Resources.Load("Fonts/Korean SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		case SystemLanguage.Russian:
			normalFont = Resources.Load("Fonts/Russian SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		default:
			normalFont = Resources.Load("Fonts/English SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
			break;
		}

		if(normalFont == null)
			normalFont = Resources.Load("Fonts/English SDF", typeof(TMP_FontAsset)) as TMP_FontAsset;
	}


	static Dictionary<string, Dictionary<SystemLanguage, string>> _localizedTexts;
	
	static Dictionary<string, Dictionary<SystemLanguage, string>> localizedTexts
	{
		get
		{
			if(_localizedTexts == null)
			{
				_localizedTexts = defaultLocalizedTexts;

				// Merge from auto generated texts.
				if(GeneratedLanguageDictionary.GeneratedLcalizedTexts != null && GeneratedLanguageDictionary.GeneratedLcalizedTexts.Count > 0)
				{
					foreach(var generatedLocalisedText in GeneratedLanguageDictionary.GeneratedLcalizedTexts)
					{
						if(_localizedTexts.ContainsKey(generatedLocalisedText.Key))
						{
							Debug.LogFormat("GameLanguage -> localizedTexts -> Replacing Default Value -> '{0}'", generatedLocalisedText.Key);

							_localizedTexts[generatedLocalisedText.Key] = generatedLocalisedText.Value;
						}
						else
						{
							_localizedTexts.Add(generatedLocalisedText.Key, generatedLocalisedText.Value);
						}
					}
				}
			}

			return _localizedTexts;
		}
	}

	/// <summary>
	/// This stores default localized terms as defined. If there is the same term in GeneratedLanguageDictionary.cs it will replace defaults.
	/// GeneratedLanguageDictionary are translations done from Fiverr etc, and are imported from CSVs.
	/// </summary>
	static Dictionary<string, Dictionary<SystemLanguage, string>> defaultLocalizedTexts = new Dictionary<string, Dictionary<SystemLanguage, string>>(StringComparer.InvariantCultureIgnoreCase)
	{
		{ "ENGLISH", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ESPAÑOL" },
				{ SystemLanguage.ChineseSimplified, "简体中文" },
				{ SystemLanguage.ChineseTraditional, "繁體中文" },
				{ SystemLanguage.French, "FRANÇAIS" },
				{ SystemLanguage.Japanese, "日本語" },
				{ SystemLanguage.German, "DEUTSCHE" },
				{ SystemLanguage.Indonesian, "BAHASA" },
				{ SystemLanguage.Portuguese, "PORTUGUÊS" },
				{ SystemLanguage.Italian, "ITALIANO" },
				{ SystemLanguage.Korean, "한국어" },
				{ SystemLanguage.Russian, "РУССКИЙ" },
			}
		},

		{ "COLLECT {0} COINS TOTAL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Recoger {0} monedas totales" },
				{ SystemLanguage.ChineseSimplified, "收集{0}硬币总" },
				{ SystemLanguage.ChineseTraditional, "收集{0}硬幣總" },
				{ SystemLanguage.French, "Collecter {0} pièces de monnaie totale" },
				{ SystemLanguage.Japanese, "合計{0}コインを収集" },
				{ SystemLanguage.German, "Sammeln Sie {0} Münzen Gesamt" },
				{ SystemLanguage.Indonesian, "Kumpulkan {0} koin Total" },
				{ SystemLanguage.Portuguese, "Colete {0} moedas total de" },
				{ SystemLanguage.Italian, "Raccogliere {0} monete totale" },
				{ SystemLanguage.Korean, "수집 {0} 동전 총" },
				{ SystemLanguage.Russian, "Собирают {0} монеты общей сложности" },
			}
		},


		{ "PASS THROUGH A GATE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Pasar a través de una puerta" },
				{ SystemLanguage.ChineseSimplified, "通过栅极通" },
				{ SystemLanguage.ChineseTraditional, "通過柵極通" },
				{ SystemLanguage.French, "Passer à travers une porte" },
				{ SystemLanguage.Japanese, "門をくぐります" },
				{ SystemLanguage.German, "Durch ein Tor" },
				{ SystemLanguage.Indonesian, "Melewati gerbang" },
				{ SystemLanguage.Portuguese, "Passar por um portão" },
				{ SystemLanguage.Italian, "Passare attraverso un cancello" },
				{ SystemLanguage.Korean, "게이트를 통과" },
				{ SystemLanguage.Russian, "Пройдите через ворота" },
			}
		},

	}; // End localizedTexts dictionary



}