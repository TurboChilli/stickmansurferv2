﻿using System;
using UnityEngine;
using System.Collections;
using Prime31;
using UnityEngine.iOS;

public class NotificationManager : MonoBehaviour 
{
    void Start()
    {
        DontDestroyOnLoad(this);
		ClearBadgeCountAndCancelAllLocalNotifications();
    }

	void OnApplicationPause(bool paused)
    {
        if (paused)
        {
			RescheduleNotifications();
        }
		else
		{
			ClearBadgeCountAndCancelAllLocalNotifications();
		}
    }

	void OnApplicationQuit()
	{
		RescheduleNotifications();
	}

	int GetTotalSecondsTillNotification(string timeStamp)
	{
		if(string.IsNullOrEmpty(timeStamp))
			return -1;

		long ticks;
		if(Int64.TryParse(timeStamp, out ticks))
		{
			DateTime scheduledDate = DateTime.FromBinary(ticks);
			TimeSpan timeDifference = scheduledDate.Subtract(DateTime.UtcNow);
			return (int)Math.Floor(timeDifference.TotalSeconds);
		}
		else
		{
			return -1;
		}
	}


	void RescheduleNotifications()
	{
		// Don't use notifications for Android
		#if UNITY_ANDROID
		return;
		#endif

		if(GlobalSettings.alertNotificationEnabled == false)
			return;

		ClearBadgeCountAndCancelAllLocalNotifications();

		StickSurferSetting surfSettings = GameServerSettings.SharedInstance.SurferSettings;

		int secondsTillJeepNotification = GetTotalSecondsTillNotification(GameState.SharedInstance.JeepTimeStamp);
		int secondsTillSkateNotification = GetTotalSecondsTillNotification(GameState.SharedInstance.HalfPipeTimeStamp);
		int secondsTillTreasureNotification = GetTotalSecondsTillNotification(GameState.SharedInstance.TreasureVaultTimeStamp);

		// Schedule local notifications
		if(secondsTillTreasureNotification > surfSettings.MinSecsToNotification)
		{
			string notificationMsg = GameLanguage.LocalizedText("Treasure chest has been unlocked! Claim your reward!");
			SetLocalNotification(notificationMsg, secondsTillTreasureNotification);
		}
		else if(secondsTillJeepNotification > surfSettings.MinSecsToNotification)
		{
			string notificationMsg = GameLanguage.LocalizedText("Jeep refueled and ready to rock the dunes!");
			SetLocalNotification(notificationMsg, secondsTillJeepNotification);

		}
		else if(secondsTillSkateNotification > surfSettings.MinSecsToNotification)
		{
			string notificationMsg = GameLanguage.LocalizedText("Half pipe is unlocked so get yer skate on!");
			SetLocalNotification(notificationMsg, secondsTillSkateNotification);
		}
	}

	void SetLocalNotification(string notificationMsg, int secondsTillNotification)
	{
		// Don't use notifications for Android
		#if UNITY_ANDROID
		return;
		#endif

		#if UNITY_ANDROID
		AndroidScheduledNotificationIDString = "";
		#endif

		#if UNITY_IOS
		EtceteraTwoBinding.scheduleLocalNotification(secondsTillNotification, notificationMsg, notificationMsg, 1,  "EngineNotify.caf", null);
		#elif UNITY_ANDROID
		AndroidNotificationConfiguration androidNotificationConfig = new AndroidNotificationConfiguration((long)secondsTillNotification, notificationMsg, notificationMsg, "1");
		int androidNotificationID = EtceteraAndroid.scheduleNotification(androidNotificationConfig);
		#endif


		// Schedule extra reminder for 24 hours after they last opened chest
		DateTime backup24HrNotification = DateTime.UtcNow.AddHours(24);
		TimeSpan backup24HrNotificationTotalSeconds = backup24HrNotification.Subtract(DateTime.UtcNow);
		int backup24HrTotalSeconds = (int)backup24HrNotificationTotalSeconds.TotalSeconds;

		#if UNITY_IOS
		EtceteraTwoBinding.scheduleLocalNotification(backup24HrTotalSeconds, notificationMsg, notificationMsg, 1,  "EngineNotify.caf", null);
		#elif UNITY_ANDROID
		AndroidNotificationConfiguration androidNotificationConfigBackup24Hr = new AndroidNotificationConfiguration(backup24HrTotalSeconds, notificationMsg, notificationMsg, "1");
		int androidNotificationID24HrBackup = EtceteraAndroid.scheduleNotification(androidNotificationConfigBackup24Hr);
		#endif

		#if UNITY_ANDROID
		AndroidScheduledNotificationIDString = string.Format("{0},{1}", androidNotificationID, androidNotificationID24HrBackup);
		#endif
	}

	#if UNITY_ANDROID
	public static string AndroidScheduledNotificationIDString
	{
		get  { return PlayerPrefs.GetString("nmandnid", ""); }
		set  { PlayerPrefs.SetString("nmandnid", value); }
	}
	#endif

    public static void RegisterForNotifications()
    {
		// Don't use notifications for Android
		#if UNITY_ANDROID
		return;
		#endif

		if (!PlayerPrefs.HasKey("RegNotify"))
		{
			PlayerPrefs.SetInt("RegNotify", 1);

			#if UNITY_IOS
			EtceteraTwoBinding.registerForNotifications();
			EtceteraBinding.registerForRemoteNotifications(P31RemoteNotificationType.Alert);
			#elif UNITY_ANDROID
				//nutin
			#endif
		}
    }

	public static void ClearBadgeCountAndCancelAllLocalNotifications()
	{
		// Don't use notifications for Android
		#if UNITY_ANDROID
		return;
		#endif

		#if UNITY_IOS

			EtceteraBinding.setBadgeCount(0);
			EtceteraTwoBinding.cancelAllLocalNotifications();

		#elif UNITY_ANDROID

		if(!string.IsNullOrEmpty(AndroidScheduledNotificationIDString) && AndroidScheduledNotificationIDString.Trim().Length > 0)
		{
			string[] notificationIds = AndroidScheduledNotificationIDString.Split(new char[] { ',' });
			if(notificationIds != null)
			{
				foreach(string notificationIdStr in notificationIds)
				{
					int androidNotificationID;
					if(System.Int32.TryParse(notificationIdStr, out androidNotificationID))
					{
						EtceteraAndroid.cancelNotification(androidNotificationID);
					}
				}

				AndroidScheduledNotificationIDString = "";
			}
		}

		#endif
	}
}
