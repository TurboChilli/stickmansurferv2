﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeItem 
{
	int UpgradeBasePrice;

	public UpgradeItem(UpgradeItemType upgradeItemType, int totalUpgradeStages, int upgradeBasePrice)
	{
		ItemType = upgradeItemType;
		TotalUpgradeStages = totalUpgradeStages;
		UpgradeBasePrice = upgradeBasePrice;

		LevelRequired = UpgradeManager.UpgradeItemLevelRequired(upgradeItemType);
		int? energyDrinkBasePrice = UpgradeManager.UpgradeItemEnergyDrinkCost(upgradeItemType);

		if(energyDrinkBasePrice.HasValue)
		{
			UpgradeBasePrice = energyDrinkBasePrice.Value;
			PriceIsEnergyDrink = true;
		}
	}

	public UpgradeItem(UpgradeItemType upgradeItemType, int totalUpgradeStages, int upgradeBasePrice, Cosmetic cosmeticItem)
	{
		ItemType = upgradeItemType;
		TotalUpgradeStages = totalUpgradeStages;
		UpgradeBasePrice = upgradeBasePrice;
		CosmeticItem = cosmeticItem;

		LevelRequired = UpgradeManager.UpgradeItemLevelRequired(upgradeItemType);
		int? energyDrinkBasePrice = UpgradeManager.UpgradeItemEnergyDrinkCost(upgradeItemType);

		if(energyDrinkBasePrice.HasValue)
		{
			UpgradeBasePrice = energyDrinkBasePrice.Value;
			PriceIsEnergyDrink = true;
		}
	}

	public UpgradeItem(UpgradeItemType upgradeItemType, int totalUpgradeStages, int upgradeBasePrice, Vehicle vehicleSurfboardItem)
	{
		ItemType = upgradeItemType;
		TotalUpgradeStages = totalUpgradeStages;
		UpgradeBasePrice = upgradeBasePrice;
		SurfBoardItem = vehicleSurfboardItem;

		LevelRequired = UpgradeManager.UpgradeItemLevelRequired(upgradeItemType);
		int? energyDrinkBasePrice = UpgradeManager.UpgradeItemEnergyDrinkCost(upgradeItemType);

		if(energyDrinkBasePrice.HasValue)
		{
			UpgradeBasePrice = energyDrinkBasePrice.Value;
			PriceIsEnergyDrink = true;
		}
	}

	public UpgradeItem(UpgradeItemType upgradeItemType, int totalUpgradeStages, int upgradeBasePrice, JeepType jeepItem)
	{
		ItemType = upgradeItemType;
		TotalUpgradeStages = totalUpgradeStages;
		UpgradeBasePrice = upgradeBasePrice;
		JeepItem = jeepItem;

		LevelRequired = UpgradeManager.UpgradeItemLevelRequired(upgradeItemType);
		int? energyDrinkBasePrice = UpgradeManager.UpgradeItemEnergyDrinkCost(upgradeItemType);

		if(energyDrinkBasePrice.HasValue)
		{
			UpgradeBasePrice = energyDrinkBasePrice.Value;
			PriceIsEnergyDrink = true;
		}
	}

	public UpgradeItemType ItemType { get; private set; }
	public int TotalUpgradeStages { get; private set; }
	public Cosmetic? CosmeticItem { get; private set; }
	public Vehicle? SurfBoardItem { get; private set; }
	public JeepType? JeepItem { get; private set; }
	public bool PriceIsEnergyDrink { get; private set; }
	public int LevelRequired { get; private set; }

	public int CurrentUpgradeStage
	{
		get
		{
			return PlayerPrefs.GetInt(string.Format("upgrItem{0}", (int)ItemType), 0);
		}
		private set 
		{
			PlayerPrefs.SetInt(string.Format("upgrItem{0}", (int)ItemType), value);
		}
	}

	public bool UpgradeCompleted()
	{
		return (CurrentUpgradeStage == TotalUpgradeStages);
	}

	public void UpgradeToNextStage()
	{
		if(!UpgradeCompleted())
		{
			int nextStage = CurrentUpgradeStage + 1;
			CurrentUpgradeStage = nextStage;

			SavedGameStateServerSettings.SharedInstance.FlagDataDirty();
		}
	}

	public void RestoreToUpgradeStage(int upgradeStage)
	{
		if(!UpgradeCompleted())
		{
			CurrentUpgradeStage = upgradeStage;
		}
	}

	public bool IsCosmeticItem()
	{
		return CosmeticItem.HasValue;
	}

	public bool CosmeticItemCurrentlyEquiped()
	{
		if(!CosmeticItem.HasValue)
			return false;

		return CosmeticItem.Value == GameState.SharedInstance.CurrentCosmetic;
	}

	public bool IsSurfBoardItem()
	{
		return SurfBoardItem.HasValue;
	}

	public bool SurfBoardItemCurrentlyEquiped()
	{
		if(!SurfBoardItem.HasValue)
			return false;

		return SurfBoardItem.Value == GameState.SharedInstance.CurrentVehicle;
	}

	public bool IsJeepItem()
	{
		return JeepItem.HasValue;
	}

	public bool JeepItemCurrentlyEquiped()
	{
		if(!JeepItem.HasValue)
			return false;

		return JeepItem.Value == GameState.SharedInstance.CurrentJeep;
	}

	public int? GetNextUgpradePrice()
	{
		if(UpgradeCompleted())
			return null;

		return GetUpradePriceByStage(CurrentUpgradeStage + 1);
	}

	public int GetUpradePriceByStage(int stageNumber)
	{
		StickSurferSetting surferSettings = GameServerSettings.SharedInstance.SurferSettings;

		if(stageNumber == 1)
			return UpgradeBasePrice;
		else if(stageNumber == 2)
			return UpgradeBasePrice * surferSettings.Upgrade2Multiple;
		else if(stageNumber == 3)
			return UpgradeBasePrice * surferSettings.Upgrade3Multiple;
		else if(stageNumber == 4)
			return UpgradeBasePrice * surferSettings.Upgrade4Multiple;
		else if(stageNumber == 5)
			return UpgradeBasePrice * surferSettings.Upgrade5Multiple;
		else 
			return UpgradeBasePrice * surferSettings.Upgrade6Multiple;
	}
}
