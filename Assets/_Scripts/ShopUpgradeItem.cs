﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class ShopUpgradeItem : MonoBehaviour 
{
	public ShopController shopController;
	public UpgradeItemType UpgradeItemType;
	public AnimatingObject UpgradeButton;
	public TextMesh UpgradeButtonText;
	public TMP_Text LevelRequiredText;

	[Header("Optional Properties")]
	public GameObject UpgradeLabelText;
	public FillBar UpgradeFillBar;
	public AudioSource CashRegisterOpenSound;
	public AudioSource SoundClick = null;
	public CoinBar CoinBar;
	public EnergyDrinkDialog energyDrinkDialog;

	UpgradeItem upgradeItem;
	bool energyDrinkSwapAutoPurchaseItem;

	void OnEnable()
	{
		if(energyDrinkDialog != null)
		{
			energyDrinkDialog.OnEnergyDrinkSpendSuccessful += EnergyDrinkSpendSuccessful;
			energyDrinkDialog.OnCloseButtonClicked += EnergyDrinkDialogCloseButtonClicked;
		}
	}
		
	void OnDisable()
	{
		if(energyDrinkDialog != null)
		{
			energyDrinkDialog.OnEnergyDrinkSpendSuccessful -= EnergyDrinkSpendSuccessful;
			energyDrinkDialog.OnCloseButtonClicked -= EnergyDrinkDialogCloseButtonClicked;
		}
	}

	void Start () 
	{
		if (shopController == null)
			shopController = FindObjectOfType<ShopController>();

		if(SoundClick == null)
			SoundClick = GameObject.FindGameObjectWithTag("SoundClick").GetComponent<AudioSource>();

		upgradeItem = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType);

		if(upgradeItem.PriceIsEnergyDrink)
		{
			Vector3 butTextPos = UpgradeButtonText.transform.localPosition;
			butTextPos.x = -0.077f;
			UpgradeButtonText.transform.localPosition = butTextPos;

			UpgradeButtonText.transform.localScale = new Vector3(0.020198f, 0.040875f, 0.18325f);
		}
			
		if(upgradeItem.UpgradeCompleted() || upgradeItem.LevelRequired <= GoalManager.SharedInstance.goalDifficultyLevel)
		{
			LevelRequiredText.gameObject.SetActive(false);
		}
		else
		{
			UpgradeButton.gameObject.SetActive(false);

			if(shopController != null && shopController.shopUpgradeItemPanelLevelRequiredMaterial != null)
			{
				Transform upgradeGroupBackground = this.transform.Find("UpgradeGroupBackground");
				if(upgradeGroupBackground != null)
				{
					Renderer upgradeGroupBackgroundRenderer = upgradeGroupBackground.GetComponent<Renderer>();
					if(upgradeGroupBackgroundRenderer != null)
					{
						upgradeGroupBackgroundRenderer.material = shopController.shopUpgradeItemPanelLevelRequiredMaterial;
					}
			 	}
			}

			LevelRequiredText.gameObject.SetActive(true);
			LevelRequiredText.text = string.Format(GameLanguage.LocalizedText("LEVEL {0}\nREQUIRED"), upgradeItem.LevelRequired);
		}

		LoadItem();
	}
	

	void Update () {
		if(Input.GetMouseButtonUp(0) && !shopController.hasScrolled)
		{
			if(Camera.main == null || !Camera.main.isActiveAndEnabled)
				return;
			
			if(UIHelpers.CheckButtonHit(UpgradeButton.transform))
			{
				if(upgradeItem.UpgradeCompleted())
				{
					if(upgradeItem.IsCosmeticItem())
					{
						PlaySoundClick();
						EquipCosmeticItem();
					}

					if(upgradeItem.IsSurfBoardItem())
					{
						PlaySoundClick();
						EquipSurfBoardItem();
					}

					if(upgradeItem.IsJeepItem())
					{
						PlaySoundClick();
						EquipJeepItem();
					}
				}
				else
				{
					BuyItem();
					AnalyticsAndCrossPromoManager.SharedInstance.LogShopPurchase(upgradeItem, 0);
				}
			}
		}

	}

	void PlaySoundClick()
	{
		if(SoundClick != null)
		{
			GlobalSettings.PlaySound(SoundClick);
		}

	}

	public void ResetCosmeticEquipButton()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsCosmeticItem())
			ShowEquipButton();
	}

	void EquipCosmeticItem()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsCosmeticItem())
		{
			
			shopController.ResetAllCosmeticEquipButtons();

			ShowEquipedCheckMark();

			GameState.SharedInstance.CurrentCosmetic = upgradeItem.CosmeticItem.Value;
		}
	}

	public void ResetSurfBoardEquipButton()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsSurfBoardItem())
			ShowEquipButton();
	}

	void EquipSurfBoardItem()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsSurfBoardItem())
		{
			shopController.ResetAllSurfBoardEquipButtons();

			ShowEquipedCheckMark();

			GameState.SharedInstance.CurrentVehicle = upgradeItem.SurfBoardItem.Value;
		}
	}
		
	public void ResetJeepEquipButton()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsJeepItem())
			ShowEquipButton();
	}

	void EquipJeepItem()
	{
		if(upgradeItem.UpgradeCompleted() && upgradeItem.IsJeepItem())
		{
			shopController.ResetAllJeepEquipButtons();

			ShowEquipedCheckMark();

			GameState.SharedInstance.CurrentJeep = upgradeItem.JeepItem.Value;

			shopController.SetCurrentlySelectedJeepHeaderIcon();
		}
	}

	void BuyItem()
	{
		GameState gameState = GameState.SharedInstance;
		int? itemPrice = upgradeItem.GetNextUgpradePrice();

		if(itemPrice.HasValue && 
			((upgradeItem.PriceIsEnergyDrink && gameState.EnergyDrink >= itemPrice.Value) || (!upgradeItem.PriceIsEnergyDrink && gameState.Cash >= itemPrice.Value))
		)
		{
			upgradeItem.UpgradeToNextStage();

			if(upgradeItem.PriceIsEnergyDrink)
				gameState.LoseEnergyDrink(itemPrice.Value);
			else
			{
				gameState.LoseCash(itemPrice.Value);

				if( gameState.Upgrades.LastUpgradeItemTypePurchased == UpgradeItemType.None ||
					gameState.Upgrades.LastUpgradeItemCost <= itemPrice.Value)
				{
					gameState.Upgrades.LastUpgradeItemTypePurchased = upgradeItem.ItemType;
					gameState.Upgrades.LastUpgradeItemCost = itemPrice.Value;
				}
			}

			ClearItemWaitTimers();
				
			if(CashRegisterOpenSound != null)
				GlobalSettings.PlaySound(CashRegisterOpenSound);

			if(shopController != null)
			{
				shopController.RefreshCoinAndEnergyBars();
				shopController.EnactMulitplierEffect();
				shopController.ShowPurchaseSuccessful();
			}

			if(CoinBar != null)
				CoinBar.DisplayCoins(gameState.Cash);


			if(upgradeItem.IsCosmeticItem())
				EquipCosmeticItem();

			if(upgradeItem.IsSurfBoardItem())
				EquipSurfBoardItem();

			if(upgradeItem.IsJeepItem())
				EquipJeepItem();

			LoadItem();

			// Update min cost variable
			GameState.SharedInstance.Upgrades.CheckAndStoreMinCostOfNextUpgrade();

		}
		else if(upgradeItem.PriceIsEnergyDrink)
		{
			if(shopController != null && shopController.energyDrinkShopController != null)
				shopController.energyDrinkShopController.Show(shopController.mainCamera);
		}
		else if(energyDrinkDialog != null && itemPrice.HasValue)
		{
			int coinRequired = itemPrice.Value - gameState.Cash;
			energyDrinkDialog.SetCoinRequiredText(coinRequired);

			int energyDrinkRequired = gameState.ConvertGoldToEnergyDrink(coinRequired);
			energyDrinkDialog.SetEnergyDrinkCostText(energyDrinkRequired);

			energyDrinkSwapAutoPurchaseItem = true;
			energyDrinkDialog.Show();

			if (shopController != null)
				shopController.HidePurchaseSuccessful();
		}	
	}

	void LoadItem()
	{
		GameState gameState = GameState.SharedInstance;

		if(upgradeItem == null)
			upgradeItem = gameState.Upgrades.UpgradeItemByType(UpgradeItemType);

		if(upgradeItem.UpgradeCompleted())
		{
			UpgradeButtonText.text = "";

			if(UpgradeLabelText != null)
			{
				UpgradeLabelText.SetActive(false);
			}

			if(upgradeItem.IsSurfBoardItem() && upgradeItem.SurfBoardItemCurrentlyEquiped())
			{
				ShowEquipedCheckMark();
			}
			else if(upgradeItem.IsCosmeticItem() && upgradeItem.CosmeticItemCurrentlyEquiped())
			{
				ShowEquipedCheckMark();
			}
			else if(upgradeItem.IsJeepItem() && upgradeItem.JeepItemCurrentlyEquiped())
			{
				ShowEquipedCheckMark();
			}
			else if(upgradeItem.IsCosmeticItem() || upgradeItem.IsSurfBoardItem() || upgradeItem.IsJeepItem())
			{
				ShowEquipButton();
			}
			else
			{
				ShowEquipedCheckMark();
			}
		}
		else
		{
			int? nextUpgradePrice = upgradeItem.GetNextUgpradePrice();
			if(nextUpgradePrice.HasValue)
			{
				UpgradeButtonText.text = nextUpgradePrice.Value.ToString();
			}
			else
			{
				UpgradeButtonText.text = "";
			}

			if(UpgradeLabelText != null)
			{
				UpgradeLabelText.SetActive(true);
			}

			if(upgradeItem.PriceIsEnergyDrink)
				ShowUpgradeButtonFrame(2);
			else
				ShowUpgradeButtonFrame(0);
		}

		if(UpgradeFillBar != null)
		{
			UpgradeFillBar.SetHorizBarAtValue(1f - ((float)upgradeItem.CurrentUpgradeStage/(float)upgradeItem.TotalUpgradeStages));
		}
	}
		
	void EnergyDrinkSpendSuccessful (int energyDrinksUsed)
	{
		if(energyDrinkSwapAutoPurchaseItem)
		{
			energyDrinkSwapAutoPurchaseItem = false;
			BuyItem();
			AnalyticsAndCrossPromoManager.SharedInstance.LogShopPurchase(upgradeItem, energyDrinksUsed);

		}
	}

	void EnergyDrinkDialogCloseButtonClicked()
	{
		if(energyDrinkSwapAutoPurchaseItem)
			energyDrinkSwapAutoPurchaseItem = false;
	}

	void ClearItemWaitTimers()
	{
		UpgradeItemType itemType = upgradeItem.ItemType;

		if (itemType == UpgradeItemType.JeepFuel ||
			itemType == UpgradeItemType.JeepEngine ||
			itemType == UpgradeItemType.JeepWheels)
		{
			GameState.SharedInstance.JeepTimeStamp = DateTime.UtcNow.AddMinutes(-1).ToBinary().ToString();
		}
		else if (itemType == UpgradeItemType.SkateDuration || itemType == UpgradeItemType.SkateGold)
		{
			GameState.SharedInstance.HalfPipeTimeStamp = DateTime.UtcNow.AddMinutes(-1).ToBinary().ToString();
		}
	}

	void ShowEquipButton()
	{
		ShowUpgradeButtonFrame(3);

		string equipText = GameLanguage.LocalizedText("EQUIP");

		Vector3 centerXPos = UpgradeButtonText.transform.localPosition;
		centerXPos.x = 0;
		UpgradeButtonText.transform.localPosition = centerXPos;
		UpgradeButtonText.color = new Color32(244, 217, 14, 255);
		if(!string.IsNullOrEmpty(equipText) && equipText.Length > 9)
		{
			UpgradeButtonText.transform.localScale = new Vector3(0.014675f, 0.03408f, 0.03408f);
		}

		UpgradeButtonText.text = equipText;
	}

	void ShowEquipedCheckMark()
	{
		UpgradeButtonText.text = "";
		ShowUpgradeButtonFrame(1);
	}

	void ShowUpgradeButtonFrame(int frameNumber)
	{
		if(UpgradeButton != null && UpgradeButton.gameObject.activeSelf)
			UpgradeButton.DrawFrame(frameNumber);
	}
}
