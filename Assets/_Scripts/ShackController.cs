﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Prime31;
using TMPro;
using PaperPlaneTools;

public class ShackController : MonoBehaviour {

	public GameCenterPluginManager gcpm;
	public Camera mainCamera;
    public GameObject tvClickRegion;
    public GameObject skateboard;
    public PhotoAlbumManager customPicture;

    public GameObject leaderboardSurfButton;
    public GameObject leaderboardJeepButton;
    public GameObject achievementButton;
    public GameObject rateButton;
    public GameObject doorRegion;
    public GameObject radioButton;
    public GameObject stickManAsleep;
    public GameObject stickManAwake;
    public GameObject fanObject;

	public GameObject gameController;
    public AnimatingObject tvCabinet;

	public GameObject fidgetSpinner;


    public GameObject stickManCoin;
	private Material coinMaterial;
	private Color origCoinColor;
	private float curCoinMoveTime = 0.0f;
	private float coinMoveTime = 1.5f;
	private Vector3 startCoinPos;
	private Vector3 endCoinPos;
	private bool coinActive = false;

    public AudioSource stickManWakeUpSound;

	public Cockroach cockroachManager;
    public GameObject cockroachObject;

    public AudioSource clickSound;

    private string imagePath = "";
 
    public AudioSource[] snoreWakeSounds;
    public AudioSource fartSound;
    public AudioSource snoringLoopSound;

    public AudioSource reggaeMusic;
    public AudioSource inGameMusic;
    public AudioSource miniGameMusic;
    public AudioSource tuneRadioSound;
    public AudioSource waveSounds;
    public AudioSource coinNoise;
    public AudioSource fanNoise;

    AudioSource currentMusic;
    private bool wakeUp = false;
    float wakeUpTimer = 0;
    float wakeUpDelay = 1.6f;

    //bool tvCabinetOpen = false;

    public GameConsoleTimer gameConsoleTimer;
    public TextMeshPro gameConsoleTimerText;
    public TextMeshPro gameConsoleDialogTimerText;
	public MenuItemMarker gameConsoleActivityMarker;
    
    public HalfPipeTimer halfpipeTimer;
    public TextMeshPro halfPipeTimerText;
    public TextMeshPro halfPipeDialogTimerText;
    public MenuItemMarker skateboardActivityMarker;

	public EnergyDrinkDialog skateboardEnergyDrinkDialog;
	public EnergyDrinkDialog gameConsoleEnergyDrinkDialog;
	GameCenterPluginManager gameCenterPluginManager = null;

	bool rateGameDialogTrigger = false;

	void OnEnable () 
	{
		AlertDialogUtil.RegisterAlertButtonClickHandler(EtceteraManagerAlertButtonClickedEvent);

		if(skateboardEnergyDrinkDialog != null)
			skateboardEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful += SkateboardEnergyDrinkDialogEnergyDrinkSpendSuccessful;

		if (gameConsoleEnergyDrinkDialog != null)
			gameConsoleEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful += GameConsoleEnergyDrinkDialogEnergyDrinkSpendSuccessful;
	}

	void OnDisable () 
	{
		AlertDialogUtil.DeregisterAlertButtonClickHandler(EtceteraManagerAlertButtonClickedEvent);

		if(skateboardEnergyDrinkDialog != null)
			skateboardEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful -= SkateboardEnergyDrinkDialogEnergyDrinkSpendSuccessful;

		if (gameConsoleEnergyDrinkDialog != null)
			gameConsoleEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful -= GameConsoleEnergyDrinkDialogEnergyDrinkSpendSuccessful;
	}
           
    void Start()
    {
		ServerSavedGameStateSync.Sync();

        Time.timeScale = 1;
		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();

		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementHomeBody();

		coinMaterial = stickManCoin.GetComponent<Renderer>().material;
		origCoinColor = coinMaterial.color;

		startCoinPos = stickManCoin.transform.position;
		endCoinPos = stickManCoin.transform.position + Vector3.up * 10.0f;

		stickManCoin.SetActive(false);

		if(skateboardEnergyDrinkDialog == null)
			skateboardEnergyDrinkDialog = FindObjectOfType<EnergyDrinkDialog>();

        currentMusicIndex = GlobalSettings.currentMenuMusicIndex;
        //////Debug.Log("GlobalSettings.currentMenuMusicIndex:" + GlobalSettings.currentMenuMusicIndex);
        if (GlobalSettings.currentMenuMusicIndex == 0)
        {
            currentMusic = reggaeMusic;
			GlobalSettings.PlayMusicFromTimePoint(reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
        }
        else if (GlobalSettings.currentMenuMusicIndex == 1)
        {
            currentMusic = miniGameMusic;
			GlobalSettings.PlayMusicFromTimePoint(miniGameMusic, GlobalSettings.miniGameMusicPlayPosition);
        }
        else if (GlobalSettings.currentMenuMusicIndex == 2)
        {
            currentMusic = inGameMusic;
			GlobalSettings.PlayMusicFromTimePoint(inGameMusic, GlobalSettings.inGameMusicPlayPosition);
        }
        if (!GlobalSettings.musicEnabled)
        	waveSounds.Stop();

	 	//int cockroachChance = Random.Range(0, 3);
	 	//if (cockroachChance == 0)
	 	{
	 		cockroachManager.Initialise();
	 	}
        stickManAwake.SetActive(false);
        stickManAsleep.SetActive(true);



        tvCabinet.DrawFrame(9); // 4
        

        GlobalSettings.PlaySound(snoringLoopSound);

		skateboardActivityMarker.gameObject.SetActive(false);
        gameConsoleActivityMarker.gameObject.SetActive(false);


		UpgradeItem hasFan = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.RetroFan);
        bool hasFanTrue = false;
        if (hasFan != null)
            hasFanTrue = (hasFan.CurrentUpgradeStage != 0);


        if (!CheckIf169Aspect())
        {
            // move the items for ipad and similar aspect ratios

            // leaderboards (-304, 243)
            // achievements (-391, 229)
            // rate 410, -118
            leaderboardSurfButton.transform.position = new Vector3(-304,243,leaderboardSurfButton.transform.position.z);
            achievementButton.transform.position = new Vector3(-400,225,achievementButton.transform.position.z);

            //if (!hasFanTrue)
	          //  rateButton.transform.position = new Vector3(410,-118,achievementButton.transform.position.z);
	        //else
				fanObject.transform.position = new Vector3(410,-118,achievementButton.transform.position.z);
			mainCamera.orthographicSize = 360;
        }

		if (hasFanTrue)
        {
			fanObject.SetActive(true);
			GlobalSettings.PlaySound(fanNoise);
        }
        else
        {
        	rateButton.transform.position = fanObject.transform.position;
        	fanObject.SetActive(false);
        }



    }

	void SkateboardEnergyDrinkDialogEnergyDrinkSpendSuccessful (int energyDrinksUsed)
	{
		AnalyticsAndCrossPromoManager.SharedInstance.LogSkateboardTimerSkipped(energyDrinksUsed);
        #if UNITY_ANDROID
        AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

        if(droidLoader != null)
        {
            droidLoader.Show();
        }
        #endif
		SceneNavigator.NavigateTo(SceneType.HalfPipe);
	}

	void GameConsoleEnergyDrinkDialogEnergyDrinkSpendSuccessful (int energyDrinksUsed)
	{
		AnalyticsAndCrossPromoManager.SharedInstance.LogGameConsoleSkipped(energyDrinksUsed);
        #if UNITY_ANDROID
        AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

        if(droidLoader != null)
        {
            droidLoader.Show();
        }
        #endif
		SceneNavigator.NavigateTo(SceneType.RetroGame);
	}


    public bool CheckIf169Aspect()
    {
        float width = Mathf.Max(Screen.width, Screen.height);
        float height = Mathf.Min(Screen.width, Screen.height);

        // i5,6 = 1.77777777 (16:9)
        // i4 = 1.5 (3:2)
        // Ipad = 1.3333 (4:3)
        if (width / height < 1.6f)
        {
            return false;            
        }
        else
        {
            return true;            
        }

    }

    void ActivateFartCoin()
    {
		if (!coinActive)
		{
			curCoinMoveTime = coinMoveTime;
			GlobalSettings.PlaySound(coinNoise);
			stickManCoin.SetActive(true);
			coinActive = true;
			GameState.SharedInstance.AddCash(1);
		}	
    }

    AudioSource PlayRandomWakeSound()
    {
        snoringLoopSound.Pause();
        int randIndex = Random.Range(0,snoreWakeSounds.Length + 1);
		if (randIndex == snoreWakeSounds.Length)
		{
			GlobalSettings.PlaySound(fartSound);
			ActivateFartCoin();
			return fartSound;
		}
		else
		{
			GlobalSettings.PlaySound(snoreWakeSounds[randIndex]);
			return snoreWakeSounds[randIndex];

		}
    }

    AudioSource PlayWakeSound(int soundIndex)
    {
        snoringLoopSound.Pause();
        GlobalSettings.PlaySound(snoreWakeSounds[soundIndex]);
        return snoreWakeSounds[soundIndex];
    }

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.CommonDialogShowing() == false || DialogTrackingFlags.EnergyDrinkCostDialog == false)
			{
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Menu);
			}
		}
		#endif 
	}

    void Update()
    {
		CheckAndroidBackButton();

		if (coinActive)
		{
			if (curCoinMoveTime >= 0)
			{
				float lerpValue = curCoinMoveTime / coinMoveTime;
				curCoinMoveTime -= Time.deltaTime;

				if (curCoinMoveTime < 0)
					lerpValue = 0;
					
				stickManCoin.transform.position = Vector3.Lerp(startCoinPos, endCoinPos, 1.0f - Mathf.Pow(lerpValue, 3.0f));
				coinMaterial.color = new Color(origCoinColor.r, origCoinColor.g, origCoinColor.b, 
													lerpValue);
			}
		}

    	

        if (wakeUp)
        {
            wakeUpTimer += Time.deltaTime;
            if (wakeUpTimer > wakeUpDelay)
            {
                wakeUpTimer = 0;
                wakeUp = false;
                stickManAwake.SetActive(false);
                stickManAsleep.SetActive(true);
                GlobalSettings.PlaySound(snoringLoopSound);
            }
        }
		if (Input.GetMouseButtonDown(0) && mainCamera.enabled)
        {
            //Debug.Log("CLICKING HERE");
            /*
            if (UIHelpers.CheckButtonHit(tvClickRegion.transform))
            {
                GlobalSettings.PlaySound(clickSound);
                StartCoroutine(PlayStreamingVideo("SurferFull.mp4"));
                Debug.Log("CLICKED TV");
                //Handheld.PlayFullScreenMovie ("SurferMovie.mov", Color.black, FullScreenMovieControlMode.CancelOnInput);
                //string url = System.IO.Path.Combine(Application.streamingAssetsPath, "SurferMovie.mp4");
                // string url = System.IO.Path.Combine( Application.dataPath, "Raw/SurfVideo.mp4" );
                //EtceteraTwoBinding.playMovie(url, true, true, false);
            }
			else 
            */

            if (UIHelpers.CheckButtonHit(fidgetSpinner.transform))
			{
				Debug.Log("Fidget spinner clicked");
				GlobalSettings.PlaySound(clickSound);

				if (GlobalSettings.currentMenuMusicIndex == 0)
                {
                    GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
                }
                else if (GlobalSettings.currentMenuMusicIndex == 1)
                {
                    GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
                }
                else if (GlobalSettings.currentMenuMusicIndex == 2)
                {
                    GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
                }

				#if UNITY_ANDROID
				AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

				if(droidLoader != null)
				{
				droidLoader.Show();
				}
				#endif
				SceneNavigator.NavigateTo(SceneType.FidgetSpinner);
			}
            else if ( UIHelpers.CheckButtonHit(tvClickRegion.transform) ||
                      UIHelpers.CheckButtonHit(tvCabinet.transform) ||
            		( UIHelpers.CheckButtonHit(gameController.transform)))
            {
                Debug.Log("Clicked TV Cabinet");
				if(gameConsoleTimer.TimeStatus)
                {
					#if UNITY_ANDROID
                    AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                    if(droidLoader != null)
                    {
                        droidLoader.Show();
                    }
                    #endif
					SceneNavigator.NavigateTo(SceneType.RetroGame);
                
                    //if (!tvCabinetOpen)
                    //{
                     //   GlobalSettings.PlaySound(clickSound);
                     //   tvCabinet.DrawFrame(9);// = 4;
                     //   tvCabinetOpen = true;
                    //}
                    //else
                    //{
                       // GlobalSettings.PlaySound(clickSound);
                        //tvCabinet.DrawFrame(5);// = 4;
                        //tvCabinetOpen = false;
                    //}
                }
                else
                {
					gameConsoleEnergyDrinkDialog.SetEnergyDrinkCostText(GameServerSettings.SharedInstance.SurferSettings.GameConsoleNowEnergyDrinkCost);
					gameConsoleEnergyDrinkDialog.Show();
                }
            }
            else if (UIHelpers.CheckButtonHit(skateboard.transform))
            {
				GlobalSettings.PlaySound(clickSound);

				if(halfpipeTimer.TimeStatus)
				{
                    #if UNITY_ANDROID
                    AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                    if(droidLoader != null)
                    {
                        droidLoader.Show();
                    }
                    #endif
					SceneNavigator.NavigateTo(SceneType.HalfPipe);
				}
				else
				{
					skateboardEnergyDrinkDialog.SetEnergyDrinkCostText(GameServerSettings.SharedInstance.SurferSettings.SkateNowEnergyDrinkCost);
					skateboardEnergyDrinkDialog.Show();
				}
            }
            else if (UIHelpers.CheckButtonHit(customPicture.transform))
            {
				// quang ignore android for now until plugin works.
				#if UNITY_IOS
                GlobalSettings.PlaySound(clickSound);
                //////Debug.Log("HIT PICTURE HERE");
                customPicture.LoadPictureFromAlbum();
				#endif 
            }
            else if (UIHelpers.CheckButtonHit(cockroachObject.transform))
            {
           		cockroachManager.Splat();
            }
			else if (UIHelpers.CheckButtonHit(leaderboardSurfButton.transform))
			{
                GlobalSettings.PlaySound(clickSound);
				gcpm.ShowLeaderboard(LeaderboardType.SurfLeaderBoard);
			}
			else if (UIHelpers.CheckButtonHit(leaderboardJeepButton.transform))
			{
                GlobalSettings.PlaySound(clickSound);
				gcpm.ShowLeaderboard(LeaderboardType.JeepLeaderBoard);
			}
			else if (UIHelpers.CheckButtonHit(achievementButton.transform))
			{
                GlobalSettings.PlaySound(clickSound);
				gcpm.ShowAchievements();
			}
			else if (UIHelpers.CheckButtonHit(rateButton.transform))
			{
                Debug.Log("RATE BUTTON");
                GlobalSettings.PlaySound(clickSound);

				rateGameDialogTrigger = true;
				float fver1 = 0.0f;
				float fver2  = 0.0f;
				float.TryParse(UnityEngine.iOS.Device.systemVersion.Split('.')[0], out fver1);
				float.TryParse(UnityEngine.iOS.Device.systemVersion.Split('.')[1], out fver2);

                Debug.Log("IN HERE FV1:" + fver1 + " fver2:" + fver2);

				if (fver1 >= 10)
				{
                    
					if (fver1 == 10 && fver2 < 3)
					{
						AlertDialogUtil.ShowAlert(GameLanguage.LocalizedText("Rate Us"), 
						GameLanguage.LocalizedText("Having fun? Rate the game so we can bring you more cool updates."), 
						GameLanguage.LocalizedText("Cancel"), 
						"OK");
					}
					else
					{
						RateBox.Instance.Statistics.DialogIsRejected = false;
						RateBox.Instance.Statistics.DialogIsRated = false;
						RateBox.Instance.Show(GameLanguage.LocalizedText("Rate Us"), 
							GameLanguage.LocalizedText("Having fun? Rate the game so we can bring you more cool updates."), 
							GameLanguage.LocalizedText("Cancel"),
							"OK");
					}
				}
				else
				{
					AlertDialogUtil.ShowAlert(GameLanguage.LocalizedText("Rate Us"), 
						GameLanguage.LocalizedText("Having fun? Rate the game so we can bring you more cool updates."), 
						GameLanguage.LocalizedText("Cancel"), 
						"OK");
				}

			}
            else if (UIHelpers.CheckButtonHit(radioButton.transform))
            {
                PlayNextMusic();
            }
            else if(UIHelpers.CheckButtonHit(stickManAsleep.transform))
            {
                WakeUpStick();
            }
            else if(UIHelpers.CheckButtonHit(doorRegion.transform))
            {
                GlobalSettings.PlaySound(clickSound);
                if (GlobalSettings.currentMenuMusicIndex == 0)
                {
                    GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
                }
                else if (GlobalSettings.currentMenuMusicIndex == 1)
                {
                    GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
                }
                else if (GlobalSettings.currentMenuMusicIndex == 2)
                {
                    GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
                }
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Menu);
            } 
        }
    }

    public void SetHalfPipeTimerText(string theTimerText)
    {
        halfPipeTimerText.text = theTimerText;
		halfPipeDialogTimerText.text = theTimerText;
    }

    public void HideHalfpipeTimerDialog()
    {
		if (skateboardEnergyDrinkDialog != null)
		{
			skateboardEnergyDrinkDialog.Hide();
		}
    }

	public void SetGameConsoleTimerText(string theTimerText)
    {
		gameConsoleTimerText.text = theTimerText;
		gameConsoleDialogTimerText.text = theTimerText;
    }

	public void HideGameConsoleTimerDialog()
    {
		if (gameConsoleEnergyDrinkDialog != null)
		{
			gameConsoleEnergyDrinkDialog.Hide();
		}
    }



    public void EnableSkateboardActivityMarker()
    {
        skateboardActivityMarker.gameObject.SetActive(true);
    }

    public void EnableGameConsoleActivtyMarker()
    {
    	gameConsoleActivityMarker.gameObject.SetActive(true);
    }

    int currentMusicIndex = 0;
    void PlayNextMusic()
    {
        if (GlobalSettings.currentMenuMusicIndex == 0)
        {
            GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
        }
        else if (GlobalSettings.currentMenuMusicIndex == 1)
        {
            GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
        }
        else if (GlobalSettings.currentMenuMusicIndex == 2)
        {
            GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
        }

        GlobalSettings.PlaySound(tuneRadioSound);

        currentMusic.Stop();
        if (currentMusicIndex == 0)
        {
            ////////Debug.Log("GlobalSettings.miniGameMusicPlayPosition:" + GlobalSettings.miniGameMusicPlayPosition);
            if (GlobalSettings.musicEnabled)
            	StartCoroutine(PlayTheSoundLateFromPosition(miniGameMusic, 0.4F, GlobalSettings.miniGameMusicPlayPosition));
            //WakeUpStick(1);
            currentMusic = miniGameMusic;

            currentMusicIndex++;
            GlobalSettings.currentMenuMusicIndex = currentMusicIndex;
        }
        else if (currentMusicIndex == 1)
        {
            ////////Debug.Log("GlobalSettings.inGameMusicPlayPosition:" + GlobalSettings.inGameMusicPlayPosition);
			if (GlobalSettings.musicEnabled)
            	StartCoroutine(PlayTheSoundLateFromPosition(inGameMusic, 0.4F, GlobalSettings.inGameMusicPlayPosition));
            //WakeUpStick(2);
            currentMusic = inGameMusic;
          
            currentMusicIndex++;
            GlobalSettings.currentMenuMusicIndex = currentMusicIndex;
        }
        else
        {
            ////////Debug.Log("GlobalSettings.reggaeMusicPlayPosition:" + GlobalSettings.reggaeMusicPlayPosition);
			if (GlobalSettings.musicEnabled)
            	StartCoroutine(PlayTheSoundLateFromPosition(reggaeMusic, 0.4F, GlobalSettings.reggaeMusicPlayPosition));
            currentMusic = reggaeMusic;

            currentMusicIndex = 0;
            GlobalSettings.currentMenuMusicIndex = currentMusicIndex;
        }

        //GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
        //GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
        //GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
    }


    void WakeUpStick(int wakeSoundIndex = -1)
    {
        stickManAsleep.SetActive(false);
        stickManAwake.SetActive(true);
        wakeUpTimer = 0;
        if (wakeSoundIndex == -1)
        {
            wakeUpDelay = PlayRandomWakeSound().clip.length;
        }
        else
        {
            wakeUpDelay = PlayWakeSound(wakeSoundIndex).clip.length;
        }
        wakeUp = true;
    }

    private IEnumerator PlayStreamingVideo(string url)
    {
        Handheld.PlayFullScreenMovie(url, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        //////Debug.Log("Video playback completed.");

    }


    private IEnumerator PlayTheSoundLateFromPosition(AudioSource theSource, float timeTillPlay, float timeInTrack)
    {
        yield return new WaitForSeconds(timeTillPlay);
        theSource.time = timeInTrack;
        theSource.Play();
    }

	void EtceteraManagerAlertButtonClickedEvent (string obj)
	{
		if(rateGameDialogTrigger && obj.ToLowerInvariant() == "ok")
		{
			gcpm.RateGame();
		}

		rateGameDialogTrigger = false;
	}

}
