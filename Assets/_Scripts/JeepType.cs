﻿using System;
using UnityEngine;
using System.Collections;

public enum JeepType
{
	Default = 0,
	Black = 1,
    QuadBike = 2
}
