﻿using UnityEngine;
using System.Collections;

public class ShopCameraController : MonoBehaviour {

    private float min_X = 0; 
    private float max_X = 0;
    private Vector3 lastPositionWorldPos;
    private Camera currentCamera;
    private bool inputDown = false;
    public bool acceptingInput = true;

    public void Start()
    {
        currentCamera = GetComponent<Camera>();
    }
      
    public void ResetXPosOnCamera()
    {
        min_X = ShopScript.MasterPanelXPos;
        currentCamera.transform.position = new Vector3(min_X, currentCamera.transform.position.y, currentCamera.transform.position.z);
    }

    public float GetHalfViewportWidthInWorldUnits()
    {
        Vector3 topRightViewportWorldXPos = currentCamera.ScreenToWorldPoint (new Vector3(Screen.width, Screen.height, currentCamera.nearClipPlane)); 
        Vector3 topDeadCenterViewportWorldXPos = currentCamera.ScreenToWorldPoint (new Vector3(Screen.width/2, Screen.height, currentCamera.nearClipPlane)); 
        return topRightViewportWorldXPos.x - topDeadCenterViewportWorldXPos.x;
    }

    public void CalculateXBounds(int numberOfPanels)
    {
        min_X = ShopScript.MasterPanelXPos;
        float halfViewWidth = GetHalfViewportWidthInWorldUnits();
        float sidePadding = 0.5f * ShopScript.XPosPlacement;
        max_X = (min_X + (numberOfPanels-1) * ShopScript.XPosPlacement) - halfViewWidth + sidePadding;
        min_X = ShopScript.MasterPanelXPos + halfViewWidth - sidePadding;
    }

    public void ApplyCameraBounds()
    {
        if(currentCamera.transform.position.x < min_X)
        {
            currentCamera.transform.position = new Vector3(min_X, currentCamera.transform.position.y, currentCamera.transform.position.z);
        }
        if(currentCamera.transform.position.x > max_X)
        {
            currentCamera.transform.position = new Vector3(max_X, currentCamera.transform.position.y, currentCamera.transform.position.z);
        }
    }

    public void UpdateCamera () 
    {
        if (Input.GetMouseButton(0) && acceptingInput)
        {
            if (inputDown)
            {
                Vector3 newPos = transform.position;

                Vector3 mouseWorldPos = currentCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, currentCamera.nearClipPlane));

                float offsetX = lastPositionWorldPos.x - mouseWorldPos.x;

                lastPositionWorldPos = mouseWorldPos;
                lastPositionWorldPos = new Vector3(lastPositionWorldPos.x + offsetX, lastPositionWorldPos.y, lastPositionWorldPos.z);

                currentCamera.transform.position = new Vector3(currentCamera.transform.position.x + offsetX, currentCamera.transform.position.y, currentCamera.transform.position.z);

                // snap to min and max x bounds if gone past them
                ApplyCameraBounds();
            }
            else
            {
                inputDown = true;
                lastPositionWorldPos = currentCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, currentCamera.nearClipPlane));
            }
        }
        else
        {
            inputDown = false;
        }
    }
}
    