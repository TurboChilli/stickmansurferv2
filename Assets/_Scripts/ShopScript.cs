﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ShopScript : MonoBehaviour {

    public MasterShopPanel masterShopPanel;
    public ShopCameraController cameraController;
    public ShopItemInfoPanel shopInfoPanel;
    public InfoPopupPanel infoPopupPanel;
    public GameObject shopTab1;
    public GameObject shopTab2;
    public GameObject shopTab3;
    public GameObject shopTab1Selected;
    public GameObject shopTab2Selected;
    public GameObject shopTab3Selected;
    public GameObject selectedButton;
    public GameObject backButton;
    public GameObject shopInfoPopupPanel;
    public GameObject noCoinInfoPopup;
    public TextMesh coinAmountText;
    public TextMesh playerLevel;

    private List<VehicleInfo> vehicleInfoItems = null;
    private VehicleInfo vehicleInfo = null;
    private static float masterShopPanelXPos = 0;
    private float masterShopPanelYPos = 0;
    private float masterShopPanelZPos = 0;
    private int vehicleBuyIndex = -1;
    private int clickIndex = -1; 

    private static float xPosPanelPlacement = 2.29f; 

    private float masterShopPanelCameraXPos = 0;
    private float category1PanelCameraYPos = 1.27f;
    private float category2PanelCameraYPos = -8;
    private float category3PanelCameraYPos = -18;

    private MasterShopPanel[] shopCategory1Panels = null;
    private MasterShopPanel[] shopCategory2Panels = null;
    private MasterShopPanel[] shopCategory3Panels = null;

    private List<GameObject> selectButtonsCategory1 = new List<GameObject>();
    //private List<GameObject> selectButtonsCategory2 = new List<GameObject>();
    //private List<GameObject> selectButtonsCategory3 = new List<GameObject>();  

    private List<GameObject> selectedButtonsCategory1 = new List<GameObject>();
    //private List<GameObject> selectedButtonsCategory2 = new List<GameObject>();
    //private List<GameObject> selectedButtonsCategory3 = new List<GameObject>();     

    private List<GameObject> levelRequiredButtonsCategory1 = new List<GameObject>();
    //private List<GameObject> levelRequiredButtonsCategory2 = new List<GameObject>();
    //private List<GameObject> levelRequiredButtonsCategory3 = new List<GameObject>();     

    private List<GameObject> buyButtonsCategory1 = new List<GameObject>();
    //private List<GameObject> buyButtonsCategory2 = new List<GameObject>();
    //private List<GameObject> buyButtonsCategory3 = new List<GameObject>();  



    public enum ShopSceneState
    {
        DefaultShopScene
    }
    private ShopSceneState currShopSceneState = ShopSceneState.DefaultShopScene;



    public enum ShopCategoryState
    {
        DefaultShopCategory,
        Vehicles,
        Category2,
        Category3
    }
    private ShopCategoryState currShopCategoryState = ShopCategoryState.Vehicles;



    private enum ShopState
    {
        BrowsingState,
        PurchaseConfirmationState
    }
    private ShopState currShopState;



    public static float XPosPlacement
    {
        get
        {
            return xPosPanelPlacement;
        }
    }



    public static float MasterPanelXPos
    {
        get
        {
            return masterShopPanelXPos;
        }
    }
       


	// Use this for initialization
	void Start () {
        
        vehicleInfoItems = GameState.SharedInstance.GetAllVehicleInfo();

        shopInfoPanel.ShowHidePopupItemInfoPanel(shopInfoPopupPanel, false);
        infoPopupPanel.ShowHidePopupMessagePanel(noCoinInfoPopup, false);

        currShopState = ShopState.BrowsingState;

        // get the position of the master panel as our population starting point
        masterShopPanelXPos = masterShopPanel.transform.position.x;
        masterShopPanelYPos = masterShopPanel.transform.position.y;
        masterShopPanelZPos = masterShopPanel.transform.position.z;



        cameraController.ResetXPosOnCamera();
        cameraController.CalculateXBounds(vehicleInfoItems.Count);
        cameraController.ApplyCameraBounds();



        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 0.8f, 0.82f, shopTab1.transform, Camera.main); 
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 2.375f, 0.82f, shopTab2.transform, Camera.main); 
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 3.95f, 0.82f, shopTab3.transform, Camera.main); 



        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 0.8f, 0.82f, shopTab1Selected.transform, Camera.main); 
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 2.375f, 0.82f, shopTab2Selected.transform, Camera.main); 
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 3.95f, 0.82f, shopTab3Selected.transform, Camera.main); 



        // deactivate shop tab 1 and activate the selected tab as default
        shopTab1.SetActive(false);
        shopTab1Selected.SetActive(true);
        shopTab2Selected.SetActive(false);
        shopTab3Selected.SetActive(false);



        // populate shop Category1 panels
        PopulateVehicleData(masterShopPanelXPos, masterShopPanelYPos, masterShopPanelZPos, 
                            ref shopCategory1Panels, selectButtonsCategory1, selectedButtonsCategory1, 
                            levelRequiredButtonsCategory1, buyButtonsCategory1);

        /*
         * 
         * 
         * 
         * Call PopulateVehicleData here for the other 2 categories
         * 
         * 
         * 
         */


		coinAmountText.text = string.Format("{0:#,0}", GameState.SharedInstance.Cash);
        //playerLevel.text = "Player level " + GameState.SharedInstance.PlayerLevel.CurrentLevel.ToString();



        masterShopPanel.gameObject.SetActive(false);
	}
	


	// Update is called once per frame
	void Update () {
	    
        // reposition camera toward product panels on corresponding category button click
        if (currShopSceneState == ShopSceneState.DefaultShopScene)
        {    
            
            if (Input.GetMouseButtonDown (0)) 
            {

                if (currShopState == ShopState.BrowsingState)
                {

                    if (UIHelpers.CheckButtonHit (shopTab1.transform) || UIHelpers.CheckButtonHit (shopTab1Selected.transform)) 
                    {
                        Camera.main.transform.position = new Vector3(masterShopPanelCameraXPos, category1PanelCameraYPos, Camera.main.transform.position.z);
                        cameraController.ResetXPosOnCamera();
                        cameraController.CalculateXBounds(shopCategory1Panels.Length);
                        currShopCategoryState = ShopCategoryState.Vehicles;

                        if (currShopCategoryState == ShopCategoryState.Vehicles)
                        {  
                            ResetTabButtons();
                            shopTab1.SetActive(false);
                            shopTab1Selected.SetActive(true);
                        }
                    } 

                    else if (UIHelpers.CheckButtonHit (shopTab2.transform)) 
                    {
                        Camera.main.transform.position = new Vector3(masterShopPanelCameraXPos, category2PanelCameraYPos, Camera.main.transform.position.z);
                        cameraController.ResetXPosOnCamera();
                        cameraController.CalculateXBounds(shopCategory2Panels.Length);
                        currShopCategoryState = ShopCategoryState.Category2;

                        if (currShopCategoryState == ShopCategoryState.Category2)
                        {   
                            ResetTabButtons();
                            shopTab2.SetActive(false);
                            shopTab2Selected.SetActive(true);
                        }
                    }

                    else if (UIHelpers.CheckButtonHit (shopTab3.transform)) 
                    {
                        Camera.main.transform.position = new Vector3(masterShopPanelCameraXPos, category3PanelCameraYPos, Camera.main.transform.position.z);
                        cameraController.ResetXPosOnCamera();
                        cameraController.CalculateXBounds(shopCategory3Panels.Length);
                        currShopCategoryState = ShopCategoryState.Category3;

                        if (currShopCategoryState == ShopCategoryState.Category3)
                        {   
                            ResetTabButtons();
                            shopTab3.SetActive(false);
                            shopTab3Selected.SetActive(true);
                        }
                    }

                    // check for back button hit
                    else if (UIHelpers.CheckButtonHit(backButton.transform))
                    {
                        SceneNavigator.NavigateToPreviousScene();
                    }

                    else if (currShopCategoryState == ShopCategoryState.Vehicles)
                    {                    
                        DetectButton(buyButtonsCategory1, shopCategory1Panels);
                        DetectButton(selectButtonsCategory1, shopCategory1Panels); 
                    }
                    /*
                     *
                     *
                     * 
                     * Add conditions here for other 2 categories
                     *
                     *
                     *
                     */
                }

                else if (currShopState == ShopState.PurchaseConfirmationState)
                {

                    if (shopInfoPanel.IsInfoPanelBuyItButtonClicked())
                    {
                        
                        vehicleInfoItems[vehicleBuyIndex].Owned = true;
                        
                        GameState.SharedInstance.CurrentVehicle = vehicleInfoItems[vehicleBuyIndex].Type;
						coinAmountText.text = string.Format("{0:#,0}", GameState.SharedInstance.Cash);

                        shopCategory1Panels[vehicleBuyIndex].SetPanelState(MasterShopPanel.PanelState.Selected);
                        masterShopPanel.CurrentPanelState = MasterShopPanel.PanelState.Select;

                        FlipButtonsBackToSelect(shopCategory1Panels, vehicleBuyIndex);
                        currShopState = ShopState.BrowsingState;
                    }

                    // else if exit button is pressed
                    else if (shopInfoPanel.IsInfoPanelCloseButtonClicked() && currShopCategoryState == ShopCategoryState.Vehicles) 
                    {
                        ////////Debug.Log("INFO PANEL X BUTTON PRESSED");
                        shopCategory1Panels[vehicleBuyIndex].SetPanelState(MasterShopPanel.PanelState.Buy); 
                        masterShopPanel.CurrentPanelState = MasterShopPanel.PanelState.Buy;
                        currShopState = ShopState.BrowsingState;
                    } 

                    else if (infoPopupPanel.IsMessagePanelCloseButtonClicked() && currShopCategoryState == ShopCategoryState.Vehicles)
                    {
                        ////////Debug.Log("NO COIN INFO PANEL X BUTTON PRESSED");
                        infoPopupPanel.ShowHidePopupMessagePanel(noCoinInfoPopup, false);
                        masterShopPanel.CurrentPanelState = MasterShopPanel.PanelState.Select;
                        currShopState = ShopState.BrowsingState;
                    }
                    /*
                     *
                     *
                     * Add conditions here for other 2 categories
                     *
                     *
                     */
                }
            }
        }

        if (currShopState == ShopState.BrowsingState)
        {
            cameraController.UpdateCamera();
        }
	}
      


    // Detect which button has been pressed
    public void DetectButton(List<GameObject> buttons, MasterShopPanel[] shopCategoryPanels)
    {
        int cash = GameState.SharedInstance.Cash;

        for (int i = 0; i < buttons.Count; i++)
        {
            vehicleInfo = vehicleInfoItems[i];

            if(UIHelpers.CheckButtonHit(buttons[i].transform) && masterShopPanel.CurrentPanelState == MasterShopPanel.PanelState.Select
                                                                && vehicleInfo.Owned == true)
            {
                ////////Debug.Log("IN 1ST IF STATEMENT");
                GameState.SharedInstance.CurrentVehicle = vehicleInfo.Type;
                shopCategoryPanels[i].SetPanelState(MasterShopPanel.PanelState.Selected);
                masterShopPanel.CurrentPanelState = MasterShopPanel.PanelState.Selected;

                clickIndex = i;
                FlipButtonsBackToSelect(shopCategoryPanels, clickIndex);
                break;
            }

            else if((UIHelpers.CheckButtonHit(buttons[i].transform) && masterShopPanel.CurrentPanelState == MasterShopPanel.PanelState.Select) 
                 || (UIHelpers.CheckButtonHit(buttons[i].transform) && masterShopPanel.CurrentPanelState == MasterShopPanel.PanelState.Buy)
                 || (UIHelpers.CheckButtonHit(buttons[i].transform) && masterShopPanel.CurrentPanelState == MasterShopPanel.PanelState.Selected)
                  && vehicleInfo.Owned == false)
            { 
                ////////Debug.Log("IN 2ND IF STATEMENT");
                // check if player has enough coins
                if (shopCategoryPanels[i].vehicleType == vehicleInfo.Type && cash >= vehicleInfo.Cost)
                {
                    ////////Debug.Log("BUY BUTTON PRESSED");
                    currShopState = ShopState.PurchaseConfirmationState;

                    shopInfoPanel.ShowHidePopupItemInfoPanel(shopInfoPopupPanel, true);
                    shopInfoPanel.panelForeground.DrawFrame(vehicleInfo.ShopImageIndex);
                    shopInfoPanel.SetPurchaseItemName(vehicleInfo.Name);
                    shopInfoPanel.SetPurchaseItemDescription(vehicleInfo.Description);
                 
                    vehicleBuyIndex = i;
                    clickIndex = i;
                    break;
                }
                // else condition if player doesn't have enough coins
                else
                {   
                    ////////Debug.Log("POPUP DIALOG IF BUYER DOES NOT HAVE ENOUGH COINS");
                    currShopState = ShopState.PurchaseConfirmationState;
                    infoPopupPanel.ShowHidePopupMessagePanel(noCoinInfoPopup, true);
                    infoPopupPanel.SetPopupPanelMessage("You don't have enough coins!");
                    break;
                }
            }

            else if(UIHelpers.CheckButtonHit(buttons[i].transform) && masterShopPanel.CurrentPanelState == MasterShopPanel.PanelState.Selected
                                                                     && vehicleInfo.Owned == true)
            {
                ////////Debug.Log("IN 3RD IF STATEMENT");
                GameState.SharedInstance.CurrentVehicle = vehicleInfo.Type;
                shopCategoryPanels[i].SetPanelState(MasterShopPanel.PanelState.Selected);
                masterShopPanel.CurrentPanelState = MasterShopPanel.PanelState.Select;

                clickIndex = i;
                FlipButtonsBackToSelect(shopCategoryPanels, clickIndex);
                break;
            }
        }
    }



    public void FlipButtonsBackToSelect(MasterShopPanel[] shopCategoryPanels, int clickIndex)
    {
        ////////Debug.Log("IN FlipButtonsBackToSelect()");
        if(clickIndex > -1)
        {           
            for (int i = 0; i < shopCategoryPanels.Length; i++)
            {

                if(shopCategoryPanels[i].GetPanelState() == MasterShopPanel.PanelState.Selected)
                {

                    if(i != clickIndex)
                    {
                        // must be the old selected item, so flip back to select
                        shopCategoryPanels[i].SetPanelState(MasterShopPanel.PanelState.Select);
                        break;
                    }
                }
            }
        }
    }



    public void ResetTabButtons()
    {
        shopTab1.SetActive(true);
        shopTab2.SetActive(true);
        shopTab3.SetActive(true);
    }



    public void PopulateVehicleData(float xPos, float yPos, float zPos, ref MasterShopPanel[] shopPanels, 
                                    List<GameObject> selectButtons, List<GameObject> selectedButtons, 
                                    List<GameObject> levelRequiredButtons, List<GameObject> buyButtons)
    {
        if (vehicleInfoItems != null)
        {
            
            shopPanels = new MasterShopPanel[vehicleInfoItems.Count];

            for (int i = 0; i < vehicleInfoItems.Count; i++)
            {
                vehicleInfo = vehicleInfoItems[i];

                shopPanels[i] = (MasterShopPanel)Object.Instantiate(masterShopPanel);
                shopPanels[i].transform.position = new Vector3(xPos, yPos, zPos);

                if (vehicleInfo.Type == GameState.SharedInstance.CurrentVehicle)
                {
                    shopPanels[i].SetPanelState(MasterShopPanel.PanelState.Selected);
                }

                else if (vehicleInfo.Owned)
                {
                    shopPanels[i].SetPanelState(MasterShopPanel.PanelState.Select);
                }

                /*else if (!vehicleInfo.Owned && vehicleInfo.RequiresPlayerLevel > GameState.SharedInstance.PlayerLevel.CurrentLevel)
                {
                    shopPanels[i].SetPanelStateRequiresAnotherLevel(vehicleInfo.RequiresPlayerLevel);
                }*/

                else if (!vehicleInfo.Owned)
                {
                    shopPanels[i].SetPanelState(MasterShopPanel.PanelState.Buy);
                }
                    
                shopPanels[i].SetFrameIndex(vehicleInfo.ShopImageIndex);
                shopPanels[i].SetItemName(vehicleInfo.Name);
                shopPanels[i].SetPrice(vehicleInfo.Cost);
                shopPanels[i].vehicleType = vehicleInfo.Type;

                selectButtons.Add(shopPanels[i].GetSelectButton());
                selectedButtons.Add(shopPanels[i].GetSelectedButton());
                levelRequiredButtons.Add(shopPanels[i].GetLevelRequiredButton());
                buyButtons.Add(shopPanels[i].GetBuyButton());

                xPos += xPosPanelPlacement;
            }
        }
    }
}
    