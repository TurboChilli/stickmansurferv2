﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMenuButtons : MonoBehaviour {

	public AudioSource clickSound;

	public GameObject resumeGameButtonEnabled;
	public GameObject resumeGameButtonDisabled;
	public GameObject resumeGameButtonClicked;

	public GameObject retryButtonEnabled;
	public GameObject retryButtonDisabled;


    public GameObject watchVideoRetryButtonEnabled;

    public GameObject retrySkipButton;

	public GameObject doubleCoinsVideoButton;
	public GameObject doubleCoinsButton;

	public GameObject goButtonParent;
	public GameObject goButtonEnabled;
	public GameObject goButtonClicked;
	public GameObject goButtonDisabled;

	public GameObject shopButtonParent;
	public GameObject shopButtonNotification;
	public GameObject shopButtonEnabled;
	public GameObject shopButtonDisabled;

    public GameObject facebookShareButton;
	/*
	public GameObject shopButtonGoalParent;
	public GameObject shopButtonGoalNotification;
	public GameObject shopButtonGoalEnabled;
	public GameObject shopButtonGoalDisabled;
	*/

	public GameObject treasureButtonEnabled;
	public GameObject treasureButtonDisabled;

	public GameObject[] preGameskipEnergyButtons;
	public GameObject[] postGameskipEnergyButtons;

	public GameObject pauseButton;

	public GameObject shackButtonParent;
	public GameObject shackButtonNotification;
    public GameObject shackButtonEnabled;
    public GameObject shackButtonDisabled;
    public GameObject shackButtonHand;
    public RotationClickableJiggle shackButtonJiggle;

	public GameObject shackButtonGoalParent;
	//public GameObject shackButtonGoalNotification;
    public GameObject shackButtonGoalEnabled;
    public GameObject shackButtonGoalDisabled;

    public Camera displayCamera;
    public Camera mainCamera;
    public SupersonicManager supersonicManager;

    public Action OnPauseButtonClicked;

	public Action OnGoButtonClicked;
	public Action OnTreasureButtonClicked;
	public Action OnBuilderButtonClicked;
	public Action OnRetryButtonClicked;
    public Action OnWatchVideoRetryButtonClicked;
    public Action OnFacebookButtonClicked;
	public Action OnRetrySkipButtonClicked;
	public Action OnDoubleCoinsButtonClicked;
	public Action OnDoubleCoinsVideoButtonClicked;

	public Action OnResumeGameButtonClicked;
	public Action<int> PreGameOnSkipGoalButtonClicked;
	public Action<int> PostGameOnSkipGoalButtonClicked;

	public bool gettingMenuInput = false;

	public enum MenuButtonOption
	{
		None,
		Retry,
		SkipRetry,
		Go,
		Treasure,
		Shop,
	}

	public void Awake()
	{
		if (Utils.IphoneXCheck())
			transform.localScale *= 0.85f;
	}	

	public void Initialise(Camera theDisplayCamera, bool autoAlignButtons = true)
	{
		displayCamera = theDisplayCamera;

		//EnableGoButton(true);
		EnableBuilderButton(true);
		doubleCoinsButton.SetActive(false);
        EnableShackButton(true);
        treasureButtonEnabled.SetActive(false);
        treasureButtonDisabled.SetActive(false);
        if (autoAlignButtons)
        {
            AlignButtonsToEdge(theDisplayCamera);
        }

		pauseButton.SetActive(false);

		shackButtonJiggle.enabled = false;
		shackButtonHand.SetActive(false);

		RefreshNotificationIcons();

		if (FacebookManager.ShowShareButton() == false)
        {
            facebookShareButton.SetActive(false);
        }
        else
        {
            facebookShareButton.SetActive(true);
        }
	}

    public void AlignButtonsToEdge(Camera theCamera)
    {
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 5f, 0, shackButtonParent.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 2.5f, 0, shopButtonParent.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 4, 0, treasureButtonEnabled.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 4, 0, treasureButtonDisabled.transform, theCamera);

		
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 2, 0, goButtonParent.transform, theCamera);
		/*
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 2, 0, goButtonClicked.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 2, 0, goButtonEnabled.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 2, 0, goButtonDisabled.transform, theCamera);
		*/
		//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 0.9f, 0.9f, shackButtonParent.transform, theCamera);

        /*
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 330, 0, shopButtonParent.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonEnabled.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonDisabled.transform, theCamera);

        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonClicked.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonEnabled.transform, theCamera);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonDisabled.transform, theCamera);
        */
		   
    }
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonUp(0))
		{
			if(displayCamera == null)
				return;

			if (UIHelpers.CheckButtonHit(pauseButton.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);
				
				if (OnPauseButtonClicked != null)
					OnPauseButtonClicked();
			}

			if (!gettingMenuInput)
				return;

            if(UIHelpers.CheckButtonHit(facebookShareButton.transform, displayCamera))
            {
                GlobalSettings.PlaySound(clickSound); 
                Debug.Log("Share Facebook button pressed here");

                if(OnFacebookButtonClicked != null)
                    OnFacebookButtonClicked();
            }
			if (UIHelpers.CheckButtonHit(goButtonEnabled.transform, displayCamera))
			{
				//GlobalSettings.mainMenuOption = MenuButtonOption.Go;
				EnableGoButton(false);
				GlobalSettings.PlaySound(clickSound);

				if(OnGoButtonClicked != null)
					OnGoButtonClicked();

			}
			else if (UIHelpers.CheckButtonHit(retryButtonEnabled.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);
				EnableRetryButton(false);

				if (OnRetryButtonClicked != null)
					OnRetryButtonClicked();

			}
            else if (UIHelpers.CheckButtonHit(watchVideoRetryButtonEnabled.transform, displayCamera))
            {
                GlobalSettings.PlaySound(clickSound);
                //EnableRetryButton(false);

                if (OnWatchVideoRetryButtonClicked != null)
                    OnWatchVideoRetryButtonClicked();

            }

            else if (UIHelpers.CheckButtonHit(retrySkipButton.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				if (OnRetrySkipButtonClicked != null)
					OnRetrySkipButtonClicked();
			}
			else if (UIHelpers.CheckButtonHit(treasureButtonEnabled.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				if(OnTreasureButtonClicked != null)
					OnTreasureButtonClicked();
			}
			else if(UIHelpers.CheckButtonHit(doubleCoinsButton.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				EnableDoubleCoinButton(false);

				if (OnDoubleCoinsButtonClicked != null)
					OnDoubleCoinsButtonClicked();
			}
			else if (UIHelpers.CheckButtonHit(doubleCoinsVideoButton.transform, displayCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				EnableDoubleCoinVideoButton(false);

				if (OnDoubleCoinsVideoButtonClicked != null)
					OnDoubleCoinsVideoButtonClicked();
			}
			else if(UIHelpers.CheckButtonHit(shopButtonEnabled.transform, displayCamera))
			{
				if (shopButtonDisabled != null)
				{
					shopButtonEnabled.SetActive(false);
					shopButtonDisabled.SetActive(true);
				}

				GlobalSettings.PlaySound(clickSound);
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Shop);


			}
			/*else if (UIHelpers.CheckButtonHit(shopButtonGoalEnabled.transform, displayCamera))
			{
				if (shopButtonGoalDisabled != null)
				{
					shopButtonGoalEnabled.SetActive(false);
					shopButtonGoalDisabled.SetActive(true);
				}

				GlobalSettings.PlaySound(clickSound);
				SceneNavigator.NavigateTo(SceneType.Shop);
			}*/
			else if(UIHelpers.CheckButtonHit(shackButtonEnabled.transform, displayCamera))
			{
				if (shackButtonDisabled != null)
				{
					shackButtonEnabled.SetActive(false);
					shackButtonDisabled.SetActive(true);
				}

				if (GameState.SharedInstance.TutorialStage == TutStageType.FirstGame || GameState.SharedInstance.TutorialStage == TutStageType.MainMenuGoButton)
				{
					GameState.SharedInstance.TutorialStage = TutStageType.MainMenuChest;
				}

				GlobalSettings.PlaySound(clickSound);
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Menu);
			}
			else if (UIHelpers.CheckButtonHit(shackButtonGoalEnabled.transform, displayCamera))
			{
				if (shackButtonDisabled != null)
				{
					shackButtonGoalEnabled.SetActive(false);
					shackButtonGoalDisabled.SetActive(true);
				}
				GlobalSettings.PlaySound(clickSound);
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Menu);
			}


			for (int i = 0; i < 3; i++)
			{
				if (UIHelpers.CheckButtonHit(preGameskipEnergyButtons[i].transform, displayCamera))
				{
					GlobalSettings.PlaySound(clickSound);
					if (PreGameOnSkipGoalButtonClicked != null)
						PreGameOnSkipGoalButtonClicked(i);
					break;
				}

				if (UIHelpers.CheckButtonHit(postGameskipEnergyButtons[i].transform, displayCamera))
				{
					GlobalSettings.PlaySound(clickSound);
					if (PostGameOnSkipGoalButtonClicked != null)
						PostGameOnSkipGoalButtonClicked(i);
					break;
				}
			}

			if (UIHelpers.CheckButtonHit(resumeGameButtonEnabled.transform, displayCamera))
			{
				EnableResumeButton(false);
				GlobalSettings.PlaySound(clickSound);

				if (OnResumeGameButtonClicked != null)
					OnResumeGameButtonClicked();
			}
		}
	}

	public void RefreshNotificationIcons()
	{
		shopButtonNotification.SetActive(GameState.SharedInstance.Upgrades.CanAffordItem());
		//shopButtonGoalNotification.SetActive(GameState.SharedInstance.Upgrades.CanAffordItem());

		bool areAnyTimersDone = GameState.SharedInstance.IsTreasureTimerDone() ||
								GameState.SharedInstance.IsJeepTimerDone() ||
								GameState.SharedInstance.IsHalfPipeTimerDone();

		shackButtonNotification.SetActive(areAnyTimersDone);
		//shackButtonGoalNotification.SetActive(areAnyTimersDone);
	}

	public void EnableGoButton(bool enable)
	{
		if(enable)
		{
			goButtonEnabled.SetActive(true);
			goButtonClicked.SetActive(false);
			goButtonDisabled.SetActive(false);
		}
		else
		{
			goButtonEnabled.SetActive(false);
			goButtonClicked.SetActive(false);
			goButtonDisabled.SetActive(true);
		}
	}

	public void EnableRetryButton(bool enable)
	{
		if(enable)
		{
			retryButtonEnabled.SetActive(true);
			retryButtonDisabled.SetActive(false);
		}
		else
		{
			retryButtonEnabled.SetActive(false);
			retryButtonDisabled.SetActive(true);
		}
	}

	public void EnableResumeButton(bool enable)
	{
		if(enable)
		{
			resumeGameButtonEnabled.SetActive(true);
			resumeGameButtonDisabled.SetActive(false);
		}
		else
		{
			resumeGameButtonEnabled.SetActive(false);
			resumeGameButtonDisabled.SetActive(true);
		}
	}


	public void EnableTreasureButton(bool enable)
	{
		if(enable)
		{
			treasureButtonEnabled.SetActive(true);
			treasureButtonDisabled.SetActive(false);
		}
		else
		{
			treasureButtonEnabled.SetActive(false);
			treasureButtonDisabled.SetActive(true);
		}
	}

	public void EnableDoubleCoinVideoButton(bool enable)
	{
		if (enable)
		{
			doubleCoinsButton.SetActive(false);
			doubleCoinsVideoButton.SetActive(true);
		}
		else
		{
			doubleCoinsButton.SetActive(false);
		}	
	}

	public void EnableDoubleCoinButton(bool enable)
	{
		if (enable)
		{
			doubleCoinsVideoButton.SetActive(false);
			doubleCoinsButton.SetActive(true);
		}
		else
		{
			doubleCoinsVideoButton.SetActive(false);
		}
	}

	public void EnableBuilderButton(bool enable)
	{
		if(enable)
		{
			shopButtonEnabled.SetActive(true);
			shopButtonDisabled.SetActive(false);
		}
		else
		{
			shopButtonEnabled.SetActive(false);
			shopButtonDisabled.SetActive(true);
		}
	}

    public void EnableShackButton(bool enable)
    {
        if(enable)
        {
            shackButtonEnabled.SetActive(true);
            shackButtonDisabled.SetActive(false);
        }
        else
        {
            shackButtonEnabled.SetActive(false);
            shackButtonDisabled.SetActive(true);
        }
    }

}
