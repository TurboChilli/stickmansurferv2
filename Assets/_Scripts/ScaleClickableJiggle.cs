﻿using UnityEngine;
using System.Collections;

public class ScaleClickableJiggle : MonoBehaviour {

	private Vector3 origScale = Vector3.one;
	private float timer = 0.0f;

	public float addedScale = 7.5f;
	public float jiggleSpeed = 40.0f;

	public float breakTime = 0.5f;
	public float scaleTime = 0.5f;
	private bool scaling = false;

	void OnEnable () 
	{
		origScale = transform.localScale;
		timer = Random.Range(0.0f, breakTime);
		scaling = false;
	}

	void OnDisable()
	{
		transform.localScale = origScale;
	}

	void Update () 
	{
		if (scaling)
		{
			float scaleWeight = Mathf.Sin( (timer / scaleTime) * Mathf.PI);

			transform.localScale = origScale + addedScale * Vector3.one * scaleWeight;
		}

		timer += Time.deltaTime;

		if (timer > breakTime && !scaling)
		{
			scaling = true;
			timer = 0.0f;
		}
		else if (timer > scaleTime & scaling)
		{
			scaling = false;
			timer = 0.0f;
		}
	}
}
