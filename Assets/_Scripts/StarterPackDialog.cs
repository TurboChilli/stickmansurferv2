﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class StarterPackDialog : MonoBehaviour 
{
    public StarterPackDialogController starterPackDialogController;

    public Camera mainCamera;

    public GameObject blackOverlay;
    private Material blackOverlayMaterial;
    private Color origBlackOverlayColor;

    public GameObject starterPackPanel;
    public GameObject closeButton;
    public GameObject savePercentBanner;
    public GameObject buyButton;
    public GameObject bundleImage;

    public AudioSource clickSound;
    public AudioSource swooshSound;

    public TMP_Text titleText;
    public TMP_Text savePercentText;
    public TMP_Text contentText;
    public TMP_Text priceText;
    public TMP_Text timerText;

    private Vector3 inactivePosition;
    private Vector3 activePosition;

    public string StarterPackId { get; set; }

    public GameObject purchaseCompleteDialogPanel;
    public GameObject purchaseCompleteCloseButton;
    public TMP_Text purchaseCompleteTitle;
    public GameObject purchasePackImage;

    private Vector3 inactivePanelPosition;
    private Vector3 activePanelPosition;

    private bool isShowing = false;
	float starterPackPrice = 0f;
	float smallPackPrice = 0f;

    Collider _blackOverlayCollider;
    private Collider blackOverlayCollider
    {
        get
        {
            if(_blackOverlayCollider == null)
                _blackOverlayCollider = blackOverlay.GetComponent<Collider>();

            return _blackOverlayCollider;
        }
        set
        {
            _blackOverlayCollider = value;
        }
    }

    // Use this for initialization
    void Start () 
    {
        blackOverlayCollider = blackOverlay.GetComponent<Collider>();
        blackOverlayMaterial = blackOverlay.GetComponent<Renderer>().material;
        origBlackOverlayColor = blackOverlayMaterial.color;
		savePercentBanner.SetActive(true);

		priceText.text = "";
		IAPManager iapManager = FindObjectOfType<IAPManager>();
		if(iapManager != null)
		{
			starterPackPrice = iapManager.GetProductPrice(IAPManager.StarterPackProductId6);
			smallPackPrice = iapManager.GetProductPrice(IAPManager.ProductIdPack1);

			if(starterPackPrice > 0)
			{
				string productPriceString = iapManager.GetProductPriceString(IAPManager.StarterPackProductId6);
				if(!string.IsNullOrEmpty(productPriceString))
				{
					priceText.text = productPriceString;
				}
			}
		}

		if(string.IsNullOrEmpty(priceText.text))
		{
			priceText.text = GameLanguage.LocalizedText("PURCHASE");
		}

		if (Utils.IphoneXCheck())
		{
			this.transform.localScale *= 0.8f;	
		}

		SetStarterPackContentText();
		CalculateAndSetSavePercentText();
    }

    // Update is called once per frame
    void Update () {

    }

    public Vector3 GetActivePosition()
    {
        return activePanelPosition;
    }

    public Vector3 GetInActivePosition()
    {
        return inactivePanelPosition;
    }
        
    public bool IsShowingPanel()
    {
        return isShowing;
    }

    public void SetDialogPositions()
    {
        UIHelpers.SetTransformToCenter(this.transform, mainCamera);

        activePosition = this.transform.position;
        inactivePosition = activePosition + Vector3.right * 2000.0f;
    }

    public void HideStarterPackDialog()
    {
        isShowing = false;
		DialogTrackingFlags.MainMenuStarterPackShowing = false;

        blackOverlayCollider.enabled = false;
        this.transform.position = inactivePosition;
    }

    public void ShowStarterPackDialog()
    {
        isShowing = true;
		DialogTrackingFlags.MainMenuStarterPackShowing = true;

        blackOverlayCollider.enabled = true;
        UIHelpers.SetTransformToCenter(this.transform, mainCamera);
    }

    public void SetPurchaseDialogPositions()
    {
        UIHelpers.SetTransformToCenter(purchaseCompleteDialogPanel.transform, mainCamera);

        activePosition = purchaseCompleteDialogPanel.transform.position;
        inactivePosition = activePosition + Vector3.right * 2000.0f;
    }

    public void HideStarterPackPurchasedDialog()
    {
        blackOverlayCollider.enabled = false;
        purchaseCompleteDialogPanel.transform.position = inactivePosition;
    }

    public void ShowStarterPackPurchasedPanel()
    {        
        GlobalSettings.PlaySound(swooshSound);
        blackOverlayCollider.enabled = true;
        UIHelpers.SetTransformToCenter(purchaseCompleteDialogPanel.transform, mainCamera);
    }

    public void SetTimerText(string time)
    {
        timerText.text = time;
    }

	public void InitialiseStarterPack(string starterPackId) 
    {
		StarterPackId = starterPackId.ToUpperInvariant();
    }

	void SetStarterPackContentText()
	{
		StickSurferSetting settings = GameServerSettings.SharedInstance.SurferSettings;

		string contentTextString = string.Format("- {0} {1}\n- {2} {3}\n- {4} {5}\n- {6}",
			settings.StarterPackNumEnergyDrinks, GameLanguage.LocalizedText("ENERGY DRINKS"),
			settings.StarterPackNumCoins, GameLanguage.LocalizedText("COINS"),
			GameLanguage.LocalizedText("JEEP"), GameLanguage.LocalizedText("FUEL"),
			GameLanguage.LocalizedText("MULLET"));


		contentText.text = contentTextString;
	}

	void CalculateAndSetSavePercentText()
	{
		if(starterPackPrice <= 0 || smallPackPrice <= 0)
		{
			savePercentBanner.SetActive(false);
		}
		else
		{
			savePercentBanner.SetActive(true);

			StickSurferSetting settings = GameServerSettings.SharedInstance.SurferSettings;

			float totalEnergyDrinks = settings.StarterPackNumEnergyDrinks + ((float)settings.StarterPackNumCoins/(float)settings.EnergyDrinkToGoldRatio);

			UpgradeItem mullet = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.HeadMullet);
			if(mullet != null && !mullet.UpgradeCompleted())
			{
				int? nextUpgradePrice = mullet.GetNextUgpradePrice();
				if(nextUpgradePrice.HasValue)
				{
					if(mullet.PriceIsEnergyDrink)
						totalEnergyDrinks += nextUpgradePrice.Value;
					else
						totalEnergyDrinks += ((float)nextUpgradePrice.Value/(float)settings.EnergyDrinkToGoldRatio);
				}
			}

			UpgradeItem jeepFuel = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepFuel);
			if(jeepFuel != null && !jeepFuel.UpgradeCompleted())
			{
				int? nextUpgradePrice = jeepFuel.GetNextUgpradePrice();
				if(nextUpgradePrice.HasValue)
				{
					if(jeepFuel.PriceIsEnergyDrink)
						totalEnergyDrinks += nextUpgradePrice.Value;
					else
						totalEnergyDrinks += ((float)nextUpgradePrice.Value/(float)settings.EnergyDrinkToGoldRatio);
				}
			}


			float baseEnergyDrinkCostPerUnit = smallPackPrice/(float)settings.IAPEnergyPackAmount1;
			float energyDrinkCostValueThisPack = baseEnergyDrinkCostPerUnit * totalEnergyDrinks;
			int percentageSaved =  (int)Math.Floor((1 - (starterPackPrice/energyDrinkCostValueThisPack)) * 100);

			if(percentageSaved > 0)
			{
				savePercentText.text = string.Format("{0} {1}%", GameLanguage.LocalizedText("SAVE"), percentageSaved);
			}
			else
			{
				savePercentBanner.SetActive(false);
			}
		}
	}
}
