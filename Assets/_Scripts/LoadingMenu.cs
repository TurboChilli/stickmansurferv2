﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingMenu : MonoBehaviour {

	public Camera mainCamera;
	public GameObject dialogContents;
	public FillBar loadingBar;

	Vector3 dialogContentsStartPos = Vector3.zero;
	Vector3 dialogContentsEndPos = Vector3.zero;

    public SceneType loadingScene =  SceneType.Undefined;

    public GameObject iPhoneBackground;
    public GameObject iPadBackground;
    public GameObject iPhoneXBackground;

	float curTimeToMove;
	float timeToMove = 0.7f;
	public float sceneLoadDelay = 1.0f;

	AsyncOperation sceneLoad;

	void Start ()
	{
		if (Utils.IphoneXCheck(mainCamera))
		{
			if (iPhoneXBackground != null)
				iPhoneXBackground.SetActive(true);
		}
        else if (iPhoneBackground != null && iPadBackground != null)
        {
        	if (iPhoneXBackground != null)
				iPhoneXBackground.SetActive(false);
        
            if (UIHelpers.IsIPadRes())
            {
                iPhoneBackground.SetActive(false);
                iPadBackground.SetActive(true);
            }
            else
            {
                iPhoneBackground.SetActive(true);
                iPadBackground.SetActive(false);
            }
        }
		Vector3 localPos = dialogContents.transform.localPosition;
		dialogContentsEndPos = localPos;
		dialogContentsStartPos = new Vector3(localPos.x, localPos.y - 800f, localPos.z);
		dialogContents.transform.localPosition = dialogContentsStartPos;

		dialogContents.transform.localPosition = dialogContentsStartPos;
		UIHelpers.SetTransformToCenter(this.transform, mainCamera);

        TutStageType curTutorialStage = GameState.SharedInstance.TutorialStage;
		if (curTutorialStage == TutStageType.MainMenuGoButton || curTutorialStage == TutStageType.FirstGame)  
        {
            loadingScene = SceneType.SurfMain;
        }

		curTimeToMove = timeToMove;
        if (loadingScene == SceneType.Undefined || loadingScene == SceneType.SurfMain)
        {
            sceneLoad = SceneManager.LoadSceneAsync("SceneSurfMain", LoadSceneMode.Single);
        }
        else
        {
            sceneLoad = SceneManager.LoadSceneAsync(SceneNavigator.GetSceneTextFromEnum(loadingScene), LoadSceneMode.Single);
        }
		sceneLoad.allowSceneActivation = false;
		loadingBar.SetHorizBarAtValue(1.0f);

 	}

	void Update () 
	{
		if (curTimeToMove >= 0)
		{
			curTimeToMove -= Time.deltaTime;
			float lerpAmount = Mathf.Pow(Mathf.Max(0.0f, curTimeToMove / timeToMove), 3.0f);
			dialogContents.transform.localPosition = Vector3.Lerp(dialogContentsStartPos, dialogContentsEndPos, 1.0f - lerpAmount);

			if (curTimeToMove < 0)
				dialogContents.transform.localPosition = dialogContentsEndPos;
		}
		else
		{
			float loadingBarValue = Mathf.Lerp(loadingBar.CurrentHorizontalBarValue, 1.0f - (sceneLoad.progress + 0.1f), Time.deltaTime * 4f);

			#if UNITY_ANDROID
			if(loadingBarValue <= 0.15f)
				loadingBarValue = 0.15f;
			#endif

			loadingBar.SetHorizBarAtValue(loadingBarValue);
		}

		sceneLoadDelay -= Time.deltaTime;

		if (sceneLoad.progress >= 0.9f && sceneLoadDelay <= 0)
			sceneLoad.allowSceneActivation = true;
	}


}
