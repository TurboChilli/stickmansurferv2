﻿using UnityEngine;
using System.Collections;

public class UnderPlayerCollider : MonoBehaviour {

	bool aboveRail = false;
	void OnTriggerEnter(Collider other)
	{
		//Debug.Log ("UNDER CUBE TRIGGER ENTERED HERE ------------------------------------:" + other.gameObject.tag);
		if(other.gameObject.tag=="Rail")
		{
			aboveRail = true;
			transform.GetComponent<Renderer>().material.color = new Color(1,0,0,1);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag=="Rail")
		{
			aboveRail = false;
			transform.GetComponent<Renderer>().material.color = new Color(1,1,1,1);
		}
	}

	public bool IsAboveRail()
	{
		if(aboveRail)
		{
			//Debug.Log ("ABOVE RAIL ^^^^^^^^^^^^^^^^^^^^^^^^");
		}
		return aboveRail;
	}
}
