﻿using UnityEngine;
using System.Collections;

public enum SpecialReward
{
	Coins,
	SingleEnergy,
	DoubleEnergy,
	None
}


public class SpecialRewardHelper 
{
	public static SpecialReward[] firstSpecialReward = new SpecialReward[4]
	{
		SpecialReward.Coins,
		SpecialReward.DoubleEnergy,
		SpecialReward.Coins,
		SpecialReward.SingleEnergy
	};

	public static string Get(SpecialReward reward)
	{
		switch (reward)
		{
			case SpecialReward.Coins:
				return CoinsTitle;
			case SpecialReward.SingleEnergy:
				return SingleEnergyTitle;
			case SpecialReward.DoubleEnergy:
				return DoubleEnergyTitle;
		};
		return string.Empty;
	}

	public const string CoinsTitle =			"+{0}";
	public const string SingleEnergyTitle = 	"+1";
	public const string DoubleEnergyTitle =		"+2";
}
