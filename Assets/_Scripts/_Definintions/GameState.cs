﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;


public class GameState  
{
	List<VehicleInfo> allVehicleInfo = null;
	List<Vehicle> ownedVehicleTypes = null;

    private int defaultNumberOfSwapsInShellGame = 6;

	public Location locationOnMapLoad = Location.Undefined;

	// Shared Instance
	static GameState instance;
	public static GameState SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new GameState();

			return instance;
		}
	}
		
	// Construction 
	public GameState()
	{
		//PlayerLevel = new PlayerLevel();
		allVehicleInfo = new List<VehicleInfo>();
		ownedVehicleTypes = new List<Vehicle>();

		this.SetLocationUnlock(Location.MainBeach);
	}

	UpgradeManager upgrades;
	public UpgradeManager Upgrades
	{
		get 
		{
			if(upgrades == null)
				upgrades = new UpgradeManager();

			return upgrades;
		}
	}

	public void RefreshUpgradePrices()
	{
		upgrades = new UpgradeManager();
	}

	public int GetCheapestPrice()
	{
		return Upgrades.MinCostOfNextUpgradeItem;
	}

	public int GetNicePercentageCostOfNextMinUpgradeItem(float percentage)
	{
		return Upgrades.GetNicePercentageCostOfNextMinUpgradeItem(percentage);
	}

	public TutStageType TutorialStage
	{
		get { return (TutStageType)PlayerPrefs.GetInt("ServerGS_TutorialStage", 0); }
		set { PlayerPrefs.SetInt("ServerGS_TutorialStage", (int)value); }
	}

	#region CashScoreCurrency

	public int Cash 
	{ 
		get { return PlayerPrefs.GetInt("ServerGS_cash", GameServerSettings.SharedInstance.SurferSettings.DefaultCoins); }
		set 
		{ 
			PlayerPrefs.SetInt("ServerGS_cash", value); 

			SavedGameStateServerSettings.SharedInstance.SavedGameStateData.Coins = value;
			SavedGameStateServerSettings.SharedInstance.FlagDataDirty();
		}
	}

	public int GetTotalVehicleCash(Vehicle vehicle)
	{
		if (VehicleHelper.IsStartingBoard(vehicle) || vehicle == Vehicle.None)
			vehicle = Vehicle.None;

		return PlayerPrefs.GetInt(string.Format("ServerGS_totalCash{0}" , (int)vehicle), 0);
	}

	public void AddTotalCash(Vehicle vehicle, int cash)
	{
		int curCash = 0;

		if (VehicleHelper.IsStartingBoard(vehicle) || vehicle == Vehicle.None)
			vehicle = Vehicle.None;
		else
		{
			curCash = PlayerPrefs.GetInt(string.Format("ServerGS_totalCash{0}" , (int)vehicle), 0);
			PlayerPrefs.SetInt(string.Format("ServerGS_totalCash{0}" , (int)vehicle), curCash + cash);
		}

		curCash = PlayerPrefs.GetInt(string.Format("ServerGS_totalCash{0}" , (int)vehicle), 0);
		PlayerPrefs.SetInt(string.Format("ServerGS_totalCash{0}" , (int)vehicle), curCash + cash);
	}


	public int TotalCash
	{
		get { return PlayerPrefs.GetInt("ServerGS_totalCash0", 0); }
		//set { PlayerPrefs.SetInt("ServerGS_totalCash0", value); }
	}


	public int Multiplier
	{
		get { return PlayerPrefs.GetInt("ServerGS_multiplier", 1); }
		set { PlayerPrefs.SetInt("ServerGS_multiplier", value); }
	}

    public int Score 
    { 
        get { return PlayerPrefs.GetInt("ServerGS_score", 500); }
        set { PlayerPrefs.SetInt("ServerGS_score", value); }
    }

    public int RetryTimes
    {
    	get { return PlayerPrefs.GetInt("ServerGS_retry", 0); }
		set { PlayerPrefs.SetInt("ServerGS_retry", value); } 

    }

    public float TotalScore
    {
    	get { return PlayerPrefs.GetFloat("ServerGS_total", 0.0f); }
		set { PlayerPrefs.SetFloat("ServerGS_total", value);}
    }

    public int EnergyDrink
    {
		get { return PlayerPrefs.GetInt("ServerGS_energyDrink", GameServerSettings.SharedInstance.SurferSettings.DefaultEnergyDrinks); }
		set 
		{ 
			PlayerPrefs.SetInt("ServerGS_energyDrink", value); 

			SavedGameStateServerSettings.SharedInstance.SavedGameStateData.EnergyDrinks = value;
			SavedGameStateServerSettings.SharedInstance.FlagDataDirty();
		}
    }

    public float TotalDistanceTraveled
    {
		get { return PlayerPrefs.GetFloat("ServerGS_totalDistance", 0.0f); }
		set 
		{ 
			if(value > GameServerSettings.SharedInstance.SurferSettings.SurfMinTutorialDistance && !GlobalSettings.SurfedPassMinTutorialDistance())
			{
				GlobalSettings.FlagSurfedPassMinTutorialDistanceCompleted();
			}

			PlayerPrefs.SetFloat("ServerGS_totalDistance", value); 
		}
    }

    public float HighestJeepDistance
    {
    	get { return PlayerPrefs.GetFloat("ServerGS_jeepHighestDistance", 0.0f); }
		set { PlayerPrefs.SetFloat("ServerGS_jeepHighestDistance", value); }
    }

    // This timestamp is for the timer display on the starter pack panel
    public string StarterPackDialogTimeStamp
    {
        get { return PlayerPrefs.GetString("ServerGS_StarterPackDialaog_TimeStamp", "0"); }
        set { PlayerPrefs.SetString("ServerGS_StarterPackDialaog_TimeStamp", value); }
    }

    public int NumberOfStarterPackDialogsShown
    {
        get { return PlayerPrefs.GetInt("ServerGS_starterPackDialogsShown", 0); }
		set { PlayerPrefs.SetInt("ServerGS_starterPackDialogsShown", value); }
    }

	public int NumberOfStarterPackOfferButtonShown
	{
		get { return PlayerPrefs.GetInt("ServerGS_starterPackOfferButton", 0); }
		set { PlayerPrefs.SetInt("ServerGS_starterPackOfferButton", value); }
	}

	public int MainMenuMenuVisit
	{
		get { return PlayerPrefs.GetInt("ServerGS_MainMenuMenuVisit", 1); }
		set { PlayerPrefs.SetInt("ServerGS_MainMenuMenuVisit", value); }
	}

    public void AddNumberOfStarterPackDialogsShown()
    {
        NumberOfStarterPackDialogsShown += 1;
    }

    public bool HasBoughtStarterPack
    {        
        get { return PlayerPrefs.GetInt("ServerGS_HasBoughtStarterPack", 0) == 1; }
        set { PlayerPrefs.SetInt("ServerGS_HasBoughtStarterPack", (value) ? 1 : 0 ); }
    }

    public bool PopUpDialogTimeIsUp
    {        
        get { return PlayerPrefs.GetInt("ServerGS_PopUpDialogTimeIsUp", 0) == 1; }
        set { PlayerPrefs.SetInt("ServerGS_PopUpDialogTimeIsUp", (value) ? 1 : 0 ); }
    }

    public bool StarterPackRulesMet
    {        
        get { return PlayerPrefs.GetInt("ServerGS_StarterPackRules", 0) == 1; }
        set { PlayerPrefs.SetInt("ServerGS_StarterPackRules", (value) ? 1 : 0 ); }
    }

    public void ShowTheStarterPack(bool value)
    {
        GameServerSettings.SharedInstance.SurferSettings.ShowTheStarterPackDialog = value;
    }

    public bool HasPlayedHalfPipe
    {
        get { return PlayerPrefs.GetInt("ServerGS_HasPlayedHalfPipe", 0) == 1; }
        set { PlayerPrefs.SetInt("ServerGS_HasPlayedHalfPipe", (value ? 1 : 0 )); }
    }

    public string HalfPipeTimeStamp
    {
        get { return PlayerPrefs.GetString("ServerGS_HalfPipe_TimeStamp", "0"); }
        set { PlayerPrefs.SetString("ServerGS_HalfPipe_TimeStamp", value); }
    }

    public bool IsHalfPipeTimerDone()
    {
        long temp = Convert.ToInt64(HalfPipeTimeStamp);
        DateTime openDateTime = DateTime.FromBinary(temp);
		TimeSpan difference = openDateTime.Subtract(DateTime.UtcNow);
        return (difference.TotalSeconds <= 0);
    }

	public bool HasPlayedGameConsole
    {
    	get { return PlayerPrefs.GetInt("ServerGS_HasPlayedGameConsole", 0) == 1; } 
		set { PlayerPrefs.SetInt("ServerGS_HasPlayedGameConsole", (value ? 1 : 0 )); }
    }

    public string GameConsoleTimeStamp
    {
        get { return PlayerPrefs.GetString("ServerGS_GameConsole_TimeStamp", "0"); }
		set { PlayerPrefs.SetString("ServerGS_GameConsole_TimeStamp", value); }
    }

	public bool IsGameConsoleTimerDone()
    {
		long temp = Convert.ToInt64(GameConsoleTimeStamp);
        DateTime openDateTime = DateTime.FromBinary(temp);
		TimeSpan difference = openDateTime.Subtract(DateTime.UtcNow);
        return (difference.TotalSeconds <= 0);
    }

    public bool HasClosedStarterPackDialog
    {
        get { return PlayerPrefs.GetInt("ServerGS_HasClosedStarterPackDialog", 0) == 0; }
        set { PlayerPrefs.SetInt("ServerGS_StarterPackPopup_TimeStamp", (value) ? 1 : 0 ); }
    }

    // This timestamp is for the time it takes between starter pack
    // popups being shown
    public string StarterPackPopupTimeStamp
    {
        get { return PlayerPrefs.GetString("ServerGS_StarterPackPopup_TimeStamp", "0"); }
        set { PlayerPrefs.SetString("ServerGS_StarterPackPopup_TimeStamp", value); }
    }

	public string TreasureVaultTimeStamp
    {
		get { return PlayerPrefs.GetString("ServerGS_Treasure_TimeStamp", "0"); }
		set { PlayerPrefs.SetString("ServerGS_Treasure_TimeStamp", value); }
    }

    public bool IsTreasureTimerDone()
    {
		long temp = Convert.ToInt64( TreasureVaultTimeStamp);
		DateTime openDateTime = DateTime.FromBinary(temp);
		TimeSpan difference = openDateTime.Subtract(DateTime.Now);
		return (difference.TotalSeconds <= 0);
    }

	public string JeepTimeStamp
    {
		get { return PlayerPrefs.GetString("ServerGS_Jeep_TimeStamp", "0"); }
		set { PlayerPrefs.SetString("ServerGS_Jeep_TimeStamp", value); }
    }

    public bool IsJeepTimerDone()
    {
		long temp = Convert.ToInt64( JeepTimeStamp);
		DateTime openDateTime = DateTime.FromBinary(temp);
		TimeSpan difference = openDateTime.Subtract(DateTime.Now);
		return (difference.TotalSeconds <= 0);
    }
        
    public int FirstTimeVehicleIndex
    {
    	get { return PlayerPrefs.GetInt("ServerGS_FirstTimeVehicles", 0); }
		set { PlayerPrefs.SetInt("ServerGS_FirstTimeVehicles", value); }
    }

    public int FirstTreasureIndex
    {
    	get { return PlayerPrefs.GetInt("ServerGS_FirstTreasureRewards", 0); }
		set { PlayerPrefs.SetInt("ServerGS_FirstTreasureRewards", value);}
    }

    public int FirstSuperRewardIndex
    {
    	get { return PlayerPrefs.GetInt("ServerGS_FirstSuperRewards", 0); }
		set { PlayerPrefs.SetInt("ServerGS_FirstSuperRewards", value); }
    }

    public float GetTotalDistanceTravelInVehicle(Vehicle vehicle)
    {
		return PlayerPrefs.GetFloat("ServerGS_totalVehicleDistance" + (int)vehicle, 0);
    }

	private void SetTotalDistanceTravelInVehicle(Vehicle vehicle, float distance)
    {
		PlayerPrefs.SetFloat("ServerGS_totalVehicleDistance" + (int)vehicle, distance);
    }

    public int GetTotalVehicleCollect(Vehicle vehicle)
    {
    	return PlayerPrefs.GetInt("ServerGS_totalVehicleCollect" + (int)vehicle, 0);
    }

	public void AddVehicleCollect(Vehicle vehicle)
	{
		int curVehicleCollect = GetTotalVehicleCollect(vehicle);
		PlayerPrefs.SetInt("ServerGS_totalVehicleCollect" + (int)vehicle, curVehicleCollect + 1);
	}

	public int GetGrindHitTotal()
	{
		return PlayerPrefs.GetInt("ServerGS_totalGrindHit", 0);
	}

	public void AddGrindHit()
	{
		int curGrindHits = GetGrindHitTotal();	
		PlayerPrefs.SetInt("ServerGS_totalGrindHit", curGrindHits + 1);
	}

	public int GetSlalomsHit()
	{
		return PlayerPrefs.GetInt("ServerGS_totalSlalomHit", 0);
	}

	public void AddSlalomHit()
	{
		int curSlaloms = GetSlalomsHit();
		PlayerPrefs.SetInt("ServerGS_totalSlalomHit", curSlaloms + 1);
	}

	/*public int GetSlalomsMiss()
	{
		return PlayerPrefs.GetInt("ServerGS_totalSlalomMiss", 0);
	}

	public void AddSlalomMiss()
	{
		int curSlaloms = GetSlalomsMiss();
		PlayerPrefs.SetInt("ServerGS_totalSlalomMiss", curSlaloms + 1);
	}*/

	public void AddSURFCollect()
	{
		int curSURFCollect = GetSURFCollect();
		PlayerPrefs.SetInt("ServerGS_totalSURFCollected", curSURFCollect + 1);
	}

	public int GetSURFCollect()
	{
		return PlayerPrefs.GetInt("ServerGS_totalSURFCollected", 0);
	}

    public void VisitLocation(Location location)
    {
		int curVisits = GetLocationVisits(location);
		PlayerPrefs.SetInt("ServerGS_locationVisits" + (int)location, curVisits + 1);
    }

    public int GetLocationVisits(Location location)
    {
		return PlayerPrefs.GetInt("ServerGS_locationVisits" + (int)location, 0);
    }

    public void AddVehcleDistance(Vehicle vehicle, float distance)
    {
		float curDistance = GetTotalDistanceTravelInVehicle(vehicle);
		SetTotalDistanceTravelInVehicle(vehicle, curDistance + distance);
    }


	public Cosmetic CurrentCosmetic
    {
    	get 
		{ 
			int val = PlayerPrefs.GetInt("ServerGS_cosmetic", -1); 
			if(val == -1)
				return Cosmetic.StickmanCap;

			return (Cosmetic)val;
		}
		set 
		{ 
			PlayerPrefs.SetInt("ServerGS_cosmetic", (int)value ); 
		}
    }


    public static bool IsFirstSession()
    {
		int firstSession = PlayerPrefs.GetInt("ServerGS_gamesPlayed", 0);
        if (firstSession == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool IsFirstFidgetSpinner()
    {
		int firstSession = PlayerPrefs.GetInt("ServerGS_fidgetsPlayed", 0);
        if (firstSession == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool IsFirstShellGame()
    {
      	return PlayerPrefs.GetInt("ServerGS_shellGamesPlayed", 0) == 0;
    }

    public int ShellGamesPlayed
    {
        get { return PlayerPrefs.GetInt("ServerGS_shellGamesPlayed", 0); }
        set { PlayerPrefs.SetInt("ServerGS_shellGamesPlayed", value); }
    }

    public void AddShellGamePlayed()
    {
        ShellGamesPlayed++;
    }

    public static bool IsFirstJeepGame
    {
        get { return PlayerPrefs.GetInt("ServerGS_HasPlayedJeepGame", 0) == 0; }
    }

    public int JeepGamesPlayed
    {
        get { return PlayerPrefs.GetInt("ServerGS_HasPlayedJeepGame", 0); }
        set { PlayerPrefs.SetInt("ServerGS_HasPlayedJeepGame", value); }
    }

    public void AddJeepGamesPlayed()
    {
        JeepGamesPlayed++;
    }

    public bool HasRemovedAds()
    {
        int removedAds = PlayerPrefs.GetInt("ServerGS_nads", 0);
        if (removedAds == 0)
            return false;
        else
            return true;
    }

	public void RemoveAds()
	{
		if(!HasRemovedAds())
			PlayerPrefs.SetInt("ServerGS_nads", 1);
	}
		
    public int GamesPlayed
    {
    	get { return PlayerPrefs.GetInt("ServerGS_gamesPlayed", 0); }
		set { PlayerPrefs.SetInt("ServerGS_gamesPlayed", value); }
    }

    public int FidgetGamesPlayed
    {
		get { return PlayerPrefs.GetInt("ServerGS_fidgetsPlayed", 0); }
		set { PlayerPrefs.SetInt("ServerGS_fidgetsPlayed", value); }
    }

	public bool SecondTutorialShown
	{
		get { return (PlayerPrefs.GetInt("ServerGS_secondTutorialShown", 0) == 1); }
		set { PlayerPrefs.SetInt("ServerGS_secondTutorialShown", value ? 1 : 0); }
	}

    public int GetSuperRewardStepUp(int curLevel)
    {
		return curLevel * GameServerSettings.SharedInstance.SurferSettings.GoldSuperRewardStepUp;
    }

	public int ConvertEnergyDrinkToGold(int energyDrink)
	{
		return energyDrink * GameServerSettings.SharedInstance.SurferSettings.EnergyDrinkToGoldRatio;
	}

	public int ConvertLevelToCoin(int level)
	{
		
		return Mathf.Min(GameServerSettings.SharedInstance.SurferSettings.GoalCompleteStart +
							level * GameServerSettings.SharedInstance.SurferSettings.GoalCompleteRewardPerLevel,
						 GameServerSettings.SharedInstance.SurferSettings.GoalCompleteRewardCap);
	}
		
	public int ConvertGoldToEnergyDrink(int gold)
	{
		int goldToEnergyDrinkRatio = GameServerSettings.SharedInstance.SurferSettings.EnergyDrinkToGoldRatio;
		int energyDrink = Mathf.CeilToInt((float)gold/goldToEnergyDrinkRatio);

		if(energyDrink <= 0)
			energyDrink = 1;

		return energyDrink;
	}

	public int RetryEnergyDrinkCost(int curGold)
	{
		return ConvertGoldToEnergyDrink(curGold);
	}


	public int DoubleCoinEnergyDrinkCost(int curGold)
	{
		return ConvertGoldToEnergyDrink(curGold);
	}

	#endregion

	public void AddGamePlayed()
	{
		GamesPlayed ++;
	}

    public int NumberOfSwapsOnShellgame
    {
        get 
		{ 
			int numSwaps = PlayerPrefs.GetInt("ShellGame_winsOrLosses", defaultNumberOfSwapsInShellGame); 

			StickSurferSetting settings = GameServerSettings.SharedInstance.SurferSettings;

			if(numSwaps >= settings.ShellGameMaxNumberSwaps)
				return UnityEngine.Random.Range(settings.ShellGameMaxRandomMin, settings.ShellGameMaxRandomMax);
			else
				return numSwaps;
		}
        set { PlayerPrefs.SetInt("ShellGame_winsOrLosses", value); }
    }

    public void AddSwapToShellGame()
    {
        NumberOfSwapsOnShellgame += 2;

		if(NumberOfSwapsOnShellgame > GameServerSettings.SharedInstance.SurferSettings.ShellGameMaxNumberSwaps)
			NumberOfSwapsOnShellgame = GameServerSettings.SharedInstance.SurferSettings.ShellGameMaxNumberSwaps;
    }

    public void MinusSwapFromShellGame()
    {
        NumberOfSwapsOnShellgame -= 2;
    }

    public int GetDefaultNumberOfSwapsInShellGame()
    {
        return defaultNumberOfSwapsInShellGame;
    }

	public void AddCash(int addedCash, bool tempCoinAdd = false)
	{
		Cash = Cash + addedCash;
	}

    public void LoseCash(int lostCash)
    {
        Cash = Cash - lostCash;
    }

    public void AddMultiplier(int multiplier)
    {
    	Multiplier = Multiplier + multiplier;
    }

    public void AddEnergyDrink(int addedDrink)
    {
    	EnergyDrink = EnergyDrink + addedDrink;
    }

	public void LoseEnergyDrink(int amount)
	{
		EnergyDrink = EnergyDrink - amount;
	}

	#region Activities
	/*
	public IList<Activity> GetActivities()
	{
		return Scheduler.SharedInstance.GetActivities();
	}

	public Activity GetCurrentActivity()
	{
		
		var activities = Scheduler.SharedInstance.GetActivities();
		if(activities != null && activities.Length > 0)
			return activities[0];
		else
			return null;
	}
		
	#endregion

	#region Useful Debug Prints

	public void DebugPrintGetActivities(bool useLogError = false)
	{
		string methodName = "******* DebugPrintGetActivities";
		if(useLogError)
			Debug.LogError(methodName);
		else
			Debug.Log(methodName);
		
		var activities = this.GetActivities();
		foreach(var activity in activities)
		{
			var tutorialInfo = activity.IsTutorial ? activity.Tutorial.Info : TutorialInfo.None;
			string debugStr = string.Format("ActivityType: {0} IsTutorial: {1} TutorialInfo: {2}"
				, activity.Type, activity.IsTutorial, tutorialInfo);

			if(useLogError)
				Debug.LogError(debugStr);
			else
				Debug.Log(debugStr);
		}
	}

	public void DebugPrintGetCurrentActivity(bool useLogError = false)
	{
		string methodName = "******* DebugPrintGetCurrentActivity";
		if(useLogError)
			Debug.LogError(methodName);
		else
			Debug.Log(methodName);
		
		var activity = this.GetCurrentActivity();
		if(activity != null)
		{
			var tutorialInfo = activity.IsTutorial ? activity.Tutorial.Info : TutorialInfo.None;
			string debugStr = string.Format("ActivityType: {0} IsTutorial: {1} TutorialInfo: {2}"
				, activity.Type, activity.IsTutorial, tutorialInfo);

			if(useLogError)
				Debug.LogError(debugStr);
			else
				Debug.Log(debugStr);
		}
	}

	public void DebugPrintActivity(Activity activity, bool useLogError = false)
	{
		string methodName = "******* DebugPrintActivity";
		if(useLogError)
			Debug.LogError(methodName);
		else
			Debug.Log(methodName);

		if(activity != null)
		{
			var tutorialInfo = activity.IsTutorial ? activity.Tutorial.Info : TutorialInfo.None;
			string debugStr = string.Format("ActivityType: {0} IsTutorial: {1} TutorialInfo: {2}"
				, activity.Type, activity.IsTutorial, tutorialInfo);

			if(useLogError)
				Debug.LogError(debugStr);
			else
				Debug.Log(debugStr);
		}
	}
	*/
	#endregion

	#region Vehicles

	public Vehicle CurrentVehicle
	{
		get 
		{
			int currentVehicleInt = PlayerPrefs.GetInt("ServerGameState_currentVeh", (int)Vehicle.None);
			Vehicle currentVehicle = (Vehicle)currentVehicleInt;

			if(currentVehicle == Vehicle.None)
				currentVehicle = Vehicle.Surfboard;

			return currentVehicle;
		}
		set 
		{
			PlayerPrefs.SetInt("ServerGameState_currentVeh", (int)value);
		}
	}

	public List<VehicleInfo> GetAllVehicleInfo()
	{
		return allVehicleInfo;
	}

	public JeepType CurrentJeep
	{
		get 
		{
			int currentJeepInt = PlayerPrefs.GetInt("ServerGameState_currentJJeep", (int)JeepType.Default);
			return (JeepType)currentJeepInt;
		}
		set 
		{
			PlayerPrefs.SetInt("ServerGameState_currentJJeep", (int)value);
		}
	}


	#endregion

	#region Trophies
	/*
	public bool CheckUnlockedTrophy(Trophy checkTrophy)
	{
		return (PlayerPrefs.GetInt( "ServerGameState_trophy_" + (int)checkTrophy, 0) == 1);
	}

	public bool[] CheckAllUnlockedTrophies()
	{
		bool[] unlocks = new bool[(int)Trophy.Count];
		for (int trophyIndex = 0; trophyIndex < (int)Trophy.Count; trophyIndex++)
		{
			unlocks[trophyIndex] = (PlayerPrefs.GetInt( "ServerGameState_trophy_" + trophyIndex, 0) == 1);
		}
		return unlocks;
	}

	public void UnlockTrophy(Trophy checkTrophy)
	{
		PlayerPrefs.SetInt( "ServerGameState_trophy_" + (int)checkTrophy, 1);
	}

	public void UnlockAllTrophies()
	{
		for (int trophyIndex = 0; trophyIndex < (int)Trophy.Count; trophyIndex++)
		{
			PlayerPrefs.SetInt( "ServerGameState_trophy_" + trophyIndex, 1);
		}
	}
	*/

	#endregion

	public void GetLocationUnlocks(ref bool[] unlocks)
	{
		unlocks = new bool[(int)Location.Count];

		for (int i = 0; i < (int)Location.Count; i++)
		{
			unlocks[i] = (PlayerPrefs.GetInt("ServerGameState_loc_" + i, 0) != 1);
		}
	}

	public bool GetLocationUnlock(Location location)
	{
		return (PlayerPrefs.GetInt("ServerGameState_loc_" + (int)location, 0) != 1);
	}

	public void SetLocationUnlock(Location location)
	{
		PlayerPrefs.SetInt("ServerGameState_loc_" + (int)location, 1);
	}

}


