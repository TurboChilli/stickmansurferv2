﻿using System;

public class VehicleInfo
{
    public VehicleInfo(Vehicle type, bool owned, int cost, int shopImageIndex)
	{
		if(type == Vehicle.None)
			throw new ArgumentException("Vehicle type can't be set to 'None'");

		if(cost < 0)
			throw new ArgumentException("Vehicle cost cannot be less than 0");
		
		Type = type;
		Owned = owned;
		Cost = cost;
        ShopImageIndex = shopImageIndex;
	}

	public string Name { get; set; }
	public Vehicle Type { get; set; }
	public string Description { get; set; }
	public bool Owned { get; set; }
	public int Cost { get; set; }
	public int RequiresPlayerLevel { get; set; }
    public int ShopImageIndex { get; set; }
}
