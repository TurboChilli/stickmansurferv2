﻿using UnityEngine;
using System.Collections;

public enum TutStageType
{
	MainMenuGoButton = 0,
	FirstGame = 1,
	MainMenuChest = 2,
	MainMenuChestClose = 3,
	MainMenuShop = 4,
	FirstShopVisit = 5,
	PostShopVisit = 6,
	MainMenuJeep = 7,
	Finished = 8
}