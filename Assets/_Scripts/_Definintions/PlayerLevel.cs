using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerLevel
{
	const int firstLevelCred = 100;
	const float nextLevelGrowthFactor = 1.2f;

	// Public
	public int CurrentCred 
	{ 
		get
		{
			return PlayerPrefs.GetInt("PL_cc", 0);
		}
		private set
		{
			PlayerPrefs.SetInt("PL_cc", value);
		}
	}

	public int CurrentLevel 
	{ 
		get
		{
			return PlayerPrefs.GetInt("PL_cl", 1);
		}
		private set
		{
			PlayerPrefs.SetInt("PL_cl", value);
		}
	}

	public int NextLevelCred 
	{ 
		get
		{
			return PlayerPrefs.GetInt("PL_nlc", firstLevelCred);
		}
		private set
		{
			PlayerPrefs.SetInt("PL_nlc", value);
		}
	}



}
