﻿public enum WaveType
{
	Undefined = 0,
	Average = 1,
	Big = 2,
	Huge = 3,
}
