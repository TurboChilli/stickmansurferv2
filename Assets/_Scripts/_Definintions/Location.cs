using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ONLY FOR SURFING LOCATIONS
public enum Location
{
	MainBeach = 0,
	BigWaveReef = 1,
	TikiBay = 2,

	Cave = 3,
	WavePark = 4,
	ShipWreck = 5,
	Harbor = 6,

	Count,
	Undefined
}

public class LocationHelper
{
	public static bool IsOutdoorLocation(Location location)
	{
		switch(location)
		{
			case Location.MainBeach:
			case Location.BigWaveReef:
			case Location.TikiBay:
			case Location.Harbor:
				return true;

			case Location.Cave:
			case Location.WavePark:
			case Location.ShipWreck:
				return false;

			default:
				return false;
		}
	}

	public static Location[] GetIndoorSections()
	{
		return new Location[] {
			Location.Cave,
			Location.WavePark,
			Location.ShipWreck
		};
	}

	public static Location[] GetOutdoorSections()
	{
		return new Location[] {
			Location.MainBeach,
			Location.BigWaveReef,
			Location.TikiBay,
			Location.Harbor
		};
	}

	public static int GetOutdoorSectionNum()
	{
		int count = 0;
		for (int i = 0; i < (int)Location.Count; i++)
		{
			switch((Location)i)
			{
				case Location.MainBeach:
				case Location.BigWaveReef:
				case Location.TikiBay:
				case Location.Harbor:
					count++;
					break;
			};
		}
		return count;
	}

	public static int GetIndoorNum()
	{
		return (int)Location.Count - LocationHelper.GetOutdoorSectionNum();
	}
}



public class LocationTitle
{
	public static string Get(Location location)
	{
		switch (location)
		{
			case Location.MainBeach:
				return GameLanguage.LocalizedText(MainBeachTitle);
			case Location.BigWaveReef:
				return GameLanguage.LocalizedText(BigWaveReefTitle);
			case Location.WavePark:
				return GameLanguage.LocalizedText(WaveParkTitle);
			case Location.TikiBay:
				return GameLanguage.LocalizedText(TikiBayTitle);
			case Location.Cave:
				return GameLanguage.LocalizedText(CaveTitle);
			case Location.ShipWreck:
				return GameLanguage.LocalizedText(ShipWreckTitle);
			case Location.Harbor:
				return GameLanguage.LocalizedText(HarborTitle);
		};
		return string.Empty;
	}

	public const string MainBeachTitle = 	"Main Beach";
	public const string BigWaveReefTitle =  "Cactus Bay";
	public const string WaveParkTitle = 	"Wave Park";
	public const string TikiBayTitle = 		"Tiki Bay";
	public const string CaveTitle = 		"Cave";
	public const string ShipWreckTitle = 	"Ship Wreck";
	public const string HarborTitle = 		"Harbor";

};

public class LocationTime
{	
	public static float Get(Location location)
	{
		switch (location)
		{
			case Location.MainBeach:
				return MainBeachTime;
			case Location.BigWaveReef:
				return BigWaveReefTime;
			case Location.WavePark:
				return WaveParkTime;
			case Location.TikiBay:
				return TikiBayTime;
			case Location.Cave:
				return CaveTime;
			case Location.ShipWreck:
				return ShipWreckTime;
			case Location.Harbor:
				return HaborTime;
		};
		return 30.0f;
	}

	public const float MainBeachTime = 		30.0f;
	public const float BigWaveReefTime =  	30.0f;
	public const float TikiBayTime = 		30.0f;
	public const float WaveParkTime = 		20.0f;
	public const float CaveTime = 			15.0f;
	public const float ShipWreckTime = 		15.0f;
	public const float HaborTime =	 		30.0f;


}


