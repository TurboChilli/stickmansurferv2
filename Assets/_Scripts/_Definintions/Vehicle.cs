﻿using UnityEngine;
using System.Collections;


public enum Vehicle
{
	None = 0,
	Surfboard = 1,
	Jetski = 2,
	Bodyboard = 3,
	Tube = 4,
	Boat = 5,
	Longboard = 6,
	Plane = 7,
    Chilli = 8,
    Motorcycle = 9,
	WindSurfBoard = 10,
	Thruster = 11,
	GoldSurfboard = 12
};

public static class NumVehicle
{
	public const int Count = 12;
}

public class VehicleHelper
{
	public static bool IsStartingBoard(Vehicle vehicle)
	{
		if(vehicle == Vehicle.Surfboard ||
			vehicle == Vehicle.Thruster ||
			vehicle == Vehicle.GoldSurfboard)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static string GetName(Vehicle vehicle)
	{
		switch (vehicle)
		{
			case Vehicle.Boat:
				return GameLanguage.LocalizedText("Speed Boat");
			case Vehicle.Bodyboard:
				return GameLanguage.LocalizedText("Bodyboard");
			case Vehicle.Jetski:
				return GameLanguage.LocalizedText("Jet ski");
			case Vehicle.Longboard:
				return GameLanguage.LocalizedText("Longboard");
			case Vehicle.Surfboard:
				return GameLanguage.LocalizedText("Surfboard");
			case Vehicle.Tube:
				return GameLanguage.LocalizedText("Tube");
			case Vehicle.Plane:
				return GameLanguage.LocalizedText("Plane");
            case Vehicle.Thruster:
				return GameLanguage.LocalizedText("Thruster");
			case Vehicle.GoldSurfboard:
				return GameLanguage.LocalizedText("Gold board");
            case Vehicle.Chilli:
				return GameLanguage.LocalizedText("Chili");
            case Vehicle.Motorcycle:
				return GameLanguage.LocalizedText("Motorcycle");
			case Vehicle.WindSurfBoard:
				return GameLanguage.LocalizedText("Windsurfer");
		};

		return string.Empty;
	}

	public static Vehicle GetRandomVehicle()
	{
		return (Vehicle)( Random.Range(0, NumVehicle.Count - 2) + 2);
	}

	public static Vehicle GetWeightedRandomVehicle()
	{
		int[] vehicleWeights = new int[ NumVehicle.Count - 2];
		int totalWeight = 0;

		StickSurferSetting gameSettings = GameServerSettings.SharedInstance.SurferSettings;

		vehicleWeights[0] = gameSettings.RandomWeightJetski; 
		totalWeight += gameSettings.RandomWeightJetski;

		vehicleWeights[1] = gameSettings.RandomWeightBodyboard + vehicleWeights[0];
		totalWeight += gameSettings.RandomWeightBodyboard;

		vehicleWeights[2] = gameSettings.RandomWeightTube + vehicleWeights[1];
		totalWeight += gameSettings.RandomWeightTube;

		vehicleWeights[3] = gameSettings.RandomWeightBoat + vehicleWeights[2];
		totalWeight += gameSettings.RandomWeightBoat;

		vehicleWeights[4] = gameSettings.RandomWeightLongboard + vehicleWeights[3];
		totalWeight += gameSettings.RandomWeightLongboard;

		vehicleWeights[5] = gameSettings.RandomWeightPlane + vehicleWeights[4];
		totalWeight += gameSettings.RandomWeightPlane;

		vehicleWeights[6] = gameSettings.RandomWeightChilli + vehicleWeights[5];
		totalWeight += gameSettings.RandomWeightChilli;

		vehicleWeights[7] = gameSettings.RandomWeightMotorcycle + vehicleWeights[6];
		totalWeight += gameSettings.RandomWeightMotorcycle;

		vehicleWeights[8] = gameSettings.RandomWeightWindSurfBoard + vehicleWeights[7];
		totalWeight += gameSettings.RandomWeightWindSurfBoard;

		int checkValue = Random.Range(0, totalWeight + 1);
		for (int i = 0; i < vehicleWeights.Length; i++)
		{
			if (checkValue <= vehicleWeights[i])
			{
				return (Vehicle)(i + 2);
			}
		}
		return (Vehicle)(NumVehicle.Count + 1);
	}

	public static Vehicle GetNextWeightedRandomVehicle(Vehicle curVehicle)
	{
		if (curVehicle == Vehicle.Jetski)
			return Vehicle.Bodyboard;
		if (curVehicle == Vehicle.Bodyboard)
			return Vehicle.Tube;
		if (curVehicle == Vehicle.Tube)
			return Vehicle.Boat;
		if (curVehicle == Vehicle.Boat)
			return Vehicle.Longboard;
		if (curVehicle == Vehicle.Longboard)
			return Vehicle.Plane;
		if (curVehicle == Vehicle.Plane)
			return Vehicle.Chilli;
		if (curVehicle == Vehicle.Chilli)
			return Vehicle.Motorcycle;
		if (curVehicle == Vehicle.Motorcycle)
			return Vehicle.WindSurfBoard;
		if (curVehicle == Vehicle.WindSurfBoard)
			return Vehicle.Jetski;

		return Vehicle.Jetski;
	}

};