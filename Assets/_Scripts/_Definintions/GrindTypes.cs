﻿
public enum GrindType
{
	Pier = 0,
	Funbox = 1,
	TikiBridge = 2,
	RopeSwing = 3,
	Count
}	

public class GrindTypeTitle
{
	public static string Get(GrindType type)
	{
		switch (type)
		{
			case GrindType.Pier:
			return PierTitle;
			case GrindType.Funbox:
				return FunBoxTitle;
			case GrindType.TikiBridge:
				return TikiBridgeTitle;
			case GrindType.RopeSwing:
				return RopeSwingTitle;
			default:
				return "";
		};
	}

	const string PierTitle = "Pier"; // Main Beach
	const string FunBoxTitle = "Funbox"; // Wave Parks
	const string TikiBridgeTitle = "Tiki bridge"; // Tiki Bay
	const string RopeSwingTitle = "Rope swing";
}

public class GrindTypeTitlePlural
{
	public static string Get(GrindType type)
	{
		switch (type)
		{
			case GrindType.Pier:
			return PierTitle;
			case GrindType.Funbox:
				return FunBoxTitle;
			case GrindType.TikiBridge:
				return TikiBridgeTitle;
			case GrindType.RopeSwing:
				return RopeSwingTitle;
			default:
				return "";
		};
	}

	const string PierTitle = "Piers"; // Main Beach
	const string FunBoxTitle = "Funboxs"; // Wave Parks
	const string TikiBridgeTitle = "Tiki bridges"; // Tiki Bay
	const string RopeSwingTitle = "Rope swings";
}