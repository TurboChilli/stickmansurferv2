﻿using UnityEngine;
using System.Collections;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

[DynamoDBTable("StickSurferSetting")]
public class StickSurferSetting
{
	#if UNITY_ANDROID
	
	[DynamoDBHashKey]   // Hash key (Primary key)
	public string StickSurferSettingId = "v1.0Android";

	#else
	
	[DynamoDBHashKey]   // Hash key (Primary key)
	public string StickSurferSettingId = "v1.0";
	
	#endif


    [DynamoDBProperty]
    public int ContactServerLoginTimeout = 30;


	[DynamoDBProperty]
	public string SettingsVersionNumber = "0";

    [DynamoDBProperty]  
	public string ShackTvUrl = "https://www.youtube.com/watch?v=C-x3zno3XsI";

	[DynamoDBProperty]
	public int RefreshIntervalMinutes = 30; // default refresh settings 

	[DynamoDBProperty]
	public bool ShowAdsAfterTut = true;

	[DynamoDBProperty]
	public bool EnableFluff = false;

	[DynamoDBProperty]
	public int SecsBtwGameSavedSync = 1;

	[DynamoDBProperty]
	public bool ShowRestorePurchaseButton = true;

	[DynamoDBProperty]
	public bool ShowFBButt = false;

	[DynamoDBProperty]
	public bool ShowFBButtIfNotLoggedIn = false;

	[DynamoDBProperty]
	public bool EnableDoubleCoinsEnergyDrink = false;

	[DynamoDBProperty]
	public int RefreshProductInfoEveryMins = 120;

	[DynamoDBProperty]
	public float VideoAdCooldown = 60.0f;

	[DynamoDBProperty]
	public int DefaultEnergyDrinks = 0;

	[DynamoDBProperty]
	public int FirstEnergyDrinkPrize = 2;

	[DynamoDBProperty]
	public int DefaultCoins = 10000;

	[DynamoDBProperty]
	public float EmotionTimerMin = 0.75f;

	[DynamoDBProperty]
	public float EmotionTimerMax = 1.5f;

	[DynamoDBProperty]
	public int EnergyDrinkToGoldRatio = 1000;

	[DynamoDBProperty]
	public int GoldSuperRewardStepUp = 50;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount1 = 20;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount2 = 100;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount3 = 220;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount4 = 500;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount5 = 750;

	[DynamoDBProperty]
	public int IAPEnergyPackAmount6 = 1500;

	[DynamoDBProperty]
	public int DefaultTimerManagerSecsInterval = 28800; // 8 hours

	[DynamoDBProperty]
	public string DefaultTimerManagerSecs = "1,0|600,10800,18000,43200,86400";

    [DynamoDBProperty]
    public int NumberOfTimesStarterPackPopupIsShown = 3;

    [DynamoDBProperty]
    public bool ShowTheStarterPackDialog = false;

	/// <summary>
	/// Definition of timer format string;
	/// [CurrentVersion] , [StartTimerAtIndexForExistingUsers] | [SecondsTillTimerActivates], [SecondsTillTimerActivates1], [SecondsTillTimerActivates2]...
	/// </summary>
	[DynamoDBProperty]
	public string TreasureChestTimerSecs = "1,0|600,10800,18000,43200";

	/// <summary>
	/// Definition of timer format string;
	/// [CurrentVersion] , [StartTimerAtIndexForExistingUsers] | [SecondsTillTimerActivates], [SecondsTillTimerActivates1], [SecondsTillTimerActivates2]...
	/// </summary>
	[DynamoDBProperty]
	public string HalfPipeTimerSecs = "1,0|0,300,1200,7200,14400";

	[DynamoDBProperty]
	public string GameConsoleTimerSecs = "1,0|300,600,1200,7200";

	/// <summary>
	/// Definition of timer format string;
	/// [CurrentVersion] , [StartTimerAtIndexForExistingUsers] | [SecondsTillTimerActivates], [SecondsTillTimerActivates1], [SecondsTillTimerActivates2]...
	/// </summary>
	[DynamoDBProperty]
	public string JeepTimerSecs = "1,0|900,3600,10800,21600,43200";

	[DynamoDBProperty]
	public int SkateNowEnergyDrinkCost = 1;

	[DynamoDBProperty]
	public int GameConsoleNowEnergyDrinkCost = 1;

	[DynamoDBProperty]
	public float GameConsoleMaxGameTime = 45.0f;

	[DynamoDBProperty]
	public float XVelocityIncreaseRate = 0.035f;
		
	[DynamoDBProperty]
	public float MaxMaxXVelocity = 18f;

	[DynamoDBProperty]
	public float MaxMaxXRetryVelocity = 14f;

	[DynamoDBProperty]
	public float MinDistanceBetweenChoppers = 175f;

	[DynamoDBProperty]
	public float MinDistanceBetweenPowerups = 100f;

	[DynamoDBProperty]
	public float MinDistanceBetweenSharks   = 350f;

	[DynamoDBProperty]
	public int StarterPackNumEnergyDrinks = 100;

	[DynamoDBProperty]
	public int StarterPackNumCoins = 25000;

	[DynamoDBProperty]
	public int StarterPackFirstShowMins = 30;

	[DynamoDBProperty]
	public int StarterPackShowBetweenMins = 1440;

	[DynamoDBProperty]
	public int StarterPackShowSeconds = 60;

	[DynamoDBProperty]
	public int JeepRefuelEnergyDrinkCost = 1;

	[DynamoDBProperty]
	public int JeepFlipCountReward = 100;

	[DynamoDBProperty]
	public int JeepCoinValue = 5;

	[DynamoDBProperty]
	public float EnergyDrinkCostPerLevel = 0.2f;

	[DynamoDBProperty]
	public int RetryEnergyDrinkCost = 1;

	[DynamoDBProperty]
	public int RetryEnergyDrinkCostMax = 16;

	[DynamoDBProperty]
	public int SurfMinTutorialDistance = 100;

	[DynamoDBProperty]
	public int MinSaveMeDistance = 150;

	[DynamoDBProperty]
	public int GoalCompleteStart = 800;

	[DynamoDBProperty]
	public int GoalCompleteRewardPerLevel = 200;

	[DynamoDBProperty]
	public int GoalCompleteRewardCap = 5000;

	[DynamoDBProperty]
	public int MinSecsToNotification = 0;

	[DynamoDBProperty]
	public int SecsBeforeFirstInterstitialShown = 90;

	[DynamoDBProperty]
	public int SecsBeforeFirstInterstitialShownFirstSession = 90;

	[DynamoDBProperty]
	public bool EnableInterstitialMainMenu = true;

	[DynamoDBProperty]
	public bool EnableInterstitialEndGame = true;

	[DynamoDBProperty]
	public int EndGameRestartsBeforeInterstitialShown = 3;

	// Positive value denotes coins
	[DynamoDBProperty]
	public int FirstTreasureRewardsRandom1 = 1000;

	// Minus denotes energy drinks, -2 means 2 energy drinks.
	[DynamoDBProperty]
	public int FirstTreasureRewardsRandom2 = -2;

	// Positive value denotes coins
	[DynamoDBProperty]
	public int FirstTreasureRewardsRandom3 = 400;

	[DynamoDBProperty]
	public int SuperCoinChainMultiple = 25;

	#region LevelGenerator Difficulty

	[DynamoDBProperty]
	public float EasyDifficultyDistance = 250.0f;

	[DynamoDBProperty]
	public float MediumDifficultyDistance = 400.0f;

	[DynamoDBProperty]
	public float HardDifficultyDistance = 800.0f;


	[DynamoDBProperty]
	public float EasyDifficulty_MediumChunkChance = 0.3f;

	[DynamoDBProperty]
	public float EasyDifficulty_HardChunkChance = 0.3f;


	[DynamoDBProperty]
	public float MediumDifficulty_HardChunkChance = 0.5f;

	[DynamoDBProperty]
	public float MediumDifficulty_MediumChunkChance = 0.5f;


	[DynamoDBProperty]
	public float HardDifficulty_MediumChunkChance = 0.5f;

	#endregion


	#region Prize Amounts

	[DynamoDBProperty]
	public int TreasureReward50 = 200;

	[DynamoDBProperty]
	public int TreasureReward100 = 400;

	[DynamoDBProperty]
	public int TreasureReward200 = 500;

	[DynamoDBProperty]
	public int TreasureReward400 = 750;

	[DynamoDBProperty]
	public int TreasureReward1000 = 1000;

	[DynamoDBProperty]
	public int TreasureReward2000 = 2000;

	[DynamoDBProperty]
	public int TreasureRewardE1 = -1;

	[DynamoDBProperty]
	public int TreasureRewardE2 = -1;

	[DynamoDBProperty]
	public int TreasureRewardE3 = -2;

	[DynamoDBProperty]
	public int ShellGameReward50 = 50;

	[DynamoDBProperty]
	public int ShellGameReward100 = 100;

	[DynamoDBProperty]
	public int ShellGameReward500 = 250;

	[DynamoDBProperty]
	public int ShellGameReward1000 = 500;

	[DynamoDBProperty]
	public int ShellGameReward2000 = 1000;

	[DynamoDBProperty]
	public int ShellGameRewardE1 = 1;

	[DynamoDBProperty]
	public int ShellGameRewardE2 = 2;


	#endregion

	#region Random Weights

	[DynamoDBProperty]
	public int RandomWeightJetski = 8;

	[DynamoDBProperty]
	public int RandomWeightBodyboard = 8;

	[DynamoDBProperty]
	public int RandomWeightTube = 10;

	[DynamoDBProperty]
	public int RandomWeightBoat = 10;

	[DynamoDBProperty]
	public int RandomWeightWindSurfBoard = 8;

	[DynamoDBProperty]
	public int RandomWeightLongboard = 10;

	[DynamoDBProperty]
	public int RandomWeightPlane = 10;

	[DynamoDBProperty]
	public int RandomWeightChilli = 25;

	[DynamoDBProperty]
	public int RandomWeightMotorcycle = 25;

	[DynamoDBProperty]
	public int TreasureWeightReward50 = 20;

	[DynamoDBProperty]
	public int TreasureWeightReward100 = 20;

	[DynamoDBProperty]
	public int TreasureWeightReward200 = 20;

	[DynamoDBProperty]
	public int TreasureWeightReward400 = 20;

	[DynamoDBProperty]
	public int TreasureWeightReward1000 = 10;

	[DynamoDBProperty]
	public int TreasureWeightReward2000 = 5;

	[DynamoDBProperty]
	public int TreasureWeightRewardE1 = 1;

	[DynamoDBProperty]
	public int TreasureWeightRewardE2 = 1;

	[DynamoDBProperty]
	public int TreasureWeightRewardE3 = 1;

	[DynamoDBProperty]
	public int SuperRewardWeightCoins = 20;

	[DynamoDBProperty]
	public int SuperRewardWeightSingleEnergy = 1;

	[DynamoDBProperty]
	public int SuperRewardWeightDoubleEnergy = 1;

	[DynamoDBProperty]
	public int ShellGameMaxNumberSwaps = 14;

	[DynamoDBProperty]
	public int ShellGameMaxRandomMin = 8;
	[DynamoDBProperty]
	public int ShellGameMaxRandomMax = 20;

	[DynamoDBProperty]
	public int ShellGameRewardWeight50 = 2;

	[DynamoDBProperty]
	public int ShellGameRewardWeight100 = 10;

	[DynamoDBProperty]
	public int ShellGameRewardWeight500 = 20;

	[DynamoDBProperty]
	public int ShellGameRewardWeight1000 = 10;

	[DynamoDBProperty]
	public int ShellGameRewardWeight2000 = 2;

	[DynamoDBProperty]
	public int ShellGameRewardWeightE1 = 2;

	[DynamoDBProperty]
	public int ShellGameRewardWeightE2 = 2;

	#endregion

	#region Reward Percentages

	[DynamoDBProperty]
	public float TreasureRewardPrecentage50 = 0.05f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentage100 = 0.10f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentage200 = 0.15f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentage400 = 0.20f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentage1000 = 0.25f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentage2000 = 0.5f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentageE1 = 0.75f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentageE2 = 1.0f;

	[DynamoDBProperty]
	public float TreasureRewardPrecentageE3 = 1.25f;


	[DynamoDBProperty]
	public float ShellGameRewardPercentage50 = 0.1f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentage100 = 0.25f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentage500 = 0.33f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentage1000 = 0.5f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentage2000 = 1.0f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentageE1 = 1.5f;

	[DynamoDBProperty]
	public float ShellGameRewardPercentageE2 = 2.0f;

	#endregion

	#region Shop Settings

	// Format [UpgradeItemType->ID],[LevelRequired]|....
	[DynamoDBProperty]
	public string UpgradeItemTypeIDLevelReq = "63,8|122,18|1006,15|1009,20|101,12|13,12|22,8|112,15|53,17|32,12|42,10|82,10|1011,10|1012,25";

	// Format [UpgradeItemType->ID],[EnergyDrinkCost]|....
	[DynamoDBProperty]
	public string UpgradeItemTypeIDEnergyDrinkCost = "62,30|151,75|2,10";

	[DynamoDBProperty]
	public int Upgrade2Multiple = 4;

	[DynamoDBProperty]
	public int Upgrade3Multiple = 7;

	[DynamoDBProperty]
	public int Upgrade4Multiple = 10;

	[DynamoDBProperty]
	public int Upgrade5Multiple = 15;

	[DynamoDBProperty]
	public int Upgrade6Multiple = 20;

	[DynamoDBProperty]
	public int ThrusterBoardPrice = 7500;

	[DynamoDBProperty]
	public int GoldSurfBoardPrice = 100000;

	[DynamoDBProperty]
	public int JeepBlackPrice = 125000;

	[DynamoDBProperty]
	public int QuadBikePrice = 55000;

	[DynamoDBProperty]
	public int RetroFanPrice = 35000;

	// HEADS
	[DynamoDBProperty]
	public int HeadPaperBagPrice = 35000;

	[DynamoDBProperty]
	public int HeadMulletPrice = 35000;

	[DynamoDBProperty]
	public int HeadRastaHatPrice = 20000;

	[DynamoDBProperty]
	public int HeadDisguisePrice = 38000;

	[DynamoDBProperty]
	public int HeadTopHatPrice = 37000;

	[DynamoDBProperty]
	public int HeadPumpkinPrice = 50000;

	[DynamoDBProperty]
	public int HeadPirateHatPrice = 35000;

	[DynamoDBProperty]
	public int HeadMohawkPrice = 32500;

	[DynamoDBProperty]
	public int HeadCowboyHatPrice = 28500;

	[DynamoDBProperty]
	public int HeadEmoHairPrice = 42000;

	[DynamoDBProperty]
	public int HeadSharkPrice = 45000;

	[DynamoDBProperty]
    public int HeadNewspaperPrice = 15000;

	[DynamoDBProperty]
    public int HeadCaptainPrice = 30000;

	[DynamoDBProperty]
    public int HeadDuckPrice = 38000;

	[DynamoDBProperty]
    public int HeadSnorkelPrice = 30000;

	[DynamoDBProperty]
    public int HeadPineapplePrice = 45000;

	[DynamoDBProperty]
    public int HeadElfPrice = 30000;

	[DynamoDBProperty]
    public int HeadOctopusPrice = 38000;

	[DynamoDBProperty]
    public int HeadSwimcapPrice = 20000;

	[DynamoDBProperty]
    public int HeadNekoEarsPrice = 30000;

    [DynamoDBProperty]
	public int HeadCatPrice = 30000;

    // HEADS END

	[DynamoDBProperty]
	public int JeepFuelPrice = 10000;

	[DynamoDBProperty]
	public int JeepEnginePrice = 15000;

	[DynamoDBProperty]
	public int JeepWheelsPrice = 9000;

	[DynamoDBProperty]
	public int SkateGoldPrice = 75000;

	[DynamoDBProperty]
	public int SkateDurationPrice = 15000;

	[DynamoDBProperty]
	public int JetskiFuelPrice = 7000;

	[DynamoDBProperty]
	public int JetskiGoldPrice = 100000;

	[DynamoDBProperty]
	public int JetskiMagnetPrice = 75000;

	[DynamoDBProperty]
	public int PlaneFuelPrice = 10000;

	[DynamoDBProperty]
	public int PlaneGoldPrice = 125000;

	[DynamoDBProperty]
	public int PlaneMagnetPrice = 150000;

	[DynamoDBProperty]
	public int BodyboardGoldPrice = 50000;

	[DynamoDBProperty]
	public int BodyboardMagnetPrice = 40000;

	[DynamoDBProperty]
	public int MotorbikeFuelPrice = 5000;

	[DynamoDBProperty]
	public int MotorbikeGoldPrice = 120000;

	[DynamoDBProperty]
	public int MotorbikeMagnetPrice = 80000;

	[DynamoDBProperty]
	public int SpeedBoatFuelPrice = 12000;

	[DynamoDBProperty]
	public int SpeedBoatGoldPrice = 85000;

	[DynamoDBProperty]
	public int SpeedBoatMagnetPrice = 75000;

	[DynamoDBProperty]
	public int TubeGoldPrice = 50000;

	[DynamoDBProperty]
	public int TubeMagnetPrice = 45000;

	[DynamoDBProperty]
	public int LongboardGoldPrice = 60000;

	[DynamoDBProperty]
	public int LongboardMagnetPrice = 50000;

	[DynamoDBProperty]
	public int ChilliGoldPrice = 60000;

	[DynamoDBProperty]
	public int ChilliMagnetPrice = 50000;

	[DynamoDBProperty]
	public int WindSurfBoardGoldPrice = 65000;

	[DynamoDBProperty]
	public int WindSurfBoardMagnetPrice = 55000;

	[DynamoDBProperty]
	public int PowerUpGoldDurationPrice = 25000;

	[DynamoDBProperty]
	public int PowerUpMagnetDurationPrice = 20000;

	#endregion

}

public class GameServerSettings
{
	static GameServerSettings instance;
	public static GameServerSettings SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new GameServerSettings();

			return instance;
		}
	}

	public StickSurferSetting SurferSettings { get; private set; }

	protected GameServerSettings()
	{
		if(!PlayerPrefs.HasKey("serverStickSurfSettings"))
		{
			SurferSettings = new StickSurferSetting();
			SaveSurferSettingsToPlayerPref();
		}
		else
		{
			string surferSettingsJson = PlayerPrefs.GetString("serverStickSurfSettings");
			SurferSettings = JsonUtility.FromJson<StickSurferSetting>(surferSettingsJson);
		}
	}

	public void UpdateSurferSettings(StickSurferSetting surferSetting)
	{
		SurferSettings = surferSetting;
		SaveSurferSettingsToPlayerPref();
	}

	void SaveSurferSettingsToPlayerPref()
	{
		string surferSettingsJson = JsonUtility.ToJson(SurferSettings);
		PlayerPrefs.SetString("serverStickSurfSettings", surferSettingsJson);
	}

	public void PrintSurferSettingsContent()
	{
		string surferSettingsJson = JsonUtility.ToJson(SurferSettings, true);
		//////Debug.Log(surferSettingsJson);
	}

}