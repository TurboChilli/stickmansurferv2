using UnityEngine;
using System.Collections;

public class WaveTextureSlide : MonoBehaviour
{
	public bool CanBeControlled = false;

	public Vector2 WaveValues  = new Vector2(-0.3f, -0.5f);
	public float   WaveSpeed   = 2f;
	public Vector2 CrestValues = new Vector2(1f, 0f);
	public float   CrestSpeed  = 2f;

	private Renderer waveRenderer = null;

    float initWaveSpeed = 0;

    void Start()
    {
        initWaveSpeed = WaveSpeed;
		if (waveRenderer == null)
			waveRenderer = GetComponent<Renderer>();
    }

	void Update ()
	{
		if(GlobalSettings.surfGamePaused)
			return;
		
		for(int i = 0; i < waveRenderer.materials.Length; i++)
		{
			Vector2 textureOffset;
			float   speed;
			if(i == 0)
			{
				textureOffset = CrestValues;
				speed  = CrestSpeed;
			}
			else
			{
				textureOffset = WaveValues;
				speed  = WaveSpeed;
			}
		
            speed = initWaveSpeed;
			waveRenderer.materials[i].mainTextureOffset += textureOffset * speed * Time.smoothDeltaTime;

			Vector2 curTextureScale = waveRenderer.materials[i].mainTextureScale;
			textureOffset = waveRenderer.materials[i].mainTextureOffset;

			if (textureOffset.x > curTextureScale.x)
				textureOffset.x = textureOffset.x - curTextureScale.x;
			if (textureOffset.x < -curTextureScale.x)
				textureOffset.x = textureOffset.x + curTextureScale.x;

			if (textureOffset.y > curTextureScale.y)
				textureOffset.y = textureOffset.y - curTextureScale.y;
				textureOffset.y = textureOffset.y + curTextureScale.y;
			
			waveRenderer.materials[i].mainTextureOffset = textureOffset;
		}
	}

	public void SetWaveSpeedForTextureOffset(float waveCameraXOffset)
	{
		//waveCameraXOffset = Mathf.Min(waveCameraXOffset, 16f);
		// wavespeed = 0, waveX = -0.3f;
		// wavespeed = 10 waveX = 3f;
		float offsetValue = waveCameraXOffset / 4f * 0.3f;
		//Debug.Log ("Setting Wave Offset for speed:" + waveCameraXOffset + " Value:" + offsetValue);
		WaveValues.x = (offsetValue);

		CrestValues.x = WaveValues.x * -1f;
		//WaveValues.y = (offsetValue);
	}
}
