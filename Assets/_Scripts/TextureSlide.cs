using UnityEngine;
using System.Collections;

public class TextureSlide : MonoBehaviour
{
	private Renderer _render;

	public bool CanBeControlled = false;

    public bool ignoreGamePause = false;
	public float ValueX = 0f;
	public float ValueY = 0f;
	public float Speed = 0f;

    private Vector2 startXOffset;
    private Vector2 startXScale;

    private bool isInitialised = false;

	// Use this for initialization
	void Start ()
	{
		if (!isInitialised)
			Initialise();
	}

    void ResetTexturePosition()
    {
		if (!isInitialised)
			Initialise();
				
        foreach(Material m in _render.materials)
        {
            m.mainTextureOffset = startXOffset;
            m.mainTextureScale = startXScale;
        }
    }

    private void Initialise()
    {
		_render = gameObject.GetComponent<Renderer>();
        startXOffset = _render.material.mainTextureOffset;
        startXScale = _render.material.mainTextureScale;
		isInitialised = true;
    }

	// Update is called once per frame
	void Update ()
	{
        if (!ignoreGamePause)
        {
            if (GlobalSettings.cameraMovementIngamePaused)
                return;
        
            if (GlobalSettings.surfGamePaused)
                return;
        }
		foreach(Material m in _render.materials)
		{
			m.mainTextureOffset += new Vector2(ValueX * Speed, ValueY * Speed) * Time.deltaTime;
            if (m.mainTextureOffset.x > 1)
            {
                m.mainTextureOffset = new Vector2(m.mainTextureOffset.x - 1, m.mainTextureOffset.y);
            }
		}
	}

	void OnGUI()
	{
		/*
		if (!CanBeControlled) {
			return;
		}
		if (GUIEnabler.foamEnabled == 0 && _render.name == "wave") {
			GUI.Label(new Rect(10, Screen.height * 0.1f + 5, 90, Screen.height * 0.05f), "TextureSpeed:");
			Speed = GUI.HorizontalSlider(new Rect(100, Screen.height * 0.1f + 10, Screen.width * 0.85f, Screen.height * 0.05f), Speed, 0f, 5f);
		}
		if (GUIEnabler.foamEnabled == 1 && _render.name == "back_wave") {
			GUI.Label(new Rect(10, Screen.height * 0.1f + 5, 90, Screen.height * 0.05f), "TextureSpeed:");
			Speed = GUI.HorizontalSlider(new Rect(100, Screen.height * 0.1f + 10, Screen.width * 0.85f, Screen.height * 0.05f), Speed, 0f, 5f);
		}
		*/
	}
}
