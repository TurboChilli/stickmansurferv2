﻿using System;
using UnityEngine;
using System.Collections;

public class DialogPanel : MonoBehaviour {

	public Camera mainCamera;
	public GameObject closeButton;
	public GameObject blackOverlay;

	public enum DialogPanelState
	{
		Hidden,
		Visible,
	}

	DialogPanelState state = DialogPanelState.Hidden;
	public DialogPanelState State
	{
		get { return state; }
		private set { state = value; }
	}

	float? showDelaySecs = null;

	private float curTimeToMove = -1.0f;
	private float timeToMove = 0.35f;

	private Vector3 activePosition;
	private Vector3 inactivePosition;


	void Start () {
		Hide();
	}

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.WatchVideosAndWinDialogShowing)
			{
				DialogTrackingFlags.FlagJustClosedPopupDialog();

				Hide();
			}
		}
		#endif 
	}

	void Update () 
	{
		CheckAndroidBackButton();

		if(showDelaySecs.HasValue)
		{
			if(showDelaySecs.Value >= 0)
			{
				showDelaySecs -= Time.deltaTime;
				if(showDelaySecs.Value <= 0)
				{
					showDelaySecs = null;
					Show();
				}
			}
		}

		if (State == DialogPanelState.Visible)
		{
			if (curTimeToMove >= 0)
			{
				float lerpAmount = curTimeToMove / timeToMove;
				transform.position = Vector3.Lerp(inactivePosition, activePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f) );
				curTimeToMove -= Time.deltaTime;
			}
			else
				transform.position = activePosition;

			if( Input.GetMouseButtonDown(0))
			{
				if(UIHelpers.CheckButtonHit(closeButton.transform, mainCamera) && State == DialogPanelState.Visible)
				{
					Hide();
				}
			}
		}

	}
		
	public void Hide()
	{
		DialogTrackingFlags.WatchVideosAndWinDialogShowing = false;

		var pos = this.transform.position;
		pos.x = -99999f;
		this.transform.position = pos;

		blackOverlay.transform.position += Vector3.left * -999999f;

		State = DialogPanelState.Hidden;
	}

	public void Show()
	{
		DialogTrackingFlags.WatchVideosAndWinDialogShowing = true;

		UIHelpers.SetTransformToCenter(this.transform, mainCamera);

		activePosition = this.transform.position;
		inactivePosition = activePosition + Vector3.left * 1000;

		UIHelpers.SetTransformToCenter(blackOverlay.transform, mainCamera);

		transform.position = inactivePosition;

		curTimeToMove = timeToMove;

		State = DialogPanelState.Visible;
	}

	public void ShowDelay(float delaySecs)
	{
		DialogTrackingFlags.WatchVideosAndWinDialogShowing = true;

		showDelaySecs = delaySecs;
	}
}
