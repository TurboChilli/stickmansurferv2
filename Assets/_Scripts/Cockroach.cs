﻿using UnityEngine;
using System.Collections;

public class Cockroach : MonoBehaviour {

	public GameObject cockroachObject;
	public GameObject cockroachSplat;
	public GameObject[] cockroachMarkers;
	public AudioSource splatSound;
	public AudioSource coinSound;

	public GameObject coinObject;
	private Material coinMaterial;
	private float curCoinMoveTime = 0.0f;
	private float coinMoveTime = 1.5f;
	private Vector3 startCoinPos;
	private Vector3 endCoinPos;

	private int lastMarkerIndex = -1;
	private int curMarkerIndex = -1;

	private float cockroachSpeed = 200.0f;
	private float curMoveTime = 0.0f;
	private float moveTime = 0.0f;
	private bool movingCoin = false;

	private Quaternion prevRotation;
	private Quaternion curRotation;

	private float minWaitTime = 2.0f;
	private float maxWaitTime = 1.0f;
	private float waitTime = 1.0f;
	private bool isWaiting = false;

	private bool isMoving = false;
	GameCenterPluginManager gameCenterPluginManager = null;

	public void Initialise()
	{
		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();
		
		lastMarkerIndex = 0;
		curMarkerIndex = 1;
		cockroachObject.transform.position = cockroachMarkers[0].transform.position;

		Vector3 direction = cockroachMarkers[1].transform.position - cockroachMarkers[0].transform.position;
		float distance = direction.magnitude;

		direction.Normalize();
		float angle = Mathf.Rad2Deg * Mathf.Atan2( direction.y, direction.x);
		prevRotation = curRotation = Quaternion.AngleAxis(angle, Vector3.forward);

		coinMaterial = coinObject.GetComponent<Renderer>().material;
		coinObject.SetActive(false);

		moveTime = distance / cockroachSpeed;
		curMoveTime = moveTime;
		isMoving = true;
	}

	public void Splat()
	{
		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementPestController();
		
		isMoving = false;
		movingCoin = true;

		cockroachSplat.SetActive(true);
		cockroachObject.SetActive(false);

		cockroachSplat.transform.position = cockroachObject.transform.position;
		cockroachSplat.transform.rotation = Quaternion.AngleAxis( Random.Range(10, 50), Vector3.forward);

		startCoinPos = cockroachObject.transform.position + Vector3.back;
		endCoinPos = startCoinPos + Vector3.up * 20.0f;
		curCoinMoveTime = coinMoveTime;

		GameState.SharedInstance.AddCash(1);
		GlobalSettings.PlaySound(splatSound);
		GlobalSettings.PlaySound(coinSound);

		coinObject.SetActive(true);
	}

	void Update () 
	{
		if (isMoving)
		{
			if (!isWaiting)
			{
				float lerpAmount = curMoveTime / moveTime;

				Vector3 fromPos = cockroachMarkers[lastMarkerIndex].transform.position;
				Vector3 toPos = cockroachMarkers[curMarkerIndex].transform.position;

				Quaternion jitter = Quaternion.AngleAxis( Mathf.Sin(Time.time * 50.0f) * 5.0f, Vector3.forward);

				cockroachObject.transform.position = Vector3.Lerp(fromPos, toPos, 1.0f - lerpAmount);
				cockroachObject.transform.rotation = jitter * Quaternion.Slerp(prevRotation, curRotation, (1.0f - lerpAmount) * 20.0f);

				curMoveTime -= Time.deltaTime;
				if (curMoveTime < 0)
				{
					isWaiting = true;
					waitTime = Random.Range(minWaitTime, maxWaitTime);
				}
			}
			else
			{
				waitTime -= Time.deltaTime;
				if (waitTime < 0)
				{
					if (curMarkerIndex < (cockroachMarkers.Length - 1))
					{
						lastMarkerIndex = curMarkerIndex;
						curMarkerIndex = (curMarkerIndex + 1 );

						Vector3 direction = cockroachMarkers[curMarkerIndex].transform.position - cockroachMarkers[lastMarkerIndex].transform.position;
						float distance = direction.magnitude;

						direction.Normalize();
						float angle = Mathf.Rad2Deg * Mathf.Atan2( direction.y, direction.x);
						prevRotation = curRotation;
						curRotation = Quaternion.AngleAxis(angle, Vector3.forward);

						moveTime = distance / cockroachSpeed;
						curMoveTime = moveTime;

						isWaiting = false;
					}
					else
					{
						cockroachObject.SetActive(false);
						isMoving = false;
					}
				}
			}
		}
		else
		{
			if (movingCoin)
			{
				float lerpAmount = curCoinMoveTime / coinMoveTime;
				coinObject.transform.position = Vector3.Lerp(startCoinPos, endCoinPos, 1.0f - lerpAmount);
				Color curColor = coinMaterial.color;
				curColor.a = Mathf.Min(lerpAmount * 20, 1.0f);
				coinMaterial.color = curColor;

				curCoinMoveTime -= Time.deltaTime;
				if (curCoinMoveTime < 0.0f)
				{
					movingCoin = false;
					coinObject.SetActive(false);
				}
			}
			
		}
	}
}
