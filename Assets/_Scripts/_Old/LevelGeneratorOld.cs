﻿using UnityEngine;
using System.Collections;

public class LevelGeneratorOld : MonoBehaviour 
{
	public enum TerrainType
	{
		ROCKS = 0,
		PIER = 1,
	};

	private int nextRockTerrainCounter = 0;
	private int nextPierTerrainCounter = 0;

	public Transform rockSectionsParentEasy;
	public Transform rockSectionsParentMedium;
	public Transform rockSectionsParentHard;
	public Transform rockSectionsParentBigWaveMedium;
	public Transform pierSectionsParent;

	private Transform rockSectionsParent;
	private Transform[] rockSections;
	private Transform[] pierSections;

	private float lastTerrainFinishX = 20f;
	private float terainStandardYScale = 4f;
	private float terrainSectionYPos = 1.1f;

	private int terrainCount = 0;

	private bool lastWasRockTerrain = true;
	private bool doubleUp = false;

	public void InitLevel () 
	{
		if(GlobalSettings.currentDifficulty == GlobalSettings.LevelDifficulty.Easy)
		{
			rockSectionsParent = rockSectionsParentEasy;
		}
		else if(GlobalSettings.currentDifficulty == GlobalSettings.LevelDifficulty.Medium)
		{
			rockSectionsParent = rockSectionsParentMedium;
		}
		else if(GlobalSettings.currentDifficulty == GlobalSettings.LevelDifficulty.Hard)
		{
			rockSectionsParent = rockSectionsParentHard;
		}
		else if(GlobalSettings.currentDifficulty == GlobalSettings.LevelDifficulty.BigWaveMedium)
		{
			rockSectionsParent = rockSectionsParentBigWaveMedium;
		}
		else
		{
			rockSectionsParent = rockSectionsParentBigWaveMedium;
		}

		// Init ROCK SECTIONS
		int rockSectionCount = 0;
		for(int i = 0; i < rockSectionsParent.childCount; i++)
		{
			if(rockSectionsParent.GetChild(i).parent == rockSectionsParent)
			{
				if(rockSectionsParent.GetChild(i).gameObject.activeSelf)
				{
					rockSectionCount ++;
				}
			}
		}

		Debug.Log ("ROCK SECTION COUNT: " + rockSectionCount);
		rockSections = new Transform[rockSectionCount];

		int rockSectionCounter = 0;
		for(int i = 0; i < rockSectionsParent.childCount; i++)
		{
			if(rockSectionsParent.GetChild(i).parent == rockSectionsParent)
			{
				if(rockSectionsParent.GetChild(i).gameObject.activeSelf)
				{
					rockSections[rockSectionCounter] = rockSectionsParent.GetChild(i);
					rockSections[rockSectionCounter].GetComponent<Renderer>().enabled = false;
					rockSectionCounter ++;
					Debug.Log ("ADDED A ROCK SECTION: " + rockSectionCounter);
				}
			}
		}        

		// INIT PIER SECTIONS
		int pierSectionCount = 0;
		for(int i = 0; i < pierSectionsParent.childCount; i++)
		{
			if(pierSectionsParent.GetChild(i).parent == pierSectionsParent)
			{
				if(pierSectionsParent.GetChild(i).gameObject.activeSelf)
				{
					pierSectionCount ++;
				}
			}
		}
		
		pierSections = new Transform[pierSectionCount];
		
		int pierSectionCounter = 0;
		for(int i = 0; i < pierSectionsParent.childCount; i++)
		{
			if(pierSectionsParent.GetChild(i).parent == pierSectionsParent)
			{
				if(pierSectionsParent.GetChild(i).gameObject.activeSelf)
				{
					pierSections[pierSectionCounter] = pierSectionsParent.GetChild(i);
					pierSections[pierSectionCounter].GetComponent<Renderer>().enabled = false;
					pierSectionCounter ++;
				}
			}
		}

		//Debug.Log ("INITIALISED LEVEL ROCK SECTIONS WITH COUNT:" + rockSectionCount);
		terrainSectionYPos = CalculateTerrainSectionYPosBasedOnSectionScale(rockSectionsParent.GetChild(0).gameObject);

		CreateNextTerrainArea(TerrainType.ROCKS);
		//CreateNextTerrainArea(TerrainType.PIER);
		//CreateNextTerrainArea(TerrainType.ROCKS);
	}

	private float CalculateTerrainSectionYPosBasedOnSectionScale(GameObject terrainSection)
	{
		if(terrainSection.transform.localScale.y > terainStandardYScale)
		{
			terrainSectionYPos = terrainSectionYPos + (terrainSection.transform.localScale.y - terainStandardYScale);
		}

		return terrainSectionYPos;
	}

	public float GetLastTerrainFinishX()
	{
		return lastTerrainFinishX;
	}

	public void CreateNextRandomTerrain()
	{
		terrainCount++;

		if(GlobalSettings.currentLocationNumber == 3)
		{
			CreateNextTerrainArea(TerrainType.ROCKS);
		}
		else
		{
			if(terrainCount % 2 == 0)
			{
				CreateNextTerrainArea(TerrainType.PIER);
			}
			else
			{
				CreateNextTerrainArea(TerrainType.ROCKS);
			}
		}

		/*
		if(lastWasRockTerrain)
		{
			CreateNextTerrainArea(TerrainType.PIER);
			lastWasRockTerrain = false;
		}
		else
		{
			CreateNextTerrainArea(TerrainType.ROCKS);
			lastWasRockTerrain = true;
		}
*/
	}

	public void CreateNextTerrainArea(TerrainType nextTerrainType)
	{
		if(nextTerrainType == TerrainType.ROCKS)
		{
			Debug.Log ("CREATING NEW ROCKS TERRAIN");
			float sectionWidth = rockSections[nextRockTerrainCounter].transform.localScale.x;
			float startX = lastTerrainFinishX + sectionWidth / 2;
			rockSections[nextRockTerrainCounter].transform.position = new Vector3(startX, terrainSectionYPos, rockSections[nextRockTerrainCounter].transform.position.z);
			lastTerrainFinishX = lastTerrainFinishX + sectionWidth;
			GlobalSettings.EnableHiddenItems(rockSections[nextRockTerrainCounter]);
			nextRockTerrainCounter++;
			if(nextRockTerrainCounter == rockSections.Length)
			{
				nextRockTerrainCounter = 0;
			}
		}
		else if(nextTerrainType == TerrainType.PIER)
		{
			//Debug.Log ("CREATING NEW PIER TERRAIN");
			float sectionWidth = pierSections[nextPierTerrainCounter].transform.localScale.x;
			float startX = lastTerrainFinishX + sectionWidth / 2;
			pierSections[nextPierTerrainCounter].transform.position = new Vector3(startX, terrainSectionYPos, pierSections[nextPierTerrainCounter].transform.position.z);
			lastTerrainFinishX = lastTerrainFinishX + sectionWidth;
			GlobalSettings.EnableHiddenItems(pierSections[nextPierTerrainCounter]);
			nextPierTerrainCounter++;
			if(nextPierTerrainCounter == pierSections.Length)
			{
				nextPierTerrainCounter = 0;
			}
		}
	}
}
