using System;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

/*
public class SchedulerSurfActivitySetting
{
	public static int MaxNumberOfActivitiesActive(int currentSurfLocationsUnlocked)
	{
		return UnityEngine.Random.Range(1, currentSurfLocationsUnlocked + 1);
	}
}

public class SchedulerSurfActivityGenrator
{
	GameState gameState = null;
	List<Activity> activities;
	List<Activity> activityHistory;
	Dictionary<Location, int> activityHistoryLocationGoalDistance = null;
	Dictionary<Location, Vehicle> activityHistoryLocationStartVehicle = null;
	Dictionary<Location, DateTime> nextLocationActivityGenerationDateTime = null;


	public SchedulerSurfActivityGenrator(GameState gameState)
	{
		this.gameState = gameState;
		activities = new List<Activity>();
		activityHistory = new List<Activity>();
		nextLocationActivityGenerationDateTime = new Dictionary<Location, DateTime>();
	}


	public void GenerateActivities(List<Activity> currentActivities)
	{
		this.activities = currentActivities;

		Location[] surfLocationsToGenerate = GetSurfLocationsToGenerateActivitiesFor();
		foreach(var location in surfLocationsToGenerate)
		{
			Activity activity = CreateSurfActivityForLocation(location);
			AddToCurrentActivities(activity);
		}

		if(this.activities.Count == 0 && surfLocationsToGenerate.Length == 0)
		{
			Activity shackActivity = CreateShackActivity();
			AddToCurrentActivities(shackActivity);
		}
		else
		{
			RemoveShackActivity();
		}
	}

	class FirstSurfSetting
	{
		public FirstSurfSetting(ActivityType activityType, Vehicle startVehicle, Difficulty difficulty, 
			WaveType waveType, List<ActivityGoal> goals)
		{
			ActivityType = activityType;
			StartVehicle = startVehicle;
			Difficulty = difficulty;
			WaveType = waveType;
			Goals = goals;
		}

		public ActivityType ActivityType { get; private set; }
		public Vehicle StartVehicle { get; private set; }
		public Difficulty Difficulty { get; private set; }
		public WaveType WaveType { get; private set; }
		public List<ActivityGoal> Goals { get; private set; }
		public bool DisableCratesBirdsAndBoosters { get; set; }
	}


	List<Location> surfLocations;
	List<Location> SurfLocations
	{
		get
		{
			if(surfLocations == null)
			{
				surfLocations = new List<Location>
				{
					Location.MainBeach,
					Location.BigWaveReef,
					Location.WavePark,
					Location.Mexico,
					Location.NuclearPlant,
					Location.Volcano,
				};
			}

			return surfLocations;
		}
	}
		
	void AddToCurrentActivities(Activity activity)
	{
		AddToActivityHistory(activity);
		UpdateNextLocationActivityGenerationDateTime(activity.Location);
		activities.Add(activity);
	}

	void AddToActivityHistory(Activity activity)
	{
		// Store for one per location only
		int indexOfExistingWithLocation = activityHistory.FindIndex(x => x.Location == activity.Location);

		if(indexOfExistingWithLocation != -1)
			activityHistory.RemoveAt(indexOfExistingWithLocation);
		
		activityHistory.Add(activity);
	}

	void UpdateNextLocationActivityGenerationDateTime(Location location)
	{
		if(!IsSurfLocation(location))
			return;
		
		int nextGenerationSecs = UnityEngine.Random.Range(20, 180);
		DateTime nextGenerationDateTime = DateTime.UtcNow.AddSeconds(nextGenerationSecs);

		if(nextLocationActivityGenerationDateTime.ContainsKey(location))
		{
			nextLocationActivityGenerationDateTime[location] = nextGenerationDateTime;
		}
		else
		{
			nextLocationActivityGenerationDateTime.Add(location, nextGenerationDateTime);
		}
	}

	DateTime? NextLocationActivityGenerationDateTime(Location location)
	{
		if(nextLocationActivityGenerationDateTime.ContainsKey(location))
			return nextLocationActivityGenerationDateTime[location];

		return null;
	}

	Activity GetActivityHistoryForLocation(Location location)
	{
		return activityHistory.FirstOrDefault(x => x.Location == location);
	}


	Location[] GetSurfLocationsToGenerateActivitiesFor()
	{
		int numberActive = CurrentNumberOfSurfActivitiesActive();
		int numberOfUnlockedSurfLocations = CurrentNumberOfUnlockedSurfLocations();
		int maxAllowableNumberActive = SchedulerSurfActivitySetting.MaxNumberOfActivitiesActive(numberOfUnlockedSurfLocations);

		if(numberActive >= maxAllowableNumberActive)
		{
			return new Location[] {};
		}

		int numberOfLocationsToGenerate = maxAllowableNumberActive - numberActive;
		List<Location> locations = SurfLocations.ToList();
		RemoveLocationsWithExistingActivities(locations);
		//RemoveLocationsWhichAreStillLocked(locations);
		//RemoveLocationsBeforeIntendedGenerationDateTime(locations);

		if(numberOfLocationsToGenerate <= 0)
			return new Location[] {};
		else if(locations.Count > numberOfLocationsToGenerate)
		{
			int locationsToRemove = locations.Count - numberOfLocationsToGenerate;
			for(int i = 0; i < locationsToRemove; i++)
			{
				locations.RemoveAt(UnityEngine.Random.Range(0, locations.Count));
			}
		}

		return locations.ToArray();
	}

	void RemoveLocationsWithExistingActivities(List<Location> locations)
	{
		foreach(var activity in activities)
		{
			locations.RemoveAll(x => x == activity.Location);
		}
	}

	/*
	void RemoveLocationsWhichAreStillLocked(List<Location> locations)
	{
		locations.RemoveAll(x => !GameState.SharedInstance.PlayerLevel.GetLocationUnlock(x));
	}

	void RemoveLocationsBeforeIntendedGenerationDateTime(List<Location> locations)
	{
		
		return; // TOREVIEW
		locations.RemoveAll(x => NextLocationActivityGenerationDateTime(x).HasValue && NextLocationActivityGenerationDateTime(x).Value > DateTime.UtcNow);
	}

	bool IsSurfLocation(Location location)
	{
		return SurfLocations.Contains(location);
	}

	int CurrentNumberOfSurfActivitiesActive()
	{
		int result = 0;
		foreach(var activity in activities)
		{
			if(IsSurfLocation(activity.Location))
			{
				result += 1;
			}
		}

		return result;
	}

	int CurrentNumberOfUnlockedSurfLocations()
	{
		int result = 0;
		foreach(var surfLoc in SurfLocations)
		{
			if(GameState.SharedInstance.PlayerLevel.GetLocationUnlock(surfLoc))
			{
				result += 1;
			}
		}

		return result;
	}
	

	Activity CreateSurfActivityForLocation(Location location)
	{
		Activity activity = new Activity(GenerateSurfActivityType(location), location, GenerateSurfGoals(location));

		if(activity.Type == ActivityType.CompetitionSurf)
			activity.SetPrize(new Prize(PrizeType.Money, PrizeMoneyByPlayerLevel()));
		
		var activityData = new ActivityDataSurf(activity, GenerateSurfWaveType(location, activity.Goals), 
			GenerateSurfDifficulty(location), GenerateSurfStartVehicle(location, activity.Type));
		
		activity.SetActivityData(activityData);

		return activity;
	}

	ActivityType GenerateSurfActivityType(Location location)
	{
		if(location == Location.WavePark)
			return ActivityType.CompetitionSurf;
		
		int randValue0to100 = UnityEngine.Random.Range(0, 101);
		var lastActivityAtLocation = GetActivityHistoryForLocation(location);

		if(lastActivityAtLocation == null)
		{
			if(randValue0to100 < 50)
				return ActivityType.FreeSurf;
			else
				return ActivityType.CompetitionSurf;
		}
		else if(lastActivityAtLocation.Type != ActivityType.FreeSurf && lastActivityAtLocation.Type != ActivityType.CompetitionSurf)
		{
			if(randValue0to100 < 50)
				return ActivityType.FreeSurf;
			else
				return ActivityType.CompetitionSurf;
		}


		int percentChanceToCreateSameActivityAsLast = 25;
		bool lastActivityWasFreeSurf = lastActivityAtLocation.Type == ActivityType.FreeSurf;
		if(lastActivityWasFreeSurf)
		{
			if(randValue0to100 < percentChanceToCreateSameActivityAsLast)
				return ActivityType.FreeSurf;
			else
				return ActivityType.CompetitionSurf;
		}
		else
		{
			if(randValue0to100 < percentChanceToCreateSameActivityAsLast)
				return ActivityType.CompetitionSurf;
			else
				return ActivityType.FreeSurf;
		}	
	}

	List<ActivityGoal> GenerateSurfGoals(Location location)
	{
		List<ActivityGoal> result = new List<ActivityGoal>();

		var allGoalTypes = Enum.GetValues(typeof(ActivityGoalType)).Cast<ActivityGoalType>().ToList();

		// Pier grinds only available on main beach.
		if(location != Location.MainBeach)
			allGoalTypes.RemoveAll(x => x == ActivityGoalType.PierGrinds);

		// Only 
		if(location == Location.WavePark)
		{
			allGoalTypes.RemoveAll(x => x != ActivityGoalType.Slalom);
		}
		else
		{
			allGoalTypes.RemoveAll(x => x == ActivityGoalType.Slalom);
		}
			
		var lastActivityAtLocation = GetActivityHistoryForLocation(location);

		if(lastActivityAtLocation != null)
		{
			foreach(var lastActivityGoal in lastActivityAtLocation.Goals)
			{
				if(allGoalTypes.Count > 1)
				{
					int indexOfExisting = allGoalTypes.FindIndex(x => x == lastActivityGoal.Type);
					if(indexOfExisting != -1)
						allGoalTypes.RemoveAt(indexOfExisting);
				}
			}
		}

		int goalsToGenerate = 1;

		if(allGoalTypes.Count > 1)
		{
			int percentChanceOf2Goals = 15;
			int randValue0to100 = UnityEngine.Random.Range(0, 101);
			if(randValue0to100 < percentChanceOf2Goals)
				goalsToGenerate = 2;
		}
		
		for (int i = 0; i < goalsToGenerate; i++)
		{
			int randGoalTypeIndex = RandomGenerator.RandomNumber(0, allGoalTypes.Count);
			ActivityGoalType goalTypeToGenerate = allGoalTypes[randGoalTypeIndex];

			if(goalTypeToGenerate == ActivityGoalType.ReachDistance)
			{
				var goal = new ActivityGoalReachDistance(CredPointByPlayerLevel(), GoalGetReachDistanceValue(location));
				result.Add(goal);
			}
			else if(goalTypeToGenerate == ActivityGoalType.Barrels)
			{
				var goal = new ActivityGoalBarrels(CredPointByPlayerLevel(), GoalGetBarrelsTotalRequiredValue(location));
				result.Add(goal);
			}
			else if(goalTypeToGenerate == ActivityGoalType.PierGrinds)
			{
				var goal = new ActivityGoalPierGrinds(CredPointByPlayerLevel(), GoalGetPierGrindsTotalRequiredValue(location));
				result.Add(goal);
			}
			else if(goalTypeToGenerate == ActivityGoalType.CollectLetters)
			{
				var goal = new ActivityGoalCollectLetters(CredPointByPlayerLevel());
				result.Add(goal);
			}
			else if(goalTypeToGenerate == ActivityGoalType.Slalom)
			{
				ActivityGoalSlalom goal = new ActivityGoalSlalom(CredPointByPlayerLevel(), GoalGetSlalomTotalRequiredValue(location));
				result.Add(goal);
			}

			allGoalTypes.RemoveAt(randGoalTypeIndex);

			if(allGoalTypes.Count == 0)
				break;
		}

		return result;
	}


	int GoalGetReachDistanceValue(Location location)
	{
		int distance = 25;

		if(activityHistoryLocationGoalDistance == null)
			activityHistoryLocationGoalDistance = new Dictionary<Location, int>();

		int distanceToAdd = 5;
		if(activityHistoryLocationGoalDistance.ContainsKey(location))
		{
			activityHistoryLocationGoalDistance[location] += distanceToAdd;
			distance = activityHistoryLocationGoalDistance[location];
		}
		else
		{
			activityHistoryLocationGoalDistance.Add(location, distance);
		}

		// Randomise after 200
		if(distance > 40)
		{
			distance = RandomGenerator.RandomNumber(2,6)*10;
		}

		return distance;
	}
		
	int GoalGetBarrelsTotalRequiredValue(Location location)
	{
		// Could be more clever with player level
		return RandomGenerator.RandomNumber(1, 4);
	}

	int GoalGetPierGrindsTotalRequiredValue(Location location)
	{
		// Could be more clever with player level
		return RandomGenerator.RandomNumber(2, 4);
	}

	int GoalGetSlalomTotalRequiredValue(Location location)
	{
		return RandomGenerator.RandomNumber(10, 15);
	}
		
	WaveType GenerateSurfWaveType(Location location, IList<ActivityGoal> goals)
	{
		if(location == Location.WavePark)
			return WaveType.Average;
		
		// Return average wave if has pier grind
		foreach(var goal in goals)
		{
			if(goal.Type == ActivityGoalType.PierGrinds)
				return WaveType.Average;
		}

		// Implement Huge randomness once functionality implemented
		// Could be more clever with player level
		int randValue0to100 = UnityEngine.Random.Range(0, 101);
		if(location == Location.BigWaveReef || location == Location.Volcano)
		{
			if(randValue0to100 < 50)
				return WaveType.Big;
			else
				return WaveType.Average;
		}
		else
		{
			if(randValue0to100 < 15)
				return WaveType.Big;
			else
				return WaveType.Average;
		}
	}

	Difficulty GenerateSurfDifficulty(Location location)
	{
		// Implement Extreme randomness once functionality implemented
		// Could be more clever with player level

		int randValue0to100 = UnityEngine.Random.Range(0, 101);
		if(randValue0to100 < 15)
			return Difficulty.HARD;
		if(randValue0to100 < 70)
			return Difficulty.EASY;

		return Difficulty.MEDIUM;
	}
		
	Vehicle GenerateSurfStartVehicle(Location location, ActivityType activityType)
	{
		List<Vehicle> vehiclesToStart = gameState.OwnedVehicleTypes();
		if(vehiclesToStart.Count == 1)
			return vehiclesToStart[0];

		if(activityHistoryLocationStartVehicle == null)
			activityHistoryLocationStartVehicle = new Dictionary<Location, Vehicle>();
			
		if(activityHistoryLocationStartVehicle.ContainsKey(location))
			vehiclesToStart.RemoveAll(x => x == activityHistoryLocationStartVehicle[location]);
		else
			activityHistoryLocationStartVehicle.Add(location, Vehicle.None);

		int randVehicleIndex = UnityEngine.Random.Range(0, vehiclesToStart.Count);
		Vehicle result = vehiclesToStart[randVehicleIndex];
		activityHistoryLocationStartVehicle[location] = result;

		return result;
	}

	int CredPointByPlayerLevel()
	{
		int result = 0;
		if(gameState.PlayerLevel.CurrentLevel <= 2)
		{
			result = (int)(gameState.PlayerLevel.NextLevelCred/4);
		}
		else
		{
			int minCredSteps = gameState.PlayerLevel.CurrentLevel * 4;
			int maxCredSteps = gameState.PlayerLevel.CurrentLevel * 5;
			int minCredPerGoal = (gameState.PlayerLevel.NextLevelCred/maxCredSteps);
			int maxCredPerGoal = (gameState.PlayerLevel.NextLevelCred/minCredSteps);

			result = UnityEngine.Random.Range(minCredPerGoal, maxCredPerGoal+1);
		}
			
		int roundUpOfLastDigit = result % 10;
		roundUpOfLastDigit = (roundUpOfLastDigit <= 5) ? 0 : 5;
		result = ((int)(result/10))*10 + roundUpOfLastDigit;

		return result;
	}

	int PrizeMoneyByPlayerLevel()
	{
		// Need to update this to be level dependent.
		return UnityEngine.Random.Range(1, 11) * 10;
	}

	Activity CreateShackActivity()
	{
		var shackActivity = new Activity(ActivityType.Shack, Location.Shack);
		return shackActivity;
	}

	void RemoveShackActivity()
	{
		var shackActivity = activities.FirstOrDefault(x => x.Type == ActivityType.Shack);

		if(shackActivity != null)
			shackActivity.MarkAsCompleted();
	}
}
*/