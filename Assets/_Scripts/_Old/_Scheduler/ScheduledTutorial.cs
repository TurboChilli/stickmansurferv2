using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScheduledTutorial
{
	public ScheduledTutorial(TutorialInfo info, int playerLevelRequired)
	{
		Info = info;
		PlayerLevelRequired = playerLevelRequired;
	}

	public TutorialInfo Info { get; private set; }
	public int? PlayerLevelRequired { get; private set; }

	public bool HasBeenCompleted()
	{
		string key = string.Format("ST_istc{0}", (int)Info);
		return PlayerPrefs.HasKey(key);
	}

	public void MarkAsCompleted()
	{
		string key = string.Format("ST_istc{0}", (int)Info);
		PlayerPrefs.SetInt(key, 1);
	}
}
