﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scheduler
{
	/*
	// Privates
	private GameState gameState;
	private List<Activity> activities;
	private List<ScheduledTutorial> scheduledTutorials;
	private SchedulerSurfActivityGenrator schedulerSurfActivityGenerator;
	private const int minutesBetweenActivitiesRefresh = 7;
	private bool ignoreTutorials = false;

	// Shared Instance
	static Scheduler instance;
	public static Scheduler SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new Scheduler(GameState.SharedInstance);

			return instance;
		}
	}

	// Construction
	protected Scheduler(GameState gameState)
	{
		this.gameState = gameState;
		activities = new List<Activity>();
		schedulerSurfActivityGenerator = new SchedulerSurfActivityGenrator(gameState);

		SetupScheduledTutorials();
	}

	// Public

	// MOCK OBJECTS
	// Replace with proper activity generator/level data loading logic.
	// Current has mock data and basic state management.

	public List<Activity> GetAllLocationActivities()
	{
		List<Activity> returnActivity = new List<Activity>();

		int debug_level = PlayerPrefs.GetInt("DEBUG_level", 0);

		if (debug_level == 0)
		{
			List<ActivityGoal> activityGoals = new List<ActivityGoal>();
			int activityGoalCredPoints = UnityEngine.Random.Range(30, 80);
			int activityGoalDistance = 30;
			ActivityGoalReachDistance activityGoal = new ActivityGoalReachDistance(activityGoalCredPoints, activityGoalDistance);
			activityGoals.Add(activityGoal);

			Activity mainBeach = new Activity(ActivityType.FreeSurf, Location.MainBeach, activityGoals);
			ActivityDataSurf mainBeachData = new ActivityDataSurf(mainBeach, WaveType.Average, Difficulty.MEDIUM);
			mainBeach.SetActivityData(mainBeachData);
			mainBeach.PreAmble = "Big 30 sprint";
			returnActivity.Add(mainBeach);
			debug_level++;
		}
		else if (debug_level == 1)
		{
			List<ActivityGoal> activityGoals = new List<ActivityGoal>();
			int activityGoalCredPoints = UnityEngine.Random.Range(30, 80);
			int activityGoalBarrels = 3;
			ActivityGoalBarrels activityGoal = new ActivityGoalBarrels(activityGoalCredPoints, activityGoalBarrels);
			activityGoals.Add(activityGoal);

			Activity mainBeach = new Activity(ActivityType.FreeSurf, Location.MainBeach, activityGoals);
			ActivityDataSurf mainBeachData = new ActivityDataSurf(mainBeach, WaveType.Average, Difficulty.MEDIUM);
			mainBeach.SetActivityData(mainBeachData);
			mainBeach.PreAmble = "Barrel-o-saurus";
			returnActivity.Add(mainBeach);
			debug_level++;
		}
		else if (debug_level == 2)
		{
			List<ActivityGoal> activityGoals = new List<ActivityGoal>();
			int activityGoalCredPoints = UnityEngine.Random.Range(30, 80);
			int activityGoalBarrels = 3;
			ActivityGoalBarrels activityGoal = new ActivityGoalBarrels(activityGoalCredPoints, activityGoalBarrels, Vehicle.Jetski);
			activityGoals.Add(activityGoal);

			Activity mexico = new Activity(ActivityType.CompetitionSurf, Location.Mexico, activityGoals);
			ActivityDataSurf mexicoData = new ActivityDataSurf(mexico, WaveType.Average, Difficulty.MEDIUM);//, Vehicle.Jetski);
			mexico.SetActivityData(mexicoData);
			mexico.PreAmble = "Jetski Banananas";
			returnActivity.Add(mexico);
			debug_level++;
		}
		else if (debug_level == 3)
		{
			List<ActivityGoal> activityGoals = new List<ActivityGoal>();
			int activityGoalCredPoints = UnityEngine.Random.Range(30, 80);
			ActivityGoalCollectLetters activityGoal = new ActivityGoalCollectLetters(activityGoalCredPoints);
			activityGoals.Add(activityGoal);

			Activity mexico = new Activity(ActivityType.FreeSurf, Location.Mexico, activityGoals);
			ActivityDataSurf mexicoData = new ActivityDataSurf(mexico, WaveType.Average, Difficulty.MEDIUM);//, Vehicle.Jetski);
			mexico.SetActivityData(mexicoData);
			mexico.PreAmble = "Learn to spell";
			returnActivity.Add(mexico);
			debug_level = 0;
		}

		PlayerPrefs.SetInt("DEBUG_level", debug_level);
		PlayerPrefs.Save();

		//first level
		//first level


		return returnActivity;
	}

	public void SetIgnoreTutorials(bool ignore)
	{
		this.ignoreTutorials = ignore;
	}

	public Activity[] GetActivities()
	{
		CheckAndRemoveCompletedActivities();

		if(!CanGenerateActivity())
			return activities.ToArray();
			
		var scheduledTutorial = GetScheduledTutorial();

		if(scheduledTutorial == null)
		{
			
			schedulerSurfActivityGenerator.GenerateActivities(activities);

			//GenerateHalfPipeActivity();
			GenerateShellGameActivity();
		}
		else
		{
			if(scheduledTutorial.Info == TutorialInfo.FreeSurfDistanceHowToPlay)
			{
				var activityGoals = new List<ActivityGoal>();
				var activityGoalCredPoints = 25;

				#if UNITY_EDITOR
				var activityGoalDistance = 10;
				#else
				var activityGoalDistance = 20;
				#endif 

				var activityGoal = new ActivityGoalReachDistance(activityGoalCredPoints, activityGoalDistance, Vehicle.Surfboard);
				activityGoals.Add(activityGoal);

				var activity = new Activity(ActivityType.FreeSurf, Location.MainBeach, activityGoals);


				var activityData = new ActivityDataSurf(activity, WaveType.Average, Difficulty.EASY, Vehicle.Surfboard);
				activityData.DisableBirds = true;
				activityData.DisableCrates = true;
				activityData.DisableBoostArrow = true;
				activity.SetActivityData(activityData);

				activity.SetTutorialInfo(scheduledTutorial);
				activity.SetPrize(Prize.CreatePrizeMoney(100));
				activity.SetAsSurfTutorial();
				activities.Add(activity);
			}
			else if(scheduledTutorial.Info == TutorialInfo.TutorialOverlayHowToPlayCongrats)
			{
				var activity = new Activity(ActivityType.Tutorial, Location.Shack);
				activity.SetTutorialInfo(scheduledTutorial);
				activities.Add(activity);
			}
			else if(scheduledTutorial.Info == TutorialInfo.FreeSurfMexicoBarrels)
			{
				var activityGoals = new List<ActivityGoal>();
				var activityGoalCredPoints = 50;

				#if UNITY_EDITOR
				var activityGoalBarrels = 1;
				#else
				var activityGoalBarrels = 2;
				#endif 

				var activityGoal = new ActivityGoalBarrels(activityGoalCredPoints, activityGoalBarrels, Vehicle.Surfboard);
				activityGoals.Add(activityGoal);

				var activity = new Activity(ActivityType.FreeSurf, Location.Mexico, activityGoals);
				var activityData = new ActivityDataSurf(activity, WaveType.Big, Difficulty.EASY, Vehicle.Surfboard);
				activity.SetActivityData(activityData);

				activity.SetTutorialInfo(scheduledTutorial);
				activities.Add(activity);
			}
			else if(scheduledTutorial.Info == TutorialInfo.TutorialOverlayMexicoMagazineCoverReveal)
			{
				var activity = new Activity(ActivityType.Tutorial, Location.Shack);
				activity.SetTutorialInfo(scheduledTutorial);
				activities.Add(activity);
			}
			else if(scheduledTutorial.Info == TutorialInfo.TutorialOverlayFirstEverCompetitionMainBeach)
			{
				var activity = new Activity(ActivityType.Tutorial, Location.Shack);
				activity.SetTutorialInfo(scheduledTutorial);
				activities.Add(activity);
			}
			else if(scheduledTutorial.Info == TutorialInfo.FirstEverCompetitionMainBeach)
			{
				var activityGoals = new List<ActivityGoal>();
				var activityCredPoints = 75;
				var activityGoal = new ActivityGoalCollectLetters(activityCredPoints, Vehicle.Surfboard);
				activityGoals.Add(activityGoal);

				var activity = new Activity(ActivityType.CompetitionSurf, Location.MainBeach, activityGoals);
				activity.SetPrize(Prize.CreatePrizeMoney(500));

				var activityData = new ActivityDataSurf(activity, WaveType.Average, Difficulty.EASY, Vehicle.Surfboard);
				activity.SetActivityData(activityData);

				activity.SetTutorialInfo(scheduledTutorial);
				activities.Add(activity);
			}
		}

		return activities.ToArray();
	}

	bool CanGenerateActivity()
	{
		if(activities.Count == 0)
		{
			return true;
		}

		return false;
		/* First run causing multiple activities to stack up.
		else if(!PlayerPrefs.HasKey("Scheduler_lastRefreshTime"))
		{
			PlayerPrefs.SetString("Scheduler_lastRefreshTime", System.DateTime.UtcNow.ToString("s"));
			return true;
		}
		else
		{
			string dateString = PlayerPrefs.GetString("Scheduler_lastRefreshTime");
			DateTime lastRefreshedDate = DateTime.ParseExact(dateString, "s", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
			int mins = (int)((DateTime.UtcNow - lastRefreshedDate).TotalMinutes);

			if(mins < minutesBetweenActivitiesRefresh)
				return false;
			else
			{
				PlayerPrefs.SetString("Scheduler_lastRefreshTime", System.DateTime.UtcNow.ToString("s"));
				return true;
			}
		}
		//
	}

	void ForceGenerateActivities()
	{
		GetActivities();
	}

	bool checkAndRemovingCompletedActivities = false;
	void CheckAndRemoveCompletedActivities()
	{
		if(checkAndRemovingCompletedActivities) // thread safety required?
			return;

		checkAndRemovingCompletedActivities = true;
		foreach(var activity in activities)
		{
			if(!activity.Completed)
			{
				if(activity.ActivityIsClosed() || activity.ActivityTriesExceeded())
					activity.MarkAsCompleted();
			}
		}

		if(activities.Count > 0)
			activities.RemoveAll(x => x.Completed == true);

		checkAndRemovingCompletedActivities = false;
	}

	void GenerateHalfPipeActivity()
	{
		string nextGenTimeString = PlayerPrefs.GetString("Sched_nextHalfPipeGenTime", DateTime.UtcNow.AddDays(-1).ToString("s"));
		DateTime nextGenTime = DateTime.ParseExact(nextGenTimeString, "s", System.Globalization.CultureInfo.InvariantCulture, 
			System.Globalization.DateTimeStyles.None);
			
		if(DateTime.UtcNow > nextGenTime)
		{
			GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.HalfPipe);

			int indexHalfPipe = activities.FindIndex(x => x.Type == ActivityType.HalfPipe);
			if(indexHalfPipe == -1 && (UnityEngine.Random.Range(1,3) % 2) == 0)
			{
				var activityHalfPipe = new Activity(ActivityType.HalfPipe, Location.HalfPipe);
				this.activities.Add(activityHalfPipe);
			}

			int nextGenMins = UnityEngine.Random.Range(2, 6);
			PlayerPrefs.SetString("Sched_nextHalfPipeGenTime", DateTime.UtcNow.AddMinutes(nextGenMins).ToString("s"));
		}
	}

    void GenerateShellGameActivity()
    {
        GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.ShellGame);

        int indexFound = activities.FindIndex(x => x.Type == ActivityType.Shack);
        if(indexFound == -1)
        {
            var activityShellGame = new Activity(ActivityType.ShellGame, Location.ShellGame);
            this.activities.Add(activityShellGame);
        }
    }

	void SetupScheduledTutorials()
	{
		scheduledTutorials = new List<ScheduledTutorial>
		{
			new ScheduledTutorial(TutorialInfo.FreeSurfDistanceHowToPlay, 1),
			new ScheduledTutorial(TutorialInfo.TutorialOverlayHowToPlayCongrats, 1),
			new ScheduledTutorial(TutorialInfo.FreeSurfMexicoBarrels, 1),
			//new ScheduledTutorial(TutorialInfo.TutorialOverlayMexicoMagazineCoverReveal, 1),
			//new ScheduledTutorial(TutorialInfo.TutorialOverlayFirstEverCompetitionMainBeach, 1),
			//new ScheduledTutorial(TutorialInfo.FirstEverCompetitionMainBeach, 1),
		};
	}

	ScheduledTutorial GetScheduledTutorial()
	{
		int playerLevel = gameState.PlayerLevel.CurrentLevel;

		if(ignoreTutorials)
		{			
			return null;
		}

		// todo: add current completed tutorial counter. Saves looping through to check all the time.
		// useful to determine if all tutorials has been completed also
		foreach(ScheduledTutorial scheduledTutorial in scheduledTutorials)
		{
			if(scheduledTutorial.PlayerLevelRequired <= playerLevel && !scheduledTutorial.HasBeenCompleted())
			{
				return scheduledTutorial;
			}
		}

		return null;
	}
	*/
}


