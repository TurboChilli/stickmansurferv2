using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TutorialInfo
{
	None = 0,
	FreeSurfDistanceHowToPlay = 1,
	TutorialOverlayHowToPlayCongrats = 2,
	FreeSurfMexicoBarrels = 3,
	TutorialOverlayMexicoMagazineCoverReveal = 4,
	TutorialOverlayFirstEverCompetitionMainBeach = 5,
	FirstEverCompetitionMainBeach = 6,
	Count
}