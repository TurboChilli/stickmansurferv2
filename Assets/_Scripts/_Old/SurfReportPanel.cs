﻿using UnityEngine;
using System.Collections;

public class SurfReportPanel : MonoBehaviour 
{
	public TextMesh panelTitle;
	public TextMesh panalSubtitle;
	public TextMesh panelTimeRemaining;
	public TextMesh panelExtraInfo;

	public MeshRenderer backgroundMeshRenderer;

	public AnimatingObject panelIcon;
	public Button panelButton;

	public void SetMaterial(Material material)
	{
		backgroundMeshRenderer.material = material;
	}
}
