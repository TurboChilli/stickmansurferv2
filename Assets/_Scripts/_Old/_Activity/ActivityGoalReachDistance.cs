using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityGoalReachDistance : ActivityGoal
{
	public ActivityGoalReachDistance(int credPoints, int distance, Vehicle vehicle = Vehicle.None)
	{
		Type = ActivityGoalType.ReachDistance;
		CredPoints = credPoints;
		Vehicle = vehicle;
		Distance = distance;
	}

	public override string GoalDescription ()
	{
		if(Distance > 1)
			return string.Format("Ride for {0} meters", Distance);
		else
			return string.Format("Ride for a meter", Distance);
	}

	public int Distance { get; private set; }
}
