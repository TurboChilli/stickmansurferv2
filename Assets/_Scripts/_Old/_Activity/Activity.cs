﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activity
{
	IActivityData activityData;
	const int defaultTriesForCompSurf = 3;

	string activityClosesInMinutesPrefKey
	{
		get { return string.Format("actcct{0}", this.Id); }
	}

	public ActivityType Type { get; private set; }
	public Location Location { get; private set; }
	public Prize Prize { get; private set; }
	public bool Completed { get; private set; } 
	public ScheduledTutorial Tutorial { get; private set; }
	public bool IsTutorial { get; private set; }
	public bool IsSurfTutorial { get; private set; }
	public IList<ActivityGoal> Goals { get; private set; }
	public DateTime ClosedDateTimeUTC { get; private set; }

	// Construction

	public Activity(ActivityType activityType, Location location, ActivityGoal goal)
	{
		Type = activityType;
		Location = location;

		if (goal != null)
		{
			Goals = new List<ActivityGoal>();
			Goals.Add(goal);
		}
		else
			Goals = null;

		GenerateActivityClosingMinutes();
	}

	public Activity(ActivityType activityType, Location location, IList<ActivityGoal> goals = null)
	{
		Type = activityType;
		Location = location;
		Goals = goals != null ? goals : new List<ActivityGoal>();

		GenerateActivityClosingMinutes();
	}

	// Public
	private string id;
	public string Id 
	{ 
		get 
		{ 
			if(string.IsNullOrEmpty(id))
				id = System.Guid.NewGuid().ToString("N");

			return id;
		}
	}

	public int? MaxNumberOfTries { get; set; }

	public enum CompletionResult
	{
		Incomplete = 0,
		Succeeded = 1,
		Failed = 2,
	}

	string preAmble;
	public string PreAmble 
	{ 
		get
		{
			if(string.IsNullOrEmpty(preAmble))
				return GetDefaultPreAmble();

			return preAmble;
		}
		set
		{
			preAmble = value;
		}
	}

	public string ActivityCloseTimeFormattedString
	{
		get
		{
			if(!ActivityCloseTimespan.HasValue || ActivityCloseTimespan.Value.TotalSeconds <= 0)
				return "";

			return string.Format("{0}:{1:00}", (int)ActivityCloseTimespan.Value.TotalMinutes, (int)ActivityCloseTimespan.Value.Seconds);
		}
	}

	public T GetActivityData<T>()
	{
		return (T)activityData;
	}

	// Currently completion of activity is based on whether goal has completed successfully.
	// Future to extend activity completion due to offer time out (eg, expired after 24 hrs).
	public CompletionResult GetCompletionResult()
	{
		if(GoalsCompleted())
		{
			MarkAsCompleted();
			return CompletionResult.Succeeded;
		}
		else if(ActivityIsClosed() || ActivityTriesExceeded())
		{
			MarkAsCompleted();
			return CompletionResult.Failed;
		}

		return CompletionResult.Incomplete;
	}

    public bool HasGoal(ActivityGoalType goalType)
    {
        foreach(ActivityGoal goal in Goals)
        {
            if (goal.Type == goalType)
            {
                return true;
            }
        }
        return false;
    }

	public void SetPrize(Prize prize)
	{
		Prize = prize;
	}

	public void SetTutorialInfo(ScheduledTutorial scheduledTutorial)
	{
		IsTutorial = true;
		Tutorial = scheduledTutorial;
	}

	public void SetAsSurfTutorial()
	{
		IsSurfTutorial = true;
	}

	public void MarkAsCompleted()
	{
		if(Tutorial != null && Tutorial.Info != TutorialInfo.None)
			Tutorial.MarkAsCompleted();

		Completed = true;

		ClosedDateTimeUTC = DateTime.UtcNow;
	}

	public void SetActivityData(IActivityData activityData)
	{
		this.activityData = activityData;
	}

	public bool ActivityIsClosed()
	{
		if(ActivityCloseTimespan.HasValue &&  ActivityCloseTimespan.Value.TotalSeconds <= 0)
			return true;

		return false;
	}
		
	public bool ActivityTriesExceeded()
	{
		if(MaxNumberOfTries.HasValue && MaxNumberOfTries.Value <= 0)
			return true;

		return false;
	}
		
	public void DecrementTries()
	{
		if(MaxNumberOfTries.HasValue)
			MaxNumberOfTries--;
	}

	public void ResetTries()
	{
		if (Type == ActivityType.CompetitionSurf && !IsTutorial)
			MaxNumberOfTries = defaultTriesForCompSurf;
	}

	// Privates

	bool GoalsCompleted()
	{
		bool goalsCompleted = true;

		foreach(ActivityGoal goal in Goals)
		{

            if(!goal.IsCompleted)
				goalsCompleted = false;
		}

		return goalsCompleted;
	}

	string GetDefaultPreAmble()
	{

		return "";
        /*
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		if(Type == ActivityType.CompetitionSurf)
		{
			sb.Append("Competition: ");
		}
		else if(Type == ActivityType.CompetitionSurf)
		{
			sb.Append("Surf: ");
		}

		if(Goals.Count > 0)
		{
			foreach(ActivityGoal goal in Goals)
			{
				sb.AppendFormat("{0}, ", goal.GoalDescription());
			}
		}

		return sb.ToString().Trim().TrimEnd(new char[] { ',' });
		  */
		  /*
        if (this.Location == Location.MainBeach)
        {
            return "Mellow beach break with piers";

        }
        else if (this.Location == Location.BigWaveReef)
        {
            return "Gnarly reef break!";

        }
        else if (this.Location == Location.Shack)
        {
            return "Your home shack bro";

        }
        else if (this.Location == Location.Shop)
        {
            return "For all things surf related";

        }
        else if (this.Location == Location.Mexico)
        {
            return "Burly beach break";

        }
        else
        {
            return "Location desc here";
        }
    */
	}

	TimeSpan? ActivityCloseTimespan
	{
		get 
		{
			// Activities other than surf / competition surf will not have expiry
			if(!CanHaveExpiryTime())
				return null;

			if(!PlayerPrefs.HasKey(activityClosesInMinutesPrefKey))
			{
				GenerateActivityClosingMinutes();
			}

			string dateString = PlayerPrefs.GetString(activityClosesInMinutesPrefKey);
			DateTime lastRefreshedDate = DateTime.ParseExact(dateString, "s", System.Globalization.CultureInfo.InvariantCulture, 
				System.Globalization.DateTimeStyles.None);

			return (lastRefreshedDate - DateTime.UtcNow);
		}
	}

	bool CanHaveExpiryTime()
	{
		if(IsTutorial) 
			return false;

        if(Type == ActivityType.CompetitionSurf || Type == ActivityType.FreeSurf || Type == ActivityType.HalfPipe || Type == ActivityType.ShellGame)
			return true;

		return false;
	}

	void GenerateActivityClosingMinutes()
	{
		float mins = RandomGenerator.RandomNumberNotSameAsPrevious(1,8);
		if((RandomGenerator.RandomNumber(1,3)%2) == 0)
			mins += 0.5f;
		
		PlayerPrefs.SetString(activityClosesInMinutesPrefKey, DateTime.UtcNow.AddMinutes(mins).ToString("s"));
	}
}