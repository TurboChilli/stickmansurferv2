using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActivityGoalType
{
	ReachDistance = 1,
	Barrels = 2,
    PierGrinds = 3,
    CollectLetters = 4,
    Slalom = 5,
}

public struct ActivityGoalTypeString
{
	public const string ReachDistanceDescription = "Surf {0} meters";
	public const string BarrelsDescription = "Get {0} barrels";
	public const string PierGrindsDescription = "Grind {0} piers";
	public const string CollectLettersDescription = "Collect SURF";
	public const string SlalomDescription = "Pass {0} slalom gates";
}
