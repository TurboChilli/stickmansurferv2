using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prize
{
	protected Prize()
	{
	}

	public static Prize CreatePrizeMoney(int amount)
	{
		return new Prize()
		{
			Type = PrizeType.Money,
			Amount = amount
		};
	}

	public static Prize CreatePrizeVehicle(Vehicle vehicle)
	{
		return new Prize()
		{
			Type = PrizeType.Vehicle,
			Vehicle = vehicle
		};
	}

	public Prize(PrizeType type, int amount = 0)
	{
		Type = type;
		Amount = amount;
	}

	public PrizeType Type { get; protected set; }
	public int? Amount { get; protected set; }
	public Vehicle Vehicle { get; protected set; }
}