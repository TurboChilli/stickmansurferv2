using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityGoalCollectLetters : ActivityGoal
{
	public ActivityGoalCollectLetters(int credPoints, Vehicle vehicle = Vehicle.Surfboard)
	{
		Type = ActivityGoalType.CollectLetters;
		CredPoints = credPoints;
        TotalRequired = 4;
		Vehicle = vehicle;
	}

	public override string GoalDescription()
	{
		return "Collect letters 'SURF'";
	}

    public int TotalRequired { get; private set; }
}

