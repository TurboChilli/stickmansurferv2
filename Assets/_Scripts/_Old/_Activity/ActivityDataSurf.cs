using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityDataSurf : IActivityData
{
	// Construction
    public ActivityDataSurf(Activity activity, WaveType waveType, Difficulty difficulty, Vehicle vehicleAtStart = Vehicle.Surfboard)
	{
		WaveType = waveType;
		Difficulty = difficulty;
        VehicleAtStart = vehicleAtStart;
		this.activity = activity;

        WipeoutOnCrestCollision = true;
		DisableCrates = activity.Type == ActivityType.CompetitionSurf ? true : false;
        DisableBirds = false;

		float additionalSpeedFactor = 1;
		if(difficulty == Difficulty.MEDIUM)
			additionalSpeedFactor += 0.1f;
		else if(difficulty == Difficulty.HARD)
			additionalSpeedFactor += 0.2f;

		if(waveType == WaveType.Big)
			additionalSpeedFactor += 0.15f;
		
		//additionalSpeedFactor += (GameState.SharedInstance.PlayerLevel.CurrentLevel * 0.15f);

		foreach (ActivityGoal goal in activity.Goals)
		{
			if (goal.Type == ActivityGoalType.Slalom)
			{
				additionalSpeedFactor -= 0.25f;
				break;
			}
		}

		if(additionalSpeedFactor > 2f)
			additionalSpeedFactor = 2f;

		additionalSpeedFactor = 1; // don't screw with speed for now
		WaveMaxXVelocityAtStart = 10f * additionalSpeedFactor;
	}
	
	// Public

	public WaveType WaveType { get; private set; }
	public Difficulty Difficulty { get; private set; }
	//public bool HasSurfTimeRestriction { get; private set; }
	//public int SurfTimeRestrictionInSeconds { get; private set; }
    public float WaveMaxXVelocityAtStart  { get; set; }
    public bool WipeoutOnCrestCollision  { get;  set; }
    public bool DisableCrates  { get;  set; }
    public bool DisableBirds  { get;  set; }
    
	public bool DisablePiers
	{
		get 
		{
			if(activity.Location == Location.MainBeach && WaveType == WaveType.Average)
				return false;

			return true;
		}
	}

    public bool DisableGrinds { get;  set; }
    public bool DisableSharks { get;  set; }

	public bool disableBoostArrow = false;
	public bool DisableBoostArrow  
	{ 
		get
		{
			return disableBoostArrow;
		}
		set 
		{
			disableBoostArrow = value;
		}
	}

    public Vehicle VehicleAtStart { get; set; }

    /*
	public void SetSurfTimeRestrictionInSeconds(int seconds)
	{
		HasSurfTimeRestriction = true;
		SurfTimeRestrictionInSeconds = seconds;
	}
	*/
	// Privates
	Activity activity = null;

}