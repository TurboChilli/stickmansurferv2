﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	public class ActivityGoalSlalom : ActivityGoal
{
	public ActivityGoalSlalom(int credPoints, int totalRequired, int accpetableMisses = 3, Vehicle vehicle = Vehicle.None)
	{
		Type = ActivityGoalType.Slalom;
		CredPoints = credPoints;
		Vehicle = vehicle;
		TotalRequired = totalRequired;
		AccpetableMisses = accpetableMisses;
	}

	public override string GoalDescription()
	{
		if(TotalRequired > 1)
			return string.Format("Pass {0} Slalom Gates", TotalRequired);
		else
			return string.Format("Pass a Slalom Gate", TotalRequired);
	}

	public int TotalRequired { get; private set; }
	public int AccpetableMisses { get; private set; }
}