using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityGoalBarrels : ActivityGoal
{
	public ActivityGoalBarrels(int credPoints, int totalRequired, Vehicle vehicle = Vehicle.None)
	{
		Type = ActivityGoalType.Barrels;
		CredPoints = credPoints;
		Vehicle = vehicle;
		TotalRequired = totalRequired;
	}

	public override string GoalDescription()
	{
		if(TotalRequired > 1)
			return string.Format("Complete {0} barrels", TotalRequired);
		else
			return string.Format("Complete a barrel", TotalRequired);
	}

	public int TotalRequired { get; private set; }
}