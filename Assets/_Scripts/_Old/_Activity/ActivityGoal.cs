using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivityGoal
{
	// Construction
	protected ActivityGoal()
	{
	}

	// Public
	private string id;
	public string Id 
	{ 
		get 
		{ 
			if(string.IsNullOrEmpty(id))
				id = System.Guid.NewGuid().ToString("N");

			return id;
		}
	}
		
	public ActivityGoalType Type { get; protected set; }
	public Vehicle Vehicle { get; protected set; }
	public int CredPoints { get; protected set; }
	public bool IsCompleted { get; private set; }

	public void MarkAsCompleted()
	{
        IsCompleted = true;
	}

	public void MarkAsIncomplete()
	{
        IsCompleted = false;
	}

	public abstract string GoalDescription();
}



