using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActivityType
{
	Undefined = 0,
	FreeSurf = 1,
	CompetitionSurf = 2,
	Shack = 3,
	HalfPipe = 4,
	Tutorial = 5,
    ShellGame = 6,
}
