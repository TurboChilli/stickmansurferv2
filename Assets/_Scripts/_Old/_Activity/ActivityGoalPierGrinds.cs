using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityGoalPierGrinds : ActivityGoal
{
	public ActivityGoalPierGrinds(int credPoints, int totalRequired, Vehicle vehicle = Vehicle.None)
	{
		Type = ActivityGoalType.PierGrinds;
		CredPoints = credPoints;
		Vehicle = vehicle;
		TotalRequired = totalRequired;
	}

	public override string GoalDescription()
	{
		if(TotalRequired > 1)
			return string.Format("Do {0} pier grinds", TotalRequired);
		else
			return string.Format("Do a pier grind", TotalRequired);
	}

	public int TotalRequired { get; private set; }
}