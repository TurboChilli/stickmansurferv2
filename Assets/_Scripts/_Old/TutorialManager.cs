﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class TutorialManager : MonoBehaviour 
{
	/*

	//private MapManager mapManager = null;
	public Camera uiCam = null;

	private bool isInitialised = false;

	private Activity curActivity = null;

	private Vector3 inactivePosition;
	private Vector3 activePosition = Vector3.zero;

	public TextMesh charNameLeftText = null;
	public TextMesh charNameRightText = null;
	public TextMesh dialogText = null;

	public Material dialogTextBubbleMaterial = null;

	public GameObject arrowObject;
	public GameObject fingerPointLHSObject;
	public GameObject fingerPointRHSObject;

	private GameObject showingImage = null;

	public GameObject darkScreenOverlay;
	public GameObject playerCharacter;
	public GameObject oldMateCharacter;
	private Vector3 characterPositionLeft;
	private Vector3 characterPositionRight;

	private bool isDialogInitialised = false;
	private Dictionary<TutorialInfo, Dialog[]> dialogPool = null;

	private Dialog[] curDialog = null;
	private int curDialogIndex = 0;

	public bool showingTut = false;
	private TUTSTATE curState = TUTSTATE.None;
	//private CHARACTER curChar = CHARACTER.None;
	private bool isLoadingSceneOnDialogEnd = false;
	private string sceneOnDialogEnd = "";

	private GameObject charBust = null;

	private float timeToMove = 1.0f;
	private float curTimeToMove = 0.0f;

	private float lettersPerSecond = 100.0f;
	private float lettersTimer = 0.0f;
	private int letterCount = 0;

	private GameObject leftCharacter;
	private GameObject rightCharacter;

	private bool showingCustomDialog = false;

	private bool isCustomDialogPoolInitialised = false;
	private Dictionary<CustomDialogID, Dialog[]> customDialogPool = null;

	void Start () 
	{
		if (!isInitialised)
			Initialise();
	}

	void Initialise()
	{
		//if (mapManager == null)
		//	mapManager = FindObjectOfType<MapManager>();

		//if (playerControl == null)
		//	playerControl = FindObjectOfType<PlayerControl>();

		characterPositionLeft = playerCharacter.transform.localPosition;
		characterPositionRight = oldMateCharacter.transform.localPosition;
		inactivePosition = transform.localPosition;

		if (!isDialogInitialised)
			InitialiseDialogPool();

		if(!isCustomDialogPoolInitialised)
			InitialiseCustomDialogPool();

		//disable all the thing
		//darkScreenOverlay.SetActive(false);
		playerCharacter.SetActive(false);
		oldMateCharacter.SetActive(false);

		isInitialised = true;

	}

	public void ShowTutorial(Activity activity)
	{
		if (!isInitialised)
			Initialise();

		curActivity = activity;

		TutorialInfo info = activity.Tutorial.Info;
		if (!dialogPool.ContainsKey(info))
		{
			//////Debug.Log("No matching tutorial dialog for " + info);
			return;
		}
		curDialog = dialogPool[info];
		curDialogIndex = 0;
		showingTut = true;

		ShowDialog(curDialogIndex);
	}

	public void ShowDialog(int dialogIndex)
	{
		NotifyCallback(CallbackType.ShowDialog, 
			new CallbackData
			{
				DialogInfo = curDialog[curDialogIndex].info
			});

		//reset assets
		dialogText.text = "";
		charNameLeftText.text = "";

		lettersTimer = 0.0f;
		letterCount = 0;

		string charName = "";
		switch (curDialog[curDialogIndex].character)
		{
			case CHARACTER.Player:
				charBust = playerCharacter;
				charName = "Stick";
			break;

			case CHARACTER.OldMate:
				charBust = oldMateCharacter;
				charName = "Old Mate";
			break;

			case CHARACTER.None:
			default:
				charBust = null;
			break;
		};

		if (curDialog[curDialogIndex].isLeftSide)
		{
			charNameLeftText.text = charName;
			charNameRightText.text = "";
			dialogTextBubbleMaterial.mainTextureOffset = new Vector2(0.0f, 0.0f);
			if (leftCharacter != null)
				leftCharacter.SetActive(false);

			leftCharacter = charBust;

		}
		else
		{
			charNameLeftText.text = "";
			charNameRightText.text = charName;
			dialogTextBubbleMaterial.mainTextureOffset = new Vector2(0.0f, 0.5f);

			if (rightCharacter != null)
				rightCharacter.SetActive(false);

			rightCharacter = charBust;
		}

		if (curDialog[curDialogIndex].showingImage)
		{
			switch (curDialog[curDialogIndex].image)
			{
				case IMAGEOVERLAY.Arrow:
					showingImage = arrowObject;
				break;
				case IMAGEOVERLAY.HandPoint_LHS:
					showingImage = fingerPointLHSObject;
				break;
				case IMAGEOVERLAY.HandPoint_RHS:
					showingImage = fingerPointRHSObject;
				break;
			};
		}

		if (curDialog[curDialogIndex].showingImage)
		{
			showingImage.transform.position = new Vector3(curDialog[curDialogIndex].imagePosition.x, 
													 	curDialog[curDialogIndex].imagePosition.y,
													 	showingImage.transform.position.z);
			showingImage.SetActive(true);
		}


		if (curState == TUTSTATE.WaitingOnInput)
		{
			if (charBust != null)
			{
				if (curDialog[curDialogIndex].isLeftSide)
					charBust.transform.localPosition = characterPositionLeft;
				else
					charBust.transform.localPosition = characterPositionRight;

				charBust.SetActive(true);
			}
			curState = TUTSTATE.DisplayingText;
		}
		else
		{
			curState = TUTSTATE.MovingAssets_In;

			if (charBust != null)
			{
				if (curDialog[curDialogIndex].isLeftSide)
					charBust.transform.localPosition = characterPositionLeft;
				else
					charBust.transform.localPosition = characterPositionRight;

				charBust.SetActive(true);
			}

			curTimeToMove = timeToMove;
			if (charBust != null)
			{
				if (curDialog[curDialogIndex].isLeftSide)
					charBust.transform.localPosition = characterPositionLeft;
				else
					charBust.transform.localPosition = characterPositionRight;
				charBust.SetActive(true);
			}
		}

		isLoadingSceneOnDialogEnd = curDialog[curDialogIndex].switchToScene;

		if (isLoadingSceneOnDialogEnd)
			sceneOnDialogEnd = curDialog[curDialogIndex].sceneToSwitchTo;
	}

	public void ShowCutscene(Activity activity)
	{
		
	}

	public void ShowCustomDialog(Dialog customDialog)
	{
		showingCustomDialog = true;

		curDialog = new Dialog[]{customDialog};
		curDialogIndex = 0;

		ShowDialog(curDialogIndex);

		curDialogIndex = 0;
		showingTut = true;
	}

	public void ShowCustomDialog(Dialog[] customDialog)
	{
		showingCustomDialog = true;

		curDialog = customDialog;
		curDialogIndex = 0;

		ShowDialog(curDialogIndex);

		curDialogIndex = 0;
		showingTut = true;
	}

	public bool IsEndOfDialog()
	{
		if(curDialog == null)
			return true;

		if (curDialogIndex < curDialog.Length - 1)
			return false;

		return true;
	}



	void Update()// UpdateTutScreen () 
	{
		if (showingTut)
		{
			switch (curState)
			{
				case TUTSTATE.MovingAssets_In:
					float lerpAmountIn = 1.0f - Mathf.Pow(curTimeToMove / timeToMove, 3.0f);

					curTimeToMove -= Time.deltaTime;
					if (curTimeToMove < 0)
					{
						lerpAmountIn = 1.0f;
						//change to other states
						if (curDialog[curDialogIndex].text != null)
							curState = TUTSTATE.DisplayingText;
						else 
							curState = TUTSTATE.WaitingOnInput;
					}
					transform.localPosition = Vector3.Lerp(inactivePosition, activePosition, lerpAmountIn);


				break;
				case TUTSTATE.DisplayingText:
					string curString =  curDialog[curDialogIndex].text;

					float timePerLetter = 1.0f / lettersPerSecond;
					while (lettersTimer > timePerLetter )
					{
						letterCount ++;
						if (letterCount >= curString.Length)
						{
							curState = TUTSTATE.WaitingOnInput;

							NotifyCallback(CallbackType.ShowDialogDisplayingTextCompleted, 
								new CallbackData
								{
									DialogInfo = curDialog[curDialogIndex].info
								});
						
							break;
						}
						lettersTimer -= Time.deltaTime;
					}

					lettersTimer += Time.deltaTime;
					curString = curString.Substring(0, letterCount);
					dialogText.text = curString;
				break;

				case TUTSTATE.MovingAssets_Out:

					float lerpAmountOut = 1.0f - Mathf.Pow(curTimeToMove / timeToMove, 3.0f);

					NotifyCallback(CallbackType.EndDialogClosingStart);

					curTimeToMove -= Time.deltaTime;
					if (curTimeToMove < 0)
					{
						lerpAmountOut = 1.0f;
						showingTut = false;

						if (curDialog[curDialogIndex].markActivityCompleteOnDialogFinish)
						{
							curActivity.MarkAsCompleted();
							Scheduler.SharedInstance.GetActivities(); // TOREVIEW replace when refreshing activities
							
							//if (mapManager != null)
							//	mapManager.UpdateActivites();
						}

						if (curDialog[curDialogIndex].imageResetOnEnd)
						{
							if (showingImage != null)
								showingImage.SetActive(false);
						}

						NotifyDialogEndCloseComplete();
						NotifyCallback(CallbackType.EndDialogClosingFinished);
					}
					transform.localPosition = Vector3.Lerp(inactivePosition, activePosition, 1.0f - lerpAmountOut);
				break;

				case TUTSTATE.WaitingOnInput:
					///jiggle the thing


				break;
			/*
			if (curTimeToMove >= 0.0f)
			{

				if (movingAssetsIn)
				{
					if (curDialog[curDialogIndex].text != null)
					{
						
					}
				}
				else
				{

				}
			}
			//
			};
		}
	}

	Action dialogEndCloseCompleteCallback = null;
	public void HandleDialogEndCloseComplete(Action dialogEndCloseCompleteCallback)
	{
		this.dialogEndCloseCompleteCallback = dialogEndCloseCompleteCallback;
	}

	void NotifyDialogEndCloseComplete()
	{
		if(dialogEndCloseCompleteCallback != null)
			dialogEndCloseCompleteCallback();
	}

	void InitialiseDialogPool()
	{
		dialogPool = new Dictionary<TutorialInfo, Dialog[]>();

		dialogPool.Add( TutorialInfo.TutorialOverlayHowToPlayCongrats, new Dialog[] { 
			new Dialog("That was awesome, this beats the crap out of skating.", CHARACTER.Player, true),
			new Dialog("Yeah dude! You're a natural on those waves.", CHARACTER.OldMate, false ),
			new Dialog("But I reckon you're ready for something better.", CHARACTER.OldMate, false),
			new Dialog("Check the surf report on your TV. I saw something more your style.", CHARACTER.OldMate, false, IMAGEOVERLAY.Arrow, new Vector2(175, 250), false, true),
		});

		dialogPool.Add( TutorialInfo.TutorialOverlayMexicoMagazineCoverReveal, new Dialog[] { 
			new Dialog("Great session man, barrels look awesome.", CHARACTER.OldMate, true),
			new Dialog("Dude check it out, looks like you got mag cover!", CHARACTER.Player, true, Trophy.MagazineCover),
		});

		dialogPool.Add( TutorialInfo.TutorialOverlayFirstEverCompetitionMainBeach, new Dialog[] { 
			new Dialog("Time to go pro man. I hear there's comp on at main beach for serious cash.", CHARACTER.Player, true),
			new Dialog("Hit the surf report and check it out...", CHARACTER.Player, true)
			{
				markActivityCompleteOnDialogFinish = true
			},
		});

		isDialogInitialised = true;
	}

	public void RecieveInput()
	{
		if (curDialog[curDialogIndex].showingImage)
		{
			if (isLoadingSceneOnDialogEnd)
			{
				SceneManager.LoadScene(sceneOnDialogEnd);
				return;
			}

			if (curDialogIndex < curDialog.Length - 1)
			{
				curDialogIndex++;
				ShowDialog(curDialogIndex);
			}
			else
			{
				curState = TUTSTATE.MovingAssets_Out;
				curTimeToMove = timeToMove;
			}
		}

		// Don't allow dismissal of dialog or quick skipping if in below states
		if(curState == TUTSTATE.MovingAssets_In || 
			curState == TUTSTATE.MovingAssets_Out ||
			curState == TUTSTATE.DisplayingText)
		{
			return;
		}

		if (curState == TUTSTATE.DisplayingText)
		{
			dialogText.text = curDialog[curDialogIndex].text;
			curState = TUTSTATE.WaitingOnInput;
		}
		else
		{
			if (isLoadingSceneOnDialogEnd)
			{
				SceneManager.LoadScene(sceneOnDialogEnd);
				return;
			}

			if (curDialogIndex < curDialog.Length - 1)
			{
				curDialogIndex++;
				ShowDialog(curDialogIndex);
			}
			else
			{
				curState = TUTSTATE.MovingAssets_Out;
				curTimeToMove = timeToMove;
			}
		}
	}

	public string GetDialogText(TutorialInfo tutorialInfo)
	{
		if(dialogPool.ContainsKey(tutorialInfo))
			return dialogPool[tutorialInfo][0].text;
		else
			return string.Empty;
	}

	public enum CustomDialogID
	{
		Undefined,
		PreGameFreeSurfDistanceHowToPlay,
		PreGameFreeSurfMexicoBarrels,
	}

	void InitialiseCustomDialogPool()
	{
		customDialogPool = new Dictionary<CustomDialogID, Dialog[]>();

		// ======= PRE GAME ===========

		customDialogPool.Add(CustomDialogID.PreGameFreeSurfDistanceHowToPlay, new Dialog[] {
			new Dialog("Surf tip: Hold down to go up and release to go down.", CHARACTER.OldMate, true),
			new Dialog("See if you can reach 20 meters.", CHARACTER.OldMate, true),
		});

		customDialogPool.Add(CustomDialogID.PreGameFreeSurfMexicoBarrels, new Dialog[] {
			new Dialog("Rad driving man! Welcome to Scorpion Reef!", CHARACTER.OldMate, true),
			new Dialog("There's a few surf mags around, Ride 3 barrels and get on a mag cover!", CHARACTER.OldMate, true)
		});


		// ======= POST GAME ===========


		isCustomDialogPoolInitialised = true;
	}

	public void ShowCustomDialogByID(CustomDialogID customDialogID)
	{
		if(customDialogPool == null)
			return;
		
		if(customDialogPool.ContainsKey(customDialogID))
		{
			var dialogs = customDialogPool[customDialogID];

			if(dialogs != null && dialogs.Length > 0)
				ShowCustomDialog(dialogs);
		}
	}

	#region Callbacks

	public enum CallbackType
	{
		ShowDialog,
		ShowDialogDisplayingTextCompleted,
		EndDialogClosingStart,
		EndDialogClosingFinished
	}

	class Callback
	{
		public Callback(CallbackType callbackType, Action<CallbackData> callbackHandler)
		{
			CallbackType = callbackType;
			CallbackHandler = callbackHandler;
		}
			
		public CallbackType CallbackType { get; private set; }
		public Action<CallbackData> CallbackHandler { get; private set; }
	}

	public class CallbackData
	{
		public Dialog.Info DialogInfo = Dialog.Info.None;
	}


	List<Callback> callbacks = null;

	public void RegisterCallBack(CallbackType callbackType, Action<CallbackData> callbackHandler)
	{
		if(callbacks == null)
			callbacks = new List<Callback>();

		callbacks.Add(new Callback(callbackType, callbackHandler));
	}

	void NotifyCallback(CallbackType callbackType, CallbackData callbackData = null)
	{
		if(callbacks == null || callbacks.Count == 0)
			return;

		if(callbackData == null)
			callbackData = new CallbackData();

		foreach(var callback in callbacks)
		{
			if(callback.CallbackType == callbackType && callback.CallbackHandler != null)
			{
				callback.CallbackHandler(callbackData);
			}
		}
	}

	#endregion

}

public class Dialog
{
	public Dialog(	string _text, CHARACTER _character, bool _isLeftSide, IMAGEOVERLAY _image = IMAGEOVERLAY.None, Vector3 _imagePosition = default(Vector3),
		bool _imageResetOnEnd = true, bool _markActivityCompleteOnDialogFinish = false, Info info = Info.None, Trophy trophy = Trophy.None)
	{
		character = _character;
		isLeftSide = _isLeftSide;

		showingImage = (_image != IMAGEOVERLAY.None);
		image = _image;
		imagePosition = _imagePosition;
		imageResetOnEnd = _imageResetOnEnd;

		markActivityCompleteOnDialogFinish = _markActivityCompleteOnDialogFinish;

		switchToScene = false;
		sceneToSwitchTo = "";

		text = "";
		text = SpaceOutDialog(_text);
	}

	public Dialog(	string _text, CHARACTER _character, bool _isLeftSide, Trophy _trophyToReveal)
	: this(_text, _character, _isLeftSide)
	{
		trophyToReveal = _trophyToReveal;
	}

	public Dialog(	string _text, CHARACTER _character, bool _isLeftSide, bool _markActivityCompleteOnDialogFinish)
		: this(_text, _character, _isLeftSide)
	{
		markActivityCompleteOnDialogFinish = _markActivityCompleteOnDialogFinish;
	}

	private string SpaceOutDialog( string input)
	{
		string newString = input;

		if (input.Length <= maxCharsPerLine)
			return newString;

		int letterCount = 0;
		int lettersSinceEndLine = 0;

		while (letterCount < newString.Length)
		{
			if (lettersSinceEndLine >= maxCharsPerLine)
			{                
				while (newString[letterCount] != ' ' && newString[letterCount] != '\n')
				{
					letterCount --;
					if (letterCount <= 0)
						return newString;
				}

				newString = newString.Insert(letterCount, "\n");	
				lettersSinceEndLine = 0;
			}
			else
				lettersSinceEndLine ++;

			if (newString[letterCount] == '\n')
				lettersSinceEndLine = 0;

			letterCount ++;
		}

		return newString;
	}
	private const int maxCharsPerLine = 28;

	public enum Info
	{	
		None,
	};

	public string text;
	public CHARACTER? character;
	public bool isLeftSide;

	public bool showingImage;
	public Vector2 imagePosition;
	public IMAGEOVERLAY image;
	public bool imageResetOnEnd = true;

	public Info info = Info.None;

	public bool switchToScene = false;
	public string sceneToSwitchTo = "";

	public Trophy trophyToReveal = Trophy.None;

	public bool markActivityCompleteOnDialogFinish = false;
*/
}

/*
public enum CHARACTER
{
	Player,
	OldMate,
	None
};

public struct CHARACTER_NAME
{
	const string PlayerName = "Stick";
	const string OldMateName = "Old Mate";
	const string None = "";
};

public enum TUTSTATE
{
	None,
	MovingAssets_In,
	DisplayingText,
	WaitingOnInput,
	MovingAssets_Out,
};

public enum IMAGEOVERLAY
{
	None,
	Arrow,
	HandPoint_LHS,
	HandPoint_RHS
}

*/
	//mistaken code f'laters
		/*switch (info)
		{
			//random natter
			case TutorialInfo.None:
				
			break;
			case TutorialInfo.CutsceneFirstTimeLearnToSurf:

			break;
			case TutorialInfo.FreeSurfDistanceHowToPlay:

			break;

			case TutorialInfo.CutsceneEnterCompetitionToWinJetski:

			break;

			case TutorialInfo.TutorialOverlayEnterCompetitionMainBeach:

			break;

			case TutorialInfo.CompetitionBarrelWinJetski:

			break;

			case TutorialInfo.CutsceneWonJetskiTakeItForSpin:

			break;

			case TutorialInfo.ChangeRideRevealJetski:

			break;

			case TutorialInfo.FreeSurfDistanceJetskiMainBeach:

			break;

			case TutorialInfo.TutorialOverlayExplainCredBar:

			break;

			case TutorialInfo.TutorialOverlayExplainGetCredToLevel2ToUnlockBigWaveLocation:

			break;

			case TutorialInfo.ShowUnlockBigWaveLocation:

			break;

			case TutorialInfo.TutorialOverlayFreeSurfBigWaveLocation:

			break;

			case TutorialInfo.TutorialOverLayPregamePopupFreeSurfBigWaveChangeRideToBoard:

			break;

			case TutorialInfo.FreeSurfBigWaveWithNewRide:

			break;
		};
		*/