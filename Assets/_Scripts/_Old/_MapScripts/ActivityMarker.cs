﻿using UnityEngine;
using System.Collections;

public class ActivityMarker : MonoBehaviour {

	public AnimatingObject markerImage;
	public TextMesh timeTextMesh;

    private float markerSinCounter = 0;
	private float markerYSin = 0;
	private float markerXVelocity = 10f;
	private float markerYStartPos = 0;

	private bool bobbingEnabled = true;
	private MarkerMode currentMarkerMode = MarkerMode.Comp;

    public enum MarkerMode
    {
        Comp = 0,
        FreeSurfSmall = 2,
        FreeSurfMedium = 3,
        FreeSurfBig = 3,
        Shack = 4,
        Shells = 5,
        HalfPipe = 6,
        Shop = 7

    };

    public void SetMarkerMode(MarkerMode theMarkerMode)
    {
        currentMarkerMode = theMarkerMode;
        markerImage.DrawFrame((int)theMarkerMode);
          
    }

    public void SetMarkerTime(string timeLeft)
    {
		timeTextMesh.text = timeLeft;
    }

	void Start () 
	{
        markerYStartPos = transform.localPosition.y;
        SetMarkerMode (currentMarkerMode);
	}

	void Update () 
	{
        if(bobbingEnabled)
        {
            // WAVELENGTH
            markerSinCounter += Time.deltaTime * 3f;
            markerYSin = Mathf.Sin(markerSinCounter);

            // AMPLITUDE
            markerYSin /= 10f;
            transform.localPosition = new Vector3(transform.localPosition.x, markerYStartPos + markerYSin,transform.localPosition.z);
        }
	}
}
