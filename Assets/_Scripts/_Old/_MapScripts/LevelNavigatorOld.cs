﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelNavigatorOld : MonoBehaviour 
{
	//const float LEVEL_MARKER_POS_Z = -3.01f;
	const float PLAYER_MARKER_LEVEL_Y_OFFSET = 0.05f;
	float moveSpeed = 5f;
	bool reverseWaypointList = false;

	Vector3 _playerMoveToPosition;

	
	bool movingToLocation = false;
	public Transform playerMarker;
	public LocationMarker locationMarkerEntryPointMaster;
	public int selectedLocationNumber;
	public int playerMaxUnlockedLevel;
	public LevelMainCamera levelMainCamera;

	public LocationMarker[] locationMarkers;
	LocationMarker[] locationEntyIconMarkers;

	int pathIndexCounter = 0;

	Vector3[] pathPoints0_1, pathPoints0_2, pathPoints0_3, pathPoints0_4, pathPoints0_5, pathPoints1_1, pathPoints1_2, 
				pathPoints1_3, pathPoints1_4, pathPoints2_3, pathPoints2_4, pathPoints3_4, currentPathPoints;


	void Start () 
	{
		//Application.targetFrameRate = 60;

		//selectedLocationNumber = GlobalSettings.levelSelectCurrentLocationNumber;
		playerMaxUnlockedLevel = 9999;
				Time.timeScale = 1;
		// TODO: REMOVE THESE HARD CODED VALUES

		locationEntyIconMarkers = new LocationMarker[locationMarkers.Length];
		//Application.LoadLevel (1);
		/*
		var levelMarkerPosXY = new List<KeyValuePair<float, float>>
		{
			new KeyValuePair<float, float>(27f, -67f),	// HOME
			new KeyValuePair<float, float>(-307f, 72f), // STADIUM
			new KeyValuePair<float, float>(283f, 157f), // BURBS
			new KeyValuePair<float, float>(233f, -265f), // SCHOOL
			new KeyValuePair<float, float>(-167f, -140f), // BEACH

		};
		*/

		float playerZPos = playerMarker.transform.position.z;

		Vector3 locationPoint0 = new Vector3(27f, -67f, playerZPos); // HOME
		Vector3 locationPoint1 = new Vector3(-307f, 72f, playerZPos);  // STADIUM
		Vector3 locationPoint2 = new Vector3(283f, 157f, playerZPos); // VOLCANO
		Vector3 locationPoint3 = new Vector3(233f, -265f, playerZPos); // SCHOOL
		Vector3 locationPoint4 = new Vector3(-167f, -140f, playerZPos);  // BEACH
		Vector3 locationPoint5 = new Vector3(329f, -44f, playerZPos); // HALFPIPE

		Vector3 waypoint1 = new Vector3(-130f, 32f, playerZPos);
		Vector3 waypoint2 = new Vector3(-122f, -102f, playerZPos);
		Vector3 waypoint3 = new Vector3(31f, -108f, playerZPos); // HOME ROUNDABOUT
		Vector3 waypoint4 = new Vector3(139f, -102f, playerZPos);
		Vector3 waypoint5 = new Vector3(189f, 137f, playerZPos);
		Vector3 waypoint6 = new Vector3(203, -115f, playerZPos);
		Vector3 waypoint7 = new Vector3(205, -269f, playerZPos);
		Vector3 waypoint8 = new Vector3(207f, -107f, playerZPos);

		Vector3[] locationPoints = new Vector3[]{locationPoint0, locationPoint1, locationPoint2,locationPoint3, locationPoint4, locationPoint5 };

		pathPoints0_1 = new Vector3[]{locationPoint0, waypoint3, waypoint2, waypoint1, locationPoint1 };
		pathPoints0_2 = new Vector3[]{locationPoint0, waypoint3, waypoint4, waypoint5, locationPoint2 };
		pathPoints0_3 = new Vector3[]{locationPoint0, waypoint3, waypoint6, waypoint7, locationPoint3 };
		pathPoints0_4 = new Vector3[]{locationPoint0, waypoint3, waypoint2, locationPoint4 };
		pathPoints0_5 = new Vector3[]{locationPoint0, waypoint3, waypoint8, locationPoint5 };

		pathPoints1_2 = new Vector3[]{locationPoint1, waypoint1, waypoint2, waypoint3, waypoint4, waypoint5, locationPoint2 };
		pathPoints1_3 = new Vector3[]{locationPoint1, waypoint1, waypoint2, waypoint3, waypoint4, waypoint6, waypoint7, locationPoint3 };
		pathPoints1_4 = new Vector3[]{locationPoint1, waypoint1, waypoint2, locationPoint4 };

		pathPoints2_3 = new Vector3[]{locationPoint2, waypoint5, waypoint4, waypoint6, waypoint7, locationPoint3 };
		pathPoints2_4 = new Vector3[]{locationPoint2, waypoint5, waypoint4, waypoint3, waypoint2, locationPoint4 };

		pathPoints3_4 = new Vector3[]{locationPoint3, waypoint7, waypoint6, waypoint4, waypoint3, waypoint2, locationPoint4 };
		
		//foreach(var posXY in levelMarkerPosXY)
		
		for(int i=0; i < locationPoints.Length; i++)
		{
			Vector3 posXY = locationPoints[i];
			//LocationMarker currentLocationMarker = (LocationMarker)Object.Instantiate(locationMarkerEntryPointMaster, new Vector3(posXY.x, posXY.y, locationMarkerEntryPointMaster.transform.position.z), Quaternion.identity);
			//currentLocationMarker.name = "LocationMarker";
			//var levelMarkerScript = currentLocationMarker.GetComponent<LocationMarker>();
			//levelMarkerScript.LocationNumber = i;

			// unlock all locations for now
			//levelMarkerScript.SetLocationLocked(false);

			/*
			locationMarkers[i].LocationNumber = i;
			locationMarkers[i].SetLocationLocked(false);
			*/
			if(i == selectedLocationNumber)
			{
				Vector3 camPos = Camera.main.transform.position;
				Camera.main.transform.position = new Vector3(posXY.x, camPos.y, camPos.z);

				playerMarker.transform.position = new Vector3(posXY.x, posXY.y + PLAYER_MARKER_LEVEL_Y_OFFSET, playerMarker.position.z);

			
			}
			
			locationEntyIconMarkers[i] = locationMarkers[i];

		}
		

		//locationMarkers[5].SetLocationLocked(true);
		//locationMarkers[4].SetLocationLocked(true);
	}
	
	void Update () 
	{
		if (Input.GetMouseButtonDown(0))
		{
			
			SelectLocation();
			//Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			//Debug.LogError("X: " + p.x + "Y: " + p.y);
		}





		if(movingToLocation)
		{
			UpdateMoveToLocation();
		}


		levelMainCamera.UpdateCamera(movingToLocation, playerMarker.transform);

	}

	void UpdateMoveToLocation()
	{

		//playerMarker.transform.position = Vector3.Lerp(playerMarker.transform.position, _playerMoveToPosition, Time.deltaTime * moveSpeed);

		playerMarker.transform.position = Vector3.MoveTowards(playerMarker.transform.position, _playerMoveToPosition, Time.deltaTime * 80 * moveSpeed);
		////////Debug.Log("MOVING: " + playerMarker.transform.position + "  TO:" + _playerMoveToPosition);
			//	//////Debug.Log("TIME ADJUST:" + Time.deltaTime);
				//playerMarker.transform.position = Vector3.MoveTowards(playerMarker.transform.position, _playerMoveToPosition, 50f);

		if(Vector3.Distance(playerMarker.transform.position, _playerMoveToPosition) < 1f)
		{
						////////Debug.Log("STUFF HERE");
			if(reverseWaypointList)
			{
				pathIndexCounter --;
				if(pathIndexCounter >= 0)
				{
					Vector3 levelPos = currentPathPoints[pathIndexCounter];
					_playerMoveToPosition = new Vector3(levelPos.x, levelPos.y + PLAYER_MARKER_LEVEL_Y_OFFSET, levelPos.z);
				}
				else
				{
					OnReachedLocation();
				}
			}
			else
			{
				pathIndexCounter ++;
				if(pathIndexCounter < currentPathPoints.Length)
				{
					Vector3 levelPos = currentPathPoints[pathIndexCounter];
					_playerMoveToPosition = new Vector3(levelPos.x, levelPos.y + PLAYER_MARKER_LEVEL_Y_OFFSET, levelPos.z);
				}
				else
				{
					OnReachedLocation();
				}
			}
		}
	}

	void OnReachedLocation()
	{
		movingToLocation = false;
		//////Debug.Log("REACHED LOCATION:" + selectedLocationNumber);
		if(selectedLocationNumber == 0)
		{
			return;
		}
		else if(selectedLocationNumber == 1)
		{
			GlobalSettings.currentLocationNumber = 1;
			GlobalSettings.currentDifficulty = GlobalSettings.LevelDifficulty.Easy;
			Application.LoadLevel (1);
			return;
		}
		else if(selectedLocationNumber == 2)
		{
			GlobalSettings.currentLocationNumber = 3;
			GlobalSettings.currentDifficulty = GlobalSettings.LevelDifficulty.BigWaveMedium;
			Application.LoadLevel (1);
			return;
		}
		else if(selectedLocationNumber == 3)
		{
			GlobalSettings.currentLocationNumber = 2;
			GlobalSettings.currentDifficulty = GlobalSettings.LevelDifficulty.Medium;
			Application.LoadLevel (1);
			return;
		}
		else if(selectedLocationNumber == 4)
		{
			GlobalSettings.currentLocationNumber = 1;
			GlobalSettings.currentDifficulty = GlobalSettings.LevelDifficulty.Easy;
			Application.LoadLevel(1);
			return;
		}
		else if(selectedLocationNumber == 5)
		{
			GlobalSettings.currentLocationNumber = 5;
			GlobalSettings.currentDifficulty = GlobalSettings.LevelDifficulty.Easy;
			Application.LoadLevel(2);
			return;
		}
	}
	
	public void SelectLocation()
	{
		var levelMarker = GetLevelMarker();
		if(levelMarker == null)
			return;

		reverseWaypointList = false;
		int prevLocationNumber = selectedLocationNumber;
		/*
		selectedLocationNumber = levelMarker.LocationNumber;
		*/
				Debug.Log ("----------------------  CARN LOCATION:" + selectedLocationNumber);
		if(selectedLocationNumber == prevLocationNumber)
		{
			currentPathPoints = null;
			reverseWaypointList = false;
			OnReachedLocation();
						//////Debug.Log("BAILING HERE");
			return;
		}
		else
		{
			Debug.Log ("----------------------  SELECTED LOCATION:" + selectedLocationNumber);
			// FORWARD 0
			if((prevLocationNumber == 0) && (selectedLocationNumber == 1))
			{
				currentPathPoints = pathPoints0_1;
			}
			else if((prevLocationNumber == 0) && (selectedLocationNumber == 2))
			{
				currentPathPoints = pathPoints0_2;
			}
			else if((prevLocationNumber == 0) && (selectedLocationNumber == 3))
			{
				currentPathPoints = pathPoints0_3;
			}
			else if((prevLocationNumber == 0) && (selectedLocationNumber == 4))
			{
				currentPathPoints = pathPoints0_4;
			}
			else if((prevLocationNumber == 0) && (selectedLocationNumber == 5))
			{
				currentPathPoints = pathPoints0_5;
			}

			// REVERSE 0
			else if((prevLocationNumber == 1) && (selectedLocationNumber == 0))
			{
				currentPathPoints = pathPoints0_1;
				reverseWaypointList = true;
			}
			if((prevLocationNumber == 2) && (selectedLocationNumber == 0))
			{
				currentPathPoints = pathPoints0_2;
				reverseWaypointList = true;
			}
			if((prevLocationNumber == 3) && (selectedLocationNumber == 0))
			{
				currentPathPoints = pathPoints0_3;
				reverseWaypointList = true;
			}
			if((prevLocationNumber == 4) && (selectedLocationNumber == 0))
			{
				currentPathPoints = pathPoints0_4;
				reverseWaypointList = true;
			}

			// FORWARD 1
			else if((prevLocationNumber == 1) && (selectedLocationNumber == 2))
			{
				currentPathPoints = pathPoints1_2;
			}
			else if((prevLocationNumber == 1) && (selectedLocationNumber == 3))
			{
				currentPathPoints = pathPoints1_3;
			}
			else if((prevLocationNumber == 1) && (selectedLocationNumber == 4))
			{
				//////Debug.Log("CURRENT PATHS HERE 1_4");
				currentPathPoints = pathPoints1_4;
			}

			// REVERSE 1
			else if((prevLocationNumber == 2) && (selectedLocationNumber == 1))
			{
				currentPathPoints = pathPoints1_2;
				reverseWaypointList = true;
			}
			else if((prevLocationNumber == 3) && (selectedLocationNumber == 1))
			{
				currentPathPoints = pathPoints1_3;
				reverseWaypointList = true;
			}
			else if((prevLocationNumber == 4) && (selectedLocationNumber == 1))
			{
				currentPathPoints = pathPoints1_4;
				reverseWaypointList = true;
			}

			// FORWARD 2
			else if((prevLocationNumber == 2) && (selectedLocationNumber == 3))
			{
				currentPathPoints = pathPoints2_3;
			}
			else if((prevLocationNumber == 2) && (selectedLocationNumber == 4))
			{
				currentPathPoints = pathPoints2_4;
			}

			// REVERSE 2
			else if((prevLocationNumber == 3) && (selectedLocationNumber == 2))
			{
				currentPathPoints = pathPoints2_3;
				reverseWaypointList = true;
			}
			else if((prevLocationNumber == 4) && (selectedLocationNumber == 2))
			{
				currentPathPoints = pathPoints2_4;
				reverseWaypointList = true;
			}

			// FORWARD 3
			else if((prevLocationNumber == 3) && (selectedLocationNumber == 4))
			{
				currentPathPoints = pathPoints3_4;
			}

			// REVERSE 3
			else if((prevLocationNumber == 4) && (selectedLocationNumber == 3))
			{
				currentPathPoints = pathPoints3_4;
				reverseWaypointList = true;
			}
		}


		Debug.Log ("SELECTED LOCATION HERE 1: " + selectedLocationNumber);
				//////Debug.Log("PATH POINTS:" + currentPathPoints.Length);
		//GlobalSettings.soundEnabled = true;
		//GlobalSettings.soundVolume = 1;
		//GlobalSettings.liteMode = false;
		//GlobalSettings.INIT_liteMode = false;
		movingToLocation = true;

		if(reverseWaypointList)
		{
			pathIndexCounter = currentPathPoints.Length-2;
		}
		else
		{
			pathIndexCounter = 1;
		}
		Vector3 levelPos = currentPathPoints[pathIndexCounter];
		_playerMoveToPosition = new Vector3(levelPos.x, levelPos.y + PLAYER_MARKER_LEVEL_Y_OFFSET, levelPos.z);
		


	}
	

	
	public LocationMarker GetLevelMarker()
	{
		var hitInfo = new RaycastHit();
		var hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

		Debug.Log ("hit object with name:" + hitInfo.transform.gameObject.name);
		if (hit && hitInfo.transform.gameObject.GetComponent<LocationMarker>() != null) 
		{
			Debug.Log ("HIT A LOCATION MARKER HERE");
			LocationMarker hitMarker = (LocationMarker)hitInfo.transform.gameObject.GetComponent<LocationMarker>();
			//return this.locationEntyIconMarkers[hitMarker.LocationNumber];
		} 
		
		return null;
	}	
		
}
