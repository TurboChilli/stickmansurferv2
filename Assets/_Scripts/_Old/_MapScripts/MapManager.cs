﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum MapAnimationType 
{
	AnimFinishActivity,
	AnimNewActivity,
	AnimNewLocation,
	Count
}
/*
public class MapManager : MonoBehaviour 
{
	public MapScreenCamera mapCamera = null;
	public Camera mainCam = null;
	public Camera uiCam = null;
	public LevelNavigator levelNav = null;
	public SpotlightEffect spotlight = null;
	public TutorialManager tutManager = null;

    public AnimatingObject waveIcon;
	public CutScene cutScene = null;

	private Activity[] curActivities;

	[Space(10)]
	[Header("Location Icons and Markers")]
	public GameObject mainBeachIcon;
	//public GameObject mainBeachNotification;
	public ActivityMarker mainBeachNotification;
	public Collider mainBeachCollider;
	public LocationMarker mainBeachMarker;
	public ParticleSystem mainBeachUnlockActivityEffect;
	private Activity mainBeachActivity;
	[Space(5)]

	public GameObject bigWaveReefIcon;
	public ActivityMarker bigWaveReefNotification;
	public Collider bigWaveReefCollider;
	public LocationMarker bigWaveReefMarker;
	public ParticleSystem bigWaveReefUnlockActivityEffect;
	private Activity bigWaveReefActivity;
	[Space(5)]

	public GameObject mexicoIcon;
    public ActivityMarker mexicoNotification;
	public Collider mexicoCollider;
	public LocationMarker mexicoMarker;
	public ParticleSystem mexicoUnlockActivityEffect;
	private Activity mexicoActivity;
	[Space(5)]

	public GameObject waveParkIcon;
    public ActivityMarker waveParkNotification;
	public Collider waveParkCollider;
	public LocationMarker waveParkMarker;
	public ParticleSystem waveParkUnlockActivityEffect;
	private Activity waveParkActivity;
	[Space(5)]

	public GameObject shackIcon;
    public ActivityMarker shackNotification;
	public Collider shackCollider;
	public LocationMarker shackMarker;
	public ParticleSystem shackUnlockActivityEffect;
	private Activity shackActivity;
	[Space(5)]

	public GameObject shopIcon;
    public ActivityMarker shopNotification;
	public Collider shopCollider;
	public LocationMarker shopMarker;
	public ParticleSystem shopUnlockActivityEffect;
	private Activity shopActivity;
	[Space(5)]

	public GameObject shellGameIcon;
    public ActivityMarker shellGameNotification;
	public LocationMarker shellGameMarker;
    public Collider shellGameCollider;
	public ParticleSystem shellGameUnlockActivityEffect;
	private Activity shellGameActivity;
	[Space(5)]

	public GameObject halfPipeIcon;
    public ActivityMarker halfPipeNotification;
	public Collider halfPipeCollider;
	public LocationMarker halfPipeMarker;
	public ParticleSystem halfPipeUnlockActivityEffect;
	private Activity halfPipeActivity;

    [Space(10)]

	public GameObject volcanoIcon;
    public ActivityMarker volcanoNotification;
    public Collider volcanoCollider;
    public LocationMarker volcanoMarker;
    public ParticleSystem volcanoActivityEffect;
    private Activity volcanoActivity;

	[Space(10)]

    public LocationMarker jeepGameMarker;
    public ParticleSystem jeepGameActivityEffect;

	[Space(10)]
    
	public CredBar credBarScript;
	public CoinBar coinBarScript;
	public Transform credBar;
	public Transform coinBar;

	public Transform locationPanel;
	public TextMesh locationTitle;
	public TextMesh locationPreamble;
	public TextMesh[] activityTexts;

	private float timeToMoveBottom = 0.5f;
	private float curTimeToMoveBottom = 0.0f;
	private Vector3 inactivePositionBottomSlice;
    private Vector3 activePositionBottomSlice = new Vector3(-20f, 100f, 0);
	private Location showingLocation;
	private bool isShowingBottomSlice = false;

	private bool waitingOnLevelNavigator = false;
	private Location locationToLoad;

	public GameObject playButton;
    //public GameObject oldMateImage;
	public GameObject lockButton;
	public GameObject unlockMoreTriesButton;
	public GameObject backButton;

	private bool inputDown = false;
	private float buttonCooldown = 0.5f;
	private float curButtonCooldown = 0.0f;

	private bool isHighlightingPositions = false;
	private float timeToHighlight = 0.0f;
	private float curTimeToHighlight = 1.0f;
	private List<Vector3> highlightPositions = null;
	private List<MapAnimationType> highlightAnimations = null;
	private int highlightIndex = -1;

	private bool isShowingTutorial = false;

    public GameObject playerMarker;

	private float updateActivityMarkerTimeEverySecs = 1.0f;
  
	void Start () 
	{
		curButtonCooldown = buttonCooldown;

		Application.targetFrameRate = 60;
		Time.timeScale = 1f;
        //waveIcon.DrawFrame(0);
        AnimatingObject playerFrames = playerMarker.GetComponentInChildren<AnimatingObject>();
        playerFrames.DrawFrame(0);
		if (mapCamera == null)
			mapCamera = FindObjectOfType<MapScreenCamera>();
		if (mainCam == null)
			mainCam = Camera.main;
		if (levelNav == null)
			levelNav = FindObjectOfType<LevelNavigator>();
		if (spotlight == null)
			spotlight = FindObjectOfType<SpotlightEffect>();
		if (tutManager == null)
			tutManager = FindObjectOfType<TutorialManager>();

		if (credBar != null)
		{
			//Vector3 uiPosition = mainCam.ScreenToWorldPoint( new Vector3(mainCam.pixelWidth * 0.075f, mainCam.pixelHeight * 0.9f, 0.0f));
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 50f, 50f, credBar.transform,Camera.main);
			//credBar.transform.position = new Vector3(uiPosition.x, uiPosition.y, credBar.transform.position.z);
		}

		if (coinBar != null)
		{
			//Vector3 uiPosition = mainCam.ScreenToWorldPoint( new Vector3(mainCam.pixelWidth * 0.45f, mainCam.pixelHeight * 0.91f, 0.0f));
            UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 180f, 50f, coinBar.transform,Camera.main);
			//coinBar.transform.position = new Vector3(uiPosition.x, uiPosition.y, credBar.transform.position.z);
		}
	
		inactivePositionBottomSlice = locationPanel.transform.position;

		//reconfigure parts of bottom slice for screen size
		locationPanel.transform.localPosition = Vector3.zero;
	

		locationPanel.transform.localPosition = inactivePositionBottomSlice;
		//f'now
		//GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.MainBeach);
		//	GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.Shack);
		//	GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.Shop);
		//	GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.BigWaveReef);
		//	GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.WavePark);
		//	GameState.SharedInstance.PlayerLevel.SetLocationUnlock(Location.HalfPipe);

		//unlock locations
		//UnlockLocations();
		bool[] unlocks = null;
		GameState.SharedInstance.PlayerLevel.UpdateLocationConditionalUnlocks(ref unlocks);

		//activities
		UpdateActivites();

		Location playerLocation = GameState.SharedInstance.PlayerLevel.GetPlayerMapLocation();
		switch (playerLocation)
		{
			case Location.MainBeach:
				levelNav.InitialisePlayerTokenPosition(mainBeachMarker);
			break;
			case Location.Mexico:		
				levelNav.InitialisePlayerTokenPosition(mexicoMarker);
			break;
			case Location.WavePark:
				levelNav.InitialisePlayerTokenPosition(waveParkMarker);
			break;
			case Location.Shack:
				levelNav.InitialisePlayerTokenPosition(shackMarker);
			break;
			case Location.Shop:	
				levelNav.InitialisePlayerTokenPosition(shopMarker);
			break;
			case Location.ShellGame:				
				levelNav.InitialisePlayerTokenPosition(shellGameMarker);
			break;
			case Location.HalfPipe:	
				levelNav.InitialisePlayerTokenPosition(halfPipeMarker);
			break;
			case Location.FourWheelDriveGame:
				levelNav.InitialisePlayerTokenPosition(jeepGameMarker);
			break;
			case Location.Undefined:
			default:
				levelNav.InitialisePlayerTokenPosition(shackMarker);	
			break;
		};


		if (GameState.SharedInstance.moveToLocationOnMapLoad != Location.Undefined)
		{
			MovePlayerAndLoadLevel(GameState.SharedInstance.moveToLocationOnMapLoad);
			GameState.SharedInstance.moveToLocationOnMapLoad = Location.Undefined;
		}

		//update credbar
		float curCred = (float)GameState.SharedInstance.PlayerLevel.CurrentCred;
		float curNextLevelCred = (float)GameState.SharedInstance.PlayerLevel.NextLevelCred;
		int curLevel = GameState.SharedInstance.PlayerLevel.CurrentLevel;

		credBarScript.FillBarToPercentage( curCred / curNextLevelCred, true);
		credBarScript.SetCurLevel(curLevel);

		int coins = GameState.SharedInstance.Cash;

		coinBarScript.DisplayCoins(coins, true);
	}

	/*
	private void UnlockLocations() 
	{
		bool[] locationUnlocks = null;
		GameState.SharedInstance.PlayerLevel.UpdateLocationConditionalUnlocks(ref locationUnlocks);

		for (int i = 0; i < (int)(Location.Count); i++)
		{
			switch((Location)(i))
			{
				case Location.MainBeach:
					mainBeachCollider.enabled = !locationUnlocks[i];
				break;
				case Location.BigWaveReef:
					bigWaveReefReefCollider.enabled = !locationUnlocks[i];
				break;
				case Location.Mexico:
					mexicoCollider.enabled = !locationUnlocks[i];
				break;
				case Location.WavePark:
					waveParkCollider.enabled = !locationUnlocks[i];
				break;
				case Location.Shack:
					shackCollider.enabled = !locationUnlocks[i];
				break;
				case Location.Shop:
					shopCollider.enabled = !locationUnlocks[i];
				break;
				case Location.ShellGame:
					shellGameCollider.enabled = !locationUnlocks[i];
				break;
				case Location.HalfPipe:
					halfPipeCollider.enabled = !locationUnlocks[i];
				break;
                case Location.Volcano:
                    volcanoLock.SetActive(!locationUnlocks[i]);
                    break;
			}
		}
	}
	//

	public void UpdateActivites()
	{
		if (GameState.SharedInstance.currentActivity != null)
		{
			Activity lastActivity = GameState.SharedInstance.currentActivity;
			Activity.CompletionResult result = lastActivity.GetCompletionResult(); 
		
			if (result == Activity.CompletionResult.Succeeded)
			{
				//get location for anim
				switch (lastActivity.Location)
				{
				case Location.MainBeach:
					mapCamera.ZoomOnPositionTimed( mainBeachIcon.transform.position, 3.0f);
				break;
				case Location.BigWaveReef:
					mapCamera.ZoomOnPositionTimed( bigWaveReefIcon.transform.position, 3.0f);
				break;
				case Location.Mexico:
					mapCamera.ZoomOnPositionTimed( mexicoIcon.transform.position, 3.0f);
				break;
				case Location.WavePark:
					mapCamera.ZoomOnPositionTimed( waveParkIcon.transform.position, 3.0f);
				break;
				case Location.Shack:
					mapCamera.ZoomOnPositionTimed( shackIcon.transform.position, 3.0f);
				break;
				case Location.Shop:
					mapCamera.ZoomOnPositionTimed( shopIcon.transform.position, 3.0f);
				break;
				case Location.ShellGame:
					mapCamera.ZoomOnPositionTimed( shellGameIcon.transform.position, 3.0f);
				break;
				case Location.HalfPipe:
					mapCamera.ZoomOnPositionTimed( halfPipeIcon.transform.position, 3.0f);
				break;
                case Location.Volcano:
                    mapCamera.ZoomOnPositionTimed( volcanoIcon.transform.position, 3.0f);
                break;
				case Location.Undefined:
				default:
					//nutin at all
				break;
				}
			}
		}

		//disable notifications
		mainBeachNotification.gameObject.SetActive(false);	
		bigWaveReefNotification.gameObject.SetActive(false);
		mexicoNotification.gameObject.SetActive(false);	
        waveParkNotification.gameObject.SetActive(false);
        shackNotification.gameObject.SetActive(false);	
        shopNotification.gameObject.SetActive(false);	
        shellGameNotification.gameObject.SetActive(false);
        halfPipeNotification.gameObject.SetActive(false);	
        volcanoNotification.gameObject.SetActive(false);

		mainBeachUnlockActivityEffect.gameObject.SetActive(false);
		bigWaveReefUnlockActivityEffect.gameObject.SetActive(false);
		mexicoUnlockActivityEffect.gameObject.SetActive(false);
		waveParkUnlockActivityEffect.gameObject.SetActive(false);
		shackUnlockActivityEffect.gameObject.SetActive(false);
		shopUnlockActivityEffect.gameObject.SetActive(false);
		shellGameUnlockActivityEffect.gameObject.SetActive(false);
		halfPipeUnlockActivityEffect.gameObject.SetActive(false);
        volcanoActivityEffect.gameObject.SetActive(false);

		mainBeachCollider.enabled 		= false;
		bigWaveReefCollider.enabled = false;
		mexicoCollider.enabled 			= false;
		waveParkCollider.enabled 		= false;
		shellGameCollider.enabled 		= false;
		halfPipeCollider.enabled 		= false;
        volcanoCollider.enabled			= false;

		shackCollider.enabled = true;
		shopCollider.enabled = true;

		Scheduler.SharedInstance.SetIgnoreTutorials(false);
		curActivities = Scheduler.SharedInstance.GetActivities();

		//enable the ones that matter
		//backwards to get the added last activities
		bool tutorialFound = false;
		for (int i = 0; i < curActivities.Length && !tutorialFound; i++)
		{
			if (curActivities[i].IsTutorial && curActivities[i].IsSurfTutorial)
			{
				GameState.SharedInstance.currentActivity = curActivities[i];
				SceneNavigator.NavigateTo(SceneType.SurfMain);
			}

            ActivityMarker.MarkerMode activityMarkerMode = ActivityMarker.MarkerMode.FreeSurfSmall;

			switch (curActivities[i].Type)
			{
                case ActivityType.FreeSurf:
                    ActivityDataSurf surfData = curActivities[i].GetActivityData<ActivityDataSurf>();

                    if (surfData.WaveType == WaveType.Average)
                        activityMarkerMode = ActivityMarker.MarkerMode.FreeSurfSmall;
					else if (surfData.WaveType == WaveType.Big)
                        activityMarkerMode = ActivityMarker.MarkerMode.FreeSurfMedium;
					else if (surfData.WaveType == WaveType.Huge)
                        activityMarkerMode = ActivityMarker.MarkerMode.FreeSurfBig;
					else 
                        activityMarkerMode = ActivityMarker.MarkerMode.FreeSurfSmall;
				break;
				case ActivityType.CompetitionSurf:
                    activityMarkerMode = ActivityMarker.MarkerMode.Comp;
				break;
				case ActivityType.Tutorial:
					if (!curActivities[i].Completed)
					{
						tutManager.ShowTutorial(curActivities[i]);
						tutorialFound = true;
					}
				break;

                case ActivityType.HalfPipe:
                    activityMarkerMode = ActivityMarker.MarkerMode.HalfPipe;
                break;
                case ActivityType.Shack:
                    activityMarkerMode = ActivityMarker.MarkerMode.Shack;
                break;

				default:
                    activityMarkerMode = ActivityMarker.MarkerMode.Comp;
				break;
			}

			isShowingTutorial = tutorialFound;

			switch (curActivities[i].Location)
			{
				case Location.MainBeach:
					mainBeachNotification.gameObject.SetActive(true);	
					mainBeachUnlockActivityEffect.gameObject.SetActive(true);
                    mainBeachNotification.SetMarkerMode(activityMarkerMode);
					mainBeachNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					mainBeachActivity = curActivities[i];
					mainBeachCollider.enabled = true;
				break;
				case Location.BigWaveReef:
					bigWaveReefNotification.gameObject.SetActive(true);	
					bigWaveReefUnlockActivityEffect.gameObject.SetActive(true);
					bigWaveReefNotification.SetMarkerMode(activityMarkerMode);
					bigWaveReefNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					bigWaveReefActivity = curActivities[i];
					bigWaveReefCollider.enabled = true;
				break;
				case Location.Mexico:
                    mexicoNotification.gameObject.SetActive(true);	
					mexicoUnlockActivityEffect.gameObject.SetActive(true);
                    mexicoNotification.SetMarkerMode(activityMarkerMode);
					mexicoNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					mexicoActivity = curActivities[i];
					mexicoCollider.enabled = true;
				break;
				case Location.WavePark:
                    waveParkNotification.gameObject.SetActive(true);	
					waveParkUnlockActivityEffect.gameObject.SetActive(true);
                    waveParkNotification.SetMarkerMode(activityMarkerMode);
					waveParkNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					waveParkActivity = curActivities[i];
					waveParkCollider.enabled = true;
				break;
				case Location.Shack:
					shackNotification.gameObject.SetActive(true);	
					shackUnlockActivityEffect.gameObject.SetActive(true);
                    shackNotification.SetMarkerMode(activityMarkerMode);
					shackNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					shackActivity = curActivities[i];
				break;
				case Location.Shop:
                    shopNotification.gameObject.SetActive(true);	
					shopUnlockActivityEffect.gameObject.SetActive(true);
                    shopNotification.SetMarkerMode(activityMarkerMode);
					shopNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					shopActivity = curActivities[i];
				break;
				case Location.ShellGame:
                    shellGameNotification.gameObject.SetActive(true);
					shellGameUnlockActivityEffect.gameObject.SetActive(true);
                    shellGameNotification.SetMarkerMode(activityMarkerMode);
					shellGameNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
					shellGameActivity = curActivities[i];
					shellGameCollider.enabled = true;
				break;
                case Location.HalfPipe:
                    halfPipeNotification.gameObject.SetActive(true);	
                    halfPipeUnlockActivityEffect.gameObject.SetActive(true);
                    halfPipeNotification.SetMarkerMode(activityMarkerMode);
                    halfPipeNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
                    halfPipeActivity = curActivities[i];
					halfPipeCollider.enabled = true;
                break;
                case Location.Volcano:
                    volcanoNotification.gameObject.SetActive(true);    
                    volcanoActivityEffect.gameObject.SetActive(true);
                    volcanoNotification.SetMarkerMode(activityMarkerMode);
                    volcanoNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
                    volcanoActivity = curActivities[i];
					volcanoCollider.enabled = true;
                break;
				default:
					//nutin at all
				break;
			}

		}
	}
		
	void CheckActivityCloseAndUpdateMarkerTime()
	{
		updateActivityMarkerTimeEverySecs -= Time.deltaTime;
		if(updateActivityMarkerTimeEverySecs >= 0)
			return;

		updateActivityMarkerTimeEverySecs = 1.0f;

		bool shouldUpdateActivities = false;
		for(int i = 0; i < curActivities.Length; i++)
		{
			if(curActivities == null || curActivities.Length == 0 || i >= curActivities.Length)
				continue;

			if(curActivities[i].ActivityIsClosed())
			{
				curActivities[i].MarkAsCompleted();
				shouldUpdateActivities = true;
			}

			switch (curActivities[i].Location)
			{
			case Location.MainBeach:
				if (mainBeachActivity != null && !mainBeachActivity.Completed)
					mainBeachNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					mainBeachActivity = null;
				break;
			case Location.BigWaveReef:
				if (bigWaveReefActivity != null && !bigWaveReefActivity.Completed)
					bigWaveReefNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					bigWaveReefActivity = null;
				break;
			case Location.Mexico:
				if (mexicoActivity != null && !mexicoActivity.Completed)
					mexicoNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					mexicoActivity = null;
				break;
			case Location.WavePark:
				if (waveParkActivity != null && !waveParkActivity.Completed)
					waveParkNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					waveParkActivity = null;
				break;
			case Location.Shack:
				if (shackActivity != null && !shackActivity.Completed)
					shackNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					shackActivity = null;
				break;
			case Location.Shop:
				if (shopActivity != null && !shopActivity.Completed)
					shopNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					shopActivity = null;
				break;
			case Location.ShellGame:
				if (shellGameActivity != null && !shellGameActivity.Completed)
					shellGameNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					shellGameActivity = null;
				break;
			case Location.HalfPipe:
				if (halfPipeActivity != null && !halfPipeActivity.Completed)
					halfPipeNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					halfPipeActivity = null;
				break;
            case Location.Volcano:
				if (volcanoActivity != null && !volcanoActivity.Completed)
                    volcanoNotification.SetMarkerTime(curActivities[i].ActivityCloseTimeFormattedString);
				else
					volcanoActivity = null;
                break;
			case Location.Undefined:
			default:
				break;
			}
		}

		if(shouldUpdateActivities)
		{
			UpdateActivites();
		}
	}

	void Update () 
	{
		CheckActivityCloseAndUpdateMarkerTime();

		isShowingTutorial = tutManager.showingTut;

		if (isHighlightingPositions)
		{
			//TODO: anims with highlighting
		}
		else
		{

			if (Input.GetMouseButton(0) && !inputDown)
			{
				if (!CheckButtons())
				{
					if (!isShowingTutorial)
						mapCamera.UpdateCamera();
					inputDown = true;
				}
			}
			else if (Input.GetMouseButton(0))
			{
				if (!isShowingTutorial)
					mapCamera.UpdateCamera();
			}
			else
			{
				inputDown = false;
				if (!isShowingTutorial)
					mapCamera.UpdateCamera();
			}

			if (curTimeToMoveBottom >= 0.0f)
			{
				float lerpVal = curTimeToMoveBottom / timeToMoveBottom;
				lerpVal = 1.0f - Mathf.Pow(lerpVal, 3.0f);
				Vector3 newPos;

				if (isShowingBottomSlice)
					newPos = Vector3.Lerp(inactivePositionBottomSlice, activePositionBottomSlice, lerpVal);
				else
					newPos = Vector3.Lerp(activePositionBottomSlice, inactivePositionBottomSlice, lerpVal);

				curTimeToMoveBottom -= Time.deltaTime;
				if (curTimeToMoveBottom < 0)
				{
					if (isShowingBottomSlice)
						newPos = activePositionBottomSlice;
					else
						newPos = inactivePositionBottomSlice;
				}
				locationPanel.transform.localPosition = newPos;
			}

			if (curButtonCooldown > 0.0f)
				curButtonCooldown -= Time.deltaTime;
		}
	}

	private bool CheckButtons()
	{
		if (Input.GetMouseButton(0) && curButtonCooldown <= 0.0f)
		{
			Ray inputRay = mainCam.ScreenPointToRay( Input.mousePosition );

			RaycastHit hitInfo;
			if (Physics.Raycast(inputRay, out hitInfo, 1000.0f))
			{
				if (hitInfo.collider.tag == "Button")
				{
					Button info = hitInfo.collider.gameObject.GetComponent<Button>();	
                    if (info != null)
                    {
						if (isShowingTutorial && info.type != ButtonType.TutButton)
							return false;

                        EnactButton(info);
                        curButtonCooldown = buttonCooldown;

                        return true;
                    }
				}
			}

			Ray inputUIRay = uiCam.ScreenPointToRay( Input.mousePosition);
			if (Physics.Raycast(inputUIRay, out hitInfo, 1000.0f, LayerMask.GetMask("UI")))
			{
				if (hitInfo.collider.tag == "Button")
				{
					Button info = hitInfo.collider.gameObject.GetComponent<Button>();	
                    if (info != null)
                    {
						if (isShowingTutorial && info.type != ButtonType.TutButton)
							return false;

                        EnactButton(info);
                        curButtonCooldown = buttonCooldown;

                        return true;
                    }
				}
			}
		}

		return false;
	}

	private void EnactButton(Button buttonInfo)
	{
		if (!waitingOnLevelNavigator)
		{
			switch (buttonInfo.type)
			{
				case ButtonType.Location:
					ShowLocationPreamble(buttonInfo.location);

				break;

				case ButtonType.Lock:
					//wiggle round a bit

				break;

				case ButtonType.TutButton:
					tutManager.RecieveInput();
				break;

				case ButtonType.StartGame:
					MovePlayerAndLoadLevel(showingLocation);
				break;

				case ButtonType.AddCoin:
					locationToLoad = Location.Shop;
					MovePlayerAndLoadLevel(Location.Shop);
					break;

				case ButtonType.ResetRetries:
					foreach (Activity activity in curActivities)
					{
						activity.ResetTries();
					}

					mainBeachActivity.ResetTries();
					bigWaveReefActivity.ResetTries();
					mexicoActivity.ResetTries();
					waveParkActivity.ResetTries();

					UpdateActivites();
					break;

				case ButtonType.Back:
					HideLocationPreamble();
					break;

                case ButtonType.DEBUG_BUTTON:
                    Application.Quit();
                    SceneNavigator.NavigateTo(SceneType.ShellGame);
					break;
			};
		}

		//////Debug.Log("Pressed : " + buttonInfo.type + " of location " + buttonInfo.location);
	}

	public void MovePlayerAndLoadLevel(Location locationToMove)
	{
		bool alreadyThere = false;
		locationToLoad = locationToMove;
        HideLocationPreamble();
		//GameState.SharedInstance.PlayerLevel.SetPlayerMapLocation(locationToMove);

		switch(locationToMove)
		{
			case Location.MainBeach:
				alreadyThere = levelNav.MoveToPosition(mainBeachMarker);
				GameState.SharedInstance.currentActivity = mainBeachActivity;						
			break;
			case Location.BigWaveReef:
				alreadyThere = levelNav.MoveToPosition(bigWaveReefMarker);
				GameState.SharedInstance.currentActivity = bigWaveReefActivity;						
			break;
			case Location.Mexico:		
				alreadyThere = levelNav.MoveToPosition(mexicoMarker, true);
				GameState.SharedInstance.currentActivity = mexicoActivity;
			break;
			case Location.WavePark:
				alreadyThere = levelNav.MoveToPosition(waveParkMarker);
				GameState.SharedInstance.currentActivity = waveParkActivity;
			break;
			case Location.Shack:
				alreadyThere = levelNav.MoveToPosition(shackMarker);
				GameState.SharedInstance.currentActivity = shackActivity;
			break;
			case Location.Shop:	
				alreadyThere = levelNav.MoveToPosition(shopMarker);
				GameState.SharedInstance.currentActivity = shopActivity;
			break;
			case Location.ShellGame:				
				alreadyThere = levelNav.MoveToPosition(shellGameMarker);
				GameState.SharedInstance.currentActivity = shellGameActivity;
			break;
			case Location.HalfPipe:	
				alreadyThere = levelNav.MoveToPosition(halfPipeMarker);
				GameState.SharedInstance.currentActivity = halfPipeActivity;
            break;
            case Location.Volcano: 
                alreadyThere = levelNav.MoveToPosition(volcanoMarker);
                GameState.SharedInstance.currentActivity = volcanoActivity;
			break;
			case Location.Undefined:
			default:
				//nutin at all
			break;
		};

		if (alreadyThere)
			LoadWaitingLevel();
	}

	public void LoadWaitingLevel()
	{
		waitingOnLevelNavigator = false;

		GameState.SharedInstance.PlayerLevel.SetPlayerMapLocation(locationToLoad);

		switch(locationToLoad)
		{
			case Location.MainBeach:
			case Location.BigWaveReef:
			case Location.Mexico:
			case Location.WavePark:
				SceneNavigator.NavigateTo(SceneType.SurfMain);
			break;
			case Location.Shack:
				SceneNavigator.NavigateTo(SceneType.Shack);
			break;
			case Location.Shop:
				SceneNavigator.NavigateTo(SceneType.Shop);
			break;
			case Location.ShellGame:
				SceneNavigator.NavigateTo(SceneType.ShellGame);
			break;
			case Location.HalfPipe:
				SceneNavigator.NavigateTo(SceneType.HalfPipe);
			break;
			case Location.Undefined:
			default:
				//nutin at all
			break;
		};
	}

	private bool SkipPreambleAndAutoNavitateToLocation(Location location)
	{
		Activity activity = GameState.SharedInstance.GetCurrentActivity();

		if(activity != null)
		{
			if(activity.IsTutorial && activity.Tutorial.Info == TutorialInfo.FreeSurfMexicoBarrels)
			{
				showingLocation = location;
				Button startGameButton = new Button();
				startGameButton.type = ButtonType.StartGame;
				EnactButton(startGameButton);

				return true;
			}
		}
			
		return false;
	}

	private void ShowLocationPreamble(Location location)
	{
		if(SkipPreambleAndAutoNavitateToLocation(location))
			return;

		showingLocation = location;
		Activity locationActivity = null;
		bool surfActivity = false;

		switch(showingLocation)
		{
		case Location.MainBeach:
			locationTitle.text = LocationTitle.MainBeachTitle;
			locationActivity = mainBeachActivity;
			surfActivity = true;
			break;
		case Location.BigWaveReef:
			locationTitle.text = LocationTitle.BigWaveReefTitle;
			locationActivity = bigWaveReefActivity;
			surfActivity = true;
			break;
		case Location.Mexico:
			locationTitle.text = LocationTitle.MexicoTitle;	
			locationActivity = mexicoActivity;
			surfActivity = true;
			break;
		case Location.WavePark:
			locationTitle.text = LocationTitle.WaveParkTitle;	
			locationActivity = waveParkActivity;
			surfActivity = true;
			break;
		case Location.Shack:
			locationTitle.text = LocationTitle.ShackTitle;		
			locationActivity = shackActivity;
			break;
		case Location.Shop:
			locationTitle.text = LocationTitle.ShopTitle;			
			locationActivity = shopActivity;
			break;
		case Location.ShellGame:
			locationTitle.text = LocationTitle.ShellGameTitle;			
			locationActivity = shellGameActivity;			
			break;
		case Location.HalfPipe:
			locationTitle.text = LocationTitle.HalfPipeTitle;	
			locationActivity = halfPipeActivity;
			break;
		case Location.Volcano:
			locationTitle.text = LocationTitle.VolcanoTitle;   
			locationActivity = volcanoActivity;
			break;
		case Location.Undefined:
		default:
			//nutin at all
			break;
		};

		mapCamera.acceptingInput = false;
		isShowingBottomSlice = true;
		curTimeToMoveBottom = timeToMoveBottom;

		if (GameState.SharedInstance.PlayerLevel.GetLocationUnlock(location) 
			|| location == Location.Shack || location == Location.Shop)
		{
			playButton.SetActive(true);
			lockButton.SetActive(false);
			unlockMoreTriesButton.SetActive(false);
		}
		else
		{
			playButton.SetActive(false);
			lockButton.SetActive(true);
			unlockMoreTriesButton.SetActive(false);
		}

		if (location == Location.Shack)
			waveIcon.DrawFrame(4);
		if (location == Location.ShellGame)
			waveIcon.DrawFrame(5);
		if (location == Location.HalfPipe)
			waveIcon.DrawFrame(6);
		if (location == Location.Shop)
			waveIcon.DrawFrame(7);


		if (surfActivity)
		{
			if (locationActivity != null)
			{
                ActivityDataSurf surfData = locationActivity.GetActivityData<ActivityDataSurf>();

                if (locationActivity.Type == ActivityType.CompetitionSurf)
                {
                    waveIcon.DrawFrame(0);
                }
                else
                {
                    if (surfData.WaveType == WaveType.Average)
                    {
                        waveIcon.DrawFrame(2);
                    }
                    else
                    {
                        waveIcon.DrawFrame(3);

                    }
                }
				if (locationActivity.Type == ActivityType.CompetitionSurf &&
					(locationActivity.MaxNumberOfTries.HasValue && locationActivity.MaxNumberOfTries <= 0))
				{
					for (int i = 0; i < activityTexts.Length; i++)
					{
						activityTexts[i].text = "";
					}
					activityTexts[0].text = "No more retrys";
					playButton.SetActive(false);
					lockButton.SetActive(true);
					unlockMoreTriesButton.SetActive(true);

					return;
				}

                if (locationActivity.Type == ActivityType.CompetitionSurf)
                {
                    locationPreamble.text = "SURF COMP";// + locationActivity.Goals.Count;
                    if (locationActivity.Prize.Type == PrizeType.Money)
                    {
                        activityTexts[0].text = "Prize:" + locationActivity.Prize.Amount + " coins";
                    }
                    else if (locationActivity.Prize.Type == PrizeType.Vehicle)
                    {
                        //////Debug.Log("PRIZE:" + locationActivity.Prize.Vehicle);
                        VehicleInfo theInfo = GameState.SharedInstance.GetVehicleInfo(locationActivity.Prize.Vehicle);
                        activityTexts[0].text = "Prize: " + theInfo.Name;
                    }
                    activityTexts[1].text = "Required Goals:" + locationActivity.Goals.Count;
                }
                else
                {
                    locationPreamble.text  = "FREE SURF";
                    activityTexts[1].text = "Goals:" + locationActivity.Goals.Count;
                    if (surfData.WaveType == WaveType.Average)
                    {
                        activityTexts[0].text = "Wave Height: 6ft";
                    }
                    else
                    {
                        activityTexts[0].text = "Wave Height: 12ft";

                    }
                }

                activityTexts[2].text = "";
                ///*
				for (int i = 0; i < activityTexts.Length; i++)
				{
					if (i >= locationActivity.Goals.Count)
					{
						activityTexts[i].text = "";
					}
					else if (locationActivity.Goals[i] != null)
					{
						switch (locationActivity.Goals[i].Type)
						{
							case ActivityGoalType.ReachDistance:
								ActivityGoalReachDistance distGoal = (ActivityGoalReachDistance)locationActivity.Goals[i];
								activityTexts[i].text = string.Format(ActivityGoalTypeString.ReachDistanceDescription, distGoal.Distance);
							break;

							case ActivityGoalType.Barrels:
								ActivityGoalBarrels barrelGoal = (ActivityGoalBarrels)locationActivity.Goals[i];
								activityTexts[i].text = string.Format(ActivityGoalTypeString.BarrelsDescription, barrelGoal.TotalRequired);
							break;

							case ActivityGoalType.PierGrinds:
								ActivityGoalPierGrinds pierGoal = (ActivityGoalPierGrinds)locationActivity.Goals[i];
								activityTexts[i].text = string.Format(ActivityGoalTypeString.PierGrindsDescription, pierGoal.TotalRequired);
							break;

							case ActivityGoalType.CollectLetters:
								activityTexts[i].text = ActivityGoalTypeString.CollectLettersDescription;
							break;
						};
					}
				}
    		//            
			}
			else
			{
				locationPreamble.text = "Surf's flat dude \nCheck back later";
				for (int i = 0; i < activityTexts.Length; i++)
					activityTexts[i].text = "";

                waveIcon.gameObject.SetActive(false);
				playButton.SetActive(false);
				lockButton.SetActive(true);
				unlockMoreTriesButton.SetActive(false);

			}
		}
		else
		{ 
			if (locationActivity != null)
			{
				locationPreamble.text = locationActivity.PreAmble;

				for (int i = 0; i < activityTexts.Length; i++)
					activityTexts[i].text = "";
			}
			else
			{
				locationPreamble.text = "";
				for (int i = 0; i < activityTexts.Length; i++)
					activityTexts[i].text = "";
			}
		}
	}

	private void HideLocationPreamble()
	{
		if(isShowingBottomSlice)
		{
			mapCamera.acceptingInput = true;
			isShowingBottomSlice = false;
			curTimeToMoveBottom = timeToMoveBottom;
		}
	}
		
}
*/