﻿using UnityEngine;
using System.Collections;

public class LevelMainCamera : MonoBehaviour {
	
	const float HORIZONTAL_SPEED = 40.0f;
	const float SCROLL_SENSITIVITY = 0.05f;
	const float SCROLL_POS_X_MIN = -100f;//140f;
	const float SCROLL_POS_X_MAX = 80f;

	const float SCROLL_POS_Y_MIN = -60f;
	const float SCROLL_POS_Y_MAX = 80f;
	
	bool _dragging;

	float speed = 0.5f;
	
	void Start () 
	{
	
	}

	public bool SnapToView()
	{
		bool didSnap = false;
		if(transform.position.x < SCROLL_POS_X_MIN)				
		{
			transform.position = new Vector3(SCROLL_POS_X_MIN, transform.position.y, transform.position.z);
			didSnap = true;
		}
		
		else if(transform.position.x > SCROLL_POS_X_MAX)
		{
			transform.position = new Vector3(SCROLL_POS_X_MAX, transform.position.y, transform.position.z);
			didSnap = true;
		}

		if(transform.position.y < SCROLL_POS_Y_MIN)				
		{
			transform.position = new Vector3(transform.position.x, SCROLL_POS_Y_MIN, transform.position.z);
			didSnap = true;
		}
		
		else if(transform.position.y > SCROLL_POS_Y_MAX)
		{
			transform.position = new Vector3(transform.position.x, SCROLL_POS_Y_MAX, transform.position.z);
			didSnap = true;
		}
		return didSnap;
	}
	
	public void UpdateCamera (bool movingToLocation, Transform playerMarkerTransform) 
	{
		/*
		if(movingToLocation)
		{
			transform.position = new Vector3(playerMarkerTransform.position.x, playerMarkerTransform.position.y, transform.position.z);
			SnapToView();
			return;
		}
		*/

		if(Input.GetMouseButton(0))
			_dragging = true;
		
		if(Input.GetMouseButtonUp(0))
			_dragging = false;

		if(Application.isEditor)
		{
			if(_dragging)
			{
				float movementX = -1 * (Input.GetAxis("Mouse X")/SCROLL_SENSITIVITY);
				float movementY = -1 * (Input.GetAxis("Mouse Y")/SCROLL_SENSITIVITY);
				//Debug.Log ("movementX:" + movementX);
				transform.Translate(movementX , movementY, 0);

				//SnapToView();
			}

		}
		else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) 
		{
			
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			
			// Move object across XY plane
			transform.Translate (-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);

			//SnapToView();
		}

		SnapToView();
	}
}
