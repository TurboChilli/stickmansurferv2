﻿using UnityEngine;
using System.Collections;

public enum ButtonType
{
	Location = 0,
	StartGame = 1,
	Back = 2,
	Menu = 3,
	AddCred = 4,
	AddCoin = 5,
	Lock = 6,
	ResetRetries = 7,
	DEBUG_BUTTON = 8,
	ChangeRide = 10,
	TutButton = 11,
	CloseTutorialPopup = 12,
	ShackTVButton = 13,
	SurfReportButton = 14,
	None
};

public class Button : MonoBehaviour 
{
	public ButtonType type = ButtonType.Location;
	public Location location = Location.MainBeach;
}
