﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelNavigator : MonoBehaviour {

	private LocationMarker[] locationMarkers = null; 

	//private MapManager mapMan = null;
	private MapScreenCamera mapCamera = null;

	public Transform playerToken = null;
	public LocationMarker currentPlayerMarker = null;
	private LocationMarker toMarker = null;
	private int pathIndex = 0;

    private float moveSpeedInit = 0.5f;
	private float timeToMove = 0.5f;
	private float curTimeToMove = 0.0f;
    private float moveVelocity = 100f; // note bigger is faster
	public bool isDoneMoving = true;
	private bool isMoving = false;
	private LocationMarker[] path = null;

    public AnimatingObject playerMarker;

    private bool initialised = false;

	void Start () 
	{
		if (!initialised)
			Initialise();
	}

	void Initialise()
	{
		locationMarkers = GameObject.FindObjectsOfType<LocationMarker>();
		//if (mapMan == null)
		//	mapMan = FindObjectOfType<MapManager>();
		if (mapCamera == null)
			mapCamera = FindObjectOfType<MapScreenCamera>();

		initialised = true;

	}

	public void InitialisePlayerTokenPosition(LocationMarker playerMarker)
	{
		if (!initialised)
			Initialise();

		currentPlayerMarker = playerMarker;
        playerToken.transform.position = currentPlayerMarker.transform.position + Vector3.up;

		mapCamera.MoveToPosition(playerToken.transform.position);
	}

	public bool MoveToPosition(LocationMarker destination, bool isJeepBlockedDestination = false)
	{	
		if (!initialised)
			Initialise();

		if (destination == null)
		{
			//////Debug.Log("Destination is null");
			return false;
		}

		if (destination == currentPlayerMarker )
			return true;

		if (isJeepBlockedDestination)
		{
			/*if(GameState.SharedInstance.PlayerLevel.GetPlayerMapLocation() == Location.FourWheelDriveGame)
	        {
				SceneNavigator.NavigateTo(SceneType.JeepSand);
				GameState.SharedInstance.PlayerLevel.SetPlayerMapLocation(Location.FourWheelDriveGame);
	        }*/
        }

		
		toMarker = destination;
		pathIndex = 0;

		//reset all markers
		for (int i = 0; i < locationMarkers.Length; i++)
		{
			locationMarkers[i].ResetPathFindingValues();
		}

		//Start stack with originPoint
		LocationMarker curMarker = null;
		float curDistance = 0;

		List<LocationMarker> markerStack = new List<LocationMarker>();
		markerStack.Add(currentPlayerMarker);

		bool foundPath = false;
		while (markerStack.Count > 0 && !foundPath)
		{
			curMarker = markerStack[markerStack.Count -1];
			markerStack.Remove(curMarker);
			curMarker.visited = true;
			curDistance = curMarker.distance;
		
			for( int i = 0; i < curMarker.connectedMarkers.Count; i++)
			{
				if (curMarker.connectedMarkers != null)
				{
					if (curMarker.connectedMarkers[i] != null)
					{
						if (!curMarker.connectedMarkers[i].visited)
						{
							float distanceFromOrigin = Vector3.Distance(
													curMarker.gameObject.transform.position,
													curMarker.connectedMarkers[i].gameObject.transform.position) +
													curDistance;
							
							if (curMarker.connectedMarkers[i].lastMarker == null)
							{
								curMarker.connectedMarkers[i].lastMarker = curMarker;
								curMarker.connectedMarkers[i].distance = distanceFromOrigin;
								markerStack.Add(curMarker.connectedMarkers[i]);

								if (curMarker.connectedMarkers[i] == destination)
								{
									foundPath = true;
									curMarker = curMarker.connectedMarkers[i];
								}
							}
							else if (distanceFromOrigin < curMarker.connectedMarkers[i].distance)
							{
								curMarker.connectedMarkers[i].lastMarker = curMarker;
								curMarker.connectedMarkers[i].distance = distanceFromOrigin;
							}
						}
					}

				}
			}
		}

		if (!foundPath)
		{
			//////Debug.Log("Path Not Found");
			isMoving = false;
		}
		else 
		{
			List<LocationMarker> listPath = new List<LocationMarker>();
			listPath.Add(curMarker);

			while (	curMarker.lastMarker != null && 
					curMarker.lastMarker != currentPlayerMarker)
			{
				listPath.Add(curMarker.lastMarker);
				curMarker = curMarker.lastMarker;

                ////////Debug.Log("CURRMARKER: " + curMarker);
			}
			listPath.Reverse();
			path = listPath.ToArray();
			////////Debug.Log("FoundPath");
			isMoving = true;

			pathIndex = 1;

			toMarker = path[0];

            float distanceBetweenNodes = Vector3.Distance(currentPlayerMarker.transform.position, toMarker.transform.position);
            curTimeToMove = moveSpeedInit * (distanceBetweenNodes / moveVelocity);
            timeToMove = curTimeToMove;

            UpdatePlayerFrame(currentPlayerMarker.transform.position,toMarker.transform.position);
		}

		return false;
	}

    void UpdatePlayerFrame(Vector3 fromPoint, Vector3 toPoint)
    {
        float xDiff = toPoint.x - fromPoint.x;
        float yDiff = toPoint.y - fromPoint.y;

        if (Mathf.Abs(xDiff) > Mathf.Abs(yDiff))
        {
            // a sideways frame
            if (toPoint.x > fromPoint.x)
            {
                playerMarker.DrawFrame(0);
            }
            else
            {
                playerMarker.DrawFrame(1);
            }
        }
        else
        {
            // an up down frame
            if (toPoint.y > fromPoint.y)
            {
                playerMarker.DrawFrame(3);
            }
            else
            {
                playerMarker.DrawFrame(2);
            }
        }


    }

	// Update is called once per frame
	void Update () 
	{
		if (isMoving)
		{
			float lerpVal = 1.0f - curTimeToMove / timeToMove;
            
			Vector3 newPos = Vector3.Lerp(currentPlayerMarker.transform.position, toMarker.transform.position, lerpVal);


			curTimeToMove -= Time.deltaTime;
			if (curTimeToMove < 0.0f)
			{
				if (pathIndex < path.Length)
				{
					currentPlayerMarker = toMarker;
					toMarker = path[pathIndex];

					pathIndex++;

                    // distanceBetweenNodes will be between 50 and 170 ish
                    float distanceBetweenNodes = Vector3.Distance(currentPlayerMarker.transform.position, toMarker.transform.position);
                    curTimeToMove = moveSpeedInit * (distanceBetweenNodes / moveVelocity);
                    timeToMove = curTimeToMove;

                    UpdatePlayerFrame(currentPlayerMarker.transform.position,toMarker.transform.position);

                    if(currentPlayerMarker.gameObject.tag.Equals("MiniGameTriggerJeep"))
                    {
                    	/*
                        if (GameState.SharedInstance.PlayerLevel.GetPlayerMapLocation() != Location.Mexico)
                        {
							SceneNavigator.NavigateTo(SceneType.JeepSand);
							GameState.SharedInstance.PlayerLevel.SetPlayerMapLocation(Location.FourWheelDriveGame);
                        }
                        */
                    }
				}
				else
				{
					isMoving = false;
                    newPos = toMarker.transform.position;
					currentPlayerMarker = toMarker;
					//mapMan.LoadWaitingLevel();
                    curTimeToMove = timeToMove;
				}
			}

            playerToken.transform.position = newPos + Vector3.up;
			mapCamera.SmoothFollowPosition(playerToken.transform.position);
            
			for (int i = 1; i < path.Length; i++)
			{
				Debug.DrawLine(	path[i - 1].transform.position + Vector3.back,
								path[i].transform.position + Vector3.back,
								Color.red);
			}
		}
	}

}
