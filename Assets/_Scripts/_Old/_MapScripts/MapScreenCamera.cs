﻿using UnityEngine;
using System.Collections;

public class MapScreenCamera : MonoBehaviour {

	public Rect maxBounds = new Rect(-600, -400, 1200, 800);
	private Vector2 smoothingBoundsMin = new Vector2(0.1f, 0.1f);
	private Vector2 smoothingBoundsMax = new Vector2(0.9f, 0.9f);


	private Vector3 lastPosition;

	private bool inputDown = false;
	public bool acceptingInput = true;

	public Camera mainCamera = null;

	private bool isZoomingAndWaiting = false;
	private bool isZooming = false;
	private Vector3 zoomPosition;
	private Vector3 prevPosition;
	private float prevOrthographicSize;
	private float zoomOrthographicSize = 150.0f;

	private float timeZooming;
	private float curTimeZooming;

	private bool isSmoothingPosition = false;
	private Vector3 smoothingPosition;
	private float timeToSmooth = 3.0f;
	private float curTimeToSmooth = 0.0f;

	void Start()
	{
		if (mainCamera == null)
			mainCamera = Camera.main;
	}

	public void ZoomOnPositionTimed(Vector3 zoomPos, float timeInZoom)
	{
		/*
		isZoomingAndWaiting = false;
		isZooming = true;
		prevPosition = transform.position;
		zoomPosition = zoomPos;
		timeZooming = timeInZoom;
		curTimeZooming = timeInZoom;
		prevOrthographicSize = mainCamera.orthographicSize;
        */
	}

	public void ZoomOnPositionTriggerOut(Vector3 zoomPos, float timeInZoom)
	{
        /*
		isZoomingAndWaiting = true;
		isZooming = true;
		prevPosition = transform.position;
		zoomPosition = zoomPos;
		timeZooming = timeInZoom;
		curTimeZooming = timeInZoom;
		prevOrthographicSize = mainCamera.orthographicSize;
        */      
	}

	public void MoveToPosition(Vector3 position)
	{
		if (position.x < maxBounds.x)
			position.x = maxBounds.x;
		if (position.x > maxBounds.x + maxBounds.width)
			position.x = maxBounds.x + maxBounds.width;

		if (position.y < maxBounds.y)
			position.y = maxBounds.y;
		if (position.y > maxBounds.y + maxBounds.height)
			position.y = maxBounds.y + maxBounds.height;

		transform.position = new Vector3(position.x, position.y, transform.position.z);
	}

	public void SmoothFollowPosition(Vector3 position)
	{
		if (isSmoothingPosition)
			return;

		Vector3 screenPos = mainCamera.WorldToScreenPoint(position);

		float xDelta = 0.0f;
		float yDelta = 0.0f;

		if(screenPos.x < mainCamera.pixelWidth * smoothingBoundsMin.x || screenPos.x > mainCamera.pixelWidth * smoothingBoundsMax.x ||
			screenPos.y < mainCamera.pixelHeight * smoothingBoundsMin.y || screenPos.y > mainCamera.pixelHeight * smoothingBoundsMax.y)
		{
			xDelta = (position - transform.position).x;
			yDelta = (position - transform.position).y;
		}

		if (xDelta != 0.0f || yDelta != 0.0f)
		{
			isSmoothingPosition = true;
			if (curTimeToSmooth <= 0.0f)
				curTimeToSmooth = timeToSmooth;
		}

		position = transform.position + new Vector3(xDelta, yDelta, 0.0f);
	
		if (position.x < maxBounds.x)
			position.x = maxBounds.x;
		if (position.x > maxBounds.x + maxBounds.width)
			position.x = maxBounds.x + maxBounds.width;

		if (position.y < maxBounds.y)
			position.y = maxBounds.y;
		if (position.y > maxBounds.y + maxBounds.height)
			position.y = maxBounds.y + maxBounds.height;
		
		smoothingPosition = new Vector3(position.x, position.y, transform.position.z);
	}


	public void TriggerZoomOut()
	{
		isZoomingAndWaiting = false;
	}

	public void UpdateCamera () 
	{
		if (isSmoothingPosition)
		{
			transform.position = Vector3.Lerp(transform.position, smoothingPosition, 5.0f * Time.deltaTime);
			if (Vector3.Distance(transform.position, smoothingPosition) < 1.0f)
				isSmoothingPosition = false;
		}

		if (isZooming)
		{
			float ratio = 1.0f - curTimeZooming / timeZooming;
			float lerpValue;
			if (ratio < 0.2f)
			{
				lerpValue = ratio / 0.2f;
				lerpValue = Mathf.Pow(lerpValue, 0.25f);

				//move without zooming
				Vector3 newPos = Vector3.Lerp(prevPosition, zoomPosition, lerpValue);
				transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);

				//zoom in
				float zoomAmount = Mathf.Lerp(prevOrthographicSize, zoomOrthographicSize, lerpValue );
				mainCamera.orthographicSize = zoomAmount;

				curTimeZooming -= Time.deltaTime;
			}
			else if (ratio < 0.8f)
			{
				lerpValue = (ratio - 0.2f) /  (0.8f - 0.2f);

				transform.position = new Vector3(zoomPosition.x, zoomPosition.y, transform.position.z);
				curTimeZooming -= Time.deltaTime;
			}
			else
			{
				lerpValue = (ratio - 0.8f) /  (1.0f - 0.8f);
				lerpValue = Mathf.Pow(lerpValue, 0.25f);


				Vector3 newPos = Vector3.Lerp(prevPosition, zoomPosition, 1.0f - lerpValue);
				transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);

				float zoomAmount = Mathf.Lerp(prevOrthographicSize, zoomOrthographicSize, 1.0f - lerpValue);
				mainCamera.orthographicSize = zoomAmount;

				if (!isZoomingAndWaiting)
					curTimeZooming -= Time.deltaTime;
			}

			if (curTimeZooming <= 0)
			{
				mainCamera.orthographicSize = prevOrthographicSize;
				isZooming = false;
			}
		}
		else if (Input.GetMouseButton(0) && acceptingInput)
		{
			if (inputDown)
			{
				Vector3 newPos = transform.position;
			
				Vector3 direction = lastPosition - mainCamera.ScreenToWorldPoint(Input.mousePosition);
				float distance = direction.magnitude;

				newPos += new Vector3(direction.x, direction.y, 0);

				if (newPos.x < maxBounds.x)
					newPos.x = maxBounds.x;
				if (newPos.x > maxBounds.x + maxBounds.width)
					newPos.x = maxBounds.x + maxBounds.width;

				if (newPos.y < maxBounds.y)
					newPos.y = maxBounds.y;
				if (newPos.y > maxBounds.y + maxBounds.height)
					newPos.y = maxBounds.y + maxBounds.height;

				transform.position = newPos;
			}
			else
			{
				inputDown = true;
				lastPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
			}
		}
		else
		{
			inputDown = false;
		}

	}
}
