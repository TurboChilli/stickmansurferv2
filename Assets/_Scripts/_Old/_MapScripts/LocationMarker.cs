﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[ExecuteInEditMode]
public class LocationMarker : MonoBehaviour, IComparable
{
	public List<LocationMarker> connectedMarkers = null;

	//for pathfinding
	public bool visited = false;
	public LocationMarker lastMarker = null;
	public float distance = -1.0f;

	public void ResetPathFindingValues()
	{
		visited = false;
		lastMarker = null;
		distance = -1.0f;
	}

	int IComparable.CompareTo ( object obj)
	{
		LocationMarker other = ( LocationMarker )obj;
		if ( distance < other.distance )
			return -1;
		if ( distance > other.distance )
         	return 1;
        else
        	return 0;
     }

	public void OnGUI()
	{
		/*if (connectedMarkers != null)
		{
			for (int i = 0; i < connectedMarkers.Count; i++)
			{
				if (connectedMarkers[i] != null)
				{
					bool connectedCheck = false;
					if (connectedMarkers[i].connectedMarkers != null)
					{
						for (int j = 0; j < connectedMarkers[i].connectedMarkers.Count; j++)
						{
							if (connectedMarkers[i].connectedMarkers[j] != null)
								if (connectedMarkers[i].connectedMarkers[j] == this)
									connectedCheck = true;
						}
					}
					else
					{
						connectedMarkers[i].connectedMarkers = new List<LocationMarker>();
					}

					if(!connectedCheck)
						connectedMarkers[i].connectedMarkers.Add(this);2
				}
			}
		}*/

	}

	public void OnDrawGizmos()
	{

		if (connectedMarkers != null)
		{
			for (int i = 0; i < connectedMarkers.Count; i++)
			{
				if (connectedMarkers[i] != null)
				{
					List<LocationMarker> otherMarkers = connectedMarkers[i].connectedMarkers;
					if (otherMarkers != null)
					{
						bool checkIfTwoWay = false;

						for (int j = 0; j < otherMarkers.Count; j++)
						{
							checkIfTwoWay = (otherMarkers[j] == this);
							if (checkIfTwoWay)
								break;
						}
						if (checkIfTwoWay)
						{
							Gizmos.color = Color.white;
							Gizmos.DrawLine(transform.position, connectedMarkers[i].transform.position);
						}
						else
						{
							Gizmos.color = Color.red;
							Gizmos.DrawLine(transform.position - Vector3.forward,
											connectedMarkers[i].transform.position - Vector3.forward);
						}
					}
				}
			}
		}
	}
}
