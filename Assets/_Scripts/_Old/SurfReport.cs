﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SurfReport : MonoBehaviour 
{
/*
	public Camera mainCamera = null;

	public GameObject reportPanelPrefab = null;
	public GameObject panelAnchorObject = null;

	private float panelHeight = 135.0f;
	private SurfReportPanel[] reportPanels;

	private Activity[] curActivities;

	private bool isInitialised = false;

	private bool scrollInputDown = false;
	private Vector3 lastPosition;

	private float minAnchorY;
	private float maxAnchorY;

	public Color panelColor1; 
	public Color panelColor2; 

	private Material panelColorMaterial1;
	private Material panelColorMaterial2;


	public void InitialisePanels()
	{
		if (!isInitialised)
		{
			if (mainCamera == null)
				mainCamera = Camera.main;

			MeshRenderer panelRenderer = reportPanelPrefab.GetComponent<SurfReportPanel>().backgroundMeshRenderer;
			panelColorMaterial1 = new Material( panelRenderer.sharedMaterial );
			panelColorMaterial1.color = panelColor1;

			panelColorMaterial2 = new Material( panelRenderer.sharedMaterial );
			panelColorMaterial2.color = panelColor2;

			isInitialised = true;
			curActivities = Scheduler.SharedInstance.GetActivities();

			if(curActivities == null || curActivities.Length == 0)
				return;
			

			List<Activity> activeActivities = new List<Activity>();

			//parse to cut out tutorials and unsurfy events
			foreach (Activity activity in curActivities)
			{
				bool addToListCheck = false;
				if (activity.Type == ActivityType.FreeSurf || activity.Type == ActivityType.CompetitionSurf)
					addToListCheck = true;

				if (activity.Type == ActivityType.Tutorial)
				{
					if (activity.GetActivityData<ActivityDataSurf>() != null)
						addToListCheck = true;	
					
				}
	
				if (addToListCheck)
					activeActivities.Add(activity);
			}

			curActivities = activeActivities.ToArray();
			reportPanels = new SurfReportPanel[curActivities.Length];

			if(curActivities.Length > 0)
			{
				for (int activityIndex  = 0; activityIndex < curActivities.Length; activityIndex++)
				{
					GameObject newPanelObject = GameObject.Instantiate(reportPanelPrefab);
					//place it where ever
					newPanelObject.transform.position = panelAnchorObject.transform.position + Vector3.down * panelHeight * (float)activityIndex;
					newPanelObject.transform.SetParent(panelAnchorObject.transform);

					reportPanels[activityIndex] = newPanelObject.GetComponent<SurfReportPanel>();
					ActivityDataSurf surfData = curActivities[activityIndex].GetActivityData<ActivityDataSurf>();

					if (activityIndex % 2 == 0)
						reportPanels[activityIndex].SetMaterial(panelColorMaterial1);
					else
						reportPanels[activityIndex].SetMaterial(panelColorMaterial2);

					//title
					reportPanels[activityIndex].panelTitle.text = LocationTitle.Get(curActivities[activityIndex].Location);

					//button
					reportPanels[activityIndex].panelButton.location = curActivities[activityIndex].Location;

					//subtitle
					if (curActivities[activityIndex].Type == ActivityType.CompetitionSurf)
					{
						reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.Comp);

						string subtitle = "";
						if (surfData.VehicleAtStart == Vehicle.Surfboard)
							subtitle = "Competition";
						else
							subtitle = VehicleTitle.Get(surfData.VehicleAtStart) + " Competition";
						
						reportPanels[activityIndex].panalSubtitle.text = subtitle;

						if (curActivities[activityIndex].Prize.Type == PrizeType.Money)
							reportPanels[activityIndex].panelExtraInfo.text = "Prize: $" + curActivities[activityIndex].Prize.Amount;
						else if (curActivities[activityIndex].Prize.Type == PrizeType.Vehicle)
							reportPanels[activityIndex].panelExtraInfo.text = "Prize: " + VehicleTitle.Get(curActivities[activityIndex].Prize.Vehicle);
						else
							reportPanels[activityIndex].panelExtraInfo.text = "";
					}
					else if (curActivities[activityIndex].Type == ActivityType.FreeSurf)
					{
						reportPanels[activityIndex].panalSubtitle.text = "Free Surf";
						if (surfData.WaveType == WaveType.Average)
							reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfSmall);
						if (surfData.WaveType == WaveType.Big)
							reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfBig);
						if (surfData.WaveType == WaveType.Average)
							reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfSmall);

						reportPanels[activityIndex].panelExtraInfo.text = "";
					}
					else if (curActivities[activityIndex].Type == ActivityType.Tutorial)
					{
						if (surfData != null)
						{
							reportPanels[activityIndex].panalSubtitle.text = "Free Surf";
							if (surfData.WaveType == WaveType.Average)
								reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfSmall);
							if (surfData.WaveType == WaveType.Big)
								reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfBig);
							if (surfData.WaveType == WaveType.Average)
								reportPanels[activityIndex].panelIcon.DrawFrame( (int)ActivityMarker.MarkerMode.FreeSurfSmall);
						}
					}

					//time remaining
					reportPanels[activityIndex].panelTimeRemaining.text = curActivities[activityIndex].ActivityCloseTimeFormattedString;
				}

				minAnchorY = panelAnchorObject.transform.position.y;
				maxAnchorY = minAnchorY + (minAnchorY - (reportPanels[ reportPanels.Length - 1].transform.position.y + 2 * panelHeight));

				if (maxAnchorY < minAnchorY)
					maxAnchorY = minAnchorY;
			}
		}
	}

	public void UpdateReportScrolling () 
	{
		if (isInitialised)
		{
			if (Input.GetMouseButton(0))
			{
				if (scrollInputDown)
				{
					Vector3 newPos = panelAnchorObject.transform.position;

					Vector3 inputPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);

					Vector3 direction = lastPosition - inputPos;
					float distance = direction.magnitude;

					newPos -= new Vector3(0, direction.y, 0);

					if (newPos.y > maxAnchorY)
						newPos.y = maxAnchorY;
					if (newPos.y <= minAnchorY)
						newPos.y = minAnchorY;


					panelAnchorObject.transform.position = newPos;
					lastPosition = inputPos;
				}
				else
				{
					scrollInputDown = true;
					lastPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
				}
			}
			else
				scrollInputDown = false;
		}
	}

	void Update()
	{
		if (isInitialised)
		{
			for (int i = 0; i < curActivities.Length; i++)
			{
				reportPanels[i].panelTimeRemaining.text = curActivities[i].ActivityCloseTimeFormattedString;
			}
		}
	}
	*/
}
