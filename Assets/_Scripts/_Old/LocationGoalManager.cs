﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

public class LocationGoalManager
{

	public enum LocationGoal
	{
		None,
		Distance,
		GrindNumberOfPiers,
		DodgeNumberOfBirds,
		JetskiDistance,
		BodyBoardDistance,
		TubeDistance,
		BoatDistance,
		LongBoardDistance,
	}

	class Goal
	{
		public Goal(LocationGoal goal, int distance)
		{
			LocationGoal = goal;
			Distance = distance;
		}

		public LocationGoal LocationGoal;
		public int Distance;
	}

	public delegate void GoalTextChanged(string text);
	public GoalTextChanged OnGoalTextChanged;

	public delegate void GoalCompleted();
	public GoalCompleted OnGoalCompleted;



	int currentLocationNumber;
	Goal currentGoal = new Goal(LocationGoal.None, 0);
	int lastPowerUpChangeCounter = 0;
	int goalCounter = 0;
	int goalCount = 0;
	bool goalCompleted = false;
	int? previousPierID = null;


	List<Goal> easyGoals = new List<Goal>()
	{
		new Goal(LocationGoal.Distance, 30),
		new Goal(LocationGoal.Distance, 50),
		new Goal(LocationGoal.Distance, 50),
		new Goal(LocationGoal.Distance, 30),
		new Goal(LocationGoal.GrindNumberOfPiers, 3),
		//new Goal(LocationGoal.JetskiDistance, 10),
		//new Goal(LocationGoal.BoatDistance, 15),
		//new Goal(LocationGoal.BodyBoardDistance, 30),
		new Goal(LocationGoal.GrindNumberOfPiers, 7),
		//new Goal(LocationGoal.TubeDistance, 40),
		new Goal(LocationGoal.Distance, 50),
		new Goal(LocationGoal.Distance, 100),
		//new Goal(LocationGoal.JetskiDistance, 30),
		//new Goal(LocationGoal.BodyBoardDistance, 50),
		new Goal(LocationGoal.Distance, 500),
		new Goal(LocationGoal.Distance, 1000)
	};


	List<Goal> mediumGoals = new List<Goal>()
	{
		new Goal(LocationGoal.Distance, 50),
		new Goal(LocationGoal.GrindNumberOfPiers, 7),
		//new Goal(LocationGoal.BoatDistance, 10),
		//new Goal(LocationGoal.JetskiDistance, 15),
		//new Goal(LocationGoal.BodyBoardDistance, 20),
		//new Goal(LocationGoal.TubeDistance, 30),
		new Goal(LocationGoal.Distance, 100),
		new Goal(LocationGoal.GrindNumberOfPiers, 15),
		//new Goal(LocationGoal.JetskiDistance, 30),
		//new Goal(LocationGoal.BoatDistance, 50),
		//new Goal(LocationGoal.BodyBoardDistance, 75),
		new Goal(LocationGoal.Distance, 500),
		new Goal(LocationGoal.Distance, 1000)
	};


	public LocationGoalManager(int locationNumber)
	{
		currentLocationNumber = locationNumber;

		InitialiseGoal(locationNumber);
		UpdateGoalText();
	}

	public void TrackDistance(int currentDistance, Vehicle currentPowerUp)
	{
		if(goalCompleted)
			return;

		if(currentGoal.LocationGoal == LocationGoal.Distance)
		{
			goalCounter = currentDistance;
			if(goalCounter >= goalCount)
			{
				goalCounter = goalCount;
				MarkGoalCompleted(currentGoal);

			}
		}
		else if(
			(currentGoal.LocationGoal == LocationGoal.JetskiDistance && currentPowerUp == Vehicle.Jetski) ||
			(currentGoal.LocationGoal == LocationGoal.BodyBoardDistance && currentPowerUp == Vehicle.Bodyboard) ||
			(currentGoal.LocationGoal == LocationGoal.TubeDistance && currentPowerUp == Vehicle.Tube) ||
			(currentGoal.LocationGoal == LocationGoal.BoatDistance && currentPowerUp == Vehicle.Boat) ||
			(currentGoal.LocationGoal == LocationGoal.LongBoardDistance && currentPowerUp == Vehicle.Longboard)
		)
		{
			if(lastPowerUpChangeCounter != currentDistance)
			{
				lastPowerUpChangeCounter = currentDistance;
				goalCounter += 1;

				if(goalCounter >= goalCount)
				{
					goalCounter = goalCount;
					MarkGoalCompleted(currentGoal);
				}
			}
		}


		UpdateGoalText();
	}

	public void TrackPierGrind(int pierID)
	{
		if(goalCompleted)
			return;
		
		if(currentGoal.LocationGoal == LocationGoal.GrindNumberOfPiers && (!previousPierID.HasValue || previousPierID.Value != pierID))
		{
			previousPierID = pierID;
			goalCounter += 1;
			if(goalCounter >= goalCount)
			{
				goalCounter = goalCount;
				MarkGoalCompleted(currentGoal);
			}
		}

		UpdateGoalText();
	}


	public void UpdateGoalText()
	{
		string goalText = "";

		if(currentGoal.LocationGoal == LocationGoal.Distance)
			goalText = string.Format("REACH : {0}M", goalCount);
		else if(currentGoal.LocationGoal == LocationGoal.GrindNumberOfPiers)
			goalText = string.Format("GRIND PIERS: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.DodgeNumberOfBirds)
			goalText = string.Format("DODGE BIRDS: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.JetskiDistance)
			goalText = string.Format("JETSKI: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.BodyBoardDistance)
			goalText = string.Format("BODYBOARD: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.TubeDistance)
			goalText = string.Format("TUBE: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.BoatDistance)
			goalText = string.Format("SPEEDBOAT: {1} of {0}", goalCount, goalCounter);
		else if(currentGoal.LocationGoal == LocationGoal.LongBoardDistance)
			goalText = string.Format("LONGBOARD: {1} of {0}", goalCount, goalCounter);

		if(OnGoalTextChanged != null)
		{
			if(goalCompleted)
				goalText = string.Format("{0} ✔", goalText);
					
			OnGoalTextChanged(goalText);
		}
	}


	void InitialiseGoal(int locationNumber)
	{

		currentGoal.LocationGoal = LocationGoal.None;

		if(currentLocationNumber == 1 || currentLocationNumber == 4)
		{
			// Easy
			foreach(var goal in easyGoals)
			{
				if(!CompletedGoal(goal))
				{
					currentGoal = goal;
					goalCount = goal.Distance;
					break;
				}
			}

		}
		else
		{
			// Medium use medium as default set for now
			foreach(var goal in mediumGoals)
			{
				if(!CompletedGoal(goal))
				{
					currentGoal = goal;
					goalCount = goal.Distance;
					break;
				}
			}
		}
	}

	void MarkGoalCompleted(Goal goal)
	{
		goalCompleted = true;
		string goalID = string.Format("PrefLoc{0}GoalID{1}GoalCount{2}", currentLocationNumber, (int)goal.LocationGoal, goal.Distance);
		PlayerPrefs.SetInt(goalID, 1);
		//TestSettings.levelCounter++;
		Application.LoadLevel(0);

	}

	bool CompletedGoal(Goal goal)
	{
		string goalID = string.Format("PrefLoc{0}GoalID{1}GoalCount{2}", currentLocationNumber, (int)goal.LocationGoal, goal.Distance);
		if(PlayerPrefs.HasKey(goalID))
		{
			return true;
		}
			
		return false;
	}
}
