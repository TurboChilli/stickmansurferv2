﻿using UnityEngine;
using System.Collections;

public enum Trophy
{	
	None,
	MagazineCover,
	Count
};
/*
public class TrophyManager : MonoBehaviour 
{
	public bool isRevealing = false;

	public GameObject[] trophyObjects = null;
	public GameObject[] trophyRestingPlaces = null;

	private GameObject showingTrophy = null;
	private Vector3 origScale;
	private Quaternion origRotation;

	public Vector3 activePosition;
	public Vector3 inactivePosition = new Vector3(-1000, 0, 0);

	void Start () 
	{
		bool[] trophyUnlocks = GameState.SharedInstance.CheckAllUnlockedTrophies();

		for (int trophyIndex = 0; trophyIndex < trophyObjects.Length; trophyIndex++)
		{
			if (trophyUnlocks[trophyIndex] && trophyRestingPlaces != null)
				trophyObjects[trophyIndex].transform.position = trophyRestingPlaces[trophyIndex].transform.position;
			else
			{
				trophyObjects[trophyIndex].transform.position = inactivePosition;
				trophyObjects[trophyIndex].SetActive(false);
			}
		}
	}

	public void RevealTrophy(Trophy trophy)
	{
		int trophyIndex = (int)trophy;
		GameState.SharedInstance.UnlockTrophy(trophy);

		showingTrophy = trophyObjects[trophyIndex];
		showingTrophy.SetActive(true);
		
	}

	public void RecieveInput()
	{

	}

	void Update () {
	
	}

	private enum ShowingState
	{
		MovingIn,
		WaitingInput,
		MovingOut
	}

}
*/

