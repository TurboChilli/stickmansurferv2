﻿using UnityEngine;
using System.Collections;
using TMPro;
using Prime31;
using UnityEngine.UI;

public class LoginRegisterManager : MonoBehaviour 
{

    public TMP_Text titleText;
    public TMP_Text buttonText;
    public GameObject loginButton;
    public GameObject loginFacebookButton;
    public GameObject forgotPasswordButton;
    public GameObject contactingServerOverlay;
    public GameObject closeButton;
    public AudioSource soundClick;
    public Camera mainCamera;
    public InputField emailInputField;
    public InputField passwordInputField;
    public SettingsDialog settingsDialog;

    bool contactingServer = false;
    float contactServerTimer = 0;
    bool registerMode = false;

    bool showNextFrame = false;
    int showNextFrameCounter = 0;

    private ServerSavedGameStateSync serverSync;

    public enum DialogPanelState
    {
        Hidden,
        MovingIn,
        Visible,
    }

    DialogPanelState state = DialogPanelState.Hidden;
    public DialogPanelState State
    {
        get { return state; }
        private set { state = value; }
    }

    void OnEnable()
    {
        serverSync = FindObjectOfType<ServerSavedGameStateSync>();
        if (serverSync != null)
        {
            serverSync.OnLoginWithEmailFailed += LgoinWithEmailFailed;
            serverSync.OnLoginWithEmailSuccess += LoginWithEmailSuccess;
            serverSync.OnRegisterWithEmailFailed += RegistrationFailed;
            serverSync.OnRegisterWithEmailSuccess += RegistrationSuccess;
        }
    }


    void OnDisable()
    {
        if (serverSync != null)
        {
            serverSync.OnLoginWithEmailFailed -= LgoinWithEmailFailed;
            serverSync.OnLoginWithEmailSuccess -= LoginWithEmailSuccess;
            serverSync.OnRegisterWithEmailFailed -= RegistrationFailed;
            serverSync.OnRegisterWithEmailSuccess -= RegistrationSuccess;
        }
    }

    void RegistrationFailed(string message)
    {
        //Debug.Log("CALLBACK RegFailed:" + message);
        contactingServerOverlay.SetActive(false);
        contactingServer = false;

		AlertDialogUtil.ShowAlert("Failed", message, "OK");
    }

    void RegistrationSuccess()
    {
        //Debug.Log("CALLBACK RegSuccess:");
        contactingServerOverlay.SetActive(false);
        contactingServer = false;

		AlertDialogUtil.ShowAlert("Success", "Logged in", "OK");

        Hide();
    }


    void LgoinWithEmailFailed(string message)
    {
        Debug.Log("CALLBACK LoginFailed:" + message);
        contactingServerOverlay.SetActive(false);
        contactingServer = false;

		AlertDialogUtil.ShowAlert("Failed", message, "OK");
    }

    void LoginWithEmailSuccess()
    {
        Debug.Log("CALLBACK LoginSuccess:");
        contactingServerOverlay.SetActive(false);
        contactingServer = false;

        AlertDialogUtil.ShowAlert("Success", "Logged in", "OK");

        Hide();
    }

	// Use this for initialization
	void Start () {
	
        PopulateUILayout();
        contactingServerOverlay.SetActive(false);

        if (Utils.IphoneXCheck())
        {
            transform.localScale *= 0.8f;
        }

	}
	
    void CheckAndroidBackButton()
    {
        #if UNITY_ANDROID || UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(State == DialogPanelState.Visible)
            {
                Hide();
            }
        }
        #endif 
    }

	// Update is called once per frame
	void Update () 
    {

        CheckAndroidBackButton();

        if (disableButtons)
        {
            disableButtonsTimer += Time.deltaTime;
            if (disableButtonsTimer > disableButtonsDelay)
            {
                disableButtonsTimer = 0;
                disableButtons = false;

            }
        }

        if (contactingServer)
        {
            contactServerTimer += Time.deltaTime;
            if (contactServerTimer >= GameServerSettings.SharedInstance.SurferSettings.ContactServerLoginTimeout)
            {
                contactServerTimer = 0;
                contactingServer = false;
                contactingServerOverlay.SetActive(false);

				AlertDialogUtil.ShowAlert("Failed", "Unable to contact server. Please try again later.", "OK");
            }
        }
        if (Input.GetMouseButtonDown(0) && (!disableButtons))
        {
            if (UIHelpers.CheckButtonHit(closeButton.transform, mainCamera))
            {
                // Close the dialog
                Hide();
                GlobalSettings.PlaySound(soundClick);
            }
            else if (UIHelpers.CheckButtonHit(loginButton.transform, mainCamera))
            {
                //Debug.Log("HIT LOGIN BUTTON");
                if (ValidateInputFields())
                { 
                    contactingServerOverlay.SetActive(true);
                    GlobalSettings.PlaySound(soundClick);
                    contactServerTimer = 0;
                    contactingServer = true;
                    // validated ok
                    if (registerMode)
                    {
                        if (serverSync != null)
                        {
                            //Debug.Log("CALLED REGISTER HERE");
                            serverSync.RegisterWithEmailAndPassword(emailInputField.text.Trim(), passwordInputField.text.Trim());
                        }
                    }
                    else
                    {
                        // login here
                        if (serverSync != null)
                        {
                            //Debug.Log("CALLED LOGIN HERE");
                            serverSync.LoginWithEmailAndPassword(emailInputField.text.Trim(), passwordInputField.text.Trim());
                        }
                    }

                }
            }

            else if (UIHelpers.CheckButtonHit(this.loginFacebookButton.transform, mainCamera))
            {
                FacebookManager.AttemptLogin();
            }

            else if (UIHelpers.CheckButtonHit(forgotPasswordButton.transform, mainCamera))
            {
				AlertDialogUtil.ShowAlert("Contact Us", "Send email to contact@turbochilli.com to reset password", "OK");
            }
        }
        if (State == DialogPanelState.Visible)
        {
            
            if (ServerSavedGameStateSync.IsLoggedIn() && !string.IsNullOrEmpty(FacebookManager.FacebookUserId))
            {
                // its logged into facebook so hide this form
                Hide();
            }
        }
	}

    bool ValidateInputFields()
    {
        if (!ValidateEmailAddress(emailInputField.text))
        {
            // Missing email field
            //Debug.Log("INVALID EMAIL FIELD");

			AlertDialogUtil.ShowAlert("Invalid Email", "You must enter a valid email", "OK");

            return false;
        }
        else if (!ValidatePassword(passwordInputField.text))
        {
            // Missing password field
            //Debug.Log("INVALID PASSWORD FIELD");
			AlertDialogUtil.ShowAlert("Invalid Password", "You must enter a password at least 5 characters long", "OK");

            return false;
        }

        return true;
    }

    bool ValidatePassword(string password)
    {
        if (string.IsNullOrEmpty(password))
        {
            return false;
        }

        if (password.Trim().Equals(""))
        {
            return false;
        }

        if (password.Length < 5)
        {
            return false;
        }

        return true;
    }

    bool ValidateEmailAddress(string address)
    {
        if (string.IsNullOrEmpty(address))
        {
            return false;
        }
         
        if (address.Trim().Equals(""))
        {
            return false;
        }
            
        if(address.IndexOf("@",  System.StringComparison.InvariantCultureIgnoreCase) <= 0)
        {
            return false;
        }

        if(address.IndexOf(".",  System.StringComparison.InvariantCultureIgnoreCase) <= 0)
        {
            return false;
        }

        if (address.Length < 6)
        {
            return false;
        }
        return true;
    }


    void PopulateUILayout()
    {
        if (registerMode)
        {
            titleText.text = GameLanguage.LocalizedText("REGISTER USER");
            buttonText.text = GameLanguage.LocalizedText("REGISTER");
            forgotPasswordButton.SetActive(false);
        }
        else
        {
            titleText.text = GameLanguage.LocalizedText("LOGIN USER");
            buttonText.text = GameLanguage.LocalizedText("LOGIN");
            forgotPasswordButton.SetActive(true);
        }
    }

    public void Hide()
    {
        var pos = this.transform.position;
        pos.x = -99999f;
        this.transform.position = pos;


        State = DialogPanelState.Hidden;
        settingsDialog.Show();
    }

    bool disableButtons = false;
    float disableButtonsTimer = 0;
    float disableButtonsDelay = 0.1f;

    public void Show(bool registerView = false)
    {
        registerMode = registerView;
        PopulateUILayout();
        //dialogContents.transform.localPosition = dialogContentsStartPos;
        disableButtons = true;
        UIHelpers.SetTransformToCenter(this.transform, mainCamera);

        State = DialogPanelState.Visible;
    }
}
