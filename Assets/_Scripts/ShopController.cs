﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ShopController : MonoBehaviour {

	[Header("Controller Properties")]
	public Camera mainCamera;
	public GameObject topMenuBar;
	public CoinBar coinBar;
	public GameObject closeButton;
	public EnergyDrinkDialog notEnoughGoldDialogPanel;
	public NumericDisplayBar energyDrinkBar;
	public TextMeshPro multiplierText;
	public EnergyDrinkShopController energyDrinkShopController;

	public GameObject purchaseOverlay;
	public Material purchaseOverlayMaterial;
	private Color origOverlayColor;
	public GameObject purchaseSuccessfulPanel;

	public SpotlightEffect spotlightEffect;
	public GameObject highlightedButton;
	public GameObject highlightedButtonHand;
	public RotationClickableJiggle highlightedButtonJiggle;

	public GameObject closeButtonHand;
	public Material shopUpgradeItemPanelLevelRequiredMaterial;
	public AnimatingObject jeepCurrentlySelectedHeaderIcon;
	public GameObject shopScrollingIndicator;

	private bool isShowingPanel = false;
	private bool isPausingPanel = false;
	private bool isMovingPanel = false;
	private bool isMovingPanelBack = false;

	private float curPanelTimer = 0.0f;
	private float moveTime = 0.15f;
	private float pauseTime = 0.65f;
	private Vector3 inactivePanelPosition;
	private Vector3 activePanelPosition;

    public AudioSource reggaeMusic;
    public AudioSource waveSounds;
    public AudioSource swooshSound;

    public AudioSource soundClick;

	enum ScrollDirection
	{
		None,
		Up,
		Down
	}
		
	float scrollMinY = -11755; 
	float scrollMaxY = -22;
	float mainCameraScrollDistance;

	float scrollIndicatorMaxY;
	float scrollIndicatorMinY;
	float scrollIndicatorDistance;

	public bool hasScrolled = false;
	private float scrollThreshold = 1.0f;
	Vector3 lastPositionWorldPos;
	Vector3 startScrollWorldPos;
	ScrollDirection scrollDirection = ScrollDirection.None;
	float scrollDistanceY;

	public void RefreshCoinAndEnergyBars()
	{
		coinBar.DisplayCoins(GameState.SharedInstance.Cash);
		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
	}

	void OnEnable ()
	{
		notEnoughGoldDialogPanel.OnEnergyDrinkSpendSuccessful += EnergyDrinkSpendSuccessful;
		energyDrinkShopController.OnClose += EnergyDrinkShopControllerCloseHandler;
	}

	void OnDisable ()
	{
		notEnoughGoldDialogPanel.OnEnergyDrinkSpendSuccessful -= EnergyDrinkSpendSuccessful;
		energyDrinkShopController.OnClose -= EnergyDrinkShopControllerCloseHandler;
	}

	void Start () 
	{
		TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();

		if (GlobalSettings.musicEnabled)
			GlobalSettings.PlayMusicFromTimePoint(reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
	    else 
	    {
			reggaeMusic.Stop();
			waveSounds.Stop();
	    }	

		SetCurrentlySelectedJeepHeaderIcon();

		activePanelPosition = Vector3.zero + Vector3.forward * purchaseSuccessfulPanel.transform.localPosition.z;
		inactivePanelPosition = activePanelPosition + Vector3.right * 2000.0f + Vector3.back * 0.01f;
		purchaseOverlayMaterial = purchaseOverlay.GetComponent<MeshRenderer>().material;
		purchaseSuccessfulPanel.transform.localPosition = inactivePanelPosition;
		origOverlayColor = purchaseOverlayMaterial.color;

		Color transparentColor = origOverlayColor;
		transparentColor.a = 0;
		purchaseOverlayMaterial.color = transparentColor;

		if(energyDrinkShopController == null)
			energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();
		
		coinBar.DisplayCoins(GameState.SharedInstance.Cash);
		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
		multiplierText.text = string.Format("<size=-26>x</size>{0}", GameState.SharedInstance.Multiplier);

		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP, 0f, 30f, topMenuBar.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 100f, 46f, coinBar.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 300f, 46f, energyDrinkBar.transform, mainCamera);

		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 30f, 46f, multiplierText.gameObject.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 70f, 46f, closeButton.transform, mainCamera);

		// Scroll indicator pos
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 14f, 116f, shopScrollingIndicator.transform, mainCamera);
		scrollIndicatorMaxY = shopScrollingIndicator.transform.position.y;
		scrollIndicatorMinY = mainCamera.ScreenToWorldPoint(new Vector3(mainCamera.pixelWidth - 10f, 14f, shopScrollingIndicator.transform.position.z)).y;

		scrollIndicatorDistance = scrollIndicatorMaxY - scrollIndicatorMinY;
		mainCameraScrollDistance = scrollMaxY - scrollMinY;

		if (GameState.SharedInstance.TutorialStage == TutStageType.FirstShopVisit || GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChestClose)
		{
			if (spotlightEffect == null)
				spotlightEffect = FindObjectOfType<SpotlightEffect>();
			
			#if UNITY_ANDROID
			if (spotlightEffect != null)
			{
				spotlightEffect.gameObject.SetActive(false);
			}
			#else
			spotlightEffect.gameObject.SetActive(true);
			spotlightEffect.marker.transform.position = highlightedButton.transform.position;
			spotlightEffect.ToggleEffect(true, 100.0f, true);
			#endif

			highlightedButtonHand.SetActive(true);
			highlightedButtonJiggle.enabled = true;

			closeButtonHand.SetActive(false);
		}
		else
		{
			spotlightEffect.gameObject.SetActive(false);
			highlightedButtonHand.SetActive(false);
			highlightedButtonJiggle.enabled = false;

			closeButtonHand.SetActive(false);
		}

		energyDrinkShopController.Hide();
	}

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape) && DialogTrackingFlags.CommonDialogShowing() == false)
		{
            #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
            #endif
            soundClick.Play();
			SceneNavigator.NavigateTo(SceneType.Menu);
		}
		#endif 
	}

	void Update () 
	{
		CheckAndroidBackButton();

		TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();

		if (isShowingPanel)
		{
			if (isMovingPanel)
			{
				if (curPanelTimer >= 0)
				{
					float lerpAmount = 1.0f - Mathf.Pow( (curPanelTimer / moveTime), 3.0f);
					purchaseSuccessfulPanel.transform.localPosition = Vector3.Lerp(inactivePanelPosition, activePanelPosition, lerpAmount);
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a * lerpAmount);
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					purchaseSuccessfulPanel.transform.localPosition = activePanelPosition;
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a);

					curPanelTimer = pauseTime;
					isMovingPanel = false;
					isPausingPanel = true;
				}
			}
			else if (isPausingPanel)
			{
				if (curPanelTimer >= 0 && !Input.GetMouseButton(0))
				{
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					curPanelTimer = moveTime;
					isPausingPanel = false;
					isMovingPanelBack = true;

					GlobalSettings.PlaySound(swooshSound);
				}
			}
			else if (isMovingPanelBack)
			{
				if (curPanelTimer >= 0)
				{
					float lerpAmount = Mathf.Pow(1.0f - (curPanelTimer / moveTime), 3.0f);
					purchaseSuccessfulPanel.transform.localPosition = Vector3.Lerp(-inactivePanelPosition, activePanelPosition, 1.0f - lerpAmount);
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, origOverlayColor.a * (1.0f - lerpAmount));
					curPanelTimer -= Time.deltaTime;
				}
				else
				{
					purchaseSuccessfulPanel.transform.localPosition = inactivePanelPosition;
					purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);

					curPanelTimer = pauseTime;
					isMovingPanelBack = false;
					isShowingPanel = false;
				}			
			}
		}

		if(!notEnoughGoldDialogPanel.isShowing)
		{
			if (highlightedButtonHand.gameObject.activeSelf == false)
				UpdateScrolling();

			HandleButtonClick();
		}
	}

	void HandleButtonClick()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(UIHelpers.CheckButtonHit(closeButton.transform, mainCamera))
			{
                GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
                soundClick.Play();

                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif

				SceneNavigator.NavigateTo(SceneType.Menu);
			}
		}
	}
        

	void UpdateScrolling()
	{
		if( Input.GetMouseButtonDown(0))
		{
			hasScrolled = false;
		
			scrollDirection = ScrollDirection.None;
			scrollDistanceY = 0f;
			startScrollWorldPos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.nearClipPlane));
		}
		else if (Input.GetMouseButtonUp(0))
		{
			Vector3 stopScrollWorldPos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.nearClipPlane));

			scrollDirection = (startScrollWorldPos.y < stopScrollWorldPos.y) ? ScrollDirection.Up : ScrollDirection.Down;
			scrollDistanceY = Mathf.Abs(startScrollWorldPos.y - stopScrollWorldPos.y);

		}
		else if(Input.GetMouseButton(0))
		{
			Vector3 stopScrollWorldPos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.nearClipPlane));

			scrollDirection = (startScrollWorldPos.y < stopScrollWorldPos.y) ? ScrollDirection.Up : ScrollDirection.Down;
			scrollDistanceY = Mathf.Abs(startScrollWorldPos.y - stopScrollWorldPos.y);

			if (scrollDistanceY > scrollThreshold)
				hasScrolled = true;
		}
			
		if(scrollDirection != ScrollDirection.None && scrollDistanceY > 0)
		{
			Vector3 camPos = mainCamera.transform.position;
			float scrollForce = (scrollDistanceY * Time.deltaTime) * 15f;

			if(scrollDirection == ScrollDirection.Down)
				camPos.y += scrollForce;
			else if(scrollDirection == ScrollDirection.Up)
				camPos.y -= scrollForce;

			scrollDistanceY -= ((scrollDistanceY * Time.deltaTime) * 1.25f);

			UpdateCameraPos(camPos);

			if(ApplyCameraBounds() || scrollDistanceY <= 0)
			{
				scrollDirection = ScrollDirection.None;
				scrollDistanceY = 0;
			}
		}

	}

	void UpdateCameraPos(Vector3 pos)
	{
		mainCamera.transform.position = pos;
		UpdateScrollIndicatorPos();
	}

	void UpdateScrollIndicatorPos()
	{
		float cameraScrollPercent = (scrollMaxY - mainCamera.transform.position.y)/mainCameraScrollDistance;

		if(cameraScrollPercent >= 0f || cameraScrollPercent <= 1f)
		{
			float scrollIndicatorYPos = scrollIndicatorMaxY - (scrollIndicatorDistance * cameraScrollPercent);
			Vector3 scrollIndicatorPos = shopScrollingIndicator.transform.localPosition;
			scrollIndicatorPos.y = scrollIndicatorYPos;
			shopScrollingIndicator.transform.localPosition = scrollIndicatorPos;
		}
	}

	bool ApplyCameraBounds()
	{
		if(mainCamera.transform.position.y < scrollMinY)
		{
			Vector3 pos = new Vector3(mainCamera.transform.position.x, scrollMinY, mainCamera.transform.position.z);
			UpdateCameraPos(pos);

			return true;
		}
		else if(mainCamera.transform.position.y > scrollMaxY)
		{
			Vector3 pos = new Vector3(mainCamera.transform.position.x, scrollMaxY, mainCamera.transform.position.z);
			UpdateCameraPos(pos);

			return true;
		}

		return false;
	}

	public void EnactMulitplierEffect()
	{
		//GameState.SharedInstance.AddMultiplier(1);
		multiplierText.text = string.Format("<size=-26>x</size>{0}", GameState.SharedInstance.Multiplier);
	}

	public void ResetAllCosmeticEquipButtons()
	{
		ShopUpgradeItem[] shopUpgradeItems = FindObjectsOfType<ShopUpgradeItem>();
		if(shopUpgradeItems != null)
		{
			foreach(ShopUpgradeItem item in shopUpgradeItems)
			{
				item.ResetCosmeticEquipButton();	
			}
		}
	}


	public void ResetAllSurfBoardEquipButtons()
	{
		MarkThrusterboardTutorialComplete();

		ShopUpgradeItem[] shopUpgradeItems = FindObjectsOfType<ShopUpgradeItem>();
		if(shopUpgradeItems != null)
		{
			foreach(ShopUpgradeItem item in shopUpgradeItems)
			{
				item.ResetSurfBoardEquipButton();	
			}
		}
	}

	public void ResetAllJeepEquipButtons()
	{
		ShopUpgradeItem[] shopUpgradeItems = FindObjectsOfType<ShopUpgradeItem>();
		if(shopUpgradeItems != null)
		{
			foreach(ShopUpgradeItem item in shopUpgradeItems)
			{
				item.ResetJeepEquipButton();	
			}
		}
	}

	public void SetCurrentlySelectedJeepHeaderIcon()
	{
		if(jeepCurrentlySelectedHeaderIcon != null)
		{
			if(GameState.SharedInstance.CurrentJeep == JeepType.Black)
				jeepCurrentlySelectedHeaderIcon.DrawFrame(7);
			else
				jeepCurrentlySelectedHeaderIcon.DrawFrame(3);
		}
	}

	public void ShowPurchaseSuccessful()
	{
		isShowingPanel = true;
		isMovingPanel = true;
		isPausingPanel = false;
		isMovingPanelBack = false;
		curPanelTimer = moveTime;

		GlobalSettings.PlaySound(swooshSound);

		purchaseSuccessfulPanel.transform.localPosition = inactivePanelPosition;
		purchaseOverlay.transform.localPosition = activePanelPosition;
		purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);

		MarkThrusterboardTutorialComplete();
	}

	void MarkThrusterboardTutorialComplete()
	{
		if (GameState.SharedInstance.TutorialStage == TutStageType.FirstShopVisit || GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChestClose)
		{
			GameState.SharedInstance.TutorialStage = TutStageType.PostShopVisit;

			closeButtonHand.SetActive(true);

			spotlightEffect.gameObject.SetActive(false);
			highlightedButtonHand.SetActive(false);
			highlightedButtonJiggle.enabled = false;
		}
	}

	public void HidePurchaseSuccessful()
	{
		isShowingPanel = false;
		isMovingPanel = false;
		isPausingPanel = false;
		isMovingPanelBack = false;
		curPanelTimer = moveTime;

		purchaseSuccessfulPanel.transform.localPosition = inactivePanelPosition;
		purchaseOverlay.transform.localPosition = inactivePanelPosition;
		purchaseOverlayMaterial.color = new Color(origOverlayColor.r, origOverlayColor.g, origOverlayColor.b, 0.0f);
	}
		
	void EnergyDrinkSpendSuccessful(int energyDrinksUsed)
	{
		RefreshCoinAndEnergyBars();
	}

	void EnergyDrinkShopControllerCloseHandler ()
	{
		RefreshCoinAndEnergyBars();
	}
}

public enum PreviousNavigation
{
	None,
	Retry,
	GoalSkip,
	ShopCosmetic,
	ShopUpgrade,
	Manual
}
