﻿using UnityEngine;
using System.Collections;
using TMPro;

public class CoinBar : MonoBehaviour {

	public TextMesh coinText = null;
	public int coinTextMaxResizeLength = 7;

	public int curCoins = 0;

	private float timeToCount = 1.0f;
	private float curTimeToCount = 0.0f;

	private bool isCounting = false;
	private int prevCoins = 0;

	Vector3 coinTextLocalScale = Vector3.zero;

	void Start()
	{
		coinTextLocalScale = coinText.transform.localScale;
	}

	public void DisplayCoins(int coins, bool isAnimated = false)
	{
		if (isAnimated)
		{
			prevCoins = curCoins;
			curCoins = coins;

			isCounting = true;
			curTimeToCount = timeToCount;
		}
		else
		{
			prevCoins = curCoins;
			curCoins = coins;

			isCounting = false;
			SetCoinText(ShortenCoins(curCoins));
		}
	}

	float lastCoinTextScale = 1.0f;

	void SetCoinText(string value)
	{
		float scale = GlobalSettings.ScaleTextAfterLength(value, coinTextMaxResizeLength);
		if(lastCoinTextScale != scale)
		{
			coinText.transform.localScale = new Vector3(coinTextLocalScale.x * scale, coinTextLocalScale.y * scale, coinTextLocalScale.z);
			lastCoinTextScale = scale;
		}

		coinText.text = value;
	}

	public void Reset()
	{
		curCoins = 0;
		prevCoins = 0;
		DisplayCoins(0);
	}

	public void Update()
	{
		if (isCounting)
		{
			float lerpAmount = curTimeToCount / timeToCount;
			lerpAmount = 1.0f - Mathf.Pow(lerpAmount, 2.0f);

			int newCoin = Mathf.CeilToInt(Mathf.Lerp((float)prevCoins, (float)curCoins, lerpAmount));

			curTimeToCount -= Time.deltaTime;
			if (curTimeToCount <= 0)
			{
				newCoin = curCoins;
				isCounting = false;
			}

			SetCoinText(ShortenCoins(newCoin));
		}
	}

	public static string ShortenCoins(int coins)
	{
		return string.Format("{0:#,0}", coins);
	}

}
