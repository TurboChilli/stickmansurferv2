﻿using UnityEngine;
using System.Collections;
using TMPro;

public class EnergyDrinkPack : MonoBehaviour 
{
	public Transform PackButton;
	public TextMesh PackAmountText;
	public TextMesh PackPriceText;

	public GameObject BestValueBanner;
	public TMP_Text BestValueBannerText;

	public GameObject BonusPercent;
	public GameObject BonusPercentRedTag;
	public GameObject BonusPercentGreenTag;
	public TextMesh BonusPercentText;

	public string PackId { get; private set; }
	public int CalculatedBonusPercent { get; private set; }
	public bool IsBasePack { get; private set; }

	public int PackAmount { get; private set; }
	float packPrice;

	int basePackAmount;
	float basePackPrice;

	public void Initialise (string packId, int packAmount, float packPrice, int basePackAmount, float basePackPrice) 
	{
		PackId = packId.ToUpperInvariant();

		#if UNITY_ANDROID
		PackId = packId.ToLowerInvariant();
		#endif 

		this.PackAmount = packAmount;

		this.packPrice = packPrice;
		PackPriceText.text = this.packPrice <= 0 ? "" : packPrice.ToString("$0.##");

		this.basePackAmount = basePackAmount;
		this.basePackPrice = basePackPrice;

		PackAmountText.text = string.Format("{0:n0}", packAmount);
		BestValueBanner.SetActive(false);

		if(this.PackAmount == this.basePackAmount && this.packPrice == this.basePackPrice)
		{
			IsBasePack = true;
			BonusPercent.SetActive(false);
		}
		else
		{
			float priceMultipleOfBasePack = packPrice/basePackPrice;
			float packAmountBasedOnBasePackPrice = ((float)basePackAmount * priceMultipleOfBasePack);
			float bonusPackAmount = packAmount - packAmountBasedOnBasePackPrice;
			CalculatedBonusPercent = Mathf.FloorToInt((bonusPackAmount/packAmountBasedOnBasePackPrice) * 100);

			if(CalculatedBonusPercent > 0)
			{
				BonusPercent.SetActive(true);
				BonusPercentText.text = string.Format("{0}%", CalculatedBonusPercent);
				ShowRedBonusPercentTag();
			}
			else
			{
				BonusPercent.SetActive(false);
			}
		}
	}

	public void ShowRedBonusPercentTag()
	{
		BonusPercentGreenTag.SetActive(false);
		BonusPercentRedTag.SetActive(true);
		BonusPercent.SetActive(true);
	}

	public void ShowGreenBonusPercentTag()
	{
		BonusPercentRedTag.SetActive(false);
		BonusPercentGreenTag.SetActive(true);
		BonusPercent.SetActive(true);
	}

	public void ShowBestValueBanner()
	{
		BestValueBanner.SetActive(true);
	}

	public void SetBestValueBannerText(string text)
	{
		BestValueBannerText.text = text;
	}

}
