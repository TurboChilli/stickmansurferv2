﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

using Prime31;

public class FacebookManager : MonoBehaviour 
{
	public static string FacebookUserId
	{
		get { return PlayerPrefs.GetString("SS_FacebookUserID", ""); }
		set 
		{ 
			PlayerPrefs.SetString("SS_FacebookUserID", value); 
			ServerSavedGameStateSync.LoggedInSavedGameStateDataId = value;
		}
	}

	void Awake ()
	{
		DontDestroyOnLoad(this);

		if (FB.IsInitialized) {
			FB.ActivateApp();
		} else {
			//Handle FB.Init
			FB.Init( () => {
				FB.ActivateApp();
			});
		}
	}

	static string sharePostText = "";


	public static void AttemptLogin()
	{
		if (FB.IsInitialized)
		{
			FB.LogInWithReadPermissions( new List<string>() {"public_profile"}, LoginResult);
		}
	}
		
	public static void SharePost(string text)
	{
		if(FB.IsInitialized)
		{
			if(string.IsNullOrEmpty(FacebookUserId))
			{
				sharePostText = text;
				AttemptLogin();
			}
			else
			{
				FB.FeedShare(FacebookUserId, new System.Uri("https://itunes.apple.com/app/id1151245201"), text, "Stickman Surfer");
			}
		}
	}

	public static void Logout()
	{
		FB.LogOut();
	}

	public static bool ShowShareButton()
	{
		if(GameServerSettings.SharedInstance.SurferSettings.ShowFBButt == false)
			return false;

		if(GameServerSettings.SharedInstance.SurferSettings.ShowFBButtIfNotLoggedIn)
			return true;

		return FB.IsLoggedIn;
	}
		
	protected static void LoginResult(IResult result)
    {
        if (result == null)
        {
			sharePostText = "";
			AlertDialogUtil.ShowAlert("FACEBOOK LOGIN", "NO RESPONSE FROM INTERNET", "DONE");
        	return;
        }

        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
			sharePostText = "";
			AlertDialogUtil.ShowAlert("FACEBOOK LOGIN", string.Format("Error - {0}", result.Error), "DONE");
        }
        else if (result.Cancelled)
        {
			sharePostText = "";
			AlertDialogUtil.ShowAlert("FACEBOOK LOGIN", "LOGIN CANCELLED", "DONE");
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
			AlertDialogUtil.ShowAlert("FACEBOOK LOGIN", "LOGIN SUCCESS", "DONE");

			AccessToken aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			FacebookUserId = aToken.UserId;

			ServerSavedGameStateSync savedGameStateSync = FindObjectOfType<ServerSavedGameStateSync>();
			if(savedGameStateSync != null)
			{
				savedGameStateSync.SyncSavedGameState();
			}

			if(!string.IsNullOrEmpty(sharePostText))
			{
				SharePost(sharePostText);
				sharePostText = "";
			}
				
			Debug.Log("User Id: " + FacebookUserId);
	        foreach (string perm in aToken.Permissions) 
	        {
	            Debug.Log(perm);
	        }	      
		}
			
		Debug.Log("Facebook" + result.RawResult);
    }


	void OnApplicationPause (bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus) {
			//app resume
			if (FB.IsInitialized) {
				FB.ActivateApp();
			} else {
				//Handle FB.Init
				FB.Init( () => {
					FB.ActivateApp();
				});
			}
		}
	}

}
