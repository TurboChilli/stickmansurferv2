﻿using UnityEngine;
using System.Collections;

[SelectionBase]
[RequireComponent (typeof(BouyBobbing))]
public class Rock : MonoBehaviour 
{

	public MeshRenderer meshRenderer = null;
	public Collider collider = null;
	public ParticleSystem particles = null;
	private BouyBobbing bobbingScript = null;
	private AnimatingObject attachedAnim = null;

	public void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		collider = GetComponent<Collider>();
		bobbingScript = GetComponent<BouyBobbing>();
		attachedAnim = GetComponent<AnimatingObject>();
	}

	public void Hide()
	{
		meshRenderer.enabled = false;
		collider.enabled = false;		
		particles.Stop();
	}

	public void ToggleBobbing(bool value)
	{
		bobbingScript.enabled = value;
	}

	public void ResetRock()
	{
		meshRenderer.enabled = true;
		collider.enabled = true;	
		attachedAnim.DrawFrame( attachedAnim.animationFrames[ Random.Range(0, attachedAnim.animationFrames.Length) ]);
		particles.Play();
	}

	public void SetMaterial(Material material)
	{
		meshRenderer.material = material;
	}
}

public enum RockType
{
	Basic,
	Flotsam,
	Barrel,
	Volcanic,
	Count
}

public class RockTitle
{
	public static string Get(RockType rockType)
	{
		switch (rockType)
		{
			case RockType.Basic:
				return BasicRockTitle;
			case  RockType.Barrel:
				return BarrelTitle;
			case  RockType.Flotsam:
				return FlotsamTitle;
			case  RockType.Volcanic:
				return VolcanicTitle;
		};
		return string.Empty;
	}

	public const string BasicRockTitle = 	"Basic Rock";
	public const string FlotsamTitle =  	"Flotsam";
	public const string BarrelTitle = 		"Barrel";
	public const string VolcanicTitle = 	"Volcanic";

};