﻿using UnityEngine;
using System.Collections;

public class SpecialRewardPickup : MonoBehaviour {

	public SpecialReward specialType = SpecialReward.None;
	public MeshRenderer specialRenderer = null;
	public ParticleSystem coinExplosion = null;
	public ParticleSystem energyDrinkExplosion = null;

	private AnimatingObject image;

	void Start () 
	{
		specialRenderer = GetComponent<MeshRenderer>();
		image = GetComponent<AnimatingObject>();
	}

	public void OnHit()
	{
		specialRenderer.enabled = false;
		if (specialType == SpecialReward.Coins)
		{
			if (coinExplosion != null)
				coinExplosion.Emit(100);
		}

		/* looks odd, no particles required for energy drink pickups
		else
		{
			if (energyDrinkExplosion != null)
				energyDrinkExplosion.Emit(100);
		}
		*/
	}

	public void SetSpecialReward(SpecialReward _specialType)
	{
		specialType = _specialType;

		switch (specialType)
		{
			case SpecialReward.Coins:
				image.DrawFrame((int)SpecialRewardFrames.Coin);
			break;
			case SpecialReward.SingleEnergy:
				image.DrawFrame((int)SpecialRewardFrames.SingleEnergy);
			break;
			case SpecialReward.DoubleEnergy:
				image.DrawFrame((int)SpecialRewardFrames.DoubleEnergy);
			break;
		};
	}

	public void ResetSpecialReward()
	{
		specialRenderer = GetComponent<MeshRenderer>();
		image = GetComponent<AnimatingObject>();

		specialRenderer.enabled = true;
	}
}

public enum SpecialRewardFrames
{
	SingleEnergy = 0,
	DoubleEnergy = 1,
	Coin = 2,
};

