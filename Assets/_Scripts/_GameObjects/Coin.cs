﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
[SelectionBase]
public class Coin : MonoBehaviour 
{
	private PowerupManager powerUpManager = null;
	private PlayerControl playerControl = null;

	public Material superCoinMaterial;
	private Material normalMaterial;

	public GameObject coinLeft = null;
	public GameObject coinRight = null;
	private Renderer coinLeftRenderer = null;
	private Renderer coinRightRenderer = null;

	private Collider coinCollider = null;

	private bool isShowingDoubleCoin = false;
	public bool isHit = false;
	private bool isGoingTowardsPlayer = false;

	private float minDistanceApart = 0.2f;
	private float minDistanceToPlayer = 5.0f;

	private float curTimeToMove = 0.0f;
	private float timeToMove = 0.4f;
	private Vector3 origPosition;

	private Vector3 origLocalPosition;
	private bool isInitialised = false;

	public void Initialise(PowerupManager _powerUpManager, PlayerControl _playerControl)
	{
		if (!isInitialised)
		{
            if (coinLeft == null)
                return;
            
			isInitialised = true;

			powerUpManager = _powerUpManager;
			playerControl = _playerControl;


			coinLeftRenderer = coinLeft.GetComponent<Renderer>();
			coinRightRenderer = coinRight.GetComponent<Renderer>();

			if (normalMaterial != null)
				normalMaterial = coinLeftRenderer.material;

			coinCollider = GetComponent<Collider>();

			coinRight.SetActive(false);
			coinLeft.transform.position = transform.position;
			isShowingDoubleCoin = false;

			origLocalPosition = transform.localPosition;
			origPosition = transform.position;
		}
	}

    bool doubleCoinOverrideMode = false;
    public void SetDoubleCoinModeManually(bool doubleCoinMode)
    {
        doubleCoinOverrideMode = doubleCoinMode;
        coinRight.SetActive(doubleCoinMode);
    }

	void Update()
	{
        if (coinLeft == null)
            return;
                    
        if (powerUpManager == null)
            return;

		if (Application.isPlaying)
		{
			if (isGoingTowardsPlayer)
			{
				if (!isHit)
				{
					float lerpValue =  Mathf.Pow( 1.0f - curTimeToMove / timeToMove, 3.0f);
					transform.position = Vector3.Lerp(origPosition, playerControl.transform.position, lerpValue);

					curTimeToMove -= Time.deltaTime;

					if (curTimeToMove < 0)
					{
						playerControl.OnCoinHit(this.gameObject);
					}
				}
			}
			else if (!isHit)
			{
				if (powerUpManager.magnetPowerupOn || powerUpManager.vehicleMagnetPowerUpOn)
				{
					float distance = Vector2.Distance(transform.position, playerControl.transform.position);
					if (distance < minDistanceToPlayer)
					{
						isGoingTowardsPlayer = true;
						curTimeToMove = timeToMove;
						origPosition = transform.position;

						coinCollider.enabled = false;
					}
				}

                if (powerUpManager.coinMulitplierPowerupOn || powerUpManager.vehicleCoinMultiplierPowerUpOn || doubleCoinOverrideMode)
				{
					if (!isShowingDoubleCoin)
					{
						coinRight.SetActive(true);
						coinRight.transform.position = transform.position + Vector3.left * minDistanceApart;
						coinLeft.transform.position = transform.position + Vector3.right * minDistanceApart;
						isShowingDoubleCoin = true;
					}
				}
				else
				{
					if (isShowingDoubleCoin)
					{
						coinRight.SetActive(false);
						coinLeft.transform.position = transform.position;
						isShowingDoubleCoin = false;
					}
				}
			}
		}
	}

	public void Hit()
	{
        if (coinLeft == null)
            return;
		isHit = true;
		coinLeft.SetActive(false);
		coinRight.SetActive(false);
	}

	public void ResetCoin()
	{
        if (coinLeft == null)
            return;
		isHit = false;
		isGoingTowardsPlayer = false;
		coinLeft.SetActive(true);

		coinCollider.enabled = true;

		transform.localPosition = origLocalPosition;

		if (powerUpManager.CheckForPowerup(PowerUp.CoinMultiplier))
		{
			coinRight.SetActive(true);
			coinRight.transform.position = transform.position + Vector3.left * minDistanceApart;
			coinLeft.transform.position = transform.position + Vector3.right * minDistanceApart;
			isShowingDoubleCoin = true;
		}
		else
		{
			coinRight.SetActive(false);
			coinLeft.transform.position = transform.position;
			isShowingDoubleCoin = false;
		}
	}

}
