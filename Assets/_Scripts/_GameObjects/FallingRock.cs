﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class FallingRock : MonoBehaviour 
{
	public PlayerControl playerControl = null;
	public GameObject shadowObject = null;
	public GameObject rockObject = null;
	public GameObject submergedRockObject = null;
	public ParticleSystem[] waterEffects = null;
	public AnimatingObject rockAnim = null;

	public AudioSource warningSound = null;
	public AudioSource crashSound = null;

	private Renderer rockRenderer;
	private Renderer croppedRockRenderer;
	private Renderer shadowRenderer;

	public bool isAnimated = true;

	private Vector3 inactiveRockPosition;
	private Vector3 origShadowScale;

	private float timeToMoveRock = 0.4f;
	private float curTimeToMoveRock = 0.0f;

	private float animTime = 0.0f;
	private float framesPerSecond = 10.0f;

	public bool hasActivated = false;
	public bool hasFired = false;

	private bool isInitialised = false;

	private float distanceToStartScaling = 18.0f;
	private float distanceToFire = 8.0f;


	void Start () 
	{
		Reset();
	}

	public void SetMaterial(Material newMaterial, Material newCropMaterial, Material newShadowMaterial)
	{
		if (rockRenderer == null)
			rockRenderer = rockObject.GetComponent<Renderer>();	
		if (croppedRockRenderer == null)
			croppedRockRenderer = submergedRockObject.GetComponent<Renderer>();
		if (shadowRenderer == null)
			shadowRenderer = shadowObject.GetComponent<Renderer>();

		rockRenderer.material = newMaterial;
		croppedRockRenderer.material = newCropMaterial;
		shadowRenderer.material = newShadowMaterial;
	}

	public void ResizeRockObject(bool largerScale)
	{
		if (largerScale)
		{
			rockObject.transform.localScale = new Vector3(5.7f, 5.1f, 5.7f);
			submergedRockObject.transform.localScale = new Vector3(5.7f, 5.1f, 5.7f) * 0.5f;

		}
		else
		{
			rockObject.transform.localScale = new Vector3(2.1f, 1.8f, 2.1f);
			submergedRockObject.transform.localScale = new Vector3(2.1f, 1.8f, 2.1f);
		}
	}

	public void Reset()
	{
		if (!isInitialised)
		{
			if (playerControl == null)
				playerControl = FindObjectOfType<PlayerControl>();
			//if (warningSignRenderer == null)
			//	warningSignRenderer = warningSign.GetComponent<Renderer>();

			inactiveRockPosition = rockObject.transform.localPosition;
			origShadowScale = shadowObject.transform.localScale;
			isInitialised = true;
		}

		submergedRockObject.SetActive(false);
		rockObject.gameObject.SetActive(true);

		rockObject.transform.localPosition = inactiveRockPosition;
		shadowObject.transform.localScale = Vector3.zero;
		for (int i = 0; i < waterEffects.Length; i++)
		{
			waterEffects[i].gameObject.SetActive(false);
		}

		curTimeToMoveRock = timeToMoveRock;
		shadowObject.SetActive(true);

		hasActivated = false;
		hasFired = false;

		if (rockAnim == null)
			rockAnim = rockObject.GetComponent<AnimatingObject>();

		if (isAnimated)
			rockAnim.DrawFrame(0);
	}

	void Update () 
	{
		if (playerControl == null)
			playerControl = FindObjectOfType<PlayerControl>();

		if (rockAnim == null)
			rockAnim = rockObject.GetComponent<AnimatingObject>();

		float distance = Mathf.Max(0, shadowObject.transform.position.x - playerControl.transform.position.x);

		if (hasFired)
		{
			if (curTimeToMoveRock > 0)
			{
				float lerpAmount = curTimeToMoveRock / timeToMoveRock;

				if (isAnimated)
				{
					while (animTime > 1.0 / framesPerSecond)
					{
						rockAnim.DrawFrame( (rockAnim.curFrame + 1) % 3);
						animTime -= 1.0f / framesPerSecond;
					}
					animTime += Time.deltaTime;
				}

				curTimeToMoveRock -= Time.deltaTime;
				if (curTimeToMoveRock <= 0)
				{
					lerpAmount = 0;
					for (int i = 0; i < waterEffects.Length; i++)
					{
						waterEffects[i].gameObject.SetActive(true);
						waterEffects[i].Play();
	
						if (isAnimated)
							waterEffects[i].transform.localPosition = shadowObject.transform.localPosition + new Vector3(0.6f, -0.3f, 0.5f);
						else
							waterEffects[i].transform.localPosition = shadowObject.transform.localPosition + new Vector3(-0.6f, 0.0f, 0.5f);
					}

					submergedRockObject.SetActive(true);

					if (isAnimated)
					{
						submergedRockObject.transform.localPosition = shadowObject.transform.localPosition + new Vector3(0.0f, 0.0f ,0.0f);
					}
					else
						submergedRockObject.transform.localPosition = shadowObject.transform.localPosition;

					submergedRockObject.transform.rotation = Quaternion.identity;
						
					rockObject.SetActive(false);
					GlobalSettings.PlaySound(crashSound);
					shadowObject.SetActive(false);
			 	}

				if (isAnimated)	
					rockObject.transform.transform.localPosition = Vector3.Lerp(inactiveRockPosition,
																			shadowObject.transform.localPosition + new Vector3(0,1,0),
																			1.0f - lerpAmount);
				else
					rockObject.transform.transform.localPosition = Vector3.Lerp(inactiveRockPosition,
																			shadowObject.transform.localPosition,
																			1.0f - lerpAmount);
			}
		}

		if (hasActivated)
		{
			float scaleLerpAmount = Mathf.InverseLerp(distanceToFire, distanceToStartScaling , distance);
			shadowObject.transform.localScale = Vector3.Lerp(Vector3.zero, origShadowScale, Mathf.Pow(1.0f - scaleLerpAmount, 2.0f));
			if (scaleLerpAmount <= 0.05f && !hasFired)
			{
				hasFired = true;
			}
		}

		if (distance < distanceToStartScaling)
			hasActivated = true;
	}
}
