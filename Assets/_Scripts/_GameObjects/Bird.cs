﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {

	// Use this for initialization
	private bool birdFlying = false; 
	private Renderer birdRenderer;
	private Renderer warningSignRenderer;

	private Vector3 warningSignLocalPos = Vector3.zero;

	public Transform warningSign;
	public AudioSource warningSound = null;
	public AudioSource birdSound = null;

	//private bool flashWarningOn = true;
	private bool signFlashing = false;

	//private static int birdsSoundsActive = 0;
	private static float lastWarningChange = 0.0f;
	private static float currentFlashTimer = 0.0f;
	private static bool shouldFlash = false;
	private static int birdsActive = 0;
	private int birdID = 0;
	private bool flashState = false;

	const float warningFlashOnDelay = 1.0f;
	const float warningFlashOffDelay = 0.2f;
	int warningSignCounter = 0;

	public bool postGameMute = false;

	private PlayerControl playerControl = null;

	void Start ()
	{
		warningSignLocalPos = warningSign.transform.localPosition;


		if (playerControl == null)
			playerControl = FindObjectOfType<PlayerControl>();
		if (birdRenderer == null)
			birdRenderer = GetComponent<Renderer>();

		warningSignRenderer = warningSign.GetComponent<Renderer>();

		warningSound.Stop();
		birdSound.Stop();
	}

	// Update is called once per frame
	void Update () 
	{
		if (GlobalSettings.surfGamePaused)
			return;

		if(!birdFlying)
		{
			if(birdRenderer.isVisible)
			{
				birdFlying = true;
				warningSignRenderer.enabled = false;
				signFlashing = false;
				birdID = birdsActive;
				birdsActive ++;
			}
		}
		 
		if(birdFlying)
		{
			transform.Translate(-5f * Time.deltaTime, 0,0);

			if (playerControl.transform.position.x - 20.0f - transform.position.x > 20.0f)
			{
				warningSound.Stop();
				birdSound.Stop();
				birdsActive --;
			}

		}
			
		if(!signFlashing)
		{
			if(warningSignRenderer.isVisible)
			{
				signFlashing = true;

				if (birdID < 3 && !postGameMute)
					GlobalSettings.PlaySound(birdSound);
			}
		}

		if (signFlashing)
		{
			currentFlashTimer += Time.deltaTime;

			if (currentFlashTimer - lastWarningChange  >= warningFlashOnDelay && shouldFlash)
			{
				shouldFlash = false;
				lastWarningChange = currentFlashTimer;
			}
			else if (currentFlashTimer - lastWarningChange >= warningFlashOffDelay && !shouldFlash)
			{
				shouldFlash = true;
				lastWarningChange = currentFlashTimer;
			}

			SetWarningSignPosToRHS();

			if (flashState != shouldFlash)
			{
				if (shouldFlash == true && warningSignCounter < 1)
				{
					warningSignCounter += 1;
					warningSignRenderer.enabled = true;

					if (warningSound != null && !playerControl.waveEnding && !postGameMute)
					{
						GlobalSettings.PlaySound(warningSound);
					}
					
					flashState = true;
				}
				else
				{
					warningSignRenderer.enabled = false;
					flashState = false;
				}
			}

			if (flashState && warningSignRenderer.enabled)
			{
				if (birdRenderer.isVisible)
				{
					signFlashing = false;
				}
			}
		}


	}


	void SetWarningSignPosToRHS()
	{
		var camPoint = Camera.main.ScreenToWorldPoint (new Vector3 (Camera.main.pixelWidth ,Camera.main.pixelHeight, Camera.main.nearClipPlane));
		float warningSignPosX = camPoint.x - 1.5f;
		warningSign.transform.position = new Vector3(warningSignPosX, warningSign.transform.position.y, warningSign.transform.position.z);
	}

	public void ResetBird()
	{
		if (birdRenderer == null)
			birdRenderer = GetComponent<Renderer>();
		if (warningSignRenderer == null)
			warningSignRenderer = warningSign.GetComponent<Renderer>();

		if (warningSignLocalPos == Vector3.zero)
			warningSignLocalPos = warningSign.localPosition;

		birdFlying = false;
		warningSign.localPosition = warningSignLocalPos;

		warningSound.Stop();
		birdSound.Stop();

		postGameMute = false;

		warningSignRenderer.enabled = true;
		signFlashing = false;

		lastWarningChange = 0.0f;
		currentFlashTimer = 0.0f;
		shouldFlash = false;
		flashState = false;
		warningSignCounter = 0;
	}

	public void MuteBirdSounds()
	{
		birdSound.Stop();
		warningSound.Stop();
		postGameMute = true;
	}



}

public enum BirdType
{
	Basic,
	Bat,
	Seagull,
	Drone,
	Count
}


public class BirdTitle
{
	public static string Get(BirdType birdType)
	{
		switch (birdType)
		{
			case BirdType.Basic:
				return BasicBirdTitle;
			case  BirdType.Bat:
				return BatTitle;
			case  BirdType.Seagull:
				return SeagullTitle;
			case  BirdType.Drone:
				return DroneTitle;
		};
		return string.Empty;
	}

	public const string BasicBirdTitle = 	"Basic Bird";
	public const string BatTitle =  		"Bat";
	public const string SeagullTitle = 		"Seagull";
	public const string DroneTitle = 		"Drone";

};