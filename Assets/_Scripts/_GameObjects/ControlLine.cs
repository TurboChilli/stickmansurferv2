﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ControlLine))]
public class ControlLineInspector : Editor
{
	private ControlLine line;
	private Transform handleTransform;
	private Quaternion handleRotation;

	[Range(5, 1000)]
	public const int numLines = 10;

	private int selectedIndex = -1;

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		line = target as ControlLine;

		if (GUILayout.Button("Add Point"))
		{
			Undo.RecordObject(line, "AddedPoint");
			line.AddPoint();
			EditorUtility.SetDirty(line);
		}
		if (GUILayout.Button("Remove Point"))
		{
			Undo.RecordObject(line, "RemovedPoint");
			line.RemovePoint();
			EditorUtility.SetDirty(line);
		}
	}

	private void OnSceneGUI()
	{
		line = target as ControlLine;
		handleTransform = line.transform;
		handleRotation = Tools.pivotRotation == PivotRotation.Local ?
		handleTransform.rotation : Quaternion.identity;

		if (line.active)
		{
			Vector3 prevPoint = ShowPoint(0);
			for (int i = 1; i < line.points.Length; i++)
			{
				Vector3 curPoint = ShowPoint(i);

				Handles.color = Color.white;
				Handles.DrawLine(prevPoint, curPoint);
				prevPoint = curPoint;
			}
		}
	}


	//returns transformed point from handle widget thingy
	private Vector3 ShowPoint(int index)
	{
		Vector3 point = handleTransform.TransformPoint(line.points[index]);

		Handles.color = Color.red;
		if (Handles.Button(point, handleRotation, 0.1f, 0.06f, Handles.DotCap))
			selectedIndex = index;
		
		if (selectedIndex == index)
		{
			EditorGUI.BeginChangeCheck();
			point = Handles.DoPositionHandle(point, handleRotation);
			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(line, "MovedControlPoint");
				EditorUtility.SetDirty(line);
				line.points[index] = handleTransform.InverseTransformPoint(point);
			}
		}
		return point;
	}
}

#endif


public class ControlLine : MonoBehaviour {

	public bool active = true;

	public Vector3[] points;

	public void Reset()
	{
		points = new Vector3[]{
			new Vector3(1.0f, 0.0f, 0.0f),
			new Vector3(2.0f, 0.0f, 0.0f),
			new Vector3(3.0f, 0.0f, 0.0f),
			new Vector3(3.0f, 0.0f, 0.0f)
			};
	}

	public Vector3 GetVelocity(float lerpValue)
	{
		float nearPointIndex = lerpValue * (float)(points.Length - 1);	

		int prevIndex = (int)(nearPointIndex);
		int nextIndex = (int)(nearPointIndex + 1);

		return points[nextIndex] - points[prevIndex]; 

	}

	public Vector3 GetPositionOnLine(float lerpValue)
	{
		float nearPointIndex = lerpValue * (float)(points.Length - 1);	

		int prevIndex = (int)(nearPointIndex);
		int nextIndex = (int)(nearPointIndex + 1);
		float ratio = nearPointIndex - (float)prevIndex;

		return transform.TransformPoint(Vector3.Lerp(points[prevIndex], points[nextIndex], ratio));
	}

	public Vector3[] GetPoints()
	{
		Vector3[] returnPoints = new Vector3[points.Length];
		for (int i = 0; i < returnPoints.Length; i++)
		{
			returnPoints[i] = transform.TransformPoint(points[i]);
		}

		return returnPoints;
	}

	public void AddPoint()
	{
		Array.Resize(ref points, points.Length + 1);
		points[points.Length - 1] = points[points.Length - 2] + Vector3.right;
	}

	public void RemovePoint()
	{
		if (points.Length > 2)
		{
			Array.Resize(ref points, points.Length - 1);
		}
	}
}
