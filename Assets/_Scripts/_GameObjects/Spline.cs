﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Spline))]
public class SplineInspector : Editor
{

	private Spline spline;
	private Transform handleTransform;
	private Quaternion handleRotation;

	[Range(0, 1000)]
	public const int numLines = 10;

	private int selectedIndex = -1;

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		spline = target as Spline;

		if (GUILayout.Button("Add Curve"))
		{
			Undo.RecordObject(spline, "AddedCurve");
			spline.AddCurve();
			EditorUtility.SetDirty(spline);
		}
		if (GUILayout.Button("Remove Curve"))
		{
			Undo.RecordObject(spline, "RemoveCurve");
			spline.RemoveCurve();
			EditorUtility.SetDirty(spline);
		}
	}

	private void OnSceneGUI()
	{
		spline = target as Spline;
		handleTransform = spline.transform;
		handleRotation = Tools.pivotRotation == PivotRotation.Local ?
		handleTransform.rotation : Quaternion.identity;

		if (spline.active)
		{
			Vector3 p0 = ShowPoint(0);
			for (int i = 1; i < spline.points.Length; i+= 3)
			{
				Vector3 p1 = ShowPoint(i + 0);
				Vector3 p2 = ShowPoint(i + 1);
				Vector3 p3 = ShowPoint(i + 2);

				Handles.color = Color.gray;
				Handles.DrawLine(p0, p1);
				Handles.DrawLine(p1, p2);
				Handles.DrawLine(p2, p3);

				Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 5f);

				p0 = p3;

			}
			DrawVelocity();
		}
	}

	private void DrawVelocity()
	{
		Vector3 lineStart = spline.GetPositionOnSpline(0);

		for (int i = 1; i < numLines * spline.CurveCount; i++)
		{
			float lerpValue = (float) i / ((float)numLines * (float)(spline.CurveCount));
			lerpValue *= spline.CurveCount;

			Vector3 velocity = spline.GetVelocity(lerpValue);
			//velocity.Normalize();

			Handles.color = Color.green;
			Handles.DrawLine(lineStart, lineStart + velocity);
			lineStart = spline.GetPositionOnSpline(lerpValue);
		}

	}

	//returns transformed point from handle widget thingy
	private Vector3 ShowPoint(int index)
	{
		Vector3 point = handleTransform.TransformPoint(spline.points[index]);
		Handles.color = Color.white;

		if (Handles.Button(point, handleRotation, 0.1f, 0.06f, Handles.DotCap))
			selectedIndex = index;
		
		if (selectedIndex == index)
		{
			EditorGUI.BeginChangeCheck();
			point = Handles.DoPositionHandle(point, handleRotation);
			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(spline, "MovedControlPoint");
				EditorUtility.SetDirty(spline);
				spline.points[index] = handleTransform.InverseTransformPoint(point);
			}
		}
		return point;
	}
}

#endif


public class Spline : MonoBehaviour 
{
	public bool active = true;

	public Vector3[] points;

	public void Reset()
	{
		points = new Vector3[] {
			new Vector3(1.0f, 0.0f, 0.0f),
			new Vector3(2.0f, 0.0f, 0.0f),
			new Vector3(3.0f, 0.0f, 0.0f),
			new Vector3(3.0f, 0.0f, 0.0f)
			};
	}

	public Vector3 GetPositionOnSpline(float lerpValue)
	{
		int index = 0;
		if ((int)lerpValue > 0)
		{
			if ((int)lerpValue >= CurveCount)
			{
				index = points.Length - 4;
				lerpValue = 1.0f;
			}
			else
			{
				index = (int)lerpValue * 3;
				lerpValue -= (int)lerpValue;
			}
		}

		Vector3 pos1a = Vector3.Lerp(points[index + 0], points[index + 1], lerpValue);
		Vector3 pos2a = Vector3.Lerp(points[index + 1], points[index + 2], lerpValue);
		Vector3 pos3a = Vector3.Lerp(points[index + 2], points[index + 3], lerpValue);

		Vector3 pos1b = Vector3.Lerp(pos1a, pos2a, lerpValue);
		Vector3 pos2b = Vector3.Lerp(pos2a, pos3a, lerpValue);

		return transform.TransformPoint(Vector3.Lerp(pos1b, pos2b, lerpValue));
	
	}

	public Vector3 GetFirstDerivative (float t) 
	{
		int index = 0;
		if ((int)t > 0)
		{
			if ((int)t >= CurveCount)
			{
				index = points.Length - 4;
				t = CurveCount;
			}
			else
			{
				index = (int)t * 3;
				t -= (int)t;
			}
		}

		t *= CurveCount;
		float oneMinusT = 1f - t;
		return 
			3f * oneMinusT * oneMinusT * (points[index + 1] - points[index + 0]) +
			6f * oneMinusT * t * (points[index + 2] - points[index + 1]) +
			3f * t * t * (points[index + 3] - points[index + 2]);
	}

	public Vector3 GetVelocity(float lerpAmount)
	{
		return transform.TransformPoint(
			GetFirstDerivative(lerpAmount) - transform.position);
	}

	public void AddCurve()
	{
		Vector3 point = points[points.Length - 1];
		Array.Resize(ref points, points.Length + 3);

		point.x += 1.0f;
		points[points.Length - 3] = point;

		point.x += 1.0f;
		points[points.Length - 2] = point;

		point.x += 1.0f;
		points[points.Length - 1] = point;
	}

	public void RemoveCurve()
	{
		if (points.Length > 4)
		{
			Array.Resize(ref points, points.Length - 3);
		}
	}

	public int CurveCount {
		get {
			return (points.Length - 1) / 3;
		}
	}
}
