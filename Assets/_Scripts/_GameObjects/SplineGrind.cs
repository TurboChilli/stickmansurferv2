﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(Spline))]
[RequireComponent(typeof(ControlLine))]
[ExecuteInEditMode]
[SelectionBase]
public class SplineGrind : MonoBehaviour 
{
	[SerializeField]
	private bool isSpline = true;
	[SerializeField]
	private bool isRepeatedTexture = true;

	public GrindType grindType = GrindType.Funbox;

	private int splineVertNum = 10;

	private Spline spline;
	private ControlLine line;
	private MeshRenderer meshRenderer;
	private MeshFilter meshFilter;
	public PlayerControl playerControl = null;
	public PowerupManager powerUpManager = null;

	private Mesh mesh = null;

	private List<Vector3> positions;
	private int[] indices;
	private List<Vector2> uvs;

	private Vector3[] linePoints = null;
	private float[] linePointXPositions = null;
	private int numPoints = 0;

	private float minDistanceToMatchRotation = 15.0f;
	private float minDistanceToLock = 0.5f;
	public bool playerLockedToGrind = false;

	private bool isInitialised = false;

	void Start () 
	{
		UpdateMesh();
	}
	
	// Update is called once per frame
	void Update () 
	{
		#if UNITY_EDITOR
			UpdateMesh();
		#else
			if (!isInitialised)
				UpdateMesh();
			else
				UpdatePoints();
		#endif

		if (playerControl != null)
		{
			Vector3 checkPosition = playerControl.transform.position;
			checkPosition.z = transform.position.z;
			checkPosition.y -= powerUpManager.GetGrindYOffset();

			if (!playerLockedToGrind)
			{
				if (checkPosition.x > linePoints[0].x && 
					checkPosition.x <= linePoints[linePoints.Length - 1].x)
				{
					for (int i = 1; i < numPoints; i++)
					{
						if (linePointXPositions[i - 1] < checkPosition.x &&
							linePointXPositions[i] >= checkPosition.x)
						{
							Vector3 projectedPoint = GetVerticallyProjectedPoint(checkPosition);
							float dist = Vector3.Distance(checkPosition, projectedPoint);


							if (dist <= minDistanceToMatchRotation && checkPosition.y >= projectedPoint.y)
							{
								Vector2 vectorDirection = GetVelocityAtVerticallyProjectedPoint(checkPosition).normalized;

								float angle = Mathf.Atan2(vectorDirection.y, vectorDirection.x) * Mathf.Rad2Deg;
								////////Debug.Log(vectorDirection.ToString() + "   " + angle.ToString());
								playerControl.transform.rotation = Quaternion.Slerp( playerControl.transform.rotation, Quaternion.AngleAxis( angle, Vector3.forward), 0.05f);
								playerControl.isAboveSpline = true;
								playerControl.splineGrind = this;
							}
							if ( playerControl.isAboveSpline &&
								 playerControl.airMoveVector.y <= 0 && 
								 checkPosition.y < projectedPoint.y)
							{
								//////Debug.Log("LOCKED");
								playerControl.OnGrindingSpline(this);
								playerLockedToGrind = true;

								break;
							}
						}
					}
				}
				else if (playerControl.isAboveSpline &&	playerControl.splineGrind == this)
				{
					playerControl.isAboveSpline = false;
				}
			}
			else 
			{
				//Debug.DrawLine(playerControl.transform.position, );
			}
		}
		else
		{
			playerControl = FindObjectOfType<PlayerControl>();
		}
	}


	public void UpdatePoints()
	{
		if (isSpline)
		{
			line.active = false;
			spline.active = true;

			linePoints = new Vector3[splineVertNum + 1];

			for (int i = 0; i <= splineVertNum; i++)
			{
				float lerpVal = (float)i / (float)splineVertNum;
				lerpVal *= spline.CurveCount;

				linePoints[i] = spline.GetPositionOnSpline(lerpVal);
			}
		}	
		else
		{
			spline.active = false;
			line.active = true;

			linePoints = line.GetPoints();
		}
		numPoints = linePoints.Length;

		linePointXPositions = new float[numPoints];
		linePointXPositions[0] = linePoints[0].x;
		for(int i = 1; i < numPoints; i++)
		{
			linePointXPositions[i] = linePointXPositions[i - 1] + Mathf.Abs((linePoints[i - 1].x - linePoints[i].x));
		}

	}

	public void UpdateMesh()
	{
		if (spline == null)
			spline = GetComponent<Spline>();
		if (line == null)
			line = GetComponent<ControlLine>();
		if (meshRenderer == null)
			meshRenderer = GetComponent<MeshRenderer>();
		if (meshFilter == null)
			meshFilter = GetComponent<MeshFilter>();

		if (playerControl == null)
			playerControl = FindObjectOfType<PlayerControl>();
		if (powerUpManager == null)
			powerUpManager = FindObjectOfType<PowerupManager>();

		isInitialised = true;

		UpdatePoints();

		//create mesh
		if (mesh == null)
		{
			mesh = new Mesh();
			mesh.name = "LineGrinder";
			meshFilter.mesh = mesh;
		}
		else
			mesh = meshFilter.sharedMesh;

		mesh.Clear();

		positions = new List<Vector3>(new Vector3[(numPoints) * 4]);
		uvs = new List<Vector2>(new Vector2[(numPoints) * 4]);
		indices = new int[(numPoints) * 6];

		//put in values
		Vector3 originVector = transform.right;

		for (int pos_index = 0, line_index = 1; line_index < numPoints; pos_index++, line_index++)
		{
			Vector3 lastPoint = linePoints[line_index - 1] - transform.position;
			Vector3 curPoint = linePoints[line_index] - transform.position;

			Vector3 projectedLastPoint = Vector3.Project(lastPoint , originVector);// + transform.position;
			Vector3 projectedCurPoint = Vector3.Project(curPoint , originVector);// + transform.position;

			positions[pos_index * 4 + 0] = lastPoint;
			positions[pos_index * 4 + 1] = curPoint;
			positions[pos_index * 4 + 2] = projectedLastPoint;
			positions[pos_index * 4 + 3] = projectedCurPoint;
		
			indices[ pos_index * 6 + 2 ] = pos_index * 4 + 0; 
			indices[ pos_index * 6 + 1 ] = pos_index * 4 + 2; 
			indices[ pos_index * 6 + 0 ] = pos_index * 4 + 3;
			 
			indices[ pos_index * 6 + 5 ] = pos_index * 4 + 0; 
			indices[ pos_index * 6 + 4 ] = pos_index * 4 + 3; 
			indices[ pos_index * 6 + 3 ] = pos_index * 4 + 1; 

			if (isRepeatedTexture)
			{
				uvs[pos_index * 4 + 0] = Vector2.up;
				uvs[pos_index * 4 + 1] = Vector2.one;
				uvs[pos_index * 4 + 2] = Vector2.zero;
				uvs[pos_index * 4 + 3] = Vector2.right;
			}
			else
			{
				float prev_x_uv = (float)(line_index - 1) / (numPoints - 1);
				float cur_x_uv = (float)line_index / (numPoints - 1);

				uvs[pos_index * 4 + 0] = new Vector2(prev_x_uv, 1);
				uvs[pos_index * 4 + 1] = new Vector2(cur_x_uv, 1);
				uvs[pos_index * 4 + 2] = new Vector2(prev_x_uv, 0);
				uvs[pos_index * 4 + 3] = new Vector2(cur_x_uv, 0);
			}
		}

		mesh.SetVertices(positions);
		mesh.SetUVs( 0, uvs);
		mesh.SetTriangles(indices, 0);
	}	

	public Vector3 GetVerticallyProjectedPoint(Vector3 checkPos)
	{
		float lerpPosition = 0.0f;
		Vector3 projectedPoint = -Vector3.one;
		for (int i = 1; i < numPoints; i++)
		{
			if (linePointXPositions[i - 1] < checkPos.x &&
				linePointXPositions[i] > checkPos.x)
			{
				if (isSpline)
				{
					float lastLerp = ((float)(i - 1) / splineVertNum) * spline.CurveCount; //curve chunk 
					float postLerp = ((float)(i    ) / splineVertNum) * spline.CurveCount;

					Vector3 lastPoint =  spline.GetPositionOnSpline( lastLerp);
					Vector3 postPoint =  spline.GetPositionOnSpline( postLerp);

				 	lerpPosition = lastLerp + ((checkPos.x - lastPoint.x) / (postPoint.x - lastPoint.x)) * (postLerp - lastLerp);
					projectedPoint = spline.GetPositionOnSpline( lerpPosition);
				}
				else
				{
					lerpPosition = (checkPos.x - linePointXPositions[i - 1]) / (linePointXPositions[i] - linePointXPositions[i - 1]);
					projectedPoint = Vector3.Lerp(linePoints[i - 1], linePoints[i], lerpPosition);
				}
			}
		}
		return projectedPoint;
	}

	public Vector3 GetVelocityAtVerticallyProjectedPoint(Vector3 checkPos)
	{
		float lerpPosition = 0.0f;
		Vector3 projectedPoint = -Vector3.one;
		for (int i = 1; i < numPoints; i++)
		{
			if (linePointXPositions[i - 1] < checkPos.x &&
				linePointXPositions[i] >= checkPos.x)
			{
				if (isSpline)
				{
					float lastLerp = ((float)(i - 1) / splineVertNum) * spline.CurveCount; //curve chunk 
					float postLerp = ((float)(i    ) / splineVertNum) * spline.CurveCount;

					Vector3 lastPoint =  spline.GetPositionOnSpline( lastLerp);
					Vector3 postPoint =  spline.GetPositionOnSpline( postLerp);

				 	lerpPosition = lastLerp + ((checkPos.x - lastPoint.x) / (postPoint.x - lastPoint.x)) * (postLerp - lastLerp);
					projectedPoint = spline.GetFirstDerivative(lerpPosition);
				}
				else
				{

					Vector3 lastPoint =  linePoints[i - 1];
					Vector3 postPoint =  linePoints[i];

					projectedPoint = postPoint - lastPoint;
				}
			}
		}
		return projectedPoint;
	}

	public Vector3 GetLerpPoint(float lerpValue)
	{
		Vector3 lerpPoint;
		if (isSpline)
		{
			float actualVal = (int)(lerpValue * (float)spline.CurveCount);
			lerpPoint = spline.GetPositionOnSpline( actualVal);
		}
		else
		{
			int index = (int)(lerpValue * (float)(numPoints - 1));
			float actualVal = lerpValue * (float)(numPoints - 1) - index;

			if (index < numPoints - 1)
				lerpPoint = Vector3.Lerp(linePoints[index], linePoints[index + 1], actualVal);
			else
				lerpPoint = linePoints[index];
		}
		return lerpPoint;
	}

	public void Reset()
	{
		playerLockedToGrind = false;
	}


}
