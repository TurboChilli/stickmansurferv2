﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[SelectionBase]
public class SlalomGate : MonoBehaviour {

	public Transform topPoint	 = null;
	public Transform bottomPoint = null;
	public Transform slalomCollider	 = null;

	public PlayerControl player = null;

	private Renderer topSlalomRenderer = null;
	private Renderer bottomSlalomRenderer = null;

	public bool hasBeenHit = false;

	void Start () 
	{
		UpdateCollider();
	}

	void UpdateCollider()
	{
		//scale
		Vector3 vectorDirection = bottomPoint.position - topPoint.position;
		float distance = vectorDirection.magnitude;
		slalomCollider.transform.localScale = new Vector3(1, distance, 1);

		//position
		slalomCollider.transform.position = bottomPoint.position - vectorDirection * 0.5f;

		//rotation
		vectorDirection.Normalize();
		float angle = Mathf.Atan2(vectorDirection.y, vectorDirection.x) * Mathf.Rad2Deg;

		slalomCollider.transform.rotation = Quaternion.AngleAxis( 90.0f, Vector3.forward) * Quaternion.AngleAxis( angle, Vector3.forward );

		if (topSlalomRenderer == null)
		{
			topSlalomRenderer = topPoint.gameObject.GetComponent<Renderer>();
			bottomSlalomRenderer = bottomPoint.gameObject.GetComponent<Renderer>();
		}
	}

	public void SetMaterial(Material newMaterial)
	{
		if (topSlalomRenderer == null)
		{
			topSlalomRenderer = topPoint.gameObject.GetComponent<Renderer>();
			bottomSlalomRenderer = bottomPoint.gameObject.GetComponent<Renderer>();
		}

		topSlalomRenderer.material = newMaterial;
		bottomSlalomRenderer.material = newMaterial;
	}

	public void OnPlayerhit()
	{
		hasBeenHit = true;
		slalomCollider.gameObject.SetActive(false);
		//effects pew pew pew pew pew pew
	}

	public void ResizeSlalomObjects(bool largerScale)
	{
		if (largerScale)
		{
			topPoint.localScale = new Vector3(1.5f, 1.4f, 1.0f);
			bottomPoint.localScale = new Vector3(1.5f, 1.4f, 1.0f);
		}
		else
		{
			topPoint.localScale = Vector3.one;
			bottomPoint.localScale = Vector3.one;
		}
	}

	public void ResetSlalom(PlayerControl playerControl)
	{
		player = playerControl;
		
		hasBeenHit = false;
		slalomCollider.gameObject.SetActive(true);
	}

	void Update () 
	{
		#if UNITY_EDITOR
			UpdateCollider();
		#endif

		if (!hasBeenHit && player != null)
		{
			if (player.transform.position.x > topPoint.position.x &&
				player.transform.position.x > bottomPoint.position.x)
			{
				//player.OnSlalomMiss();
				hasBeenHit = true;
			}

		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawLine(topPoint.position, bottomPoint.position);
	}
}
