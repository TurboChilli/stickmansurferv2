﻿using UnityEngine;
using System.Collections;

public class GoalDisplay : MonoBehaviour 
{
	public GameObject GoalVehicleIcon = null;
	public TextMesh GoalText = null;

	public void SetGoalText(string goalText)
	{
		GoalText.text = goalText;
	}

	public void SetVehicleIcon(Vehicle vehicle)
	{
		var vehicleIcon = GoalVehicleIcon.GetComponent<VehicleIconSelector>();
		vehicleIcon.SetVehicleIcon(vehicle);
	}
}
