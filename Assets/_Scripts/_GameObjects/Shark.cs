﻿using UnityEngine;
using System.Collections;
using TMPro;

public class Shark : MonoBehaviour 
{

	public GameObject fin = null;
	public GameObject head = null;

	private float sharkXVelocity = 1.0f;
	private float sharkYVelocity = 0.0f;
	private float sharkAttackImpulse = 4.0f;
    private Vector3 sharkStartPos;
	private float gravity = -9.8f;

	public GameObject warningSign;
	private Renderer warningSignRenderer = null;
	private Vector3 warningSignLocalPos;

	private float warningFlashTimer = 0;
	private float warningFlashOnDelay = 0.25f;
	private float warningFlashOffDelay = 0.1f;
	private float warningFlashDelay = 0f;
	private bool flashWarningOn = true;

	public AudioSource warningSound;
	//public AudioSource sharkAttackSound;

	private bool signFlashing = false;
	private bool sharkActive = false;
	private bool sharkAttacking = false;
    
    private bool sharkExit = false;
    private bool attackLunge = false;
    AnimatingObject animObject = null;
    private int countDownValue = 0;
    private int prevCountDownValue = 0;
	private float distanceToAttack = 5.0f;

    float sharkFrameTimer = 0;
    float sharkFrameDelay = 0.1f;
    int risingUpFrameCounter = 0;
    float sharkExitTimer = 0;
    float sharkExitDelay = 0.05f;

    float attackLungeDelay = 3;
    float attackLungeTimer = 0;
	bool isTutorialMode;

    public TextMeshPro countdownWarningText;   

	public bool sharkGotPlayer { get; private set; }

    public enum SharkState
    {
        SharkChasingFin,
        SharkRisingUp,
        SharkChasingMouthOpen,
        SharkEatingStickman,
        SharkRisingDown,
        SharkExiting
    };

    public void DisableShark()
    {
        sharkActive = false;
        transform.position = new Vector3(99999f, 99999f, transform.position.z);
    }

    private SharkState currentSharkState = SharkState.SharkChasingFin;

    float sharkTrackingDistance = 0;

	void Start()
	{
		isTutorialMode = !GlobalSettings.TutorialCompleted();
        countdownWarningText.text = "";
        animObject = (AnimatingObject)head.GetComponent<AnimatingObject>();
        //sharkFrameDelay = animObject.frameDelay;

		if (warningSignRenderer == null)
			warningSignRenderer = warningSign.GetComponent<Renderer>();

		warningFlashDelay = warningFlashOnDelay;
        sharkStartPos = transform.localPosition;
		warningSignLocalPos = warningSign.transform.localPosition;

        fin.SetActive(true);
        head.SetActive(false);
        // TODO: BH remove this only for testing

        if (GameState.IsFirstSession())	
	        sharkActive = true;
	     
	}
    /*
    public void SetTargetPos(Vector3 playerPos)
    {
        //////Debug.Log("SETTING SHARK TO POS HERE!" + playerPos); 
        ResetShark(true);

    }
    */
    float sharkSinX = 0;

    public bool GetIsSharkActive()
    {
        return sharkActive;
    }

    public void UpdateShark (Vector3 playerPos, float barrelYPos, float playerXVelocity, bool wipingOut) 
	{
        if (wipingOut)
        {
            sharkExit = true;
        }

		if(!sharkActive)
		{
			if(warningSignRenderer.isVisible)
			{
				sharkActive = true;
				warningSignRenderer.enabled = true;
				fin.SetActive(true);
				head.SetActive(false);
			}
		}

		if(!signFlashing)
		{
            if (GlobalSettings.TutorialCompleted())
            {
                if (warningSignRenderer.isVisible)
                {
                    signFlashing = true;
                    if (GlobalSettings.soundEnabled)
                    {
                        if (warningSound != null)
                            warningSound.Play();
                    }
                }
            }
		}

		if(sharkActive)
        {
            ////////Debug.Log("playerXVelocity:" + playerXVelocity);
            if (currentSharkState == SharkState.SharkChasingFin)
			{
                ////////Debug.Log("Shark Chasing");
				Vector3 warningBeforeTranslate = warningSign.transform.position;
                transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, transform.position.y, transform.position.z);

				float lerpY = 2f;
				float trackPlayerPosYOffset = 0f;
				if(isTutorialMode)
				{
					lerpY = 6f;
					trackPlayerPosYOffset = -0.75f;
				}
					
				transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, playerPos.y + trackPlayerPosYOffset, transform.position.z), lerpY * Time.deltaTime);
                if (transform.position.y > barrelYPos * 0.7f)
                {
                    transform.position = new Vector3(transform.position.x, barrelYPos * 0.7f, transform.position.z);
                }

                sharkSinX += Time.deltaTime * 2f; 

                float sharkTrackingDistance = 4 + Mathf.Sin(sharkSinX) * 0.25f;

				if(isTutorialMode)
				{
					sharkTrackingDistance = 0.75f + Mathf.Sin(sharkSinX) * 0.25f;
				}

                if (transform.position.x > playerPos.x - sharkTrackingDistance)
                {
                    transform.position = new Vector3(playerPos.x - sharkTrackingDistance, transform.position.y, transform.position.z);
                }



				//warningSign.transform.position = warningBeforeTranslate;
                sharkXVelocity += 4f * Time.deltaTime;
                if (playerPos.x - transform.position.x > 7)
                {
                    transform.position = new Vector3(playerPos.x - 7, transform.position.y, transform.position.z);
                }

                if (sharkXVelocity > playerXVelocity * 1.3f)
                {
                    sharkXVelocity = playerXVelocity * 1.3f;
                }
                if (sharkXVelocity < playerXVelocity * 0.8f)
                {
                    sharkXVelocity = playerXVelocity  * 0.8f;
                }
                   
                Vector2 playerPos2D = new Vector2(playerPos.x, playerPos.y);
				Vector2 sharkPos2D = new Vector2(transform.position.x, transform.position.y);
                if (true)//Vector2.Distance(sharkPos2D, playerPos2D) < distanceToAttack)
                {
                    if (!attackLunge)
                    {
                        attackLungeTimer += Time.deltaTime;
                        if (attackLungeTimer > attackLungeDelay)
                        {
                            attackLunge = true;
                            currentSharkState = SharkState.SharkRisingUp;

                            fin.SetActive(false);
                            head.SetActive(true);
                            animObject.DrawFrame(12);
                            animObject.autoPlay = false;


                            sharkYVelocity += sharkAttackImpulse;
                        }
                        countDownValue = 3 - ((int)attackLungeTimer);

                        if (prevCountDownValue != countDownValue)
                        {
							#if UNITY_EDITOR
							//////Debug.Log("PLAYING WARNING SOUND HERE");
                            #endif
                            GlobalSettings.PlaySound(warningSound);
                        }
                        if(countDownValue > 0)
                        {
							if(isTutorialMode)
								countdownWarningText.text = "";
							else
	                            countdownWarningText.text = "" + countDownValue;

							if(countDownValue == 1 && isTutorialMode)
							{
								//risingUpFrameCounter++;
								animObject.autoPlay = false;
								animObject.DrawFrame(12);
								fin.SetActive(false);
								head.SetActive(true);
							}
                        }
                        else
                        {
                            countdownWarningText.text = "";
                        }

                        prevCountDownValue = countDownValue;
                    }

                }
                else
                {
                    fin.SetActive(true);
                    head.SetActive(false);
                }
			}
            else if (currentSharkState == SharkState.SharkRisingUp)
            {
                animObject.autoPlay = false;
                
                sharkFrameTimer += Time.deltaTime;
                if(sharkFrameTimer > sharkFrameDelay)
                {
					#if UNITY_EDITOR
					//////Debug.Log("INC SHARK RISING FRAME HERE" + Time.time);
                    #endif
                    risingUpFrameCounter++;
                    sharkFrameTimer = 0;
                }
                if(risingUpFrameCounter == 0)
                {
                    //.Log("Drawing Frame 13:" + Time.time);
                    animObject.DrawFrame(13);
                }
                else if(risingUpFrameCounter == 1)
                {
                    ////////Debug.Log("Drawing Frame 14:" + Time.time);
                    animObject.DrawFrame(14);
                }
                else if(risingUpFrameCounter == 2)
                {
                    risingUpFrameCounter = 0;
                    currentSharkState = SharkState.SharkChasingMouthOpen;
                    animObject.autoPlay = true;
                }

                sharkXVelocity += 18f * Time.deltaTime;
                if (sharkXVelocity > 120f)//playerXVelocity * 3f)
                {
                    sharkXVelocity = 120f;//playerXVelocity * 3f;
                }

                transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, transform.position.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, playerPos.y, transform.position.z), 2f * Time.deltaTime);

                //transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, playerPos.y, transform.position.z);
                if (transform.position.y > barrelYPos * 0.7f)
                {
                    transform.position = new Vector3(transform.position.x, barrelYPos * 0.7f, transform.position.z);
                }
            }
            else if (currentSharkState == SharkState.SharkChasingMouthOpen)
			{
               // //////Debug.Log("FUUUUUU");
				Vector3 warningBeforeTranslate = warningSign.transform.position;

                //transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, playerPos.y, transform.position.z);

                transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, transform.position.y, transform.position.z);
				transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z), 0.1f * Time.deltaTime);;

                if (transform.position.y > barrelYPos * 0.7f)
                {
                    transform.position = new Vector3(transform.position.x, barrelYPos * 0.7f, transform.position.z);
                }

				warningSign.transform.position = warningBeforeTranslate;

                sharkXVelocity += 18f * Time.deltaTime;
                if (sharkXVelocity > 30f)//playerXVelocity * 3f)
                {
                    sharkXVelocity = 30f;//playerXVelocity * 3f;
                }

                /*
                if (attackLunge)
                {
                    sharkXVelocity += 8f * Time.deltaTime;
                    if (sharkXVelocity > 60f)//playerXVelocity * 3f)
                    {
                        sharkXVelocity = 60f;//playerXVelocity * 3f;
                    }
                }
                else
                {
                    sharkXVelocity += 3f * Time.deltaTime;
                    if (sharkXVelocity > 30f)//playerXVelocity * 3f)
                    {
                        sharkXVelocity = 30f;//playerXVelocity * 3f;
                    }
                }
                */


                if (transform.position.x > playerPos.x + 2f)
                {
					#if UNITY_EDITOR
                    //////Debug.Log("Shark EXIT");
                    #endif

                    sharkExit = true;
                    countdownWarningText.text = "";
                    currentSharkState = SharkState.SharkExiting;
                    //fin.SetActive(true);
                    //head.SetActive(false);
                }

			}
            else if (currentSharkState == SharkState.SharkExiting)
            {
                if (sharkXVelocity > 17f)//playerXVelocity * 3f)
                {
                    sharkXVelocity = 17f;//playerXVelocity * 3f;
                }

                sharkXVelocity = Mathf.Max(playerXVelocity * 1.3f, sharkXVelocity);

               // //////Debug.Log("Shark Exiting sharvelocity:" + sharkXVelocity);
                if (sharkGotPlayer)
                {
                    transform.position = new Vector3(transform.position.x + 1f * Time.deltaTime, transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x + sharkXVelocity * Time.deltaTime, transform.position.y, transform.position.z);
                }
                if (transform.position.y > barrelYPos * 0.7f)
                {
                    transform.position = new Vector3(transform.position.x, barrelYPos * 0.7f, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y - 1f * Time.deltaTime, transform.position.z);
                }
                sharkExitTimer += Time.deltaTime;
                if (!sharkGotPlayer)
                {
                    if (sharkExitTimer > sharkExitDelay)
                    {
                        if (head.activeSelf)
                        {
                            fin.SetActive(true);
                            head.SetActive(false);
                            animObject.autoPlay = true;
                        }
                    }
                }
                // sharkXVelocity += 8f * Time.deltaTime;
                /*
                if (sharkXVelocity >  25f)
                {
                    sharkXVelocity = 25f;
                }
                */
            }

		}
        /*
		if(signFlashing)
		{
			warningFlashTimer += Time.deltaTime;

			if(warningFlashTimer > warningFlashDelay)
			{
				warningFlashTimer = 0;
				flashWarningOn =  !flashWarningOn;
				if(flashWarningOn)
				{
					if(!warningSignRenderer.enabled)
					{
						warningSignRenderer.enabled = true;

						if(warningSignRenderer.isVisible)
						{
							warningFlashDelay = warningFlashOffDelay;
							if(GlobalSettings.soundEnabled)
								warningSound.Play ();
						}
						else
							signFlashing = false;
					}
				}
				else
				{
					if(warningSignRenderer.enabled)
					{
						warningSignRenderer.enabled = false;
						warningFlashDelay = warningFlashOnDelay;
					}
				}
			}
		}
  */      
	}

    public bool IsSharkVisibleOnScreen()
    {
        if( (this.head.transform.GetComponent<Renderer>().isVisible) || (this.fin.transform.GetComponent<Renderer>().isVisible))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OnSharkAttack()
    {
        animObject.DrawFrame(15);
        animObject.autoPlay = false;
        sharkGotPlayer = true;
        //sharkAttackSound.Play();
        currentSharkState = SharkState.SharkExiting;
    }

    public void ResetShark(Vector3 currentPlayerPosition, bool setSharkActiveAfterReset = false)
	{
        sharkXVelocity = 1.0f;
        sharkYVelocity = 0.0f;
        sharkAttackImpulse = 4.0f;

		#if UNITY_EDITOR
        //////Debug.Log("RESETTING SHARK HERE");
        #endif

        sharkActive = setSharkActiveAfterReset;
        transform.position = new Vector3(currentPlayerPosition.x - 8f, currentPlayerPosition.y, transform.position.z);
		warningSign.transform.localPosition = warningSignLocalPos;
        currentSharkState = SharkState.SharkChasingFin;
		fin.SetActive(true);
		head.SetActive(false);

		head.transform.rotation = Quaternion.identity;
        sharkExitTimer = 0;
		warningSignRenderer.enabled = true;
        countdownWarningText.text = "!";
		warningFlashTimer = 0.0f;
		flashWarningOn = true;
		warningFlashDelay = warningFlashOnDelay;
		signFlashing = false;
        sharkExit = false;
        sharkGotPlayer = false;
        attackLunge = false;
        attackLungeTimer = 0;
		risingUpFrameCounter = 0;
		isTutorialMode = false;
	}

	public void PauseFinAnimatingObject()
	{
		AnimatingObject animatingObject = fin.GetComponent<AnimatingObject>();
		if(animatingObject != null)
			animatingObject.autoPlay = false;
	}

	public void ResumeFinAnimatingObject()
	{
		AnimatingObject animatingObject = fin.GetComponent<AnimatingObject>();
		if(animatingObject != null)
			animatingObject.autoPlay = true;
	}
}
