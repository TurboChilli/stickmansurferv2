﻿using UnityEngine;
using System.Collections;

public class SpecialPickup : MonoBehaviour {

	public PowerUp powerUp = PowerUp.None;
	public MeshRenderer powerUpRenderer = null;

	private AnimatingObject image;

	void Start()
	{
		powerUpRenderer = GetComponent<MeshRenderer>();
		image = GetComponent<AnimatingObject>();
	}


	public void SetPowerUp(PowerUp _powerUp)
	{
		powerUp = _powerUp;

		switch (powerUp)
		{
			case PowerUp.CoinMagnet:
				image.DrawFrame((int)PowerUpFrame.CoinMagnet);
			break;
			case PowerUp.CoinMultiplier:
				image.DrawFrame((int)PowerUpFrame.CoinMultiplier);
			break;
			case PowerUp.Plane:
				image.DrawFrame((int)PowerUpFrame.Plane);
			break;
			case PowerUp.ScoreMultiplier:
				image.DrawFrame((int)PowerUpFrame.ScoreMultipier);
			break;
		};
	}

	public void ResetPickup()
	{
		if (powerUpRenderer == null)
			powerUpRenderer = GetComponent<MeshRenderer>();
		if (image == null)
			image = GetComponent<AnimatingObject>();

		powerUpRenderer.enabled = true;
	}

	private enum PowerUpFrame
	{
		CoinMultiplier = 0,
		CoinMagnet = 1,
		Plane = 2,
		ScoreMultipier = 3
	};
}

