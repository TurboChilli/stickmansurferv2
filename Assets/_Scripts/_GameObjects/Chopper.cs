﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class Chopper : MonoBehaviour {

	float chopperStartLocalXPos;
	float chopperStartLocalYPos;
	float chopperLocalYPos = 0;
	float chopperSinCounter = 0;
	public Transform chopperClaw = null;
	public bool isRescueChopper = false;
	public LineRenderer chopperLineRenderer;
	public Material clawClosedMaterial;
	public Material clawOpenMaterial;

	public AudioSource chopperSound;

	// Use this for initialization
	void Start () 
	{
		chopperStartLocalXPos = transform.localPosition.x;
		chopperStartLocalYPos = transform.localPosition.y;
		chopperLocalYPos = chopperStartLocalYPos;

		if (chopperSound == null)
			chopperSound = GetComponent<AudioSource>();

		if (!GlobalSettings.soundEnabled)
			chopperSound.Stop();
		else
			chopperSound.Play();

		if(chopperClaw != null)
		{
			chopperClaw.GetComponent<Renderer>().material = clawOpenMaterial;
			Debug.Log ("CHOPPER LOCAL POS:" + transform.localPosition);
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateChopper();
	}

	public float GetXVelocity()
	{
		return chopperXVelocity;
	}

	//public void SetPlayerTransform(Transform player)
	//{
	//	playerTransform = player;
	//}

	public void AttachPlayer(Transform player)
	{
		attachXOffset = chopperClaw.position.x - GlobalSettings.playerPosition.x;//player.position.x;
		attachYOffset = chopperClaw.position.y - GlobalSettings.playerPosition.y;//player.position.y;
		playerTransform = player;
		playerAttachedToClaw = true;
	}

	public void UpdateClawRope(Vector3 topPos, Vector3 bottomPos)
	{
		//chopperLineRenderer.SetPosition(0, topPos);
		//chopperLineRenderer.SetPosition(1, bottomPos);
	}

	public void DetachPlayer(Transform playerTransform)
	{
		playerAttachedToClaw = false;
		playerTransform = null;
	}

	float attachYOffset =0;
	float attachXOffset =0;
	Transform playerTransform = null;
	bool playerAttachedToClaw = false;
	public void RetractClaw()
	{
		if(chopperClaw != null)
		{
			chopperClaw.GetComponent<Collider>().enabled = false;
			chopperClaw.GetComponent<Renderer>().enabled = false;
			//chopperLadder.localScale = new Vector3("0.5f;
		}
	}


	public void ShowClaw()
	{
		if(chopperClaw != null)
		{
			chopperClaw.GetComponent<Collider>().enabled = true;
			chopperClaw.GetComponent<Renderer>().enabled = true;
			//chopperLadder.localScale = 1f;
		}
	}

	public void FlyUp()
	{
		chopperLocalYPos += 1f * Time.deltaTime;
		chopperXVelocity = 0.2f;
	}

	public void FallDown()
	{
		chopperLocalYPos -= 3f * Time.deltaTime;
	}

	public void ResetChopper()
	{
		/*
		playerAttachedToClaw = false;
		bobbingEnabled = true;
		playerTransform = null;
		chopperXVelocity = 0;
		chopperLocalYPos = chopperStartLocalYPos;
		transform.localPosition = new Vector3(chopperStartLocalXPos, chopperStartLocalYPos, transform.localPosition.z);
		*/

		if (chopperSound == null)
			chopperSound = GetComponent<AudioSource>();

		if (!GlobalSettings.soundEnabled)
			chopperSound.Stop();
		else
			GlobalSettings.PlaySound(chopperSound);
	}

	float chopperXVelocity = 0;
	float clawMinimumHangDistanceUnderChopper = 1.6f;
	bool bobbingEnabled = true;
	void UpdateChopper()
	{
		if(bobbingEnabled || playerAttachedToClaw)
		{
			// WAVELENGTH
			chopperSinCounter += Time.deltaTime * 3f;
			float chopperYSin = Mathf.Sin(chopperSinCounter);

			// AMPLITUDE
			chopperYSin /= 5f;
			transform.localPosition = new Vector3(transform.localPosition.x + chopperXVelocity * Time.deltaTime, chopperLocalYPos + chopperYSin,transform.localPosition.z);
		}

		if(playerTransform != null)
		{
			if(playerAttachedToClaw)
			{
				chopperClaw.GetComponent<Renderer>().material = clawClosedMaterial;
				if(chopperClaw.transform.position.y < transform.position.y - clawMinimumHangDistanceUnderChopper)
				{
					chopperClaw.transform.position = new Vector3(chopperClaw.transform.position.x, chopperClaw.transform.position.y + 2f * Time.deltaTime, chopperClaw.transform.position.z);
					if(chopperClaw.transform.position.y > transform.position.y - clawMinimumHangDistanceUnderChopper)
					{
						chopperClaw.transform.position = new Vector3(chopperClaw.transform.position.x, transform.position.y - clawMinimumHangDistanceUnderChopper, chopperClaw.transform.position.z);
					}
				}
				playerTransform.position = new Vector3(chopperClaw.transform.position.x - attachXOffset, chopperClaw.transform.position.y - attachYOffset, playerTransform.position.z);

				//playerTransform.position = new Vector3(transform.position.x - attachXOffset, transform.position.y - attachYOffset, playerTransform.position.z);
			}

		}
		else if(isRescueChopper)
		{
			chopperClaw.GetComponent<Renderer>().material = clawOpenMaterial;

			if(GlobalSettings.playerPosition.y > transform.position.y - 2f)
			{
				if(transform.position.x - GlobalSettings.playerPosition.x < 8f)
				{
					transform.position = new Vector3(transform.position.x, transform.position.y + 5f * Time.deltaTime, transform.position.z);
					//chopperLocalYPos += 1f * Time.deltaTime;
					bobbingEnabled = false;
					chopperLocalYPos = transform.localPosition.y;
					//chopperSinCounter = 0;
				}
			}

			else if(GlobalSettings.playerPosition.y < transform.position.y - 3f)
			{
				if(transform.position.x - GlobalSettings.playerPosition.x < 8f)
				{
					transform.position = new Vector3(transform.position.x, transform.position.y - 2f * Time.deltaTime, transform.position.z);
					bobbingEnabled = false;
					chopperLocalYPos = transform.localPosition.y;
					//chopperLocalYPos -= 0.5f * Time.deltaTime;
					//chopperSinCounter = 1.5f;
				}
			}
			else
			{
				//bobbingEnabled = true;
			}

			//Debug.Log ("X GAP TO CHOPPER:" + (transform.position.x - GlobalSettings.playerPosition.x));

			chopperClaw.transform.position = new Vector3(chopperClaw.position.x, GlobalSettings.playerPosition.y, chopperClaw.position.z);

			if(chopperClaw.transform.position.y > transform.position.y - clawMinimumHangDistanceUnderChopper)
			{
				chopperClaw.transform.position = new Vector3(chopperClaw.position.x, transform.position.y - clawMinimumHangDistanceUnderChopper, chopperClaw.position.z);
			}
		}

		if(chopperClaw != null)
		{
			UpdateClawRope(
				new Vector3(chopperClaw.transform.position.x,chopperClaw.transform.position.y + 0.5f, chopperClaw.transform.position.z), 
				new Vector3(transform.position.x,transform.position.y - 0.5f, transform.position.z)
				);
		}
	}
}
