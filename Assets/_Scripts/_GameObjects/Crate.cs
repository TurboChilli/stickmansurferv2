﻿using UnityEngine;
using System.Collections;

public class Crate : MonoBehaviour {

	private MeshRenderer crateRenderer = null;
	private Collider crateCollider = null;

	void Start()
	{
		crateRenderer = GetComponent<MeshRenderer>();
		crateCollider = GetComponent<Collider>();
	}

	public void ResetCrate()
	{
		if (crateRenderer == null)
			crateRenderer = GetComponent<MeshRenderer>();

		if (crateCollider == null)
			crateCollider = GetComponent<Collider>();
		
		crateCollider.enabled = true;
		crateRenderer.enabled = true;
	}
}
