﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.CognitoIdentity;
using Amazon.Runtime;

public class ServerSettingSync : CommonDynamoDBContext
{
	const string refreshPrefKey = "ServSettRefreshRate";
	static bool refreshedOnceBefore = false;

	void Start()
	{
		DontDestroyOnLoad(this);

		#if UNITY_EDITOR
		DebugOnlyUpdateSurferSettingsToServer();
		#endif
	}

	public void SyncServerSettings()
	{
		if(ShouldRefresh())
		{
			RetrieveSurferSettings();
		}
	}

	bool ShouldRefresh()
	{
		if(refreshedOnceBefore == false)
		{
			refreshedOnceBefore = true;
			return true;
		}

		bool result;

		if(!PlayerPrefs.HasKey(refreshPrefKey))
		{
			result = true;
		}
		else
		{
			System.DateTime lastRefreshed = System.DateTime.UtcNow.AddYears(-1);
			string dateString = PlayerPrefs.GetString(refreshPrefKey);

			if(System.DateTime.TryParseExact(dateString, "s", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out lastRefreshed))
			{
				int mins = (int)((System.DateTime.UtcNow - lastRefreshed).TotalMinutes);

				if(mins <= GameServerSettings.SharedInstance.SurferSettings.RefreshIntervalMinutes)
					result = false;
				else
					result = true;
			}
			else
				result = true;
		}

		Debug.LogFormat("ServerSettingsSync - ShouldRefresh: {0}", result);
		return result;
	}

	void UpdateLastRefreshDate()
	{
		PlayerPrefs.SetString(refreshPrefKey, System.DateTime.UtcNow.ToString("s"));
		PlayerPrefs.Save();
	}


	void RetrieveSurferSettings()
	{
		//////Debug.Log("ServerSettingsSync - RetrieveSurferSettings");

		string surferSettingId = GameServerSettings.SharedInstance.SurferSettings.StickSurferSettingId;
		StickSurferSetting surferSettingFromServer = null;

		Context.LoadAsync<StickSurferSetting>(surferSettingId,(result)=>
			{
				if(result.Exception == null )
				{
					surferSettingFromServer = result.Result as StickSurferSetting;

					if(surferSettingFromServer != null)
					{
						//////Debug.Log("ServerSettingsSync - RetrieveSurferSettings success.");
						GameServerSettings.SharedInstance.UpdateSurferSettings(surferSettingFromServer);
						GameState.SharedInstance.RefreshUpgradePrices();

						UpdateLastRefreshDate();
					}
					else
					{
						//////Debug.Log("ServerSettingsSync - RetrieveSurferSettings null record from server.");
					}
				}
				else
				{
					Debug.LogFormat("ServerSettingsSync - RetrieveSurferSettings error: {0}", result.Exception.Message);
				}
			});
	}

	void DebugOnlyUpdateSurferSettingsToServer()
	{
		StickSurferSetting surferSettings = new StickSurferSetting();
		surferSettings.StickSurferSettingId = string.Format("DEBUG{0}", surferSettings.StickSurferSettingId);

		Context.SaveAsync<StickSurferSetting>(surferSettings, (result)=>
			{
				if(result.Exception == null)
					Debug.Log("ServerSettingsSync - DebugOnlyUpdateSurferSettingsToServer update success.");
				else
					Debug.LogFormat("ServerSettingsSync - DebugOnlyUpdateSurferSettingsToServer update error: {0}", result.Exception.Message);
			});
	}

}
