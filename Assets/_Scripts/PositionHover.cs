﻿using UnityEngine;
using System.Collections;

public class PositionHover : MonoBehaviour {

	private Vector3 origPosition;

	public float frequency = 1.0f;
	public float hoverHeight = 1.0f;

	void Start () 
	{
		origPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float lerpValue = (Mathf.Sin(Time.time * frequency) + 1.0f) * 0.5f;
		transform.localPosition = Vector3.Lerp(origPosition, origPosition + Vector3.up * hoverHeight, lerpValue);
	}
}
