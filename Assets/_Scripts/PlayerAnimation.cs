using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {
	
	int framesAcross = 8;
	float frameFraction = 0;

	bool lockAnimation = false;

	public AnimatingObject cosmeticObject;
	public AnimatingObject emotionObject;

	public Cosmetic curCosmetic = Cosmetic.None;
	private Emotion curEmotion = Emotion.Determined;

	private bool cosmeticFramesLoaded = false;
	private Vector3[] cosmeticLocalPositions;
	private Quaternion[] cosmeticLocalRotations;
	private Vector3[] cosmeticLocalScale;

	private bool emotionFramesLoaded = false;
	private Vector3[] emotionLocalPositions;
	private Quaternion[] emotionLocalRotations;
	private Vector3[] emotionLocalScale;

	private float curEmotionTimer = 0.0f;
	private Emotion defaultEmotion = Emotion.Determined;
	float emotionTimerMin = 0.75f;
	float emotionTimerMax = 1.25f;

	public Material surferFramesMaterial;
	int[] idleFrames = {0};
	int[] paddleFrames = {0,1,2,3,4,5,6,7};
	int[] standingUpFrames = {16,17,18,19,8};
	int standingUpAnimationFrame = 8;
	int[] carveLeftFrames = {9,9,10,10,11};
	int[] carveRightFrames = {10,10,9,9,8};
	int[] kickflipFrames = {32,33,34,35,36,37,9,8};
	int[] airGrabFrames = {8, 13, 14,15,15,15,15,15,15,15,15,15,15,15,14,13,8};

	int[] jetSkiFrames = {40};
	int[] bodyBoardFrames = {48};
	int[] tubeFrames = {44};
	int[] boatFrames = {53};
    int[] chilliFrames = {60};

	int[] jetSkiAirFrames = {40,41,42,43,43,43,43,43,43,43,43,43,43,43,42,41,40};
	int[] bodyBoardAirFrames = {48,49,50,51};
	int[] tubeAirFrames = {44,45,46,46,46,46,46,46,46,46,46,46,46,46,45,44};
	int[] boatAirFrames = {53};
	int[] chilliAirFrames = {60,61,62,62,62,62,62,62,62,62,62,62,62,61,60};

	int[] jetskiGrindFrames = {40};
	int[] bodyboardGrindFrames = {48};
	int[] tubeGrindFrames = {44};
	int[] boatGrindFrames = {53};
    int[] chilliGrindFrames = {60};

    int[] longboardCarveLeftFrames = {9,9,10,10,11};
	int[] longboardVehicleCarveLeftFrames = {0,0,1,2,3};
	int[] longboardVehicleCarveRightFrames = {3,3,2,1,0};

    int[] longboardCarveRightFrames = {10,10,9,9,8};
	int[] longboardGrindFrames = {10};
	int[] longboardVehicleGrindFrames = {0};
	int[] longboardAirGrabFrames = {8, 13, 14, 15,15,15,15,15,15,15,15,15,14,13,8};
	int[] longboardVehicleAirGrabFrames = {1};

	int[] windSurfBoardCarveLeftFrames = {58};
	int[] windSurfBoardVehicleCarveLeftFrames = {2};
	int[] windSurfBoardVehicleCarveRightFrames = {1};

	int[] windSurfBoardCarveRightFrames = {57};
	int[] windSurfBoardGrindFrames = {56};
	int[] windSurfBoardVehicleGrindFrames = {0};
	int[] windSurfBoardAirGrabFrames = {56};
	int[] windSurfBoardVehicleAirGrabFrames = {0};

	int[] slamFrames = {20};
	int[] boardSeparatedFrames = {8};
	int[] holdRopeLadderFrames = {21};

	int[] victoryFrames = {20};
	int[] planeFrames = {26, 27, 28, 29};
	float frameTimer = 0;
	float frameDelay = 0.04f;
	int currentFrame = 0;

    int[] orangeBoardCarveLeftFrames = {9,9,10,10,11};
    int[] orangeBoardVehicleCarveLeftFrames = {0,0,1,2,3};
    int[] orangeBoardCarveRightFrames = {10,10,9,9,8};
    int[] orangeBoardVehicleCarveRightFrames = {3,3,2,1,0};
    int[] orangeBoardKickflipFrames = {32,33,34,35,36,37,9,8};
    int[] orangeBoardVehicleKickflipFrames = {2,3,4,5,6,7,0,1};
	int[] orangeBoardAirGrabFrames = {8, 13, 14, 15,15,15,15,15,15,15,15,15,14,13,8};
    int[] orangeBoardGrindFrames = {10};
    int[] orangeBoardGrindVehicleFrames = {0};

	int[] thrusterBoardCarveLeftFrames = {9,9,10,10,11};
	int[] thrusterBoardVehicleCarveLeftFrames = {0,0,1,2,3};
	int[] thrusterBoardCarveRightFrames = {10,10,9,9,8};
	int[] thrusterBoardVehicleCarveRightFrames = {3,3,2,1,0};
	int[] thrusterBoardKickflipFrames = {32,33,34,35,36,37,9,8};
    int[] thrusterBoardVehicleKickflipFrames = {2,3,4,5,6,7,0,1};
	int[] thrusterBoardAirGrabFrames = {8, 13, 14, 15,15,15,15,15,15,15,15,15,14,13,8};
	int[] thrusterBoardGrindFrames = {10};
	int[] thrusterBoardGrindVehicleFrames = {0};

	int[] goldBoardCarveLeftFrames = {9,9,10,10,11};
	int[] goldBoardVehicleCarveLeftFrames = {0,0,1,2,3};
	int[] goldBoardCarveRightFrames = {10,10,9,9,8};
	int[] goldBoardVehicleCarveRightFrames = {3,3,2,1,0};
	int[] goldBoardKickflipFrames = {32,33,34,35,36,37,9,8};
    int[] goldBoardVehicleKickflipFrames = {2,3,4,5,6,7,0,1};
	int[] goldBoardAirGrabFrames = {8, 13, 14, 15,15,15,15,15,15,15,15,15,14,13,8};
	int[] goldBoardGrindFrames = {10};
	int[] goldBoardGrindVehicleFrames = {0};

    int[] motorcycleFrames = {54};
	int[] motorcycleAirFrames = {54, 55,47,47,47,47,47,47,47,47,47,47,47,47,55,54};
    int[] motorcycleGrindFrames = {54};

	public PowerupManager powerUpManager;

	public enum Animation
	{
		NONE = 0,
		PADDLE = 1,
		STANDING_UP = 2,
		CARVE_LEFT = 3,
		CARVE_RIGHT = 4,
		SURFBOARD_SKY = 5,
		KICKFLIP = 6,

		JETSKI = 7,
		TUBE = 8,
		BOAT = 9,
		BODYBOARD = 10,

		HOLD_ROPE = 11,
		LONGBOARD_CARVE_LEFT = 12,
		LONGBOARD_VEHICLE_CARVE_LEFT = 13,
		LONGBOARD_CARVE_RIGHT = 14,
		LONGBOARD_VEHICLE_CARVE_RIGHT = 15,
		SLAM = 16,
		VICTORY = 17,
		PLANE = 18,

		JETSKI_AIR = 19,
		TUBE_AIR = 20,
		BOAT_AIR = 21,
		BODYBOARD_AIR = 22,

        CHILLI = 23,
        CHILLI_AIR = 24,
        MOTORCYCLE = 25,
		MOTORCYCLE_AIR = 26,

        ORANGEBOARD_CARVE_LEFT = 27,
        ORANGEBOARD_VEHICLE_CARVE_LEFT = 28,
        ORANGEBOARD_CARVE_RIGHT = 29,
        ORANGEBOARD_VEHICLE_CARVE_RIGHT = 30,
        ORANGEBOARD_KICKFLIP = 31,
        ORANGEBOARD_VEHICLE_KICKFLIP = 32,
        ORANGEBOARD_SKY = 33,

		THRUSTER_CARVE_LEFT = 34,
		THRUSTER_VEHICLE_CARVE_LEFT = 35,
		THRUSTER_CARVE_RIGHT = 36,
		THRUSTER_VEHICLE_CARVE_RIGHT = 37,
		THRUSTER_KICKFLIP = 38,
		THRUSTER_VEHICLE_KICKFLIP = 39,
		THRUSTER_SKY = 40,
			
		GOLD_BOARD_CARVE_LEFT = 41,
		GOLD_BOARD_VEHICLE_CARVE_LEFT = 42,
		GOLD_BOARD_CARVE_RIGHT = 43,
		GOLD_BOARD_VEHICLE_CARVE_RIGHT = 44,
		GOLD_BOARD_KICKFLIP = 45,
		GOLD_BOARD_VEHICLE_KICKFLIP = 46,
		GOLD_BOARD_SKY = 47,

		ORANGEBOARD_GRIND = 48,
		ORANGEBOARD_VEHICLE_GRIND = 49,
		THRUSTER_GRIND = 50,
		THRUSTER_VEHICLE_GRIND = 51,
		GOLD_BOARD_GRIND = 52,
		GOLD_BOARD_VEHICLE_GRIND = 53,

		JETSKI_GRIND = 54,
		BODYBOARD_GRIND = 55,
		TUBE_GRIND = 56,
		BOAT_GRIND = 57, 
		CHILLI_GRIND = 58,
		MOTORCYCLE_GRIND = 59,

		LONGBOARD_AIR = 60,
		LONGBOARD_VEHICLE_AIR = 61,
		LONGBOARD_GRIND = 62,
		LONGBOARD_VEHICLE_GRIND = 63,

		WINDSURF_CARVE_LEFT = 64,
		WINDSURF_VEHICLE_CARVE_LEFT = 65,
		WINDSURF_CARVE_RIGHT = 66,
		WINDSURF_VEHICLE_CARVE_RIGHT = 67,

		WINDSURF_AIR = 68,
		WINDSURF_VEHICLE_AIR = 69,
		WINDSURF_GRIND = 70,
		WINDSURF_VEHICLE_GRIND = 71,

	};
	
	Animation currentAnimation = Animation.NONE;

	int[] currentAnimationFrames = null;
	int[] currentVehicleAnimationFrames = null;

	int[][] animFrameIndexes = null;

	// Use this for initialization
	void Awake () 
	{
		emotionTimerMin = GameServerSettings.SharedInstance.SurferSettings.EmotionTimerMin;
		emotionTimerMax = GameServerSettings.SharedInstance.SurferSettings.EmotionTimerMax;

		////////Debug.Log("AWAKE HERE: currentFrame = " + currentFrame + " currentAnimationFrames:" + currentAnimationFrames);
		frameFraction = 1f / framesAcross;

		int numberOfAnimations = System.Enum.GetNames(typeof(Animation)).Length;
		animFrameIndexes = new int[numberOfAnimations][];
		animFrameIndexes[(int)Animation.NONE] = idleFrames;
		animFrameIndexes[(int)Animation.PADDLE] = paddleFrames;
		animFrameIndexes[(int)Animation.STANDING_UP] = standingUpFrames;
		animFrameIndexes[(int)Animation.CARVE_LEFT] = carveLeftFrames;
		animFrameIndexes[(int)Animation.CARVE_RIGHT] = carveRightFrames;
		animFrameIndexes[(int)Animation.SURFBOARD_SKY] = airGrabFrames;
		animFrameIndexes[(int)Animation.KICKFLIP] = kickflipFrames;

		animFrameIndexes[(int)Animation.JETSKI] = jetSkiFrames;
        animFrameIndexes[(int)Animation.BOAT] = boatFrames;		
		animFrameIndexes[(int)Animation.TUBE] = tubeFrames;
        animFrameIndexes[(int)Animation.BODYBOARD] = bodyBoardFrames;	

        animFrameIndexes[(int)Animation.HOLD_ROPE] = holdRopeLadderFrames;
        animFrameIndexes[(int)Animation.LONGBOARD_CARVE_LEFT] = longboardCarveLeftFrames;
        animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_CARVE_LEFT] = longboardVehicleCarveLeftFrames;
        animFrameIndexes[(int)Animation.LONGBOARD_CARVE_RIGHT] = longboardCarveRightFrames;
        animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_CARVE_RIGHT] = longboardVehicleCarveRightFrames;
        animFrameIndexes[(int)Animation.SLAM] = slamFrames;
        animFrameIndexes[(int)Animation.VICTORY] = victoryFrames;
        animFrameIndexes[(int)Animation.PLANE] = planeFrames;

		animFrameIndexes[(int)Animation.JETSKI_AIR] = jetSkiAirFrames;
        animFrameIndexes[(int)Animation.TUBE_AIR] = tubeAirFrames;
        animFrameIndexes[(int)Animation.BOAT_AIR] = boatAirFrames;
		animFrameIndexes[(int)Animation.BODYBOARD_AIR] = bodyBoardAirFrames;

        animFrameIndexes[(int)Animation.CHILLI] = chilliFrames;
        animFrameIndexes[(int)Animation.CHILLI_AIR] = chilliAirFrames;
        animFrameIndexes[(int)Animation.MOTORCYCLE] = motorcycleFrames;
        animFrameIndexes[(int)Animation.MOTORCYCLE_AIR] = motorcycleAirFrames;

        animFrameIndexes[(int)Animation.ORANGEBOARD_CARVE_LEFT] = orangeBoardCarveLeftFrames;
        animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_CARVE_LEFT] = orangeBoardVehicleCarveLeftFrames;
        animFrameIndexes[(int)Animation.ORANGEBOARD_CARVE_RIGHT] = orangeBoardCarveRightFrames;
        animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_CARVE_RIGHT] = orangeBoardVehicleCarveRightFrames;
        animFrameIndexes[(int)Animation.ORANGEBOARD_KICKFLIP] = orangeBoardKickflipFrames;
        animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_KICKFLIP] = orangeBoardVehicleKickflipFrames;
		animFrameIndexes[(int)Animation.ORANGEBOARD_SKY] = orangeBoardAirGrabFrames;
        
		animFrameIndexes[(int)Animation.THRUSTER_CARVE_LEFT] = thrusterBoardCarveLeftFrames;
		animFrameIndexes[(int)Animation.THRUSTER_VEHICLE_CARVE_LEFT] = thrusterBoardVehicleCarveLeftFrames;
		animFrameIndexes[(int)Animation.THRUSTER_CARVE_RIGHT] = thrusterBoardCarveRightFrames;
		animFrameIndexes[(int)Animation.THRUSTER_VEHICLE_CARVE_RIGHT] = thrusterBoardVehicleCarveRightFrames;
		animFrameIndexes[(int)Animation.THRUSTER_KICKFLIP] = thrusterBoardKickflipFrames;
		animFrameIndexes[(int)Animation.THRUSTER_VEHICLE_KICKFLIP] = thrusterBoardVehicleKickflipFrames;
		animFrameIndexes[(int)Animation.THRUSTER_SKY] = thrusterBoardAirGrabFrames;

		animFrameIndexes[(int)Animation.GOLD_BOARD_CARVE_LEFT] = goldBoardCarveLeftFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_VEHICLE_CARVE_LEFT] = goldBoardVehicleCarveLeftFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_CARVE_RIGHT] = goldBoardCarveRightFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_VEHICLE_CARVE_RIGHT] = goldBoardVehicleCarveRightFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_KICKFLIP] = goldBoardKickflipFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_VEHICLE_KICKFLIP] = goldBoardVehicleKickflipFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_SKY] = goldBoardAirGrabFrames;


		animFrameIndexes[(int)Animation.ORANGEBOARD_GRIND] = orangeBoardGrindFrames;
		animFrameIndexes[(int)Animation.THRUSTER_GRIND] = thrusterBoardGrindFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_GRIND] = goldBoardGrindFrames;

		animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_GRIND] = orangeBoardGrindVehicleFrames;
		animFrameIndexes[(int)Animation.THRUSTER_VEHICLE_GRIND] = thrusterBoardGrindVehicleFrames;
		animFrameIndexes[(int)Animation.GOLD_BOARD_VEHICLE_GRIND] = goldBoardGrindVehicleFrames;


		animFrameIndexes[(int)Animation.JETSKI_GRIND] = jetskiGrindFrames;
		animFrameIndexes[(int)Animation.BODYBOARD_GRIND] = bodyboardGrindFrames;
		animFrameIndexes[(int)Animation.TUBE_GRIND] = tubeGrindFrames;
		animFrameIndexes[(int)Animation.BOAT_GRIND] = boatGrindFrames;
		animFrameIndexes[(int)Animation.CHILLI_GRIND] = chilliGrindFrames;
		animFrameIndexes[(int)Animation.MOTORCYCLE_GRIND] = motorcycleGrindFrames;

		animFrameIndexes[(int)Animation.LONGBOARD_AIR] = longboardAirGrabFrames;
		animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_AIR] = longboardVehicleAirGrabFrames;

		animFrameIndexes[(int)Animation.LONGBOARD_GRIND] = longboardGrindFrames;
		animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_GRIND] = longboardVehicleGrindFrames;
		//animFrameIndexes[(int)Animation.LONGBOARD_KICKFLIP] = longboardAirGrabFrames;

		animFrameIndexes[(int)Animation.WINDSURF_CARVE_LEFT] = windSurfBoardCarveLeftFrames;
		animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_CARVE_LEFT] = windSurfBoardVehicleCarveLeftFrames;
		animFrameIndexes[(int)Animation.WINDSURF_CARVE_RIGHT] = windSurfBoardCarveRightFrames;
		animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_CARVE_RIGHT] = windSurfBoardVehicleCarveRightFrames;
		animFrameIndexes[(int)Animation.WINDSURF_AIR] = windSurfBoardAirGrabFrames;
		animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_AIR] = windSurfBoardVehicleAirGrabFrames;
		animFrameIndexes[(int)Animation.WINDSURF_GRIND] = windSurfBoardGrindFrames;
		animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_GRIND] = windSurfBoardVehicleGrindFrames;

		SetAnimationFrame(0);

		//f'now
		//GameState.SharedInstance.CurCosmetic = (int)Cosmetic.Disguise;

		//load up cosmetics
		Cosmetic checkCosmetic = GameState.SharedInstance.CurrentCosmetic;
		if (checkCosmetic != Cosmetic.None)
		{
			curCosmetic = checkCosmetic;
			cosmeticFramesLoaded = CosmeticitemManager.LoadCosmeticItems(curCosmetic, ref cosmeticLocalPositions, ref cosmeticLocalRotations, ref cosmeticLocalScale);
			if (cosmeticFramesLoaded)
			{
				cosmeticObject.DrawFrame((int)checkCosmetic);
				cosmeticObject.transform.localPosition = cosmeticLocalPositions[0];
				cosmeticObject.transform.localRotation = cosmeticLocalRotations[0];
				cosmeticObject.transform.localScale = cosmeticLocalScale[0];
			}
			else
			{
				cosmeticObject.gameObject.SetActive(false);
				checkCosmetic = Cosmetic.None;
			}
		}
		else
			cosmeticObject.gameObject.SetActive(false);


		//load up emotions
		emotionFramesLoaded = CosmeticitemManager.LoadEmotionObjects(ref emotionLocalPositions, ref emotionLocalRotations, ref emotionLocalScale);
		if (cosmeticFramesLoaded)
		{
			emotionObject.DrawFrame((int)curEmotion);
			emotionObject.transform.localPosition = emotionLocalPositions[0];
			emotionObject.transform.localRotation = emotionLocalRotations[0];
			emotionObject.transform.localScale = emotionLocalScale[0];
		}
		else
		{
			emotionObject.gameObject.SetActive(false);
			curEmotion = defaultEmotion;
		}



	}
	// Update is called once per frame

	public void OnUpdateAnimation()
	{
		if(currentAnimationFrames != null)
		{
			UpdateAnimation(currentAnimationFrames, currentVehicleAnimationFrames);
		}


	}

	void AnimationFinished()
	{
		currentAnimationFrames = null;
		currentVehicleAnimationFrames = null;
	}

	public void LockAnimationToAnim(Animation anim)
	{
		DoAnimation(anim);
		lockAnimation = true;
	}

	public void UnlockAnimation()
	{
		lockAnimation = false;
	}

	bool holdingRope = false;
	public void SetHoldRope(bool holding)
	{
		if(holding)
		{
			DoAnimation(Animation.HOLD_ROPE);
		}
		else
		{
			SetAnimationFrame(8);
		}

		holdingRope = holding;
	}


	public void SetEmotion(Emotion newEmotion, float customTime = 0.0f)
	{
		curEmotion = newEmotion;	
		emotionObject.DrawFrame((int)curEmotion);
		if (customTime != 0.0f)
			curEmotionTimer = customTime;
		else
		{
			curEmotionTimer = UnityEngine.Random.Range(emotionTimerMin, emotionTimerMax);
		}
	}

	public void ShowSurfboardStandingUpFrame()
	{
		SetAnimationFrame(standingUpAnimationFrame);
	}
		
	public void DoAnimation(Animation doAnimation, bool forceAnimation = false)
	{
		if (lockAnimation)
			return;

		if(currentAnimation == doAnimation && forceAnimation == false)
			return;

		
        if (currentAnimation == Animation.THRUSTER_KICKFLIP || 
			currentAnimation == Animation.ORANGEBOARD_KICKFLIP ||
			currentAnimation == Animation.GOLD_BOARD_KICKFLIP)/* ||
			currentAnimation == Animation.LONGBOARD_KICKFLIP)*/
		{    
 			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_KICKFLIP];
            
            if (doAnimation == Animation.THRUSTER_CARVE_LEFT ||
                doAnimation == Animation.THRUSTER_CARVE_RIGHT ||
                doAnimation == Animation.ORANGEBOARD_CARVE_LEFT ||
                doAnimation == Animation.ORANGEBOARD_CARVE_RIGHT ||
				doAnimation == Animation.GOLD_BOARD_CARVE_LEFT ||
				doAnimation == Animation.GOLD_BOARD_CARVE_RIGHT) /* ||
				doAnimation == Animation.LONGBOARD_CARVE_LEFT ||
				doAnimation == Animation.LONGBOARD_CARVE_RIGHT)*/
			{                
				if (currentAnimationFrames != null ||
					currentVehicleAnimationFrames != null)
				{
					return;
				}
			}
		}


		//Debug.Log ("DOING AN ANIMATION HERE!" + doAnimation);
		currentAnimation = doAnimation;
		currentAnimationFrames = animFrameIndexes[(int)doAnimation];

		if(doAnimation == Animation.LONGBOARD_CARVE_LEFT || doAnimation == Animation.LONGBOARD_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];

		else if (doAnimation == Animation.LONGBOARD_AIR)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_AIR];
		else if (doAnimation == Animation.LONGBOARD_GRIND)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_GRIND];
		/*else if (doAnimation ==  Animation.LONGBOARD_KICKFLIP)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.LONGBOARD_VEHICLE_GRIND];
		*/
		else if(doAnimation == Animation.WINDSURF_CARVE_LEFT || doAnimation == Animation.WINDSURF_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if (doAnimation == Animation.WINDSURF_AIR)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_AIR];
		else if (doAnimation == Animation.WINDSURF_GRIND)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.WINDSURF_VEHICLE_GRIND];
		
        else if(doAnimation == Animation.ORANGEBOARD_CARVE_LEFT || doAnimation == Animation.ORANGEBOARD_CARVE_RIGHT)
            currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if (doAnimation == Animation.ORANGEBOARD_GRIND)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_GRIND];
		else if (doAnimation == Animation.ORANGEBOARD_KICKFLIP)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_KICKFLIP];
		
		else if(doAnimation == Animation.THRUSTER_CARVE_LEFT || doAnimation == Animation.THRUSTER_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if (doAnimation == Animation.THRUSTER_GRIND)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.THRUSTER_VEHICLE_GRIND];
		else if (doAnimation == Animation.THRUSTER_KICKFLIP)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_KICKFLIP];
		
		else if(doAnimation == Animation.GOLD_BOARD_CARVE_LEFT || doAnimation == Animation.GOLD_BOARD_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if (doAnimation == Animation.GOLD_BOARD_GRIND)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.GOLD_BOARD_VEHICLE_GRIND];
		else if (doAnimation == Animation.GOLD_BOARD_KICKFLIP)
			currentVehicleAnimationFrames = animFrameIndexes[(int)Animation.ORANGEBOARD_VEHICLE_KICKFLIP];
		else
		{
			currentVehicleAnimationFrames = null;
		}
		currentFrame = 0;
		frameTimer = 0;
	}
	

	void UpdateAnimation(int[] animationFrames, int[] vehicleAnimationFrames)
	{
		if(currentAnimation != Animation.NONE)
		{
			frameTimer += Time.deltaTime;
			if(frameTimer >= frameDelay)
			{
				frameTimer = 0;
				
				if(currentFrame >= animationFrames.Length)
				{
					if(IsLoopingAnimation(currentAnimation))
					{
						currentFrame = 0;
					}
					else
					{
						AnimationFinished();
					}
				}
				else
				{
					SetAnimationFrame(animationFrames[currentFrame]);	
					if(vehicleAnimationFrames != null)
					{
						if (currentFrame < vehicleAnimationFrames.Length)
							powerUpManager.SetPowerUpFrame(vehicleAnimationFrames[currentFrame]);
					}
				}
				currentFrame++;
			}
		}
	}

	public void UpdateFaceEmotionFrame()
	{
		if (curEmotionTimer > 0.0f)
		{
			curEmotionTimer -= Time.deltaTime;
			if (curEmotionTimer <= 0)
			{
				curEmotion = defaultEmotion;
				emotionObject.DrawFrame((int)curEmotion);
			}
		}
	}

	bool IsLoopingAnimation(Animation checkAnimation)
	{
		if(checkAnimation == Animation.PADDLE || checkAnimation == Animation.PLANE)
		{
			return true;
		}

		return false;
	}


	void SetAnimationFrame(int newFrame)
	{
		int colPos = 0;
		if(newFrame >= framesAcross)
		{
			colPos = newFrame % framesAcross;	
		}
		else
		{
			colPos = newFrame;	
		}
		int rowPos = newFrame / framesAcross;
		float offsetX = colPos * frameFraction;
		float offsetY = rowPos * frameFraction;
		surferFramesMaterial.mainTextureOffset = new Vector2 (offsetX,(1f-frameFraction) - offsetY);
		surferFramesMaterial.mainTextureScale = new Vector2 (frameFraction,frameFraction);

		if ((int)curCosmetic != -1 && curCosmetic != Cosmetic.None && cosmeticFramesLoaded)
		{
			cosmeticObject.transform.localPosition 	= cosmeticLocalPositions[newFrame];
			cosmeticObject.transform.localRotation 	= cosmeticLocalRotations[newFrame];
			cosmeticObject.transform.localScale 	= cosmeticLocalScale[newFrame];
		}

		if ((int)curEmotion != -1 && emotionFramesLoaded)
		{
			emotionObject.transform.localPosition 	= emotionLocalPositions[newFrame];
			emotionObject.transform.localRotation 	= emotionLocalRotations[newFrame];
			emotionObject.transform.localScale 		= emotionLocalScale[newFrame];
		}
	}

	public void DoJumpAnimation()
	{
		Vehicle currentVehicle = powerUpManager.GetCurrentVehicle();

		if((UnityEngine.Random.Range(1,4) % 3) == 0)
		{
			SetEmotion(Emotion.Content);
		}

		switch (currentVehicle)
		{
		case Vehicle.Surfboard:
			DoAnimation(PlayerAnimation.Animation.SURFBOARD_SKY, true);
			ForceSurfBoardCarveFrames(Animation.ORANGEBOARD_CARVE_RIGHT);
			break;
		case Vehicle.GoldSurfboard:
			DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_SKY, true);
			ForceSurfBoardCarveFrames(Animation.GOLD_BOARD_CARVE_RIGHT);
			break;
		case Vehicle.Longboard:
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_AIR, true);
			ForceSurfBoardCarveFrames(Animation.LONGBOARD_CARVE_RIGHT);
			break;
		case Vehicle.Thruster:
			DoAnimation(PlayerAnimation.Animation.THRUSTER_SKY, true);
			ForceSurfBoardCarveFrames(Animation.THRUSTER_CARVE_RIGHT);
			break;
		case Vehicle.Boat:
			DoAnimation(PlayerAnimation.Animation.BOAT_AIR, true);
			break;
		case Vehicle.WindSurfBoard:
			DoAnimation(PlayerAnimation.Animation.WINDSURF_AIR, true);
			ForceSurfBoardCarveFrames(Animation.WINDSURF_CARVE_RIGHT);
			break;
		case Vehicle.Bodyboard:
			DoAnimation(PlayerAnimation.Animation.BODYBOARD_AIR, true);
			break;
		case Vehicle.Jetski:
			DoAnimation(PlayerAnimation.Animation.JETSKI_AIR, true);
			break;
		case Vehicle.Tube:
			DoAnimation(PlayerAnimation.Animation.TUBE_AIR, true); 
			break;
		case Vehicle.Chilli:
			DoAnimation(PlayerAnimation.Animation.CHILLI_AIR, true); 
			break;
		case Vehicle.Motorcycle:
			DoAnimation(PlayerAnimation.Animation.MOTORCYCLE_AIR, true);
			break;
		}	
	}

	public void DoGrindJumpAnimation()
	{
		Vehicle currentVehicle = powerUpManager.GetCurrentVehicle();
		switch (currentVehicle)
		{
		case Vehicle.Surfboard:
			DoAnimation(PlayerAnimation.Animation.ORANGEBOARD_KICKFLIP, true);
			break;
		case Vehicle.Thruster:
			DoAnimation(PlayerAnimation.Animation.THRUSTER_KICKFLIP, true);
			break;
		case Vehicle.GoldSurfboard:
			DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_KICKFLIP, true);
			break;

		case Vehicle.Longboard:
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_AIR, true);
			break;
		case Vehicle.WindSurfBoard:
			DoAnimation(PlayerAnimation.Animation.WINDSURF_AIR, true);
			break;
		case Vehicle.Boat:
			DoAnimation(PlayerAnimation.Animation.BOAT_AIR, true);
			break;
		case Vehicle.Bodyboard:
			DoAnimation(PlayerAnimation.Animation.BODYBOARD_AIR, true);
			break;
		case Vehicle.Jetski:
			DoAnimation(PlayerAnimation.Animation.JETSKI_AIR, true);
			break;
		case Vehicle.Tube:
			DoAnimation(PlayerAnimation.Animation.TUBE_AIR, true); 
			break;
		case Vehicle.Chilli:
			DoAnimation(PlayerAnimation.Animation.CHILLI_AIR, true); 
			break;
		case Vehicle.Motorcycle:
			DoAnimation(PlayerAnimation.Animation.MOTORCYCLE_AIR, true);
			break;
		}	
	}

	public void DoLandingAnimation()
	{
		Vehicle	curVehicle = powerUpManager.GetCurrentVehicle();

		switch (curVehicle)
		{
		case Vehicle.Surfboard:
			DoAnimation(PlayerAnimation.Animation.CARVE_LEFT, true);
			currentAnimation = Animation.CARVE_LEFT;
			break;
		case Vehicle.GoldSurfboard:
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_CARVE_LEFT, true);
			currentAnimation = Animation.LONGBOARD_CARVE_LEFT;
			break;

		case Vehicle.Longboard:
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_CARVE_LEFT, true);
			currentAnimation = Animation.LONGBOARD_CARVE_LEFT;
			break;

		case Vehicle.WindSurfBoard:
			DoAnimation(PlayerAnimation.Animation.WINDSURF_CARVE_RIGHT, true);
			currentAnimation = Animation.WINDSURF_CARVE_RIGHT;
			break;

		case Vehicle.Boat:
			DoAnimation(PlayerAnimation.Animation.BOAT, true);
			currentAnimation = Animation.BOAT;
			break;
		case Vehicle.Bodyboard:
			DoAnimation(PlayerAnimation.Animation.BODYBOARD, true);
			currentAnimation = Animation.BODYBOARD;
			break;
		case Vehicle.Jetski:
			DoAnimation(PlayerAnimation.Animation.JETSKI, true);
			currentAnimation = Animation.JETSKI;
			break;
		case Vehicle.Tube:
			DoAnimation(PlayerAnimation.Animation.TUBE, true);
			currentAnimation = Animation.TUBE_GRIND;
			break;
		case Vehicle.Thruster:
			DoAnimation(PlayerAnimation.Animation.THRUSTER_CARVE_LEFT, true);
			currentAnimation = Animation.THRUSTER_CARVE_LEFT;
			break;
		case Vehicle.Chilli:
			DoAnimation(PlayerAnimation.Animation.CHILLI, true);
			currentAnimation = Animation.CHILLI;
			break;
		case Vehicle.Motorcycle:
			DoAnimation(PlayerAnimation.Animation.MOTORCYCLE, true);
			currentAnimation = Animation.MOTORCYCLE;
			break;
		};
	}

	public void DoGrindLandingAnimation()
	{
		Vehicle	curVehicle = powerUpManager.GetCurrentVehicle();

		switch (curVehicle)
		{
			case Vehicle.Surfboard:
			DoAnimation(PlayerAnimation.Animation.ORANGEBOARD_GRIND, true);
			currentAnimation = Animation.ORANGEBOARD_GRIND;
			break;
		case Vehicle.GoldSurfboard:
			DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_GRIND, true);
			currentAnimation = Animation.GOLD_BOARD_GRIND;
			break;
		case Vehicle.Boat:
			DoAnimation(PlayerAnimation.Animation.BOAT_GRIND, true);
			currentAnimation = Animation.BOAT_GRIND;
			break;
		case Vehicle.WindSurfBoard:
			DoAnimation(PlayerAnimation.Animation.WINDSURF_GRIND, true);
			currentAnimation = Animation.WINDSURF_GRIND;
			break;

		case Vehicle.Longboard:
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_GRIND, true);
			currentAnimation = Animation.LONGBOARD_GRIND;
			break;

		case Vehicle.Bodyboard:
			DoAnimation(PlayerAnimation.Animation.BODYBOARD_GRIND, true);
			currentAnimation = Animation.BODYBOARD_GRIND;
			break;
		case Vehicle.Jetski:
			DoAnimation(PlayerAnimation.Animation.JETSKI_GRIND, true);
			currentAnimation = Animation.JETSKI_GRIND;
			break;
		case Vehicle.Tube:
			DoAnimation(PlayerAnimation.Animation.TUBE_GRIND, true);
			currentAnimation = Animation.TUBE_GRIND;
			break;
		case Vehicle.Thruster:
			DoAnimation(PlayerAnimation.Animation.THRUSTER_GRIND, true);
			currentAnimation = Animation.THRUSTER_GRIND;
			break;
		case Vehicle.Chilli:
			DoAnimation(PlayerAnimation.Animation.CHILLI_GRIND, true);
			currentAnimation = Animation.CHILLI_GRIND;
			break;
		case Vehicle.Motorcycle:
			DoAnimation(PlayerAnimation.Animation.MOTORCYCLE_GRIND, true);
			currentAnimation = Animation.MOTORCYCLE_GRIND;
			break;
		};
	}

	public void DoTurnLeftAnimation()
	{
		if(powerUpManager.GetCurrentVehicle() == Vehicle.Surfboard)
		{
			DoAnimation(PlayerAnimation.Animation.ORANGEBOARD_CARVE_LEFT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.Longboard)
		{
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_CARVE_LEFT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.WindSurfBoard)
		{
			DoAnimation(PlayerAnimation.Animation.WINDSURF_CARVE_LEFT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.Thruster)
		{
			DoAnimation(PlayerAnimation.Animation.THRUSTER_CARVE_LEFT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.GoldSurfboard)
		{
			DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_CARVE_LEFT);
		}
	}

	public void DoTurnRightAnimation()
	{
		if(powerUpManager.GetCurrentVehicle() == Vehicle.Surfboard)
		{
			DoAnimation(PlayerAnimation.Animation.ORANGEBOARD_CARVE_RIGHT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.Longboard)
		{
			DoAnimation(PlayerAnimation.Animation.LONGBOARD_CARVE_RIGHT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.WindSurfBoard)
		{
			DoAnimation(PlayerAnimation.Animation.WINDSURF_CARVE_RIGHT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.Thruster)
		{
			DoAnimation(PlayerAnimation.Animation.THRUSTER_CARVE_RIGHT);
		}
		else if(powerUpManager.GetCurrentVehicle() == Vehicle.GoldSurfboard)
		{
			DoAnimation(PlayerAnimation.Animation.GOLD_BOARD_CARVE_RIGHT);
		}
	}

	void ForceSurfBoardCarveFrames(Animation doAnimation)
	{
		if(doAnimation == Animation.LONGBOARD_CARVE_LEFT || doAnimation == Animation.LONGBOARD_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if(doAnimation == Animation.WINDSURF_CARVE_LEFT || doAnimation == Animation.WINDSURF_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if(doAnimation == Animation.ORANGEBOARD_CARVE_LEFT || doAnimation == Animation.ORANGEBOARD_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if(doAnimation == Animation.THRUSTER_CARVE_LEFT || doAnimation == Animation.THRUSTER_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];
		else if(doAnimation == Animation.GOLD_BOARD_CARVE_LEFT || doAnimation == Animation.GOLD_BOARD_CARVE_RIGHT)
			currentVehicleAnimationFrames = animFrameIndexes[(int)doAnimation + 1];

		currentFrame = 0;
		frameTimer = 0;
	}
		
}
