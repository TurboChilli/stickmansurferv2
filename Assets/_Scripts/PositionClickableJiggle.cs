﻿using UnityEngine;
using System.Collections;

public class PositionClickableJiggle : MonoBehaviour {

	private Vector3 origPosition = Vector3.one;
	private float timer = 0.0f;

	public Vector3 addedPosition = Vector3.right;

	public float breakTime = 0.5f;
	public float moveTime = 0.5f;
	private bool moving = false;

	public void ResetPosition()
	{
		origPosition = transform.position;
	}

	void OnEnable () 
	{
		origPosition = transform.localPosition;
		timer = Random.Range(0.0f, breakTime);
		moving = false;
	}
	
	void Update () 
	{
		if (moving)
		{
			float positionWeight = Mathf.Sin( (timer / moveTime) * Mathf.PI);

			transform.localPosition = origPosition + addedPosition * positionWeight;
		}

		timer += Time.deltaTime;

		if (timer > breakTime && !moving)
		{
			moving = true;
			timer = 0.0f;
		}
		else if (timer > moveTime & moving)
		{
			moving = false;
			timer = 0.0f;
		}
	}
}
