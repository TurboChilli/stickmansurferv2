﻿using UnityEngine;
using System.Collections;
using System;

public class SupersonicManager : MonoBehaviour 
{
	//IOS
	private string supersonic_app_key
	{
		get
		{
			#if UNITY_IOS
			return "4c9a4a6d";
			#elif UNITY_ANDROID
			return "5a492e05";
			#endif
		}
		
	}

	private static bool isInitialised = false;
	private static bool isAdReady = false;

	private static bool showInterstitialAdFirstTime = false;
	private static float timeSinceLastInterstitialAd = 0;

	private static long timeSinceShownVideo 
	{
		get 
		{  
			return Int64.Parse(PlayerPrefs.GetString("Supersonic_TimeSinceVid", "0"));  
		}
		set 
		{
			PlayerPrefs.SetString("Supersonic_TimeSinceVid", value.ToString());
		}
	}


	public Action OnRewardedVideoFinish = null;
    public Action OnRewardedVideoStarted = null;

	bool hasShownInterstitialAd = false;


	void OnEnable()
	{
		//Interstitial Ads
		//IronSourceEvents.on += InterstitialInitSuccessEvent;
		//IronSourceEvents.onInterstitialInitFailedEvent += InterstitialInitFailEvent; 
		IronSourceEvents.onInterstitialAdReadyEvent += InterstitialReadyEvent;
		IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialLoadFailedEvent;
		IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialShowSuccessEvent; 
		IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialShowFailEvent; 
		IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
		IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
		IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

		//IronSourceEvents.onRewardedVideo += RewardedVideoInitSuccessEvent;
		IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoInitFailEvent;
		IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
		IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
	}




	void OnDisable()
	{
		hasShownInterstitialAd = false;

		IronSourceEvents.onInterstitialAdReadyEvent -= InterstitialReadyEvent;
		IronSourceEvents.onInterstitialAdLoadFailedEvent -= InterstitialLoadFailedEvent;
		IronSourceEvents.onInterstitialAdShowSucceededEvent -= InterstitialShowSuccessEvent; 
		IronSourceEvents.onInterstitialAdShowFailedEvent -= InterstitialShowFailEvent; 
		IronSourceEvents.onInterstitialAdClickedEvent -= InterstitialAdClickedEvent;
		IronSourceEvents.onInterstitialAdOpenedEvent -= InterstitialAdOpenedEvent;
		IronSourceEvents.onInterstitialAdClosedEvent -= InterstitialAdClosedEvent;

		//IronSourceEvents.onRewardedVideo += RewardedVideoInitSuccessEvent;
		IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoInitFailEvent;
		IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
		IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;
	}

	public void Initialise () 
	{

        if (!isInitialised)
		{

			showInterstitialAdFirstTime = true;
			timeSinceLastInterstitialAd = Time.time;

			string deviceUniqueId = "";

			#if UNITY_EDITOR
			deviceUniqueId = SystemInfo.deviceUniqueIdentifier;
			#elif UNITY_ANDROID
			AndroidJavaClass androidUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject unityPlayerActivity = androidUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject unityPlayerResolver = unityPlayerActivity.Call<AndroidJavaObject>("getContentResolver");
			AndroidJavaClass androidSettingsSecure = new AndroidJavaClass("android.provider.Settings$Secure");
			deviceUniqueId  = androidSettingsSecure.CallStatic<string>("getString", unityPlayerResolver, "android_id");
			#else
			deviceUniqueId = SystemInfo.deviceUniqueIdentifier;
			#endif 

			IronSource.Agent.init(supersonic_app_key, IronSourceAdUnits.REWARDED_VIDEO);

			if(!GameState.SharedInstance.HasRemovedAds())
				IronSource.Agent.init(supersonic_app_key, IronSourceAdUnits.INTERSTITIAL);

			isInitialised = true;
		}
	}

	void OnApplicationPause(bool isPaused) 
	{ 
		IronSource.Agent.onApplicationPause(isPaused);
		
		if (!isPaused)
		{
			if(timeSinceLastInterstitialAd == 0 || (Time.time - timeSinceLastInterstitialAd) > 1800f) // Has paused for more than 30 minutes.
			{
				showInterstitialAdFirstTime = true;
				timeSinceLastInterstitialAd = Time.time;
			}
		}
	}


	void RewardedVideoInitSuccessEvent ()
	{
		//////Debug.Log("Supersonic Rewarded Video Init Success");
	}

	void RewardedVideoInitFailEvent (IronSourceError obj)
	{
		//////Debug.Log(string.Format("Supersonic Rewarded Video Init Fail {0}: {1}", obj.getCode(), obj.getDescription()));
	}

	void RewardedVideoAdRewardedEvent (IronSourcePlacement obj)
	{
        //////Debug.Log("Supersonic Rewarded Video Rewarded Event");
        /// 
        #if UNITY_EDITOR
                rewardedAdClosed = true;
        #endif
        rewardedAdRewarded = true;

        if (rewardedAdClosed && rewardedAdRewarded)
        {
            if (OnRewardedVideoFinish != null)
            {
                Time.timeScale = 1.0f;
                //gm.waitingForAd = false;   v,. . c,v,v,,vc 

                AudioListener.pause = false;
                rewardedAdRewarded = false;
                rewardedAdClosed = false;
                OnRewardedVideoFinish();
            }
        }

    }

	public bool isRewardedVideoReady ()
	{
        //if(IronSource.Agent.isRewardedVideoPlacementCapped("DefaultRewardedVideo"))
        //	return false;
        //else	
#if UNITY_EDITOR
        return true;
#endif
        return IronSource.Agent.isRewardedVideoAvailable();
	}

    void RewardedVideoAdOpenedEvent()
    {
        if (OnRewardedVideoStarted != null)
            OnRewardedVideoStarted();
    }

	void RewardedVideoAdClosedEvent ()
	{
        rewardedAdClosed = true;
        //////Debug.Log("Supersonic Rewarded Video Close Event");

        //if (OnRewardedVideoFinish != null)
        //	OnRewardedVideoFinish();

        Time.timeScale = 1.0f;
        AudioListener.pause = false;

        if (rewardedAdClosed && rewardedAdRewarded)
        {
            rewardedAdRewarded = false;
            rewardedAdClosed = false;

            if (OnRewardedVideoFinish != null)
                OnRewardedVideoFinish();
        }
    }

    bool rewardedAdClosed = false;
    bool rewardedAdRewarded = false;

    public bool ShowRewardedVideo()
	{
        rewardedAdClosed = false;
        rewardedAdRewarded = false;

        #if UNITY_EDITOR
            timeSinceShownVideo = ToUnixTime(System.DateTime.UtcNow);
            timeSinceShownVideo = ToUnixTime(System.DateTime.UtcNow);

            showInterstitialAdFirstTime = true;
            timeSinceLastInterstitialAd = Time.time;
            RewardedVideoAdRewardedEvent(null);
            return true;
        #endif

        Debug.Log ("Attempt to show Rewarded video ------------------------");
		bool isReady = isRewardedVideoReady();
		if(!isReady)
		{
			//////Debug.Log("Rewarded video not ready!");
			return false;
		}
		else
		{
			//////Debug.Log("Showing Rewarded video success!");
			IronSource.Agent.showRewardedVideo();
			timeSinceShownVideo = ToUnixTime(System.DateTime.UtcNow);

			showInterstitialAdFirstTime = true;
			timeSinceLastInterstitialAd = Time.time;

			return true;
		}
	}


	public bool isInterstisialReady ()
	{
		if (!GameState.SharedInstance.HasRemovedAds())
			return (isAdReady);
		else
			return false;
	}

	public static long ToUnixTime(System.DateTime date)
	{
		var epoch = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
	    return System.Convert.ToInt64((date - epoch).TotalSeconds);
	}

	public bool isVideoCooldownDone()
	{
		long timeNow = ToUnixTime(System.DateTime.UtcNow);
		return (timeNow - timeSinceShownVideo >= GameServerSettings.SharedInstance.SurferSettings.VideoAdCooldown);
	}

	public void LoadInterstisial()
	{
		if(GameState.SharedInstance.HasRemovedAds())
		{
			isAdReady = false;
			return;
		}
		
		if (!isAdReady)
		{
			Debug.Log ("Loading Interstitial");
			isAdReady = false;
				IronSource.Agent.loadInterstitial();
		}
		else
			Debug.Log ("Interstitial already loaded");
	}

	public bool ShowInterstitial()
	{
		if(!CanShowInterstitialAd())
			return false;
		
		Debug.Log ("Attempt to show Interstitial");
		bool isReady = isInterstisialReady();

		if (isReady)
		{
			Debug.Log ("Interstitial success!");

			IronSource.Agent.showInterstitial();

			hasShownInterstitialAd = true;
			showInterstitialAdFirstTime = false;
			timeSinceLastInterstitialAd = Time.time;
		}
		else
			Debug.Log ("Interstitial Failure!");

		return isReady;
	}


	public bool HasShownInterstitialAd()
	{
		return hasShownInterstitialAd;
	}

	//Callbacks
	/*
	* Invoked when Interstitial initialization process completes successfully.
	*/
	void InterstitialInitSuccessEvent () 
	{
		Debug.Log ("Interstitial Init Success Event");
		LoadInterstisial();
	}

	public bool CanShowInterstitialAd()
	{
        Debug.Log("Checking removed ads");
		if(GameState.SharedInstance.HasRemovedAds())
			return false;

        Debug.Log("Checking showInterstitialAdFirstTime:" + showInterstitialAdFirstTime);
        if (showInterstitialAdFirstTime)
		{
			float secondsSinceLastShown = Time.time - timeSinceLastInterstitialAd;

			if(GameState.IsFirstSession() && secondsSinceLastShown < GameServerSettings.SharedInstance.SurferSettings.SecsBeforeFirstInterstitialShownFirstSession)
				return false;
			else if(secondsSinceLastShown < GameServerSettings.SharedInstance.SurferSettings.SecsBeforeFirstInterstitialShown)
				return false;
		}
		
		//if(IronSource.Agent.isInterstitialPlacementCapped("DefaultInterstitial"))
	//	return false;

		if(Application.isEditor)
			return false;

		if(GameServerSettings.SharedInstance.SurferSettings.ShowAdsAfterTut)
		{
			TutStageType curTutorialStage = GameState.SharedInstance.TutorialStage;

			if(curTutorialStage != TutStageType.Finished)
			{
				return false;
			}
		}

		return isAdReady;
	}

	void InterstitialReadyEvent()
	{
		//////Debug.Log("Interstitial Ready Event");
		isAdReady = true;
	}

	void InterstitialInitFailEvent (IronSourceError error) 
	{
		Debug.Log ("InterstitialInitFailEvent, code: " + error.getCode()+ ", description : " + error.getDescription());
	}

	/* 
	* Invoked when the initialization process has failed.
	* @param description - string - contains information about the failure.
	*/
	void InterstitialLoadFailedEvent (IronSourceError error) 
	{
		Debug.Log ("InterstitialLoadFailedEvent, code: " + error.getCode()+ ", description : " + error.getDescription());
	}
		
	/* 
	* Invoked right before the Interstitial screen is about to open.
	*/
	void InterstitialShowSuccessEvent() 
	{
		Debug.Log ("Showing interstitial now!");
		//Time.timeScale = 0.0f;
		//gm.waitingForAd = true;
	}

	/* Invoked when the ad fails to show.
	* @param description - string - contains information about the failure.
	*/
		void InterstitialShowFailEvent(IronSourceError error) 
	{
		Debug.Log ("InterstitialShowFailEvent, code :  " + error.getCode() + ", description : " + error.getDescription());
        Time.timeScale = 1.0f;
        //gm.waitingForAd = false;

        AudioListener.pause = false;
    }

	/* Invoked when end user clicked on the interstitial ad */
	void InterstitialAdClickedEvent () 
	{
		Debug.Log ("InterstitialAdClickedEvent");
		//Time.timeScale = 1.0f;
		//gm.waitingForAd = false;
	}

	void InterstitialAdOpenedEvent ()
	{
		Debug.Log ("InterstitialAdOpenedEvent");
		//Time.timeScale = 1.0f;
		//gm.waitingForAd = false;
	}

	/* 
	* Invoked when the interstitial ad closed and the user goes back to the application screen.
	*/
	void InterstitialAdClosedEvent () 
	{
		Debug.Log ("InterstitialAdClosedEvent");
		Time.timeScale = 1.0f;
		//gm.waitingForAd = false;

		AudioListener.pause = false;
		isAdReady = false;
		LoadInterstisial();
	}
}
