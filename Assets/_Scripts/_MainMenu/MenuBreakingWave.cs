﻿using UnityEngine;
using System.Collections;

public class MenuBreakingWave : MonoBehaviour {

    float scaleVal = 0;
    float startY = 0;
	// Use this for initialization
    Vector3 startPos = new Vector3(0,0,0);
    Vector3 startScale = new Vector3(0,0,0);
    Vector3 barrelParentStartPos = new Vector3(0,0,0);
    float scaleIncrease = 0.02f;
    float scaleDecrease = 0.05f;

    public AudioSource barrelSound;

    public Transform breakingWaveParent;
    bool scalingUp = true;
    public Material beachWaveMaterial;
    float scalingUpTimer = 0;
    float scalingUpDelay = 15;

	void Start () 
	{
        startPos = transform.position;
        barrelParentStartPos = breakingWaveParent.transform.position;
        breakingWaveParent.localScale = new Vector3(breakingWaveParent.localScale.x,0.5f,breakingWaveParent.localScale.z);
        startScale = breakingWaveParent.localScale;
        transparency = 0;
        beachWaveMaterial.color = new Color(1, 1, 1, transparency);
        //transform.localScale = new Vector3(0, 0.3f, 0);

        if (!GlobalSettings.soundEnabled)
			barrelSound.Stop();

	}
	
    float transparency = 1;
	// Update is called once per frame
	void Update () 
	{

		if (!GlobalSettings.soundEnabled)
			barrelSound.Stop();
		else
		{
			if (!barrelSound.isPlaying)
				barrelSound.Play();
		}

        if(scalingUp)
        {
            scalingUpTimer += Time.deltaTime;
            if(scalingUpTimer > scalingUpDelay)
            {
                scalingUp = false;
                scalingUpTimer = 0;
            }
        }


        if(scalingUp)
        {
            if (transparency  < 1)
            {   
                transparency += Time.deltaTime / 1f;
            }
            else
            {
                transparency = 1;
            }
            beachWaveMaterial.color = new Color(1, 1, 1, transparency);
            ////////Debug.Log("SCALE UP:" + breakingWaveParent.localScale + "TransPos:" + transform.position);
            if (breakingWaveParent.localScale.y < 2)
            {
                breakingWaveParent.localScale += new Vector3(scaleIncrease * Time.deltaTime, scaleIncrease * Time.deltaTime * 25f, 0);
            }
            
        }
        else

        {
            if (transparency > 0)
            {   
                transparency -= Time.deltaTime / 15f;
            }
            else
            {
                transparency = 0;
            }
            beachWaveMaterial.color = new Color(1, 1, 1, transparency);
            ////////Debug.Log("SCALING DOWN!");
            breakingWaveParent.localScale -= new Vector3(0, scaleIncrease * Time.deltaTime * 5f, 0);
            breakingWaveParent.transform.position = new Vector3(breakingWaveParent.transform.position.x, breakingWaveParent.transform.position.y - 2f * Time.deltaTime, breakingWaveParent.transform.position.z);

            //
        }

        if(breakingWaveParent.localScale. y <=0)
        {
            ResetToStartPos();
        }
        transform.position = new Vector3(transform.position.x + 70f * Time.deltaTime, transform.position.y - 0.1f * Time.deltaTime, transform.position.z);
       

	}

    void ResetToStartPos()
    {
        breakingWaveParent.transform.position = barrelParentStartPos;
        ////////Debug.Log("RESETTING MENU WAVE HERE!");
        scalingUp = true;
        scalingUpTimer = 0;
        transform.localPosition = new Vector3(282,1.2f,-0.5f);
        breakingWaveParent.localScale = startScale;
    }

}
