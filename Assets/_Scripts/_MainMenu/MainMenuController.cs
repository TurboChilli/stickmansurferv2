﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using TMPro;

public class MainMenuController : MonoBehaviour {

    public Camera mainCamera;
	public Camera cratePrizeCamera;
    public Camera treasureVaultUICamera;

    public GameObject specialOfferButton;
	public GameObject settingsButton;
	public GameObject androidAchievementsButton;
	public GameObject androidLeaderboardButton;

    public GameObject locationBackground;

    public CoinBar coinBarTotal;
	public NumericDisplayBar energyBar;

    public AudioSource clickSound;

	public SettingsDialog settingsDialog;

	public TutStageType curTutorialStage;

	public EnergyDrinkDialog jeepEnergyDrinkDialog;
    public JeepTimer jeepTimer;
    public HalfPipeTimer halfpipeTimer;
    public GameConsoleTimer gameConsoleTimer;

    public GameObject jeepActivityMarker;
    public GameObject jeepBowserOn;
    public GameObject jeepBowserOff;

	public Material jeepDefaultMaterial;
    public Material jeepBlackMaterial;
    public Material jeepQuadBikeMaterial;
    public Renderer jeepRenderer;

    private int currentLocation = 0;
    public NotificationManager notifications;
	public TreasureVaultUIController treasureVaultUIController;
	public TreasureVaultUITimer treasureVaultUITimer;

	public RotationClickableJiggle goButtonJiggleEffect;
	public RotationClickableJiggle treasureButtonJiggleEffect;
	public RotationClickableJiggle shopButtonJiggleEffect;

	public GameObject theCrate;
	public DialogPanel watchAndWinDialog;
	public GameObject watchVideoPanelButton;

	public AudioSource soundCrate;
    public AudioSource reggaeMusic;
    public AudioSource inGameMusic;
    public AudioSource miniGameMusic;

    public AudioSource waveSounds;

	public ParticleSystem crateExplodeParticles;
	public MenuChopper menuCrateChopper;

    public BackgroundBird theBird;

    public GameObject goButtonHand;
    public GameObject treasureHand;
	public GameObject shopHand;
	public GameObject jeepHand;
	public GameObject shackHand;
    
    public SpotlightEffect spotlightEffect;

	[Space(20)]
	[Header("Main Menu Buttons")]
	public GameObject goButtonParent;
	public GameObject goButtonEnabled;
	public GameObject goButtonDisabled;
	public TMP_Text goButtonDisabledText;

	public GameObject shopButtonParent;
	public GameObject shopButtonNotification;
	public GameObject shopButtonEnabled;
	public GameObject shopButtonDisabled;

	public GameObject jeepButton;
	public GameObject shackButton;
	public GameObject shackMarkerNotification;

    private SupersonicManager superSonicManager;

    public GameObject treasureButtonParent;
    public TextMeshPro treasureButtonTimerText;
	public GameObject treasureButtonNotfication;
	public GameObject treasureButtonEnabled;
	public GameObject treasureButtonDisabled;

    public GameObject surfboardWhite;
    public GameObject surfboardOrange;
    public GameObject surfboardGold;

	bool loadingJeepScene = false;
	float mainMenuSceneDisableClickCooldown = 0.3f;
	private StarterPackDialog starterPackDialog;
	public GameCenterPluginManager gcpm;
	bool checkTutorialCompleteReload = false;
	EnergyDrinkShopController energyDrinkShopController = null;

    public void SetTreasureButtonTimerText(string theTimerText)
    {
		if(!string.Equals(treasureButtonTimerText.text, theTimerText, StringComparison.InvariantCultureIgnoreCase))
	        treasureButtonTimerText.text = theTimerText;
    }

    public void SetTreasureButtonNotification(bool state)
    {
		if (GameState.SharedInstance.TutorialStage == TutStageType.Finished)
			treasureButtonNotfication.SetActive(state);
		else
			treasureButtonNotfication.SetActive(false);

    }

	public void EnableMainMenuDisableClickCoolDown()
	{
		mainMenuSceneDisableClickCooldown = 0.2f;
	}

	void OnEnable () 
	{
		jeepEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful += JeepEnergyDrinkDialogEnergyDrinkSpendSuccessful;
		IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
		//IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoClosedEventHandler;

		AlertDialogUtil.RegisterAlertButtonClickHandler(EtceteraManagerAlertButtonClickedEvent);

		if(energyDrinkShopController == null)
		{
			energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();

			if(energyDrinkShopController != null)
			{
				energyDrinkShopController.OnPurchaseSuccessful += EnergyDrinkShopOnPurchaseSuccessfulHandler;
			}
		}
	}


	void OnDisable () 
	{
		jeepEnergyDrinkDialog.OnEnergyDrinkSpendSuccessful -= JeepEnergyDrinkDialogEnergyDrinkSpendSuccessful;
		IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
        //SupersonicEvents.onRewardedVideoAdClosedEvent -= RewardedVideoClosedEventHandler;

		AlertDialogUtil.DeregisterAlertButtonClickHandler(EtceteraManagerAlertButtonClickedEvent);

		if(energyDrinkShopController != null)
		{
			energyDrinkShopController.OnPurchaseSuccessful -= EnergyDrinkShopOnPurchaseSuccessfulHandler;
		}
	}


	void EnergyDrinkShopOnPurchaseSuccessfulHandler (int numberOfEnergyDrinksPurchased)
	{
		UpdateEnergyDrinkBar(GameState.SharedInstance.EnergyDrink);
	}

	void EtceteraManagerAlertButtonClickedEvent (string obj)
	{
		if(checkTutorialCompleteReload)
		{
			checkTutorialCompleteReload = false;
			TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();
		}
	}


	void Start ()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        if (gcpm == null)
			gcpm = FindObjectOfType<GameCenterPluginManager>();

		TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();

		ServerSavedGameStateSync.Sync();

		ServerSettingSync serverSettingsSync = FindObjectOfType<ServerSettingSync>();
		if(serverSettingsSync != null)
		{
			/*var loggingConfig = Amazon.AWSConfigs.LoggingConfig;
			loggingConfig.LogTo = Amazon.LoggingOptions.UnityLogger;
			loggingConfig.LogMetrics = true;
			loggingConfig.LogResponses = Amazon.ResponseLoggingOption.Always;
			loggingConfig.LogResponsesSizeLimit = 4096;
			loggingConfig.LogMetricsFormat = Amazon.LogMetricsFormatOption.JSON;

			Amazon.AWSConfigs.HttpClient = Amazon.AWSConfigs.HttpClientOption.UnityWebRequest;*/

			serverSettingsSync.SyncServerSettings();
		}

		if(jeepEnergyDrinkDialog == null)
			jeepEnergyDrinkDialog = FindObjectOfType<EnergyDrinkDialog>();


		if(starterPackDialog == null)
			starterPackDialog = FindObjectOfType<StarterPackDialog>();

		if(jeepEnergyDrinkDialog != null)
		{
			string localisedDialogTitle = string.Format("{0} {1}", GameLanguage.LocalizedText("JEEP"), GameLanguage.LocalizedText("RE-FUEL"));
			jeepEnergyDrinkDialog.SetTitleText(localisedDialogTitle);
		}

        surfboardWhite.SetActive(false);
        surfboardGold.SetActive(false);
        surfboardOrange.SetActive(false);

        if (GameState.SharedInstance.CurrentJeep == JeepType.Black)
			jeepRenderer.material = jeepBlackMaterial;
        else if(GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
            jeepRenderer.material = jeepQuadBikeMaterial;
		else
			jeepRenderer.material = jeepDefaultMaterial;
		

        //GameState.SharedInstance.CurrentVehicle = Vehicle.Thruster;
        if(GameState.SharedInstance.CurrentJeep != JeepType.QuadBike)
        {
            if (GameState.SharedInstance.CurrentVehicle == Vehicle.Thruster)
            {
                surfboardWhite.SetActive(true);
            }
            else if (GameState.SharedInstance.CurrentVehicle == Vehicle.GoldSurfboard)
            {
                surfboardGold.SetActive(true);
            }
            else
            {
                surfboardOrange.SetActive(true);
            }
        }
        ////////Debug.Log("GlobalSettings.currentMenuMusicIndex:" + GlobalSettings.currentMenuMusicIndex);
        if (GlobalSettings.musicEnabled)
        {
	        if (GlobalSettings.currentMenuMusicIndex == 0)
	        {
				GlobalSettings.PlayMusicFromTimePoint(reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
	        }
	        else if (GlobalSettings.currentMenuMusicIndex == 1)
	        {
				GlobalSettings.PlayMusicFromTimePoint(miniGameMusic, GlobalSettings.miniGameMusicPlayPosition);
	        }
	        else if (GlobalSettings.currentMenuMusicIndex == 2)
	        {
				GlobalSettings.PlayMusicFromTimePoint(inGameMusic, GlobalSettings.inGameMusicPlayPosition);
	        }
        }
        else 
        {
			reggaeMusic.Stop();
			miniGameMusic.Stop();
			inGameMusic.Stop();
			waveSounds.Stop();
        }

		if(superSonicManager == null)
		{
	        superSonicManager = FindObjectOfType<SupersonicManager>();
	        superSonicManager.Initialise();
	        superSonicManager.LoadInterstisial();
		}

		AnalyticsAndCrossPromoManager.SharedInstance.Initialise();

		if(menuCrateChopper == null)
			menuCrateChopper = FindObjectOfType<MenuChopper>();

		if(settingsDialog == null)
			settingsDialog = FindObjectOfType<SettingsDialog>();

		if (treasureVaultUIController == null)
			treasureVaultUIController = FindObjectOfType<TreasureVaultUIController>();
		if (treasureVaultUITimer == null)
			treasureVaultUITimer = FindObjectOfType<TreasureVaultUITimer>();

        jeepTimer.StartTimer();
        halfpipeTimer.Start();
		gameConsoleTimer.Start();

        //Application.targetFrameRate = 60;

        if (Utils.IphoneXCheck())
        {
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 100, 35f, coinBarTotal.transform, mainCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 290, 35f, energyBar.transform, mainCamera);
	        //UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 490, 0f, scoreBarTotal.transform, mainCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 60, 40f, settingsButton.transform, mainCamera);
        }
        else
        {
	        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, coinBarTotal.transform, mainCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 290, 0f, energyBar.transform, mainCamera);
	        //UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 490, 0f, scoreBarTotal.transform, mainCamera);
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 60, 0f, settingsButton.transform, mainCamera);
		}
		if(GameState.SharedInstance.HasBoughtStarterPack || ShouldShowStarterPackButton() == false)
			specialOfferButton.SetActive(false);

		#if UNITY_IOS
		androidAchievementsButton.SetActive(false);
		androidLeaderboardButton.SetActive(false);

		if (Utils.IphoneXCheck())
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 180, 40f, specialOfferButton.transform, mainCamera);
		else
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 180, 0f, specialOfferButton.transform, mainCamera);

		#else
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 220, 0f, androidAchievementsButton.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 140, 0f, androidLeaderboardButton.transform, mainCamera);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 340, 10f, specialOfferButton.transform, mainCamera);
		#endif

		InitialiseMainMenuButtons();
        		
		//scoreBarTotal.mainText.text = ScoreBar.ShortenScore( GameState.SharedInstance.Score);
		//energyBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
        //coinBarTotal.DisplayCoins(GameState.SharedInstance.Cash);
        UpdateEnergyDrinkBar(GameState.SharedInstance.EnergyDrink);
        UpdateCoinBar(GameState.SharedInstance.Cash);

		NavigateToLocation(GlobalSettings.MainMenuLocation.MainMenu);

        /*UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 90, 120, 
                                                treasureButtonTimerText.transform, mainCamera); 
                                                */
		crateExplodeParticles.gameObject.SetActive(false);
		crateExplodeParticles.Stop();

		if (spotlightEffect == null)
			spotlightEffect = FindObjectOfType<SpotlightEffect>();


		#if UNITY_ANDROID
		if (spotlightEffect != null)
			spotlightEffect.gameObject.SetActive(false);
		#endif

		SetupTutorial();

		if (curTutorialStage == TutStageType.Finished && !GameState.SharedInstance.HasPlayedHalfPipe && GameState.SharedInstance.IsHalfPipeTimerDone())
			shackHand.SetActive(true);	
		else
			shackHand.SetActive(false);	

		if(!GlobalSettings.IsShowingStarterPackDialog())
		{
			ShowMainMenuInterstitial();
		}
	}

	bool ShouldShowStarterPackButton()
	{
		if(GameState.SharedInstance.HasBoughtStarterPack)
			return false;
		
		if(GameState.SharedInstance.NumberOfStarterPackDialogsShown < GameServerSettings.SharedInstance.SurferSettings.NumberOfTimesStarterPackPopupIsShown)
			return false;

		if(GameState.SharedInstance.NumberOfStarterPackOfferButtonShown > 20)
			return false;
		
		UpgradeItem mullet = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.HeadMullet);
		if(mullet != null && mullet.UpgradeCompleted())
		{
			return false;
		}

		UpgradeItem jeepFuel = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepFuel);
		if(jeepFuel != null && jeepFuel.UpgradeCompleted())
		{
			return false;
		}

		TutStageType curTutorialStage = GameState.SharedInstance.TutorialStage;
		if(curTutorialStage != TutStageType.Finished)
		{
			return false;
		}
			
		if((GameState.SharedInstance.MainMenuMenuVisit % 4) != 0)
		{
			return false;
		}

		GameState.SharedInstance.MainMenuMenuVisit += 1;
		GameState.SharedInstance.NumberOfStarterPackOfferButtonShown += 1;

		return true;
	}

	void ShowMainMenuInterstitial()
	{
		if(superSonicManager == null)
			superSonicManager = FindObjectOfType<SupersonicManager>();

		if(superSonicManager == null)
			return;

		if(GameServerSettings.SharedInstance.SurferSettings.EnableInterstitialMainMenu &&
		 superSonicManager.CanShowInterstitialAd())
		{
			AudioListener.pause = true;
			superSonicManager.ShowInterstitial();
		}
	}

    public void UpdateEnergyDrinkBar(int newTotal)
    {
        energyBar.DisplayValue(newTotal);
    }

    public void UpdateCoinBar(int newTotal)
    {
        coinBarTotal.DisplayCoins(newTotal);
    }

    public void UpdateLimitedTimeOffer()
    {
		if(ShouldShowStarterPackButton() == false)
			specialOfferButton.SetActive(false);
    }

    public void DisableJeepTimer()
    {
        jeepTimer.InitializeTimer(0);
    }

	bool StarterPackDialogShowing()
	{
		if(starterPackDialog == null)
			return false;

		return starterPackDialog.IsShowingPanel();
	}

	public void SetupTutorial()
	{
		curTutorialStage = GameState.SharedInstance.TutorialStage;

		if (curTutorialStage == TutStageType.MainMenuChestClose)
			curTutorialStage = TutStageType.FirstShopVisit;

		if (curTutorialStage == TutStageType.MainMenuGoButton || curTutorialStage == TutStageType.FirstGame) 
		{
			coinBarTotal.gameObject.SetActive(false);
			//scoreBarTotal.gameObject.SetActive(false);

			treasureButtonTimerText.gameObject.SetActive(false);
			treasureButtonNotfication.gameObject.SetActive(false);
			treasureButtonEnabled.gameObject.SetActive(false);
			treasureButtonDisabled.gameObject.SetActive(false);

			energyBar.gameObject.SetActive(false);

			shopButtonParent.gameObject.SetActive(false);

			//settingsButton.gameObject.SetActive(false);

			jeepButton.GetComponent<Collider>().enabled = false;
			jeepActivityMarker.gameObject.SetActive(false);
			jeepTimer.gameObject.SetActive(false);

			menuCrateChopper.gameObject.SetActive(false);
			shackMarkerNotification.gameObject.SetActive(false);
			shackButton.GetComponent<Collider>().enabled  = false;

			treasureButtonJiggleEffect.enabled = false;
			treasureHand.SetActive(false);

			shopButtonJiggleEffect.enabled = false;
			shopHand.SetActive(false);

			goButtonJiggleEffect.enabled = true;
			goButtonHand.SetActive(true);

			jeepHand.SetActive(false);

			if(spotlightEffect != null)
				spotlightEffect.gameObject.SetActive(false);


		}
		else if (curTutorialStage == TutStageType.MainMenuChest)
		{
			coinBarTotal.gameObject.SetActive(true);
			//scoreBarTotal.gameObject.SetActive(true);
			energyBar.gameObject.SetActive(true);

			treasureButtonTimerText.gameObject.SetActive(true);
			treasureButtonNotfication.gameObject.SetActive(false);
			treasureButtonEnabled.gameObject.SetActive(true);
			treasureButtonDisabled.gameObject.SetActive(true);

			shopButtonParent.gameObject.SetActive(false);

			//settingsButton.gameObject.SetActive(false);

			jeepButton.GetComponent<Collider>().enabled = false;
			jeepActivityMarker.gameObject.SetActive(false);
			jeepTimer.gameObject.SetActive(false);

			menuCrateChopper.gameObject.SetActive(false);
			shackMarkerNotification.gameObject.SetActive(false);
			shackButton.GetComponent<Collider>().enabled  = false;

			treasureButtonJiggleEffect.enabled = true;
			treasureHand.SetActive(true);

			shopButtonJiggleEffect.enabled = false;
			shopHand.SetActive(false);

			goButtonJiggleEffect.enabled = false;
			goButtonHand.SetActive(false);

			jeepHand.SetActive(false);

			#if !UNITY_ANDROID
			spotlightEffect.gameObject.SetActive(true);
			spotlightEffect.marker.transform.position = treasureButtonEnabled.transform.position;
			spotlightEffect.ToggleEffect(true, 100.0f, true);
			#endif

		}
		else if (curTutorialStage == TutStageType.MainMenuShop || curTutorialStage == TutStageType.FirstShopVisit)
		{
			coinBarTotal.gameObject.SetActive(true);
			//scoreBarTotal.gameObject.SetActive(true);
			energyBar.gameObject.SetActive(true);

			treasureButtonTimerText.gameObject.SetActive(true);
			treasureButtonNotfication.gameObject.SetActive(false);
			treasureButtonEnabled.gameObject.SetActive(true);
			treasureButtonDisabled.gameObject.SetActive(true);

			shopButtonParent.gameObject.SetActive(true);

			//settingsButton.gameObject.SetActive(false);

			jeepButton.GetComponent<Collider>().enabled = false;
			jeepActivityMarker.gameObject.SetActive(false);
			jeepTimer.gameObject.SetActive(false);

			menuCrateChopper.gameObject.SetActive(false);
			shackMarkerNotification.gameObject.SetActive(false);
			shackButton.GetComponent<Collider>().enabled  = false;

			treasureButtonJiggleEffect.enabled = false;
			treasureHand.SetActive(false);

			shopButtonJiggleEffect.enabled = true;
			shopHand.SetActive(true);

			goButtonJiggleEffect.enabled = false;
			goButtonHand.SetActive(false);

			jeepHand.SetActive(false);

			#if !UNITY_ANDROID
			spotlightEffect.gameObject.SetActive(true);
			spotlightEffect.marker.transform.position = shopButtonEnabled.transform.position;
			spotlightEffect.ToggleEffect(true, 100.0f, true);
			#endif

		}
		else if (curTutorialStage == TutStageType.PostShopVisit)
		{
			coinBarTotal.gameObject.SetActive(true);
			//scoreBarTotal.gameObject.SetActive(true);
			energyBar.gameObject.SetActive(true);

			treasureButtonTimerText.gameObject.SetActive(true);
			treasureButtonNotfication.gameObject.SetActive(false);
			treasureButtonEnabled.gameObject.SetActive(true);
			treasureButtonDisabled.gameObject.SetActive(true);

			shopButtonParent.gameObject.SetActive(true);

			//settingsButton.gameObject.SetActive(false);

			jeepButton.GetComponent<Collider>().enabled = false;
			jeepActivityMarker.gameObject.SetActive(false);
			jeepTimer.gameObject.SetActive(false);

			menuCrateChopper.gameObject.SetActive(false);
			shackMarkerNotification.gameObject.SetActive(false);
			shackButton.GetComponent<Collider>().enabled  = false;

			if (treasureVaultUITimer.TimeStatus)
			{
				treasureButtonJiggleEffect.enabled = true;
				treasureButtonDisabled.SetActive(false);
			}
			else
			{
				treasureButtonJiggleEffect.enabled = false;
				treasureButtonDisabled.SetActive(true);
			}

			treasureHand.SetActive(false);

			shopButtonJiggleEffect.enabled = false;
			shopHand.SetActive(false);

			goButtonJiggleEffect.enabled = false;
			goButtonHand.SetActive(true);

			jeepHand.SetActive(false);

			if(spotlightEffect != null)
				spotlightEffect.gameObject.SetActive(false);
		}
		else if (curTutorialStage == TutStageType.MainMenuJeep)
		{
			coinBarTotal.gameObject.SetActive(true);
			//scoreBarTotal.gameObject.SetActive(true);
			energyBar.gameObject.SetActive(true);

			treasureButtonTimerText.gameObject.SetActive(true);
			treasureButtonNotfication.gameObject.SetActive(false);
			treasureButtonEnabled.gameObject.SetActive(true);
			treasureButtonDisabled.gameObject.SetActive(true);

			shopButtonParent.gameObject.SetActive(true);

			//settingsButton.gameObject.SetActive(false);

			jeepButton.GetComponent<Collider>().enabled = true;
			jeepActivityMarker.gameObject.SetActive(true);
			jeepTimer.gameObject.SetActive(true);

			menuCrateChopper.gameObject.SetActive(false);
			shackMarkerNotification.gameObject.SetActive(false);
			shackButton.GetComponent<Collider>().enabled  = false;

			if (treasureVaultUITimer.TimeStatus)
			{
				treasureButtonJiggleEffect.enabled = true;
				treasureButtonDisabled.SetActive(false);
			}
			else
			{
				treasureButtonJiggleEffect.enabled = false;
				treasureButtonDisabled.SetActive(true);
			}
			treasureHand.SetActive(false);

			shopButtonJiggleEffect.enabled = false;
			shopHand.SetActive(false);

			goButtonJiggleEffect.enabled = false;
			goButtonHand.SetActive(false);

			jeepHand.SetActive(true);

			if(spotlightEffect != null)
				spotlightEffect.gameObject.SetActive(false);
		}
		else
		{
			if(spotlightEffect != null)
				spotlightEffect.gameObject.SetActive(false);

			menuCrateChopper.gameObject.SetActive(true);

			if (GameState.SharedInstance.IsHalfPipeTimerDone() || GameState.SharedInstance.IsGameConsoleTimerDone())
				shackMarkerNotification.gameObject.SetActive(true);
			else
				shackMarkerNotification.gameObject.SetActive(false);
			
			shackButton.GetComponent<Collider>().enabled  = true;

			if (treasureVaultUITimer.TimeStatus)
			{
				treasureButtonJiggleEffect.enabled = true;
				treasureButtonDisabled.SetActive(false);
			}
			else
			{
				treasureButtonJiggleEffect.enabled = false;
				treasureButtonDisabled.SetActive(true);
			}
			
			treasureHand.SetActive(false);

			jeepHand.SetActive(false);

			shopButtonJiggleEffect.enabled = false;
			shopHand.SetActive(false);

			goButtonJiggleEffect.enabled = false;
			goButtonHand.SetActive(false);
		}
	}


	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.MainMenuDialogsShowing() == false && DialogTrackingFlags.MainMenuIgnoreAndroidBackCheck == false)
			{
				Application.Quit();
			}

			if(DialogTrackingFlags.MainMenuIgnoreAndroidBackCheck)
			{
				DialogTrackingFlags.MainMenuIgnoreAndroidBackCheck = false;
			}
		}
		#endif 
	}

	int playServicesAutoLoginCheck = 0;

    bool checkedForNullMesh = false;

	void Update () 
	{
#if UNITY_ANDROID
		if(playServicesAutoLoginCheck == 1)
		{
			if(gcpm != null)
			{
				gcpm.CheckAndroidGameServicesLogin();
			}
		}

		if(gcpm != null)
		{
			playServicesAutoLoginCheck += 1;
		}

#endif

        if (!checkedForNullMesh)
        {
            checkedForNullMesh = true;
            var res = GameObject.FindObjectsOfType<MeshFilter>();
            //dreaded Do not use ReadObjectThreaded on scene objects!
            foreach (var nmo in res)
            {
                if (nmo.sharedMesh == null)
                {
                    Debug.LogError("BH ******** null meshfilter in " + nmo.transform.name, nmo.gameObject);
                }
            }
        }


		CheckAndroidBackButton();
        //Debug.Log("IM IN UPDATE Time:" + Time.time);
		if (ServerSavedGameStateSync.DataSyncedFromServer)
		{
			ServerSavedGameStateSync.DataSyncedFromServer = false;

			if(energyBar != null)
			{
				energyBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
			}

			if(coinBarTotal != null)
			{
				coinBarTotal.DisplayCoins(GameState.SharedInstance.Cash);
			}

			checkTutorialCompleteReload = true;

			AlertDialogUtil.ShowAlert("RESTORE COMPLETE", "Game Data Restored", "Done");
		}

		if(mainMenuSceneDisableClickCooldown >= 0)
			mainMenuSceneDisableClickCooldown -= Time.deltaTime;
		
		if ((int)GameState.SharedInstance.TutorialStage >= (int)TutStageType.MainMenuJeep)
		{ 
			if(GlobalSettings.BackToMainMenuFromRewardedVideo)
			{
				GlobalSettings.BackToMainMenuFromRewardedVideo = false;

				coinBarTotal.DisplayCoins(GameState.SharedInstance.Cash);
				energyBar.DisplayValue(GameState.SharedInstance.EnergyDrink);


				#if UNITY_EDITOR
				menuCrateChopper.FlyInCrateAfterDelay(5f);
				#else
				menuCrateChopper.FlyInCrateAfterDelay(UnityEngine.Random.Range(7f, 15f));
				#endif

				if(!superSonicManager.isRewardedVideoReady())
					watchAndWinDialog.Hide();
			}

			if (jeepTimer.TimeElapsed)
	        {
	            jeepActivityMarker.SetActive(true);
	            jeepBowserOn.SetActive(false);
	            jeepBowserOff.SetActive(true);
	        }
	        else if(!loadingJeepScene)
	        {
	            jeepActivityMarker.SetActive(false);
	            jeepBowserOn.SetActive(true);
	            jeepBowserOff.SetActive(false);
	        }
        
	        if(!loadingJeepScene)
	        {
	            jeepTimer.UpdateTimer();
	        }
        }

		if (!loadingJeepScene)
		{
			HandleButtonClick();
		}
		
	}

    /*void RewardedVideoClosedEventHandler()
    {
		//////Debug.Log("RewardedVideoClosedEventHandler");
		AudioListener.pause = false;
        SceneNavigator.NavigateTo(SceneType.ShellGame);
    }*/

	void RewardedVideoAdRewardedEvent (IronSourcePlacement obj)
	{
		//NavigateToLocation(GlobalSettings.MainMenuLocation.CratePrize);
	}

	void HandleButtonClick()
	{
		if(StarterPackDialogShowing())
			return;
		
        
		// On mouse button down
		// Protect buttons from being accidentally clicked on navigate
		if(Input.GetMouseButtonDown(0) && mainMenuSceneDisableClickCooldown <= 0)
		{
			#if UNITY_ANDROID
			if(UIHelpers.CheckButtonHit(androidAchievementsButton.transform, mainCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				if(gcpm != null)
				{
					gcpm.ShowAchievements();
				}
			}
			else if(UIHelpers.CheckButtonHit(androidLeaderboardButton.transform, mainCamera))
			{
				GlobalSettings.PlaySound(clickSound);

				if(gcpm != null)
				{
					gcpm.ShowLeaderboard(LeaderboardType.SurfLeaderBoard);
				}
			}
			#endif

			if(UIHelpers.CheckButtonHit(settingsButton.transform, mainCamera))
			{
				GlobalSettings.PlaySound(clickSound);
				settingsDialog.Show();
			}

			if (UIHelpers.CheckButtonHit( specialOfferButton.transform, mainCamera))
			{
				GlobalSettings.PlaySound(clickSound);
				starterPackDialog.ShowStarterPackDialog();
			}

	        if (watchAndWinDialog.State == DialogPanel.DialogPanelState.Visible)
	        {
	            if (UIHelpers.CheckButtonHit(watchVideoPanelButton.transform, mainCamera))
	            {
					GlobalSettings.PlaySound(clickSound);
	                #if UNITY_EDITOR
	                //NavigateToLocation(GlobalSettings.MainMenuLocation.CratePrize);
	                SceneNavigator.NavigateTo(SceneType.ShellGame);
	                #else

					superSonicManager.OnRewardedVideoFinish = null;
					superSonicManager.OnRewardedVideoFinish = OnRewardedVideoEnd;

					if (superSonicManager.isRewardedVideoReady())
					{
						AudioListener.pause = true;
						superSonicManager.ShowRewardedVideo();
					}
	                #endif
	            }
	        }
	        else if (UIHelpers.CheckButtonHit(theBird.transform, mainCamera))
	        {
	            theBird.ShootBird();   
	        }
			else if(UIHelpers.CheckButtonHit(theCrate.transform, mainCamera) && 
				(menuCrateChopper.currentChopperState == MenuChopper.ChopperState.DroppingCrate || menuCrateChopper.currentChopperState == MenuChopper.ChopperState.Finished))
			{
				if(settingsDialog == null || settingsDialog.State == SettingsDialog.DialogPanelState.Hidden)
				{
					#if UNITY_EDITOR
					GlobalSettings.PlaySound(soundCrate);
					crateExplodeParticles.gameObject.transform.position = new Vector3(theCrate.transform.position.x, theCrate.transform.position.y, theCrate.transform.position.z);
					crateExplodeParticles.gameObject.SetActive(true);
					crateExplodeParticles.time = 0.0f;
					crateExplodeParticles.Play();

					menuCrateChopper.ResetCrateAndChopperPosition();
					watchAndWinDialog.ShowDelay(0.5f);
					#else
					GlobalSettings.PlaySound(clickSound);

					if (superSonicManager.isRewardedVideoReady())
					{
						watchAndWinDialog.Show();
					}
					#endif 
				}
			}
			else if (UIHelpers.CheckButtonHit(goButtonEnabled.transform, mainCamera))
			{
				EnableGoButton(false);
				GlobalSettings.PlaySound(clickSound);

				if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuGoButton)
					GameState.SharedInstance.TutorialStage = TutStageType.FirstGame;
				if (GameState.SharedInstance.TutorialStage == TutStageType.PostShopVisit)
					GameState.SharedInstance.TutorialStage = TutStageType.MainMenuJeep;
					SceneNavigator.NavigateTo(SceneType.SurfMainLoading);

			}
			else if (UIHelpers.CheckButtonHit(treasureButtonEnabled.transform, mainCamera))
			{
				if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuChest)
				{
					treasureHand.SetActive(false);
				}

				treasureButtonJiggleEffect.enabled = false;

				GlobalSettings.PlaySound(clickSound);
				NavigateToLocation(GlobalSettings.MainMenuLocation.Treasure);
			}
			else if(UIHelpers.CheckButtonHit(shopButtonEnabled.transform, mainCamera))
			{
	            shopButtonEnabled.SetActive(false);
	            shopButtonDisabled.SetActive(true);
				GlobalSettings.PlaySound(clickSound);
	            if (GlobalSettings.currentMenuMusicIndex == 0)
	            {
	                GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
	            }
	            else if (GlobalSettings.currentMenuMusicIndex == 1)
	            {
	                GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
	            }
	            else if (GlobalSettings.currentMenuMusicIndex == 2)
	            {
	                GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
	            }

	            if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuShop)
					GameState.SharedInstance.TutorialStage = TutStageType.FirstShopVisit;
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Shop);
			}
			else if (UIHelpers.CheckButtonHit(jeepButton.transform, mainCamera))
			{	
				GlobalSettings.PlaySound(clickSound);

				if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuJeep)
				{
					GameState.SharedInstance.TutorialStage = TutStageType.Finished;
				}

				JeepButtonClicked();
			}
			else if(UIHelpers.CheckButtonHit(shackButton.transform, mainCamera))
			{
				GlobalSettings.PlaySound(clickSound);

	            if (GlobalSettings.currentMenuMusicIndex == 0)
	            {
	                GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
	            }
	            else if (GlobalSettings.currentMenuMusicIndex == 1)
	            {
	                GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
	            }
	            else if (GlobalSettings.currentMenuMusicIndex == 2)
	            {
	                GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
	            }
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
                SceneNavigator.NavigateTo(SceneType.Shack);
			}
		}
	}

	void OnRewardedVideoEnd()
	{
		////////Debug.Log("OnRewardVideoEnd");
		superSonicManager.OnRewardedVideoFinish -= OnRewardedVideoEnd;
		AudioListener.pause = false;
        #if UNITY_ANDROID
        AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

        if(droidLoader != null)
        {
            droidLoader.Show();
        }
        #endif
		SceneNavigator.NavigateTo(SceneType.ShellGame);
	}

	void JeepEnergyDrinkDialogEnergyDrinkSpendSuccessful (int energyDrinksUsed)
	{
		coinBarTotal.coinText.text = string.Format("{0:#,0}", GameState.SharedInstance.Cash);
		energyBar.DisplayValue(GameState.SharedInstance.EnergyDrink);

		AnalyticsAndCrossPromoManager.SharedInstance.LogJeepTimerSkipped(energyDrinksUsed);

		loadingJeepScene = true;
		jeepTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSeconds(TimerManager.TimerID.Jeep));
        #if UNITY_ANDROID
        AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

        if(droidLoader != null)
        {
            droidLoader.Show();
        }
        #endif
		SceneNavigator.NavigateTo(SceneType.JeepSand);
	}

    void JeepButtonClicked()
    {
        if (jeepTimer.TimeElapsed)
        {
            loadingJeepScene = true;
			jeepTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSeconds(TimerManager.TimerID.Jeep));
            #if UNITY_ANDROID
            AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

            if(droidLoader != null)
            {
                droidLoader.Show();
            }
            #endif
			SceneNavigator.NavigateTo(SceneType.JeepSand);
        }
        else
        {
			jeepEnergyDrinkDialog.SetEnergyDrinkCostText(GameServerSettings.SharedInstance.SurferSettings.JeepRefuelEnergyDrinkCost);
			jeepEnergyDrinkDialog.Show();
        }          
    }


	public void NavigateToLocation(GlobalSettings.MainMenuLocation location)
	{
		GlobalSettings.CurrentMainMenuLocation = location;			
		mainCamera.enabled = false;
		treasureVaultUICamera.enabled = false;
		cratePrizeCamera.enabled = false;
		DialogTrackingFlags.MainMenuTreasureChestShowing = false;


		if(GlobalSettings.CurrentMainMenuLocation == GlobalSettings.MainMenuLocation.MainMenu)
		{
			mainCamera.enabled = true;
		}
		else if(GlobalSettings.CurrentMainMenuLocation == GlobalSettings.MainMenuLocation.Treasure)
        {
			DialogTrackingFlags.MainMenuTreasureChestShowing = true;

			treasureVaultUIController.Initialise();
			treasureVaultUICamera.enabled = true;
		}
		else if(GlobalSettings.CurrentMainMenuLocation == GlobalSettings.MainMenuLocation.CratePrize)
		{
			cratePrizeCamera.enabled = true;
		}
	}

	public void InitialiseMainMenuButtons()
	{
		EnableGoButton(true);
		EnableShopButton(true);
		EnableTreasureButton(true);

		if (Utils.IphoneXCheck())
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 330, 110, shopButtonParent.transform, mainCamera);

			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_LEFT, 100, 60, treasureButtonParent.transform, mainCamera);

			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonEnabled.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonDisabled.transform, mainCamera);

			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_BOTTOM_RIGHT, 160, 60, goButtonParent.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonEnabled.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonDisabled.transform, mainCamera);
		}
		else
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 330, 0, shopButtonParent.transform, mainCamera);

			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonParent.transform, mainCamera);

			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonEnabled.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_LEFT, 100, 0, treasureButtonDisabled.transform, mainCamera);

			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonParent.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonEnabled.transform, mainCamera);
			//UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.SIDE_RIGHT, 160, 0, goButtonDisabled.transform, mainCamera);
		}
		RefreshShopNotificationIcon();
	}

	public void RefreshShopNotificationIcon()
	{
		if (GameState.SharedInstance.TutorialStage == TutStageType.Finished)
			shopButtonNotification.SetActive(GameState.SharedInstance.Upgrades.CanAffordItem());
		else
			shopButtonNotification.SetActive(false);

	}

	public void EnableGoButton(bool enable)
	{
		if(enable)
		{
			goButtonEnabled.SetActive(true);
			goButtonDisabled.SetActive(false);
		}
		else
		{
			goButtonEnabled.SetActive(false);

			if(goButtonDisabledText != null)
				goButtonDisabledText.GetComponent<Localizer>().LoadLocalizedText();
			
			goButtonDisabled.SetActive(true);
		}
	}

	public void EnableTreasureButton(bool enable)
	{
		if(enable)
		{
			treasureButtonEnabled.SetActive(true);
			treasureButtonDisabled.SetActive(false);
		}
		else
		{
			treasureButtonEnabled.SetActive(false);
			treasureButtonDisabled.SetActive(true);
		}
	}

	public void EnableShopButton(bool enable)
	{
		if(enable)
		{
			shopButtonEnabled.SetActive(true);
			shopButtonDisabled.SetActive(false);
		}
		else
		{
			shopButtonEnabled.SetActive(false);
			shopButtonDisabled.SetActive(true);
		}
	}

}
