﻿using UnityEngine;
using System.Collections;

public class MenuChopper : MonoBehaviour {

    public enum ChopperState
    {
        Inactive,
        FlyingDown,
        DroppingCrate,
        Finished
    };

    Vector3 chopperStartPos;
    float chopperTimer = 0;
    float chopperDelay = 3;
    float chopperMinYPos = 10f;
    
	public GameObject theCrate;
	public RotationClickableJiggle theCrateRotJiggleEffect;
	public PositionClickableJiggle theCratePosJiggleEffect;
	private Vector3 crateStartPos;
	private bool crateDropped = false;

	private float rotationJitterBounce = 3.0f;

	private int crateBounceTimes = 2;
	private int curCrateBounces = 0;
	private float bounceFriction = 0.1f;
    private float chopperMaxYPos = 0;
    private float minCrateYPos = -205f;
    private float chopperDropXPos = 40f;
    private float minChopperYPos = 166;
    private float crateHangingYOffset = -180f;
   	private float crateHangingZOffset = -1;
    private float chopperXVelocity = -150f;
    private float chopperYVelocity = -80f;
    private float crateDropVelocity = -400f;
    private float gravityScale = 300.0f;

    public AudioSource chopperSound;
    public AudioSource crateLandSound;

    public Camera theCamera;
    float chopperLocalYPos = 0;

    private SupersonicManager superSonicManager;

    public ChopperState currentChopperState = ChopperState.Inactive;

	public void ResetCrateAndChopperPosition()
	{
		transform.position = chopperStartPos;
		theCrate.transform.position = crateStartPos;
		theCrateRotJiggleEffect.enabled = false;
		theCratePosJiggleEffect.enabled = false;
	}

	public void FlyInCrateAfterDelay(float delaySecs)
	{
		chopperDelay = delaySecs;
		currentChopperState = ChopperState.Inactive;
	}

	// Use this for initialization
	void Start () 
    {
        if (Application.isEditor)
        {
            chopperDelay = 2f;
        }
        superSonicManager = FindObjectOfType<SupersonicManager>();
        chopperStartPos = transform.position;
		crateStartPos = theCrate.transform.position;
		theCrateRotJiggleEffect.enabled = false;
		theCratePosJiggleEffect.enabled = false;

		if (!GlobalSettings.soundEnabled)
			chopperSound.Stop();
	}
	
    float chopperSinCounter = 0;
    bool bobbingEnabled = true;

	void Update () 
    {
		if (!GlobalSettings.soundEnabled)
			chopperSound.Stop();
		else
		{
			if (!chopperSound.isPlaying)
				chopperSound.Play();
		}

		if(currentChopperState == ChopperState.Inactive)
        {
			theCrate.transform.position = new Vector3(transform.position.x , transform.position.y + crateHangingYOffset ,transform.position.z + crateHangingZOffset);
            chopperTimer += Time.deltaTime;
            if(chopperTimer > chopperDelay)
            {
				if (superSonicManager.isRewardedVideoReady() || Application.isEditor)
                {
                    currentChopperState = ChopperState.FlyingDown;
                }
            }
        }
        else if(currentChopperState == ChopperState.FlyingDown)
        {
			theCrate.transform.position = new Vector3(transform.position.x, transform.position.y + crateHangingYOffset,transform.position.z + crateHangingZOffset);
            transform.position = new Vector3(transform.position.x + chopperXVelocity * Time.deltaTime, transform.position.y + chopperYVelocity * Time.deltaTime, transform.position.z);
            if(transform.position.y < minChopperYPos)
            {
                //transform.position = new Vector3(transform.position.x, minChopperYPos, transform.position.z);
            }
            if(transform.position.x < chopperDropXPos)
            {
                currentChopperState = ChopperState.DroppingCrate;
            }
        }
        else if(currentChopperState == ChopperState.DroppingCrate)
        {

			if (!crateDropped)
			{
            	crateDropVelocity -= gravityScale * Time.deltaTime;
            	theCrate.transform.position = new Vector3(theCrate.transform.position.x, theCrate.transform.position.y + crateDropVelocity * Time.deltaTime, theCrate.transform.position.z);
            }

            transform.position = new Vector3(transform.position.x + chopperXVelocity * Time.deltaTime, transform.position.y - chopperYVelocity * Time.deltaTime, transform.position.z);

			if(theCrate.transform.position.y < minCrateYPos && curCrateBounces < crateBounceTimes)
            {
				curCrateBounces ++;
				theCrate.transform.position = new Vector3(theCrate.transform.position.x, minCrateYPos, theCrate.transform.position.z);
				crateDropVelocity = -crateDropVelocity * bounceFriction;
				theCrate.transform.rotation = Quaternion.AngleAxis( Random.Range(-rotationJitterBounce, rotationJitterBounce), Vector3.forward);
				GlobalSettings.PlaySound(crateLandSound);
            }
			else if (theCrate.transform.position.y < minCrateYPos && !crateDropped)
            {
				crateDropped = true;
				theCrate.transform.position = new Vector3(theCrate.transform.position.x, minCrateYPos, theCrate.transform.position.z);
				theCrate.transform.rotation = Quaternion.identity;

				theCrateRotJiggleEffect.enabled = true;
				theCratePosJiggleEffect.enabled = true;
				theCratePosJiggleEffect.ResetPosition();
            }

            if(transform.position.y > chopperStartPos.y)
            {
                transform.position = chopperStartPos;
                currentChopperState = ChopperState.Finished;
            }
        }
       
        chopperLocalYPos = transform.position.y;
        if(bobbingEnabled)
        {
            // WAVELENGTH
            chopperSinCounter += Time.deltaTime * 3f;
            float chopperYSin = Mathf.Sin(chopperSinCounter);

            // AMPLITUDE
            chopperYSin *= 1.3f;
            transform.position = new Vector3(transform.position.x, chopperLocalYPos + chopperYSin,transform.localPosition.z);
        }

	}

}
