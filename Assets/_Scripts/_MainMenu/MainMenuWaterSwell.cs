﻿using UnityEngine;
using System.Collections;

public class MainMenuWaterSwell : MonoBehaviour {

    float localScaleMinY = 100f;
    float localScaleMaxY = 160f;
    float localMinScaleX = 1138f;
    float currentScale = 0;
    float swellSpeed = 1f;
    bool swellUp = false;
	// Use this for initialization
    float sinFraction = 0;
    Material waterMaterial;
    Renderer waterRenderer;

	void Start () {
        
        currentScale = localScaleMinY;
        waterRenderer = GetComponent<Renderer>();
        waterMaterial = waterRenderer.sharedMaterial;
        waterMaterial.color = new Color(1,1,1,1);
	}
	
	// Update is called once per frame
	void Update () {

        sinFraction = GetSineMultiple();
        currentScale = (localScaleMaxY - localScaleMinY) * sinFraction;
        currentScale += localScaleMinY;

        this.transform.localScale = new Vector3(localMinScaleX + currentScale, currentScale, this.transform.localScale.z);
        //waterMaterial.color = new Color(1,1,1,sinFraction);
	}

    float sinCounter = 0;
    private float GetSineMultiple()
    {
        // WAVELENGTH
        sinCounter += Time.deltaTime * swellSpeed;
        float chopperYSin = Mathf.Sin(sinCounter) + 1f;
        float zeroToOneRange = chopperYSin / 3f;
        return zeroToOneRange;
    }
}
