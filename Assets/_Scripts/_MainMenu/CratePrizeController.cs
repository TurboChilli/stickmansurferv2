﻿using UnityEngine;
using System.Collections;
using TMPro;

public class CratePrizeController : MonoBehaviour {

	public Camera mainMenuCamera;
	public Camera cratePrizeCamera;
	public GameObject pickOneText;
	public GameObject closeButton;
	public GameObject crate1;
	public GameObject crate2;
	public GameObject crate3;

    // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
    public ParticleSystem crateExplodeParticles;

	public AudioSource soundCrate;
	public AudioSource soundKaching;

	public GameObject prizeCoinStack;
	public TMP_Text prizeCoinStackAmount;

	public GameObject prizeEnergyDrink;
	public TMP_Text prizeEnergyAmount;

	public GameObject prizeLlama;
	public TMP_Text prizeLlamaAmount;

	float autoCloseSecs = -1f;
	float revealNonWinningSecs = -1f;
	int? prizeAmount;

	enum PrizeWon
	{
		Undefined,
		EnergyDrink,
		CoinStash,
		Llama,
	}

	PrizeWon prizeWon = PrizeWon.Undefined;

	bool crateOpened = false;

	int[] nonWinningCrates;

	void Start () {
		ResetCrates();
	}
	

	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			if(UIHelpers.CheckButtonHit(closeButton.transform, cratePrizeCamera))
			{
				NavigateToMainMenu();
			}
			else if(UIHelpers.CheckButtonHit(crate1.transform, cratePrizeCamera))
			{
				OpenCrate(1);
			}
			else if(UIHelpers.CheckButtonHit(crate2.transform, cratePrizeCamera))
			{
				OpenCrate(2);
			}
			else if(UIHelpers.CheckButtonHit(crate3.transform, cratePrizeCamera))
			{
				OpenCrate(3);
			}
		}

		if(crateOpened)
		{
			if(autoCloseSecs >= 0)
			{
				autoCloseSecs -= Time.deltaTime;
				if(autoCloseSecs <= 0)
				{
					NavigateToMainMenu();
				}
			}

			if(revealNonWinningSecs >= 0)
			{
				revealNonWinningSecs -= Time.deltaTime;
				if(revealNonWinningSecs <= 0)
				{
					RevealNonWinningCrates();
				}
			}


		}
	}

	void OpenCrate(int crateNumber)
	{
		if(crateOpened)
			return;

		autoCloseSecs = 5f;
		revealNonWinningSecs = 2f;
		pickOneText.SetActive(false);
		closeButton.SetActive(true);
		
		crateOpened = true;

		GlobalSettings.PlaySound(soundCrate);
		GlobalSettings.PlaySoundDelayed(soundKaching, 0.5f);

		int percent = UnityEngine.Random.Range(0, 100);

		// Prize Energy Drink Amount
		int energyDrinkAmountValue = UnityEngine.Random.Range(1, 4);
		prizeEnergyAmount.text = energyDrinkAmountValue.ToString();

		// Prize Llama Amount
		int prizeLlamaAmountValue = (UnityEngine.Random.Range(1, 10) * 10);
		prizeLlamaAmount.text = prizeLlamaAmountValue.ToString();

		int coinStashPrizePercent = UnityEngine.Random.Range(0, 100);
		int coinStashPrizeValue;

		if(coinStashPrizePercent < 25)
		{
			coinStashPrizeValue = (UnityEngine.Random.Range(10, 20) * 50);
			prizeCoinStackAmount.text = coinStashPrizeValue.ToString();
		}
		else
		{
			coinStashPrizeValue = (UnityEngine.Random.Range(1, 10) * 50);
			prizeCoinStackAmount.text = coinStashPrizeValue.ToString();
		}
		

		if(percent <= 10)
		{
			prizeWon = PrizeWon.EnergyDrink;
			prizeAmount = energyDrinkAmountValue;
			PlacePrizeAtCrate(prizeEnergyDrink, crateNumber);
		}
		else if(percent <= 40)
		{
			prizeWon = PrizeWon.Llama;
			prizeAmount = prizeLlamaAmountValue;
			PlacePrizeAtCrate(prizeLlama, crateNumber);
		}
		else if(percent <= 101)
		{
			prizeWon = PrizeWon.CoinStash;
			prizeAmount = coinStashPrizeValue;
			PlacePrizeAtCrate(prizeCoinStack, crateNumber);
		}
	}

	void PlacePrizeAtCrate(GameObject prize, int crateNumber)
	{
		Vector3[] cratesAvailablePos = null;
		if(crateNumber == 1)
		{
			cratesAvailablePos = new Vector3[] { crate2.transform.position, crate3.transform.position };
			nonWinningCrates = new int[] { 2, 3 };

			prize.transform.position = new Vector3(crate1.transform.position.x, crate1.transform.position.y, prize.transform.position.z);
			crateExplodeParticles.gameObject.transform.position = new Vector3(crate1.transform.position.x, crate1.transform.position.y, crateExplodeParticles.transform.position.z);
            // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
            //crateExplodeParticles.Emit();

            crate1.SetActive(false);
		}
		else if(crateNumber == 2)
		{
			cratesAvailablePos = new Vector3[] { crate1.transform.position, crate3.transform.position };
			nonWinningCrates = new int[] { 1, 3 };

			prize.transform.position = new Vector3(crate2.transform.position.x, crate2.transform.position.y, prize.transform.position.z);
			crateExplodeParticles.gameObject.transform.position = new Vector3(crate2.transform.position.x, crate2.transform.position.y, crateExplodeParticles.transform.position.z);
            // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
            //crateExplodeParticles.Emit();

			crate2.SetActive(false);
		}
		else if(crateNumber == 3)
		{
			cratesAvailablePos = new Vector3[] { crate1.transform.position, crate2.transform.position };
			nonWinningCrates = new int[] { 1, 2 };

			prize.transform.position = new Vector3(crate3.transform.position.x, crate3.transform.position.y, prize.transform.position.z);
			crateExplodeParticles.gameObject.transform.position = new Vector3(crate3.transform.position.x, crate3.transform.position.y, crateExplodeParticles.transform.position.z);
            // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
            //crateExplodeParticles.Emit();

			crate3.SetActive(false);
		}
			
		Vector3 firstCratePos = Vector3.zero;
		Vector3 secondCratePos = Vector3.zero;

		if((Random.Range(1,3)%2) == 0)
		{
			firstCratePos = new Vector3(cratesAvailablePos[0].x, cratesAvailablePos[0].y, cratesAvailablePos[0].z);
			secondCratePos = new Vector3(cratesAvailablePos[1].x, cratesAvailablePos[1].y, cratesAvailablePos[1].z);
		}
		else
		{
			firstCratePos = new Vector3(cratesAvailablePos[1].x, cratesAvailablePos[1].y, cratesAvailablePos[1].z);
			secondCratePos = new Vector3(cratesAvailablePos[0].x, cratesAvailablePos[0].y, cratesAvailablePos[0].z);
		}

		// Reward Value
		if(prizeAmount.HasValue)
		{
			if(prizeWon == PrizeWon.EnergyDrink)
				GameState.SharedInstance.AddEnergyDrink(prizeAmount.Value);
			
			if(prizeWon == PrizeWon.CoinStash)
				GameState.SharedInstance.AddCash(prizeAmount.Value);

			prizeAmount = null;
		}
			
			
		bool firstCratePosUsed = false;
		if(prizeWon != PrizeWon.EnergyDrink)
		{
			firstCratePosUsed = true;
			prizeEnergyDrink.transform.position = new Vector3(firstCratePos.x, firstCratePos.y, prizeEnergyDrink.transform.position.z);
		}
			
		if(prizeWon != PrizeWon.CoinStash)
		{
			if(firstCratePosUsed)
				prizeCoinStack.transform.position = new Vector3(secondCratePos.x, secondCratePos.y, prizeCoinStack.transform.position.z);
			else
			{
				firstCratePosUsed = true;
				prizeCoinStack.transform.position = new Vector3(firstCratePos.x, firstCratePos.y, prizeCoinStack.transform.position.z);
			}
		}

		if(prizeWon != PrizeWon.Llama)
		{
			if(firstCratePosUsed)
				prizeLlama.transform.position = new Vector3(secondCratePos.x, secondCratePos.y, prizeLlama.transform.position.z);
			else
			{
				firstCratePosUsed = true;
				prizeLlama.transform.position = new Vector3(firstCratePos.x, firstCratePos.y, prizeLlama.transform.position.z);
			}
		}
	}

	void RevealNonWinningCrates()
	{
		if(nonWinningCrates != null)
		{
			foreach(int i in nonWinningCrates)
			{
				if(i == 1)
					crate1.SetActive(false);
				
				if(i == 2)
					crate2.SetActive(false);

				if(i == 3)
					crate3.SetActive(false);
			}
		}
	}

	void ResetCrates()
	{
		crateOpened = false;
		autoCloseSecs = -1f;
		revealNonWinningSecs = -1f;
		prizeAmount = null;
		prizeWon = PrizeWon.Undefined;
		pickOneText.gameObject.SetActive(true);
		crate1.SetActive(true);
		crate2.SetActive(true);
		crate3.SetActive(true);

		prizeCoinStack.transform.position = new Vector3(-99999f, prizeCoinStack.transform.position.y, prizeCoinStack.transform.position.z);
		prizeEnergyDrink.transform.position = new Vector3(-99999f, prizeEnergyDrink.transform.position.y, prizeEnergyDrink.transform.position.z);
		prizeLlama.transform.position = new Vector3(-99999f, prizeLlama.transform.position.y, prizeLlama.transform.position.z);
	}

	void NavigateToMainMenu()
	{
		GlobalSettings.CurrentMainMenuLocation = GlobalSettings.MainMenuLocation.MainMenu;	
		GlobalSettings.BackToMainMenuFromRewardedVideo = true;

		mainMenuCamera.enabled = false;
		cratePrizeCamera.enabled = false;

		mainMenuCamera.enabled = true;

		ResetCrates();
	}
}
