﻿using UnityEngine;
using System.Collections;

public class TreeSway : MonoBehaviour {


    public Transform rotationBase;
    private Vector3 rotationAxis = new Vector3(0,0,1);
    private float chopperSinCounter = 0;

    private Quaternion origRotation;

    void Start()
    {
        chopperSinCounter += Random.Range(0f,100f);
    }

    void Update () 
    {
        
        chopperSinCounter += Time.deltaTime * 1f;
        float chopperYSin = Mathf.Sin(chopperSinCounter);
        chopperYSin /= 40f;

       ////////Debug.Log("chopperYSin:" + chopperYSin);
        //chopperYSin *= 1.3f;
        transform.RotateAround(new Vector3(rotationBase.transform.position.x, rotationBase.transform.position.y, transform.position.z), rotationAxis, chopperYSin);
    }
}
