﻿using UnityEngine;
using System.Collections;
using System;
using TMPro;

public class JeepTimer : MonoBehaviour {

    private DateTime openDateTime;
    private TimeSpan timeDifference;

	public TMP_Text timerRefuelText; 

    private bool timeIsUp = false;
    public AudioSource timesUpSound;
    private TMP_Text timerText; 

    public bool hasPrefix = false;

    public bool TimeElapsed
    {
        get
        {
            return timeIsUp;
        }
        set 
        {
            timeIsUp = value;
        }
    }

	bool timerTextVisible = false;
	void HideTimerText()
	{
		if(timerTextVisible || (timerText != null && !string.IsNullOrEmpty(timerText.text)))
		{
			timerTextVisible = false;
			timerText.text = "";

			if(hasPrefix && timerRefuelText != null)
			{
				timerRefuelText.text = "";
			}
		}
	}

	void ShowTimerText()
	{
		if(!timerTextVisible || (timerText != null && string.IsNullOrEmpty(timerText.text)))
		{
			timerTextVisible = true;

			if(hasPrefix && timerRefuelText != null)
			{
				timerRefuelText.text = string.Format("<size=-32>{0}:</color></size>", GameLanguage.LocalizedText("RE-FUEL"));
			}
		}
	}

    public void StartTimer () {
        timerText = GetComponent<TMP_Text>();

        if (GameState.SharedInstance.TutorialStage == TutStageType.Finished)
        {
	        timerText.text = "";
	        if (GameState.SharedInstance.JeepTimeStamp == "0")
	        {
				InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Jeep));

				UpdateTimer();
	        }
	        else
	        {
	            GetOldTime();
				UpdateTimer();
	        }
        }
        else
        {
			InitializeTimer(0); 
        }
    }
		
    public void UpdateTimer () {

        timeDifference = openDateTime.Subtract(DateTime.UtcNow);

        if (timeDifference.TotalSeconds > 0)
        {
			ShowTimerText();
            string result = TimeStringFormatter(timeDifference);

			if(!string.Equals(timerText.text, result, StringComparison.CurrentCultureIgnoreCase))
				timerText.text = result;
        }
        else
        {
            timeIsUp = true;
			HideTimerText();
        }
    }

    public void InitializeTimer(int minutes)
    {
        openDateTime = DateTime.UtcNow.AddSeconds(minutes);
		GameState.SharedInstance.JeepTimeStamp = openDateTime.ToBinary().ToString();
        timeIsUp = false;
    }

    public DateTime GetOldTime()
    {
		long temp = Convert.ToInt64(GameState.SharedInstance.JeepTimeStamp);
        openDateTime = DateTime.FromBinary(temp);

        return openDateTime;
    }

    public string TimeStringFormatter(TimeSpan timeDifference)
    {
        string result = string.Format("{0}:{1}:{2}",
            timeDifference.Hours,
            Pad2(timeDifference.Minutes),
            Pad2(timeDifference.Seconds));

        return result;
    }

    public string Pad2(int thePadValue)
    {
        string padVal = "" + thePadValue;
        if (padVal.Length == 1)
        {
            padVal = "0" + padVal;
        }
        return padVal;
    }
}
