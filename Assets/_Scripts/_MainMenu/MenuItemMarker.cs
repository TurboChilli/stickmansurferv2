﻿using UnityEngine;
using System.Collections;
using TMPro;

public class MenuItemMarker : MonoBehaviour {

    public TextMeshPro timeTextMesh;

    private float markerSinCounter = 0;
    private float markerYSin = 0;
    private float markerXVelocity = 10f;
    private float markerYStartPos = 0;

    private bool bobbingEnabled = true;

    public void SetMarkerTime(string timeLeft)
    {
        timeTextMesh.text = timeLeft;
    }

    void Start () 
    {
        markerYStartPos = transform.localPosition.y;
       
    }

    void Update () 
    {
        if(bobbingEnabled)
        {
            // WAVELENGTH
            markerSinCounter += Time.deltaTime * 3f;
            markerYSin = Mathf.Sin(markerSinCounter);

            // AMPLITUDE
            markerYSin *= 5f;
            transform.localPosition = new Vector3(transform.localPosition.x, markerYStartPos + markerYSin,transform.localPosition.z);
        }
    }
}
