﻿using UnityEngine;
using System.Collections;

public class BackgroundBird : MonoBehaviour {

    float birdYPos = 0;
    float birdSinCounter = 0;
    float startXPos = 0;
    float startYPos = 0;

    float delayBeforeBird = 1f;

    public ParticleSystem featherEmitter;
    public AudioSource shootSound;
    public AudioSource birdSound;

	// Use this for initialization
	void Start () 
    {
        startXPos = transform.position.x;
        startYPos = transform.position.y;
        SetRandomBirdInterval();

        if (!GlobalSettings.soundEnabled)
			birdSound.Stop();
	}

    void SetRandomBirdInterval()
    {
        delayBeforeBird = Random.Range(10, 30);
    }

    public void ShootBird()
    {
    	if (featherEmitter != null)
    	{
	        featherEmitter.transform.position = transform.position;
	        featherEmitter.Emit(20);
        }

        if (shootSound != null)
	        GlobalSettings.PlaySound(shootSound);

        transform.position = new Vector3(startXPos, transform.position.y, transform.position.z);
        SetRandomBirdInterval();
    }

	// Update is called once per frame
	void Update () 
    {
    	if (birdSound != null)
    	{
			if (!GlobalSettings.soundEnabled)
				birdSound.Stop();
			else
			{
				if (!birdSound.isPlaying)
					birdSound.Play();
			}
		}

        delayBeforeBird -= Time.deltaTime;
        if (delayBeforeBird < 0)
        {
            birdYPos = transform.position.y;

            // WAVELENGTH
            birdSinCounter += Time.deltaTime * 3f;
            float birdYSin = Mathf.Sin(birdSinCounter);

            // AMPLITUDE
            birdYSin *= 12.3f;
            transform.position = new Vector3(transform.position.x - 150f * Time.deltaTime, startYPos + birdYSin, transform.localPosition.z);
            if (transform.position.x < startXPos - 2200f)
            {
                transform.position = new Vector3(startXPos, transform.position.y, transform.position.z);
                SetRandomBirdInterval();
            }
        }


	}
}
