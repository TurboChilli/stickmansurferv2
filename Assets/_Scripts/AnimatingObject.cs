using UnityEngine;
using System.Collections;
using System.Collections.Generic;


#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(AnimatingObject))]
[CanEditMultipleObjects]
public class AnimatingObjectInspector : Editor
{
	private AnimatingObject owner = null;
	private int animationFrame = 0;

	public override void OnInspectorGUI()
	{
		owner = target as AnimatingObject;

		if (!Application.isPlaying)
		{
			EditorGUI.BeginChangeCheck ();

			GUILayout.Label ("Debug values");
			animationFrame = EditorGUILayout.IntField("Current debug frame: ", animationFrame);

			if (EditorGUI.EndChangeCheck ())
			{
				if (owner.curFrame != animationFrame)
				{
					int checkFrameBounds = (int)((owner.tileSheetPixelWidth/owner.tileWidth) * (owner.tileSheetPixelWidth/owner.tileHeight));
					if (0 <= animationFrame && animationFrame < checkFrameBounds)
					{
						//owner.isInitialised = false;
						owner.DrawFrame(animationFrame);
					}
				}
				EditorUtility.SetDirty(owner);
				Undo.RecordObject(owner, "Changed anim debug frame");
			}
		}

		GUILayout.Label ("In game values");
		base.OnInspectorGUI();
	}

	void Update()
	{
		int checkFrameBounds = (int)((owner.tileSheetPixelWidth/owner.tileWidth) * (owner.tileSheetPixelWidth/owner.tileHeight));
		if (0 <= animationFrame && animationFrame < checkFrameBounds)
		{
			if (owner.curFrame != animationFrame)
			{
				owner.DrawFrame(animationFrame);
			}
		}
	}
}
#endif

[ExecuteInEditMode]
public class AnimatingObject : MonoBehaviour {

	public int[] animationFrames;
	int numberOfFrames;
	public float frameDelay = 0.05f;
	
	public float tileWidth = 83f;
	public float tileHeight = 110f;
	public float tileSheetPixelWidth = 256f;
	
	public float tilesAcross = 2f;

	public bool autoPlay = false;

	int currentFrameIndex = 0;

	float animationTimer = 0f;

	private MeshFilter meshFilter;
	//private Mesh mesh = null;
	private Mesh[] frameMeshes;


    public bool randomStartFrame = false;

    public bool isInitialised = false;
	private bool playOnce;

    public int curFrame = -1;

	static Vector3[] vertexs = {
		new Vector3(-0.5f, -0.5f, 0.0f),
		new Vector3(0.5f, 0.5f, 0.0f),
		new Vector3(0.5f, -0.5f, 0.0f),
		new Vector3(-0.5f, 0.5f, 0.0f)
	};

	static int[] indexs = {
		0, 1, 2,
		1, 0, 3
	};

	private Vector2[] uvs = {
		Vector2.one,
		Vector2.one,
		Vector2.one,
		Vector2.one
	};

    void Start()
    {
    	Initialise();
    }

	void Initialise()
	{
		if (!isInitialised)
		{
			if (meshFilter == null)
				meshFilter = GetComponent<MeshFilter>();

			int numFrames = (int)((tileSheetPixelWidth / tileWidth) * (tileSheetPixelWidth / tileHeight));

			frameMeshes = new Mesh[numFrames];

			for (int i = 0; i < numFrames; i++)
			{		
				frameMeshes[i] = new Mesh();		
				frameMeshes[i].vertices = vertexs;
				frameMeshes[i].SetTriangles( indexs, 0);
				frameMeshes[i].RecalculateNormals();

				float startX = 0; // 0
				float startY = 0; // 1
				float endX = 0; // 2
				float endY = 0; // 3
				
				int tileIndexX = i % (int)tilesAcross;
				int tileIndexY = i / (int)tilesAcross;
				
				startX = tileIndexX * tileWidth;
				startY = tileIndexY * tileHeight;
				endX = startX + tileWidth;
				endY = startY + tileHeight;

				float left = startX / tileSheetPixelWidth;
				float right = left + (endX - startX)  / tileSheetPixelWidth;

				float bottom = (tileSheetPixelWidth - endY) / tileSheetPixelWidth;
				float top = bottom + (endY - startY)  / tileSheetPixelWidth;

				uvs[0] = new Vector2(left, bottom);
				uvs[1] = new Vector2(right, top);
				uvs[2] = new Vector2(right, bottom);
				uvs[3] = new Vector2(left, top);

				frameMeshes[i].uv = uvs;
				frameMeshes[i].name = "animFrameQuad";
			}

			isInitialised = true;

	        if (randomStartFrame)
	        {
	            int randomIndex = Random.Range(0, numberOfFrames);
	            DrawFrame(randomIndex);
	        }
            else
            {
                DrawFrame(curFrame);
            }
        }
	}

	void Update () 
	{
		if (Application.isPlaying)
		{
			if (GlobalSettings.surfGamePaused)
				return;
		}

		Initialise();

        if(playOnce)
        {
            animationTimer += Time.deltaTime;
            if(animationTimer > frameDelay)
            {
                animationTimer = 0f;
                currentFrameIndex ++;
                if(currentFrameIndex >= animationFrames.Length)
                {
                    playOnce = false;
                }
                else
                {
                    DrawFrame(animationFrames[currentFrameIndex]);
                }
            }
        }
        
		if(!autoPlay)
			return;

		animationTimer += Time.deltaTime;
		if(animationTimer > frameDelay)
		{
			animationTimer = 0f;
			currentFrameIndex ++;
			if(currentFrameIndex >= animationFrames.Length)
			{
				currentFrameIndex = 0;	
			}
		}
		
		DrawFrame(animationFrames[currentFrameIndex]);
	}

   
    public void PlayOnce(int fromFrame = 0)
    {       
        currentFrameIndex = fromFrame;
        DrawFrame(animationFrames[currentFrameIndex]);

        playOnce = true;
    }

	/* public void UpdateMesh()
	{
		meshFilter = GetComponent<MeshFilter>();

		Mesh curMesh = null;

        if (Application.isPlaying)
            curMesh = meshFilter.mesh;
		else
			curMesh = meshFilter.sharedMesh;

		if (curMesh != null)
		{
			if (curMesh.name != "animFrameQuad")
			{
				if (Application.isPlaying)
					DestroyImmediate(curMesh, true);

				curMesh = null;
			}
			else
			{
				if (Application.isPlaying)
					curMesh = meshFilter.mesh;
				else
					curMesh = null;
			}
		}
	} */

	public void DrawFrame(int frameToDraw)
	{
		if (frameMeshes == null)
		{
			isInitialised = false;
		}
		Initialise();

		if (frameToDraw == -1)
			return;

		if (meshFilter == null)
			meshFilter = GetComponent<MeshFilter>();

		if (Application.isPlaying)
		{
			if (frameToDraw < frameMeshes.Length)
				meshFilter.mesh = frameMeshes[frameToDraw];
			else
			{
				Debug.Log("FailFrame from " + gameObject.name + " frameToDraw :" + frameToDraw);
			}	
		}
		else if(frameMeshes != null && frameToDraw < frameMeshes.Length)
		{
			meshFilter.sharedMesh = frameMeshes[frameToDraw];
		}

		curFrame = frameToDraw;
	}

}
