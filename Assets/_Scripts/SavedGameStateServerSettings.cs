using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

public class SavedGameStateServerSettings
{
	static SavedGameStateServerSettings instance;

	public static SavedGameStateServerSettings SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new SavedGameStateServerSettings();

			return instance;
		}
	}

	public SavedGameState SavedGameStateData { get; set; }

	protected SavedGameStateServerSettings()
	{
		if(!PlayerPrefs.HasKey("sgsGSSS"))
		{
			SavedGameStateData = InitialiseSavedGameState();
			UpdateGameStateDataToLocalStore(SavedGameStateData);
		}
		else
		{
			string json = PlayerPrefs.GetString("sgsGSSS");
			SavedGameStateData = JsonUtility.FromJson<SavedGameState>(json);
		}
	}

	SavedGameState InitialiseSavedGameState()
	{
		SavedGameState newInstance = new SavedGameState();

		newInstance.Coins = GameState.SharedInstance.Cash;
		newInstance.EnergyDrinks = GameState.SharedInstance.EnergyDrink;
		newInstance.CurrentLevel = GoalManager.SharedInstance.goalDifficultyLevel;

		return newInstance;
	}

	public bool DataIsDirty 
	{
		get { return (PlayerPrefs.GetInt("sgsDDDTY", 0) != 0); }
	}

	public void FlagDataDirty()
	{
		PlayerPrefs.SetInt("sgsDDDTY", 1); 
	}

	public void FlagDataClean()
	{
		PlayerPrefs.SetInt("sgsDDDTY", 0); 
	}

	public void UpdateGameStateDataToLocalStore(SavedGameState gameStateData)
	{
		string json = JsonUtility.ToJson(gameStateData);
		PlayerPrefs.SetString("sgsGSSS", json);
	}

	public void UpdateLocalDataWithSavedGameState()
	{
		GameState.SharedInstance.Cash = SavedGameStateData.Coins;
		GameState.SharedInstance.EnergyDrink = SavedGameStateData.EnergyDrinks;
		GoalManager.SharedInstance.goalDifficultyLevel = SavedGameStateData.CurrentLevel;
		GameState.SharedInstance.Multiplier = SavedGameStateData.CurrentLevel;

		GameState.SharedInstance.Upgrades.RestorePurchasedUpgradeItemFromSeparatedString(SavedGameStateData.ShopUpgradeItemsString);

	}

	public void RefreshDataWithLocalGameState()
	{
		SavedGameStateData.Coins = GameState.SharedInstance.Cash;
		SavedGameStateData.EnergyDrinks = GameState.SharedInstance.EnergyDrink;
		SavedGameStateData.CurrentLevel = GoalManager.SharedInstance.goalDifficultyLevel;

		SavedGameStateData.ShopUpgradeItemsString = GameState.SharedInstance.Upgrades.GetPurchasedUpgradeItemsSeparatedString();
	}

}