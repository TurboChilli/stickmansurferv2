﻿using UnityEngine;
using System.Collections;
using System;

// This script is for the timer display on the starter pack panel
public class StarterPackDialogTimer : MonoBehaviour {

    private DateTime openDateTime;
    private TimeSpan timeDifference;
    private bool timeIsUp = false;
    public AudioSource countDownSound;
    public AudioSource timeUpSound;
    private bool playTimesUpSound = false;
    private double totalSecs = 0;
    private int countdown = 5;
    public StarterPackDialogController starterPackDialogController;
    public StarterPackDialog starterPackDialog;

    public bool TimeStatus
    {
        get
        {
            return timeIsUp;
        }
        set 
        {
            timeIsUp = value;
        }
    }

    public int TimerLength
    {
        get
        {
			return GameServerSettings.SharedInstance.SurferSettings.StarterPackShowSeconds;
        }
    }

    public int GetTimeInSeconds()
    {
		return GameServerSettings.SharedInstance.SurferSettings.StarterPackShowSeconds;
    }

    // Use this for initialization
    public void Start () 
    {
        
    }

    // Update is called once per frame
    void Update () {

        timeDifference = openDateTime.Subtract(DateTime.UtcNow);
        totalSecs = timeDifference.TotalSeconds;

        if (totalSecs > 0)
        {            
            string result = TimeStringFormatter(timeDifference); 

            if (starterPackDialogController != null)
            {
                starterPackDialog.SetTimerText(result);

                if(totalSecs < 1)
                {
                    starterPackDialog.SetTimerText(result);
                }
            }

            if(((int)totalSecs == countdown || totalSecs < (double)countdown) && (int)totalSecs != 0)
            {
                if (countDownSound != null)
                {
                    GlobalSettings.PlaySound(countDownSound);
                }

                playTimesUpSound = true;
                countdown--;
            }
            if((int)totalSecs == 0 && playTimesUpSound)
            {
                if (timeUpSound != null)
                {
                    GlobalSettings.PlaySound(timeUpSound);
                }

                playTimesUpSound = false;
                countdown = 5;
            }
        }
        else
        {
            timeIsUp = true;
        }
    }

    public void InitializeTimer(int seconds)
    {
        openDateTime = DateTime.UtcNow.AddSeconds(seconds);
        GameState.SharedInstance.StarterPackDialogTimeStamp = openDateTime.ToBinary().ToString();
        timeIsUp = false;
    }

    public DateTime GetOldTime()
    {
        long temp = Convert.ToInt64(GameState.SharedInstance.StarterPackDialogTimeStamp);
        openDateTime = DateTime.FromBinary(temp);

        return openDateTime;
    }

    public string TimeStringFormatter(TimeSpan timeDifference)
    {
        string result = string.Format("{0:D1}:{1:D2}:{2:D2}",
            timeDifference.Hours,
            timeDifference.Minutes,
            timeDifference.Seconds);

        return result;
    }
}
