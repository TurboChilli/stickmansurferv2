﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class EnergyDrinkDialog : MonoBehaviour {

	public Camera mainCamera;
	public GameObject closeButton;
	public TextMesh coinRequiredText;

	public GameObject energyDrinkButton;
	public TextMesh energyDrinkCostText;
	public TMP_Text titleText;
	public EnergyDrinkShopController energyDrinkShopController;

	public delegate void EnergyDrinkSpendSuccessful(int energyDrinksUsed);
	public event EnergyDrinkSpendSuccessful OnEnergyDrinkSpendSuccessful;

	public delegate void CloseButtonClicked();
	public event CloseButtonClicked OnCloseButtonClicked;

	public GameObject blackOverlay;
	private Collider blackOverlayCollider;
	private Material blackOverlayMaterial;
	private Color origBlackOverlayColor;

	public bool isShowing = false;
	private Vector3 inactivePosition;
	private Vector3 activePosition;
	private float timeToMove = 0.35f;
	private float curTimeToMove;

	public JeepTimer jeepTimer;
	public AudioSource energyDrinkUseSound;

	private bool initialised = false;

	int? energyDrinkCost;
	int? coinRequired;

	void Start () 
	{
		Init();
 	}

 	void Init()
 	{
		if (!initialised)
		{
			initialised = true;
		
			UIHelpers.SetTransformToCenter(this.transform, mainCamera);
			activePosition = transform.position;
			inactivePosition = activePosition + Vector3.right * 2000.0f;

			blackOverlayCollider = blackOverlay.GetComponent<Collider>();
			blackOverlayMaterial = blackOverlay.GetComponent<Renderer>().material;
			origBlackOverlayColor = blackOverlayMaterial.color;

			Hide(false);

			if(energyDrinkShopController == null)
				energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();

			if(energyDrinkShopController != null)
				energyDrinkShopController.Hide();

			if (jeepTimer != null)
				jeepTimer.StartTimer();
		}

 	}

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.EnergyDrinkCostDialog)
			{
				DialogTrackingFlags.FlagJustClosedPopupDialog();

				Hide();
			}
		}
		#endif 
	}
		
	void Update () 
	{
		CheckAndroidBackButton();

		if (jeepTimer != null)
			jeepTimer.UpdateTimer();

		if (isShowing)
		{
			if (curTimeToMove >= 0)
			{
				float lerpAmount = curTimeToMove / timeToMove;
				transform.position = Vector3.Lerp(inactivePosition, activePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
				blackOverlayMaterial.color = new Color(origBlackOverlayColor.r, origBlackOverlayColor.g, origBlackOverlayColor.b, (1.0f - Mathf.Pow(lerpAmount, 3.0f)) * origBlackOverlayColor.a );

				curTimeToMove -= Time.deltaTime;
			}
		}
		else
		{
			if (curTimeToMove >= 0)
			{
				float lerpAmount = curTimeToMove / timeToMove;
				transform.position = Vector3.Lerp(activePosition, inactivePosition, Mathf.Pow( 1.0f - lerpAmount, 3.0f));
				blackOverlayMaterial.color = new Color(origBlackOverlayColor.r, origBlackOverlayColor.g, origBlackOverlayColor.b,  Mathf.Pow( lerpAmount, 3.0f) * origBlackOverlayColor.a);

				curTimeToMove -= Time.deltaTime;
			}
		}


		if( Input.GetMouseButtonDown(0))
		{
			if(UIHelpers.CheckButtonHit(closeButton.transform, mainCamera))// || UIHelpers.CheckButtonHit(blackOverlay.transform, mainCamera))
			{
				if(OnCloseButtonClicked != null)
					OnCloseButtonClicked();
				
				Hide();
			}
			else if(UIHelpers.CheckButtonHit(energyDrinkButton.transform, mainCamera) && energyDrinkCost.HasValue)
			{
				if(GameState.SharedInstance.EnergyDrink >= energyDrinkCost.Value)
				{
					GameState.SharedInstance.LoseEnergyDrink(energyDrinkCost.Value);

					// If it is used as an energy drinks wap dialog, [coinRequired] will be set.
					if(coinRequired.HasValue)
					{
						int coinsFromEnergyDrinkSwap = energyDrinkCost.Value * GameServerSettings.SharedInstance.SurferSettings.EnergyDrinkToGoldRatio;
						GameState.SharedInstance.AddCash(coinsFromEnergyDrinkSwap);
					}

					if (energyDrinkUseSound != null)
						GlobalSettings.PlaySound(energyDrinkUseSound);

					if(OnEnergyDrinkSpendSuccessful != null)
						OnEnergyDrinkSpendSuccessful(energyDrinkCost.Value);

					coinRequired = null;
					energyDrinkCost = null;

					Hide();
				}
				else if(energyDrinkShopController != null)
				{
					energyDrinkShopController.Show(mainCamera);
				}
			}
		}
	}

	public void SetCoinRequiredText(int coinRequiredParam)
	{
		coinRequired = coinRequiredParam;

		if(coinRequiredText != null)
			coinRequiredText.text =  string.Format("{0:#,0}", coinRequired.Value);
	}


	public void SetEnergyDrinkCostText(int energyDrinkCost)
	{
		this.energyDrinkCost = energyDrinkCost;

		if(energyDrinkCostText != null)
			energyDrinkCostText.text = string.Format("{0:#,0}", energyDrinkCost);
	}

	public void SetTitleText(string title)
	{
		if(titleText != null)
			titleText.text = title;
	}

	public void Hide(bool animated = true)
	{
		if (isShowing)
		{
			if (!initialised)
				Init();

			DialogTrackingFlags.EnergyDrinkCostDialog = false;

			if (blackOverlayCollider != null)
				blackOverlayCollider.enabled = false;

			if (animated)
			{
				curTimeToMove = timeToMove;
				isShowing = false;
			}
			else
			{
				transform.position = inactivePosition;
				isShowing = false;
				blackOverlayMaterial.color = new Color(origBlackOverlayColor.r, origBlackOverlayColor.g, origBlackOverlayColor.b, 0.0f);
			}
		}
	}

	public void Show()
	{
		DialogTrackingFlags.EnergyDrinkCostDialog = true;

		blackOverlayCollider.enabled = true;
	
		UIHelpers.SetTransformToCenter(this.transform, mainCamera);
		activePosition = transform.position;
		inactivePosition = activePosition + Vector3.right * 3000.0f;

		transform.position = inactivePosition;
		blackOverlay.transform.position = activePosition;
		blackOverlayMaterial.color = new Color(origBlackOverlayColor.r, origBlackOverlayColor.g, origBlackOverlayColor.b, 0.0f);

		curTimeToMove = timeToMove;
		isShowing = true;
	}
}
