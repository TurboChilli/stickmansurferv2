﻿
#define FACEBOOK_ANALYTICS
#define FLURRY_ANALYTICS

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Prime31;
using Facebook.Unity;


public class AnalyticsAndCrossPromoManager : MonoBehaviour
{
	private AnalyticsAndCrossPromoManager()
	{
		
	}

	private const string startSessionScope = "start_game";
	private const string endSurfGameScope = "end_surf_game";
	private const string endJeepGameScope = "end_jeep_game";

	private const int coinRound = 50;
	private const int distanceRound = 100;

	private const string flurryKey = "P2PYXRVPZ84PG9N7X8PG";

	private static AnalyticsAndCrossPromoManager instance;
	public static AnalyticsAndCrossPromoManager SharedInstance
	{
		get
		{
			if(instance == null)
			{
				GameObject go = new GameObject();
				instance = go.AddComponent<AnalyticsAndCrossPromoManager>();
				DontDestroyOnLoad(go);
				go.name = "AnalyticsManager";
			}

			return instance;
		}
	}

	private bool isInitialised = false;

	public bool isAdWaiting = false;
	private List<string> adsWaitingScope = new List<string>(); 
	private List<float> adsWaitingTime = new List<float>();


	public void Initialise()
	{
		if (!isInitialised)
		{
			NotifyStartSessionAd();
			//Prime31.FlurryAnalytics.startSession(flurryKey, true);

			isInitialised = true;
		}
	}

	public void NotifyStartSessionAd()
	{
	}

	public void NotifyEndSurfGame()
	{
		
	}

	public void NotifyEndJeepGame()
	{

	}


	public void LogIAP()
	{
		if(IAPProductPurchaseRecord.IAPMatchesVerifiedPurchase())
		{
			//#if FACEBOOK_ANALYTICS && !UNITY_EDITOR
			if (FB.IsInitialized)
			{
				FB.LogPurchase(
					IAPProductPurchaseRecord.Price,
					IAPProductPurchaseRecord.CurrencyString,
					new Dictionary<string, object>
					{
						{ "StickSurferSettingId", GameServerSettings.SharedInstance.SurferSettings.StickSurferSettingId },
						{ "SettingsVersionNumber", GameServerSettings.SharedInstance.SurferSettings.SettingsVersionNumber },
						{ "ProductId", IAPProductPurchaseRecord.VerifiedPurchaseProductId },
						{ "Quantity", IAPProductPurchaseRecord.Quantity.ToString() },
						{ "PurchaseTransactionData", IAPProductPurchaseRecord.PurchaseTransactionData },
						{ "PurchaseTransactionIdentifier", IAPProductPurchaseRecord.PurchaseTransactionIdentifier },
					});
			}
			//#endif
		}

		IAPProductPurchaseRecord.ClearPurchaseRecord();
	}

	public void LogSurfGamePlayed(int coinsMadeThisGame, int energyDrinksMadeThisGame, float distanceTraveled)
	{
		Dictionary<string, object> dataUpsightInput = new Dictionary<string, object>();
		dataUpsightInput.Add( "Coins Made", ((coinsMadeThisGame / coinRound) * coinRound));
		dataUpsightInput.Add( "Energy Drinks Made", energyDrinksMadeThisGame);
		dataUpsightInput.Add( "Distance Traveled", ((distanceTraveled / distanceRound) * distanceRound));

		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();
		dataFlurryInput.Add( "Coins Made", ((coinsMadeThisGame / coinRound) * coinRound).ToString());
		dataFlurryInput.Add( "Energy Drinks Made", energyDrinksMadeThisGame.ToString());
		dataFlurryInput.Add( "Distance Traveled", ((distanceTraveled / distanceRound) * distanceRound).ToString());

		LogEvent("Game Played", dataFlurryInput);

	}

	public void LogJeepGamePlayed(int coinsMadeThisGame, int energyDrinksMadeThisGame, float distanceTraveled)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		dataInput.Add( "Coins Made", ((coinsMadeThisGame / coinRound) * coinRound));
		dataInput.Add( "Energy Drinks Made", energyDrinksMadeThisGame);
		dataInput.Add( "Distance Traveled", ((distanceTraveled / distanceRound) * distanceRound));

		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();
		dataFlurryInput.Add( "Coins Made", ((coinsMadeThisGame / coinRound) * coinRound).ToString());
		dataFlurryInput.Add( "Energy Drinks Made", energyDrinksMadeThisGame.ToString());
		dataFlurryInput.Add( "Distance Traveled", ((distanceTraveled / distanceRound) * distanceRound).ToString());

		LogEvent("Jeep Game Played", dataFlurryInput);

	}

	public void LogShopPurchase(UpgradeItem boughtItem, int energyDrinksUsed)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		if ( boughtItem.TotalUpgradeStages == 1)
			dataInput.Add("Item Type", UpgradeItemTypeTitle.Get(boughtItem.ItemType));
		else 
			dataInput.Add("Item Type",  string.Format("{0}_{1}", UpgradeItemTypeTitle.Get(boughtItem.ItemType), boughtItem.CurrentUpgradeStage));
		
		dataInput.Add( "Energy Drinks Used", energyDrinksUsed);
		dataFlurryInput.Add("Energy Drinks Used", energyDrinksUsed.ToString());

		LogEvent("Shop Item Purchased", dataFlurryInput);
	}

	public void LogRetryGame( int energyDrinkCost, float distance)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drink Cost", energyDrinkCost);
		dataInput.Add("Distance", ((distance / distanceRound) * distanceRound));

		dataFlurryInput.Add("Energy Drink Cost", energyDrinkCost.ToString());
		dataFlurryInput.Add("Distance", ((distance / distanceRound) * distanceRound).ToString());

		LogEvent("Retry purchased", dataFlurryInput);

	}

	public void LogVideoWatched(int coinsEarned, int energyDrinkEarned)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drinks Earned", energyDrinkEarned);
		dataInput.Add("Coins Earned", ((coinsEarned / coinRound) * coinRound));

		dataFlurryInput.Add("Energy Drinks Earned", energyDrinkEarned.ToString());
		dataFlurryInput.Add("Coins Earned", ((coinsEarned / coinRound) * coinRound).ToString());

		LogEvent("Video Watched", dataFlurryInput);
	}

	public void LogTreasureOpened(int coinsEarned, int energyDrinkEarned)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drinks Earned", energyDrinkEarned);
		dataInput.Add("Coins Earned", ((coinsEarned / coinRound) * coinRound));

		dataFlurryInput.Add("Energy Drinks Earned", energyDrinkEarned.ToString());
		dataFlurryInput.Add("Coins Earned", ((coinsEarned / coinRound) * coinRound).ToString());

		LogEvent("Treasure Opened", dataFlurryInput);
	}

	public void LogGoalSkipped(int energyDrinkCost, Goal goalSkipped)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Goal ", GoalText.Get(goalSkipped));
		dataInput.Add("Energy Drink Cost", energyDrinkCost);

		dataFlurryInput.Add("Goal ", GoalText.Get(goalSkipped));
		dataFlurryInput.Add("Energy Drink Cost", energyDrinkCost.ToString());

		LogEvent("Goal Skipped", dataFlurryInput);
	}

	public void LogJeepTimerSkipped(int energyDrinkCost)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drink Cost", energyDrinkCost);

		dataFlurryInput.Add("Energy Drink Cost", energyDrinkCost.ToString());

		LogEvent("Jeep Timer Skipped", dataFlurryInput);
	}

	public void LogSkateboardTimerSkipped(int energyDrinkCost)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drink Cost", energyDrinkCost);

		dataFlurryInput.Add("Energy Drink Cost", energyDrinkCost.ToString());

		LogEvent("Skateboard Timer Skipped", dataFlurryInput);
	}

	public void LogGameConsoleSkipped(int energyDrinkCost)
	{
		Dictionary<string, object> dataInput = new Dictionary<string, object>();
		Dictionary<string, string> dataFlurryInput = new Dictionary<string, string>();

		dataInput.Add("Energy Drink Cost", energyDrinkCost);

		dataFlurryInput.Add("Energy Drink Cost", energyDrinkCost.ToString());

		LogEvent("GameConsole Timer Skipped", dataFlurryInput);
	}

	//FLURRY FUNCTIONS
	public void LogEvent(string eventString)
	{
		Debug.Log("Logging Event: " + eventString);
		FacebookLogEvent(eventString, null);
        // TODO: Replace this with GameAnalytics
		//Prime31.FlurryAnalytics.logEvent(eventString, null);
	}

	public void LogEvent( string eventString, Dictionary<string, string> eventParams)
	{
		Debug.Log("Logging Event: " + eventString);
	
		FacebookLogEvent(eventString, eventParams);
        // TODO: Replace this with GameAnalytics
        //#if FLURRY_ANALYTICS && !UNITY_EDITOR
        //Prime31.FlurryAnalytics.logEvent(eventString, eventParams);
		//#endif
	}

	void FacebookLogEvent(string eventString, Dictionary<string, string> eventParams)
	{ 
		//#if FACEBOOK_ANALYTICS && !UNITY_EDITOR
		if (FB.IsInitialized)
		{
			Dictionary<string, object> fbEventParams = null;

			if(eventParams == null)
			{
				eventParams = new Dictionary<string, string>();
			}

			eventParams.Add("StickSurferSettingId", GameServerSettings.SharedInstance.SurferSettings.StickSurferSettingId);
			eventParams.Add("SettingsVersionNumber", GameServerSettings.SharedInstance.SurferSettings.SettingsVersionNumber);

			fbEventParams = new Dictionary<string, object>();
			foreach(var keyValuePair in eventParams)
			{
				if(!fbEventParams.ContainsKey(keyValuePair.Key))
				{
					fbEventParams.Add(keyValuePair.Key, keyValuePair.Value);
				}
			}

			FB.LogAppEvent(eventString, null, fbEventParams);
		}
		//#endif
	}
}
