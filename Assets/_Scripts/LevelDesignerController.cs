﻿using UnityEngine;
using System.Collections;

public class LevelDesignerController : MonoBehaviour 
{

	void Start () {
	
	}

	void Update () {
	
	}

	public void MainMenuButtonClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.Undefined;
		SceneNavigator.NavigateTo(SceneType.Menu);
	}

	public void MainBeachClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.MainBeach;
		SceneNavigator.NavigateTo(SceneType.SurfMain);
	}

	public void TikiBayClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.TikiBay;
		SceneNavigator.NavigateTo(SceneType.SurfMain);
	}

	public void WaveParkClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.WavePark;
		SceneNavigator.NavigateTo(SceneType.SurfMain);
	}

	public void ShipWreckClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.ShipWreck;
		SceneNavigator.NavigateTo(SceneType.SurfMain);
	}

	public void HarborClicked()
	{
		GlobalSettings.ForceDebugLocation = Location.Harbor;
		SceneNavigator.NavigateTo(SceneType.SurfMain);
	}
}
