﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.CognitoIdentity;
using Amazon.Runtime;

public class CommonDynamoDBContext : MonoBehaviour {


	void Awake()
	{	
		UnityInitializer.AttachToGameObject(this.gameObject);
		_client = Client;
	}

	#region Amazon DynamoDB Context

	const string IdentityPoolId = "us-east-1:6444d9a9-1591-4957-8ccf-613de08fab07";
	string CognitoPoolRegion = RegionEndpoint.USEast1.SystemName;
	string DynamoRegion = RegionEndpoint.USEast1.SystemName;

	RegionEndpoint _CognitoPoolRegion
	{
		get { return RegionEndpoint.GetBySystemName(CognitoPoolRegion); }
	}

	RegionEndpoint _DynamoRegion
	{
		get { return RegionEndpoint.GetBySystemName(DynamoRegion); }
	}

	static IAmazonDynamoDB _ddbClient;

	AWSCredentials _credentials;
	AWSCredentials Credentials
	{
		get
		{
			if (_credentials == null)
				_credentials = new CognitoAWSCredentials(IdentityPoolId, _CognitoPoolRegion);
			return _credentials;
		}
	}

	protected IAmazonDynamoDB Client
	{
		get
		{
			if (_ddbClient == null)
			{
				_ddbClient = new AmazonDynamoDBClient(Credentials, _DynamoRegion);
			}

			return _ddbClient;
		}
	}

	IAmazonDynamoDB _client;
	DynamoDBContext _context;

	public DynamoDBContext Context
	{
		get
		{
			if(_context == null)
				_context = new DynamoDBContext(_client);

			return _context;
		}
	}

	#endregion
}
