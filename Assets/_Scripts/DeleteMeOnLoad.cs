﻿using UnityEngine;
using System.Collections;

public class DeleteMeOnLoad : MonoBehaviour {

	void Awake()
	{
		DestroyImmediate(this.gameObject);
	}
}
