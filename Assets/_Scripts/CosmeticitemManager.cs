﻿using UnityEngine;
using System.Collections;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(CosmeticitemManager))]
public class CosmeticitemManagerEditor : Editor
{
	private CosmeticitemManager owner = null;

	public override void OnInspectorGUI()
	{
		owner = target as CosmeticitemManager;

		EditorGUI.BeginChangeCheck();

		base.OnInspectorGUI();

		if (EditorGUI.EndChangeCheck () || GUILayout.Button("Draw"))
		{
			//sort list of animating objects
			if (owner.playerFrames != null)
			{
				if (owner.playerFrames.Length == CosmeticitemManager.numPlayerFrames)
				{
					for (int i = 0; i < CosmeticitemManager.numPlayerFrames; i++)
					{
						if (owner.playerFrames[i] != null)
							owner.playerFrames[i].DrawFrame(i);
					}
				}
			}


			owner.DrawCurrentCosmetics();
			owner.DrawCurrentEmotion();
			EditorUtility.SetDirty(owner);
			Undo.RecordObject(owner, "Changed anim cosmetic");
		}

		if (GUILayout.Button("Load Item File"))
		{
			owner.UpdateCosmeticTransforms();
			owner.DrawCurrentCosmetics();
			EditorUtility.SetDirty(owner);
			Undo.RecordObject(owner, "Updated anim cosmetic");
		}

		if (GUILayout.Button("Save Item File"))
		{
			owner.SaveCosmeticTransforms();
			//owner.UpdateCosmeticTransforms();
			owner.DrawCurrentCosmetics();
		
			EditorUtility.SetDirty(owner);
			Undo.RecordObject(owner, "Saved anim cosmetic");
		}



		if (GUILayout.Button("Load Emotion Item File"))
		{
			owner.UpdateEmotionTransforms();
			owner.DrawCurrentEmotion();
			EditorUtility.SetDirty(owner);
			Undo.RecordObject(owner, "Updated emotion cosmetic");
		}

		if (GUILayout.Button("Save Emotion Item File"))
		{
			owner.SaveEmotionTransforms();
			//owner.UpdateCosmeticTransforms();
			owner.DrawCurrentEmotion();
		
			EditorUtility.SetDirty(owner);
			Undo.RecordObject(owner, "Saved emotion cosmetic");
		}

	}
};
#endif

public class CosmeticitemManager : MonoBehaviour 
{
	public const int numPlayerFrames = 64;

	//public 
	public Cosmetic curCosmetic = Cosmetic.Disguise;
	public Emotion curEmotion = Emotion.Content;

	public AnimatingObject[] cosmeticItemObjects;
	public AnimatingObject[] emotionItemObjects;
	public AnimatingObject[] playerFrames;

	public AnimatingObject jeepCosmeticHead;
	public AnimatingObject jeepEmotionObject;

	public void UpdateCosmeticTransforms()
	{	
        #if UNITY_EDITOR
		AssetDatabase.Refresh();
        #endif
		TextAsset cosmeticFile = Resources.Load("CosmeticFiles/cosmetic_" + (int)curCosmetic) as TextAsset;
	
		if (cosmeticFile == null)
		{
			SaveCosmeticTransforms();
			cosmeticFile = Resources.Load("CosmeticFiles/cosmetic_" + (int)curCosmetic) as TextAsset;
		}	

		//parse the string
		string[] strings = cosmeticFile.text.Split('\n');
		//////Debug.Log("Loading: " + strings[0]);

		string[] parsedStrings = strings[1].Split('_');

		jeepCosmeticHead.transform.localPosition  = new Vector3(	float.Parse(parsedStrings[1]),
																			float.Parse(parsedStrings[2]),
																			float.Parse(parsedStrings[3]));

		jeepCosmeticHead.transform.localRotation  = new Quaternion(	float.Parse(parsedStrings[5]),
																			float.Parse(parsedStrings[6]),
																			float.Parse(parsedStrings[7]),
																			float.Parse(parsedStrings[8]));

		jeepCosmeticHead.transform.localScale = new Vector3(	float.Parse(parsedStrings[10]),
																	float.Parse(parsedStrings[11]),
																	float.Parse(parsedStrings[12]));
		int stringIndex = 2;
		for (int i = 0; i < numPlayerFrames; i++)
		{
			while (strings[stringIndex] == "")
				stringIndex ++;

			parsedStrings = strings[stringIndex].Split('_');
			cosmeticItemObjects[i].transform.localPosition  = new Vector3(	float.Parse(parsedStrings[1]),
																			float.Parse(parsedStrings[2]),
																			float.Parse(parsedStrings[3]));

			cosmeticItemObjects[i].transform.localRotation  = new Quaternion(	float.Parse(parsedStrings[5]),
																				float.Parse(parsedStrings[6]),
																				float.Parse(parsedStrings[7]),
																				float.Parse(parsedStrings[8]));

			cosmeticItemObjects[i].transform.localScale = new Vector3(	float.Parse(parsedStrings[10]),
																		float.Parse(parsedStrings[11]),
																		float.Parse(parsedStrings[12]));

			stringIndex++;
		}	
	}

	public void UpdateEmotionTransforms()
	{	
        #if UNITY_EDITOR
		AssetDatabase.Refresh();
        #endif
		TextAsset emotionFile = Resources.Load("CosmeticFiles/emotion") as TextAsset;
	
		if (emotionFile == null)
		{
			SaveEmotionTransforms();
			emotionFile = Resources.Load("CosmeticFiles/emotion") as TextAsset;
		}	

		//parse the string
		string[] strings = emotionFile.text.Split('\n');
		//////Debug.Log("Loading: " + strings[0]);

		string[] parsedStrings = strings[1].Split('_');

		jeepEmotionObject.transform.localPosition  = new Vector3(	float.Parse(parsedStrings[1]),
																			float.Parse(parsedStrings[2]),
																			float.Parse(parsedStrings[3]));

		jeepEmotionObject.transform.localRotation  = new Quaternion(	float.Parse(parsedStrings[5]),
																			float.Parse(parsedStrings[6]),
																			float.Parse(parsedStrings[7]),
																			float.Parse(parsedStrings[8]));

		jeepEmotionObject.transform.localScale = new Vector3(	float.Parse(parsedStrings[10]),
																	float.Parse(parsedStrings[11]),
																	float.Parse(parsedStrings[12]));
		int stringIndex = 2;
		for (int i = 0; i < numPlayerFrames; i++)
		{
			while (strings[stringIndex] == "")
				stringIndex ++;

			parsedStrings = strings[stringIndex].Split('_');
			emotionItemObjects[i].transform.localPosition  = new Vector3(	float.Parse(parsedStrings[1]),
																			float.Parse(parsedStrings[2]),
																			float.Parse(parsedStrings[3]));

			emotionItemObjects[i].transform.localRotation  = new Quaternion(	float.Parse(parsedStrings[5]),
																				float.Parse(parsedStrings[6]),
																				float.Parse(parsedStrings[7]),
																				float.Parse(parsedStrings[8]));

			emotionItemObjects[i].transform.localScale = new Vector3(	float.Parse(parsedStrings[10]),
																		float.Parse(parsedStrings[11]),
																		float.Parse(parsedStrings[12]));

			stringIndex++;
		}	
	}

	//position_x_y_z_rotation_x_y_z_w_scale_x_y_z_\n
	public void SaveCosmeticTransforms()
	{
		using (StreamWriter newFile = new StreamWriter(string.Format( "Assets/Resources/CosmeticFiles/cosmetic_{0}.txt", (int)curCosmetic)))
		{
			newFile.WriteLine("CosmeticFiles/cosmetic_{0}", (int)curCosmetic);

			string positionString = string.Format("{0}_{1}_{2}",
									jeepCosmeticHead.transform.localPosition.x,
									jeepCosmeticHead.transform.localPosition.y,
									jeepCosmeticHead.transform.localPosition.z );
			newFile.Flush();
									
			string rotationString = string.Format("{0}_{1}_{2}_{3}",
									jeepCosmeticHead.transform.localRotation.x,
									jeepCosmeticHead.transform.localRotation.y,
									jeepCosmeticHead.transform.localRotation.z,
									jeepCosmeticHead.transform.localRotation.w );
			newFile.Flush();
									
			string scaleString = string.Format("{0}_{1}_{2}",
									jeepCosmeticHead.transform.localScale.x,
									jeepCosmeticHead.transform.localScale.y,
									jeepCosmeticHead.transform.localScale.z );
			newFile.Flush();
									
			newFile.WriteLine( string.Format("Jeep position_{0}_rotation_{1}_scale_{2}",  
											positionString,
											rotationString,
											scaleString));
			newFile.Flush();



			newFile.Flush();
			for (int i = 0; i < cosmeticItemObjects.Length; i++)
			{
				positionString = string.Format("{0}_{1}_{2}",
										cosmeticItemObjects[i].transform.localPosition.x,
										cosmeticItemObjects[i].transform.localPosition.y,
										cosmeticItemObjects[i].transform.localPosition.z );
				newFile.Flush();
										
				rotationString = string.Format("{0}_{1}_{2}_{3}",
										cosmeticItemObjects[i].transform.localRotation.x,
										cosmeticItemObjects[i].transform.localRotation.y,
										cosmeticItemObjects[i].transform.localRotation.z,
										cosmeticItemObjects[i].transform.localRotation.w );
				newFile.Flush();
										
				scaleString = string.Format("{0}_{1}_{2}",
										cosmeticItemObjects[i].transform.localScale.x,
										cosmeticItemObjects[i].transform.localScale.y,
										cosmeticItemObjects[i].transform.localScale.z );
				newFile.Flush();
										
				newFile.WriteLine( string.Format("position_{0}_rotation_{1}_scale_{2}",  
												positionString,
												rotationString,
												scaleString));
				newFile.Flush();
			}
		}
		#if UNITY_EDITOR
		AssetDatabase.Refresh();
		#endif
		//////Debug.Log("Done saving CosmeticFiles/cosmetic_" + (int)curCosmetic );
	}

	public void SaveEmotionTransforms()
	{
		using (StreamWriter newFile = new StreamWriter(string.Format( "Assets/Resources/CosmeticFiles/emotion.txt")))
		{
			newFile.WriteLine("CosmeticFiles/emotion");

			string positionString = string.Format("{0}_{1}_{2}",
									jeepEmotionObject.transform.localPosition.x,
									jeepEmotionObject.transform.localPosition.y,
									jeepEmotionObject.transform.localPosition.z );
			newFile.Flush();
									
			string rotationString = string.Format("{0}_{1}_{2}_{3}",
									jeepEmotionObject.transform.localRotation.x,
									jeepEmotionObject.transform.localRotation.y,
									jeepEmotionObject.transform.localRotation.z,
									jeepEmotionObject.transform.localRotation.w );
			newFile.Flush();
									
			string scaleString = string.Format("{0}_{1}_{2}",
									jeepEmotionObject.transform.localScale.x,
									jeepEmotionObject.transform.localScale.y,
									jeepEmotionObject.transform.localScale.z );
			newFile.Flush();
									
			newFile.WriteLine( string.Format("Jeep position_{0}_rotation_{1}_scale_{2}",  
											positionString,
											rotationString,
											scaleString));
			newFile.Flush();
			newFile.Flush();

			for (int i = 0; i < emotionItemObjects.Length; i++)
			{
				positionString = string.Format("{0}_{1}_{2}",
											emotionItemObjects[i].transform.localPosition.x,
											emotionItemObjects[i].transform.localPosition.y,
											emotionItemObjects[i].transform.localPosition.z );
				newFile.Flush();
										
				rotationString = string.Format("{0}_{1}_{2}_{3}",
											emotionItemObjects[i].transform.localRotation.x,
											emotionItemObjects[i].transform.localRotation.y,
											emotionItemObjects[i].transform.localRotation.z,
											emotionItemObjects[i].transform.localRotation.w );
				newFile.Flush();
										
				scaleString = string.Format("{0}_{1}_{2}",
											emotionItemObjects[i].transform.localScale.x,
											emotionItemObjects[i].transform.localScale.y,
											emotionItemObjects[i].transform.localScale.z );
				newFile.Flush();
										
				newFile.WriteLine( string.Format("position_{0}_rotation_{1}_scale_{2}",  
												positionString,
												rotationString,
												scaleString));
				newFile.Flush();
			}
		}

		#if UNITY_EDITOR
		AssetDatabase.Refresh();
		#endif
		//////Debug.Log("Done saving CosmeticFiles/cosmetic_" + (int)curCosmetic );
	}

	public void DrawCurrentCosmetics()
	{
		for (int i = 0; i < cosmeticItemObjects.Length; i++)
		{
			cosmeticItemObjects[i].DrawFrame((int)curCosmetic);
		}
		jeepCosmeticHead.DrawFrame((int)curCosmetic);
	}

	public void DrawCurrentEmotion()
	{
		for (int i = 0; i < emotionItemObjects.Length; i++)
		{
			emotionItemObjects[i].DrawFrame((int)curEmotion);
		}
		jeepEmotionObject.DrawFrame((int)curEmotion);
	}

	public static bool LoadJeepCosmeticItem(Cosmetic cosmetic, ref Vector3 position, ref Quaternion rotation, ref Vector3 scale)
	{
		#if UNITY_EDITOR
		    AssetDatabase.Refresh();
        #endif
		TextAsset cosmeticFile = Resources.Load("CosmeticFiles/cosmetic_" + (int)cosmetic) as TextAsset;

		if (cosmeticFile == null)
		{
			//////Debug.Log("Cosmetic File not found");
			return false;
		}

		//parse the string
		string[] strings = cosmeticFile.text.Split('\n');
		string[] parsedStrings = strings[1].Split('_');

		position  = new Vector3(	float.Parse(parsedStrings[1]),
									float.Parse(parsedStrings[2]),
									float.Parse(parsedStrings[3]));

		rotation  = new Quaternion(	float.Parse(parsedStrings[5]),
									float.Parse(parsedStrings[6]),
									float.Parse(parsedStrings[7]),
									float.Parse(parsedStrings[8]));

		scale = new Vector3(	float.Parse(parsedStrings[10]),
								float.Parse(parsedStrings[11]),
								float.Parse(parsedStrings[12]));

		
		//////Debug.Log("Finished Jeep Loading: " + strings[0]);
		return true;	
	}

	public static bool LoadJeepEmotion(ref Vector3 position, ref Quaternion rotation, ref Vector3 scale)
	{
		#if UNITY_EDITOR
		    AssetDatabase.Refresh();
        #endif
		TextAsset emotionFile = Resources.Load("CosmeticFiles/emotion") as TextAsset;

		if (emotionFile == null)
		{
			//////Debug.Log("Cosmetic File not found");
			return false;
		}

		//parse the string
		string[] strings = emotionFile.text.Split('\n');
		string[] parsedStrings = strings[1].Split('_');

		position  = new Vector3(	float.Parse(parsedStrings[1]),
									float.Parse(parsedStrings[2]),
									float.Parse(parsedStrings[3]));

		rotation  = new Quaternion(	float.Parse(parsedStrings[5]),
									float.Parse(parsedStrings[6]),
									float.Parse(parsedStrings[7]),
									float.Parse(parsedStrings[8]));

		scale = new Vector3(	float.Parse(parsedStrings[10]),
								float.Parse(parsedStrings[11]),
								float.Parse(parsedStrings[12]));

		
		//////Debug.Log("Finished Jeep Loading: " + strings[0]);
		return true;	
	}

	public static bool LoadCosmeticItems(Cosmetic cosmetic, ref Vector3[] positions, ref Quaternion[] rotations, ref Vector3[] scales)
	{
        #if UNITY_EDITOR
		    AssetDatabase.Refresh();
        #endif
		TextAsset cosmeticFile = Resources.Load("CosmeticFiles/cosmetic_" + (int)cosmetic) as TextAsset;

		if (cosmeticFile == null)
		{
			//////Debug.Log("Cosmetic File not found");
			return false;
		}

		positions = new Vector3[numPlayerFrames];
		rotations = new Quaternion[numPlayerFrames];
		scales = new Vector3[numPlayerFrames];

		//parse the string
		string[] strings = cosmeticFile.text.Split('\n');
		int stringIndex = 2;
		for (int i = 0; i < numPlayerFrames; i++)
		{
			while (strings[stringIndex] == "")
				stringIndex ++;

			string[] parsedStrings = strings[stringIndex].Split('_');
			positions[i]  = new Vector3(	float.Parse(parsedStrings[1]),
											float.Parse(parsedStrings[2]),
											float.Parse(parsedStrings[3]));

			rotations[i]  = new Quaternion(	float.Parse(parsedStrings[5]),
											float.Parse(parsedStrings[6]),
											float.Parse(parsedStrings[7]),
											float.Parse(parsedStrings[8]));

			scales[i] = new Vector3(	float.Parse(parsedStrings[10]),
										float.Parse(parsedStrings[11]),
										float.Parse(parsedStrings[12]));

			stringIndex++;
		}
		//////Debug.Log("Finished Loading: " + strings[0]);

		return true;	
	}

	public static bool LoadEmotionObjects(ref Vector3[] positions, ref Quaternion[] rotations, ref Vector3[] scales)
	{
		#if UNITY_EDITOR
		    AssetDatabase.Refresh();
        #endif
		TextAsset emotionFile = Resources.Load("CosmeticFiles/emotion") as TextAsset;

		if (emotionFile == null)
		{
			//////Debug.Log("Emotion File not found");
			return false;
		}

		positions = new Vector3[numPlayerFrames];
		rotations = new Quaternion[numPlayerFrames];
		scales = new Vector3[numPlayerFrames];

		//parse the string
		string[] strings = emotionFile.text.Split('\n');
		int stringIndex = 2;
		for (int i = 0; i < numPlayerFrames; i++)
		{
			while (strings[stringIndex] == "")
				stringIndex ++;

			string[] parsedStrings = strings[stringIndex].Split('_');
			positions[i]  = new Vector3(	float.Parse(parsedStrings[1]),
											float.Parse(parsedStrings[2]),
											float.Parse(parsedStrings[3]));

			rotations[i]  = new Quaternion(	float.Parse(parsedStrings[5]),
											float.Parse(parsedStrings[6]),
											float.Parse(parsedStrings[7]),
											float.Parse(parsedStrings[8]));

			scales[i] = new Vector3(	float.Parse(parsedStrings[10]),
										float.Parse(parsedStrings[11]),
										float.Parse(parsedStrings[12]));

			stringIndex++;
		}
		return true;	
	}

};

public enum Emotion
{
	Content = 63,
	Mad = 62,
	Ecstatic = 61,
	Determined = 60,
	Scared = 59
};

public enum Cosmetic
{
	Disguise = 0,
	TopHat = 1,
	RastaHat = 2,
	Mullet = 3,
	PaperBag = 4,
	PumpkinHead = 5,
	StickmanCap = 6,
    PirateHat = 7,
    Mohawk = 8,
    CowboyHat = 9,
    EmoHair = 10,

    Shark = 11,
    Newspaper = 12,
    Captain = 13,
    Duck = 14,
    Snorkel = 15,
    Pineapple = 16,
    Elf = 17,
    Octopus = 18,
    Swimcap = 19,
    NekoEars = 20,
    Cat = 21,
	Count,
	None = 9999
};
