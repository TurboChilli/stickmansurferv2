﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class LanguageSelectController : MonoBehaviour {

	public Camera languageSelectCamera;
	public GameObject closeButton;
	public AudioSource soundClick;

	public TMP_Text title;
	public Material selectedButtonMaterial;

	public Transform englishButton;
	public Transform spanishButton;
	public Transform chineseSimplifiedButton;
	public Transform chineseTraditionalButton;
	public Transform frenchButton;
	public Transform japaneseButton;
	public Transform germanButton;
	public Transform indonesianButton;
	public Transform portugeseButton;
	public Transform italianButton;
	public Transform koreanButton;
	public Transform russianButton;


	public delegate void Close();
	public event Close OnClose;

	public Material unselectedButtonMaterial;
	Camera launchingCameraReference;
	float clickDelay = -1f;

	public enum DialogPanelState
	{
		Hidden,
		Visible,
	}

	DialogPanelState state = DialogPanelState.Hidden;
	public DialogPanelState State
	{
		get { return state; }
		private set { state = value; }
	}

	public void Show(Camera launchedFromCamera)
	{
		DialogTrackingFlags.MainMenuSettingsLanguageMenuShowing = true;

		launchingCameraReference = launchedFromCamera;

		launchingCameraReference.enabled = false;
		languageSelectCamera.enabled = true;
		clickDelay = 0.2f;
		State = DialogPanelState.Visible;
	}

	public void Hide()
	{
		DialogTrackingFlags.MainMenuSettingsLanguageMenuShowing = false;

		State = DialogPanelState.Hidden;

		if(OnClose != null)
			OnClose();
		
		languageSelectCamera.enabled = false;

		if(launchingCameraReference != null)
			launchingCameraReference.enabled = true;
	}

	void Start () 
	{
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 65.0f, 65.0f, closeButton.transform, languageSelectCamera);
		unselectedButtonMaterial = englishButton.GetComponent<Renderer>().material;
		SetSelectedLanguageButtonMaterial(GameLanguage.SelectedLanguage);
	}

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape) && DialogTrackingFlags.MainMenuSettingsLanguageMenuShowing)
		{
			DialogTrackingFlags.FlagJustClosedPopupDialog();

			Hide();
		}
		#endif 
	}

	void Update () {

		CheckAndroidBackButton();

		if(clickDelay >= 0)
			clickDelay -= Time.deltaTime;


		if(Input.GetMouseButtonUp(0) && State == DialogPanelState.Visible)
		{
			if(clickDelay > 0 || languageSelectCamera == null || !languageSelectCamera.isActiveAndEnabled)
				return;
			
			if(UIHelpers.CheckButtonHit(closeButton.transform, languageSelectCamera))
			{
				//GlobalSettings.PlaySound(soundClick);
				Hide();
			}
			else if(UIHelpers.CheckButtonHit(englishButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.English);
			}
			else if(UIHelpers.CheckButtonHit(spanishButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Spanish);
			}
			else if(UIHelpers.CheckButtonHit(chineseSimplifiedButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.ChineseSimplified);
			}
			else if(UIHelpers.CheckButtonHit(chineseTraditionalButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.ChineseTraditional);
			}
			else if(UIHelpers.CheckButtonHit(frenchButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.French);
			}
			else if(UIHelpers.CheckButtonHit(japaneseButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Japanese);
			}
			else if(UIHelpers.CheckButtonHit(germanButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.German);
			}
			else if(UIHelpers.CheckButtonHit(indonesianButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Indonesian);
			}
			else if(UIHelpers.CheckButtonHit(portugeseButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Portuguese);
			}
			else if(UIHelpers.CheckButtonHit(italianButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Italian);
			}
			else if(UIHelpers.CheckButtonHit(koreanButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Korean);
			}
			else if(UIHelpers.CheckButtonHit(russianButton.transform, languageSelectCamera))
			{
				SetSelectedLanguage(SystemLanguage.Russian);
			}
		}
	}

	void SetButtonsMaterial(Material mat)
	{
		englishButton.GetComponent<Renderer>().material = mat;
		spanishButton.GetComponent<Renderer>().material = mat;
		chineseSimplifiedButton.GetComponent<Renderer>().material = mat;
		chineseTraditionalButton.GetComponent<Renderer>().material = mat;
		frenchButton.GetComponent<Renderer>().material = mat;
		japaneseButton.GetComponent<Renderer>().material = mat;
		germanButton.GetComponent<Renderer>().material = mat;
		indonesianButton.GetComponent<Renderer>().material = mat;
		portugeseButton.GetComponent<Renderer>().material = mat;
		italianButton.GetComponent<Renderer>().material = mat;
		koreanButton.GetComponent<Renderer>().material = mat;
		russianButton.GetComponent<Renderer>().material = mat;
	}



	void SetSelectedLanguage(SystemLanguage lang)
	{
		//GlobalSettings.PlaySound(soundClick);
		SetButtonsMaterial(unselectedButtonMaterial);

		GameLanguage.SelectedLanguage = lang;

		// Refresh all text in scene with new language
		Localizer[] localizedTexts = FindObjectsOfType<Localizer>();
		if(localizedTexts != null)
		{
			foreach(Localizer localizedText in localizedTexts)
			{
				localizedText.LoadLocalizedText();
			}
		}

		SetSelectedLanguageButtonMaterial(lang);
	}

	void SetSelectedLanguageButtonMaterial(SystemLanguage lang)
	{
		switch(lang)
		{
		case SystemLanguage.English:
			englishButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Spanish:
			spanishButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.ChineseSimplified:
			chineseSimplifiedButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.ChineseTraditional:
			chineseTraditionalButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Chinese:
			chineseSimplifiedButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.French:
			frenchButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Japanese:
			japaneseButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.German:
			germanButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Indonesian:
			indonesianButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Portuguese:
			portugeseButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Italian:
			italianButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Korean:
			koreanButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		case SystemLanguage.Russian:
			russianButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		default:
			englishButton.GetComponent<Renderer>().material = selectedButtonMaterial;
			break;
		}
	}

}

