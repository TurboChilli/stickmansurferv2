﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public enum SceneType
{
	Undefined,
	SurfMain,
	SurfMainLoading,
	Menu,
	Shop,
	Shack,
	HalfPipe,
	ShellGame,
	JeepSand,
    RetroGame,
	FidgetSpinner
}

public class SceneNavigator
{
	static SceneType currentScene = SceneType.Undefined;
	static SceneType previousScene = SceneType.Undefined;

    public static string GetSceneTextFromEnum(SceneType scene)
    {
        string sceneToLoad = "";

        switch(scene)
        {
            case SceneType.SurfMain:
                sceneToLoad = "SceneSurfMain";
                break;
            case SceneType.SurfMainLoading:
                sceneToLoad = "SceneSurfMainLoading";
                break;
            case SceneType.Menu:
                sceneToLoad = "SceneMainMenu";
                break;
            case SceneType.Shop:
                sceneToLoad = "SceneShop";
                break;
            case SceneType.Shack:
                sceneToLoad = "SceneShack";
                break;
            case SceneType.HalfPipe:
                sceneToLoad = "SceneHalfPipe";
                break;
            case SceneType.ShellGame:
                sceneToLoad = "SceneShellGame";
                break;
            case SceneType.JeepSand:
                sceneToLoad = "SceneJeepSand";
                break;
            case SceneType.RetroGame:
                sceneToLoad = "SceneRetroGames";
                break;
			case SceneType.FidgetSpinner:
				sceneToLoad = "SceneFidgetSpinner";
				break;
            default:
                break;
        }

        return sceneToLoad;
    }

	public static void NavigateTo(SceneType scene)
	{
        string sceneToLoad = GetSceneTextFromEnum(scene);

		previousScene = currentScene;
		currentScene = scene;

		if(!string.IsNullOrEmpty(sceneToLoad))
		{

			SceneManager.LoadScene(sceneToLoad);
		}
	}

	public static void NavigateToPreviousScene()
	{
		if(previousScene == SceneType.Undefined)
			NavigateTo(SceneType.Menu);
		else
			NavigateTo(previousScene);
	}

	public static SceneType PreviousScene
	{
		get { return previousScene; }
	}

	public static void AutoNavigateToLocationIfTutorial(Activity activity)
	{
		
		if(!activity.IsTutorial)
			return;
		/*
		if(previousScene == SceneType.Undefined)
		{
			switch(activity.Location)
			{
			case Location.Map:
				SceneNavigator.NavigateTo(SceneType.Menu);
				break;
			case Location.Shack:
				SceneNavigator.NavigateTo(SceneType.Shack);
				break;
			case Location.Shop:
			case Location.ChangeRide:
				SceneNavigator.NavigateTo(SceneType.Shop);
				break;
			case Location.ShellGame:
				SceneNavigator.NavigateTo(SceneType.ShellGame);
				break;
			case Location.HalfPipe:
				SceneNavigator.NavigateTo(SceneType.HalfPipe);
				break;
			case Location.FourWheelDriveGame:
				SceneNavigator.NavigateTo(SceneType.JeepSand);
				break;
			case Location.MainBeach:
			case Location.BigWaveReef:
			case Location.WavePark:
			case Location.Mexico:
			case Location.NuclearPlant:
			case Location.Volcano:
				SceneNavigator.NavigateTo(SceneType.SurfMain);
				break;
			default:
				break;
			}
		}
		*/

	}

}
