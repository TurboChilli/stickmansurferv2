﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class GoalManager
{
	public Goal[] goals;

	private List<Func<Goal, bool>> isGoalCompleteFunctions = new List<Func<Goal, bool>>();
	private List<Func<Goal, float>> goalDonePercentageFunctions = new List<Func<Goal, float>>();

	public int goalDifficultyLevel 
	{
		get
		{
			return PlayerPrefs.GetInt("GM_DifficultyLevel", 1);
		}
		set
		{
			PlayerPrefs.SetInt("GM_DifficultyLevel", value);
		}
	}

	public const int MaxNumGoals = 3;

	//Ingame values
	public float[] scoreThisGame = new float[NumVehicle.Count];
	public float[] distanceThisGame = new float[NumVehicle.Count];
	public int[] coinsThisGame = new int[NumVehicle.Count];
	public int[] locationVisits = new int[(int)Location.Count];
	public int[] vehiclesCollected = new int[NumVehicle.Count];
	public int grindsHit = 0;
	public bool collectedCoin = false;

	public int multiplierThisGame = 0;
	public int slalomsHitThisGame = 0;
	public int slalomsMissThisGame = 0;

	private GoalType[] easyGoals;
	private GoalType[] hardGoals;

	static GoalManager instance;
	public static GoalManager SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new GoalManager();

			return instance;
		}
	}
		
	// Construction 
	public GoalManager()
	{
		//refresh all values
		isGoalCompleteFunctions.Add( 	FuncPlayNumGames);

		isGoalCompleteFunctions.Add( 	FuncReachDistance);
		isGoalCompleteFunctions.Add( 	FuncReachDistanceVehicle);
		isGoalCompleteFunctions.Add( 	FuncReachTotalDistance);
		isGoalCompleteFunctions.Add( 	FuncReachTotalDistanceVehicle);
		//isGoalCompleteFunctions.Add( 	FuncReachDistanceNoCoins);

		isGoalCompleteFunctions.Add( 	FuncReachScore);
		//isGoalCompleteFunctions.Add( 	FuncReachTotalScore);
		//isGoalCompleteFunctions.Add( 	FuncReachMultiplier);

		isGoalCompleteFunctions.Add( 	FuncGetCoins);
		isGoalCompleteFunctions.Add( 	FuncGetTotalCoins);
		//isGoalCompleteFunctions.Add( 	FuncHaveCoins);
		isGoalCompleteFunctions.Add( 	FuncGetCoinsVehicle);

		isGoalCompleteFunctions.Add( 	FuncVisitLocation);
		isGoalCompleteFunctions.Add( 	FuncVisitTotalLocation);

		isGoalCompleteFunctions.Add( 	FuncGetVehicle);
		isGoalCompleteFunctions.Add( 	FuncGetTotalVehicle);

		isGoalCompleteFunctions.Add( 	FuncGrind);
		isGoalCompleteFunctions.Add( 	FuncGrindTotal);

		isGoalCompleteFunctions.Add( 	FuncSlalomsHit);				
		//isGoalCompleteFunctions.Add( 	FuncSlalomsMissed);
		isGoalCompleteFunctions.Add( 	FuncSlalomHitOverall);
		//isGoalCompleteFunctions.Add( 	FuncSlalomMissedOverall);
		isGoalCompleteFunctions.Add(	FuncCollectSURFLetters);


		goalDonePercentageFunctions.Add( 	FuncPercentPlayNumGames);

		goalDonePercentageFunctions.Add( 	FuncPercentReachDistance);
		goalDonePercentageFunctions.Add( 	FuncPercentReachDistanceVehicle);
		goalDonePercentageFunctions.Add( 	FuncPercentReachTotalDistance);
		goalDonePercentageFunctions.Add( 	FuncPercentReachTotalDistanceVehicle);
		//goalDonePercentageFunctions.Add( 	FuncReachDistanceNoCoins);

		goalDonePercentageFunctions.Add( 	FuncPercentReachScore);
		//goalDonePercentageFunctions.Add( 	FuncReachTotalScore);
		//goalDonePercentageFunctions.Add( 	FuncReachMultiplier);

		goalDonePercentageFunctions.Add( 	FuncPercentGetCoins);
		goalDonePercentageFunctions.Add( 	FuncPercentGetTotalCoins);
		//goalDonePercentageFunctions.Add( 	FuncHaveCoins);
		goalDonePercentageFunctions.Add( 	FuncPercentGetCoinsVehicle);

		goalDonePercentageFunctions.Add( 	FuncPercentVisitLocation);
		goalDonePercentageFunctions.Add( 	FuncPercentVisitTotalLocation);

		goalDonePercentageFunctions.Add( 	FuncPercentGetVehicle);
		goalDonePercentageFunctions.Add( 	FuncPercentGetTotalVehicle);

		goalDonePercentageFunctions.Add( 	FuncPercentGrind);
		goalDonePercentageFunctions.Add( 	FuncPercentGrindTotal);

		goalDonePercentageFunctions.Add( 	FuncPercentSlalomsHit);				
		//goalDonePercentageFunctions.Add( 	FuncPercentSlalomsMissed);
		goalDonePercentageFunctions.Add( 	FuncPercentSlalomHitOverall);
		//goalDonePercentageFunctions.Add( 	FuncPercentSlalomMissedOverall);
		goalDonePercentageFunctions.Add(	FuncPercentCollectSURFLetters);


		easyGoals = new GoalType[]{ 
			GoalType.PlayNumGames,
			GoalType.ReachDistance,
			GoalType.ReachTotalDistance,
			GoalType.ReachScore,
			//GoalType.ReachTotalScore,
			//GoalType.ReachMultiplier,
			GoalType.GetCoins,
			GoalType.GetTotalCoins,
			//GoalType.HaveCoins,
			GoalType.VisitLocation,
			GoalType.GetVehicle,
			GoalType.CollectSURFLetters
		};

		hardGoals = new GoalType[]{
			GoalType.ReachDistanceVehicle,
			GoalType.ReachTotalDistanceVehicle,
			//GoalType.ReachDistanceNoCoins,
			GoalType.GetCoinsVehicle,
			GoalType.VisitTotalLocation,
			GoalType.GetTotalVehicle,
			GoalType.Grind,
			GoalType.GrindTotal,
			GoalType.SlalomsHit,
			GoalType.SlalomHitOverall,
		};

		StartNewGame();
	}

	public void StartNewGame()
	{
		scoreThisGame = new float[NumVehicle.Count];
		distanceThisGame = new float[NumVehicle.Count];
		coinsThisGame = new int[NumVehicle.Count];
		locationVisits = new int[(int)Location.Count];
		vehiclesCollected = new int[NumVehicle.Count];
		grindsHit = 0;

		for (int i = 0; i < NumVehicle.Count; i++)
		{
			scoreThisGame[i] = 0;
			distanceThisGame[i] = 0;
			coinsThisGame[i] = 0;
			vehiclesCollected[i] = 0;
		}

		for (int i = 0; i < (int)Location.Count; i++)
		{
			locationVisits[i] = 0;
		}

		multiplierThisGame = 0;
		slalomsHitThisGame = 0;
		slalomsMissThisGame = 0;

		collectedCoin = false;

		UpdateGoals();
	}

	public int GetEnergyDrinkCost()
	{
		return Mathf.CeilToInt(goalDifficultyLevel * GameServerSettings.SharedInstance.SurferSettings.EnergyDrinkCostPerLevel);
	}

	public void AddDifficultyLevel()
	{
		goalDifficultyLevel += 1;
		SavedGameStateServerSettings.SharedInstance.SavedGameStateData.CurrentLevel = goalDifficultyLevel;
		SavedGameStateServerSettings.SharedInstance.FlagDataDirty();
	}

	public void UpdateDistance(float distanceAdded, Vehicle curVehicle)
	{
		//to get a zero index
		if (VehicleHelper.IsStartingBoard(curVehicle))
			curVehicle = Vehicle.None;

		if (curVehicle != Vehicle.None)
			distanceThisGame[0] += distanceAdded;

		distanceThisGame[(int)curVehicle] += distanceAdded;
		GameState.SharedInstance.TotalDistanceTraveled += distanceAdded;
		GameState.SharedInstance.AddVehcleDistance(curVehicle, distanceAdded);
	}

	public void UpdateScore(float scoreAdded, Vehicle curVehicle)
	{
		//to get a zero index
		if (VehicleHelper.IsStartingBoard(curVehicle))
			curVehicle = Vehicle.None;

		if (curVehicle != Vehicle.None)
			scoreThisGame[0] += scoreAdded;

		scoreThisGame[(int)curVehicle] += scoreAdded;
		GameState.SharedInstance.TotalScore = GameState.SharedInstance.TotalScore + scoreAdded;
	}

	public void UpdateCoins(float addedCoins, Vehicle curVehicle)
	{
		//to get a zero index
		if (VehicleHelper.IsStartingBoard(curVehicle))
			curVehicle = Vehicle.None;

		if (curVehicle != Vehicle.None)
		{
			coinsThisGame[0] += (int)addedCoins;
		}
		coinsThisGame[(int)curVehicle] += (int)addedCoins;

		collectedCoin = true;
		GameState.SharedInstance.AddTotalCash(curVehicle, (int)addedCoins);

		UpdateGoals();
	}

	public void UpdateLocationVisits(Location location)
	{
		locationVisits[(int)location] ++;
		GameState.SharedInstance.VisitLocation(location);
	}

	public void UpdateMultiplier(int curMultiplier)
	{
		if (!GlobalSettings.SurfedPassMinTutorialDistance())
    		return;

		multiplierThisGame = curMultiplier;
	}

	public void UpdateGrinds()
	{
		if (!GlobalSettings.SurfedPassMinTutorialDistance())
    		return;

		grindsHit ++;
		GameState.SharedInstance.AddGrindHit();
	}	

	public void UpdateVehicleCollects(Vehicle curVehicle)
	{		
		GameState.SharedInstance.AddVehicleCollect(curVehicle);
		vehiclesCollected[(int)curVehicle] ++;
	}

	public void AddSlalomHit()
	{
		slalomsHitThisGame += 1;
		GameState.SharedInstance.AddSlalomHit();
	}

	public void UpdateGoals()
	{
		if (goals == null)
		{
			RefreshGoals();
		}

		if (!GlobalSettings.SurfedPassMinTutorialDistance())
			return;
				
		for (int i = 0; i < MaxNumGoals; i++)
		{
			if (goals[i].checkIsGoalComplete(goals[i]) && !goals[i].complete)
			{
				//////Debug.Log("Finished Goal of type: " + goals[i].goalType);

				PlayerPrefs.SetInt("GM_Goal" + i + "_complete", 1);
				goals[i].complete = true;

				GoalManager.SharedInstance.UpdateGoals();
				PlayerPrefs.Save();

				RefreshGoals();
			}
		}
	}

	public void ClearGoals()
	{
		PlayerPrefs.SetInt("GM_LastGoal0", PlayerPrefs.GetInt("GM_Goal0", -1));
		PlayerPrefs.SetInt("GM_LastGoal1", PlayerPrefs.GetInt("GM_Goal1", -1));
		PlayerPrefs.SetInt("GM_LastGoal2", PlayerPrefs.GetInt("GM_Goal2", -1));

		PlayerPrefs.SetInt("GM_Goal0", -1);
		PlayerPrefs.SetInt("GM_Goal1", -1);
		PlayerPrefs.SetInt("GM_Goal2", -1);

		goals[0] = null;
		goals[1] = null;
		goals[2] = null;

		RefreshGoals();
		StartNewGame();
	}

	public void RefreshGoals()
	{
		if (goals == null)
			goals = new Goal[MaxNumGoals];

		for (int goalIndex = 0; goalIndex < 3; goalIndex++)
		{
			if (goals[goalIndex] == null)
			{
				int goalType = PlayerPrefs.GetInt("GM_Goal" + goalIndex, -1);
				if (goalType == -1 || !DoesGoalExist(goalType))
					goals[goalIndex] = CreateNewGoal(goalIndex);
				else
					goals[goalIndex] = LoadInGoal(goalIndex, (GoalType)goalType);
			}
		}
		PlayerPrefs.Save();
	}

	private Goal LoadInGoal(int goalIndex, GoalType goalNum)
	{
		Goal returnGoal = new Goal();
		returnGoal.goalType = goalNum;
		returnGoal.complete = (PlayerPrefs.GetInt("GM_Goal" + goalIndex + "_complete", 0) == 1);
		returnGoal.hasShown = (PlayerPrefs.GetInt("GM_Goal" + goalIndex + "_hasShown", 0) == 1);
		returnGoal.arg1 = PlayerPrefs.GetFloat("GM_Goal" + goalIndex + "_arg1", -1f);
		returnGoal.arg2 = PlayerPrefs.GetFloat("GM_Goal" + goalIndex + "_arg2", -1f);
		returnGoal.arg3 = PlayerPrefs.GetFloat("GM_Goal" + goalIndex + "_arg3", -1f);

		returnGoal.checkIsGoalComplete = isGoalCompleteFunctions[(int)goalNum];

		#if UNITY_EDITOR
		//////Debug.Log("Loaded goal type of: " + returnGoal.goalType);
		#endif

		return returnGoal;
	}

	//AAAAALL THE ARGUMENTS	--------------------------------------------------------------------------------------------

	private Goal CreateNewGoal(int goalIndex)
	{	
		Goal returnGoal = new Goal();									
		int newGoalType = -2;	

		Goal checkGoal = CreateInitialGoals(goalIndex);
		if ( checkGoal != null)
			return checkGoal;	

		int otherGoalType1 =  PlayerPrefs.GetInt("GM_Goal" + (goalIndex + 1) % 3, -1);
		int otherGoalType2 =  PlayerPrefs.GetInt("GM_Goal" + (goalIndex + 2) % 3, -1);

		int prevGoalType1 = PlayerPrefs.GetInt("GM_LastGoal0", -1);
		int prevGoalType2 = PlayerPrefs.GetInt("GM_LastGoal1", -1);
		int prevGoalType3 = PlayerPrefs.GetInt("GM_LastGoal2", -1);

		while (	newGoalType == -2 || 
				newGoalType == otherGoalType1 ||
				newGoalType == otherGoalType2)
		{
			if (goalIndex == 2)
				newGoalType = (int)hardGoals[UnityEngine.Random.Range(0, hardGoals.Length)];
			else
				newGoalType = (int)easyGoals[UnityEngine.Random.Range(0, easyGoals.Length)];

			//check for simlar goals
			if (((GoalType)newGoalType == GoalType.VisitLocation || (GoalType)newGoalType == GoalType.VisitTotalLocation) 
				&& ( (GoalType)otherGoalType1 == GoalType.VisitLocation ||
					(GoalType)otherGoalType1 == GoalType.VisitTotalLocation ||
					(GoalType)otherGoalType2 == GoalType.VisitLocation ||
					(GoalType)otherGoalType2 == GoalType.VisitTotalLocation))
			{
				newGoalType = -2;
			}

			if (((GoalType)newGoalType == GoalType.GetVehicle || (GoalType)newGoalType == GoalType.GetTotalVehicle) 
				&& ( (GoalType)otherGoalType1 == GoalType.GetVehicle ||
					(GoalType)otherGoalType1 == GoalType.GetTotalVehicle ||
					(GoalType)otherGoalType2 == GoalType.GetVehicle ||
					(GoalType)otherGoalType2 == GoalType.GetTotalVehicle))
			{
				newGoalType = -2;
			}

			if ((GoalType)prevGoalType1 == (GoalType)newGoalType ||
				(GoalType)prevGoalType2 == (GoalType)newGoalType ||
				(GoalType)prevGoalType3 == (GoalType)newGoalType)
			{
				newGoalType = -2;
			}
		}
		returnGoal.goalType = (GoalType)newGoalType;

		returnGoal.complete = false;
		returnGoal.checkIsGoalComplete =  isGoalCompleteFunctions[(int)returnGoal.goalType];
		PlayerPrefs.SetInt("GM_Goal" + goalIndex, (int)returnGoal.goalType);
		PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_complete", 0);
		PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_hasShown", 0);

		switch (returnGoal.goalType)
		{
			case GoalType.PlayNumGames:
				returnGoal.arg1 = Mathf.Min((Mathf.Ceil(2 + goalDifficultyLevel * 0.25f)), 25);
				returnGoal.arg2 = (float)GameState.SharedInstance.GamesPlayed;
				returnGoal.arg3 = -1;
			break;

			case GoalType.ReachDistance:
				returnGoal.arg1 = Mathf.Min((200.0f + 25.0f * goalDifficultyLevel), 2500);
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;
			break;

			case GoalType.ReachTotalDistance:
				returnGoal.arg1 = Mathf.Min(200.0f * goalDifficultyLevel, 20000);
				returnGoal.arg2 = GameState.SharedInstance.TotalDistanceTraveled;
				returnGoal.arg3 = -1;
			break;


			case GoalType.ReachDistanceVehicle:
				returnGoal.arg1 = Mathf.Min(40.0f * Mathf.Ceil(goalDifficultyLevel * 0.15f), 600);
				returnGoal.arg2 = (float)VehicleHelper.GetWeightedRandomVehicle();
				returnGoal.arg3 = -1;
			break;

			case GoalType.ReachTotalDistanceVehicle:
				returnGoal.arg1 = Mathf.Min(100.0f + 25.0f * Mathf.Ceil(goalDifficultyLevel * 0.4f), 1000);
				returnGoal.arg2 = (float)VehicleHelper.GetWeightedRandomVehicle();
				returnGoal.arg3 = GameState.SharedInstance.GetTotalDistanceTravelInVehicle((Vehicle)((int)returnGoal.arg2));
			break;

			/*case GoalType.ReachDistanceNoCoins:
				returnGoal.arg1 = 100.0f + 50.0f * Mathf.Max(1.0f, Mathf.RoundToInt(0.5f * goalDifficultyLevel));
				returnGoal.arg2 = -1;//(float)UnityEngine.Random.Range(2, 10);
				returnGoal.arg3 = -1; //last total distance
			break;*/

			case GoalType.ReachScore:
				returnGoal.arg1 = Mathf.Min(1000.0f + 100.0f * Mathf.Ceil(goalDifficultyLevel * goalDifficultyLevel), 1000000);
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;
			break;

			/*case GoalType.ReachTotalScore:
				returnGoal.arg1 = Mathf.Min(1000.0f + 500.0f *  Mathf.Ceil(goalDifficultyLevel * goalDifficultyLevel), 5000000);
				returnGoal.arg2 = GameState.SharedInstance.TotalScore;
				returnGoal.arg3 = -1;
			break;
			*/
			/*case GoalType.ReachMultiplier:
				returnGoal.arg1 = GameState.SharedInstance.Multiplier + 4.0f;
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;
			break;				
			*/

			case GoalType.GetCoins:
				returnGoal.arg1 = Mathf.Min(100.0f + 50.0f * goalDifficultyLevel, 5000);
				returnGoal.arg2 = -1; 
				returnGoal.arg3 = -1;
			break;		
		
			case GoalType.GetTotalCoins:
				returnGoal.arg1 = Mathf.Min(250.0f * goalDifficultyLevel, 25000);
				returnGoal.arg2 = GameState.SharedInstance.TotalCash;
				returnGoal.arg3 = -1;
			break;		

			/*case GoalType.HaveCoins:
				returnGoal.arg1 = GameState.SharedInstance.Cash + 300.0f + 50.0f * Mathf.Ceil(0.5f * goalDifficultyLevel);
				returnGoal.arg2 = -1; 
				returnGoal.arg3 = -1;
			break;*/
								
			case GoalType.GetCoinsVehicle:
				returnGoal.arg2 =  (float)VehicleHelper.GetWeightedRandomVehicle();
				returnGoal.arg1 = Mathf.Min(20.0f + 5.0f * Mathf.Ceil(goalDifficultyLevel), 500);
				returnGoal.arg3 = (float)GameState.SharedInstance.GetTotalVehicleCash((Vehicle)returnGoal.arg2);
			break;

			case GoalType.VisitLocation:	
				if (goalDifficultyLevel < 50)
				{
					Location[] indoorLocations = LocationHelper.GetIndoorSections();
					returnGoal.arg1 = (float) indoorLocations[UnityEngine.Random.Range(0, indoorLocations.Length)];  
				}
				else
					returnGoal.arg1 = (float)UnityEngine.Random.Range(1, (int) LocationHelper.GetOutdoorSectionNum());  

				returnGoal.arg2 = Mathf.Min(Mathf.Ceil(goalDifficultyLevel * 0.05f), 2);
				returnGoal.arg3 = -1;	
			break;

			case GoalType.VisitTotalLocation:
			if (goalDifficultyLevel < 30)
				{
					Location[] indoorLocations = LocationHelper.GetIndoorSections();
					returnGoal.arg1 = (float) indoorLocations[UnityEngine.Random.Range(0, indoorLocations.Length)];  
				}
				else
				returnGoal.arg2 = Mathf.Min((Mathf.Ceil(2 + goalDifficultyLevel * 0.25f)), 25); 
				returnGoal.arg3 = GameState.SharedInstance.GetLocationVisits((Location)((int)returnGoal.arg1));
			break;

			case GoalType.GetVehicle:
				returnGoal.arg1 =  (float)VehicleHelper.GetWeightedRandomVehicle();
				returnGoal.arg2 = Mathf.Min(Mathf.Ceil(goalDifficultyLevel * 0.05f), 2);
				returnGoal.arg3 = -1;	
			break;

			case GoalType.GetTotalVehicle:
				returnGoal.arg1 = (float)VehicleHelper.GetWeightedRandomVehicle();
				returnGoal.arg2 = Mathf.Min((Mathf.Ceil(2 + goalDifficultyLevel * 0.25f)), 25); 
				returnGoal.arg3 = GameState.SharedInstance.GetTotalVehicleCollect((Vehicle)returnGoal.arg1);	
			break;				

			case GoalType.Grind:
				returnGoal.arg1 = Mathf.Min((Mathf.Ceil(goalDifficultyLevel * 0.24f)), 20); 
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;	
			break;		
					
			case GoalType.GrindTotal:				
				returnGoal.arg1 = Mathf.Min(goalDifficultyLevel, 100);
				returnGoal.arg2 = -1;
				returnGoal.arg3 = GameState.SharedInstance.GetGrindHitTotal();	
			break;

			case GoalType.SlalomsHit:
				returnGoal.arg1 = Mathf.Min((Mathf.Ceil(goalDifficultyLevel * 0.24f)), 20); 
				returnGoal.arg2 = -1; 
				returnGoal.arg3 = -1;	
			break;

			/*																								
			case GoalType.SlalomsMissed:
				returnGoal.arg1 = Mathf.Ceil(0.3334f * goalDifficultyLevel);
				returnGoal.arg2 = -1; 
				returnGoal.arg3 = -1;	
			break;
			*/

			case GoalType.SlalomHitOverall:
				returnGoal.arg1 = Mathf.Min(goalDifficultyLevel, 100);
				returnGoal.arg2 = GameState.SharedInstance.GetSlalomsHit();
				returnGoal.arg3 = -1;	
			break;

			/*case GoalType.SlalomMissedOverall:
				returnGoal.arg1 = Mathf.Ceil(0.5f * goalDifficultyLevel);
				returnGoal.arg2 = GameState.SharedInstance.GetSlalomsMiss();
				returnGoal.arg3 = -1;	
			break;
			*/
			case GoalType.CollectSURFLetters:
				returnGoal.arg1 = Mathf.Min( 1 + Mathf.Ceil(goalDifficultyLevel * 0.5f), 50);
				returnGoal.arg2 = GameState.SharedInstance.GetSURFCollect();
				returnGoal.arg3 = -1;
			break;
		};

		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg1", returnGoal.arg1);
		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg2", returnGoal.arg2);
		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg3", returnGoal.arg3);
		PlayerPrefs.Save();
		//////Debug.Log("Created new goal type: " + returnGoal.goalType);

		return returnGoal;
	}

	private Goal CreateInitialGoals(int goalIndex)
	{
		int goalsComplete = PlayerPrefs.GetInt("GM_InitialGoalsCreated", 0);
	
		Goal returnGoal = new Goal();
		switch (goalsComplete)
		{
			//First Batch
			case 0:
				returnGoal.goalType = GoalType.ReachDistance;
				returnGoal.arg1 = GameServerSettings.SharedInstance.SurferSettings.SurfMinTutorialDistance;
				returnGoal.arg2 =  -1; 
				returnGoal.arg3 = 0; //-1;
			break;

			case 1:
				returnGoal.goalType = GoalType.GetCoins;
				returnGoal.arg1 = 50;
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;
			break;

			case 2:
				returnGoal.goalType = GoalType.ReachScore;
				returnGoal.arg1 = GameServerSettings.SharedInstance.SurferSettings.SurfMinTutorialDistance + 50;
				returnGoal.arg2 = -1;
				returnGoal.arg3 = -1;
			break;

			//Second Batch
			case 3:
				returnGoal.goalType = GoalType.PlayNumGames;
				returnGoal.arg1 = 3;
				returnGoal.arg2 = (float)GameState.SharedInstance.GamesPlayed;
				returnGoal.arg3 = -1;
			break;

			case 4:
				returnGoal.goalType = GoalType.VisitLocation;
				returnGoal.arg1 = (float)Location.Cave;
				returnGoal.arg2 = 1;
				returnGoal.arg3 = -1;	
			break;		

			case 5:
				returnGoal.goalType = GoalType.CollectSURFLetters;
				returnGoal.arg1 = 1;
				returnGoal.arg2 = GameState.SharedInstance.GetSURFCollect();
				returnGoal.arg3 = -1;
			break;

			//Third Batch
			case 6:
				returnGoal.goalType = GoalType.GetVehicle;
				returnGoal.arg1 = (float)UnityEngine.Random.Range(2, 10); 
				returnGoal.arg2 = 1;
				returnGoal.arg3 = -1;
			break;
			default:
				return null;
		};

		PlayerPrefs.SetInt("GM_InitialGoalsCreated", goalsComplete + 1);

		returnGoal.complete = false;
		returnGoal.checkIsGoalComplete =  isGoalCompleteFunctions[(int)returnGoal.goalType];

		PlayerPrefs.SetInt("GM_Goal" + goalIndex, (int)returnGoal.goalType);
		PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_complete", 0);
		PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_hasShown", 0);
		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg1", returnGoal.arg1);
		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg2", returnGoal.arg2);
		PlayerPrefs.SetFloat("GM_Goal" + goalIndex + "_arg3", returnGoal.arg3);
		PlayerPrefs.Save();

		//////Debug.Log("Initial goal type: " + returnGoal.goalType);
		return returnGoal;
	}

	//AAAAALL THE PERCENTAGES	--------------------------------------------------------------------------------------------

	#region Percentage Functions

	private float FuncPercentPlayNumGames(Goal goal)
	{
		return ((float)GameState.SharedInstance.GamesPlayed - goal.arg2) / goal.arg1;
	}

	private float FuncPercentReachDistance(Goal goal)
	{
		return distanceThisGame[0] / goal.arg1;
	}

	private float FuncPercentReachDistanceVehicle(Goal goal)
	{	
		return distanceThisGame[(int)goal.arg2] / goal.arg1;
	}

	private float FuncPercentReachTotalDistance(Goal goal)
	{
		return (GameState.SharedInstance.TotalDistanceTraveled - goal.arg2) / goal.arg1;
	}

	private float FuncPercentReachTotalDistanceVehicle(Goal goal)
	{
		return (GameState.SharedInstance.GetTotalDistanceTravelInVehicle((Vehicle)((int)goal.arg2)) - goal.arg3) / goal.arg1;
	}

	private float FuncPercentReachScore(Goal goal)
	{
		return scoreThisGame[0] / goal.arg1;
	}

	private float FuncPercentGetCoins(Goal goal)
	{
		return (float)coinsThisGame[0] / goal.arg1;
	}

	private float FuncPercentGetTotalCoins(Goal goal)
	{
		return ((float)GameState.SharedInstance.TotalCash - goal.arg2) / goal.arg1;
	}

	private float FuncPercentGetCoinsVehicle(Goal goal)
	{
		return (((float)GameState.SharedInstance.GetTotalVehicleCash((Vehicle)goal.arg2)) - goal.arg3) / goal.arg1;
	}

	private float FuncPercentVisitLocation(Goal goal)
	{
		return (float)locationVisits[(int)goal.arg1] / goal.arg2;
	}

	private float FuncPercentVisitTotalLocation(Goal goal)
	{
		return ((float)GameState.SharedInstance.GetLocationVisits((Location)((int)goal.arg1)) - goal.arg3) / goal.arg2;
	}

	private float FuncPercentGetVehicle(Goal goal)
	{
		return (float)vehiclesCollected[(int)goal.arg1] / goal.arg2;
	}

	private float FuncPercentGetTotalVehicle(Goal goal)
	{
		return ((float)GameState.SharedInstance.GetTotalVehicleCollect((Vehicle)((int)goal.arg1)) - goal.arg3) / goal.arg2;
	}

	private float FuncPercentGrind(Goal goal)
	{		
		return (float)grindsHit / goal.arg1;
	}

	private float FuncPercentGrindTotal(Goal goal)
	{
		return ((float)GameState.SharedInstance.GetGrindHitTotal() - goal.arg3) / goal.arg1;
	}

	private float FuncPercentSlalomsHit(Goal goal)
	{
		return (float)slalomsHitThisGame / goal.arg1;
	}

	private float FuncPercentSlalomHitOverall(Goal goal)
	{
		return ((float)GameState.SharedInstance.GetSlalomsHit() - goal.arg2) / goal.arg1;
	}

	private float FuncPercentCollectSURFLetters(Goal goal)
	{
		return ((float)GameState.SharedInstance.GetSURFCollect() - goal.arg2 / goal.arg1);
	}


	#endregion

	//AAAAALL THE CHECKS	--------------------------------------------------------------------------------------------

	#region Check Functions

	private bool FuncPlayNumGames(Goal goal)
	{
		if (GameState.SharedInstance.GamesPlayed - goal.arg2  >= goal.arg1)
			return true;
		return false;
	}

	private bool FuncReachDistance(Goal goal)
	{
		if (distanceThisGame[0] >= goal.arg1)
			return true;
		return false;
	}

	private bool FuncReachDistanceVehicle(Goal goal)
	{	
		if (distanceThisGame[(int)goal.arg2] >= goal.arg1)
			return true;
		return false;
	}

	private bool FuncReachTotalDistance(Goal goal)
	{
		if  (GameState.SharedInstance.TotalDistanceTraveled > goal.arg2 + goal.arg1)
			return true;
		return false;
	}

	private bool FuncReachTotalDistanceVehicle(Goal goal)
	{
		if  (GameState.SharedInstance.GetTotalDistanceTravelInVehicle((Vehicle)((int)goal.arg2)) > goal.arg3 + goal.arg1)
			return true;
		return false;
	}

	private bool FuncReachDistanceNoCoins(Goal goal)
	{
		if (distanceThisGame[0] > goal.arg1 && !collectedCoin)
			return true;
		return false;
	}

	private bool FuncReachScore(Goal goal)
	{
		if (scoreThisGame[0] > goal.arg1)
			return true;
		return false;
	}

	private bool FuncGetCoins(Goal goal)
	{
		if (coinsThisGame[0] >= (int)goal.arg1)
			return true;
		return false;
	}

	private bool FuncGetTotalCoins(Goal goal)
	{
		if (GameState.SharedInstance.TotalCash >= (int)goal.arg1 + (int)goal.arg2 )
			return true;
		return false;
	}

	private bool FuncGetCoinsVehicle(Goal goal)
	{
		if (GameState.SharedInstance.GetTotalVehicleCash((Vehicle)goal.arg2) >= (int)goal.arg3 + (int)goal.arg1)
			return true;
		return false;
	}

	private bool FuncVisitLocation(Goal goal)
	{
		if (locationVisits[(int)goal.arg1] >= (int)goal.arg2)
			return true;
		return false;
	}

	private bool FuncVisitTotalLocation(Goal goal)
	{
		if (GameState.SharedInstance.GetLocationVisits((Location)((int)goal.arg1)) >= (int)goal.arg2 + (int)goal.arg3)
			return true;
		return false;
	}

	private bool FuncGetVehicle(Goal goal)
	{
		if (vehiclesCollected[(int)goal.arg1] >= (int)goal.arg2)
			return true;
		return false;
	}

	private bool FuncGetTotalVehicle(Goal goal)
	{
		if (GameState.SharedInstance.GetTotalVehicleCollect((Vehicle)((int)goal.arg1)) >= (int)goal.arg2 + (int)goal.arg3)
			return true;
		return false;
	}

	private bool FuncGrind(Goal goal)
	{		
		if (grindsHit >= (int)goal.arg1)
			return true;	
		return false;
	}

	private bool FuncGrindTotal(Goal goal)
	{
		if (GameState.SharedInstance.GetGrindHitTotal() >= (int)goal.arg1 + (int)goal.arg3)
			return true;
		return false;
	}

	private bool FuncSlalomsHit(Goal goal)
	{
		if (slalomsHitThisGame >= goal.arg1)
			return true;
		return false;
	}

	private bool FuncSlalomHitOverall(Goal goal)
	{
		if (GameState.SharedInstance.GetSlalomsHit() >= (int)goal.arg1 + (int)goal.arg2)
			return true;
		return false;
	}

	private bool FuncCollectSURFLetters(Goal goal)
	{
		if (GameState.SharedInstance.GetSURFCollect() >= goal.arg1 + goal.arg2)
			return true;
		return false;
	}

	public bool DoesGoalExist(int checkGoal)
	{
		return Enum.IsDefined( typeof(GoalType), checkGoal);
	}

	public int GetGoalTypeSize()
	{
		return Enum.GetNames( typeof(GoalType) ).Length;
	}
	#endregion
}

public class Goal
{
	public GoalType goalType;
	public bool complete = false;
	public bool hasShown = false;
	public Func<Goal, bool> checkIsGoalComplete;
	public float arg1;
	public float arg2;
	public float arg3;
}

public enum GoalType
{
	PlayNumGames = 0,					//Play x games

	ReachDistance = 1,					//reach x distance in one session
	ReachDistanceVehicle = 2,			//reach x distance in one session in vehicle
	ReachTotalDistance = 3,				//reach x total distance overall
	ReachTotalDistanceVehicle = 4,		//reach x total distance in a vehilce overall
	//ReachDistanceNoCoins = 5,			//reach x distance with no coins

	ReachScore = 5,						//reach x score in one session
	//ReachTotalScore = 6,				//reach x total score overall
	//ReachMultiplier = 7,				//Get x multipler overall

	GetCoins = 6,						//get x coins
	GetTotalCoins = 7,					//get x coins overall
	//HaveCoins = 11,					//have x number of coins 
	GetCoinsVehicle = 8,				//get x coins in a vehicle

	VisitLocation = 9,					//visit a location once
	VisitTotalLocation = 10,			//visit a location x time overall

	GetVehicle = 11,					//Get vehicle x times
	GetTotalVehicle = 12,				//Get vehicle x times overall

	Grind = 13,							//Grind x things
	GrindTotal = 14,					//Grind x things overall

	SlalomsHit = 15,						
	//SlalomsMissed,
	SlalomHitOverall = 16,
	//SlalomMissedOverall,

	CollectSURFLetters = 17,
};

public class GoalText
{
	public static string Get(Goal goal)
	{
		return string.Format("{0} {1}",GetGoalDescription(goal), GetGoalProgress(goal));
	}


	public static string GetGoalProgress(Goal goal)
	{
		if (goal.complete)
			return "";

		switch (goal.goalType)
		{
			case GoalType.PlayNumGames:
			{
				int gamesLeft = (int)(GameState.SharedInstance.GamesPlayed - goal.arg2);
				return string.Format("({0}/{1})", gamesLeft, (int)goal.arg1);
			}
			case GoalType.ReachDistance:
			{	
				return "";
				/*
				int metersLeft = (int)(GoalManager.SharedInstance.distanceThisGame[0]);
				return string.Format("({0}/{1})", metersLeft, (int)goal.arg1);*/
			}					
			case GoalType.ReachDistanceVehicle:
			{
				return "";
				/*int metersLeft = (int)(GoalManager.SharedInstance.distanceThisGame[(int)goal.arg2]);
				return string.Format("({0}/{1})", metersLeft, (int)goal.arg1);*/
			}			
			case GoalType.ReachTotalDistance:
			{
				int metersLeft = (int)(GameState.SharedInstance.TotalDistanceTraveled - (int)(goal.arg2));
				return string.Format("({0}/{1})", metersLeft, (int)goal.arg1);
			}				
			case GoalType.ReachTotalDistanceVehicle:
			{
				int metersLeft = (int)(GameState.SharedInstance.GetTotalDistanceTravelInVehicle((Vehicle)((int)goal.arg2)) - goal.arg3);
				return string.Format("({0}/{1})", metersLeft, (int)goal.arg1);
			}		
			/*case GoalType.ReachDistanceNoCoins:
			{
				if (!GoalManager.SharedInstance.collectedCoin)
				{
					int metersLeft = (int)GoalManager.SharedInstance.distanceThisGame[0];
					return string.Format("({0}/{1})", metersLeft, (int)goal.arg1);
				}
				else
					return string.Format("(Try again)");
			}
			*/
			case GoalType.ReachScore:
			{
				return "";/*
				int scoreLeft = (int)GoalManager.SharedInstance.scoreThisGame[0];
				return string.Format("({0} / {1})", (int)scoreLeft, (int)goal.arg1); */
			}						
			/*case GoalType.ReachTotalScore:
			{
				int scoreLeft = (int)(GameState.SharedInstance.TotalScore - goal.arg2);
				return string.Format("({0}/{1})", (int)scoreLeft, (int)goal.arg1);
			}*/				
			/*case GoalType.ReachMultiplier:
			{
				return "";
				/*
				int multiLeft = GoalManager.SharedInstance.multiplierThisGame;
				return string.Format("({0} / {1})", multiLeft, (int)goal.arg1);
			}*/
			case GoalType.GetCoins:
			{
				return "";
				/*
				int coinsLeft = (int)(GoalManager.SharedInstance.coinsThisGame[0]);
				return string.Format("({0}/{1})", coinsLeft, (int)goal.arg1);*/
			}						
			case GoalType.GetTotalCoins:
			{
				int coinsLeft = (int)(GameState.SharedInstance.TotalCash - goal.arg2);
				return string.Format("({0}/{1})", coinsLeft, (int)goal.arg1);
			}							
			case GoalType.GetCoinsVehicle:
			{
				int curCoins = GameState.SharedInstance.GetTotalVehicleCash((Vehicle)goal.arg2);
				int coinsLeft = curCoins - (int)goal.arg3;
				return string.Format("({0}/{1})", coinsLeft, (int)goal.arg1);
			}				
			case GoalType.VisitLocation:
			{
				return "";
				/*
				int visitsLeft = GoalManager.SharedInstance.locationVisits[(int)goal.arg1];
				return string.Format("({0} / {1})", visitsLeft, (int)goal.arg2); */
			}				
			case GoalType.VisitTotalLocation: 
			{
				int visitsLeft = GameState.SharedInstance.GetLocationVisits((Location)((int)goal.arg1)) - (int)goal.arg3;
				return string.Format("({0}/{1})", visitsLeft, (int)goal.arg2);
			}	
			case GoalType.GetVehicle: 
			{
				return "";
				/*			
				int vehiclesLeft = GoalManager.SharedInstance.vehiclesCollected[(int)goal.arg1];
				return string.Format("({0}/{1})", vehiclesLeft, (int)goal.arg2);*/
			}						
			case GoalType.GetTotalVehicle:
			{
				int vehiclesLeft = (GameState.SharedInstance.GetTotalVehicleCollect((Vehicle)((int)goal.arg1)) - (int)goal.arg3);
				return string.Format("({0}/{1})", vehiclesLeft, (int)goal.arg2);
			}				
			case GoalType.Grind:
			{
				return "";
				/*int grindsLeft = GoalManager.SharedInstance.grindsHit;
				return string.Format("({0}/{1})", grindsLeft, (int)goal.arg1); */
			}					
			case GoalType.GrindTotal:
			{
				int grindsLeft = (GameState.SharedInstance.GetGrindHitTotal() - (int)goal.arg3);
				return string.Format("({0}/{1})", grindsLeft, (int)goal.arg1);
			}				
			case GoalType.SlalomsHit: 
			{
				return "";
				/*
				int slalomsLeft = (int)(GoalManager.SharedInstance.slalomsHitThisGame);
				return string.Format("({0}/{1})", slalomsLeft, (int)goal.arg1); */
			}							
			case GoalType.SlalomHitOverall: 
			{
				int slalomsLeft = (int)( GameState.SharedInstance.GetSlalomsHit() - (int)goal.arg2 );
				return string.Format("({0}/{1})", slalomsLeft, (int)goal.arg1);
			}
			case GoalType.CollectSURFLetters:
			{
				int curSURF = (int)GameState.SharedInstance.GetSURFCollect();  
				return string.Format("({0}/{1})", curSURF - goal.arg2, (int)goal.arg1);
			}
			default:
				return "";
		};
	}

	public static string GetGoalDescription(Goal goal)
	{
		switch (goal.goalType)
		{
			case GoalType.PlayNumGames:
				if (goal.arg1 > 1)
					return string.Format(GameLanguage.LocalizedText("PLAY {0} GAMES"), (int)goal.arg1);
				else
					return string.Format(GameLanguage.LocalizedText("PLAY A GAME"), (int)goal.arg1);

			case GoalType.ReachDistance:
				return string.Format(GameLanguage.LocalizedText("REACH {0}M IN A GAME"), (int)goal.arg1);

			case GoalType.ReachDistanceVehicle:
				return string.Format(GameLanguage.LocalizedText("SURF {0}M ON THE {1} IN A GAME"), (int)goal.arg1, VehicleHelper.GetName((Vehicle)((int)goal.arg2)));
				
			case GoalType.ReachTotalDistance:
				return string.Format(GameLanguage.LocalizedText("SURF {0}M"), (int)goal.arg1);
			
			case GoalType.ReachTotalDistanceVehicle:
				return string.Format(GameLanguage.LocalizedText("SURF {0}M WITH THE {1}"), (int)goal.arg1, VehicleHelper.GetName((Vehicle)((int)goal.arg2)));
			
			/*case GoalType.ReachDistanceNoCoins:
				return string.Format(GameLanguage.LocalizedText("AVOID COINS FOR THE FIRST {0}M"), (int)goal.arg1);
			*/
			case GoalType.ReachScore:
				return string.Format(GameLanguage.LocalizedText("REACH {0} SCORE IN A GAME"), (int)goal.arg1);


			/*case GoalType.ReachTotalScore:
				return string.Format(GameLanguage.LocalizedText(" SCORE OF {0}"), (int)goal.arg1);
			*/				
			/*case GoalType.ReachMultiplier:
				return string.Format(GameLanguage.LocalizedText("REACH x{0} SCORE MULTIPLIER"), (int)goal.arg1);
			*/
			case GoalType.GetCoins:
				return string.Format(GameLanguage.LocalizedText("COLLECT {0} COINS IN A GAME"), (int)goal.arg1);

			case GoalType.GetTotalCoins:
				return string.Format(GameLanguage.LocalizedText("COLLECT {0} COINS"), (int)goal.arg1);
								
			case GoalType.GetCoinsVehicle:
				return string.Format(GameLanguage.LocalizedText("COLLECT {0} COINS WITH THE {1}"), (int)goal.arg1, VehicleHelper.GetName((Vehicle)((int)goal.arg2)));
							
			case GoalType.VisitLocation:
				if (goal.arg2 > 1)
					return string.Format(GameLanguage.LocalizedText("VISIT {0} {1} TIMES IN A GAME"), LocationTitle.Get((Location)((int)goal.arg1)), (int)goal.arg2);
				else
					return string.Format(GameLanguage.LocalizedText("VISIT {0}"), LocationTitle.Get((Location)((int)goal.arg1)));
					
			case GoalType.VisitTotalLocation: 
				if (goal.arg2 > 1)
				return string.Format(GameLanguage.LocalizedText("VISIT {0} {1} TIMES"), LocationTitle.Get((Location)((int)goal.arg1)) , (int)goal.arg2);
				else
					return string.Format(GameLanguage.LocalizedText("VISIT {0}"), LocationTitle.Get((Location)((int)goal.arg1)));
										
			case GoalType.GetVehicle: 
				if (goal.arg2 > 1)
					return string.Format(GameLanguage.LocalizedText("PICK UP THE {0} {1} TIMES IN A GAME"), VehicleHelper.GetName((Vehicle)((int)goal.arg1)), (int)goal.arg2);
				else
					return string.Format(GameLanguage.LocalizedText("PICK UP THE {0}"), VehicleHelper.GetName((Vehicle)((int)goal.arg1)));
									
			case GoalType.GetTotalVehicle:
				if (goal.arg2 > 1)									
					return string.Format(GameLanguage.LocalizedText("PICK UP THE {0} {1} TIMES"), VehicleHelper.GetName((Vehicle)((int)goal.arg1)), (int)goal.arg2);
				else
					return string.Format(GameLanguage.LocalizedText("PICK UP THE {0}"), VehicleHelper.GetName((Vehicle)((int)goal.arg1)));
											
			case GoalType.Grind:
				if (goal.arg1 > 1)									
				{
					return string.Format(GameLanguage.LocalizedText("GRIND {0} TIMES IN A GAME"), (int)goal.arg1); 
				}
				else
				{
					// QTODO: Needs new translation
					return GameLanguage.LocalizedText("GRIND ONCE");
				}
												
			case GoalType.GrindTotal:
				if (goal.arg1 > 1)								
				{
					return string.Format(GameLanguage.LocalizedText("GRIND {0} TIMES"), (int)goal.arg1); 
				}
				else
					return GameLanguage.LocalizedText("GRIND ONCE"); // QTODO: Needs new translation
							
			case GoalType.SlalomsHit: 
				if (goal.arg1 > 1)
					return string.Format(GameLanguage.LocalizedText("PASS THROUGH {0} GATES IN A GAME"), (int)goal.arg1);
				else
					return string.Format(GameLanguage.LocalizedText("PASS THROUGH A GATE"), (int)goal.arg1);

			case GoalType.SlalomHitOverall: 
				if (goal.arg1 > 1)
					return string.Format(GameLanguage.LocalizedText("PASS THROUGH {0} GATES"), (int)goal.arg1);
				else
					return string.Format(GameLanguage.LocalizedText("PASS THROUGH A GATE"), (int)goal.arg1);
			
			case GoalType.CollectSURFLetters:

				if (goal.arg1 > 1)
					return string.Format(GameLanguage.LocalizedText("COLLECT S.U.R.F {0} TIMES"), (int)goal.arg1);
				else
					return string.Format(GameLanguage.LocalizedText("COLLECT S.U.R.F"));
				

			default:
				return "";
		};
	}
				
}