﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ScoreBar : MonoBehaviour {

	public TextMesh mainText;
	Vector3 mainTextLocalScale = Vector3.zero;

	public TextMeshPro multiplierText;

	public string mutliplierString = "<size=-26>x</size>{0}";
	public GameObject subMeshPrefab = null;

	private GameObject[] subMeshes;
	private TextMeshPro[] subMeshComponent;
	private float[] subMeshShowTimes;
	private int curSubMeshIndex = 0;
	private float timeToMove = 0.65f;
	//public Color popupMeshColor = Color.white;

    private Vector3 startOriginPosition;

	private Vector3 originPosition;
	private Vector3 moveToPosition;

	private const int numSubMeshes = 4;

	private bool isInitialised = false;

	public float curScore = 0.0f;
	public float curMultiplier = 0.0f;

	private bool localSpaceAnimation = false;

	private bool scorePowerUpOn = false;
	private float timeSincePowerUpOn = 0.0f;
	private Vector3 multiplierOrigScale;
	private float scaleFactor = 0.05f;
	private float scaleSpeed = 1.5f;

	public void Initialise()
	{
		isInitialised = true;

		mainTextLocalScale = mainText.transform.localScale;
		subMeshes = new GameObject[numSubMeshes];
		subMeshComponent = new TextMeshPro[numSubMeshes];
		subMeshShowTimes = new float[numSubMeshes];
		for (int i = 0; i < numSubMeshes; i++)
		{
			subMeshes[i]		= GameObject.Instantiate(subMeshPrefab);
			subMeshComponent[i] = subMeshes[i].GetComponent<TextMeshPro>();
			subMeshComponent[i].transform.SetParent(mainText.transform.parent);
			subMeshComponent[i].transform.localScale = mainText.transform.localScale * 2.5f;

			subMeshShowTimes[i] = -1.0f;
			subMeshes[i].SetActive(false);
		}

        startOriginPosition = mainText.transform.position;

		originPosition = mainText.transform.localPosition;
		moveToPosition = originPosition + Vector3.down * 1.5f;

		multiplierOrigScale = multiplierText.transform.localScale;

		multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier));
		GoalManager.SharedInstance.UpdateMultiplier(GameState.SharedInstance.Multiplier + (int)curMultiplier);
		GoalManager.SharedInstance.UpdateGoals();

	}

    public void AddSpecialValue(float addedValue)
    {
        AddSpecialValue(addedValue, new Vector3(0, 0, 0));
    }

    public float AddSpecialValue(float addedValue, Vector3 startPos)
	{
		if (!isInitialised)
			Initialise();

        if (startPos.y != 0)
        {
            this.originPosition = new Vector3(startPos.x, startPos.y, startOriginPosition.z);
            this.moveToPosition = new Vector3(startPos.x, startPos.y + 1f, startOriginPosition.z);
            ////////Debug.Log("SETTING COIN ANIMATION POINTS HERE:" + originPosition + " , " + moveToPosition);
            localSpaceAnimation = false;
        }
        else
        {
            this.originPosition = mainText.transform.localPosition;
            this.moveToPosition = new Vector3(this.originPosition.x, this.originPosition.y + 2f, startOriginPosition.z);
            ////////Debug.Log("SETTING DEFAULT COIN PATH HERE:" + originPosition + " , " + moveToPosition);
            localSpaceAnimation = true;
        }

		if (scorePowerUpOn)
			subMeshComponent[curSubMeshIndex].text = string.Format("+ {0}", Mathf.RoundToInt(addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier)));
		else
			subMeshComponent[curSubMeshIndex].text = string.Format("+ {0}", Mathf.RoundToInt(addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2));

		subMeshShowTimes[curSubMeshIndex] = timeToMove;
		subMeshComponent[curSubMeshIndex].gameObject.SetActive(true);

		curSubMeshIndex = (curSubMeshIndex + 1) % numSubMeshes;

		return AddValue(addedValue);
	}

	public float AddValue(float addedValue)
	{
		if (!isInitialised)
			Initialise();

		addedValue = Mathf.Max(0.0f, addedValue);

		if (scorePowerUpOn)
			curScore += addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2.0f;
		else
			curScore += addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier);

		SetMainText(ShortenScore(curScore));

		if (scorePowerUpOn)
			return addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2.0f;
		else
			return addedValue * (GameState.SharedInstance.Multiplier + (int)curMultiplier);
	}

	float lastResizeTextScale = 1.0f;
	void SetMainText(string value)
	{
		float scale = GlobalSettings.ScaleTextAfterLength(value, 8);
		if(lastResizeTextScale != scale)
		{
			mainText.transform.localScale = new Vector3(mainTextLocalScale.x * scale, mainTextLocalScale.y * scale, mainTextLocalScale.z);
			lastResizeTextScale = scale;
		}

		mainText.text = value;
	}

	public static string ShortenScore(float score)
	{
		score = Mathf.Max(0, Mathf.RoundToInt(score));
		return string.Format("{0:#,0}", (int)score);
	}

	public void Reset()
	{
		curMultiplier = 0.0f;
		curScore = 0.0f;
		multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier));
		SetMainText("0");
	}

    public float GetScore()
    {
        return curScore;
    }

	public void AddMultiplier(float addedMultiplier)
	{
		if (!isInitialised)
			Initialise();

		curMultiplier += addedMultiplier;

		if (scorePowerUpOn)
		{
			multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2);
			GoalManager.SharedInstance.UpdateMultiplier((GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2);

		}
		else
		{
			multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier));
			GoalManager.SharedInstance.UpdateMultiplier(GameState.SharedInstance.Multiplier + (int)curMultiplier);
		}
	}

	public void ActivatePowerUp()
	{
		scorePowerUpOn = true;
		multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2);
		timeSincePowerUpOn = 0;
		GoalManager.SharedInstance.UpdateMultiplier((GameState.SharedInstance.Multiplier + (int)curMultiplier) * 2);
	}

	public void DeactivatePowerUp()
	{
		scorePowerUpOn = false;
		multiplierText.SetText(mutliplierString, (GameState.SharedInstance.Multiplier + (int)curMultiplier));
		GoalManager.SharedInstance.UpdateMultiplier((GameState.SharedInstance.Multiplier + (int)curMultiplier));

	}

	void Update () 
	{
		if (!isInitialised)
			Initialise();

		if (scorePowerUpOn)
		{
			multiplierText.transform.localScale = multiplierOrigScale + Vector3.one * (Mathf.Sin(timeSincePowerUpOn * scaleSpeed) + 1) * 0.5f * scaleFactor;
			timeSincePowerUpOn += Time.deltaTime;
		}
		else
			multiplierText.transform.localScale = multiplierOrigScale;
		
		for (int i = 0; i < numSubMeshes; i++)
		{
			if (subMeshShowTimes[i] >= 0)
			{
				float lerpValue = subMeshShowTimes[i] / timeToMove;

				subMeshShowTimes[i] -= Time.deltaTime;
				if (subMeshShowTimes[i] <= 0)
				{
					lerpValue = 0;
					subMeshShowTimes[i] = -1;
					subMeshes[i].SetActive(false);
				}
				subMeshComponent[i].transform.localPosition = Vector3.Lerp(originPosition, moveToPosition, 1.0f - Mathf.Pow(lerpValue, 3.0f));

				float colorLerp = 1.0f - (  (lerpValue < 0.5) ? lerpValue * 2.0f : 1.0f - (lerpValue - 0.5f) * 2.0f);
                if (localSpaceAnimation)
                {
                    subMeshComponent[i].transform.localPosition = Vector3.Lerp(originPosition, moveToPosition, 1.0f - lerpValue);
                }
                else
                {
                    subMeshComponent[i].transform.position = Vector3.Lerp(originPosition, moveToPosition, 1.0f - lerpValue);
                }

				subMeshComponent[i].alpha = Mathf.Lerp(1.0f, 0.0f, Mathf.Pow(colorLerp, 3.0f));

			}
		}
	}
}
