﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class GlobalSettings : MonoBehaviour {


    public static float reggaeMusicPlayPosition = 0;
    public static float inGameMusicPlayPosition = 0;
    public static float miniGameMusicPlayPosition = 0;
    public static int currentMenuMusicIndex = 0;

	public static void DebugResetPrefs()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
		tutCompleted = null;
		surfedPassMinTotalDist = null;
	}

	static bool? tutCompleted = null;
	public static bool TutorialCompleted()
	{
		if(!tutCompleted.HasValue)
			tutCompleted = PlayerPrefs.GetInt("GStutCompl", 0) == 1;

		return tutCompleted.Value;
	}

	public static bool IsShowingStarterPackDialog()
	{
		if(GameState.SharedInstance.TutorialStage == TutStageType.Finished &&
		 !GameState.SharedInstance.StarterPackRulesMet)
		{
			return true;
		}

		return false;
	}

	static bool JeepGamePlayed()
	{
		if(GameState.SharedInstance.JeepGamesPlayed > 0)
		{
			return true;
		}
		else 
			return false;
	}

	static bool ThrusterBoardPurchased()
	{
		UpgradeItem thruster = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.SurfBoardThruster);

		if(thruster.UpgradeCompleted())
		{
			return true;
		}
		else
			return false;            
	}

	public static void FlagTutorialAsCompleted()
	{
		tutCompleted = true;
		PlayerPrefs.SetInt("GStutCompl", 1);
	}

	public static void FlagTutorialAsUncompleted()
	{
		tutCompleted = false;
		PlayerPrefs.SetInt("GStutCompl", 0);
	}

	static bool? surfedPassMinTotalDist = null;
	public static bool SurfedPassMinTutorialDistance()
	{
		if(!surfedPassMinTotalDist.HasValue)
			surfedPassMinTotalDist = PlayerPrefs.HasKey("GSSurpmtd");

		return surfedPassMinTotalDist.Value;
	}

	public static void FlagSurfedPassMinTutorialDistanceCompleted()
	{
		surfedPassMinTotalDist = true;
		PlayerPrefs.SetInt("GSSurpmtd", 1);
	}

    public static bool firstTimePlayVideo
    {
        get { return PlayerPrefs.GetInt("gss_playvid", 1) == 1; }
        set { PlayerPrefs.SetInt("gss_playvid", value ? 1 : 0); }
    }

    public static bool firstTimeJeepGame
    {
        get { return PlayerPrefs.GetInt("gss_jeeptut", 1) == 1; }
        set { PlayerPrefs.SetInt("gss_jeeptut", value ? 1 : 0); }
    }

	public static bool soundEnabled
	{
		get { return PlayerPrefs.GetInt("gss_sound", 1) == 1; }
		set { PlayerPrefs.SetInt("gss_sound", value ? 1 : 0); }
	}

	public static bool musicEnabled
	{
		get { return PlayerPrefs.GetInt("gss_music", 1) == 1; }
		set { PlayerPrefs.SetInt("gss_music", value ? 1 : 0); }
	}

	public static bool alertNotificationEnabled
	{
		get { return PlayerPrefs.GetInt("gss_alertNotif", 1) == 1; }
		set { PlayerPrefs.SetInt("gss_alertNotif", value ? 1 : 0); }
	}

	public static Vector3 playerPosition;
	public static int currentLocationNumber = 3;
	public static LevelDifficulty currentDifficulty = LevelDifficulty.None;
	public static string currentGoalText = "";
	public static bool surfGamePaused = false;
    public static bool cameraMovementIngamePaused = false;
    //public static bool whiteWashIdleSlowDown = false;
    public static int menuSelectedLocation = 0;

	//public static MainMenuButtons.MenuButtonOption mainMenuOption = MainMenuButtons.MenuButtonOption.None;

	public static Location ForceDebugLocation = Location.Undefined;

	public enum LevelDifficulty
	{
		None,
		Easy,
		Medium,
		Hard,
		BigWaveEasy,
		BigWaveMedium,
		BigWaveHard,
	};

	public enum MainMenuLocation
	{
		MainMenu,
		Treasure,
		CratePrize,
	}

	public static MainMenuLocation CurrentMainMenuLocation = MainMenuLocation.MainMenu;
	public static bool BackToMainMenuFromRewardedVideo = false;


	public static void EnableHiddenItems(Transform parentTransform)
	{  
		Transform[] childComponents = parentTransform.GetComponentsInChildren<Transform>();
		foreach(Transform child in childComponents)
		{
			if(
				child.gameObject.tag.Equals("Coin") || 
				child.gameObject.tag.Equals("Crate")
			)
			{
				if(!child.gameObject.GetComponent<Renderer>().enabled)
				{
					child.gameObject.GetComponent<Renderer>().enabled = true; 
					if(child.gameObject.GetComponent<Collider>() != null)
					{
						child.gameObject.GetComponent<Collider>().enabled = true;  
					}
				}    
			}
			else if(child.gameObject.tag.Equals("Chopper"))
			{
				Chopper theChopper = child.gameObject.transform.GetComponent<Chopper>();
				theChopper.ShowClaw();
				theChopper.ResetChopper();
			}
			else if(child.gameObject.tag.Equals("Bird"))
			{
				Bird theBird = child.gameObject.transform.GetComponent<Bird>();
				theBird.ResetBird();
			}

		}
	}

    public static void PlaySoundFromTimePoint(AudioSource soundSource, float soundDelay)
    {
        if(GlobalSettings.soundEnabled)
        {
            soundSource.time = soundDelay;
            soundSource.Play();
        }
    }

	public static void PlayMusicFromTimePoint(AudioSource soundSource, float soundDelay)
    {
        if(GlobalSettings.musicEnabled)
        {
            soundSource.time = soundDelay;
            soundSource.Play();
        }
    }

	public static void PlaySound(AudioSource soundSource)
	{
		if(GlobalSettings.soundEnabled)
		{
			//if(!soundSource.GetComponent<AudioSource>().isPlaying)
			soundSource.Play ();
		}
	}

	public static void PlaySoundDelayed(AudioSource soundSource, float delaySecs)
	{
		if(GlobalSettings.soundEnabled)
		{
			soundSource.PlayDelayed(delaySecs);
		}
	}

	public static float ScaleTextAfterLength(string text, int cutOffLength)
	{
		if(string.IsNullOrEmpty(text) || text.Length <= cutOffLength)
			return 1.0f;
		else
		{
			int lengthOver = text.Length - cutOffLength;
			float percentScale = (100 - (lengthOver * 10))/100f;
			if(percentScale <= 0.4f)
				percentScale = 0.4f;

			return percentScale;
		}
	}

}
