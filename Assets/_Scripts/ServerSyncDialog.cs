﻿using UnityEngine;
using System.Collections;

using TMPro;

public class ServerSyncDialog : MonoBehaviour {

	public Transform confirmButton;
	public Camera mainCamera;

	public bool isMoving = false;
	public bool isShowing = false;

	private float timeToMove = 0.5f;
	private float curTimeToMove = 0.0f;

	private Vector3 activePosition;
	private Vector3 inactivePosition;

	void Start()
	{
		if (mainCamera == null)
			mainCamera = Camera.main;

		activePosition = new Vector3(0,0, transform.localPosition.z);
		inactivePosition = activePosition + Vector3.left * 2000f;
		transform.localPosition = inactivePosition;
	}

	public void ShowPanel()
	{
		isMoving = true;
		isShowing = true;
		curTimeToMove = timeToMove;
	}

	public void HidePanel()
	{
		isMoving = true;
		isShowing = false;
		curTimeToMove = timeToMove;
	}

	void Update () 
	{
		if (isMoving)
		{
			if (curTimeToMove < 0)
			{
				curTimeToMove = 0;
				isMoving = false;
			}

			if (isShowing)
			{
				float lerpValue = 1.0f - Mathf.Pow(curTimeToMove / timeToMove, 3.0f);
				transform.localPosition = Vector3.Lerp(inactivePosition, activePosition, lerpValue);
			}
			else
			{
				float lerpValue = 1.0f - Mathf.Pow(1.0f - curTimeToMove / timeToMove, 3.0f);
				transform.localPosition = Vector3.Lerp(inactivePosition, activePosition, lerpValue);
			}
			curTimeToMove -= Time.deltaTime;
		}

		if (isShowing)
		{
			if (Input.GetMouseButton(0))
			{
				if (UIHelpers.CheckButtonHit(confirmButton, mainCamera))
				{
					TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();
					HidePanel();
				}
			}
		}
	}
}
