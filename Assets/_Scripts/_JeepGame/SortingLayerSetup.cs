﻿using UnityEngine;
using System.Collections;

public class SortingLayerSetup : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Renderer panelRenderer = GetComponent<Renderer>();
        panelRenderer.sortingLayerName = "Default";
        panelRenderer.sortingOrder = 8;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
