﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Timer manager, used to manage and return increasing timer countdown checks. Will use last value of comma separated countdown once reached.
/// 
/// Example of countdown configuration string;
/// 
/// string TreasureChestTimerSecs = "1,0|0,600,10800,1800,43200";
/// Format: [CurrentVersion] , [StartTimerAtIndexForExistingUsers] | [SecondsTillTimerActivates], [SecondsTillTimerActivates1], [SecondsTillTimerActivates2]...
/// </summary>

/// </summary>
public class TimerManager
{
	static TimerManager instance;
	public static TimerManager SharedInstance
	{
		get
		{
			if(instance == null)
				instance = new TimerManager();

			return instance;
		}
	}

	public enum TimerID
	{
		Default = 0,
		Treasure = 100,
		Halfpipe = 200,
		Jeep = 300,
		GameConsole = 400,
        //StarterPackDialog = 400,
	}

	Dictionary<TimerID, TimerInfo> timers;

	protected TimerManager()
	{
		timers = new Dictionary<TimerID, TimerInfo>
		{
			{ TimerID.Default, new TimerInfo((int)TimerID.Default, GameServerSettings.SharedInstance.SurferSettings.DefaultTimerManagerSecs) },
			{ TimerID.Treasure, new TimerInfo((int)TimerID.Treasure, GameServerSettings.SharedInstance.SurferSettings.TreasureChestTimerSecs) },
			{ TimerID.Jeep, new TimerInfo((int)TimerID.Jeep, GameServerSettings.SharedInstance.SurferSettings.JeepTimerSecs) },
			{ TimerID.Halfpipe, new TimerInfo((int)TimerID.Halfpipe, GameServerSettings.SharedInstance.SurferSettings.HalfPipeTimerSecs) },
			{ TimerID.GameConsole, new TimerInfo((int)TimerID.GameConsole, GameServerSettings.SharedInstance.SurferSettings.GameConsoleTimerSecs )}
            //{ TimerID.StarterPackDialog, new TimerInfo((int)TimerID.StarterPackDialog, GameServerSettings.SharedInstance.SurferSettings.StarterPackDialogTimerSecs) }
		};
	}

	public int GetTimeInSeconds(TimerID timerID)
	{
		if(timers.ContainsKey(timerID))
		{
			return timers[timerID].GetCurrentTimeInSeconds();
		}
		else
		{
			return GameServerSettings.SharedInstance.SurferSettings.DefaultTimerManagerSecsInterval;
		}
	}

	public int GetTimeInSecondsAndIncrementToNextInterval(TimerID timerID)
	{
		if(timers.ContainsKey(timerID))
		{
			return timers[timerID].GetCurrentTimeInSecondsAndMoveToNextInterval();
		}
		else
		{
			return GameServerSettings.SharedInstance.SurferSettings.DefaultTimerManagerSecsInterval;
		}
	}
}


public class TimerInfo
{
	string TimerPrefKey { get { return string.Format("TInftpk{0}", (int)TimerInfoID); } }

	string TimerCurrentIndexPrefKey { get { return string.Format("TInftcik{0}", (int)TimerInfoID); } }

	string TimerCurrentSecondsPrefKey { get { return string.Format("TInfcss{0}", (int)TimerInfoID); } }

	int TimerInfoID { get; set; }


	public TimerInfo(int timerID, string timerSettings)
	{
		TimerInfoID = timerID;

		bool checkForNewVersions = true;
		string currentTimerString = PlayerPrefs.GetString(TimerPrefKey, "");

		if(string.IsNullOrEmpty(currentTimerString))
		{
			currentTimerString = timerSettings;
			PlayerPrefs.SetString(TimerPrefKey, timerSettings);
			checkForNewVersions = false;
		}

		string[] currentTimerSettingsString = currentTimerString.Trim().Split(new char[] { '|' });

		// Checks for updated server settings
		if(checkForNewVersions)
		{
			string[] newTimerSettingsString = timerSettings.Trim().Split(new char[] { '|' });

			if(currentTimerSettingsString.Length >= 2 && newTimerSettingsString.Length >= 2)
			{
				string currentVersionString = currentTimerSettingsString[0];
				string newVersionString = newTimerSettingsString[0];
				int currentVersion;
				int newVersion;
				if(Int32.TryParse(currentVersionString, out currentVersion) && Int32.TryParse(newVersionString, out newVersion))
				{
					if(newVersion > currentVersion)
					{
						PlayerPrefs.SetString(TimerPrefKey, timerSettings);
						currentTimerString = timerSettings;

						int newIndexToSet;
						if(Int32.TryParse(newTimerSettingsString[1], out newIndexToSet))
						{
							if(PlayerPrefs.HasKey(TimerCurrentIndexPrefKey))
								CurrentValueSecsIndex = newIndexToSet;
							else
								CurrentValueSecsIndex = 0;
						}
					}
				}
			}
		}

		string[] currentTimersValues = currentTimerString.Trim().Split(new char[] { '|' });
		if(currentTimersValues.Length >= 2)
		{
			PlayerPrefs.SetString(TimerCurrentSecondsPrefKey, currentTimersValues[1]);
		}
	}

	public int GetCurrentTimeInSecondsAndMoveToNextInterval()
	{
		return GetCurrentTimeInSeconds(true);
	}

	public int GetCurrentTimeInSeconds(bool moveToNextIntervalSeconds = false)
	{
		int result = GameServerSettings.SharedInstance.SurferSettings.DefaultTimerManagerSecsInterval; // If none found use default.
		string timerSecs = PlayerPrefs.GetString(TimerCurrentSecondsPrefKey, "");

		if(string.IsNullOrEmpty(timerSecs) || CurrentValueSecsIndex < 0)
			return result; 
		else
		{
			string[] secondsValue = timerSecs.Split(new char[] {','});

			int secs;
			if(CurrentValueSecsIndex < secondsValue.Length && Int32.TryParse(secondsValue[CurrentValueSecsIndex], out secs))
			{
				result = secs;
			}
			else
			{
				int index = secondsValue.Length-1;
				if(index >= 0 &&  Int32.TryParse(secondsValue[index], out secs))
					result = secs;
			}
		}

		if(moveToNextIntervalSeconds)
		{
			CurrentValueSecsIndex = CurrentValueSecsIndex + 1;
		}

		return result;
	}

	int CurrentValueSecsIndex
	{
		get { return PlayerPrefs.GetInt(TimerCurrentIndexPrefKey, 0); }
		set 
		{
			string currentTimerString = PlayerPrefs.GetString(TimerPrefKey, "");

			if(!string.IsNullOrEmpty(currentTimerString))
			{
				string[] currentTimerSettingsString = currentTimerString.Trim().Split(new char[] { '|' });
				if(currentTimerSettingsString.Length >= 2)
				{
					string[] secondsValue = currentTimerSettingsString[1].Trim().Split(new char[] { ',' });

					int indexToSet = value;
					if(indexToSet >= secondsValue.Length)
					{
						indexToSet = secondsValue.Length - 1;
					}
						
					PlayerPrefs.SetInt(TimerCurrentIndexPrefKey, indexToSet);
				}
			}
		}
	}
}
