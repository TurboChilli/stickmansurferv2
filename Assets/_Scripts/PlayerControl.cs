using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using Prime31;

public class PlayerControl : MonoBehaviour 
{
	public Camera uiCam = null;
	public Parallax foregroundParallax = null;
	public Parallax midgroundParallax = null;
	public Parallax midgroundRocksParallax = null;
	public TextMesh ScoreBarTextMesh = null;
	public GameObject midgroundRocks = null;
	private Texture midgroundRocksTexture = null;
	public Texture midgroundRocksEmptyTexture = null;
	public AnimatingObject tutorialThumb = null;
	public TMP_Text tutorialThumbText = null;
	bool showTutorialThumb;

	public GametimeTool gameTimeTool;

	[Space(20)]
	[Header("Other Properties")]
	public float MaxPositionY;
	public float MinPositionY;	

    public GameObject readyOverlayPanel;
    public GameObject goText;
	public float UpwardsSpeed;
	public float TransitionSpeed;
	public float RotatingSpeed;

	public float MaximumUpAngle;
	public float MaximumDownAngle;
	public float DefaultAngle;
	public float TurnAngle;
	public float CrouchAngle;

    public Letter[] lettersToCollect;
    // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
    public ParticleSystem waveBlowoutParticles;
	public ParticleSystem crateExplodeParticles;
	public ParticleSystem bodyPartsParticles;

	public Transform debugText;
	public Transform goalText;
	public Transform highScoreText;

	public GameUI gameUI = null;

	//==============SURFER TEXTURES============\\
	public Texture2D Standing;
	public Texture2D Crouching;

	public int Coins;
	public float distance;
	private float distanceMilestone = 500.0f;
	private int distanceMilestonesReached = 0;

	public int currentMovement;
	public Vector2 inputPosition;

	private Transform playerTransform;
	public GameObject playerTransformReference;

	private Rect MoveUp;
	private Rect MoveDown;
	private bool isCrouched;
	
	public Transform waveParent;

	public Camera mainCamera;
	private bool hasIndoorSwitchSpawned = false;
	private bool postIndoorCameraSwitch = false;
	private GameObject indoorSwitchObject = null;
	public InGameTextureManager inGameTextureManager;
	private float halfScreenWorldUnit;
	GameCenterPluginManager gameCenterPluginManager = null;

	//public Transform midgroundLayer;
	//public Transform foregroundLayer;

	public Animation waveCrestAnimation;

	Vector3 moveVector = new Vector3(0,0,0);
	float waveVelocityX = 0f;
	float specialWaveCooldown = 2.0f;
	float curSpecialWaveCooldown = 0.0f;

	float startXVelocity = 1f;
	float cameraXOffset = 3f;
	float carveRotationSpeedDown = -55f;
	float carveRotationSpeedUp = 75f;
	float carveDownXVelocityIncrease = 12f;
	float carveUpXVelocityDecrease = -7f;

	float waveYIncreaseVelocity = 0.3f;
	float waveYIncreaseVelocityTakeOffSurge = 2f;

	float waveYDecreaseVelocity = 0.3f;
	bool carvingDown = false;
	float carveDragXFactor = 0;
	float currentCarveRotationSpeed = 0;
    float airRotationSpeed = -100f;//-100f;

	float minDownRotationZ = 325;
	float minUpRotationZ = 45;

	float startMaxXVelocity = 10.0f;
	float startMinXVelocity = 9.0f;

	float minXVelocity = 9f;
	float maxXVelocity = 10f;//13f;

	float waveVelocityMaxX = 23f;
	float waveVelocityMaxXBarrelSurge = 29f;//29f;
	float faceSlowdownDistance = 13;
	float maxBlowoutXVelocity = 30f;
	float headBreakBarrelXDrag = 20f;// 130f;
	
	float waveVelocityStartX = 0;
	float gravityForce = 20f;
	float cruisingZRotation = 240f;
	float cameraStartYPos = 0;
	//public Transform rockGroup;

    float wipeoutXPos = 0;
    float safeResetTime = 2.5f;
    bool goalsComplete = false;
	public Transform vehicleBanner;
    public TextMeshPro powerUpBannerText;

	public TrailRenderer surferWaveTrail;
    //TrailRenderer currentSurferWaveTrail;
    public TrailRenderer motorbikeWaveTrail;
	bool wavePeaked = false;

	public ParticleSystem barrelBreakParticleEmitter;

	public AudioSource gameMusic;

	public AudioSource soundCoinHit;
    public AudioSource soundInvincibleWarning;
	public AudioSource soundSuperCoinHit;
	public AudioSource soundWave;
    public AudioSource soundGoalsComplete;
	public AudioSource soundWaveBarrel;
	public AudioSource soundCarve;
	public AudioSource soundDeath;
	public AudioSource soundJetski;
	public AudioSource soundBarrelBlowout;
	public AudioSource soundCrate;
    public AudioSource soundLetter;
	public AudioSource soundPowerUp;
	public AudioSource soundBannerSlide;
    public AudioSource soundGateMissed;
	public AudioSource soundPlane;
	public AudioSource soundBoing;
    
	public AudioSource soundExplode;
	public AudioSource soundGrind;

	float headBreakBarrelYPos = 0;
	float headBreakBarrelXWavOffset = 0;
	int prevHighScore = 0;

	public UnderPlayerCollider underPlayerCollider;

	public LevelGenerator levelGenerator;
	GameMenuButtons gameMenuButtons;
	public GameObject loadingScreen;

	// Use this for initialization
	private Location currentLocation;
	private float underCollisionOffsetX = 0;
	private float underCollisionOffsetY = 0;
	public PlayerAnimation playerAnimation;
	private Transform wipeoutBoard;
	private Vector3 origBoardPosition;
	private Vector3 origBoardScale;
	private Quaternion origBoardRotation;

	//int nextFadeLocationNumber = 1;
	private Vector3 bannerStartLocalPos;

	public bool waveEnding = false;

	public PowerupManager powerUpManager;
	public GoalUIManager goalUIManager;

    private bool isGrindingSpline = false;
    public SplineGrind splineGrind = null;
    public bool isAboveSpline = false;

    private bool isPlane = false;
    private Vector3 planeMovementVector;
	private float planeAscendAccel = 20.0f;
    private float planeAscendBoost = 80.0f;
    private float planeDecendAccel = 20.0f;
	private float maxPlaneAscend = 5.0f;
    private float maxPlaneDecend = -8.0f;

    private float maxPlaneAngle = 10.0f;
	private float minPlaneAngle = -40.0f;

	private float minPlaneSoundPitch = 1.4f;
	private float maxPlaneSoundPitch = 2.0f;

	private float maxPlaneHeight = 12.0f;

    private float planeStartTime = 0.4f;
	private float curPlaneStartTime = 0.0f;

	private float maxTimeBetweenCoinHits = 0.3f;
	private float timeSinceCoinHit = 0.0f;
	private int coinHitsSoFar = 0;
	private int maxCoinHits = 9;
	private float origSoundCoinHitPitch;
	public ParticleSystem superCoinEffect;

	public GameObject smashEffect = null;
	public GameObject rockSmashEffectPrefab = null;
	private GameObject[] rockSmashEffects = null;
	private int maxRockSmashEffects = 4;
	private int curRockSmashIndex = 0;
	private float smashTime = 1.0f;
	private float curSmashTime = 0.0f;

	private TutorialManager tutManager = null;

    public GameObject foregroundRocks = null;
    public GameObject[] gateMissedIcons;

    private float curTimeMovementLocked = 0.0f;
    private float timeMovementLocked = 1.0f;

    private Vector3 startPosition;
	private Quaternion startRotation;
	private Vector3 waveStartPosition;
	private Vector3 origWaveBreakPosition;

    private bool resetInvincible = false;

    public Shark theShark;

    //pier grind variables
	GameObject currentGrindCollider;

	bool tutorialForceCarveUp;
	float tutorialStepCountdownTime = 0.5f;
	float tutorialCarveUpRotationSpeed = 0.7f;
	float tutorialCarveDownRotationSpeed = -0.7f;
	float aboutToShowTutorialCounter = 0.6f;
	bool tutorialCarveStop = false;

	int tutorialPlayerCarveCounter = 0;
	int gamesRestarted = 0;
	public bool inEndGameMenuMode = false;

	public Vehicle StartingVehicle { get; private set; }

	void OnDestroy()
	{
		GlobalSettings.surfGamePaused = false;
	}

	#region InGameState

	GameCompletionResult CompletionResult = GameCompletionResult.Undefined;

	protected enum GameCompletionResult
	{
		Undefined,
		WipedOut,
	}

	InGameState CurrentInGameState { get; set; }

	protected enum InGameState
	{
		Initialization,
		PreGame,
		PreGamePaddlingIntro,
		GamePlaying,
		GameTutorialPopup,
		PostGame,
	}
		
	void SetInGameState(InGameState state)
	{
		SetInGameStateAfterDelay(state, 0f);
	}

	void SetInGameStateAfterDelay(InGameState state, float delay = 0f)
	{
		#if UNITY_EDITOR
        //////Debug.Log("SETTING INGAME: "+  state);
		#endif

		if(delay > 0)
		{
			delayedGameState = state;
			inGameStateChangeDelaySeconds = delay;
		}
		else
		{
			CurrentInGameState = state;

			if(state == InGameState.Initialization)
			{
				HandleInGameStateInitialization();
			}
			else if(state == InGameState.PreGame)
			{
				HandleInGameStatePreGame();
			}
			else if(state == InGameState.PreGamePaddlingIntro)
            {
				HandleInGameStatePreGamePaddlingIntro();
			}
			else if(state == InGameState.GamePlaying)
			{
				
			}
			else if(state == InGameState.PostGame)
			{
				HandleInGameStatePostGame();
			}
			else if(state == InGameState.GameTutorialPopup)
			{
				theShark.PauseFinAnimatingObject();
				//gameUI.ShowEndGameDarkScreen();
				ShowTutorialInstruction();
			}
		}
	}

	InGameState? delayedGameState = null;
	float inGameStateChangeDelaySeconds = 0f;

	void HandleDelayedInGameStateChange()
	{
		if(inGameStateChangeDelaySeconds > 0)
		{
			inGameStateChangeDelaySeconds -= Time.deltaTime;
			if(inGameStateChangeDelaySeconds <= 0)
			{
				SetInGameState(delayedGameState.Value);
			}
		}
	}

	#endregion

	#region Initialization

	void HandleInGameStateInitialization()
	{
		if (levelGenerator == null)
		{
			levelGenerator = FindObjectOfType<LevelGenerator>();
		}

		if (gameUI == null)
			gameUI = FindObjectOfType<GameUI>();
		if (inGameTextureManager == null)
			inGameTextureManager = FindObjectOfType<InGameTextureManager>();
		if (powerUpManager == null)
			powerUpManager = FindObjectOfType<PowerupManager>();

		if (tutManager == null)
		{
			//tutManager = FindObjectOfType<TutorialManager>();
			//tutManager.RegisterCallBack(TutorialManager.CallbackType.EndDialogClosingStart, TutorialManagerEndDialogClosingStart);
		}
		
		LoadNewLevel();

        PauseParallaxForegroundAndSky();

		SetWaveSize();

		Vector3 uiPos = Camera.main.ScreenToWorldPoint( new Vector3(Camera.main.pixelWidth * 0.98f, Camera.main.pixelHeight * 0.92f, 0.0f));

		for (int i = 0; i < lettersToCollect.Length; i++)
		{
			lettersToCollect[i].gameObject.SetActive(false);
		}

		// FADE TEXTURES
		//midgroundLayer.transform.renderer.material.SetFloat("_Amount",0.5);
		//midgroundLayer.transform.renderer.material.SetTexture("_MainTex", Texture2Dhere);
		//midgroundLayer.transform.renderer.material.SetTexture("_AltTex", Texture2Dhere);

		bannerStartLocalPos = vehicleBanner.localPosition;

		underCollisionOffsetX =  underPlayerCollider.transform.position.x - transform.position.x;
		underCollisionOffsetY = underPlayerCollider.transform.position.y - transform.position.y;


		// Load level assets
		loadingScreen.SetActive(false);


		playerTransform = transform;
		beforeGrindZPos = playerTransform.position.z;
		headBreakBarrelYPos = barrelBreakParticleEmitter.transform.position.y;
		headBreakBarrelXWavOffset = waveParent.transform.position.x - barrelBreakParticleEmitter.transform.position.x;

		moveVector.x = startXVelocity;
		waveVelocityX = startXVelocity + 6;
		waveVelocityStartX = waveVelocityX;


		//prevHighScore = PlayerPrefs.GetInt("HS",0);

		cameraStartYPos = mainCamera.transform.position.y;

		if (playerStartX == -1)
			this.playerStartX = playerTransform.position.x;
		if (playerStartY == -1)
			this.playerStartY = playerTransform.position.y;

		playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, -45);

		if (GlobalSettings.soundEnabled)
		{
			soundWave.volume = 0;
			soundWave.loop  = true;
			soundWave.Play ();

			soundWaveBarrel.volume = 0;
			soundWaveBarrel.loop  = true;
			soundWaveBarrel.Play ();
		}
		else
		{
			soundWave.Stop();
			soundWaveBarrel.Stop();
		}
	

		rockSmashEffects = new GameObject[maxRockSmashEffects];
		for (int i = 0; i < maxRockSmashEffects; i++)
		{
			rockSmashEffects[i] = GameObject.Instantiate(rockSmashEffectPrefab);
			rockSmashEffects[i].SetActive(false);
		}

		//playerAnimation = (PlayerAnimation)transform.GetComponent("PlayerAnimation");
		if(VehicleHelper.IsStartingBoard(GameState.SharedInstance.CurrentVehicle))
			StartingVehicle  = GameState.SharedInstance.CurrentVehicle;
		else
			StartingVehicle = Vehicle.Surfboard;

		if (VehicleHelper.IsStartingBoard(GameState.SharedInstance.CurrentVehicle))
		{
			paddlingIntoWave = true;
			powerUpManager.HideAllVehicleTransforms();
			powerUpManager.SetCurrentVehicle(GameState.SharedInstance.CurrentVehicle);
			powerUpManager.ShowBaseSurfboard();
		}
		else
		{
			powerUpManager.SetCurrentVehicle(GameState.SharedInstance.CurrentVehicle);
			aboutToSlideBanner = true;
		}

		SetupHUD();

        if (currentLocation == Location.WavePark)
        {
            // disable the rock layer
            foregroundRocks.SetActive(false);

        }
        for (int i = 0; i < gateMissedIcons.Length; i++)
        {
            gateMissedIcons[i].SetActive(false);
        }

		if(!GlobalSettings.firstTimePlayVideo && GlobalSettings.TutorialCompleted())
        {
		    PauseGame();
		    goalUIManager.ShowGoalPanel(false);
			if (gameTimeTool != null)
				gameTimeTool.EndTimer();

        }


       // //////Debug.Log("FINISH INIT INGAME HERE");
	}

	public void LoadNewLevel()
	{
		#if UNITY_EDITOR
		//////Debug.Log("CURRENT LOCATION FROM MENU:" + GlobalSettings.menuSelectedLocation);
		#endif
        if (GlobalSettings.menuSelectedLocation == 1)
        {
            currentLocation = Location.WavePark;
            inGameTextureManager.LoadBackgroundImages(Location.WavePark);
        }
        else if (GlobalSettings.menuSelectedLocation == 2)
        {
            currentLocation = Location.BigWaveReef;
            levelGenerator.LoadLevelAssets();
   		}
        else
        {
            currentLocation = Location.MainBeach;
            inGameTextureManager.LoadBackgroundImages(Location.MainBeach);
        }

		levelGenerator.LoadLevelAssets();
		levelGenerator.SetLocationConfigs(currentLocation);
	}

	#endregion

	#region PreGamePaddlingIntro

	void HandleInGameStatePreGamePaddlingIntro()
	{
		paddleTimer = 0;
		playerAnimation.DoAnimation(PlayerAnimation.Animation.PADDLE);
		preGamePaddlingIntroEndSeconds = paddleDelay;
	}

	bool ShouldShowQuickPaddleInAnimationPriorToPreGame()
	{
		return true;
	}

	float? preGamePaddlingIntroEndSeconds = null;
	bool PreGamePaddlingIntroCompleted()
	{
		if(!preGamePaddlingIntroEndSeconds.HasValue)
			return true;

		preGamePaddlingIntroEndSeconds -= Time.deltaTime;
		if(preGamePaddlingIntroEndSeconds.Value < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	#endregion

	#region PreGame

	void HandleInGameStatePreGame()
	{
		loadingScreen.SetActive(false);

		if(HasPreGameDialogToShow())
		{
			ShowPreGameDialog();
		}
		else
		{
			SetInGameState(InGameState.GamePlaying);
		}
	}

	void HandlePreGameButtonClick()
	{
		if (Input.GetMouseButtonDown(0))
		{
			ButtonType buttonClicked = GetButtonTypeClicked();


			if(ButtonType.TutButton == buttonClicked)
			{
				//tutManager.RecieveInput();
			}
			else if(ButtonType.ChangeRide == buttonClicked)
			{
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				SceneNavigator.NavigateTo(SceneType.Shop);
			}
		}
	}

	bool HasPreGameDialogToShow()
	{
		return false;
	}
		
	void ShowPreGameDialog()
	{
		PauseGame();
	}
		
	void PreGameDialogEndCloseCompleteHandler()
	{
		SetInGameState(InGameState.GamePlaying);
	}

	bool PreGameDialogShowOnceCompleted(TutorialInfo tutorialInfo)
	{
		string key = string.Format("pc_prgdsoc{0}", (int)tutorialInfo);
		return PlayerPrefs.HasKey(key);
	}
		
	void FlagPreGameDialogShowOnceCompleted(TutorialInfo tutorialInfo)
	{
		string key = string.Format("pc_prgdsoc{0}", (int)tutorialInfo);
		PlayerPrefs.SetInt(key, 1);
	}

	#endregion 


	#region PostGame

	void HandleInGameStatePostGame()
	{
		if(CompletionResult == GameCompletionResult.WipedOut)
		{
		}
					
		if(HasPostGameDialogToShow())
		{
			ShowPostGameDialog();

			//tutManager.HandleDialogEndCloseComplete(() => HandlePostGameDialogEndCloseComplete());
		}
		else if(HasPostGameUIToShow())
		{
			ShowPostGameUI();
		}
		else
		{
            #if UNITY_ANDROID
            AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

            if(droidLoader != null)
            {
                droidLoader.Show();
            }
            #endif
			SceneNavigator.NavigateTo(SceneType.Menu);
		}
	}

	void SetGameCompletionResult(GameCompletionResult result)
	{
		CompletionResult = result;

		SetInGameState(InGameState.PostGame);
	}

	void HandlePostGameButtonClick()
	{
		if (Input.GetMouseButtonDown(0))
		{
			//ButtonType buttonClicked = GetButtonTypeClicked();
		}
	}

	bool HasPostGameDialogToShow()
	{
		return false;
	}

	void ShowPostGameDialog()
	{
		PauseGame();

        PauseParallaxForegroundAndSky();
	}


	void HandlePostGameDialogEndCloseComplete()
	{
	}


	bool HasPostGameUIToShow()
	{
		return true;
	}

	void ShowPostGameUI()
	{
		if(!GlobalSettings.SurfedPassMinTutorialDistance())
			postGameUIDelaySeconds = 1.5f;
		else
			postGameUIDelaySeconds = 1f;
	}

	float postGameUIDelaySeconds = 0f;
	bool showingPostGameDialog = false;
	void ShowPostGameUIAfterDelay()
	{
		if(postGameUIDelaySeconds > 0)
		{
			postGameUIDelaySeconds -= Time.deltaTime;
			if(postGameUIDelaySeconds <= 0)
			{
				if(!GlobalSettings.SurfedPassMinTutorialDistance())
				{
					gameUI.ResetCoinAndScoreBar();
					RestartGame();
				}
				else
				{
					PauseGame();
					gameUI.ShowEndSequence(currentLocation);
                    highPassFilter.enabled = true;
                    //chorusFilter.enabled = true;
                    gameMusic.volume = 0.5f;
					showingPostGameDialog = true;
				}
			}
		}
		else if(showingPostGameDialog)
		{
			gameUI.UpdateEndGame();
		}
	}
		
	void EnableAllParticleSystems(bool enable)
    {
		
		ParticleSystem[] particleSystems = FindObjectsOfType<ParticleSystem>();
		if(particleSystems != null)
		{
			foreach(var particleSystem in particleSystems)
			{
                if (enable)
                {
                    particleSystem.Simulate(0f, true, true);
                    if (particleSystem != waveBlowoutParticles)
                    {
                        particleSystem.Play();
                    }
				}
				else
				{
					particleSystem.Simulate(0f);
				}
			}
		}
    }


	bool PostGameDialogShowOnceCompleted(TutorialInfo tutorialInfo)
	{
		string key = string.Format("pc_postgdsoc{0}", (int)tutorialInfo);
		return PlayerPrefs.HasKey(key);
	}

	void FlagPostGameDialogShowOnceCompleted(TutorialInfo tutorialInfo)
	{
		string key = string.Format("pc_postgdsoc{0}", (int)tutorialInfo);
		PlayerPrefs.SetInt(key, 1);
	}
    
	#endregion

	#region TutorialManager Callbacks
	/*
	void TutorialManagerEndDialogClosingStart(TutorialManager.CallbackData callbackData)
	{
		if(CompletionResult == GameCompletionResult.Undefined)
		{
			SetInGameState(InGameState.GamePlaying);
		}
	}
	*/
	#endregion

    float waveResetYPosBigWave = -1.8f;
    //float waveResetYPosMedWave = -1f;
    //float waveResetYPosSmallWave = -1f;
    //float currentWaveResetYPos = -1f;

	void UpdateWaveIdleState()
	{
		if(GlobalSettings.surfGamePaused)
			return;
		
        if (playerTransform.position.y < -1f)
        {
            playerTransform.position = new Vector3(mainCamera.transform.position.x, -2f, playerTransform.position.z);
        }

        UpdateBarrel();

        //;//Mathf.Max(24f, waveVelocityX);
      
        waveParent.position = new Vector3(waveParent.position.x + (waveVelocityX * Time.deltaTime), waveParent.position.y, waveParent.position.z);

        if (currentWaveIdleState == WaveIdleState.WaveIdleCloseOut)
        {
            waveVelocityX = 12f;

            if (waveParent.position.x > mainCamera.transform.position.x + 28f)
            {
                currentWaveIdleState = WaveIdleState.WaveIdleNone;
            }

        }
        /*
        if (currentWaveIdleState == WaveIdleState.WaveIdleFaceRise)
        {
            inBarrel = false;
            waveVelocityX = 24f;
            UpdateWaveSounds();
           
            waveParent.position = new Vector3(waveParent.position.x, waveParent.position.y + 4f * Time.deltaTime, waveParent.position.z);

            if (waveParent.transform.position.y > maxWaveYPos)
            {
                currentWaveIdleState = WaveIdleState.WaveIdleCloseOut;
            }
        }

        if (currentWaveIdleState == WaveIdleState.WaveIdleAbate)
        {
            waveVelocityX = 3f;
            waveParent.position = new Vector3(waveParent.position.x, waveParent.position.y - 0.3f * Time.deltaTime, waveParent.position.z);
            waveParent.localScale = new Vector3(waveParent.localScale.x, waveParent.localScale.y * 1f - (Time.deltaTime * 0.2f), waveParent.localScale.z);

            waveParent.localScale = new Vector3(waveParent.localScale.x + Time.deltaTime / 6f, waveParent.localScale.y , waveParent.localScale.z);
            ////////Debug.Log("SACELE Y:" + waveParent.localScale.y);
            if (waveParent.localScale.y < 0)
                waveParent.localScale = new Vector3(waveParent.localScale.x, 0, waveParent.localScale.z);
            
            inBarrel = true;
            UpdateWaveSounds();


            if (waveParent.position.y < currentWaveResetYPos)
            {
                //////Debug.Log("REPOSITIONING WAVE HERE:");
                waveParent.transform.position = new Vector3(mainCamera.transform.position.x - 50f, -5f, waveParent.transform.position.z);
                currentWaveIdleState = WaveIdleState.WaveIdleFaceRise;
                waveParent.localScale = new Vector3(1, 1, 1);

            }
        }
        */

        float waveCameraXOffset = mainCamera.transform.position.x - waveParent.transform.position.x;
        if(barrelSurging)
        {
            //Debug.Log ("waveCameraXOffset:" + waveCameraXOffset);
        }
        waveCameraXOffset = Mathf.Abs (waveCameraXOffset);
        waveCameraXOffset = Mathf.Max (waveCameraXOffset, 13f);
        waveCameraXOffset *= -1f;
        inGameTextureManager.UpdateWaveTextureOffset(waveCameraXOffset);
	}

    AudioHighPassFilter highPassFilter;
    AudioChorusFilter chorusFilter;

	void ReShowTutorial()
	{
		if(!GameState.SharedInstance.SecondTutorialShown && GameState.SharedInstance.GamesPlayed > 3)
		{
			tutorialFramesShowCount = 0;
			GameState.SharedInstance.SecondTutorialShown = true;
			GlobalSettings.FlagTutorialAsUncompleted();
		}
	}

	void Start ()
	{

        // Prior to this, thruster's enum value was 10. Now Windsurfer Board takes that value. 
        // Code below corrects issue where wind surfboard is starting vehicle for players who have updated game.
        if (GameState.SharedInstance.CurrentVehicle == Vehicle.WindSurfBoard)
			GameState.SharedInstance.CurrentVehicle = Vehicle.Thruster;
		
		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();
		
		GameState.SharedInstance.AddGamePlayed();

		ReShowTutorial();
        Time.timeScale = 1;
        //Application.targetFrameRate = 60;

        //GlobalSettings.firstTimePlayVideo = false;


        highPassFilter = (AudioHighPassFilter)gameMusic.GetComponent<AudioHighPassFilter>();
        chorusFilter = (AudioChorusFilter)gameMusic.GetComponent<AudioChorusFilter>();
        highPassFilter.enabled = false;
        chorusFilter.enabled = false;
        gameMusic.volume = 1f;
		tutorialThumb.gameObject.SetActive(false);

		if (gameTimeTool != null)
			gameTimeTool.StartTimer();

		GoalManager.SharedInstance.StartNewGame();
		GoalManager.SharedInstance.UpdateLocationVisits(levelGenerator.curLocation);

        blackFadePanelRenderer = blackFadePanel.transform.GetComponent<Renderer>();
		if (!GlobalSettings.musicEnabled)
			gameMusic.Stop();

		AnalyticsAndCrossPromoManager.SharedInstance.Initialise();

        ////////Debug.Log("ERE 1");
		SetInGameState(InGameState.Initialization);

		GlobalSettings.surfGamePaused = false;
        //Application.targetFrameRate = 60;
        Time.timeScale = 1f;

		GameState.SharedInstance.RetryTimes = 0;

		midgroundRocksTexture = midgroundRocks.GetComponent<Renderer>().material.mainTexture;

		halfScreenWorldUnit = Vector3.Distance(	mainCamera.ScreenToWorldPoint(new Vector3 (mainCamera.pixelWidth, 0, mainCamera.nearClipPlane)),
												mainCamera.ScreenToWorldPoint(new Vector3 (mainCamera.pixelWidth * 0.5f, 0, mainCamera.nearClipPlane)));


		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementPlayedGames( GameState.SharedInstance.GamesPlayed );

		startPosition = transform.position;
		startRotation = transform.rotation;
		waveStartPosition = waveParent.transform.position;
		origWaveBreakPosition = waveBlowoutParticles.transform.position;

		OnFinishInvincibleAfterReset();

		//coin effects
		origSoundCoinHitPitch = soundCoinHit.pitch;
		//superCoinEffect.gameObject.SetActive(false);
		//superCoinEffect.Stop();

		gameMenuButtons = FindObjectOfType<GameMenuButtons>();

		if (gameMenuButtons != null)
		{
			gameMenuButtons.gettingMenuInput = true;
			gameMenuButtons.OnResumeGameButtonClicked = delegate()
			{
				ResumeGame();
				gameMenuButtons.gettingMenuInput = false;
				gameMenuButtons.pauseButton.SetActive(true);
				goalUIManager.HideGoalPanel();
				gameUI.HideEndGameDarkScreen();
			};
		}

		if(GlobalSettings.TutorialCompleted())
			PauseGame();
        
		#if UNITY_ANDROID
		// quang don't play intro videos on android devices
		GlobalSettings.firstTimePlayVideo = false;
		#endif

		if(Application.isEditor)
			GlobalSettings.firstTimePlayVideo = false;
        // TODO: BH REMOVE THIS
        //GlobalSettings.firstTimePlayVideo = false;

        if (GlobalSettings.firstTimePlayVideo)
        {

#if UNITY_IOS
            //blackFadePanel.SetActive(false);
            //EtceteraTwoBinding.playMovie(url, false, true, false);
#elif UNITY_ANDROID
            gameMusic.Stop();
            blackFadePanel.SetActive(true);
            //blackVideoOverlay.SetActive(true);
            showingReady = false;
            readyOverlayPanel.SetActive(false);
            //firstTimePlayVideo = false;
            gameUI.HideEndGameDarkScreen();
            string url = System.IO.Path.Combine(Application.dataPath, "Raw/SurferShort.mov");

#endif
            // string url = Path.Combine( Application.dataPath, "Raw/demoVid.mp4" );

            //////Debug.Log("Url:" + url);

#if UNITY_IOS
            // TODO: BH - Find an alternate video player here
            //
            //StartCoroutine("WaitForMoveDone");

            StartCoroutine(PlayMovie());
            blackFadePanel.SetActive(false);
            MoviePlayerFinished();
#elif UNITY_ANDROID

            // TODO: BH TEST IF WE CAN USE THE SAME UNITY Handheld. call here as with IOS
				StartCoroutine("WaitForMoveDone");
	            EtceteraAndroid.playMovie(url, 0x000000, false, EtceteraAndroid.ScalingMode.Fill, false);
#endif
        }
        else
        {
            blackFadePanel.SetActive(false);
        }

        if (GlobalSettings.TutorialCompleted())
        {
            theShark.DisableShark();

			//if(GlobalSettings.firstTimePlayVideo)
	         //   StartCoroutine(PlayStreamingVideo("SurferShort.mov"));
        }
    }

    IEnumerator PlayMovie()
    {
        Handheld.PlayFullScreenMovie("SurferShort.mov", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return 0;
    }

    public GameObject blackFadePanel;
    bool fadingIn = true;

    void OnEnable()
    {
#if UNITY_IOS
        EtceteraTwoManager.moviePlayerDidFinishEvent += MoviePlayerFinished;
#elif UNITY_ANDROID
        //nutin
#endif
    }

    void OnDisable()
    {
#if UNITY_IOS
        EtceteraTwoManager.moviePlayerDidFinishEvent -= MoviePlayerFinished;
#elif UNITY_ANDROID
        //nutin
#endif
    }

#if UNITY_ANDROID
    IEnumerator WaitForMoveDone()
    {
    	yield return new WaitForSeconds(10.0f);
    	MoviePlayerFinished();
    }
#endif

    void MoviePlayerFinished()
    {
        GlobalSettings.firstTimePlayVideo = false;
        fadingIn = true;
        if (GlobalSettings.musicEnabled)
            gameMusic.Play();

    }

	void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			if(CurrentInGameState != InGameState.GameTutorialPopup)
			{
				/*if(CurrentInGameState == InGameState.GameTutorialPopup)
				{
					GlobalSettings.FlagTutorialAsCompleted();
					tutorialForceCarveUp = true;
					theShark.ResumeFinAnimatingObject();
					gameUI.HideEndGameDarkScreen();
					ResumeGame();
					SetInGameState(InGameState.GamePlaying);
					HideTutorialInstruction();
				}*/

				if (!goalUIManager.isShowing && !wipingOut)
				{	
					if(Time.timeScale == 0)
						Time.timeScale = 1;

					if (gameMenuButtons != null)
					{
						if (gameMenuButtons.OnPauseButtonClicked != null && GlobalSettings.TutorialCompleted())
							gameMenuButtons.OnPauseButtonClicked();
					}
				}
			}
		}
	}

	void CapRetryMaxAndMinXVelocity()
	{
		if(maxXVelocity > GameServerSettings.SharedInstance.SurferSettings.MaxMaxXRetryVelocity)
		{
			maxXVelocity = GameServerSettings.SharedInstance.SurferSettings.MaxMaxXRetryVelocity;

			if(maxXVelocity < startMaxXVelocity)
				maxXVelocity = startMaxXVelocity;
		}

		if(minXVelocity > GameServerSettings.SharedInstance.SurferSettings.MaxMaxXRetryVelocity)
		{
			minXVelocity = GameServerSettings.SharedInstance.SurferSettings.MaxMaxXRetryVelocity;

			if(minXVelocity < startMinXVelocity)
				minXVelocity = startMinXVelocity;
		}
	}

    float resetTime = -9999;

    public void MakePlayerHorizontalForRestart()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.eulerAngles.y, 45f);
        carvingDown = false;
    }
    public void Reset ()
    {
		inEndGameMenuMode = false;
		CapRetryMaxAndMinXVelocity();

		// On save me reset to starting vehicle
		//powerUpManager.RemoveAllPowerUps();
		powerUpManager.SetCurrentVehicle(StartingVehicle);

		highPassFilter.enabled = false;
        chorusFilter.enabled = false;
        gameMusic.volume = 1f;


		if (gameTimeTool != null)
			gameTimeTool.ResumeTimer();

		OnGrindFinish();
		theShark.ResetShark(transform.position,false);

		if(!playerTransformReference.activeSelf)
			playerTransformReference.SetActive(true);
		
        OnStartInvincibleAfterReset();
		ResumeParallaxForegroundAndSky();
		ResumeGame();

		ShowSurferTrail();
        ////////Debug.Log("RESETTING TO levelGenerator.lastChunkFinishX:" + levelGenerator.lastChunkFinishX);

		//playerTransform.transform.position = new Vector3(levelGenerator.lastChunkFinishX, playerStartY, playerTransform.position.z);
		//levelGenerator.lastChunkFinishX += 20.0f;
		beforeFirstClick = true;
		SetInGameState(InGameState.GamePlaying);

        playerTransform.transform.position = new Vector3(wipeoutXPos - 4f, playerStartY, playerTransform.position.z);
		playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, -5f);

		playerAnimation.SetEmotion(Emotion.Determined);

		wipeoutBoard.SetParent(playerTransform);
		wipeoutBoard.transform.localPosition = origBoardPosition;
		wipeoutBoard.transform.localRotation = origBoardRotation;
		wipeoutBoard.transform.localScale = origBoardScale;
		wipeoutBoard = null;

		waveParent.transform.position = new Vector3(playerTransform.transform.position.x, waveParent.transform.position.y, waveParent.transform.position.z);
		inGameTextureManager.StopScrollingTextures();
		playerAnimation.ShowSurfboardStandingUpFrame();
		playerAnimation.SetEmotion(Emotion.Determined);

		powerUpManager.ToggleFreezePowerUps(false);

		waveVelocityX = startXVelocity + 6;
		barrelSurging = false;

		wipingOut = false;
		waveEnding = false;
        if(barrelSurging)
        {
            DoBarrelReturn();
        }

        barrelSurgeTimer = 0;
        barrelReturnTimer = 0;

		levelGenerator.multiplierParticles.gameObject.SetActive(false);
		//superCoinEffect.gameObject.SetActive(false);

       // if(hasIndoorSwitchSpawned)
        //{
            //hasIndoorSwitchSpawned = false;
            //levelGenerator.chunksSinceSwitchLocation = LevelGenerator.maxChunksBetweenSwitchLocations + 1;
       // }

        //postIndoorCameraSwitch = false;
    }

	public int GamesRestartedCount()
	{
		return gamesRestarted;
	}

    public void RestartGame ()
    {
		inEndGameMenuMode = false;
		gameUI.GameUISyncSavedGameData = false;
		gamesRestarted += 1;

		GameState.SharedInstance.AddGamePlayed();

		ReShowTutorial();
        //surferWaveTrail.gameObject.SetActive(false);
        //surferWaveTrail.emitting = false;
        //surferWaveTrail.Clear();
        //surferWaveTrail.transform.parent = null;
        //motorbikeWaveTrail.Clear();

		tutorialPlayerCarveCounter = 0;



        highPassFilter.enabled = false;
        chorusFilter.enabled = false;
        gameMusic.volume = 1f;
		if(!playerTransformReference.activeSelf)
			playerTransformReference.SetActive(true);

		if (gameTimeTool != null)
			gameTimeTool.StartTimer();

		GameState.SharedInstance.RetryTimes = 0;

		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementPlayedGames( GameState.SharedInstance.GamesPlayed );

        //player
        //Reset();
        surferTrailHidden = false;
        HideSurferTrail();
		transform.position = new Vector3(playerStartX, playerStartY, transform.position.z);
		playerTransform.rotation = startRotation;//Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, -5f);
		moveVector = Vector3.zero;

		ResumeParallaxForegroundAndSky();
		ResumeGame();
		ShowSurferTrail();

		playerAnimation.SetEmotion(Emotion.Determined);

		wipeoutBoard.SetParent(playerTransform);
		wipeoutBoard.transform.localPosition = origBoardPosition;
		wipeoutBoard.transform.localRotation = origBoardRotation;
		wipeoutBoard.transform.localScale = origBoardScale;
		wipeoutBoard = null;

		OnGrindFinish();
		powerUpManager.RemoveAllPowerUps();

		if (!VehicleHelper.IsStartingBoard(GameState.SharedInstance.CurrentVehicle))
			GameState.SharedInstance.CurrentVehicle = StartingVehicle;

		paddlingIntoWave = true;
		powerUpManager.HideAllVehicleTransforms();
		powerUpManager.SetCurrentVehicle(GameState.SharedInstance.CurrentVehicle);
		powerUpManager.ShowBaseSurfboard();

		carvingDown = false;
		beforeFirstClick = true;

		waveParent.transform.position = waveStartPosition;
		waveBlowoutParticles.transform.position = origWaveBreakPosition;
		maxXVelocity = startMaxXVelocity;
		minXVelocity = startMinXVelocity;

		moveVector.x = startXVelocity;
		waveVelocityX = startXVelocity + 6;
		barrelSurging = false;
		prevFrameXMove = 0;

        barrelReturning = true;
        barrelSurgeTimer = 0;
        barrelReturnTimer = 0;

		prevFrameYMove = 0;
		randomBarrelInterval = 5;
		barrelIntervalTimer = 0;
		barrelSurgeTimer = 0;
		barrelReturnTimer = 0;
		barrelSurging = false;
		barrelReturning = false;

        takeOffSurge = true;
        wavePeaked = false;
		wipingOut = false;
		waveEnding = false;

		distance = 0;
		distanceMilestonesReached = 0;
		Coins = 0;

		inGameTextureManager.StopScrollingTextures();

		//foreground objects
		InitialiseParallaxLayers();

		//level gen and location
		levelGenerator.ResetGame();

		inGameTextureManager.LoadBackgroundImages(Location.MainBeach);

		GoalManager.SharedInstance.StartNewGame();
		GoalManager.SharedInstance.UpdateLocationVisits(levelGenerator.curLocation);

		UpdateCamera();

		gameUI.ResetHighscore();

		SetInGameState(InGameState.PreGamePaddlingIntro);

		ResetParallaxLayerPlacements();

		theShark.ResetShark(transform.position,false);
		powerUpManager.ToggleFreezePowerUps(false);

		levelGenerator.multiplierParticles.gameObject.SetActive(false);
		//superCoinEffect.gameObject.SetActive(false);

		EnableDisableMidgroundRocks();

		indoorSwitchObject = null;
		postIndoorCameraSwitch = false;
		hasIndoorSwitchSpawned = false;
    }

	void CheckAndroidBackButton()
	{
#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape) && DialogTrackingFlags.CommonDialogShowing() == false)
		{
			if(gameMenuButtons != null)
			{
				if(inEndGameMenuMode)
				{
					AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

					if(droidLoader != null)
					{
						droidLoader.Show();
					}
					
					SceneNavigator.NavigateTo(SceneType.Menu);
				}
				else if(gameMenuButtons.pauseButton == null || gameMenuButtons.pauseButton.activeSelf == false)
				{
                    ResumeGame();
                    gameMenuButtons.gettingMenuInput = false;
                    gameMenuButtons.pauseButton.SetActive(true);
                    goalUIManager.HideGoalPanel();
                    gameUI.HideEndGameDarkScreen();
				}
				else
				{
					if(gameMenuButtons.OnPauseButtonClicked != null)
						gameMenuButtons.OnPauseButtonClicked();
				}
			}
		}
#endif
	}

    float showingGoTimer = 0;
    float showingGoDelay = 0.7f;
    bool showingGo = false;
    bool showingReady = true;
    Renderer blackFadePanelRenderer;
    float fadeTransparency = 1;
	void Update () 
    {
		CheckAndroidBackButton();

        //Debug.Log("Rotation z:" + transform.rotation.eulerAngles.z);
		if (!levelGenerator.loadedAssets)
        	return;

		if (fadingIn && !Application.isEditor)
        {
            fadeTransparency -= Time.deltaTime * 1.3f;
            if (fadeTransparency <= 0)
            {
                fadeTransparency = 0;
                fadingIn = false;

            }
            blackFadePanelRenderer.material.color = new Color(blackFadePanelRenderer.material.color.r, blackFadePanelRenderer.material.color.g, blackFadePanelRenderer.material.color.b, fadeTransparency); 
            if(!fadingIn)
            {
                this.blackFadePanel.SetActive(false);
            }
        }
		else if (Application.isEditor)
        {
			this.blackFadePanel.SetActive(false);
     		fadingIn = false;
        }

        if(GlobalSettings.firstTimePlayVideo && !Application.isEditor)
        {
            return;
        }
			
        GoalManager.SharedInstance.UpdateGoals();

        if (goalUIManager.isShowing)
        	return;

        if (showingReady)
        {
			if (levelGenerator.loadedAssets)
			{
				showingReady = false;
				readyOverlayPanel.SetActive(false);
				goText.SetActive(true);
				showingGo = true;
			}

        }
        else if (showingGo)
        {
            showingGoTimer += Time.deltaTime;
            if (showingGoTimer > showingGoDelay)
            {
                goText.SetActive(false);
                showingGo = false;
            }
        }

		HandleDelayedInGameStateChange();
		InitialiseParallaxLayers();


		// DEBUG KEYS
#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.Space))
		{
			PauseResumeGame();
		}

		if(Input.GetKeyDown(KeyCode.W))
		{
			DoBarrelSurge(true);
		}

		if(Input.GetKeyDown(KeyCode.P))
		{
			powerUpManager.SetCurrentPowerUp(PowerUp.Plane);
			powerUpManager.SetCurrentVehicle(Vehicle.Plane);
			playerAnimation.LockAnimationToAnim(PlayerAnimation.Animation.PLANE);
			curPlaneStartTime = planeStartTime;
			curSmashTime = smashTime;
			isPlane = true;
		}
			
		if (Input.GetKeyDown(KeyCode.W))
		{
			OnWipeout();
		}

		if(Input.GetKeyDown(KeyCode.M))
		{
			powerUpManager.SetCurrentPowerUp(PowerUp.CoinMagnet);
		}

		if(Input.GetKeyDown(KeyCode.B))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Boat);
		}

		if(Input.GetKeyDown(KeyCode.S))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.WindSurfBoard);
		}

		if(Input.GetKeyDown(KeyCode.J))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Jetski);
		}

		if(Input.GetKeyDown(KeyCode.O))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Longboard);
		}

		if(Input.GetKeyDown(KeyCode.T))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Tube);
		}

		if (Input.GetKeyDown(KeyCode.K))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Chilli);
		}

		if(Input.GetKeyDown(KeyCode.C))
		{
			powerUpManager.SetCurrentPowerUp(PowerUp.CoinMultiplier);
		}
		if (Input.GetKeyDown(KeyCode.D))
		{
			powerUpManager.SetCurrentPowerUp(PowerUp.ScoreMultiplier);
		}

		if(Input.GetKeyDown(KeyCode.L))
		{
			powerUpManager.SetCurrentVehicle(Vehicle.Motorcycle);
		}
#endif

		// END DEBUG KEYS

		if(CurrentInGameState == InGameState.Initialization)
		{
            if(ShouldShowQuickPaddleInAnimationPriorToPreGame() || GlobalSettings.firstTimePlayVideo)
			{
				SetInGameState(InGameState.PreGamePaddlingIntro);
			}
			else
			{
				SetInGameState(InGameState.PreGame);
			}
            UpdateCamera();	
			return;
		}

		if(CurrentInGameState == InGameState.GameTutorialPopup)
		{
			ShowHideTutorialThumbFrames();

			if(tutorialStepCountdownTime >= 0)
			{
				tutorialStepCountdownTime -= Time.deltaTime;
			}
			else if(tutorialFramesShowCount >= 2 && Input.GetMouseButtonDown(0))
			{
				GlobalSettings.FlagTutorialAsCompleted();
				tutorialForceCarveUp = true;
				theShark.ResumeFinAnimatingObject();
				gameUI.HideEndGameDarkScreen();
				ResumeGame();
				SetInGameState(InGameState.GamePlaying);
				HideTutorialInstruction();
			}

			return;
		}
			
		if(CurrentInGameState == InGameState.PreGame)
		{
			if (levelGenerator.loadedAssets)
			{
				HandlePreGameButtonClick();

				//UpdateWaveIdleState();
			}

		}
		else if(CurrentInGameState == InGameState.GamePlaying ||
				CurrentInGameState == InGameState.PreGamePaddlingIntro ||
				CurrentInGameState == InGameState.PostGame)
		{
			playerAnimation.UpdateFaceEmotionFrame();

			/*if(wipingOut && theShark.sharkGotPlayer && playerTransformReference.activeSelf)
			{
				playerTransformReference.SetActive(false);
			}*/


			if(CurrentInGameState == InGameState.PostGame)
			{
				if(!GameState.SharedInstance.SecondTutorialShown && tutorialPlayerCarveCounter > 3)
					GameState.SharedInstance.SecondTutorialShown = true;
				
					
				HandlePostGameButtonClick();
				//UpdateWaveIdleState();
				ShowPostGameUIAfterDelay();
			}

			if(!GlobalSettings.firstTimePlayVideo)
            {
			    gameUI.ShowInGameUI();
            }

			if (GlobalSettings.surfGamePaused && GlobalSettings.TutorialCompleted())
			{
                return;
			}

			if(CurrentInGameState == InGameState.PreGamePaddlingIntro)
			{
				if(PreGamePaddlingIntroCompleted())
				{
					if(GlobalSettings.TutorialCompleted())
					{
						SetInGameState(InGameState.PreGame);
					}
					else
					{
						ResumeGame();
						SetInGameState(InGameState.GamePlaying);
					}
				}
			}
				

			GlobalSettings.playerPosition = playerTransform.position;
			if(UpdatePowerUpBanner())
			{
				return;
			}

			////////Debug.Log("UPDATE CALLED:" + Time.time);
			float beforeTranslateX = transform.position.x;
			float beforeTranslateY = transform.position.y;

			bool planeCheck = isPlane;
			isPlane = powerUpManager.CheckForPowerup(PowerUp.Plane);

			if (!isPlane && planeCheck)
			{
				playerAnimation.UnlockAnimation();
				powerUpManager.OnCrash(transform.position);
				soundPlane.GetComponent<AudioSource>().Stop ();
                Debug.Log("Blowout 3");
                OnBarrellBlowout();
				//barrelSurging = false;
				//barrelReturning = true;
			}

			if (curSmashTime > 0.0f)
			{
				//if (!smashEffect.activeSelf)
				//	smashEffect.SetActive(true);

				curSmashTime -= Time.deltaTime;
			}
			else
			{
				if (smashEffect.activeSelf)
					smashEffect.SetActive(false);
			}


			if (curTimeMovementLocked <= 0.0f)
			{
				if(!wipingOut)
				{
					if (isPlane)
						UpdatePlane();
					else if(grindingRail)
						UpdateGrind();
					else
						UpdateMovement();	
				}
				else
					UpdateWipeout();
			}
			else
			{
				playerTransform.rotation = Quaternion.Slerp(playerTransform.rotation, Quaternion.identity, Time.deltaTime * 5.0f);
				playerTransform.Translate(8f * Time.deltaTime ,0, 0);
				curTimeMovementLocked -= Time.deltaTime;
				if (curTimeMovementLocked <= 0.0f)
				{
                    Debug.Log("Blowout 2");
                    OnBarrellBlowout();
				}
			}

			if (!waveEnding)
			{
				UpdateWave();
				UpdateWaveSounds();
			}

			UpdateInput();

			UpdateCheckIsInAir();

			UpdateShark();
			//UpdateBird();
			//UpdateChopper ();

			if(boosting)
			{
				//Debug.Log ("BOOSTING:" + Time.time);
				UpdateBoost();
			}

            if (airMiniBoosting)
            {
                UpdateAirMiniBoost();
            }


			prevFrameXMove = playerTransform.position.x - beforeTranslateX;
			prevFrameYMove = playerTransform.position.y - beforeTranslateY;

			//UpdateDistance();
			UpdateWaveSounds();
			UpdateTerrain();

			if(levelingOut)
			{
				UpdateLevelingOut();
			}
			UpdateUnderColliderPosition();

			if(!wipingOut && !waveEnding)
			{
				UpdateCamera();
			}
			DebugPrint("WX: " + (int)waveVelocityX + " PLAYERX: " + (int)moveVector.x + " MAXX: " + (int)maxXVelocity);

			/*
			fadeTimer += Time.deltaTime;
			if(fadeTimer > fadeDelay)
			{
				fadeTimer = 0;
				StartFadeToNextLocation();
			}
			*/

			playerAnimation.OnUpdateAnimation();

			float distanceTravelledThisFrame = transform.position.x - distance;
			distance += distanceTravelledThisFrame;

			if (distance - (distanceMilestone * (float)(distanceMilestonesReached + 1)) > 0)
			{
				distanceMilestonesReached += 1;
				levelGenerator.ShowDistanceMarker(distanceMilestone * (float)distanceMilestonesReached);
			}

            if (!wipingOut)
            {
                gameUI.AddScore(distanceTravelledThisFrame);
                GoalManager.SharedInstance.UpdateDistance(distanceTravelledThisFrame, powerUpManager.GetCurrentVehicle());
                GoalManager.SharedInstance.UpdateGoals();
            }

            if (GlobalSettings.soundEnabled)
            {
				if(	powerUpManager.GetCurrentVehicle() == Vehicle.Jetski ||
					powerUpManager.GetCurrentVehicle() == Vehicle.Boat ||
					powerUpManager.GetCurrentVehicle() == Vehicle.Motorcycle)
				{
					if(!soundJetski.GetComponent<AudioSource>().isPlaying)
					{
						GlobalSettings.PlaySound(soundJetski);
					}
				}
				else
				{
					if(soundJetski.GetComponent<AudioSource>().isPlaying)
					{
						soundJetski.GetComponent<AudioSource>().Stop ();
					}
				}
			}

		} // End InGameState.GamePlaying handler


        if(resetInvincible)
        {
            UpdateInvincibleAfterReset();
        }

	} // End Update function

	void ShowTutorialInstruction()
	{
		//gameUI.ShowDarkScreenSpotlight(transform.position, 1.4f, true);
		tutorialThumb.gameObject.SetActive(true);
		tutorialThumbText.text = GameLanguage.LocalizedText("PRESS\n<size=-26>TO GO UP</size>");
		tutorialThumb.DrawFrame(0);
		showTutorialThumb = true;

		HideSurferTrail();

		PauseGame();

	}

	bool tutorialForceFastMoveVelocity = false;
	float tutorialForceMoveFastCounter = 0f;
	void HideTutorialInstruction()
	{
		// On tutorial increase players velocity to ensure shark does not bite
		tutorialForceFastMoveVelocity = true;

		gameUI.HideDarkScreenSpotlight();

		ShowSurferTrail();

		showTutorialThumb = false;
		tutorialThumb.gameObject.SetActive(false);

		gameUI.HideEndGameDarkScreen();
	}


	float tutorialThumbTimer;
	int tutorialFramesShowCount = 0;

	void ShowHideTutorialThumbFrames()
	{
		if(showTutorialThumb && tutorialThumb.isActiveAndEnabled)
		{
			tutorialThumbTimer += Time.deltaTime;

			if(tutorialThumbTimer > 1.0f)
			{
				tutorialThumbTimer = 0;

				if(tutorialThumb.curFrame == 0)
				{
					tutorialThumbText.text = GameLanguage.LocalizedText("RELEASE\n<size=-26>TO GO DOWN</size>");
					tutorialThumb.DrawFrame(1);
					tutorialFramesShowCount += 1;
				}
				else
				{
					tutorialThumbText.text = GameLanguage.LocalizedText("PRESS\n<size=-26>TO GO UP</size>");
					tutorialThumb.DrawFrame(0);
					tutorialFramesShowCount += 1;
				}
			}
		}
	}

    void OnStartInvincibleAfterReset()
    {
        resetTime = Time.time;
        resetInvincible = true;
        playerAnimation.surferFramesMaterial.color = new Color(1,1,1,0.5f);
        blinkOn = false;
        currentBlinkDelay = blinkOffColorDelay;
        blinkColorTimer = 0;

    }

    void OnFinishInvincibleAfterReset()
    {
        resetInvincible = false;
        playerAnimation.surferFramesMaterial.color = new Color(1,1,1,1f);
    }

    float blinkColorTimer = 0;
    float blinkOnColorDelay = 0.2f;
    float blinkOffColorDelay = 0.5f;
    float currentBlinkDelay = 0;
    bool blinkOn = false;
    void UpdateInvincibleAfterReset()
    {
        blinkColorTimer += Time.deltaTime;
        if (blinkColorTimer > currentBlinkDelay)
        {
            blinkColorTimer = 0;
            blinkOn = !blinkOn;
            if (blinkOn)
            {
            	if (GlobalSettings.soundEnabled)
	                GlobalSettings.PlaySound(soundInvincibleWarning);
                playerAnimation.surferFramesMaterial.color = new Color(1, 1, 1, 1f);
                currentBlinkDelay = blinkOnColorDelay;
            }
            else
            {
                playerAnimation.surferFramesMaterial.color = new Color(1,1,1,0.5f);
                currentBlinkDelay = blinkOffColorDelay;
            }

        }

        if(Time.time - resetTime > safeResetTime)
        {
            OnFinishInvincibleAfterReset();
        }
    }

    void UpdateShark()
    {
        theShark.UpdateShark(playerTransform.position, headBreakBarrelYPos, moveVector.x, wipingOut);
    }

	bool parallaxLayerInitialised;
	void InitialiseParallaxLayers()
	{
		if(!parallaxLayerInitialised)
		{
			parallaxLayerInitialised = true;

			foregroundParallax.Initialise();
			midgroundRocksParallax.Initialise();
			midgroundParallax.Initialise(currentLocation);
		}
	}

	void ResetParallaxLayerPlacements()
	{
		foregroundParallax.ResetTerrainPoolPositionToReferenceFocalPoint();
		midgroundRocksParallax.ResetTerrainPoolPositionToReferenceFocalPoint();
		midgroundParallax.ResetTerrainPoolPositionToReferenceFocalPoint();
	}

	ButtonType GetButtonTypeClicked()
	{
		Ray inputRay = /*tutManager.showingTut ? 
			this.uiCam.ScreenPointToRay( Input.mousePosition ) : */
			this.mainCamera.ScreenPointToRay( Input.mousePosition );

		RaycastHit hitInfo;
		if (Physics.Raycast(inputRay, out hitInfo, 1000.0f))
		{
			if (hitInfo.collider.tag == "Button")
			{
				Button attachedButton = hitInfo.collider.gameObject.GetComponent<Button>();
				return attachedButton.type;
			}
		}

		return ButtonType.None;
	}

	void HandleGoalTextChanged(string text)
	{

	}

	void SetWaveSize()
	{
        ////////Debug.Log("SETTING WAVE SIZE: currentLocation:" + currentLocation);
		/*if(currentLocation == Location.BigWaveReef)
		{
			var scale = waveParent.localScale;
			waveParent.localScale = new Vector3(scale.x, 1.5f, scale.z);
			barrelCeilingYOffset *= 1.5f;
            currentWaveResetYPos = waveResetYPosBigWave;
		}*/
	}

	private void SetupHUD()
	{


        UpdateGoalsText();
	}

    private void UpdateGoalsText()
    {
		/*
        if(goalsComplete)
            return;
        
        goalTextMesh.text = "";

		foreach(ActivityGoal goal in curGoals.Goals.ToArray())
        {
            if (goal.Type == ActivityGoalType.ReachDistance)
            {
                if (!goal.IsCompleted)
                {
                    ActivityGoalReachDistance goalReachDistance = (ActivityGoalReachDistance)goal;
                    goalTextMesh.text += "DISTANCE: " + currentDistance + "/" + goalReachDistance.Distance + "\n";
                    highScoreTextMesh.text = ""; 
                }
                else
                {
                    goalTextMesh.text += "DISTANCE COMPLETE!\n";
                    highScoreTextMesh.text = ""; 
                } 
            }
            else if (goal.Type == ActivityGoalType.Barrels)
            {
                if (!goal.IsCompleted)
                {
                    ActivityGoalBarrels goalBarrels = (ActivityGoalBarrels)goal;
                    goalTextMesh.text += "BARRELS: " + barrellCount + "/" + goalBarrels.TotalRequired + "\n";
                    highScoreTextMesh.text = ""; 
                }
                else
                {
                    goalTextMesh.text += "BARRELS COMPLETE!\n";
                    highScoreTextMesh.text = ""; 
                } 
            }
            else if (goal.Type == ActivityGoalType.CollectLetters)
            {
                if (!goal.IsCompleted)
                {
                    ActivityGoalCollectLetters goalLetters = (ActivityGoalCollectLetters)goal;
					goalTextMesh.text += "LETTERS: " + levelGenerator.letterCollectCount + "/" + goalLetters.TotalRequired + "\n";
                    highScoreTextMesh.text = ""; 
                }
                else
                {
                    goalTextMesh.text += "LETTERS COMPLETE!\n";
                    highScoreTextMesh.text = ""; 
                } 
            }
            else if (goal.Type == ActivityGoalType.Slalom)
            {
            	if (!goal.IsCompleted)
            	{
            		ActivityGoalSlalom goalSlalom = (ActivityGoalSlalom)goal;
            		goalTextMesh.text += " SLALOM GATES PASSED:" + slalomGatesPassed + "/" + goalSlalom.TotalRequired + "\n";
					highScoreTextMesh.text = ""; 
            	}
            	else
            	{
					goalTextMesh.text += "LETTERS COMPLETE!\n";
                    highScoreTextMesh.text = ""; 
            	}

            }
            else if (goal.Type == ActivityGoalType.PierGrinds)
            {
                if (!goal.IsCompleted)
                {
                    ActivityGoalPierGrinds goalPierGrinds = (ActivityGoalPierGrinds)goal;
                    goalTextMesh.text += "GRINDS: " + pierGrindCount + "/" + goalPierGrinds.TotalRequired + "\n";
                    highScoreTextMesh.text = ""; 
                }
                else
                {
                    goalTextMesh.text += "GRINDS COMPLETE!\n";
                    highScoreTextMesh.text = ""; 
                } 
            }

        }
		*/
	}


	


	float generateNewTerrainXTriggerDistance = 50f;
	void UpdateTerrain()
	{
		levelGenerator.UpdateLevel(GlobalSettings.playerPosition);
		//chopperStartPos = chopper.transform.position;
	}

	void HideItem(Transform theItem)
	{
		theItem.localPosition = new Vector3(theItem.localPosition.x, theItem.localPosition.y, theItem.localPosition.z - 9999999f);
	}

	void ShowItem(Transform theItem)
	{
		//if(theItem.localPosition.z < -9999)
		//{
			theItem.localPosition = new Vector3(theItem.localPosition.x, theItem.localPosition.y, theItem.localPosition.z + 9999999f);
		//}
	}

	/*
	Vector3 chopperStartPos;
	float chopperSinCounter = 0;
	void UpdateChopper()
	{
		chopperSinCounter += Time.deltaTime * 5f;
		float chopperYSin = Mathf.Sin(chopperSinCounter);
		chopperYSin /= 3f;
		chopper.position = new Vector3(chopper.position.x, chopperStartPos.y + chopperYSin,chopper.position.z);

		if(chopper.position.x < playerTransform.position.x - 20)
		{
			chopper.position = new Vector3(playerTransform.position.x + 20, chopper.position.y, chopper.position.z);
		}
	}
	*/


	public void OnGrindingSpline(SplineGrind hitSpline)
	{
		isGrindingSpline = true;
		grindingRail = true;
		boosting = false;
        airMiniBoosting = false;
		jumpedFromGrind = false;
		airMoveVector.y = 0;
		splineGrind = hitSpline;

		playerAnimation.SetEmotion(Emotion.Content);

		UpdateGoalsText();

		GlobalSettings.PlaySound(soundGrind);

		playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 360);

		currentGrindYPos = hitSpline.GetVerticallyProjectedPoint(transform.position).y + powerUpManager.GetGrindYOffset();
		currentGrindXEndPos = hitSpline.GetLerpPoint(1.0f).x;

		Vector3 newPos = new Vector3(playerTransform.position.x, currentGrindYPos, grindZPos);
		playerTransform.position = newPos;

		grindSpeedMin = minXVelocity;

		gameUI.AddScore(20.0f, true, playerTransform.position);

		if (!isPlane)
		{
			playerAnimation.UnlockAnimation();
			playerAnimation.DoGrindLandingAnimation();
		}

		GoalManager.SharedInstance.UpdateGrinds();
		GoalManager.SharedInstance.UpdateGoals();
    }

	float wipeoutTimer = 0;
	float wipeoutDelay = 1;

	bool wipingOutUpFace = false;
	float wipeoutYForce = 0;
	float wipeoutYForceStartVelocity = 11f;
	float wipeoutYForceWaveGravity = 50f;


	bool levelingOut = false;

	bool IsLeveled()
	{
		if((playerTransform.rotation.eulerAngles.z == 0) || (playerTransform.rotation.eulerAngles.z == 360))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void UpdateLevelingOut()
	{
		if (isAboveSpline)
			return;

		if(IsLeveled())
		{
			levelingOut = false;
			return;
		}
		float beforeZRotation = playerTransform.rotation.eulerAngles.z;

		bool rotatingClockwise = false;
		if(beforeZRotation > 0.0001 && beforeZRotation < 180)
		{
			rotatingClockwise = true;
			playerTransform.Rotate (0,0,-45f * Time.deltaTime);
		}
		else if(beforeZRotation < 359.999f && beforeZRotation > 180)
		{
			rotatingClockwise = false;
			playerTransform.Rotate (0,0, 45f * Time.deltaTime);
		}

		if(rotatingClockwise)
		{
			if(playerTransform.rotation.eulerAngles.z > 300)
			{
				//playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 360);
			}
		}
		else
		{
			if(playerTransform.rotation.eulerAngles.z > 300)
			{
				//playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 360);
			}
		}
	}

	bool IsAboveRail()
	{
		return underPlayerCollider.IsAboveRail();
	}


	void UpdateWipeout()
	{
	   if (playerTransform.position.y > -2)
        {
            playerTransform.Rotate(0,0,-400 * Time.deltaTime);
            wipeoutYForce -= wipeoutYForceWaveGravity * Time.deltaTime;
            playerTransform.position = new Vector3(playerTransform.position.x + 5f * Time.deltaTime, playerTransform.position.y + wipeoutYForce * Time.deltaTime, playerTransform.position.z);

			wipeoutBoard.Rotate(0,0,-200 * Time.deltaTime);
			wipeoutYForce -= wipeoutYForceWaveGravity * 0.5f * Time.deltaTime;
			wipeoutBoard.position = new Vector3(wipeoutBoard.position.x + 3f * Time.deltaTime,
												wipeoutBoard.position.y + wipeoutYForce * Time.deltaTime,
												wipeoutBoard.position.z);


        }
    }

	float barrelSurgeTimer = 0;
	float barrelSurgeDelay = 1;
	float barrelReturnTimer = 0;
	float barrelReturnDelay = 2;
	bool barrelSurging = false;
	bool barrelReturning = false;

	void UpdateBarrel()
	{
		if(barrelSurging)
		{
			UpdateBarrelSurge();
		}
		else if(barrelReturning)
		{
			UpdateBarrelReturn ();
		}
	}

	bool barrelReturnShowAnimation = false;
	void DoBarrelSurge(bool showAnimation)
	{
		if(isPlane || grindingRail || isGrindingSpline)
            return;
        
		barrelReturnShowAnimation = showAnimation;
		barrelSurging = true;
		barrelReturning = false;
		barrelSurgeTimer = 0;
		barrelReturnTimer = 0;
		wavePeaked = false;
		if(showAnimation)
		{
			waveCrestAnimation.Play("Straighten");
		}
	}

	void DoBarrelReturn()
	{
		barrelReturning = true;
		barrelSurging = false;
		barrelReturnTimer = 0;
		barrelSurgeTimer = 0;
		if(barrelReturnShowAnimation)
		{
			waveCrestAnimation.Play("StraightenReset");
		}
	}

	bool blowoutBoosting = false;
	float blowoutBoostingTimer = 0;
	float blowoutBoostingDelay = 3f;
	float barrelSurgeXVelocity = 25f;
	void UpdateBarrelSurge()
	{
        if (isPlane)
        {
            DoBarrelReturn();
        }
		barrelSurgeTimer += Time.deltaTime;

		if(playerTransform.position.x > waveParent.transform.position.x -12)
		//if(playerTransform.position.x > waveParent.transform.position.x -5)
		{
			waveVelocityX += 180f * Time.deltaTime;
		}
		else
		{
			if(barrelSurgeTimer > barrelSurgeDelay)
			{
				DoBarrelReturn();
			}

		}
		//Debug.Log ("BARREL TO SURFER X DISTANCE:" + (waveParent.position.x - playerTransform.position.x));
		//waveParent.transform.position = new Vector3(waveParent.transform.position.x + barrelSurgeXVelocity * Time.deltaTime ,waveParent.transform.position.y,waveParent.transform.position.z);

		/*

			barrelSurgeTimer = 0;
			Debug.Log ("STARTING BARREL RETURN HERE 101");

			if(barrelReturnShowAnimation)
			{
				waveBlowoutParticles.Emit(150);
			}
			DoBarrelReturn();
			// TODO Make sure player is within the blowout zone
			//blowoutBoosting = true;
		}
		*/
	}

	void UpdateBarrelReturn()
	{
		barrelReturnTimer += Time.deltaTime;
		if(waveVelocityX > waveVelocityStartX)
		{
			waveVelocityX -= 40f * Time.deltaTime;
		}

		if(playerTransform.position.x > waveParent.transform.position.x -5)
		{
			if(barrelReturnTimer > barrelReturnDelay)
			{
				barrelReturning = false;
				barrelSurging = false;
				barrelSurgeTimer = 0;
				barrelReturnTimer = 0;
				//Debug.Log ("FINSIHED BARREL RETURN HERE 101");
			}
		}
	}

	void PauseResumeGame()
	{
		if(Time.timeScale == 0)
			Time.timeScale = 1;
		else
			Time.timeScale = 0;
	}

	public void PauseGame()
	{
		GlobalSettings.surfGamePaused = true;
        EnableAllParticleSystems(false);
	}

    void PauseParallaxForegroundAndSky()
    {
        GlobalSettings.cameraMovementIngamePaused = true;
    }   

    void ResumeParallaxForegroundAndSky()
    {
        GlobalSettings.cameraMovementIngamePaused = false;
    }

	public void ResumeGame()
	{
		if (gameTimeTool != null)
			gameTimeTool.ResumeTimer();

        Time.timeScale = 1;
        AudioListener.pause = false;

        EnableAllParticleSystems(true);
		powerUpManager.ToggleFreezePowerUps(false);

		GlobalSettings.surfGamePaused = false;
	}

	float barrelCeilingYOffset = 3.5f;
	float prevFrameXMove = 0;
	float prevFrameYMove = 0;

	public Vector3 airMoveVector = new Vector3(0,0,0);

	bool paddlingIntoWave = false;
	float paddleTimer = 0;
	float paddleDelay = 0.7f;

    void OnTakeOff()
    {
		if(VehicleHelper.IsStartingBoard(powerUpManager.GetCurrentVehicle()))
	        playerAnimation.DoAnimation(PlayerAnimation.Animation.STANDING_UP);
   
        ResumeParallaxForegroundAndSky();
    }

	void UpdateMovement()
	{
		float currentZRotation = 0;

		if(paddlingIntoWave)
		{
			paddleTimer += Time.deltaTime;
			if(paddleTimer > paddleDelay)
			{
				paddleTimer = 0;
				paddlingIntoWave = false;
                OnTakeOff();
			}
			else
			{
				playerTransform.Translate(0,2f * Time.deltaTime, 0);
				return;
			}
		}

		//Debug.Log ("moveVector.x:" + moveVector.x + " RotationZ:" + playerTransform.rotation.eulerAngles.z);

		currentZRotation = playerTransform.rotation.eulerAngles.z;
		//bool disableSpeedMin = false;

		if(carvingDown) 
		{
			currentCarveRotationSpeed = powerUpManager.GetPowerUpCarveRotationSpeed(carveRotationSpeedDown, carvingDown);

			soundJetski.GetComponent<AudioSource>().pitch -= 2f * Time.deltaTime;
			if(soundJetski.GetComponent<AudioSource>().pitch < 2.0f)
			{
				soundJetski.GetComponent<AudioSource>().pitch = 2.0f;
			}

			if(currentZRotation > 270 && currentZRotation < 360)
			{
				////////Debug.Log("CARVING DOWN HERE EXTRA SPEEEEEED" + moveVector.x);
				if(playerTransform.position.x < waveParent.position.x)
				{
					waveVelocityX -= 15f * Time.deltaTime;
					moveVector.x += carveDownXVelocityIncrease * 2.5f * Time.deltaTime;
					currentCarveRotationSpeed = carveRotationSpeedDown / 2f;
				}
				else
				{
					moveVector.x += carveDownXVelocityIncrease * Time.deltaTime;
				}
			}
			else
			{
				moveVector.x += carveDownXVelocityIncrease / 2f * Time.deltaTime;
			}
		}
		else
		{
			currentCarveRotationSpeed = powerUpManager.GetPowerUpCarveRotationSpeed(carveRotationSpeedUp, carvingDown);

			soundJetski.GetComponent<AudioSource>().pitch += 2f * Time.deltaTime;
			if(soundJetski.GetComponent<AudioSource>().pitch > 2.9f)
			{
				soundJetski.GetComponent<AudioSource>().pitch = 2.9f;
			}

			if(currentZRotation > 270 && currentZRotation < 360)
			{
				moveVector.x += carveDownXVelocityIncrease * -1 * Time.deltaTime;
			}
		}

		carveDragXFactor = 0;
		if(currentZRotation > 0 && currentZRotation < 90)
		{
			// Q1 NorthEast Quadrant
			carveDragXFactor = playerTransform.rotation.eulerAngles.z / 90 * 1;
			//moveVector.x -= carveDragXFactor * 10f * Time.deltaTime;
		}
		//carveRotationSpeedUp

		float mouseTimer = 0;
		if(carvingDown)
		{
			mouseTimer = mouseReleaseTimer;
		}
		else
		{
			mouseTimer = mouseHoldTimer;
			mouseTimer *= 50f;
		}
		float mouseRotationScaleFactor = (1 + mouseTimer * 2);
		mouseRotationScaleFactor = Mathf.Min(mouseRotationScaleFactor,2);
		//Debug.Log ("mouseRotationScaleFactor:" + mouseRotationScaleFactor);
		currentCarveRotationSpeed *= mouseRotationScaleFactor;

		if (!isAboveSpline)
		{
			if(!IsAboveRail())
			{
				if(inAir)
				{
					if(!boosting) 
					{
						if((jumpedFromGrind) && airMoveVector.y > 0)
						{
							playerTransform.Rotate(0,0, airRotationSpeed * 0.75f * Time.deltaTime);
						}
						else
						{
							playerTransform.Rotate(0,0, airRotationSpeed * Time.deltaTime);
						}
					}
				}
				else if(!GlobalSettings.TutorialCompleted() && !tutorialForceCarveUp)
				{
					if(tutorialCarveStop)
					{
						if(currentZRotation < 90)
						{
							playerTransform.Rotate(0,0, (currentCarveRotationSpeed * tutorialCarveDownRotationSpeed) * Time.deltaTime);
						}
						else
						{
							aboutToShowTutorialCounter -= Time.deltaTime;
							if(aboutToShowTutorialCounter <= 0)
							{
								aboutToShowTutorialCounter = 0;
								SetInGameState(InGameState.GameTutorialPopup);
							}
						}
					}
					else
					{
						playerTransform.Rotate(0,0, (currentCarveRotationSpeed * tutorialCarveUpRotationSpeed) * Time.deltaTime);
						if(currentZRotation > 20f && currentZRotation < 70f)
						{
							tutorialCarveStop = true;
						}
					}

				}
				else if(beforeFirstClick || CurrentInGameState == InGameState.PostGame)
				{
					if (CurrentInGameState == InGameState.PostGame && transform.position.y < headBreakBarrelYPos * 0.5f)
						currentZRotation = Mathf.Lerp(currentZRotation, 355.0f, 0.1f);

					if((currentZRotation > cruisingZRotation) && (currentZRotation < 355))
					{
						playerTransform.Rotate(0,0, currentCarveRotationSpeed * Time.deltaTime);
					}
					else
					{
						playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 355);
					}
				}
				else
				{
                    if(!justLanded)
                    {
					    playerTransform.Rotate(0,0, currentCarveRotationSpeed * Time.deltaTime);
                    }
				}
			}
			else
			{
				levelingOut = true;
			}
			float adjustedZRotation = playerTransform.rotation.eulerAngles.z;

			if((adjustedZRotation < minDownRotationZ) && (adjustedZRotation > 180))
			{
				// Over rotated turning clockwise
				playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, minDownRotationZ);
			}
			else if((adjustedZRotation > minUpRotationZ) && (adjustedZRotation < 180))
			{
				// Over rotated turning clockwise
				playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, minUpRotationZ);
			}
		}
		else
		{
			if (playerTransform.position.y <= headBreakBarrelYPos)
			{
				isAboveSpline = false;
			}
		}

		if(distanceWaveFromPlayer > faceSlowdownDistance)
		{
			float slowdownFactor = distanceWaveFromPlayer - faceSlowdownDistance;
			//Debug.Log ("slowdownFactor:" + slowdownFactor);
			moveVector.x -= slowdownFactor * 10f * Time.deltaTime;//(distanceWaveFromPlayer * distanceWaveFromPlayer) * Time.deltaTime;

			waveVelocityX += 35f * Time.deltaTime;


			if((!barrelSurging) && (!barrelReturning))
			{
				barrelIntervalTimer += Time.deltaTime;
				if(barrelIntervalTimer > randomBarrelInterval)
				{
					//Debug.Log ("DOING BARREL SURGE HERE 101");
					DoBarrelSurge(true);
					SetNextRandomBarrelInterval();
				}
			}

		}
		else if(distanceWaveFromPlayer < 0)
		{
			float slowdownFactor = distanceWaveFromPlayer;
			moveVector.x += slowdownFactor * Time.deltaTime;//(distanceWaveFromPlayer * distanceWaveFromPlayer) * Time.deltaTime;
		}
		else
		{
			if(!barrelSurging)
			{
				barrelIntervalTimer += Time.deltaTime;
				if(barrelIntervalTimer > randomBarrelInterval)
				{
					if((!barrelSurging) && (!barrelReturning))
					{
						//Debug.Log ("DOING BARREL SURGE HERE 101");
						DoBarrelSurge(true);
						SetNextRandomBarrelInterval();
					}
				}

				if(waveVelocityX > waveVelocityStartX) 
				{
					waveVelocityX -= 25f * Time.deltaTime;
				}
			}
		}
			
		moveVector.x = Mathf.Max(minXVelocity, moveVector.x);
		moveVector.x = Mathf.Min(maxXVelocity, moveVector.x);
		if(tutorialForceFastMoveVelocity)
		{
			tutorialForceMoveFastCounter += Time.deltaTime;
			if(tutorialForceMoveFastCounter > 1.25f)
			{
				tutorialForceMoveFastCounter = 0;
				tutorialForceFastMoveVelocity = false;
			}
			moveVector.x = 13.5f;
		}

	
		if(blowoutBoosting)
		{
			/*
			blowoutBoostingTimer += Time.deltaTime;
			if(blowoutBoostingTimer > blowoutBoostingDelay)
			{
				blowoutBoostingTimer = 0;
				blowoutBoosting = false;
			}
			else
			{
				moveVector.x += 100f * Time.deltaTime;
			}
			moveVector.x = Mathf.Min(maxBlowoutXVelocity, moveVector.x);
			*/
		}
		else
		{

			if(powerUpManager.GetCurrentVehicle() != Vehicle.Surfboard)
			{
				moveVector.x = powerUpManager.GetVehicleXVelocity(moveVector.x);
			}
			else
			{
				if(moveVector.x > maxXVelocity && tutorialForceFastMoveVelocity == false)
				{
					moveVector.x = maxXVelocity;
				}
			}
		}

		if (CurrentInGameState == InGameState.PostGame)
		{
			transform.position += Vector3.up * Time.deltaTime;
		}
			
		if(maxXVelocity <= GameServerSettings.SharedInstance.SurferSettings.MaxMaxXVelocity)
		{
			maxXVelocity += GameServerSettings.SharedInstance.SurferSettings.XVelocityIncreaseRate * Time.deltaTime;
			minXVelocity += GameServerSettings.SharedInstance.SurferSettings.XVelocityIncreaseRate * Time.deltaTime;
		}

		// MAIN TRANSLATION
		if(inAir)
		{
			if(powerUpManager.GetCurrentVehicle() == Vehicle.WindSurfBoard)
			{
				airMoveVector.y -= (gravityForce * 0.7f) * Time.deltaTime;
			}
			else
			{
				airMoveVector.y -= gravityForce * Time.deltaTime;
			}

			playerTransform.position = new Vector3(playerTransform.position.x + airMoveVector.x * Time.deltaTime, playerTransform.position.y + airMoveVector.y * Time.deltaTime, playerTransform.position.z);
		}
		else
		{
			// Debug.Log ("moveVector.x:" + moveVector.x);
			playerTransform.Translate(moveVector.x * Time.deltaTime, 0,0);

			inGameTextureManager.UpdateScrollingTextures(moveVector);

			if(inBarrel && playerTransform.position.y > barrelCeilingYOffset)
			{	
				//if (TestSettings.wipeoutOnCrestCollision)
				//{
				//	OnWipeout();	
				//}
				//playerTransform.position = new Vector3(playerTransform.position.x,barrelCeilingYOffset, playerTransform.position.z);
			}


			if(wipeoutsEnabled)
			{
				if(playerTransform.position.y <= wipeOutYPos)
				{
					//Debug.Log ("PUD 7");
					playerTransform.position = new Vector3(playerTransform.position.x,wipeOutYPos, playerTransform.position.z);
					DebugPrint("WIPEOUT!");
					OnWipeout();

					wipingOutUpFace = false;
					waveEnding = false;
				}
			}

			if((playerTransform.position.y < this.headBreakBarrelYPos - 1.1f) && (surferTrailHidden))
			{
				ShowSurferTrail();
				ShowSurferBikeTrail();
			}
			else if((playerTransform.position.y > this.headBreakBarrelYPos - 0.5f) && (!surferTrailHidden))
			{
				HideSurferTrail();
				HideSurferBikeTrail();
			}
		}

		if(cruisingAlongBottom)
		{
			if(playerTransform.rotation.eulerAngles.z < cruisingZRotation)
			{
				playerTransform.Rotate(0,0,290f * Time.deltaTime);
			}
			else
			{
				playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, cruisingZRotation);
			}
		}


		if((!wipingOut) && (playerTransform.position.x < waveParent.transform.position.x - headBreakBarrelXWavOffset))
		{
			float yLipOffset = barrelBreakParticleEmitter.transform.position.y - (playerTransform.position.y + 0.2f);
			////////Debug.Log("yLipOffset:" + yLipOffset);
			if(powerUpManager.GetCurrentVehicle() != Vehicle.Bodyboard)
			{
				if((yLipOffset < 0.3) && (yLipOffset > -0.6))
				{
					moveVector.x -= headBreakBarrelXDrag * Time.deltaTime;
					waveVelocityX += headBreakBarrelXDrag * Time.deltaTime;
					barrelBreakParticleEmitter.transform.position = new Vector3(playerTransform.position.x - 0.7f, barrelBreakParticleEmitter.transform.position.y, barrelBreakParticleEmitter.transform.position.z);
				}
				else
				{
					barrelBreakParticleEmitter.transform.position = new Vector3(-9999f,barrelBreakParticleEmitter.transform.position.y,barrelBreakParticleEmitter.transform.position.z);
				}
			}
		}
		else
		{
			barrelBreakParticleEmitter.transform.position = new Vector3(-9999f,barrelBreakParticleEmitter.transform.position.y,barrelBreakParticleEmitter.transform.position.z);
		}
	}


	void UpdatePlane()
	{
		if (curPlaneStartTime > 0)
		{
			curPlaneStartTime -= Time.deltaTime;

			planeMovementVector.y += planeAscendBoost * Time.deltaTime;
			planeMovementVector.y = Mathf.Min(planeMovementVector.y, maxPlaneAscend * 2.0f);

		}

		if (Input.GetMouseButton(0) && transform.position.y < maxPlaneHeight)
		{
			planeMovementVector.y += planeAscendAccel * Time.deltaTime;
			planeMovementVector.y = Mathf.Min(planeMovementVector.y, maxPlaneAscend);

			planeMovementVector.x += carveDownXVelocityIncrease * 7.5f * Time.deltaTime;
		}
		else
		{
			planeMovementVector.y -= planeDecendAccel * Time.deltaTime;
			planeMovementVector.y = Mathf.Max(planeMovementVector.y, maxPlaneDecend);

			planeMovementVector.x += carveUpXVelocityDecrease * 7.5f * Time.deltaTime;
		}

		if (levelGenerator.aboutToTransition)
		{
			Vector3 aim = levelGenerator.transitionObject.transform.position;
			if (transform.position.x < aim.x)
			{
				float speed = planeMovementVector.magnitude;
				Vector3 direction = (aim - transform.position);
				direction.z = 0;
				planeMovementVector = direction.normalized * speed;
			}
		}

		planeMovementVector.x = Mathf.Max(minXVelocity * 1.2f, planeMovementVector.x);
		planeMovementVector.x = Mathf.Min(maxXVelocity * 1.2f, planeMovementVector.x);

		transform.position += planeMovementVector * Time.deltaTime;

		float lerpVal = (planeMovementVector.y - maxPlaneDecend) / (maxPlaneAscend - maxPlaneDecend);
		float angle = Mathf.Lerp(minPlaneAngle, maxPlaneAngle, lerpVal);
		float pitch = Mathf.Lerp(minPlaneSoundPitch, maxPlaneSoundPitch, lerpVal);

		soundPlane.pitch = pitch;
		GlobalSettings.PlaySound(soundPlane);

		transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 5.0f);

		if (playerTransform.position.y < this.headBreakBarrelYPos)
		{
			ShowSurferTrail();
			ShowSurferBikeTrail();
		}
		else
		{
			HideSurferTrail();
			HideSurferBikeTrail();
		}
		//Update Wave
		if(distanceWaveFromPlayer > faceSlowdownDistance)
		{
			float slowdownFactor = distanceWaveFromPlayer - faceSlowdownDistance;

			waveVelocityX += 35f * Time.deltaTime;

			if((!barrelSurging) && (!barrelReturning))
			{
				barrelIntervalTimer += Time.deltaTime;
				if(barrelIntervalTimer > randomBarrelInterval)
				{
					//Debug.Log ("DOING BARREL SURGE HERE 101");
					DoBarrelSurge(true);
					SetNextRandomBarrelInterval();
				}
			}
		}
		else
		{
			if(!barrelSurging)
			{
				barrelIntervalTimer += Time.deltaTime;
				if(barrelIntervalTimer > randomBarrelInterval)
				{
					if((!barrelSurging) && (!barrelReturning))
					{
						DoBarrelSurge(true);
						SetNextRandomBarrelInterval();
					}
				}
				if(waveVelocityX > waveVelocityStartX) 
					waveVelocityX -= 25f * Time.deltaTime;
			}
		}

		//check for beneath wave
		if(wipeoutsEnabled)
		{
			if(playerTransform.position.y <= wipeOutYPos)
			{
				playerTransform.position = new Vector3(playerTransform.position.x,wipeOutYPos, playerTransform.position.z);
				OnWipeout();
			}
		}

	}

	float randomBarrelInterval = 5;
	float barrelIntervalTimer = 0;
	void SetNextRandomBarrelInterval()
	{
		barrelIntervalTimer = 0;
		randomBarrelInterval = Random.Range (0f, 12f);//;12f);
	}

	int currentDistance = 0;
	float playerStartX = -1.0f;
	float playerStartY = -1.0f;

	void UpdateUnderColliderPosition()
	{
		underPlayerCollider.transform.position = new Vector3(playerTransform.position.x + underCollisionOffsetX, playerTransform.position.y + underCollisionOffsetY, underPlayerCollider.transform.position.z);
	}
		
	void UpdateDistance()
	{
		if(wipingOut)
			return;
		float distanceSurfed  = playerTransform.position.x - playerStartX;
		distanceSurfed /= 10;
		currentDistance = (int)distanceSurfed;
		ScoreBarTextMesh.text = currentDistance.ToString();
	}
		
	AudioSource currentWaveSound = null;

	float waveSoundFullVolumeDistance = 3;
	float waveSoundMinVolumeDistance = 10;
	float waveSoundMinVolume = 0.2f;
	float maxWaveVolume = 0.7f;

	void UpdateWaveSounds()
	{
		if(inBarrel)
		{
			if(soundWave.isPlaying)
			{
				currentWaveSound = soundWaveBarrel;
				soundWave.Stop();
				if (GlobalSettings.soundEnabled)
					soundWaveBarrel.Play ();
			}
		}
		else
		{
			if(soundWaveBarrel.isPlaying)
			{
				currentWaveSound = soundWave;
				soundWaveBarrel.Stop();
				if (GlobalSettings.soundEnabled)
					soundWave.Play ();
			}
		}
		if(distanceWaveFromPlayer < waveSoundFullVolumeDistance)
		{
			if (currentWaveSound != null)
				currentWaveSound.volume = maxWaveVolume;
		}
		else
		{
			float volumeValue = distanceWaveFromPlayer - waveSoundFullVolumeDistance;
			volumeValue = Mathf.Min (volumeValue, waveSoundMinVolumeDistance);
			volumeValue = waveSoundMinVolumeDistance - volumeValue;

			volumeValue = volumeValue / waveSoundMinVolumeDistance;
			volumeValue = Mathf.Max (volumeValue,waveSoundMinVolume);

			if (currentWaveSound != null)
				currentWaveSound.volume = volumeValue;
		}
	}


	void DebugPrint(string msg)
	{
		//debugTextMesh.text = msg;
	}

   

	bool OnWipeout(bool tooDeepInBarrel = false)
	{
        GlobalSettings.firstTimePlayVideo = false;

        if(resetInvincible)
            return false;

		if(barrelSurging)
			DoBarrelReturn();

        wipeoutXPos = transform.position.x;

		PauseParallaxForegroundAndSky();

		if(CurrentInGameState == InGameState.PostGame)
            return false;

		bool planeCheck = powerUpManager.CheckForPowerup(PowerUp.Plane);
		if (curPlaneStartTime > 0.0f && planeCheck)
		{
			ResumeParallaxForegroundAndSky();
            return false;
		}

		if(boosting)
		{
			ResumeParallaxForegroundAndSky();
            return false;
		}

		playerAnimation.SetEmotion(Emotion.Mad, 0.5f);

		if(!tooDeepInBarrel)
		{
			if(!VehicleHelper.IsStartingBoard(powerUpManager.GetCurrentVehicle()) || planeCheck)
			{
				// extra life
				powerUpManager.RemoveCurrentPowerUp(PowerUp.Plane);
				powerUpManager.OnCrash(playerTransform.position);

				OnBoost();
				ResumeParallaxForegroundAndSky();
				return false;
			}
		}


		Vehicle curVehicle = powerUpManager.GetCurrentVehicle();

		if (curVehicle == Vehicle.Thruster)
			wipeoutBoard = powerUpManager.thrusterboardTransform.transform;
		else if (curVehicle == Vehicle.GoldSurfboard)
			wipeoutBoard = powerUpManager.goldboardTransform.transform;
		else
			wipeoutBoard = powerUpManager.surfboardTransform.transform;

		origBoardPosition = wipeoutBoard.transform.localPosition;
		origBoardRotation = wipeoutBoard.transform.localRotation;
		origBoardScale = wipeoutBoard.transform.localScale;
		wipeoutBoard.SetParent(null, true);


		if(VehicleHelper.IsStartingBoard(powerUpManager.GetCurrentVehicle()))
		{
			playerAnimation.DoAnimation(PlayerAnimation.Animation.SLAM);
		}

		gameMenuButtons.pauseButton.SetActive(false);

		inGameTextureManager.StopScrollingTextures();

		wipingOutUpFace = true;
		wipeoutYForce = wipeoutYForceStartVelocity;
		if (!waveEnding)
		{
			GlobalSettings.PlaySound (soundDeath);
			wipingOut = true;
			 
			if ((CurrentInGameState != InGameState.PostGame))
				SetGameCompletionResult(GameCompletionResult.WipedOut);

			waveEnding = true;
		}
       
        HideSurferTrail();
		HideSurferBikeTrail();

		if(currentDistance > prevHighScore)
		{
			PlayerPrefs.SetInt("HS", currentDistance);
		}

		powerUpManager.ToggleFreezePowerUps(true);

        currentWaveIdleState = WaveIdleState.WaveIdleCloseOut;

        if (gameTimeTool != null)
			gameTimeTool.EndTimer();

		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementLongDistanceSurfer(distance);
		
		playerAnimation.SetEmotion(Emotion.Scared, 0.5f);

        return true;
	}

	bool inAir = false;
	void UpdateCheckIsInAir()
	{
		if(((powerUpManager.GetCurrentVehicle() == Vehicle.Jetski) ||
			(powerUpManager.GetCurrentVehicle() == Vehicle.Boat) ||
			 powerUpManager.GetCurrentVehicle() == Vehicle.Motorcycle)
			&& (inAir || boosting || airMiniBoosting))
		{
			soundJetski.GetComponent<AudioSource>().pitch = 2.9f;
		}

		if(boosting || airMiniBoosting)
			return;

		if((!inBarrel) && playerTransform.position.y > headBreakBarrelYPos)
		{
			if(!inAir)
				OnAir();
		}
		else
		{
			if(inAir)
				OnAirLanded();
		}
	}

	int airCount = 0;
	Vector3 surferTrailLocalPos = Vector3.zero;
	Vector3 surferBikeTrailLocalPos;

	float airXValue = 0;
	float airYValue = 0;
	void OnAir()
	{
		if(beforeFirstClick)
			return;
	
       // //////Debug.Log("AIR");
       /*
		if((!carvingDown) && (!wipingOut) )
		{
			//Debug.Log ("CARVE RIGHT CALL HERE 2");
			playerAnimation.DoTurnRightAnimation();
		}*/

		playerAnimation.DoJumpAnimation();
		//playerAnimation.DoTurnRightAnimation();

		DebugPrint("AIR");
		airCount ++;
		inAir = true;

		if(tutorialForceCarveUp)
		{
			tutorialForceCarveUp = false;
			beforeFirstClick = true;
		}

		airXValue = prevFrameXMove * Application.targetFrameRate;
		airXValue = Mathf.Min (airXValue, 15f);

		airYValue = prevFrameYMove * Application.targetFrameRate;
		airYValue = Mathf.Min (airYValue, 13f);

		//airMoveVector = new Vector3(prevFrameXMove * Application.targetFrameRate ,prevFrameYMove * Application.targetFrameRate,0f);
		airMoveVector = new Vector3(airXValue ,airYValue,0f);

        OnAirMiniBoost();

		//airMoveVector = new Vector3(12f,6f,0f);
	}

	float jumpAirVelocity = 8.0f;

	float maxJumpAngle = 30.0f;
	float jumpMaxAirVelocity = 2.0f;

	bool jumpedFromGrind = false;
	void OnGrindJump()
    {
        ////////Debug.Log("GRIND OLLIE!");

		float angle = playerTransform.rotation.eulerAngles.z;
		if (angle > maxJumpAngle)
			angle = 0;

		float addedVelocity = Mathf.InverseLerp(0.0f, maxJumpAngle, angle) * jumpMaxAirVelocity;

		airMoveVector.y = jumpAirVelocity + addedVelocity;

		soundGrind.Stop ();
		grindingRail = false;
		isGrindingSpline = false;

		if (splineGrind != null)
		{	
			splineGrind.Reset();
			splineGrind = null;
		}
		isAboveSpline = false;

		playerAnimation.DoGrindJumpAnimation();

		jumpedFromGrind = true;		
		playerTransform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, beforeGrindZPos);
		//playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 15);
		//airMoveVector = new Vector3(12f,6f,0f);
	}

    private IEnumerator PlayStreamingVideo(string url)
    {
        Handheld.PlayFullScreenMovie(url, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);

        yield return new WaitForEndOfFrame();
        //this.blackVideoOverlay.SetActive(false);
        yield return new WaitForEndOfFrame();

#if UNITY_EDITOR
        //////Debug.Log("Video playback completed.");
#endif
    }

    bool justLanded = false;
    float justLandedTimer = 0;
    float justLandedDelay = 0.1f;
	void OnAirLanded()
	{
		if(
			(powerUpManager.GetCurrentVehicle() == Vehicle.Jetski) ||
			(powerUpManager.GetCurrentVehicle() == Vehicle.Boat) ||
			(powerUpManager.GetCurrentVehicle() == Vehicle.Motorcycle)

			)
		{
			soundJetski.GetComponent<AudioSource>().pitch = 2.0f;
		}
		levelingOut = false;
		inAir = false;
		jumpedFromGrind = false;
		GlobalSettings.PlaySound (soundCarve);
		moveVector.x = airMoveVector.y;
        justLanded = true;

        if (!isPlane)
        {
			playerAnimation.DoLandingAnimation();
		}
	}

	bool surferTrailHidden = false;
	void HideSurferTrail()
	{
		if(surferTrailHidden)
			return;

		surferTrailHidden = true;
		if (surferTrailLocalPos == Vector3.zero)
			surferTrailLocalPos = surferWaveTrail.transform.localPosition;
        surferWaveTrail.emitting = false;
        surferWaveTrail.transform.parent = null;


    }

	void ShowSurferTrail()
	{

		if(!surferTrailHidden)
			return;
		surferTrailHidden = false;
        surferWaveTrail.emitting = true;

        surferWaveTrail.transform.parent = playerTransform;
		surferWaveTrail.transform.localPosition = surferTrailLocalPos + powerUpManager.GetTrailOffset();
        surferWaveTrail.Clear();

    }

	bool surferBikeTrailHidden = false;
	public void HideSurferBikeTrail()
	{
		if(surferBikeTrailHidden)
			return;

		surferBikeTrailHidden = true;
		surferBikeTrailLocalPos = motorbikeWaveTrail.transform.localPosition;
        motorbikeWaveTrail.emitting = false;

        motorbikeWaveTrail.transform.parent = null;
       

    }

	public void ShowSurferBikeTrail()
	{
		if(!surferBikeTrailHidden)
			return;

		surferBikeTrailHidden = false;
        motorbikeWaveTrail.emitting = true;

        motorbikeWaveTrail.transform.parent = playerTransform;
		motorbikeWaveTrail.transform.localPosition = surferBikeTrailLocalPos;
        motorbikeWaveTrail.Clear();


    }

	public void SetSurferBikeTrailActive(bool state)
	{
		motorbikeWaveTrail.gameObject.SetActive(state);
	}

	bool inBarrel = false;
	float timeInBarrel = 0;
	float timeInBarrelBlowout = 0.75f;
	int barrellCount = 0;

	void UpdateCheckIsInBarrel()
	{
		float xDiff = playerTransform.position.x - waveParent.position.x;
		if(playerTransform.position.y > headBreakBarrelYPos)
		{
			timeInBarrel = 0;
			inBarrel = false;
			return; 
		}
		////////Debug.Log("XDIFF:" + xDiff);
		if(xDiff < -20f)
		{
			inBarrel = true;
			DebugPrint("FOAMBALL WIPEOUT");
			OnWipeout(true);
		}
		else if(xDiff < -10f)
		{
			if(!inBarrel)
			{
				timeInBarrel = 0;
			}
			timeInBarrel += Time.deltaTime;
			inBarrel = true;
			DebugPrint("Barrel");
		}
		else
		{
			if(inBarrel)
			{

				if(timeInBarrel > timeInBarrelBlowout)
				{
					timeInBarrel = 0;
                    Debug.Log("Blowout 1");
					OnBarrellBlowout();
				}
			}
			inBarrel = false;
		}
	}
	
	void OnBarrellBlowout()
	{
        //waveBlowoutParticles.Emit(150);
        waveBlowoutParticles.Play();

        GlobalSettings.PlaySound(soundBarrelBlowout);

        barrellCount++;
	}

	bool wipingOut = false;
	bool wipeoutsEnabled = true;
	float wipeOutYPos = -1f;
	float cruisingYPos = 1f;
	float maxWaveYPos = 0.5f;
	float minWaveYPos = -0.2f;

	bool takeOffSurge = true;

	bool waveSpeedingUp = true;
	bool cruisingAlongBottom = false;
	float distanceWaveFromPlayer = 0;

    public enum WaveIdleState
    {
        WaveIdleFlyPast,
        WaveIdleCloseOut,
        WaveIdleAbate,
        WaveIdleFaceRise,
        WaveIdleNone
    };

    WaveIdleState currentWaveIdleState = WaveIdleState.WaveIdleFlyPast;


	void UpdateWave()
	{
        distanceWaveFromPlayer = playerTransform.position.x - waveParent.position.x;


        UpdateBarrel();

		// Update wave X Position / speed
		if(barrelSurging)
		{
			waveVelocityX = Mathf.Min (waveVelocityX, waveVelocityMaxXBarrelSurge);
		}
		else
		{
			waveVelocityX = Mathf.Min (waveVelocityX, waveVelocityMaxX);
		}



       // //////Debug.Log("WAVE X VELOCITY :" + waveVelocityX);
		//float waveVelocityXCurrent = waveVelocityX;
		//waveVelocityXCurrent = Mathf.Min (waveVelocityX, waveVelocityMaxX);//Mathf.Lerp (waveVelocityX, waveVelocityStartX, Time.deltaTime);

		if(grindingRail || isGrindingSpline)
		{
			waveVelocityX = Mathf.Max (grindXVelocity,waveVelocityX);
			curSpecialWaveCooldown = specialWaveCooldown;
		}
		else if (isPlane)
		{
			waveVelocityX = planeMovementVector.x;

			/*
			if (Vector3.Distance(transform.position, waveParent.position) > 3.0f)
				waveVelocityX = minXVelocity;
			else
				waveVelocityX = Mathf.Min(minXVelocity, moveVector.x);
			*/
			curSpecialWaveCooldown = specialWaveCooldown;
		}
		else if (curSpecialWaveCooldown > 0)
		{
			curSpecialWaveCooldown -= Time.deltaTime;

			if (Vector3.Distance(transform.position, waveParent.position) > 3.0f)
				waveVelocityX = minXVelocity;
			else
				waveVelocityX = Mathf.Min(minXVelocity, moveVector.x);
		}

		waveParent.position = new Vector3(waveParent.position.x + (waveVelocityX * Time.deltaTime), waveParent.position.y, waveParent.position.z);


		// Update Wave Y Pos
		if(!wavePeaked)
		{
			if(takeOffSurge)
			{
				waveParent.position = new Vector3(waveParent.position.x, waveParent.position.y + waveYIncreaseVelocityTakeOffSurge * Time.deltaTime, waveParent.position.z);
			}
			else
			{
				waveParent.position = new Vector3(waveParent.position.x, waveParent.position.y + waveYIncreaseVelocity * Time.deltaTime, waveParent.position.z);
			}
		}
		else
		{
			waveParent.position = new Vector3(waveParent.position.x, waveParent.position.y - waveYDecreaseVelocity * Time.deltaTime, waveParent.position.z);
		}


		if(waveParent.position.y > maxWaveYPos)
		{
			waveParent.position = new Vector3(waveParent.position.x, maxWaveYPos, waveParent.position.z);
			wavePeaked = true;
			takeOffSurge = false;
		}
		else if(waveParent.position.y < minWaveYPos)
		{
			if(wavePeaked)
			{
				waveParent.position = new Vector3(waveParent.position.x, minWaveYPos, waveParent.position.z);
				wavePeaked = false;
			}
		}

		headBreakBarrelYPos = barrelBreakParticleEmitter.transform.position.y;
       
		UpdateCheckIsInBarrel();

		float waveCameraXOffset = mainCamera.transform.position.x - waveParent.transform.position.x;
		if(barrelSurging)
		{
			//Debug.Log ("waveCameraXOffset:" + waveCameraXOffset);
		}
		waveCameraXOffset = Mathf.Abs (waveCameraXOffset);
		waveCameraXOffset = Mathf.Max (waveCameraXOffset, 13f);
		waveCameraXOffset *= -1f;
		inGameTextureManager.UpdateWaveTextureOffset(waveCameraXOffset);
	}

	float currentGrindYPos = 0;
	float currentGrindXEndPos = 0;
	float grindZPos = -10f;
	float beforeGrindZPos = 0;
	float grindSpeedMin = 10f;
	float grindXVelocity = 0;

    void OnGrindStart(GameObject railColliderGameObject)
	{
		playerAnimation.SetEmotion(Emotion.Content);

		// get the box collider from the object
		jumpedFromGrind = false;
		GlobalSettings.PlaySound(soundGrind);

		currentGrindCollider = railColliderGameObject;
		playerTransform.rotation = Quaternion.Euler(playerTransform.rotation.eulerAngles.x, playerTransform.rotation.eulerAngles.y, 360);

		//Debug.Log ("PUD 5");
		playerTransform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, grindZPos);

		currentGrindYPos = powerUpManager.GetGrindYOffset() + railColliderGameObject.GetComponent<Collider>().bounds.center.y + railColliderGameObject.GetComponent<Collider>().bounds.extents.y;

		currentGrindXEndPos = currentGrindCollider.transform.position.x + 7f;
		airMoveVector.y = 0;
		grindingRail = true;
		boosting = false;
        airMiniBoosting = false;
		grindSpeedMin = minXVelocity;

		playerTransform.position = new Vector3(playerTransform.position.x, currentGrindYPos, grindZPos);


        gameUI.AddScore(50.0f, true, playerTransform.position);

        GoalManager.SharedInstance.UpdateGrinds();
        GoalManager.SharedInstance.UpdateGoals();

        if (!isPlane)
        {
			playerAnimation.UnlockAnimation();
			playerAnimation.DoGrindLandingAnimation();
		}
	}

	void UpdateGrind()
	{
		if (isGrindingSpline)
		{
			Vector3 prevPlayerPos = playerTransform.position;
			Vector3 newPlayerPos = playerTransform.position;

			grindXVelocity = Mathf.Max (airMoveVector.x, grindSpeedMin);
			airMoveVector.x = grindXVelocity;

			newPlayerPos = new Vector3(playerTransform.position.x + grindXVelocity * Time.deltaTime, currentGrindYPos, grindZPos);

			if(newPlayerPos.x >= currentGrindXEndPos)
			{
				//playerTransform.position = newPlayerPos;
				OnGrindFinish();
			}
			else
			{
				newPlayerPos = splineGrind.GetVerticallyProjectedPoint(newPlayerPos);
				newPlayerPos.z = grindZPos;
				newPlayerPos.y += powerUpManager.GetGrindYOffset();

				Vector2 vectorDirection = splineGrind.GetVelocityAtVerticallyProjectedPoint(newPlayerPos).normalized;

				float angle = Mathf.Atan2(vectorDirection.y, vectorDirection.x) * Mathf.Rad2Deg;

				playerTransform.rotation = Quaternion.Slerp( playerTransform.rotation, Quaternion.AngleAxis( angle, Vector3.forward), 0.1f);

				playerTransform.position = newPlayerPos;
			}
		}
		else
		{
			grindXVelocity = Mathf.Max (airMoveVector.x, grindSpeedMin);
			airMoveVector.x = grindXVelocity;
		
			playerTransform.position = new Vector3(playerTransform.position.x + grindXVelocity * Time.deltaTime, currentGrindYPos, grindZPos);
			if(playerTransform.position.x > currentGrindXEndPos)
				OnGrindFinish();
		}
	}

	bool boosting = false;
	float boostTimer = 0;
	float boostDelay = 2.4f;

    bool airMiniBoosting = false;
    float airMiniBoostTimer = 0;
    float airMiniBoostDelay = 1f;

	void OnBoost()
	{
		if(!boosting)
		{
			/*
			if((!carvingDown) && (!wipingOut))
			{
				Debug.Log ("CARVE RIGHT CALL HERE 3");
				playerAnimation.DoTurnRightAnimation();
			}
			*/

			//if (VehicleHelper.IsStartingBoard(powerUpManager.GetCurrentVehicle()))
			//	playerAnimation.DoTurnRightAnimation();
			//else
				playerAnimation.DoJumpAnimation();


			airCount ++;
			inAir = true;
			airMoveVector.x = powerUpManager.GetVehicleBoostX();
			airMoveVector.y = 16f;
			boosting = true;
		}

	}

    void OnAirMiniBoost()
    {
        if(!airMiniBoosting)
        {
        	/*
            if((!carvingDown) && (!wipingOut))
            {
                //Debug.Log ("CARVE RIGHT CALL HERE 3");
				playerAnimation.DoTurnRightAnimation();
            }
            */
			if (!VehicleHelper.IsStartingBoard(powerUpManager.GetCurrentVehicle()))
				playerAnimation.DoJumpAnimation();

            airCount ++;
            inAir = true;
            airMoveVector.x = powerUpManager.GetVehicleBoostX();
			airMoveVector.y = powerUpManager.GetVehicleBoostY();
            airMiniBoosting = true;
        }
    }

	float boostForce = 13f;

    void UpdateAirMiniBoost()
    {
        HideSurferTrail();
		HideSurferBikeTrail();

        airMiniBoostTimer += Time.deltaTime;
        if(airMiniBoostTimer > airMiniBoostDelay)
        {
            airMiniBoostTimer = 0;
            airMiniBoosting = false;
        }
        else
        {
            //if(currentZRotation > 3)
            //{
            //  playerTransform.Rotate(0,0, -65f * Time.deltaTime);
            //}
            //playerTransform.position = new Vector3(playerTransform.position.x, playerTransform.position.y + boostForce * Time.deltaTime, playerTransform.position.z);
        }

        if(IsFallingInAir())
        {
            airMiniBoosting = false;
        }
    }

	void UpdateBoost()
	{
		HideSurferTrail();
		HideSurferBikeTrail();

		boostTimer += Time.deltaTime;
		if(boostTimer > boostDelay)
		{
			boostTimer = 0;
			boosting = false;
		}
		else
		{
			//if(currentZRotation > 3)
			//{
			//	playerTransform.Rotate(0,0, -65f * Time.deltaTime);
			//}
			//playerTransform.position = new Vector3(playerTransform.position.x, playerTransform.position.y + boostForce * Time.deltaTime, playerTransform.position.z);
		}

		if(IsFallingInAir())
		{
			boosting = false;
		}
	}

	void OnGrindFinish()
	{
		levelingOut = false;
		grindingRail = false;
		if (isGrindingSpline)
		{
			isGrindingSpline = false;
			airMoveVector.y = 4.0f;
		}

		isAboveSpline = false;
        if(splineGrind != null)
        {
		    splineGrind.Reset();
		    splineGrind = null;
        }
		soundGrind.Stop ();
		//Debug.Log ("PUD 2");
		playerTransform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, beforeGrindZPos);
	}

	public bool grindingRail = false;

	bool updatingPowerUpBanner = false;

	public enum PowerUpBannerSlideState
	{
		RESET,
		SLIDING_ON,
		PAUSING,
		SLIDING_OFF
	}
	
	Vector3 powerUpCrateExplodePos;

	PowerUpBannerSlideState currentBannerSlideState = PowerUpBannerSlideState.RESET;

	float powerUpBannerPauseTimer = 0;
	float powerUpBannerPauseDelay = 0.5f;
	float bannerSlideSpeed = 120f;

    public void SetVehicleBannerTitle(string vehicleTitle)
    {
        powerUpBannerText.text = vehicleTitle;
    }

	bool UpdatePowerUpBanner()
	{
       // //////Debug.Log("UpdatePowerUpBanner");
		if(aboutToSlideBanner)
		{
			aboutToSlideBannerTimer += Time.deltaTime;
			if(aboutToSlideBannerTimer > aboutToSlideBannerDelay)
			{
				currentBannerSlideState = PowerUpBannerSlideState.SLIDING_ON;
				GlobalSettings.PlaySound(soundBannerSlide);
				MuteAllLoopingSounds();
				aboutToSlideBanner = false;
				aboutToSlideBannerTimer = 0;
			}
			else
			{
				return false;
			}
		}
			
		if(currentBannerSlideState == PowerUpBannerSlideState.RESET)
		{
			return false;
		}
		else if(currentBannerSlideState == PowerUpBannerSlideState.SLIDING_ON)
		{
			vehicleBanner.position = new Vector3(vehicleBanner.position.x - bannerSlideSpeed * Time.deltaTime, vehicleBanner.position.y, vehicleBanner.position.z);

			if(vehicleBanner.position.x < mainCamera.transform.position.x)
			{
				vehicleBanner.position = new Vector3(mainCamera.transform.position.x, vehicleBanner.position.y, vehicleBanner.position.z);
				currentBannerSlideState = PowerUpBannerSlideState.PAUSING;
				GlobalSettings.PlaySound(soundPowerUp);
				powerUpBannerPauseTimer = 0;
			}
		}
		else if(currentBannerSlideState == PowerUpBannerSlideState.PAUSING)
		{
			powerUpBannerPauseTimer += Time.deltaTime;
			if(powerUpBannerPauseTimer > powerUpBannerPauseDelay)
			{
				powerUpBannerPauseTimer = 0;
				currentBannerSlideState = PowerUpBannerSlideState.SLIDING_OFF;
				GlobalSettings.PlaySound(soundBannerSlide);
			}
		}
		else if(currentBannerSlideState == PowerUpBannerSlideState.SLIDING_OFF)
		{
			vehicleBanner.position = new Vector3(vehicleBanner.position.x - bannerSlideSpeed * Time.deltaTime, vehicleBanner.position.y, vehicleBanner.position.z);
			if(vehicleBanner.transform.position.x < mainCamera.transform.position.x - bannerStartLocalPos.x)//!vehicleBanner.GetComponent<Renderer>().isVisible)
			{
				currentBannerSlideState = PowerUpBannerSlideState.RESET;
				vehicleBanner.localPosition = bannerStartLocalPos;
				ResumeAllLoopingSounds();
				powerUpBannerPauseTimer = 0;
			}
		}
		return true;
	}

	bool aboutToSlideBanner = false;
	float aboutToSlideBannerTimer = 0;
	float aboutToSlideBannerDelay = 0.1f;

	void MuteAllLoopingSounds()
	{
		soundWave.GetComponent<AudioSource>().Stop();
		soundJetski.volume = 0;
	}

	void ResumeAllLoopingSounds()
	{
		if (GlobalSettings.soundEnabled)
			soundWave.GetComponent<AudioSource>().Play();
		soundJetski.volume = 1;
	}
		
	void OnCratePowerup(Vector3 explodePos)
	{
        //////Debug.Log("ONCRATEPOWERUP");
		//if (isPlane)
		//{
			//powerUpManager.RemoveCurrentPowerUp(PowerUp.Plane);
			powerUpManager.OnCrash(playerTransform.position, false);
		//}

		powerUpManager.SetRandomVehicle();

		crateExplodeParticles.gameObject.transform.position = explodePos;
        // TODO: BH REPLACE THIS WITH NEW PARTICLE SYSTEM
        crateExplodeParticles.Play();
        GlobalSettings.PlaySound(soundCrate);
		aboutToSlideBanner = true;

		Vehicle curVehicle = powerUpManager.GetCurrentVehicle();

		if(curVehicle == Vehicle.Plane && gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementFrequentFlyer();

		GoalManager.SharedInstance.UpdateVehicleCollects(curVehicle);
		GoalManager.SharedInstance.UpdateGoals();

		HideSurferTrail();
		HideSurferBikeTrail();
		if((playerTransform.position.y < this.headBreakBarrelYPos - 1.1f))
		{
			ShowSurferTrail();
			if (curVehicle == Vehicle.Motorcycle)
				ShowSurferBikeTrail();
		}

		playerAnimation.SetEmotion(Emotion.Content);
	}

	void SpawnRockExplosion(Vector3 spawnPosition)
	{
		rockSmashEffects[curRockSmashIndex].transform.position = spawnPosition;
		rockSmashEffects[curRockSmashIndex].SetActive(true);

		ParticleSystem[] attachedParticles = rockSmashEffects[curRockSmashIndex].GetComponentsInChildren<ParticleSystem>();
		for (int i = 0; i < attachedParticles.Length; i++)
		{
			attachedParticles[i].Stop();
			attachedParticles[i].time = 0.0f;
			attachedParticles[i].Play();
		}
		GlobalSettings.PlaySound(soundExplode);

		curRockSmashIndex = ((curRockSmashIndex + 1) % maxRockSmashEffects);
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag=="Rock")
		{
			if (curSmashTime > 0.0f)
			{
				other.GetComponent<Rock>().Hide();
			}
			else
				OnWipeout();
		}
		else if(other.gameObject.tag=="Bird")
		{
			OnWipeout();
		}
		else if(other.gameObject.tag=="Crate")
		{
			other.gameObject.GetComponent<Renderer>().enabled = false;
			other.gameObject.GetComponent<Collider>().enabled = false;
			OnCratePowerup(other.gameObject.transform.position);
		}
		else if(other.gameObject.tag=="ChopperBlades")
		{
			other.gameObject.GetComponent<Renderer>().enabled = false;
			other.gameObject.GetComponent<Collider>().enabled = false;

			//OnHitChopperBlades();
		}
		else if(other.gameObject.tag=="Rail")
		{
			//OnWipeout();
			if(IsFallingInAir())
			{
				OnGrindStart(other.gameObject);
			}
		}
		else if(other.gameObject.tag=="Boost")
		{
			OnBoost();

			GlobalSettings.PlaySound(this.soundBoing);

			playerAnimation.SetEmotion(Emotion.Content, 0.5f);

		}
		else if(other.gameObject.tag == "Shark")
		{
			OnSharkAttack();
		}
		else if(other.gameObject.tag=="Coin")
		{
			OnCoinHit(other.gameObject);
		}
        else if(other.gameObject.tag=="Letter")
        {
            OnLetterHit(other.gameObject);
        }
        else if (other.gameObject.tag == "Slalom")
        {
        	OnSlalomHit(other.gameObject);
        }
        else if (other.gameObject.tag == "SpecialPickup")
        {
        	OnSpecialPickupHit(other.gameObject);
        }
        else if (other.gameObject.tag == "SpecialRewardPickup")
       	{
			OnSpecialRewardPickupHit(other.gameObject);
       	}
		else if (other.gameObject.tag == "IndoorSwitchObject")
       	{
       		OnIndoorSwitch(other.gameObject);
       	}
		else
		{
			Debug.Log ("HIT UNKNOWN OBJECT: " + other.gameObject.tag);
		}
	}

	bool IsFallingInAir()
	{
		if(airMoveVector.y < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void OnSharkAttack()
	{
		//////Debug.Log("OM NOM NOM NOM NOM");

        theShark.OnSharkAttack();
        //playerTransform.position = new Vector3(-999f, playerTransform.position.y, playerTransform.position.z);
		
		playerAnimation.SetEmotion(Emotion.Scared, 0.5f);

        wipingOutUpFace = true;
        if(OnWipeout())
        {
            playerTransformReference.SetActive(false);
        }
        /*
		if(IsStartingVehicle(powerUpManager.GetCurrentVehicle()))
		{
			playerAnimation.DoAnimation(PlayerAnimation.Animation.SLAM);
		}
        */
		//inGameTextureManager.StopScrollingTextures();
        /*
		wipingOutUpFace = true;
		wipeoutYForce = wipeoutYForceStartVelocity;

		if (!waveEnding)
		{
			GlobalSettings.PlaySound (soundDeath);
			wipingOut = true;

			SetGameCompletionResult(GameCompletionResult.WipedOut);
		}
		HideSurferTrail();

		if(currentDistance > prevHighScore)
		{
			PlayerPrefs.SetInt("HS", currentDistance);
		}
  */      
	}


	public void OnCoinHit(GameObject coinObject)
	{
		coinObject.GetComponent<Coin>().Hit();

		int coinsAdded = 0;
		int coinEarningsMultiple = 1;

		if(StartingVehicle == Vehicle.GoldSurfboard)
			coinEarningsMultiple = 2;

		if (powerUpManager.coinMulitplierPowerupOn || powerUpManager.vehicleCoinMultiplierPowerUpOn)
		{
			Coins += (2 * coinEarningsMultiple);
			coinsAdded += (2 * coinEarningsMultiple);
		}
		else
		{
			Coins += (1 * coinEarningsMultiple);
			coinsAdded += (1 * coinEarningsMultiple);
		}
		//check for superCoin
		bool isSuperCoin = false;
		float curTime = Time.time;
		if (curTime - timeSinceCoinHit < maxTimeBetweenCoinHits)
		{
			coinHitsSoFar++;
			if (coinHitsSoFar >= maxCoinHits)
			{
				isSuperCoin = true;
			}
			else
			{
				float pitchShiftAmount = 0.005f;
				float pitchShift = origSoundCoinHitPitch * (1 + ((float)(coinHitsSoFar - 1) / (float)maxTimeBetweenCoinHits) * pitchShiftAmount);

				soundCoinHit.pitch = pitchShift;

				GlobalSettings.PlaySound(soundCoinHit);		
			}	
		}
		else
		{
			soundCoinHit.pitch = origSoundCoinHitPitch;
			GlobalSettings.PlaySound(soundCoinHit);
			coinHitsSoFar = 1;
		}
		timeSinceCoinHit = curTime;
	

		if (isSuperCoin)
		{
			GlobalSettings.PlaySound (soundSuperCoinHit);

			int superCoinsEarnings = GameServerSettings.SharedInstance.SurferSettings.SuperCoinChainMultiple;
			if (powerUpManager.coinMulitplierPowerupOn || powerUpManager.vehicleCoinMultiplierPowerUpOn)
			{
				Coins += (superCoinsEarnings * coinEarningsMultiple);
				coinsAdded += (superCoinsEarnings * coinEarningsMultiple);
			}
			else
			{
				Coins += (superCoinsEarnings * coinEarningsMultiple);
				coinsAdded += (superCoinsEarnings * coinEarningsMultiple);
			}

			coinHitsSoFar = 1;
			superCoinEffect.Emit(8);


			//do something with coin bar

            //gameUI.AddScore(20.0f, true,playerTransform.position);
		}
		else if (StartingVehicle == Vehicle.GoldSurfboard)
		{
			superCoinEffect.gameObject.SetActive(true);
			superCoinEffect.Emit(2);

		}
		//else
			//GlobalSettings.PlaySound (soundCoinHit);

		gameUI.UpdateCoinTotal(Coins);
		GoalManager.SharedInstance.UpdateCoins(coinsAdded, powerUpManager.GetCurrentVehicle());
	}

    void OnLetterHit(GameObject letterObject)
    {
        //letterObject.GetComponent<Renderer>().enabled = false;
        letterObject.SetActive(false);
        //HideItem(coinObject.transform);
		levelGenerator.letterCollectCount++;

        Letter theHitLetter = letterObject.GetComponent<Letter>();
        GlobalSettings.PlaySound (soundLetter);
        //GlobalSettings.PlaySound (soundGoalsComplete);
        int hitLetterArrayIndex = (int)theHitLetter.CurrentLetter;

		levelGenerator.ActivateLetter(hitLetterArrayIndex);
        levelGenerator.ShowLetters();

        BoxCollider theCollider = lettersToCollect[hitLetterArrayIndex].gameObject.GetComponent<BoxCollider>();
        theCollider.enabled = false;

        if (levelGenerator.CheckAllLettersCollected())
        {
			playerAnimation.SetEmotion(Emotion.Ecstatic);
        
			//////Debug.Log("YAY ALL LETTERS COLLECTED!");
			GameState.SharedInstance.AddSURFCollect();
			levelGenerator.CollectLetters();
            GlobalSettings.PlaySound (soundGoalsComplete);
        }
    }

	void OnSlalomHit(GameObject slalomCollider)
	{
		SlalomGate slalom = slalomCollider.transform.parent.GetComponent<SlalomGate>();

        gameUI.AddScore(50.0f, true, playerTransform.position);

		if (!slalom.hasBeenHit)
		{
			slalom.OnPlayerhit();
			GoalManager.SharedInstance.AddSlalomHit();

			//GlobalSettings.PlaySound (soundPowerUp);
		}
	}

	public void OnSpecialPickupHit(GameObject pickup)
	{
		SpecialPickup pickUpPower = pickup.GetComponent<SpecialPickup>();
		pickUpPower.powerUpRenderer.enabled = false;

		playerAnimation.SetEmotion(Emotion.Content);

		switch (pickUpPower.powerUp)
		{
			case PowerUp.CoinMultiplier:
				powerUpManager.SetCurrentPowerUp(PowerUp.CoinMultiplier);
			break;
			case PowerUp.CoinMagnet:
				powerUpManager.SetCurrentPowerUp(PowerUp.CoinMagnet);	
			break;
			case PowerUp.ScoreMultiplier:
				powerUpManager.SetCurrentPowerUp(PowerUp.ScoreMultiplier);
			break;
		}

        //gameUI.AddScore(100.0f, true, playerTransform.position);

		GlobalSettings.PlaySound (soundCoinHit);
	}

	public void OnSpecialRewardPickupHit(GameObject pickup)
	{
		SpecialRewardPickup reward = pickup.GetComponent<SpecialRewardPickup>();
		reward.OnHit();

		playerAnimation.SetEmotion(Emotion.Ecstatic);

		switch (reward.specialType)
		{
			case SpecialReward.Coins:
				levelGenerator.GiveSuperCoinReward();
			break;
			case SpecialReward.SingleEnergy:
				GameState.SharedInstance.AddEnergyDrink(1);
				gameUI.ShowSuperRewardText(reward.specialType);
			break;
			case SpecialReward.DoubleEnergy:
				GameState.SharedInstance.AddEnergyDrink(2);
				gameUI.ShowSuperRewardText(reward.specialType);
			break;
		};

		GlobalSettings.PlaySound (soundGoalsComplete);
	}

	public void OnIndoorSwitch(GameObject indoorSwitch)
	{
		IndoorSwitchObject indoorSwitchObject = indoorSwitch.GetComponent<IndoorSwitchObject>();
		bool result = indoorSwitchObject.Hit();
		if (result)
		{
            //////Debug.Log("ON LOCATIONS SWITCH HERE HIT");
			curTimeMovementLocked = timeMovementLocked;
			mainCamera.transform.position = mainCamera.transform.position + Vector3.right * halfScreenWorldUnit * 2.5f;
			playerTransform.position = new Vector3(playerTransform.position.x, startPosition.y, playerTransform.position.z);

			inGameTextureManager.LoadBackgroundImages(levelGenerator.curLocation);
			inGameTextureManager.ResetTextures();

			hasIndoorSwitchSpawned = false;
			postIndoorCameraSwitch = true;

			GoalManager.SharedInstance.UpdateLocationVisits(levelGenerator.curLocation);
			GoalManager.SharedInstance.UpdateGoals();

			EnableDisableMidgroundRocks();

			waveVelocityX = waveVelocityMaxX * 0.75f;
			waveParent.position = new Vector3(playerTransform.position.x - waveVelocityX * 0.1f, waveParent.position.y, waveParent.position.z);

			levelGenerator.aboutToTransition = false;

			float planeSpeed = planeMovementVector.magnitude;

			planeMovementVector = (new Vector3(1.0f, 0.5f, 0.0f)).normalized * planeSpeed;
		}
	}

	public void EnableDisableMidgroundRocks()
	{
		switch(levelGenerator.curLocation)
		{
		case Location.ShipWreck:
		case Location.WavePark:
			midgroundRocksParallax.UpdateAllTerrainPoolTexture(midgroundRocksEmptyTexture);
			break;
		default:
			midgroundRocksParallax.UpdateAllTerrainPoolTexture(midgroundRocksTexture);
			break;
		}

	}

	public void PrepareIndoorCameraSwitch(GameObject indoorSwitch)
	{
		indoorSwitchObject = indoorSwitch;
		hasIndoorSwitchSpawned = true;
	}

	void UpdateCamera()
	{
		//check for stopping at indoorCameraSwitchObjets
		bool indoorSwitchCheck = false;
		if (hasIndoorSwitchSpawned)
		{	
			if (playerTransform.position.x + cameraXOffset > indoorSwitchObject.transform.position.x - halfScreenWorldUnit * 0.55)
				indoorSwitchCheck = true;
		}

		if (postIndoorCameraSwitch)
		{
			if (playerTransform.position.x > mainCamera.transform.position.x - cameraXOffset)
			{
				postIndoorCameraSwitch = false;
			}
			else
			{
				indoorSwitchCheck = true;
				foregroundParallax.UpdateFrame(false);
				midgroundParallax.UpdateFrame(false);
				midgroundRocksParallax.UpdateFrame(false);
			}

		}

		float camPlayerYOffset = 2f;
		if (!indoorSwitchCheck)
		{
			if(CurrentInGameState == InGameState.GamePlaying)
			{
				foregroundParallax.UpdateFrame();
				midgroundParallax.UpdateFrame();
				midgroundRocksParallax.UpdateFrame();
			}
			else
			{
				foregroundParallax.UpdateFrame(false);
				midgroundParallax.UpdateFrame(false);
				midgroundRocksParallax.UpdateFrame(false);
			}

			float camYOffset = mainCamera.transform.position.y - cameraStartYPos;
	        float midgroundCamOffset = camYOffset + 3.8f;//3.6f;//3.1f;
	        float foregroundCamOffset = camYOffset * 0.8f + 3.7f;//4f;

			mainCamera.transform.position = new Vector3(playerTransform.position.x + cameraXOffset, playerTransform.position.y - camPlayerYOffset, mainCamera.transform.position.z);
			if(mainCamera.transform.position.y < cameraStartYPos)
			{
				mainCamera.transform.position = new Vector3(playerTransform.position.x + cameraXOffset, this.cameraStartYPos, mainCamera.transform.position.z);
			}
		}
		else
		{
			mainCamera.transform.position = new Vector3(mainCamera.transform.position.x, playerTransform.position.y - camPlayerYOffset, mainCamera.transform.position.z);
			if(mainCamera.transform.position.y < cameraStartYPos)
			{
				mainCamera.transform.position = new Vector3(mainCamera.transform.position.x, this.cameraStartYPos, mainCamera.transform.position.z);
			}
		}

		//midgroundLayer.position = new Vector3(mainCamera.transform.position.x, mainCamera.transform.position.y - midgroundCamOffset, midgroundLayer.transform.position.z);
		//foregroundLayer.position = new Vector3(mainCamera.transform.position.x, mainCamera.transform.position.y - foregroundCamOffset, foregroundLayer.transform.position.z);
	}

	float mouseHoldTimer = 0;
	float mouseReleaseTimer = 0;

	bool mouseDown = false;
	void UpdateInput() 
	{
        if(justLanded)
        {
            justLandedTimer += Time.deltaTime;
            if(justLandedTimer > justLandedDelay)
            {
                justLanded = false;
                justLandedTimer = 0;
            }
            return;
        }
            
        
		if(CurrentInGameState != InGameState.GamePlaying)
		{	
			mouseDown = false;
			mouseHoldTimer = 0;
			mouseReleaseTimer += Time.deltaTime;
			if((!carvingDown) && (!beforeFirstClick))
			{
				//Debug.Log ("MOUSE UP CARVE DIR CHANGED HERE");
				OnChangeCarveDirection(true);
			}
			return;
		}
		if(grindingRail)
		{
			if(Input.GetMouseButtonDown(0))
			{
				OnGrindJump();
			}
			
			return;
		}
			
		if(!waveEnding && (Input.GetMouseButton(0) && GlobalSettings.TutorialCompleted()) || (tutorialForceCarveUp && !inAir))
		{ 
			if(!GameState.SharedInstance.SecondTutorialShown && Input.GetMouseButtonDown(0))
			{
				tutorialPlayerCarveCounter += 1;
			}


			if (paddleTimer == 0)
			{
	            if (beforeFirstClick)
	            {
	                OnTakeOff();
	            }
					
				beforeFirstClick = false;
				cruisingAlongBottom = false;
				paddlingIntoWave = false;

				if(!wipingOut && !inAir)
				{
					playerAnimation.DoTurnLeftAnimation();
				}
				mouseDown = true;
				mouseReleaseTimer = 0;
				mouseHoldTimer += Time.deltaTime;
				if(carvingDown)
				{
					//Debug.Log ("MOUSE DOWN CARVE DIR CHANGED HERE");
					OnChangeCarveDirection(false);
				}
			}
		}
		else
		{
			mouseDown = false;
			mouseHoldTimer = 0;
			mouseReleaseTimer += Time.deltaTime;
			if((!carvingDown) && (!beforeFirstClick))
			{
				//Debug.Log ("MOUSE UP CARVE DIR CHANGED HERE");
				OnChangeCarveDirection(true);
			}
		}
	

	}

	bool beforeFirstClick = true;

	void OnChangeCarveDirection(bool downCarve)
	{
		if(wipingOut)
			return;
		carvingDown = downCarve;
		if(!carvingDown)
		{
			if(!wipingOut)
			{
				GlobalSettings.PlaySound (soundCarve);
			}
		}
		if((!beforeFirstClick) && (!wipingOut) && !inAir)
		{
			if(carvingDown)
			{
				playerAnimation.DoTurnRightAnimation();
			}
			else
			{
				playerAnimation.DoTurnLeftAnimation();
			}
		}
	}

	public float GetYPositionOffset()
	{
		return transform.position.y - powerUpManager.GetGrindYOffset();
	}

}
