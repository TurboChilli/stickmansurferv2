﻿using UnityEngine;
using System.Collections;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class StarterPackDialogController : MonoBehaviour {

    public StarterPackDialog starterPackDialog;
    public Camera mainCamera;
    public StarterPackDialogTimer starterPackDialogTimer;
    private IAPManager iapManager;
    private StarterPackDialog starterPack1;
    private MainMenuController mainMenuController = null;

    private bool isShowingDialog = true;
    private DateTime openDateTime;
    private TimeSpan timeDifference;
    private double totalSeconds = 0;
    public AudioSource cashRegisterSound;
    public AudioSource clickSound;
    private bool showingContactingServerOverlay;
    private float showingContactingServerOverlayTimeoutSecs = -1f;
    public GameObject contactingServerOverlay;
    private int amountOfEnergyDrinks = 50;
    private int amountOfCoins = 10000;
    private int popUpDelayTime = 3;
    private bool delayTimeIsUp = false;
    private bool inMainMenu = false;

    // Use this for initialization
    void Start () 
    {   
        HideContactingServerOverlay();

        if(mainMenuController == null)
        {
            mainMenuController = FindObjectOfType<MainMenuController>();
        }

        starterPackDialog.SetDialogPositions();
        starterPackDialog.HideStarterPackDialog();
        starterPackDialog.SetPurchaseDialogPositions();
        starterPackDialog.HideStarterPackPurchasedDialog();

        starterPack1 = new StarterPackDialog();

        InitialiseIAPManager();
        InitialiseStarterPacks();
    }

    void DisableMainMenuClickCooldown()
    {
        if(mainMenuController != null)
        {
            mainMenuController.EnableMainMenuDisableClickCoolDown();
        }
    }

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape) && DialogTrackingFlags.MainMenuStarterPackShowing)
		{
			DialogTrackingFlags.FlagJustClosedPopupDialog();

			CloseStarterPackDialog();
		}
		#endif 
	}

	void CloseStarterPackDialog()
	{
		DisableMainMenuClickCooldown();

		GlobalSettings.PlaySound(clickSound);
		starterPackDialog.HideStarterPackDialog();
		starterPackDialogTimer.countDownSound.enabled = false;
		starterPackDialogTimer.timeUpSound.enabled = false;
		InitializeTimerBetweenDialogPopups(GameServerSettings.SharedInstance.SurferSettings.StarterPackShowBetweenMins);
		GameState.SharedInstance.PopUpDialogTimeIsUp = false;
		isShowingDialog = false;

		if (mainMenuController != null)
			mainMenuController.UpdateLimitedTimeOffer();
	}

    // Update is called once per frame
    void Update () 
    {
		CheckAndroidBackButton();

        GetOldTime();

        if(GameState.SharedInstance.StarterPackRulesMet && !GameState.SharedInstance.PopUpDialogTimeIsUp)
        {
            timeDifference = openDateTime.Subtract(DateTime.UtcNow);
            totalSeconds = timeDifference.TotalSeconds;
        }
        if(totalSeconds <= 0)
        {           
            GameState.SharedInstance.PopUpDialogTimeIsUp = true;
        }
       
        if(Input.GetMouseButtonDown(0))
        {
            if(UIHelpers.CheckButtonHit(starterPackDialog.closeButton.transform, mainCamera))
            {
				CloseStarterPackDialog();
            }

            else if(UIHelpers.CheckButtonHit(starterPackDialog.buyButton.transform, mainCamera))
            {
                DisableMainMenuClickCooldown();

                BuyStarterPack(starterPack1.StarterPackId);
                starterPackDialog.HideStarterPackDialog();
                starterPackDialogTimer.countDownSound.enabled = false;
                starterPackDialogTimer.timeUpSound.enabled = false;
                isShowingDialog = false;
            }

            else if(UIHelpers.CheckButtonHit(starterPackDialog.purchaseCompleteCloseButton.transform, mainCamera))
            {
                DisableMainMenuClickCooldown();
                GlobalSettings.PlaySound(clickSound);
                starterPackDialog.HideStarterPackPurchasedDialog();
                mainMenuController.UpdateLimitedTimeOffer();
            }
        }

        // Starter pack rules #1
        // if thruster board is purchased and at least 1 jeep game has 
        // been played then offer starter pack
		if(GlobalSettings.IsShowingStarterPackDialog())
        {            
            GameState.SharedInstance.StarterPackRulesMet = true;
			InitializeTimerBetweenDialogPopups(GameServerSettings.SharedInstance.SurferSettings.StarterPackFirstShowMins);
            isShowingDialog = true;
        }

        if(GameState.SharedInstance.StarterPackRulesMet && GameState.SharedInstance.PopUpDialogTimeIsUp)
        {  
            if(isShowingDialog && GameState.SharedInstance.NumberOfStarterPackDialogsShown < GameServerSettings.SharedInstance.SurferSettings.NumberOfTimesStarterPackPopupIsShown)
            {
                if(!GameState.SharedInstance.HasBoughtStarterPack)
                {
                    starterPackDialogTimer.InitializeTimer(starterPackDialogTimer.GetTimeInSeconds());
                    starterPackDialog.ShowStarterPackDialog();

                    if(isShowingDialog)
                    {
                        isShowingDialog = false;
                        GameState.SharedInstance.AddNumberOfStarterPackDialogsShown();
                    }
                }
            }
            else if(starterPackDialogTimer.TimeStatus && GameState.SharedInstance.NumberOfStarterPackDialogsShown > 0)
            {
                starterPackDialog.HideStarterPackDialog();
				InitializeTimerBetweenDialogPopups(GameServerSettings.SharedInstance.SurferSettings.StarterPackShowBetweenMins);
            }
        }
    }

    public bool CheckIfInMainMenu()
    {
        Scene scene = SceneManager.GetActiveScene();

        if(scene.name == "SceneMainMenu")
        {
            return true;
        }
        else 
            return false;
    }

    public void InitializeTimerBetweenDialogPopups(int minutes)
    {
        openDateTime = DateTime.UtcNow.AddMinutes(minutes);
        GameState.SharedInstance.StarterPackPopupTimeStamp = openDateTime.ToBinary().ToString();
        GameState.SharedInstance.PopUpDialogTimeIsUp = false;
    }

    public DateTime GetOldTime()
    {
        long temp = Convert.ToInt64(GameState.SharedInstance.StarterPackPopupTimeStamp);
        openDateTime = DateTime.FromBinary(temp);

        return openDateTime;
    }

    public void InitialiseStarterPacks()
    {
        starterPack1.InitialiseStarterPack(IAPManager.StarterPackProductId6);
    }

    public void HideContactingServerOverlay()
    {
        Debug.Log("HideContactingServerOverlay");
        showingContactingServerOverlay = false;
        showingContactingServerOverlayTimeoutSecs = -1f;
        contactingServerOverlay.SetActive(false);
    }

    public void ShowContactingServerOverlay()
    {
        showingContactingServerOverlay = true;
        showingContactingServerOverlayTimeoutSecs = 15f;
        contactingServerOverlay.SetActive(true);
    }
   
    public void IAPManagerPurchaseSuccessfulHandler(string productId)
    {
        GameState.SharedInstance.HasBoughtStarterPack = true;
        HideContactingServerOverlay();

        if(productId.ToLowerInvariant() != IAPManager.StarterPackProductId6.ToLowerInvariant())
        {
            return;
        }

        if(GameState.SharedInstance.HasBoughtStarterPack == true)
        {
            GlobalSettings.PlaySound(cashRegisterSound);
            PostStarterPackPurchaseUpdate(productId);
        }
    }

    public void IAPManagerPurchaseFailedHandler(string errorMsg)
    {
        HideContactingServerOverlay();
    }

    public void IAPManagerPurchaseCancelledHandler(string msg)
    {
        HideContactingServerOverlay();
    }

    public void InitialiseIAPManager()
    {
        if(iapManager == null)
        {
            iapManager = FindObjectOfType<IAPManager>();

            if(iapManager != null)
            {
                iapManager.OnPurchaseSuccessful += IAPManagerPurchaseSuccessfulHandler;
                iapManager.OnPurchaseFailed += IAPManagerPurchaseFailedHandler;
                iapManager.OnPurchaseCancelled += IAPManagerPurchaseCancelledHandler;
            }
        }
    }

    public void BuyStarterPack(string starterPackID)
    {        
        GlobalSettings.PlaySound(clickSound);

        #if UNITY_EDITOR    

        PostStarterPackPurchaseUpdate(starterPackID);

        GameState.SharedInstance.HasBoughtStarterPack = true;

        starterPackDialog.ShowStarterPackPurchasedPanel();

        #else
        if(iapManager != null)
        {
            ShowContactingServerOverlay();           
            iapManager.PurchaseProduct(starterPackID);
        }
        #endif
    }

    public void RefreshCoinAndEnergyBars()
    {
        mainMenuController.UpdateEnergyDrinkBar(GameState.SharedInstance.EnergyDrink);
        mainMenuController.UpdateCoinBar(GameState.SharedInstance.Cash);
    }

    public void PostStarterPackPurchaseUpdate(string starterPackID)
    {
        GameState.SharedInstance.AddEnergyDrink(amountOfEnergyDrinks);
        GameState.SharedInstance.AddCash(amountOfCoins);
        RefreshCoinAndEnergyBars();

        mainMenuController.DisableJeepTimer();

        UpgradeItem mullet = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.HeadMullet);
        mullet.UpgradeToNextStage();

        if(mullet.IsCosmeticItem())
        {
            GameState.SharedInstance.CurrentCosmetic = mullet.CosmeticItem.Value;
        }            

        UpgradeItem fuel = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepFuel);
        fuel.UpgradeToNextStage();

		if (mainMenuController != null)
			mainMenuController.UpdateLimitedTimeOffer();

		ServerSavedGameStateSync.Sync();
    }

    void OnDisable()
    {
        if(iapManager != null)
        {
            iapManager.OnPurchaseSuccessful -= IAPManagerPurchaseSuccessfulHandler;
            iapManager.OnPurchaseFailed -= IAPManagerPurchaseFailedHandler;
            iapManager.OnPurchaseCancelled -= IAPManagerPurchaseCancelledHandler;
        }
    }
}
