﻿using UnityEngine;
using System.Collections;

public class DialogTrackingFlags
{
	public static bool EnergyDrinksShopShowing = false;
	public static bool WatchVideosAndWinDialogShowing = false;
	public static bool EnergyDrinkCostDialog = false;


	static bool _JustClosedPopupDialog = false;
	public static void FlagJustClosedPopupDialog()
	{
		_JustClosedPopupDialog = true;
	}

	public static bool CommonDialogShowing()
	{
		if(_JustClosedPopupDialog)
		{
			_JustClosedPopupDialog = false;
			return true;
		}

		if(EnergyDrinksShopShowing || 
			WatchVideosAndWinDialogShowing ||
			EnergyDrinkCostDialog)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Main Menu
	public static bool MainMenuStarterPackShowing = false;
	public static bool MainMenuSettingsMenuShowing = false;
	public static bool MainMenuSettingsLanguageMenuShowing = false;
	public static bool MainMenuSettingsCreditMenuShowing = false;
	public static bool MainMenuTreasureChestShowing = false;

	public static bool MainMenuIgnoreAndroidBackCheck = false;

	public static bool MainMenuDialogsShowing()
	{
		if(CommonDialogShowing() ||
			MainMenuStarterPackShowing ||
			MainMenuSettingsMenuShowing ||
			MainMenuSettingsLanguageMenuShowing ||
			MainMenuSettingsCreditMenuShowing ||
			MainMenuTreasureChestShowing)
		{
			return true;
		}

		return false;
	}


}
