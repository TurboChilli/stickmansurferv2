using UnityEngine;
using System.Collections;

public class Handler : MonoBehaviour {
	public int VertexIndex = 0;


	public Vector3 TargetVertex;

	void Start() {
		TargetVertex = transform.parent.GetComponent<GetVertices>().vertices[VertexIndex];
	}

	void Update() {
		transform.position = transform.parent.position + transform.parent.GetComponent<GetVertices>().vertices[VertexIndex];
	}



	void OnDrawGizmosSelected() {
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(transform.position, 0.1f);
	}
}
