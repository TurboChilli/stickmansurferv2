﻿using UnityEngine;
using System.Collections;

public class LevelLinker : MonoBehaviour {

	//utility object to speed up level loading
	public Transform easyChunks;
	public Transform mediumChunks;
	public Transform hardChunks;

	[Space (10)]

	public Transform slalomEasyChunks;
	public Transform slalomMediumChunks;
	public Transform slalomHardChunks;

	[Space (10)]

	public Transform introChunks;

	[Space (10)]

	public Transform pierChunks;
	public Transform woodChunks;
	public Transform fallingRockChunks;
	public Transform boatChunks;
	public Transform funboxChunks;

	[Space (10)]

	public Transform planeChunks;

	[Space (10)]

	public Transform rewardChunks;
	public Transform superRewardChunks;

}
