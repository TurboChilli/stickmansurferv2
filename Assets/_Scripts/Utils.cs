﻿using UnityEngine;
using System.Collections;

public class Utils
{
	public static bool HasInternet()
	{
		return Application.internetReachability != NetworkReachability.NotReachable;
	}

	public static int GetiOSVersion()
	{
		float osVersion = -1f;
		string versionString = SystemInfo.operatingSystem.Replace("iPhone OS ", "");
		string majorVersionString = versionString;
		if(!string.IsNullOrEmpty(versionString))
		{
			int majorVersionIndex = versionString.IndexOf(".");
			if(majorVersionIndex != -1)
			{
				majorVersionString = versionString.Substring(0, majorVersionIndex);
			}
			else
				majorVersionString = versionString;
		}

		float.TryParse(majorVersionString, out osVersion);
		int version = (int)System.Math.Floor(osVersion);
		return version;
	}

	public static bool IphoneXCheck(Camera camToCheck = null)
	{
		if (camToCheck == null)
			camToCheck = Camera.main;

		if (camToCheck == null)
		{
			Debug.Log("Camera.main null for some silly reason"); 
			return false;
		}

		if (camToCheck.pixelWidth / camToCheck.pixelHeight >= 1.9f)
		{
			return true;	
		}
		return false;
	}
}
