﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;
using Prime31;

public class SettingsDialog : MonoBehaviour {

	public Camera mainCamera;
	public MainMenuController mainMenuController;
	public GameObject dialogContents;
	public GameObject closeButton;
	public AudioSource soundClick;

	public Transform musicCheckmark;
	public Transform soundCheckmark;
	public Transform alertsCheckmark;
	public TextMeshPro alertsText;

    public TextMesh loginDescriptionText;

	public Material checkmarkOnMaterial;
	public Material checkmarkOffMaterial;

	public Transform settingsDialogPanel;
	public Transform languageButton;
	public Transform creditsButton;
	public Transform creditsClose;
	public Transform restorePurchasesButton;
	public Transform loginButton;
    public Transform registerButton;
    public Transform logoutButton;
	public GameObject contactingServerOverlay;

	public TextMeshPro facebookLoginText;

	public LanguageSelectController languageSelectController;

    public LoginRegisterManager loginDialog;

	public GameObject creditsPanel;
	public IAPManager iapManager;

	public delegate void CloseButtonClicked();
	public event CloseButtonClicked OnCloseButtonClicked;

	bool showingContactingServerOverlay;
	float showingContactingServerOverlayTimeoutSecs = -1f;

	bool isShowingCredits = false;

	Vector3 dialogContentsStartPos = Vector3.zero;
	Vector3 dialogContentsEndPos = Vector3.zero;

	float curTimeToMove = -1.0f;
	float timeToMove = 0.15f;
	float delaySetHiddenStateCounter = -1f;

	public enum DialogPanelState
	{
		Hidden,
		MovingIn,
		Visible,
	}

	DialogPanelState state = DialogPanelState.Hidden;
	public DialogPanelState State
	{
		get { return state; }
		private set { state = value; }
	}

	void OnDisable()
	{
		if(iapManager != null)
		{
			iapManager.OnRestorePurchasesFinished -= IAPManagerRestorePurchasesFinishedHandler;
			iapManager.OnRestorePurchasesSuccessful -= IAPManagerRestorePurchasesSuccessfulHandler;
			iapManager.OnRestorePurchasesFailed -= IAPManagerRestorePurchasesFailedHandler;
		}

        if (languageSelectController != null)
        {
            languageSelectController.OnClose -= LanguageDialogClose;
        }
	}

	void Start () 
	{
		HideContactingServerOverlay();

		#if UNITY_ANDROID
		alertsCheckmark.gameObject.SetActive(false);
		alertsText.gameObject.SetActive(false);
		#endif

        if(loginDialog == null)
            loginDialog = FindObjectOfType<LoginRegisterManager>();
        
		if(languageSelectController == null)
			languageSelectController = FindObjectOfType<LanguageSelectController>();
		
		if (mainMenuController == null)
			mainMenuController = FindObjectOfType<MainMenuController>();

		if(iapManager == null)
			iapManager = FindObjectOfType<IAPManager>();

		if(iapManager != null)
		{
			iapManager.OnRestorePurchasesFinished += IAPManagerRestorePurchasesFinishedHandler;
			iapManager.OnRestorePurchasesSuccessful += IAPManagerRestorePurchasesSuccessfulHandler;
			iapManager.OnRestorePurchasesFailed += IAPManagerRestorePurchasesFailedHandler;
		}

        if (languageSelectController != null)
        {
            languageSelectController.OnClose += LanguageDialogClose;
        }
		Vector3 localPos = dialogContents.transform.localPosition;
		dialogContentsEndPos = localPos;
		dialogContentsStartPos = new Vector3(localPos.x, localPos.y - 650f, localPos.z);
		dialogContents.transform.localPosition = dialogContentsStartPos;

		if(GameServerSettings.SharedInstance.SurferSettings.ShowRestorePurchaseButton == false)
		{
			settingsDialogPanel.localPosition = new Vector3(settingsDialogPanel.localPosition.x, -1008f, settingsDialogPanel.localPosition.z);
			settingsDialogPanel.localScale = new Vector3(settingsDialogPanel.localScale.x, 398.797f, settingsDialogPanel.localScale.z);

			restorePurchasesButton.gameObject.SetActive(false);
			creditsButton.localPosition = new Vector3(creditsButton.localPosition.x, -1135.5f, creditsButton.localPosition.z);
		}

		if (Utils.IphoneXCheck())
		{
			transform.localScale *= 0.85f;
		}

		Hide();
 	}

    void LanguageDialogClose()
    {
        Show();
    }

	void CheckAndroidBackButton()
	{
        //if (State != DialogPanelState.Visible)
       //     return;
        
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogTrackingFlags.MainMenuSettingsCreditMenuShowing)
			{
				DialogTrackingFlags.FlagJustClosedPopupDialog();

				HideCreditsPanel();
                Show();
			}
			else if(DialogTrackingFlags.MainMenuSettingsMenuShowing)
			{
				DialogTrackingFlags.FlagJustClosedPopupDialog();

				Hide();
			}
		}
		#endif 
	}

	void Update () 
	{
        //Debug.Log("STATER:" + state);
		CheckAndroidBackButton();

		if(delaySetHiddenStateCounter > 0)
		{
			delaySetHiddenStateCounter -= Time.deltaTime;
			if(delaySetHiddenStateCounter <= 0)
			{
				State = DialogPanelState.Hidden;
			}
		}

		if(showingContactingServerOverlay && showingContactingServerOverlayTimeoutSecs >= 0)
		{
			showingContactingServerOverlayTimeoutSecs -= Time.deltaTime;
			if(showingContactingServerOverlayTimeoutSecs <= 0)
			{
				HideContactingServerOverlay();
			}
		}

		if(showingContactingServerOverlay)
			return;

		if(state == DialogPanelState.MovingIn)
		{
			if (curTimeToMove >= 0)
			{
				curTimeToMove -= Time.deltaTime;
				float lerpAmount = Mathf.Pow(Mathf.Max(0.0f, curTimeToMove / timeToMove), 3.0f);
				dialogContents.transform.localPosition = Vector3.Lerp(dialogContentsStartPos, dialogContentsEndPos, 1.0f - lerpAmount);

				if(curTimeToMove <= 0)
					State = DialogPanelState.Visible;
			}
		}


		if (isShowingCredits)
		{
			if( Input.GetMouseButtonDown(0))
			{
				if(UIHelpers.CheckButtonHit(creditsClose.transform, mainCamera))
				{
					HideCreditsPanel();
					GlobalSettings.PlaySound(soundClick);

					creditsPanel.transform.localPosition = dialogContentsEndPos;

					isShowingCredits = false;

					if(OnCloseButtonClicked != null)
						OnCloseButtonClicked();
					
                    Show();
					//Hide(true);
				}
			}

			if (curTimeToMove >= 0.0f)
			{
				float lerpAmount = Mathf.Pow(Mathf.Max(0.0f, curTimeToMove / timeToMove), 3.0f);
				dialogContents.transform.localPosition = Vector3.Lerp(dialogContentsStartPos, dialogContentsEndPos, lerpAmount);
				creditsPanel.transform.localPosition = Vector3.Lerp(new Vector3(dialogContentsStartPos.x, -dialogContentsStartPos.y, dialogContentsStartPos.z), dialogContentsEndPos, 1.0f - lerpAmount);
				curTimeToMove -= Time.deltaTime;
			}
		}
		else
		{
			if( Input.GetMouseButtonDown(0) && state == DialogPanelState.Visible)
			{
                if (UIHelpers.CheckButtonHit(closeButton.transform, mainCamera))
                {
                    GlobalSettings.PlaySound(soundClick);

                    if (OnCloseButtonClicked != null)
                        OnCloseButtonClicked();
                    mainMenuController.EnableMainMenuDisableClickCoolDown();
                    Hide(true);
                }
                else if (UIHelpers.CheckButtonHit(soundCheckmark, mainCamera))
                {
                    GlobalSettings.soundEnabled = !GlobalSettings.soundEnabled;
                    GlobalSettings.PlaySound(soundClick);

                    RefreshButtonStates();
                }
                else if (UIHelpers.CheckButtonHit(musicCheckmark, mainCamera))
                {
                    GlobalSettings.musicEnabled = !GlobalSettings.musicEnabled;
                    GlobalSettings.PlaySound(soundClick);

                    if (!GlobalSettings.musicEnabled)
                    {
                        if (GlobalSettings.currentMenuMusicIndex == 0)
                        {
                            GlobalSettings.reggaeMusicPlayPosition = mainMenuController.reggaeMusic.time;
                            mainMenuController.reggaeMusic.Stop();
                        }
                        else if (GlobalSettings.currentMenuMusicIndex == 1)
                        {
                            GlobalSettings.miniGameMusicPlayPosition = mainMenuController.inGameMusic.time;
                            mainMenuController.miniGameMusic.Stop();
                        }
                        else if (GlobalSettings.currentMenuMusicIndex == 2)
                        {
                            GlobalSettings.inGameMusicPlayPosition = mainMenuController.inGameMusic.time;
                            mainMenuController.inGameMusic.Stop();
                        }
                        mainMenuController.waveSounds.Stop();
                    }
                    else
                    {
                        if (GlobalSettings.currentMenuMusicIndex == 0)
                        {
                            GlobalSettings.PlayMusicFromTimePoint(mainMenuController.reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
                        }
                        else if (GlobalSettings.currentMenuMusicIndex == 1)
                        {
                            GlobalSettings.PlayMusicFromTimePoint(mainMenuController.miniGameMusic, GlobalSettings.miniGameMusicPlayPosition);
                        }
                        else if (GlobalSettings.currentMenuMusicIndex == 2)
                        {
                            GlobalSettings.PlayMusicFromTimePoint(mainMenuController.inGameMusic, GlobalSettings.inGameMusicPlayPosition);
                        }
                        mainMenuController.waveSounds.Play();
                    }
                    RefreshButtonStates();
                }
                else if (UIHelpers.CheckButtonHit(alertsCheckmark, mainCamera))
                {
					#if !UNITY_ANDROID
                    GlobalSettings.alertNotificationEnabled = !GlobalSettings.alertNotificationEnabled;

					if(GlobalSettings.alertNotificationEnabled == false)
					{
						NotificationManager.ClearBadgeCountAndCancelAllLocalNotifications();
					}

					GlobalSettings.PlaySound(soundClick);

                    RefreshButtonStates();
					#endif
                }
                else if (UIHelpers.CheckButtonHit(languageButton, mainCamera))
                {
                    GlobalSettings.PlaySound(soundClick);

                    languageSelectController.Show(mainCamera);
                }
				/*else if(restorePurchasesButton.gameObject.activeSelf && UIHelpers.CheckButtonHit(restorePurchasesButton, mainCamera))
				{
					GlobalSettings.PlaySound(soundClick);

					if(iapManager != null)
					{
						iapManager.RestorePurchases();
						ShowContactingServerOverlay();
					}
				}*/

                else if (UIHelpers.CheckButtonHit(logoutButton, mainCamera))
                {
                    Hide(true);
                    GlobalSettings.PlaySound(soundClick);
                    // TODO: Check if its a facbeook login or not then logout accordingly

                    //FacebookManager.Logout();
                    ServerSavedGameStateSync.LogOut();
                    Debug.Log("Logout");
					AlertDialogUtil.ShowAlert("SUCCESS", "User logged out", "DONE");
                }
				else if (UIHelpers.CheckButtonHit(loginButton, mainCamera))
                {
                    if (!Utils.HasInternet())
                    {
						AlertDialogUtil.ShowAlert("", "Internet connection required", "OK");
                        return;
                    }
                    loginDialog.Show(false);
                    Hide(true);
                    //Show();

                    GlobalSettings.PlaySound(soundClick);  
                }
                else if (UIHelpers.CheckButtonHit(registerButton, mainCamera))
                {
                    if (!Utils.HasInternet())
                    {
						AlertDialogUtil.ShowAlert("", "Internet connection required", "OK");

                        return;
                    }
                    
                    loginDialog.Show(true);
                    Hide(true);
                    GlobalSettings.PlaySound(soundClick);
                }
				else if(UIHelpers.CheckButtonHit(creditsButton, mainCamera))
				{
					GlobalSettings.PlaySound(soundClick);

					curTimeToMove = timeToMove;
					isShowingCredits = true;

					DialogTrackingFlags.MainMenuSettingsCreditMenuShowing = true;
				}
			}
		}
	}

	void HideCreditsPanel()
	{
		DialogTrackingFlags.MainMenuSettingsCreditMenuShowing = false;

		GlobalSettings.PlaySound(soundClick);

		creditsPanel.transform.position += new Vector3(-9999, 0, 0);

		isShowingCredits = false;

		if(OnCloseButtonClicked != null)
			OnCloseButtonClicked();

		Hide();
	}


	public void Hide(bool delayHiddenStateValue = false)
	{
        Debug.Log("HIDING SETTINGS HERE");
		DialogTrackingFlags.MainMenuSettingsMenuShowing = false;
		if(delayHiddenStateValue)
		{
			delaySetHiddenStateCounter = 0.4f;
		}

		var pos = this.transform.position;
		pos.x = -99999f;
		this.transform.position = pos;

		if(delayHiddenStateValue == false)
			State = DialogPanelState.Hidden;

        TutorialCompletionChecker.CheckMarkTutorialsCompleteAndRedirectToMenu();
	}

	public void Show()
	{
        delaySetHiddenStateCounter = 0;
        Debug.Log("SHOWING SETTINGS HERE");
		DialogTrackingFlags.MainMenuSettingsMenuShowing = true;

		RefreshButtonStates();

		dialogContents.transform.localPosition = dialogContentsStartPos;
		UIHelpers.SetTransformToCenter(this.transform, mainCamera);
		creditsPanel.transform.localPosition = dialogContentsStartPos;


        // TODO: BH REMOVED THIS TO DISABLE DynamoDB Server Login Functionality

        loginButton.gameObject.SetActive(false);
        registerButton.gameObject.SetActive(false);
        logoutButton.gameObject.SetActive(false);
        loginDescriptionText.gameObject.SetActive(false);
        /*
        if (!ServerSavedGameStateSync.IsLoggedIn())
		{
            loginDescriptionText.text = GameLanguage.LocalizedText("LOGIN TO BACKUP AND RESTORE GAME DATA");

			//facebookLoginText.text =  GameLanguage.LocalizedText("LOG IN");
            logoutButton.gameObject.SetActive(false);
            loginButton.gameObject.SetActive(true);
            registerButton.gameObject.SetActive(true);
		}
		else
		{
			//facebookLoginText.text = GameLanguage.LocalizedText("LOG OUT");
            logoutButton.gameObject.SetActive(true);
            if (!string.IsNullOrEmpty(FacebookManager.FacebookUserId))
            {
                
                loginDescriptionText.text = GameLanguage.LocalizedText("Logged in") +  " Facebook";
            }
            else
            {
                // logged in via email
				string loggedInEmailAddress = ServerSavedGameStateSync.LoggedInSavedGameStateDataId;
                
                if (!string.IsNullOrEmpty(loggedInEmailAddress))
                {
                    loginDescriptionText.text = GameLanguage.LocalizedText("Logged in") + ": " + loggedInEmailAddress;
                }
                else
                {
                    loginDescriptionText.text = "";
                }
            }
            loginButton.gameObject.SetActive(false);
            registerButton.gameObject.SetActive(false);
		}
        */

        curTimeToMove = timeToMove;
		State = DialogPanelState.MovingIn;
	}

	void RefreshButtonStates()
	{
		soundCheckmark.GetComponent<Renderer>().material = GlobalSettings.soundEnabled ? checkmarkOnMaterial : checkmarkOffMaterial;
		musicCheckmark.GetComponent<Renderer>().material = GlobalSettings.musicEnabled ? checkmarkOnMaterial : checkmarkOffMaterial;

		#if !UNITY_ANDROID
		alertsCheckmark.GetComponent<Renderer>().material = GlobalSettings.alertNotificationEnabled ? checkmarkOnMaterial : checkmarkOffMaterial;
		#endif
	}

	void HideContactingServerOverlay()
	{
		showingContactingServerOverlay = false;
		showingContactingServerOverlayTimeoutSecs = -1f;
		contactingServerOverlay.SetActive(false);
	}

	void ShowContactingServerOverlay()
	{
		showingContactingServerOverlay = true;
		showingContactingServerOverlayTimeoutSecs = 30;
		contactingServerOverlay.SetActive(true);
	}


	void IAPManagerRestorePurchasesFinishedHandler ()
	{
		HideContactingServerOverlay();
	}

	void IAPManagerRestorePurchasesSuccessfulHandler (string productId)
	{
		HideContactingServerOverlay();
	}

	void IAPManagerRestorePurchasesFailedHandler (string errorMessage)
	{
		HideContactingServerOverlay();
	}

}
