﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCrashFix : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		DontDestroyOnLoad(this.gameObject);

		Application.targetFrameRate = 60;
		#if UNITY_IOS && !UNITY_EDITOR
        	StartCoroutine(ForceAndFixLandscape());
        #else
        	Screen.orientation = ScreenOrientation.LandscapeLeft;
        #endif	
	}

	void OnApplicationPause(bool pauseStatus)
	{
		Application.targetFrameRate = 60;
		#if UNITY_IOS && !UNITY_EDITOR
        	StartCoroutine(ForceAndFixLandscape());
        #else
        	Screen.orientation = ScreenOrientation.LandscapeLeft;
        #endif	
	}

	
	IEnumerator ForceAndFixLandscape()
    {
        ScreenOrientation prev = ScreenOrientation.Portrait;
        for (int i = 0; i < 3; i++)
        {
            Screen.orientation = 
                (prev == ScreenOrientation.Portrait ? ScreenOrientation.LandscapeLeft : ScreenOrientation.Portrait);
            yield return new WaitWhile(() => {
                return prev == Screen.orientation;
            });
            prev = Screen.orientation; 
            yield return new WaitForSeconds(0.25f); //this is an arbitrary wait value -- it may need tweaking for different iPhones!
        }
    }

}
