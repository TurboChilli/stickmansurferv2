﻿using UnityEngine;
using System.Collections;

public class SharkMarker : MonoBehaviour 
{
    Renderer sharkRenderer = null;
    //Renderer warningSignRenderer = null;

    public void TurnOffRenderers()
    {
        if (sharkRenderer == null)
        {
            sharkRenderer = GetComponent<Renderer>();
            //warningSignRenderer = transform.GetChild(0).GetComponent<Renderer>();
        }

        sharkRenderer.enabled = false;
       // warningSignRenderer.enabled = false;
    }
}
