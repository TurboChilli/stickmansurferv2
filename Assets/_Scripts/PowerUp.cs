﻿
public enum PowerUp
{
	CoinMultiplier,
	Plane,
	CoinMagnet,
	ScoreMultiplier,
	Jetski,
	Boat,
    Motorcycle,
	WindSurfBoard,
	None
}

public class PowerUpTitle
{
	public static string Get(PowerUp powerUp)
	{
		switch (powerUp)
		{
			case PowerUp.CoinMultiplier:
				return CoinMultiplierTitle;
			case PowerUp.Plane:
				return PlaneTitle;
			case PowerUp.CoinMagnet:
				return CoinMagnetTitle;
			case PowerUp.ScoreMultiplier:
				return ScoreMultiplierTitle;
			case PowerUp.Jetski:
				return JetskiTitle;
			case PowerUp.Boat:
				return BoatTitle;
            case PowerUp.Motorcycle:
                return MotorcycleTitle;
			case PowerUp.WindSurfBoard:
				return WindSurfBoardTitle;
				
		};
		return string.Empty;
	}

	public const string CoinMultiplierTitle =	"Coin x2";
	public const string ScoreMultiplierTitle = 	"Score x2";
	public const string PlaneTitle 			=	"Plane";
	public const string CoinMagnetTitle =		"Magnet";
	public const string RetryTitle =			"Retry";
	public const string JetskiTitle = 			"Jet Ski";
	public const string BoatTitle =				"Speed Boat";
    public const string MotorcycleTitle =       "Motorcycle";
	public const string WindSurfBoardTitle =    "Windsurfer";
}