﻿using UnityEngine;
using System.Collections;

public class Letter : MonoBehaviour {

    public LetterCharacter CurrentLetter;

    public enum LetterCharacter
    {
        Letter_S,
        Letter_U,
        Letter_R,
        Letter_F 
    }

}
