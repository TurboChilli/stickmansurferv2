﻿using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class Localizer : MonoBehaviour {

	public bool IsNumeric = false;

	SystemLanguage debugLanguage = SystemLanguage.Unknown;
	public SystemLanguage DebugLanguage = SystemLanguage.Unknown;

	public int EnglishFontSize = -1;
	public int SpanishFontSize = -1;
	public int ChineseSimplFontSize = -1;
	public int ChineseTradFontSize = -1;
	public int FrenchFontSize = -1;
	public int JapaneseFontSize = -1;
	public int GermanFontSize = -1;
	public int IndonesianFontSize = -1;
	public int PortugueseFontSize = -1;
	public int ItalianFontSize = -1;
	public int KoreanFontSize = -1;
	public int RussianFontSize = -1;

	string originalText;

	void Start () 
	{
		LoadLocalizedText();	
	}

	public void LoadLocalizedText()
	{
		if(GameLanguage.SelectedLanguage != SystemLanguage.Unknown)
		{
			TextMesh textMesh = GetComponent<TextMesh>();
			if(textMesh != null)
			{
				int forceFontSize = GetForcedFontSize();
				if(forceFontSize > 0 && !IsNumeric)
				{
					textMesh.fontSize = forceFontSize;
				}

				if(string.IsNullOrEmpty(originalText))
					originalText = textMesh.text;

				string localizedText = GameLanguage.LocalizedText(originalText);
				if(!string.IsNullOrEmpty(localizedText))
					textMesh.text = localizedText;
			}
			else
			{
				TMP_Text tmp_Text = GetComponent<TMP_Text>();
				if(tmp_Text != null)
				{
					if(!IsNumeric)
						tmp_Text.font = GameLanguage.NormalFont;

					int forceFontSize = GetForcedFontSize();
					if(forceFontSize > 0 && !IsNumeric)
					{
						tmp_Text.enableAutoSizing = false;
						tmp_Text.fontSize = forceFontSize;
					}

					if(string.IsNullOrEmpty(originalText))
						originalText = tmp_Text.text;
				
					string localizedText = GameLanguage.LocalizedText(originalText);
					if(!string.IsNullOrEmpty(localizedText))
						tmp_Text.text = localizedText;
				}
			}
		}
	}

	int GetForcedFontSize()
	{
		if(GameLanguage.SelectedLanguage == SystemLanguage.English)
			return EnglishFontSize;
		if(GameLanguage.SelectedLanguage == SystemLanguage.Spanish && SpanishFontSize > 0)
			return SpanishFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.ChineseSimplified && ChineseSimplFontSize > 0)
			return ChineseSimplFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.ChineseTraditional && ChineseTradFontSize > 0)
			return ChineseTradFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.French && FrenchFontSize > 0)
			return FrenchFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Japanese && JapaneseFontSize > 0)
			return JapaneseFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.German && GermanFontSize > 0)
			return GermanFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Indonesian && IndonesianFontSize > 0)
			return IndonesianFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Portuguese && PortugueseFontSize > 0)
			return PortugueseFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Italian && ItalianFontSize > 0)
			return ItalianFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Korean && KoreanFontSize > 0)
			return KoreanFontSize;
		else if(GameLanguage.SelectedLanguage == SystemLanguage.Russian && RussianFontSize > 0)
			return RussianFontSize;
				
		return -1;
	}

	#if UNITY_EDITOR

	void Update()
	{
		if(DebugLanguage != SystemLanguage.Unknown && DebugLanguage != debugLanguage)
		{
			debugLanguage = DebugLanguage;
			GameLanguage.SelectedLanguage = DebugLanguage;
			GameLanguage.Initialise();
			LoadLocalizedText();
		}
	}
	#endif
}
