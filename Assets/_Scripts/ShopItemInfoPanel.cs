﻿using UnityEngine;
using System.Collections;

public class ShopItemInfoPanel : MonoBehaviour {

    public GameObject shopInfoPopupPanel;
    public GameObject buyItButton;
    public GameObject itemInfoCloseButton;
    public TextMesh purchaseItemName;
    public TextMesh purchaseItemDescription;
    public AnimatingObject panelForeground;



    public void SetPurchaseItemName(string itemName)
    {
        purchaseItemName.text = itemName;
    }



    public void SetPurchaseItemDescription(string itemDescription)
    {
        purchaseItemDescription.text = itemDescription;
    }



    public void ShowHidePopupItemInfoPanel(GameObject theDialogPanel, bool state)
    {        
        theDialogPanel.SetActive(state);
    }
     


    public bool IsInfoPanelBuyItButtonClicked()
    {
        bool result = false;
        if (UIHelpers.CheckButtonHit(buyItButton.transform)) 
        {
            ShowHidePopupItemInfoPanel(shopInfoPopupPanel, false);
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }



    public bool IsInfoPanelCloseButtonClicked()
    {
        bool result = false;
        if (UIHelpers.CheckButtonHit(itemInfoCloseButton.transform)) 
        {
            ShowHidePopupItemInfoPanel(shopInfoPopupPanel, false);
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }
}
