﻿using UnityEngine;
using System.Collections;

public class GlowEffect : MonoBehaviour 
{
	private Vector3 origScale;
	private Quaternion origRotation;

	private Renderer attachedRenderer;
	public ParticleSystem particles;

	private float scaleMultiplier = 1.1f;
	private float scaleFrequency = 2.0f;

	private float angleJiggle = 20.0f;
	private float angleFrequency = 1.0f;

	void Start()
	{
		origRotation = transform.rotation;
		origScale = transform.localScale;

		attachedRenderer = GetComponent<Renderer>();
	}

	void Update () 
	{
		float lerpValue = (Mathf.Sin(Time.time * scaleFrequency) + 1.0f) * 0.5f;
		transform.localScale = Vector3.Lerp(origScale, origScale * scaleMultiplier, lerpValue);

		lerpValue = Mathf.Sin(Time.time * angleFrequency) * angleJiggle;
		transform.rotation = Quaternion.AngleAxis(lerpValue, Vector3.forward) * origRotation;
	}
}
