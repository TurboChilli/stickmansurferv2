﻿using UnityEngine;
using System.Collections;
using TMPro;

public class GoalUIManager : MonoBehaviour 
{
	public GameMenuButtons gameMenuButtons;
	public LevelGenerator levelGen; 

	public GameObject[] goalPanels;
	public TextMeshPro[] goalDescriptions;
	public TextMesh[] energyDrinkCosts;
	public GameObject[] energyDrinkButtons;
	public GameObject[] completedObjects;
	public EnergyDrinkShopController energyDrinkShopController;

	public AudioSource goalCompleteAudio;
	public AudioSource allGoalCompleteAudio;
    public AudioSource kachingSound;
    public AudioSource energyDrinkUse;

	public GameObject resumeButtonParent = null;
	public GameObject shackButtonParent = null;

	private bool isInitialised = false;

	public GameObject goalGamePopup;
	public TextMeshPro goalGamePopupText;
	public bool popupActive = false;
	private bool showingPopup = false;
	private Vector3 activePopupPosition;
	private Vector3 inactivePopupPosition;
	private float timeToMovePopup = 0.5f;
	private float curTimeToMovePopup = 0.0f;

	private float timeToShowPopup = 2.0f;
	private float curTimeToShowPopup = 0.0f;

	public NumericDisplayBar energyDrinkBar;

	private bool movingPanel = false;
	private bool movingDescriptions = false;
	private Vector3 activePanelPosition;
	private Vector3 inactivePanelPosition;
	public bool isShowing = false;
	private float timeToMovePanel = 1.0f;
	private float curTimeToMovePanel = 0.0f;

	private Vector3[] activeGoalDescPositions;
	private Vector3[] inactiveGoalDescPositions;
	private bool isShowingGoalDescription = false;
	private float[] timeToMoveGoalDesc;
	private float[] curTimeToMoveGoalDesc;

	bool energyDrinkPurchased;
	bool attemptingSkipGoal;
	float autoUseEnergyDrinkDelaySecs = -1f;
	int? autoSkipGoalIndex;

	//private bool waitingOnSURF = false;
	//private bool waitingOnDistanceMilestone = false;

	void OnEnable()
	{
		if(energyDrinkShopController == null)
			energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();

		if (levelGen == null)
			levelGen = FindObjectOfType<LevelGenerator>();

		if(energyDrinkShopController != null)
		{
			energyDrinkShopController.OnClose += EnergyDrinkShopControllerOnClose;
			energyDrinkShopController.OnPurchaseSuccessful += EnergyDrinkShopControllerOnPurchaseSuccessful;
		}
	}

	void OnDisable()
	{
		if(energyDrinkShopController != null)
		{
			energyDrinkShopController.OnClose -= EnergyDrinkShopControllerOnClose;
			energyDrinkShopController.OnPurchaseSuccessful -= EnergyDrinkShopControllerOnPurchaseSuccessful;
		}
	}

	void Initialise()
	{
		if (!isInitialised)
		{
			UIHelpers.SetTransformToCenter(transform, Camera.main);
			activePanelPosition = transform.localPosition;
			inactivePanelPosition = activePanelPosition + Vector3.right * 20.0f;
			transform.localPosition = inactivePanelPosition;

			activeGoalDescPositions = new Vector3[3];
			inactiveGoalDescPositions = new Vector3[3];
			timeToMoveGoalDesc = new float[3];
			curTimeToMoveGoalDesc = new float[3];

			Goal[] curGoals = GoalManager.SharedInstance.goals;

			for (int i = 0; i < 3; i++)
			{
				activeGoalDescPositions[i] = goalPanels[i].transform.localPosition;
				inactiveGoalDescPositions[i] = activeGoalDescPositions[i] + Vector3.right * 10.0f;
				goalPanels[i].transform.localPosition = inactiveGoalDescPositions[i];

				if (curGoals[i].complete)
				{
					energyDrinkButtons[i].SetActive(false);
					completedObjects[i].SetActive(true);
				}
				else
				{
					energyDrinkButtons[i].SetActive(true);
					completedObjects[i].SetActive(false);

					energyDrinkCosts[i].text = (GoalManager.SharedInstance.GetEnergyDrinkCost()).ToString();
				}

				goalDescriptions[i].text = GoalText.Get(GoalManager.SharedInstance.goals[i]);

				timeToMoveGoalDesc[i] = 0.0f;
				curTimeToMoveGoalDesc[i] = 0.0f;
			}
				

			if(UIHelpers.IsIPadRes())
			{
				UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 3.2f, 0.62f, goalGamePopup.transform, Camera.main);
				Vector3 goalLocalScale = goalGamePopup.transform.localScale;
				goalGamePopup.transform.localScale = new Vector3(goalLocalScale.x * 0.85f, goalLocalScale.y * 0.85f, goalLocalScale.z);
			}
			else
			{
				UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 4f, 0.73f, goalGamePopup.transform, Camera.main);
			}

			activePopupPosition = goalGamePopup.transform.localPosition;
			inactivePopupPosition = activePopupPosition + Vector3.up * 20.0f;
			goalGamePopup.transform.localPosition = inactivePopupPosition;

			gameMenuButtons.PreGameOnSkipGoalButtonClicked = delegate(int goalIndex) 
			{
				/*#if UNITY_EDITOR
					GoalManager.SharedInstance.goals[goalIndex].complete = true;
					completedObjects[goalIndex].SetActive(true);
					energyDrinkButtons[goalIndex].SetActive(false);

				#else
	             */  
					if (HasEnoughEnergyDrinksToSkipGoal())
	    			{
						UseEnergyDrinkToSkipGoal(goalIndex);
	    			}
	    			else
	    			{
						attemptingSkipGoal = true;
						autoSkipGoalIndex = goalIndex;
						energyDrinkShopController.Show(Camera.main);
	    			}
				//#endif
			};

			isInitialised = true;
		}
	}

	void Start () 
	{
		Initialise();
	}

	bool HasEnoughEnergyDrinksToSkipGoal()
	{
		if(GameState.SharedInstance.EnergyDrink >= GoalManager.SharedInstance.GetEnergyDrinkCost())
			return true;
		else
			return false;
	}

	void UseEnergyDrinkToSkipGoal(int goalIndex)
	{
		if(GoalManager.SharedInstance.goals != null && goalIndex >= 0 && goalIndex < GoalManager.SharedInstance.goals.Length)
		{
			if(HasEnoughEnergyDrinksToSkipGoal())
			{
				GlobalSettings.PlaySound(energyDrinkUse);
				GameState.SharedInstance.LoseEnergyDrink(GoalManager.SharedInstance.GetEnergyDrinkCost());
				energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);

				GoalManager.SharedInstance.goals[goalIndex].complete = true;
				GoalManager.SharedInstance.goals[goalIndex].hasShown = true;

				PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_complete", 1);
				PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_hasShown", 1);


				completedObjects[goalIndex].SetActive(true);
				energyDrinkButtons[goalIndex].SetActive(false);
			}
		}
			
		autoSkipGoalIndex = null;
	}

	public void ShowGoalPanel(bool showShack = true)
	{
		Initialise();

		if (showShack)
		{
			resumeButtonParent.transform.localPosition = new Vector3(0.175f, 0.0f, 0.0f);
			shackButtonParent.SetActive(true);
		}
		else
		{
			resumeButtonParent.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
			shackButtonParent.SetActive(false);
		}

		//gameObject.SetActive(true);

		isShowing = true;
		isShowingGoalDescription = true;

		movingPanel = true;
		movingDescriptions = false;

		curTimeToMovePanel = timeToMovePanel;
		Goal[] curGoals = GoalManager.SharedInstance.goals;

		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);

		for (int i = 0; i < 3; i++)
		{
			timeToMoveGoalDesc[i] = timeToMovePanel * 0.5f + (float)i * 0.1f; 
			curTimeToMoveGoalDesc[i] = timeToMovePanel * 0.5f + (float)i * 0.1f; 

			goalDescriptions[i].text = GoalText.Get(GoalManager.SharedInstance.goals[i]);

			goalPanels[i].transform.localPosition = inactiveGoalDescPositions[i];

			if (curGoals[i].complete)
			{
				energyDrinkButtons[i].SetActive(false);
				completedObjects[i].SetActive(true);
			}
			else
			{
				energyDrinkButtons[i].SetActive(true);
				completedObjects[i].SetActive(false);

				energyDrinkCosts[i].text = (GoalManager.SharedInstance.GetEnergyDrinkCost()).ToString();
			}
		}
	}

	public void HideGoalPanel(bool disablePanel = false)
	{
		Initialise();
		isShowing = false;
		isShowingGoalDescription = false;

		movingPanel = true;
		movingDescriptions = false;

		curTimeToMovePanel = timeToMovePanel;
		if (disablePanel)
		{
			//gameObject.SetActive(false);
		}

	}

	void Update () 
	{
		//gets any waiting goals
		GoalManager.SharedInstance.UpdateGoals();
		Goal[] curGoals = GoalManager.SharedInstance.goals;

		int waitingGoalIndex = -1;
		for (int i = 0; i < 3; i++)
		{
			if (curGoals[i].complete && !curGoals[i].hasShown)
			{
				waitingGoalIndex = i;
				break;
			}
		}

		if (waitingGoalIndex != -1)
		{
			for (int i = 0; i < 3; i++)
			{
				if (GoalManager.SharedInstance.goals[i].complete)
				{
					completedObjects[i].SetActive(true);
					energyDrinkButtons[i].SetActive(false);
					goalDescriptions[i].text = GoalText.Get(GoalManager.SharedInstance.goals[i]);
				}
			}
		}

		if(autoUseEnergyDrinkDelaySecs >= 0)
		{
			autoUseEnergyDrinkDelaySecs -= Time.deltaTime;
			if(autoUseEnergyDrinkDelaySecs <= 0)
			{
				if(attemptingSkipGoal)
				{
					attemptingSkipGoal = false;

					if(autoSkipGoalIndex.HasValue)
						UseEnergyDrinkToSkipGoal(autoSkipGoalIndex.Value);
				}
			}
		}

		if (!showingPopup && curTimeToMovePopup <= 0.0f)
		{
			if (waitingGoalIndex != -1)
			{
				GlobalSettings.PlaySound(goalCompleteAudio);

				GoalManager.SharedInstance.goals[waitingGoalIndex].hasShown = true;
				PlayerPrefs.SetInt("GM_Goal" + waitingGoalIndex + "_hasShown", 1);

				showingPopup = true;
				popupActive = true;

				//waitingOnSURF = levelGen.IsShowingLetterPopup();
				//waitingOnDistanceMilestone = levelGen.IsShowingDistancePopup();

				curTimeToMovePopup = timeToMovePopup;
				goalGamePopupText.text = GoalText.Get(GoalManager.SharedInstance.goals[waitingGoalIndex]);
			}
		}

		if (movingPanel)
		{
			if (curTimeToMovePanel > 0)
			{
				float lerpAmount = Mathf.Pow(curTimeToMovePanel / timeToMovePanel, 3.0f);

				curTimeToMovePanel -= Time.deltaTime;
				if (curTimeToMovePanel <= 0)
					lerpAmount = 0;
				
				if (isShowing)
				{
					transform.localPosition = Vector3.Lerp(inactivePanelPosition, activePanelPosition, 1.0f - lerpAmount);

					if (lerpAmount < 0.25f)
						movingDescriptions = true;
					
					if (lerpAmount == 0.0f)
					{
						movingPanel = false;
					}
				}
				else
				{
					transform.localPosition = Vector3.Lerp(inactivePanelPosition, activePanelPosition, lerpAmount);
					if (lerpAmount == 0)
					{
						movingPanel = false;					
					}
				}
			}
		}

		if (movingDescriptions)
		{
			bool[] isDoneMovingChecks = {false, false, false};
			for (int i = 0; i < 3; i++)
			{
				float lerpAmount = Mathf.Pow(curTimeToMoveGoalDesc[i] / timeToMoveGoalDesc[i], 3.0f);

				curTimeToMoveGoalDesc[i] -= Time.deltaTime;
				if (curTimeToMoveGoalDesc[i] <= 0)
				{
					lerpAmount = 0;
					isDoneMovingChecks[i] = true;
				}

				if (isShowingGoalDescription)
					goalPanels[i].transform.localPosition = Vector3.Lerp(inactiveGoalDescPositions[i], activeGoalDescPositions[i], 1.0f - lerpAmount);
				else
					goalPanels[i].transform.localPosition = Vector3.Lerp(inactiveGoalDescPositions[i], activeGoalDescPositions[i], lerpAmount);
			}

			if (isDoneMovingChecks[0] && 
				isDoneMovingChecks[1] && 
				isDoneMovingChecks[2])
			{
				movingDescriptions = false;
			}
		}

		if (popupActive)
		{
			//if (! (waitingOnSURF || waitingOnDistanceMilestone))
			//{	
				if (curTimeToMovePopup > 0)
				{
					float lerpAmount = Mathf.Pow(curTimeToMovePopup / timeToMovePopup, 3.0f);

					curTimeToMovePopup -= Time.deltaTime;
					if (curTimeToMovePopup <= 0)
						lerpAmount = 0;

					if (showingPopup)
					{
						goalGamePopup.transform.localPosition = Vector3.Lerp(inactivePopupPosition, activePopupPosition , 1.0f - lerpAmount);
						if (lerpAmount == 0)
							curTimeToShowPopup = timeToShowPopup;
					}
					else
					{
						goalGamePopup.transform.localPosition = Vector3.Lerp(inactivePopupPosition, activePopupPosition , lerpAmount);
						if (lerpAmount == 0)
							popupActive = false;
					}
				}
				else
				{
					if (curTimeToShowPopup <= 0)
					{	
						showingPopup = false;
						curTimeToMovePopup = timeToMovePopup;
					}
					curTimeToShowPopup -= Time.deltaTime;
				}
			/*}
			else
			{
				if (waitingOnSURF)
					waitingOnSURF = levelGen.IsShowingLetterPopup();

				if (waitingOnDistanceMilestone)
					waitingOnDistanceMilestone = levelGen.IsShowingDistancePopup();
			}*/
		}
	}

	public bool IsShowingGoalPopup()
	{
		return (showingPopup || curTimeToMovePopup > 0);
	}

	void EnergyDrinkShopControllerOnPurchaseSuccessful (int energyDrinkAmount)
	{
		energyDrinkPurchased = true;
		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
	}

	void EnergyDrinkShopControllerOnClose ()
	{
		energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);

		if(energyDrinkPurchased)
		{
			energyDrinkPurchased = false;

			if(attemptingSkipGoal)
			{
				if(HasEnoughEnergyDrinksToSkipGoal())
				{
					autoUseEnergyDrinkDelaySecs = 0.3f;
				}
				else
					attemptingSkipGoal = false;
			}
		}
	}

}
