﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class GameUI : MonoBehaviour 
{
	public GameMenuButtons gameMenuButtons;
	public PowerupManager powerUpManager;
	public PlayerControl playerControl;
	public GoalUIManager goalUIManager;
	public CoinBar coinBar = null;
	public ScoreBar scoreBar = null;
	public Camera mainCam = null;
	public Camera UICam = null;
	public GameCenterPluginManager gcpm = null;
	public SupersonicManager supersonicManager = null;

	public GameObject highScorePanel = null;
	public TextMesh highScoreText = null;
	Vector3 highScoreTextLocalScale = Vector3.zero;
	public GameObject highScoreWinText = null;

	public GameObject superRewardBannerObject = null;
	public TextMeshPro superRewardText = null;
	public AnimatingObject superRewardIcon = null;

	public AudioSource soundCoin;
	public AudioSource highScoreSound;
	public AudioSource kachingSound;
	public AudioSource energyDrinkUseSound;
    public AudioSource swooshSound;
	public AudioSource goalsAllCompleteSound;
    public AudioSource levelUpSound;

	public EnergyDrinkShopController energyDrinkShopController = null;

	public SpotlightEffect highlightScreen;

	[Header("Post Game UI")]
	public GameObject postGameUIPanel = null;
	public Transform endGameOverlay = null;
    public Transform endGameDarkScreen = null;
	public Transform endGameButtons = null;

	private Vector3 endGameButtonLocalPositionActive;
	private Vector3 endGameButtonLocalPositionInactive;
	private float timeToMoveEndGameButtons = 0.5f;
	private float curTimeToMoveEndGameButtons = 0.0f;

	public TextMeshPro endGameTitle = null;
	public TextMesh endGameCoins = null;
	Vector3 endGameCoinsLocalScale = Vector3.zero;
	public TextMesh endGameScore = null;
	public TextMeshPro endGameMutiplier = null;
	public GameObject endGameCoinBar;
	public GameObject endGameHighScoreText = null;
	public GameObject retryPanel = null;
	public ParticleSystem doubleCoinBurst;

	private Vector3 activePanelPosition;
	private Vector3 inactivePanelPosition;
	private Vector3 altInactivePanelPosition;
	private float timeToPanelMove = 0.5f;
	private float curTimeToPanel = 0.0f;

	public GameObject[] goalPanels;
	public TextMeshPro[] goalPanelDescriptions;
	public GameObject[] goalPanelCompletes;
	public TextMesh[] goalSkipGoalCosts;
	public GameObject[] goalPanelButtons;
	public GameObject goalGoalCompleteText;
	public TextMeshPro coinRewardText;
	public ParticleSystem coinParticle;
	public ParticleSystem coinLevelUpParticleEffect;

    public GameObject retryEnergyDrinkButton = null;
    public GameObject watchVideoRetryButton = null;
	public GameObject credLevel;
	public TextMeshPro credText;

	public TextMesh doubleCoinEnergyDrinkCost;

	private Vector3[] activeGoalPosition;
	private Vector3[] inactiveGoalPosition;
	private Vector3[] altInactiveGoalPosition;

	private Vector3 inactiveGoalCompletePosition;
	private Vector3 activeGoalCompletePosition;
	private float timeToShowGoalCompleteText = 1f;
	private float curTimeToShowGoalCompleteText = 0.0f;
	private float timeToShowCredCompleteText = 1f;
	private float curTimeToShowCredCompleteText= 0.0f;

	private float timeToPauseCred = 0.5f;
	private float curTimeToPauseCred = 0.0f;
	private Vector3 origCredScale;

	private bool movingInGoalComplete = false;
	private bool isShowingCredLevel = false;

	private float baseTimeToMoveGoalPanels = 0.35f;
	private float[] timeToMoveGoalPanel;
	private float[] curTimeToMoveGoalPanel;

	public GameObject pauseButton = null;

	public TMPro.TMP_Text curEnergyDrinkTextMain = null;
	public NumericDisplayBar retryEnergyDrinkBar = null;
	public TextMesh retryEnergyDrinkCost = null;

	private bool isShowing = false;
	private float timeToMove = 0.5f;
	private float curTimeToMove = 0.0f;

	private float coinScaleCountPerSecond = 1.0f;
	private float scoreScaleCountPerSecond = 1.0f;

	private float multiplierCountPerSecond = 20.0f;
	private float coinCountPerSecond = 30.0f;
	private float scoreCountPerSecond = 5000.0f;

	private string mutiplierPrefix = "lvl";
	private int finalCoins = 0;
	private int finalMultiplier = 0;
	private int finalScore = 0;

	private float animFinalCoins = 0;
	private float animFinalMultiplier = 0;
	private float animFinalScore = 0;

	private int highScore = 500;
	private Vector3 activeHighScorePanel;
	private Vector3 inactiveHighScorePanel;

	private float highScoreAppearDelay = 1.0f;
	private float curhighScoreAppearDelay = 1.0f;

	public FillBar retryFillBar = null;
	private float curRetryTime = 0.0f;
	private float retryTime = 3.5f;

	private float timeToMoveHighScorePanel = 1.0f;
	private float curTimeToMoveHighScorePanel = 0.0f;
	private float timeToShowHighScorePanel = 3.0f;
	private bool isShowingHighScorePanel = false;
	private bool hasShownInitialHighScorePanel = false;
	private bool hasShownFinalHighScorePanel = false;

	private Vector3 activeHighScoreTextPosition;
	private Vector3 inactiveHighScoreTextPositionLeft;
	private Vector3 inactiveHighScoreTextPositionRight;
	private float curTimeToMoveHighScoreWin = 0.0f;
	private float timeToMoveHighScoreWin = 3.0f;
	private bool isShowingHighScoreWinText = false;
	private bool waitingOnSuperReward = false;

	private Vector3 activeSuperRewardTextPosition;
	private Vector3 inactiveSuperRewardTextPositionLeft;
	private Vector3 inactiveSuperRewardTextPositionRight;
	private float curTimeToMoveSuperReward = 0.0f;
	private float timeToMoveSuperReward = 3.0f;
	private bool isShowingSuperRewardText = false;
	private bool waitingOnHighscore = false;

	private EndUIState curState;
	private bool isMovingRetryPanel = false;
	public bool isRetryBarLocked = false;
	bool attemptingRetry = false;
    bool attemptingVideoRetry = false;
	bool energyDrinkPurchased;
	float autoUseEnergyDrinkDelaySecs = -1f;

	bool attemptingDoubleCoins;
	bool pressedDoubleCoins = false;

	bool attemptingSkipGoal;
	int? autoSkipGoalIndex;

	public bool GameUISyncSavedGameData = false;

	void OnEnable()
	{
		if(energyDrinkShopController == null)
			energyDrinkShopController = FindObjectOfType<EnergyDrinkShopController>();


		if(energyDrinkShopController != null)
		{
			energyDrinkShopController.OnClose += EnergyDrinkShopControllerOnClose;
			energyDrinkShopController.OnPurchaseSuccessful += EnergyDrinkShopControllerOnPurchaseSuccessful;
		}
	}

	void OnDisable()
	{
		if(energyDrinkShopController != null)
		{
			energyDrinkShopController.OnClose -= EnergyDrinkShopControllerOnClose;
			energyDrinkShopController.OnPurchaseSuccessful -= EnergyDrinkShopControllerOnPurchaseSuccessful;
		}
	}


	void Start () 
	{
		GameUISyncSavedGameData = false;
		endGameCoinsLocalScale = endGameCoins.transform.localScale;
		highScoreTextLocalScale = highScoreText.transform.localScale;
		
		if (coinBar == null)
			coinBar = FindObjectOfType<CoinBar>();
		if (mainCam == null)
			mainCam = Camera.main;
		if (powerUpManager == null)
			powerUpManager = FindObjectOfType<PowerupManager>();
		if (playerControl == null)
			playerControl = FindObjectOfType<PlayerControl>();
		if (gcpm == null)
			gcpm = FindObjectOfType<GameCenterPluginManager>();
		//activePositionBottomSlice = PostGameUIPanel.transform.localPosition;

		if(supersonicManager == null)
		{
	        supersonicManager = FindObjectOfType<SupersonicManager>();
	        supersonicManager.Initialise();
	        supersonicManager.LoadInterstisial();
		}


		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 1.4f, 0.55f, coinBar.transform, mainCam);
		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 4.3f, 0.6f, scoreBar.transform, mainCam);

		UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 0.8f, 1.8f, highScorePanel.transform, mainCam);
		UIHelpers.SetTransformRelativeToCenter( 0f, 0f, highScoreWinText.transform, mainCam);

		if(UIHelpers.IsIPadRes())
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 0.6f, 0.6f, pauseButton.transform, mainCam);
			Vector3 pauseLocalScale = pauseButton.transform.localScale;
			pauseButton.transform.localScale = new Vector3(pauseLocalScale.x * 0.85f, pauseLocalScale.y * 0.85f, pauseLocalScale.z);
		}
		else
		{
			UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 0.8f, 0.75f, pauseButton.transform, mainCam);
		}

        //UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 1.75f, 0.75f, gameMenuButtons.goButtonEnabled.transform, mainCam);

		WireUpMainMenuButtons(mainCam);

		activeHighScorePanel = highScorePanel.transform.localPosition;
		inactiveHighScorePanel = activeHighScorePanel + Vector3.right * 2.0f;
		highScorePanel.transform.localPosition = inactiveHighScorePanel;

		//high score
		activeHighScoreTextPosition = new Vector3( highScoreWinText.transform.localPosition.x, highScorePanel.transform.localPosition.y,  highScoreWinText.transform.localPosition.z);
		inactiveHighScoreTextPositionLeft = activeHighScoreTextPosition + Vector3.left * 20.0f;
		inactiveHighScoreTextPositionRight = activeHighScoreTextPosition + Vector3.right * 20.0f;
		highScoreWinText.transform.localPosition = inactiveHighScoreTextPositionRight;

		//super reward
		inactiveSuperRewardTextPositionLeft = inactiveHighScoreTextPositionLeft;
		inactiveSuperRewardTextPositionRight = inactiveHighScoreTextPositionRight;
		activeSuperRewardTextPosition = activeHighScoreTextPosition;
		superRewardBannerObject.transform.position = inactiveSuperRewardTextPositionLeft;

		activePanelPosition = new Vector3(0,0, postGameUIPanel.transform.localPosition.z);
		inactivePanelPosition = activePanelPosition + Vector3.up * 20.0f;
		altInactivePanelPosition = activePanelPosition + Vector3.right * 20.0f;

        postGameUIPanel.transform.localPosition = activePanelPosition;
        if(gameMenuButtons == null)
            gameMenuButtons = FindObjectOfType<GameMenuButtons>();

        gameMenuButtons.AlignButtonsToEdge(mainCam);
		postGameUIPanel.transform.localPosition = inactivePanelPosition;

		coinBar.DisplayCoins( 0, false); 

		curhighScoreAppearDelay = highScoreAppearDelay;

		highScore = GameState.SharedInstance.Score;
		SetHighScoreText(string.Format("{0:#,0}", highScore));

		activeGoalPosition = new Vector3[3];
		inactiveGoalPosition = new Vector3[3];
		altInactiveGoalPosition = new Vector3[3];
		timeToMoveGoalPanel = new float[3];
		curTimeToMoveGoalPanel = new float[3];

		for (int i = 0; i < 3; i++)
		{
			activeGoalPosition[i] = goalPanels[i].transform.localPosition;
			inactiveGoalPosition[i] = activeGoalPosition[i] + Vector3.right * 5000.0f;
			altInactiveGoalPosition[i] = activeGoalPosition[i] + Vector3.left * 5000.0f;

			goalPanels[i].transform.localPosition = inactiveGoalPosition[i];
			timeToMoveGoalPanel[i] = baseTimeToMoveGoalPanels + i * 0.05f;
		}

		if(GlobalSettings.TutorialCompleted())
			ShowEndGameDarkScreen();

		activeGoalCompletePosition = goalGoalCompleteText.transform.localPosition;
		inactiveGoalCompletePosition = activeGoalCompletePosition + Vector3.right * 5000.0f;
		goalGoalCompleteText.transform.localPosition = inactiveGoalCompletePosition;

		HideDarkScreenSpotlight();

		doubleCoinBurst.gameObject.SetActive(false);

		//for sanity
		energyDrinkShopController.Hide();

		coinLevelUpParticleEffect.gameObject.SetActive(false);
	}

	float lastResizeTextScale = 1.0f;
	void SetHighScoreText(string value)
	{
		float scale = GlobalSettings.ScaleTextAfterLength(value, 8);
		if(lastResizeTextScale != scale)
		{
			highScoreText.transform.localScale = new Vector3(highScoreTextLocalScale.x * scale, highScoreTextLocalScale.y * scale, highScoreTextLocalScale.z);
			lastResizeTextScale = scale;
		}

		highScoreText.text = value;
	}
		
	public void ResetCoinAndScoreBar()
	{
		scoreBar.Reset();
		coinBar.Reset();
	}

	public void ResetHighscore()
	{
		highScore = GameState.SharedInstance.Score;
		SetHighScoreText(string.Format("{0:#,0}", highScore));
	}

	void WireUpMainMenuButtons(Camera toCamera)
	{
		if(gameMenuButtons == null)
			gameMenuButtons = FindObjectOfType<GameMenuButtons>();

		gameMenuButtons.Initialise(toCamera, false);

		gameMenuButtons.OnGoButtonClicked = delegate() 
		{
			//SceneNavigator.NavigateTo(SceneType.SurfMain);
			playerControl.RestartGame();
			ResetCoinAndScoreBar();

			for (int i = 0; i < 3; i++)
			{
				goalPanels[i].transform.localPosition = inactiveGoalPosition[i];
			}

			HideEndSequence();

			isShowing = false;
			curState = EndUIState.NONE;
			endGameDarkScreen.gameObject.SetActive(false);

			postGameUIPanel.transform.localPosition = inactivePanelPosition;

			pressedDoubleCoins = false;
		};

        gameMenuButtons.OnFacebookButtonClicked = delegate()
        {
           string scoreText = "" + (int)(this.scoreBar.GetScore());
           string coinText = this.coinBar.coinText.text;

           string facebookShareMessage = "I just scored " + scoreText + " points and earned " + coinText + " coins on Stickman Surfer!";
		   FacebookManager.SharePost(facebookShareMessage);
           Debug.Log("FB SHARE MSG:" + facebookShareMessage);
        };

        gameMenuButtons.OnWatchVideoRetryButtonClicked = delegate ()
        {
            supersonicManager.OnRewardedVideoFinish = null;
            supersonicManager.OnRewardedVideoFinish = this.WatchVideoRetryRewarded;



            supersonicManager.OnRewardedVideoStarted = null;
            supersonicManager.OnRewardedVideoStarted = RewardedVidStarted;

            attemptingVideoRetry = true;
            isRetryBarLocked = true;

            supersonicManager.ShowRewardedVideo();
            /*#if UNITY_EDITOR
                playerControl.Reset();
                HideEndSequence();
                HideEndGameDarkScreen();
                GlobalSettings.surfGamePaused = false;
            #else*/
            /*
            if (HasEnoughEnergyDrinkForRetry())
            {
                UseEnergyDrinkOnRetry();
            }
            else
            {
                attemptingRetry = true;
                isRetryBarLocked = true;
                energyDrinkShopController.Show(mainCam);
                gameMenuButtons.EnableRetryButton(true);
            }*/
            //#endif
        };

        gameMenuButtons.OnRetryButtonClicked = delegate() 
		{
			/*#if UNITY_EDITOR
				playerControl.Reset();
				HideEndSequence();
                HideEndGameDarkScreen();
                GlobalSettings.surfGamePaused = false;
			#else*/

				if (HasEnoughEnergyDrinkForRetry())
    			{
					UseEnergyDrinkOnRetry();
    			}
    			else
    			{
					attemptingRetry = true;
					isRetryBarLocked = true;
					energyDrinkShopController.Show(mainCam);
					gameMenuButtons.EnableRetryButton(true);
    			}
			//#endif
		};

		gameMenuButtons.OnRetrySkipButtonClicked = delegate() {
			curState = EndUIState.MOVING_IN;
			powerUpManager.RemoveAllPowerUps();

			RefreshCurrentEnergyDrinkText();
		};

		gameMenuButtons.OnTreasureButtonClicked = delegate() {
            #if UNITY_ANDROID
            AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCam.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

            if(droidLoader != null)
            {
                droidLoader.Show();
            }
            #endif
			SceneNavigator.NavigateTo(SceneType.Menu);
		};

		gameMenuButtons.OnBuilderButtonClicked = delegate() {
            #if UNITY_ANDROID
            AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCam.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

            if(droidLoader != null)
            {
                droidLoader.Show();
            }
            #endif
			SceneNavigator.NavigateTo(SceneType.Menu);
		};

		gameMenuButtons.OnDoubleCoinsButtonClicked = delegate() 
		{
			if(GameServerSettings.SharedInstance.SurferSettings.EnableDoubleCoinsEnergyDrink)
			{
				if (HasEnoughEnergyDrinkForDoubleCoins())
				{
					UseEnergyDrinkOnDoubleCoins();
					pressedDoubleCoins = true;
				}
				else
				{		
					gameMenuButtons.EnableDoubleCoinButton(true);
					attemptingDoubleCoins = true;
					energyDrinkShopController.Show(mainCam);
				}
			}
		};

		gameMenuButtons.OnDoubleCoinsVideoButtonClicked = delegate() 
		{
			supersonicManager.OnRewardedVideoFinish = null;
			supersonicManager.OnRewardedVideoFinish = DoubleCoins;

            supersonicManager.OnRewardedVideoStarted = null;
            supersonicManager.OnRewardedVideoStarted = RewardedVidStarted;

			

            if(!supersonicManager.ShowRewardedVideo())
            {
                Debug.Log("rewwarded start event returned false here BH1");
            }

			pressedDoubleCoins = true;
		};

		gameMenuButtons.OnPauseButtonClicked = delegate()
		{
			if(Time.timeScale == 0)
				Time.timeScale = 1;

			goalUIManager.ShowGoalPanel();
			ShowEndGameDarkScreen();

			powerUpManager.ToggleFreezePowerUps(true);

			gameMenuButtons.gettingMenuInput = true;
			gameMenuButtons.pauseButton.SetActive(false);
			gameMenuButtons.EnableResumeButton(true);
			gameMenuButtons.RefreshNotificationIcons();
		};

		gameMenuButtons.PostGameOnSkipGoalButtonClicked = delegate(int goalIndex) 
		{
			if (HasEnoughEnergyDrinkForSkipGoal())
			{
				UseEnergyDrinkOnSkipGoal(goalIndex);
			}
			else
			{
				attemptingSkipGoal = true;
				autoSkipGoalIndex = goalIndex;
				energyDrinkShopController.Show(mainCam);
			} 
		};
	}

	bool HasEnoughEnergyDrinkForDoubleCoins()
	{
		if(GameState.SharedInstance.EnergyDrink >= GameState.SharedInstance.DoubleCoinEnergyDrinkCost((int)finalCoins))
			return true;
		else 
			return false;
	}

	void UseEnergyDrinkOnDoubleCoins()
	{
		if(HasEnoughEnergyDrinkForDoubleCoins())
		{
			GameState.SharedInstance.LoseEnergyDrink(GameState.SharedInstance.DoubleCoinEnergyDrinkCost(finalCoins));
			DoubleCoins();
		}
	}

    void RewardedVidStarted()
    {
        // pause audio here
        AudioListener.pause = true;
    }

	void DoubleCoins()
	{
		AudioListener.pause = false;

		GlobalSettings.PlaySound(energyDrinkUseSound);

		if (supersonicManager.OnRewardedVideoFinish != null)	
			supersonicManager.OnRewardedVideoFinish -= DoubleCoins;


		gameMenuButtons.EnableDoubleCoinButton(false);
		gameMenuButtons.EnableDoubleCoinVideoButton(false);

		doubleCoinBurst.time = 0;
		doubleCoinBurst.gameObject.SetActive(true);
		doubleCoinBurst.Play();

		//double ya coins 
		GameState.SharedInstance.AddCash(finalCoins);
		if (curState == EndUIState.COUNTING)
			finalCoins = 2 * finalCoins;
		else
		{
			finalCoins = 2 * finalCoins;
			SetEndGameCoinsText(string.Format("+{0}", (finalCoins)));
		}

        curState = EndUIState.COUNTING;
        coinScaleCountPerSecond = 1.0f;
		RefreshCurrentEnergyDrinkText();
	}

	void RefreshCurrentEnergyDrinkText()
	{
		retryEnergyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink);
		curEnergyDrinkTextMain.text = string.Format("{0:#,0}", GameState.SharedInstance.EnergyDrink);
	}

    void WatchVideoRetryRewarded()
    {
        curState = EndUIState.RETRYING_GAME;
        GlobalSettings.PlaySound(energyDrinkUseSound);
        GameState.SharedInstance.LoseEnergyDrink(RetyEnergyDrinkCost());
        playerControl.Reset();
        playerControl.MakePlayerHorizontalForRestart();
        HideEndGameDarkScreen();
        HideEndSequence();
        isRetryBarLocked = false;
        GameState.SharedInstance.RetryTimes += 1;
       
    }

    float lastCoinTextScale = 1.0f;
	void SetEndGameCoinsText(string value)
	{
		float scale = GlobalSettings.ScaleTextAfterLength(value, 8);
		if(lastCoinTextScale != scale)
		{
			endGameCoins.transform.localScale = new Vector3(endGameCoinsLocalScale.x * scale, endGameCoinsLocalScale.y * scale, endGameCoinsLocalScale.z);
			lastCoinTextScale = scale;
		}


		endGameCoins.text = value;
	}

	bool HasEnoughEnergyDrinkForRetry()
	{
		if(GameState.SharedInstance.EnergyDrink >= RetyEnergyDrinkCost())
			return true;
		else
			return false;
	}

	void UseEnergyDrinkOnRetry()
	{
		if(HasEnoughEnergyDrinkForRetry())
		{
			curState = EndUIState.RETRYING_GAME;
			GlobalSettings.PlaySound(energyDrinkUseSound);
			GameState.SharedInstance.LoseEnergyDrink(RetyEnergyDrinkCost());
			playerControl.Reset();
            playerControl.MakePlayerHorizontalForRestart();
            HideEndGameDarkScreen();
			HideEndSequence();

			GameState.SharedInstance.RetryTimes += 1;

			RefreshCurrentEnergyDrinkText();
		}
	}

	bool HasEnoughEnergyDrinkForSkipGoal()
	{
		if(GameState.SharedInstance.EnergyDrink >= GoalManager.SharedInstance.GetEnergyDrinkCost())
			return true;
		else
			return false;
	}

	void UseEnergyDrinkOnSkipGoal(int goalIndex)
	{
		if(GoalManager.SharedInstance.goals != null && goalIndex >= 0 && goalIndex < GoalManager.SharedInstance.goals.Length)
		{
			if(HasEnoughEnergyDrinkForSkipGoal())
			{
				GlobalSettings.PlaySound(energyDrinkUseSound);
				GameState.SharedInstance.LoseEnergyDrink(GoalManager.SharedInstance.GetEnergyDrinkCost());

				GoalManager.SharedInstance.goals[goalIndex].complete = true;
				GoalManager.SharedInstance.goals[goalIndex].hasShown = true;
				PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_complete", 1);
				PlayerPrefs.SetInt("GM_Goal" + goalIndex + "_hasShown", 1);

				goalPanelCompletes[goalIndex].SetActive(true);
				goalPanelButtons[goalIndex].SetActive(false);

				Goal[] goals = GoalManager.SharedInstance.goals;
				bool goalCheck = true;
				for (int i = 0; i < 3; i++)
				{
					if (!goals[i].complete)
					{
						goalCheck = false;
						break;
					}
				}

				if (goalCheck)
				{
					curState = EndUIState.GOAL_REWARD;
					curTimeToShowGoalCompleteText = timeToShowGoalCompleteText;
					for (int i = 0; i < 3; i++)
						curTimeToMoveGoalPanel[i] = timeToMoveGoalPanel[i];
					movingInGoalComplete = true;

					int addedCoins = GameState.SharedInstance.ConvertLevelToCoin(GoalManager.SharedInstance.goalDifficultyLevel);
					GameState.SharedInstance.AddCash(addedCoins);
					finalCoins += addedCoins;

					GameState.SharedInstance.AddMultiplier(1);
					finalMultiplier += 1;

					if (finalCoins > 0 && !pressedDoubleCoins && GameState.SharedInstance.GamesPlayed > 1)
					{
						if (supersonicManager.isRewardedVideoReady() && supersonicManager.isVideoCooldownDone())
							gameMenuButtons.EnableDoubleCoinVideoButton(true);
						else if(GameServerSettings.SharedInstance.SurferSettings.EnableDoubleCoinsEnergyDrink)
						{
							gameMenuButtons.EnableDoubleCoinButton(true);
							doubleCoinEnergyDrinkCost.text = GameState.SharedInstance.ConvertGoldToEnergyDrink(finalCoins).ToString();
						}
					}

					coinRewardText.text = string.Format("+{0} {1}", addedCoins, GameLanguage.LocalizedText("COINS"));
					coinParticle.gameObject.SetActive(true);
					coinParticle.Play();

					GoalManager.SharedInstance.AddDifficultyLevel();

					gcpm.SubmitHighScore(GoalManager.SharedInstance.goalDifficultyLevel, LeaderboardType.CredLeaderBoard);

					GoalManager.SharedInstance.ClearGoals();
					GoalManager.SharedInstance.RefreshGoals();
					GoalManager.SharedInstance.UpdateGoals();
				}

				RefreshCurrentEnergyDrinkText();
			}
		}

		autoSkipGoalIndex = null;
	}


	void ShowEndGameOverlay()
	{
		var pos = endGameOverlay.position;
		pos.x = mainCam.transform.position.x;
		pos.y = mainCam.transform.position.y;
		endGameOverlay.position = pos;
	}

    public void HideEndGameDarkScreen()
    {
        GlobalSettings.surfGamePaused = false;
        endGameDarkScreen.gameObject.SetActive(false);
    }

    public void ShowEndGameDarkScreen()
    {
        
        GlobalSettings.surfGamePaused = true;
        var pos = endGameDarkScreen.position;
        pos.x = mainCam.transform.position.x;
        pos.y = mainCam.transform.position.y;
        endGameDarkScreen.position = pos;
        endGameDarkScreen.gameObject.SetActive(true);
    }

	public void ShowDarkScreenSpotlight(Vector3 highlightPosition, float cullRadius = 2.0f, bool zoomDownEffect = false)
    {
		#if UNITY_ANDROID
		if (highlightScreen == null)
			highlightScreen = FindObjectOfType<SpotlightEffect>();

		if (highlightScreen != null)
			highlightScreen.gameObject.SetActive(false);

		return;
		#endif

		GlobalSettings.surfGamePaused = true;

		if (highlightScreen == null)
			highlightScreen = FindObjectOfType<SpotlightEffect>();

		Vector3 pos = highlightScreen.transform.position;
        pos.x = mainCam.transform.position.x;
        pos.y = mainCam.transform.position.y;

		highlightScreen.transform.position = pos;
		highlightScreen.marker.position = highlightPosition;

		if(zoomDownEffect)
		{
			highlightScreen.ToggleEffect(true, cullRadius, true, 0.5f);
		}
		else
		{
			highlightScreen.ToggleEffect(true, cullRadius);
		}

		highlightScreen.gameObject.SetActive(true);
    }

    public void HideDarkScreenSpotlight()
    {
		if (highlightScreen == null)
			highlightScreen = FindObjectOfType<SpotlightEffect>();

		if (highlightScreen != null)
			highlightScreen.gameObject.SetActive(false);
    }

    public void PersistScore()
    {
		if (scoreBar.GetScore() > GameState.SharedInstance.Score)
	        GameState.SharedInstance.Score = (int)scoreBar.GetScore();
		//GameState.SharedInstance.TotalScore += scoreBar.GetScore();
    }

	public void UpdateCoinTotal(int curCoins)
	{
		coinBar.DisplayCoins( curCoins, false); 
	}

    public void AddScore(float addedScore, bool special)
    {
        AddScore(addedScore, special, new Vector3(0, 0, 0));
    }

    public void AddScore(float addedScore, bool special, Vector3 scoreStartPos)
    {
		float scoreAdded = 0;
        if (special)
			scoreAdded = scoreBar.AddSpecialValue(addedScore, scoreStartPos);
        else
			scoreAdded = scoreBar.AddValue(addedScore);

		GoalManager.SharedInstance.UpdateScore(scoreAdded, playerControl.powerUpManager.GetCurrentVehicle());
    }

    public void AddScore(float addedScore)
	{
		float calculatedAddedScore = scoreBar.AddValue(addedScore);
		GoalManager.SharedInstance.UpdateScore(calculatedAddedScore, playerControl.powerUpManager.GetCurrentVehicle());
	}

	public void AddMultiplier(float multiplier)
	{
		scoreBar.AddMultiplier(multiplier);
	}

	public void ShowInGameUI()
	{
		if (GameState.SharedInstance.GamesPlayed >= 2 && GlobalSettings.SurfedPassMinTutorialDistance())
			UpdateHighScorePanel();

		if (isShowingSuperRewardText)
			UpdateSuperRewardPanel();
	}

	public void UpdateSuperRewardPanel()
	{
		if (!waitingOnHighscore)
		{
			float lerpValue = 1.0f - curTimeToMoveSuperReward / timeToMoveSuperReward;
			//right to center
			if (lerpValue < 0.5f)
			{
				lerpValue = Mathf.Pow(lerpValue * 2.0f, 3.0f);

				superRewardBannerObject.transform.localPosition = Vector3.Lerp(inactiveSuperRewardTextPositionRight, activeSuperRewardTextPosition, lerpValue);


				//if ( (1.0f - (curTimeToMoveSuperReward - Time.deltaTime) / timeToMoveSuperReward) >= 0.5f)
				//	GlobalSettings.PlaySound(highScoreSound);
			}
			//center to left
			else
			{
				lerpValue =  Mathf.Pow((lerpValue - 0.5f) * 2.0f, 3.0f);
				superRewardBannerObject.transform.localPosition = Vector3.Lerp(activeSuperRewardTextPosition, inactiveSuperRewardTextPositionLeft , lerpValue);
			}

			curTimeToMoveSuperReward -= Time.deltaTime;

			if (curTimeToMoveSuperReward < 0.0f)
			{
				superRewardBannerObject.transform.localPosition = inactiveSuperRewardTextPositionRight;
				isShowingSuperRewardText = false;
				waitingOnSuperReward = false;
			}
		}
	}

	public void ShowSuperRewardText(SpecialReward specialReward, int coins = -1)
	{
		switch (specialReward)
		{
			case SpecialReward.Coins:
				superRewardIcon.DrawFrame((int)SpecialRewardFrames.Coin);

				if (coins == -1)
					superRewardText.text = string.Format(SpecialRewardHelper.Get(specialReward), 150);
				else
					superRewardText.text = string.Format(SpecialRewardHelper.Get(specialReward), coins);

			break;
			case SpecialReward.SingleEnergy:
				superRewardIcon.DrawFrame((int)SpecialRewardFrames.SingleEnergy);
				superRewardText.text = SpecialRewardHelper.Get(specialReward);
			break;
			case SpecialReward.DoubleEnergy:
				superRewardIcon.DrawFrame((int)SpecialRewardFrames.DoubleEnergy);
				superRewardText.text = SpecialRewardHelper.Get(specialReward);	
			break;
		}

		curTimeToMoveSuperReward = timeToMoveSuperReward;
		isShowingSuperRewardText = true;

		if (isShowingHighScoreWinText)
			waitingOnHighscore = true;
	}

	public void StopPanelAnim()
	{
		//highscore text
		highScoreWinText.transform.localPosition = inactiveHighScoreTextPositionRight;
		isShowingHighScoreWinText = false;

		//Super Reward
		superRewardBannerObject.transform.localPosition = inactiveSuperRewardTextPositionRight;
		isShowingSuperRewardText = false;
	}

	public void UpdateHighScorePanel()
	{
		if (!waitingOnSuperReward)
		{
			//first time play
			if (curhighScoreAppearDelay > 0.0f && !isShowingHighScorePanel && !hasShownInitialHighScorePanel && !hasShownFinalHighScorePanel)
			{
				highScoreAppearDelay -= Time.deltaTime;
				if (highScoreAppearDelay <= 0.0f)
				{
					isShowingHighScorePanel = true;
					curTimeToMoveHighScorePanel = timeToMoveHighScorePanel;
				}
			}
			//close to final high score
			else if ((int)scoreBar.GetScore() > highScore * 0.8f && !isShowingHighScorePanel && !hasShownFinalHighScorePanel)
			{
				isShowingHighScorePanel = true;
				curTimeToMoveHighScorePanel = timeToMoveHighScorePanel;
			}

			//moving it
			if (curTimeToMoveHighScorePanel > 0.0f)
			{
				if (isShowingHighScorePanel)
				{
					float lerpValue = Mathf.Pow(curTimeToMoveHighScorePanel / timeToMoveHighScorePanel, 3.0f);	

					curTimeToMoveHighScorePanel -= Time.deltaTime;
					if (curTimeToMoveHighScorePanel <= 0)
						lerpValue = 0;
					
					highScorePanel.transform.localPosition = Vector3.Lerp(inactiveHighScorePanel, activeHighScorePanel, 1.0f - lerpValue);
				}
				else
				{
					float lerpValue =  1.0f - Mathf.Pow(1.0f - curTimeToMoveHighScorePanel / timeToMoveHighScorePanel, 3.0f);		

					curTimeToMoveHighScorePanel -= Time.deltaTime;
					if (curTimeToMoveHighScorePanel <= 0)
						lerpValue = 0;

					highScorePanel.transform.localPosition = Vector3.Lerp(inactiveHighScorePanel, activeHighScorePanel, lerpValue);
				}
			}

			//update and turn it off
			if (isShowingHighScorePanel)
			{
				SetHighScoreText(string.Format("{0:#,0}", (highScore - (int)scoreBar.GetScore())));
				
				if (!hasShownInitialHighScorePanel && curTimeToMoveHighScorePanel <= 0.0f)
				{
					timeToShowHighScorePanel -= Time.deltaTime;
					if (timeToShowHighScorePanel <= 0)
					{
						isShowingHighScorePanel = false;
						curTimeToMoveHighScorePanel = timeToMoveHighScorePanel;
						hasShownInitialHighScorePanel = true;
					}
				}

				if (scoreBar.GetScore() >= highScore)
				{
					SetHighScoreText("0");

					isShowingHighScoreWinText = true;
					curTimeToMoveHighScoreWin = timeToMoveHighScoreWin;

					if (isShowingSuperRewardText)
						waitingOnSuperReward = true;

					isShowingHighScorePanel = false;
					curTimeToMoveHighScorePanel = timeToMoveHighScorePanel;
					hasShownFinalHighScorePanel = true;
				}
			}
			//high score text
			if (isShowingHighScoreWinText)
			{
				float lerpValue = 1.0f - curTimeToMoveHighScoreWin / timeToMoveHighScoreWin;
				//right to center
				if (lerpValue < 0.5f)
				{
					lerpValue = Mathf.Pow(lerpValue * 2.0f, 4.0f);

					highScoreWinText.transform.localPosition = Vector3.Lerp(inactiveHighScoreTextPositionRight, activeHighScoreTextPosition, lerpValue);

					if ( (1.0f - (curTimeToMoveHighScoreWin - Time.deltaTime) / timeToMoveHighScoreWin) >= 0.5f)
						GlobalSettings.PlaySound(highScoreSound);

				}
				//center to left
				else
				{
					lerpValue =  Mathf.Pow((lerpValue - 0.5f) * 2.0f, 4.0f);
					highScoreWinText.transform.localPosition = Vector3.Lerp(activeHighScoreTextPosition, inactiveHighScoreTextPositionLeft , lerpValue);
				}

				curTimeToMoveHighScoreWin -= Time.deltaTime;

				if (curTimeToMoveHighScoreWin < 0.0f)
				{
					highScoreWinText.transform.localPosition = inactiveHighScoreTextPositionRight;
					isShowingHighScoreWinText = false;
					waitingOnHighscore = false;
				}
			}
		}
	}

	public void HideEndSequence()
	{
		isShowing = false;
       // //////Debug.Log("HIDING SEQUENCE HERE");
		var endGamePos = endGameOverlay.transform.position;
		endGameOverlay.transform.position = new Vector3(-99999f, endGamePos.y, endGamePos.z);
		retryPanel.transform.localPosition = inactivePanelPosition;



		coinBar.gameObject.SetActive(true);
		scoreBar.gameObject.SetActive(true); 
		pauseButton.gameObject.SetActive(true);
	}

	int RetyEnergyDrinkCost()
	{
		StickSurferSetting settings = GameServerSettings.SharedInstance.SurferSettings;

		return Mathf.Min((GameState.SharedInstance.RetryTimes + 1) * settings.RetryEnergyDrinkCost, settings.RetryEnergyDrinkCostMax);
	}

	public void ShowEndSequence(Location location)
    {
		if (!isShowing)
		{
			coinBar.gameObject.SetActive(false);
			scoreBar.gameObject.SetActive(false);
			pauseButton.gameObject.SetActive(false);

			if(gameMenuButtons != null)
			{
				gameMenuButtons.RefreshNotificationIcons();
				gameMenuButtons.gettingMenuInput = true;
			}
			//ShowEndGameOverlay();

			finalCoins = coinBar.curCoins;
			finalMultiplier = (int)scoreBar.curMultiplier + GameState.SharedInstance.Multiplier;
			mutiplierPrefix = scoreBar.mutliplierString;
			finalScore = (int)scoreBar.curScore;

			AnalyticsAndCrossPromoManager.SharedInstance.LogSurfGamePlayed(	finalCoins, 
																0, GoalManager.SharedInstance.distanceThisGame[0]);

			animFinalCoins = 0;
			animFinalMultiplier = 0;
			animFinalScore = 0;

			scoreScaleCountPerSecond = 1.0f;
			coinScaleCountPerSecond = 1.0f;

			endGameScore.text = string.Format("{0:#,0}", animFinalScore);
			SetEndGameCoinsText(string.Format("+{0:#,0}", animFinalCoins));
			endGameMutiplier.text = string.Format(mutiplierPrefix, animFinalMultiplier);

			StopPanelAnim();

			endGameButtonLocalPositionActive = endGameButtons.transform.localPosition;

			endGameButtonLocalPositionInactive = endGameButtons.transform.localPosition + Vector3.down * 10000.0f;
			endGameButtons.gameObject.SetActive(false); //transform.localPosition = endGameButtonLocalPositionInactive;


            RefreshCurrentEnergyDrinkText();
			retryEnergyDrinkCost.text = RetyEnergyDrinkCost().ToString();

			isShowing = true;

			retryPanel.transform.localPosition = activePanelPosition;

			if (playerControl.distance > GameServerSettings.SharedInstance.SurferSettings.MinSaveMeDistance && GameState.SharedInstance.TutorialStage == TutStageType.Finished)
			{
				curState = EndUIState.RETRY_COUNTDOWN;
				ShowEndGameDarkScreen();
			}
			else
			{
				EndGame(false);
			}

			curTimeToMove = timeToMove;
			curRetryTime = retryTime;

			gameMenuButtons.EnableRetryButton(true);

            if (GameState.SharedInstance.RetryTimes == 0 && supersonicManager.isRewardedVideoReady())
            {
                watchVideoRetryButton.gameObject.SetActive(true);
                retryEnergyDrinkButton.gameObject.SetActive(false);
            }
            else
            {
                watchVideoRetryButton.gameObject.SetActive(false);
                retryEnergyDrinkButton.gameObject.SetActive(true);
            }


            GoalManager.SharedInstance.UpdateGoals();
			Goal[] curGoals = GoalManager.SharedInstance.goals;
			for (int i = 0; i < 3; i++)
			{
				curTimeToMoveGoalPanel[i] = timeToMoveGoalPanel[i];
				goalPanelDescriptions[i].text = GoalText.Get(curGoals[i]);

				if (curGoals[i].complete)
				{
					goalPanelCompletes[i].SetActive(true);
					goalPanelButtons[i].SetActive(false);

				}
				else
				{
					goalPanelCompletes[i].SetActive(false);

					if (GameState.SharedInstance.TutorialStage == TutStageType.Finished)
						goalPanelButtons[i].SetActive(true);
					else
						goalPanelButtons[i].SetActive(false);


					goalSkipGoalCosts[i].text = (GoalManager.SharedInstance.GetEnergyDrinkCost()).ToString();
				}
			}
		}
	}


	private void EndGame (bool transitioningOutOfRetry = true)
	{
		playerControl.inEndGameMenuMode = true;
		GameState.SharedInstance.AddCash(finalCoins);
		PersistScore();

		curState = EndUIState.MOVING_IN;
        GlobalSettings.PlaySoundDelayed(this.swooshSound, 0.2f);
        ShowEndGameOverlay();

        GlobalSettings.surfGamePaused = false;
		powerUpManager.RemoveAllPowerUps();
		gameMenuButtons.EnableGoButton(true);

		AnalyticsAndCrossPromoManager.SharedInstance.NotifyEndSurfGame();


		Bird[] birdObjects = GameObject.FindObjectsOfType<Bird>();
		foreach (Bird bird in birdObjects)
		{
			bird.MuteBirdSounds();
		}

		Chopper[] chopperObjects = GameObject.FindObjectsOfType<Chopper>();
		foreach (Chopper chopper in chopperObjects)
		{
			chopper.chopperSound.Stop();
		}

		isShowingCredLevel = false;


		gcpm.SubmitHighScore(finalScore, LeaderboardType.SurfLeaderBoard);

		RefreshCurrentEnergyDrinkText();

		attemptingRetry = false;

		if (finalCoins > 0 && !pressedDoubleCoins && GameState.SharedInstance.GamesPlayed > 1)
		{
			if (supersonicManager.isRewardedVideoReady() && supersonicManager.isVideoCooldownDone())
				gameMenuButtons.EnableDoubleCoinVideoButton(true);
			else if(GameServerSettings.SharedInstance.SurferSettings.EnableDoubleCoinsEnergyDrink)
			{
				gameMenuButtons.EnableDoubleCoinButton(true);
				doubleCoinEnergyDrinkCost.text = GameState.SharedInstance.ConvertGoldToEnergyDrink(finalCoins).ToString();
			}
		}
		else
		{
			gameMenuButtons.EnableDoubleCoinButton(false);
			gameMenuButtons.EnableDoubleCoinVideoButton(false);
		}

		GoalManager.SharedInstance.StartNewGame();

		if (transitioningOutOfRetry)
		{
			isMovingRetryPanel = true;
			HideEndGameDarkScreen();
		}
		else
		{
			isMovingRetryPanel = false;
			retryPanel.transform.localPosition = inactivePanelPosition;
		}

		if (GameState.SharedInstance.TutorialStage != TutStageType.Finished)
		{
			if (GameState.SharedInstance.TutorialStage == TutStageType.FirstGame || GameState.SharedInstance.TutorialStage == TutStageType.MainMenuGoButton)
			{
				//disable goal skips and double coins

				for (int i = 0; i < 3; i++)
				{
					gameMenuButtons.postGameskipEnergyButtons[i].SetActive(false);
				}

				gameMenuButtons.doubleCoinsButton.SetActive(false);
				gameMenuButtons.doubleCoinsVideoButton.SetActive(false);

				gameMenuButtons.goButtonParent.SetActive(false);
				gameMenuButtons.shopButtonParent.SetActive(false);
			}
			else if (GameState.SharedInstance.TutorialStage != TutStageType.PostShopVisit &&
					 GameState.SharedInstance.TutorialStage != TutStageType.MainMenuJeep)
			{
				gameMenuButtons.doubleCoinsButton.SetActive(false);
				gameMenuButtons.doubleCoinsVideoButton.SetActive(false);
				gameMenuButtons.goButtonParent.SetActive(false);
				gameMenuButtons.shopButtonParent.SetActive(false);
			}
		}

		credLevel.transform.localPosition = inactiveGoalCompletePosition;
		isShowingCredLevel = false;

		goalGoalCompleteText.transform.localPosition = inactiveGoalCompletePosition;
		coinLevelUpParticleEffect.gameObject.SetActive(false);
		coinLevelUpParticleEffect.Stop();

		animFinalMultiplier = GoalManager.SharedInstance.goalDifficultyLevel;
		finalMultiplier = (int)animFinalMultiplier;
	
		endGameMutiplier.text = string.Format(mutiplierPrefix, (int)(animFinalMultiplier));
	
	}

	public void UpdateEndGame () 
	{
		if (isShowing)
		{
			if(autoUseEnergyDrinkDelaySecs >= 0)
			{
				autoUseEnergyDrinkDelaySecs -= Time.deltaTime;
				if(autoUseEnergyDrinkDelaySecs <= 0)
				{
					if(attemptingRetry)
					{
						attemptingRetry = false;
						UseEnergyDrinkOnRetry();
					}
					else if(attemptingDoubleCoins)
					{
						attemptingDoubleCoins = false;
						UseEnergyDrinkOnDoubleCoins();
					}
					else if(attemptingSkipGoal)
					{
						attemptingSkipGoal = false;

						if(autoSkipGoalIndex.HasValue)
							UseEnergyDrinkOnSkipGoal(autoSkipGoalIndex.Value);
					}
				}
			}
				
			if (curState == EndUIState.RETRY_COUNTDOWN)
			{
				//check for retry button
				float retryCalc = curRetryTime < 0 ? 0 : curRetryTime;
				float retryPercentLeft = retryCalc / retryTime;
				retryFillBar.SetHorizBarAtValue(1.0f - retryPercentLeft);

				if (!isRetryBarLocked)
					curRetryTime -= Time.deltaTime;

				if (curRetryTime < -0.125f)
				{
					EndGame();
				}
			}
			else if (curState == EndUIState.MOVING_IN)
			{
				float lerpValue = 1.0f - Mathf.Pow(curTimeToMove / timeToMove, 3.0f);

				curTimeToMove -= Time.deltaTime;

				if (curTimeToMove <= 0.0f)
				{
                    Debug.Log("Moving in UI here 1");

                    if (GameServerSettings.SharedInstance.SurferSettings.EnableInterstitialEndGame)
                    {
                        Debug.Log("Moving in UI here 2");
                        if (supersonicManager != null && playerControl != null && GlobalSettings.TutorialCompleted())
                        {
                            //Debug.Log("Showing end game interstitial here 1 canshow?:" + supersonicManager.CanShowInterstitialAd());
                            //if (playerControl.GamesRestartedCount() >= GameServerSettings.SharedInstance.SurferSettings.EndGameRestartsBeforeInterstitialShown &&
                            //    supersonicManager.CanShowInterstitialAd())
                            if(supersonicManager.CanShowInterstitialAd())
                            {
                                Debug.Log("Showing end game interstitial here 2");
                                AudioListener.pause = true;
                                supersonicManager.ShowInterstitial();
                            }
                        }
                    }
                    else
                    {
                        GameServerSettings.SharedInstance.PrintSurferSettingsContent();
                    }

					if(GameUISyncSavedGameData == false)
					{
						GameUISyncSavedGameData = true;
						ServerSavedGameStateSync.Sync();
					}

					lerpValue = 1.0f;
					curState = EndUIState.GOAL_BREAKDOWN;
                    GlobalSettings.PlaySoundDelayed(this.swooshSound, 0.1f);
                    //GlobalSettings.PlaySoundDelayed(this.swooshSound,0.2f);
                    //GlobalSettings.PlaySoundDelayed(this.swooshSound,0.7f);
                    //GlobalSettings.PlaySoundDelayed(this.swooshSound,1.2f);
					curTimeToMoveEndGameButtons = timeToMoveEndGameButtons;

					endGameButtonLocalPositionActive = endGameButtons.transform.localPosition;
					endGameButtonLocalPositionInactive = endGameButtons.transform.localPosition + Vector3.down * 500.0f;
					endGameButtons.transform.localPosition = endGameButtonLocalPositionInactive;
					endGameButtons.gameObject.SetActive(true);
				}
				postGameUIPanel.transform.localPosition = Vector3.Lerp(inactivePanelPosition, activePanelPosition, lerpValue);

				if (isMovingRetryPanel)
					retryPanel.transform.localPosition = Vector3.Lerp(activePanelPosition, altInactivePanelPosition, lerpValue);
				
			}
			else if (curState == EndUIState.GOAL_BREAKDOWN)
			{
				bool[] checkMovement = {false, false, false, false};
				for (int i = 0; i < 3; i++)
				{
					if (curTimeToMoveGoalPanel[i] >= 0)
					{
						float lerpAmount = Mathf.Pow(curTimeToMoveGoalPanel[i] / timeToMoveGoalPanel[i], 3.0f);

						curTimeToMoveGoalPanel[i] -= Time.deltaTime;
                        if (curTimeToMoveGoalPanel[i] <= 0)
                        {   
                            lerpAmount = 0;
                        }
						goalPanels[i].transform.localPosition = Vector3.Lerp(inactiveGoalPosition[i], activeGoalPosition[i], 1.0f - lerpAmount);
					}
					else
						checkMovement[i] = true;
				}

				//Move cred level out
				if (isShowingCredLevel)
				{
					if (curTimeToShowCredCompleteText >= 0)
					{
						float lerpAmount = curTimeToShowCredCompleteText / timeToShowCredCompleteText;
					
						curTimeToShowCredCompleteText -= Time.deltaTime;
						if (curTimeToShowCredCompleteText <= 0)
						{
							lerpAmount = 0;
							curTimeToShowGoalCompleteText = timeToShowCredCompleteText;
						}
						credLevel.transform.localPosition = Vector3.Lerp(new Vector3(-inactiveGoalCompletePosition.x, inactiveGoalCompletePosition.y, inactiveGoalCompletePosition.z), activeGoalCompletePosition,  Mathf.Pow(lerpAmount, 3.0f));
					}
					else
					{
						credLevel.transform.localPosition = -new Vector3(-inactiveGoalCompletePosition.x, inactiveGoalCompletePosition.y, inactiveGoalCompletePosition.z);
						checkMovement[3] = true;
						coinLevelUpParticleEffect.gameObject.SetActive(false);
						isShowingCredLevel = false;
					}
				}
				else
					checkMovement[3] = true;


				if (curTimeToShowGoalCompleteText >= 0 && !isShowingCredLevel)
				{
					float lerpAmount = curTimeToShowGoalCompleteText / timeToShowGoalCompleteText;

					curTimeToShowGoalCompleteText -= Time.deltaTime;
					if (curTimeToShowGoalCompleteText <= 0)
					{
						lerpAmount = 0;
					}
					if (lerpAmount <= 0.25f)
					{
						goalGoalCompleteText.transform.localPosition = Vector3.Lerp(inactiveGoalCompletePosition, activeGoalCompletePosition, Mathf.Pow(lerpAmount / 0.25f, 3.0f));
					}
				}

				if (checkMovement[0] && checkMovement[1] && checkMovement[2] && checkMovement[3])
				{
					Goal[] curGoals = GoalManager.SharedInstance.goals;
					bool checkCompletedGoals = true;
					isShowingCredLevel = false;

					for (int i = 0; i < 3; i++)
					{
						if (!curGoals[i].complete)
						{
							checkCompletedGoals = false;
							break;
						}
					}

					if (!checkCompletedGoals)
					{
						curState = EndUIState.COUNTING;
						coinParticle.gameObject.SetActive(false);
					}
					else
					{
						curState = EndUIState.GOAL_REWARD;
						curTimeToShowGoalCompleteText = timeToShowGoalCompleteText;

						for (int i = 0; i < 3; i++)
							curTimeToMoveGoalPanel[i] = timeToMoveGoalPanel[i];

						int addedCoins = GameState.SharedInstance.ConvertLevelToCoin(GoalManager.SharedInstance.goalDifficultyLevel);
						GameState.SharedInstance.AddCash(addedCoins);

						GameState.SharedInstance.AddMultiplier(1);
						finalMultiplier += 1;

						finalCoins += addedCoins;

						if (finalCoins > 0 && !pressedDoubleCoins && GameState.SharedInstance.GamesPlayed > 1)
						{
							if (supersonicManager.isRewardedVideoReady() && supersonicManager.isVideoCooldownDone())
								gameMenuButtons.EnableDoubleCoinVideoButton(true);
							else if(GameServerSettings.SharedInstance.SurferSettings.EnableDoubleCoinsEnergyDrink)
							{
								gameMenuButtons.EnableDoubleCoinButton(true);
								doubleCoinEnergyDrinkCost.text = GameState.SharedInstance.ConvertGoldToEnergyDrink(finalCoins).ToString();
							}
						}
						else
						{
							gameMenuButtons.EnableDoubleCoinButton(false);
							gameMenuButtons.EnableDoubleCoinVideoButton(false);
						}

						coinLevelUpParticleEffect.gameObject.SetActive(true);
						coinLevelUpParticleEffect.Play();

						coinRewardText.text = string.Format("+{0}", addedCoins);
						coinParticle.gameObject.SetActive(true);
						coinParticle.Play();
						movingInGoalComplete = true;
						GlobalSettings.PlaySound(goalsAllCompleteSound);

						GoalManager.SharedInstance.AddDifficultyLevel();

						gcpm.SubmitHighScore(GoalManager.SharedInstance.goalDifficultyLevel, LeaderboardType.CredLeaderBoard);

						GoalManager.SharedInstance.ClearGoals();
						GoalManager.SharedInstance.RefreshGoals();
						GoalManager.SharedInstance.UpdateGoals();
					}
				}
			}
			else if (curState == EndUIState.GOAL_REWARD)
			{
				bool[] checkMovement = {false, false, false, false};

				//move in complete text
				if (curTimeToShowGoalCompleteText >= 0 && movingInGoalComplete)
				{
					float lerpAmount = curTimeToShowGoalCompleteText / timeToShowGoalCompleteText;
				
					curTimeToShowGoalCompleteText -= Time.deltaTime;
					if (curTimeToShowGoalCompleteText <= 0)
					{
						lerpAmount = 0;
                        GlobalSettings.PlaySound (this.swooshSound);
                        GlobalSettings.PlaySound(kachingSound);
						movingInGoalComplete = false;
						curTimeToShowGoalCompleteText = timeToShowGoalCompleteText;
					}
					goalGoalCompleteText.transform.localPosition = Vector3.Lerp(inactiveGoalCompletePosition, activeGoalCompletePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
				}
				else
					checkMovement[3] = true;

				//move goals left
				for (int i = 0; i < 3; i++)
				{
					if (curTimeToMoveGoalPanel[i] > 0)
					{
						float lerpAmount = Mathf.Pow(curTimeToMoveGoalPanel[i] / timeToMoveGoalPanel[i], 3.0f);

						curTimeToMoveGoalPanel[i] -= Time.deltaTime;
                        if (curTimeToMoveGoalPanel[i] <= 0)
                        {
                            lerpAmount = 0;
                        }
						goalPanels[i].transform.localPosition = Vector3.Lerp(activeGoalPosition[i], altInactiveGoalPosition[i] , 1.0f - lerpAmount);
					}
					else
						checkMovement[i] = true;
				}

				if (checkMovement[0] && checkMovement[1] && checkMovement[2] && checkMovement[3])
				{
					//refresh goals
					Goal[] curGoals = GoalManager.SharedInstance.goals;
					for (int i = 0; i < 3; i++)
					{
						curTimeToMoveGoalPanel[i] = timeToMoveGoalPanel[i];
						goalPanelDescriptions[i].text = GoalText.Get(curGoals[i]);

						if (!curGoals[i].complete)
						{
							goalSkipGoalCosts[i].text = (GoalManager.SharedInstance.GetEnergyDrinkCost()).ToString();
							goalPanelCompletes[i].SetActive(false);

							if (GameState.SharedInstance.TutorialStage == TutStageType.Finished)
								goalPanelButtons[i].SetActive(true);
							else
								goalPanelButtons[i].SetActive(false);
						}
						else
						{
							goalPanelCompletes[i].SetActive(true);
							goalPanelButtons[i].SetActive(false);
						}
					}
					curState = EndUIState.CRED_REWARD;
					credText.text = string.Format("<size=-26>x</size>{0}", GoalManager.SharedInstance.goalDifficultyLevel - 1);
					curTimeToShowCredCompleteText = timeToShowCredCompleteText;
				}
			}
			else if (curState == EndUIState.CRED_REWARD)
			{
				//Move cred level in
				if (curTimeToShowCredCompleteText >= 0)
				{
					float lerpAmount = curTimeToShowCredCompleteText / timeToShowCredCompleteText;
				
					curTimeToShowCredCompleteText -= Time.deltaTime;
					if (curTimeToShowCredCompleteText <= 0)
					{
						lerpAmount = 0;
                        GlobalSettings.PlaySound (levelUpSound);
						curTimeToShowGoalCompleteText = timeToShowCredCompleteText;
						curTimeToPauseCred = timeToPauseCred;
						credText.text = string.Format("<size=-26>x</size>{0}", GoalManager.SharedInstance.goalDifficultyLevel);
						origCredScale = credText.gameObject.transform.localScale;
					}

					credLevel.transform.localPosition = Vector3.Lerp(inactiveGoalCompletePosition, activeGoalCompletePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
					goalGoalCompleteText.transform.localPosition = Vector3.Lerp(new Vector3(-inactiveGoalCompletePosition.x, inactiveGoalCompletePosition.y, inactiveGoalCompletePosition.z), activeGoalCompletePosition, Mathf.Pow(lerpAmount, 3.0f));
				}
				else if (curTimeToPauseCred >= 0)
				{
					credText.gameObject.transform.localScale = origCredScale + Vector3.one * (curTimeToPauseCred / timeToPauseCred) * 0.4f;
					curTimeToPauseCred -= Time.deltaTime;
				}
				else
				{
					isShowingCredLevel = true;

					credLevel.transform.localPosition = inactiveGoalCompletePosition;

					curState = EndUIState.GOAL_BREAKDOWN;
                    GlobalSettings.PlaySound(this.swooshSound);
					curTimeToShowCredCompleteText = timeToShowCredCompleteText;
				}
			}
			else if (curState == EndUIState.COUNTING)
			{
				for (int i = 0; i < 3; i++)
				{
					goalPanels[i].transform.localPosition = activeGoalPosition[i];
				}

				//end game buttons
				if (curTimeToMoveEndGameButtons >= 0)
				{
					float lerpAmount = Mathf.Pow(curTimeToMoveEndGameButtons / timeToMoveEndGameButtons, 3.0f);
					curTimeToMoveEndGameButtons -= Time.deltaTime;

					if (curTimeToMoveEndGameButtons <= 0)
						lerpAmount = 0;

					endGameButtons.transform.localPosition = Vector3.Lerp(endGameButtonLocalPositionInactive, endGameButtonLocalPositionActive, 1.0f -lerpAmount);
				}


				if (animFinalScore != finalScore)
				{
					animFinalScore += (Time.deltaTime * Mathf.Pow(scoreCountPerSecond, scoreScaleCountPerSecond));
					animFinalScore = Mathf.Min(animFinalScore, finalScore);
					endGameScore.text = string.Format("{0:#,0}", (int)animFinalScore);

					scoreScaleCountPerSecond += Time.deltaTime * 0.2f;
				}

				if (animFinalCoins != finalCoins)
				{
					endGameCoinBar.SetActive(true);
					animFinalCoins += (Time.deltaTime * Mathf.Pow(coinCountPerSecond, coinScaleCountPerSecond * 1.7f));
					animFinalCoins = Mathf.Min(animFinalCoins, finalCoins);
					SetEndGameCoinsText(string.Format("+{0}", ((int)(animFinalCoins))));

					coinScaleCountPerSecond += Time.deltaTime * 0.1f;

					GlobalSettings.PlaySound(soundCoin);
				}

				if (animFinalMultiplier != finalMultiplier)
				{
					animFinalMultiplier += Time.deltaTime * multiplierCountPerSecond;
					animFinalMultiplier = Mathf.Min(animFinalMultiplier, finalMultiplier);				
					endGameMutiplier.text = string.Format(mutiplierPrefix, (int)(animFinalMultiplier));
				}

				if (animFinalScore == finalScore && animFinalCoins == finalCoins  &&  animFinalMultiplier == finalMultiplier)
				{
					//sparkles here!
					if (curTimeToMoveEndGameButtons <= 0)
					{
						curState = EndUIState.WAITING_FOR_INPUT;
						if (GameState.SharedInstance.TutorialStage != TutStageType.PostShopVisit &&
					 			GameState.SharedInstance.TutorialStage != TutStageType.MainMenuJeep &&
								GameState.SharedInstance.TutorialStage != TutStageType.Finished)
						{
							ShowDarkScreenSpotlight( gameMenuButtons.shackButtonEnabled.transform.position, 2.0f);	
							gameMenuButtons.shackButtonHand.SetActive(true);
							gameMenuButtons.shackButtonJiggle.enabled = true;
						}
						else if (GameState.SharedInstance.TutorialStage == TutStageType.MainMenuJeep)
						{
							gameMenuButtons.shackButtonHand.SetActive(true);
							gameMenuButtons.shackButtonJiggle.enabled = true;
						}

					}

				}
			}
		}
	}
		
	void EnergyDrinkShopControllerOnPurchaseSuccessful (int energyDrinkAmount)
	{
		energyDrinkPurchased = true;
		RefreshCurrentEnergyDrinkText();
	}

	void EnergyDrinkShopControllerOnClose ()
	{
		RefreshCurrentEnergyDrinkText();

		if(energyDrinkPurchased)
		{
			energyDrinkPurchased = false;

			if(attemptingRetry)
			{
				if(HasEnoughEnergyDrinkForRetry())
				{
					autoUseEnergyDrinkDelaySecs = 0.3f;
					if(curRetryTime < 0.5f)
						curRetryTime = 0.5f;
				}
				else
					attemptingRetry = false;
			}
			else if(attemptingDoubleCoins)
			{
				if(HasEnoughEnergyDrinkForDoubleCoins())
					autoUseEnergyDrinkDelaySecs = 0.3f;
				else 
					attemptingDoubleCoins = false;
			}
			else if(attemptingSkipGoal)
			{
				if(HasEnoughEnergyDrinkForSkipGoal())
					autoUseEnergyDrinkDelaySecs = 0.3f;
				else
					attemptingSkipGoal = false;
			}
		}

		isRetryBarLocked = false;
	}

	void OnDelete()
	{
		
	}

	private enum EndUIState
	{
		MOVING_IN,
		RETRY_COUNTDOWN,
		SUPER_REWARD,
		GOAL_BREAKDOWN,
		GOAL_REWARD,
		CRED_REWARD,
		COUNTING,
		WAITING_FOR_INPUT,
		RETRYING_GAME,
		NONE
	}
}
