﻿using UnityEngine;
using System.Collections;

public class SimpleSpriteAnimator : MonoBehaviour 
{
	private static int curOffset = 0;

	public SpriteRenderer spriteRenderer;
	public Sprite[] sprites;

	private int curSpriteIndex = 0;
	private float timeSinceLastChange = 0.0f;

	public float fps = 10.0f;

	public bool isLooping = true;
	public bool offsetStartIndex = true;

	public bool animating = true;

	void Start () 
	{
		if (offsetStartIndex)
		{
			curSpriteIndex = curOffset;
			curOffset ++;
		}
		else
			curSpriteIndex = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (animating)
		{
			timeSinceLastChange += Time.deltaTime;

			while (timeSinceLastChange > 1.0f / fps)
			{
				timeSinceLastChange -= 1.0f / fps;

				curSpriteIndex ++;
				if (!isLooping)
				{
					if (curSpriteIndex >= sprites.Length)
					{
						curSpriteIndex = 0;
						animating = false;
					}
				}
				else
					curSpriteIndex = (curSpriteIndex) % sprites.Length;

				spriteRenderer.sprite = sprites[curSpriteIndex];

			}
		}
	}
}
