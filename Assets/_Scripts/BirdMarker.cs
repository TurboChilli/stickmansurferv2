﻿using UnityEngine;
using System.Collections;

public class BirdMarker : MonoBehaviour 
{
	Renderer birdRenderer = null;
	Renderer warningSignRenderer = null;

	public void TurnOffRenderers()
	{
		if (birdRenderer == null)
		{
			birdRenderer = GetComponent<Renderer>();
			warningSignRenderer = transform.GetChild(0).GetComponent<Renderer>();
		}

		birdRenderer.enabled = false;
		warningSignRenderer.enabled = false;
	}
}
