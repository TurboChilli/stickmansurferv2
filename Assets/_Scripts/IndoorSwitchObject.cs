﻿using UnityEngine;
using System.Collections;

public class IndoorSwitchObject : MonoBehaviour 
{
	private Collider attachedCollider = null;
	private bool isHit = false;

	void Start () 
	{

		if (attachedCollider == null)	
			attachedCollider = GetComponent<Collider>();
	}

	public void ResetIndoorSwitch()
	{

		if (attachedCollider == null)	
			attachedCollider = GetComponent<Collider>();

		attachedCollider.enabled = true;
		isHit = false;
	}

	public bool Hit()
	{
		if (!isHit)
		{
			isHit = true;
			attachedCollider.enabled = false;
			return true;
		}
		return false;
	}

}
