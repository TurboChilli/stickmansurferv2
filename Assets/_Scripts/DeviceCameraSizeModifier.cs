﻿using UnityEngine;
using System.Collections;

public class DeviceCameraSizeModifier : MonoBehaviour {

	public float SetToOrthographicSize = 0f;
	public Camera TheCamera = null;

	void Awake()
	{
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;

		if(screenWidth > 50 && screenHeight > 50 && SetToOrthographicSize > 0f && TheCamera != null && TheCamera.orthographic)
		{
			float ratio = screenHeight > screenWidth ? (screenWidth / screenHeight) : (screenHeight / screenWidth);

			// Set custom orthographic size if iPad
			if(ratio >= 0.725f)
			{
				TheCamera.orthographicSize = SetToOrthographicSize;
			}
		}
	}
}