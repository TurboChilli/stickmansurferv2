﻿using UnityEngine;
using Prime31;
using System.Collections;
using System.Collections.Generic;

public class IAPManager : MonoBehaviour 
{
	#if UNITY_ANDROID

	public const string ProductIdPack1 = "androidsticksurfpack1";
	public const string ProductIdPack2 = "androidsticksurfpack2";
	public const string ProductIdPack3 = "androidsticksurfpack3";
	public const string ProductIdPack4 = "androidsticksurfpack4";
	public const string ProductIdPack5 = "androidsticksurfpack5";
	public const string ProductIdPack6 = "androidsticksurfpack6";

	public const string StarterPackProductId6 = "androidsticksurfstarterpack1";

	bool IsConsumableProduct(string productId)
	{
		if(productId.ToLower() == ProductIdPack1.ToLower() ||
			productId.ToLower() == ProductIdPack2.ToLower() ||
			productId.ToLower() == ProductIdPack3.ToLower() ||
			productId.ToLower() == ProductIdPack4.ToLower() ||
			productId.ToLower() == ProductIdPack5.ToLower() ||
			productId.ToLower() == ProductIdPack6.ToLower())
		{
			return true;
		}

		return false;
	}

	#else

	public const string ProductIdPack1 = "STICKSURFPACK1";
	public const string ProductIdPack2 = "STICKSURFPACK2";
	public const string ProductIdPack3 = "STICKSURFPACK3";
	public const string ProductIdPack4 = "STICKSURFPACK4";
	public const string ProductIdPack5 = "STICKSURFPACK5";
	public const string ProductIdPack6 = "STICKSURFPACK6";

	public const string StarterPackProductId6 = "STICKSURFSTARTERPACK1";

	#endif 

    private string[] productIds = 
	{ 
		ProductIdPack1, 
        ProductIdPack2, 
        ProductIdPack3, 
        ProductIdPack4, 
        ProductIdPack5, 
        ProductIdPack6, 
        StarterPackProductId6 
	};

	public delegate void PurchaseSuccessfulHandler(string productId);
	public event PurchaseSuccessfulHandler OnPurchaseSuccessful;

	public delegate void PurchaseFailedHandler(string errorMessage);
	public event PurchaseFailedHandler OnPurchaseFailed;

	public delegate void PurchaseCancelledHandler(string cancelMessage);
	public event PurchaseCancelledHandler OnPurchaseCancelled;

	public delegate void ProductPricesRefreshedHandler();
	public event ProductPricesRefreshedHandler OnProductPricesRefreshed;

	public delegate void RestorePurchasesFailedHandler(string errorMessage);
	public event RestorePurchasesFailedHandler OnRestorePurchasesFailed;

	public delegate void RestorePurchasesFinishedHandler();
	public event RestorePurchasesFinishedHandler OnRestorePurchasesFinished;

	public delegate void RestorePurchasesSuccessfulHandler(string productId);
	public event RestorePurchasesSuccessfulHandler OnRestorePurchasesSuccessful;

	private const string refreshProductInfoPrefKey = "IAPM_rppk";
	private string currentPurchasingProductID;

	bool restoringPurchases = false;
	bool printIAPManagerDebugMsg = false;
	static bool iapManagerInitialised = false;

	void Start ()
	{
		if(!iapManagerInitialised)
		{
			iapManagerInitialised = true;

			// Finish pending transactions
			#if UNITY_IOS
			List<StoreKitTransaction> pendingTransactions = StoreKitBinding.getAllCurrentTransactions();
			foreach(StoreKitTransaction transaction in pendingTransactions)
			{
				StoreKitBinding.finishPendingTransaction(transaction.transactionIdentifier);

				if(printIAPManagerDebugMsg)
					Debug.LogFormat("IAPManager -> Start -> FinishPendingTransactionId: {0}", transaction.transactionIdentifier);
			}
			#endif

			// Refresh product prices
			RefreshProductPrices();
		}
		else if(ShouldRefreshProductInfo())
		{
			RefreshProductPrices();
		}
	}

	void OnEnable() 
	{
		#if UNITY_IOS
		StoreKitManager.productListReceivedEvent += ProductListReceivedEventHandler;
		StoreKitManager.purchaseSuccessfulEvent += PurchaseSuccessfulEventHandler;
		StoreKitManager.purchaseFailedEvent += PurchaseFailedEventHandler;
		StoreKitManager.purchaseCancelledEvent += PurchaseCancelledEventHandler;

		StoreKitManager.restoreTransactionsFinishedEvent += RestoreTransactionsFinishedEventHandler;
		StoreKitManager.restoreTransactionsFailedEvent += RestoreTransactionsFailedEventHandler;

		#elif UNITY_ANDROID

		string googleBillingLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArpqozkjsjT8g/8L8WYPx8WmhMOxuu+aO6plhy8mVRdEO3IAxTDbbXTo+hVUrZU5vwtilVgH4/LcHDBPdd/9uzlR0biMFCpSx+YXWDYYN4oOVHuOqJHbMO7ehJYv9hkGgdeoaztpKDcq5LVdYChSjuxLEfonFRu3D0l68Xxf/OcCgDNQanW3O6RiF65JxSVI5x1NCPH63BffqonFJHNYLs5ynoxlZMBqASzbJIFJ7NIpegPJtXqHwGdjt51dL3q21MdCfvEVSAJ2FEHo2TOvOvyMvt5NkbaOISXyyLfOfePCIGIP/iYPkmkx+bssuEyxoCoHhsRZxAjBMSJ8hKNpD6QIDAQAB";
		GoogleIAB.init(googleBillingLicenseKey);

		GoogleIABManager.billingSupportedEvent += BillingSupportedEventHandler;
		GoogleIABManager.queryInventorySucceededEvent += QueryInventorySucceededEventHandler;
		GoogleIABManager.purchaseSucceededEvent += PurchaseSucceededEventHandler;
		GoogleIABManager.purchaseFailedEvent += PurchaseFailedEventHandler;

		#endif
	}

	void OnDisable()
	{
		#if UNITY_IOS
		StoreKitManager.productListReceivedEvent -= ProductListReceivedEventHandler;
		StoreKitManager.purchaseSuccessfulEvent -= PurchaseSuccessfulEventHandler;
		StoreKitManager.purchaseFailedEvent -= PurchaseFailedEventHandler;
		StoreKitManager.purchaseCancelledEvent -= PurchaseCancelledEventHandler;
		StoreKitManager.restoreTransactionsFinishedEvent -= RestoreTransactionsFinishedEventHandler;
		StoreKitManager.restoreTransactionsFailedEvent -= RestoreTransactionsFailedEventHandler;

		#elif UNITY_ANDROID
		GoogleIABManager.billingSupportedEvent -= BillingSupportedEventHandler;
		GoogleIABManager.queryInventorySucceededEvent -= QueryInventorySucceededEventHandler;
		GoogleIABManager.purchaseSucceededEvent -= PurchaseSucceededEventHandler;
		GoogleIABManager.purchaseFailedEvent -= PurchaseFailedEventHandler;
		#endif
	}

	public static bool ProductIdMatchesEnergyDrinkPackIds(string productId)
	{
		if(productId.ToLowerInvariant() == ProductIdPack1.ToLowerInvariant() || 
			productId.ToLowerInvariant() == ProductIdPack2.ToLowerInvariant() || 
			productId.ToLowerInvariant() == ProductIdPack3.ToLowerInvariant() || 
			productId.ToLowerInvariant() == ProductIdPack4.ToLowerInvariant() || 
			productId.ToLowerInvariant() == ProductIdPack5.ToLowerInvariant() || 
			productId.ToLowerInvariant() == ProductIdPack6.ToLowerInvariant()
		)
		{
			return true;
		}
		
		return false;
	}

	public string GetProductPriceString(string productId)
	{
		#if UNITY_EDITOR
		return string.Format("${0}", GetProductPrice(productId));
		#else
		return PlayerPrefs.GetString(string.Format("IAPMpps{0}", productId.ToUpperInvariant()), "");
		#endif
	}

	void SetProductPriceString(string productId, string price)
	{
		PlayerPrefs.SetString(string.Format("IAPMpps{0}", productId.ToUpperInvariant()), price);
	}

	public float GetProductPrice(string productId)
	{
		#if UNITY_EDITOR
		if(productId.ToLowerInvariant() == ProductIdPack1.ToLowerInvariant())
			return 0.99f;
		else if(productId.ToLowerInvariant() == ProductIdPack2.ToLowerInvariant())
			return 2.99f;
		else if(productId.ToLowerInvariant() == ProductIdPack3.ToLowerInvariant())
			return 4.99f;
		else if(productId.ToLowerInvariant() == ProductIdPack4.ToLowerInvariant())
			return 9.99f;
		else if(productId.ToLowerInvariant() == ProductIdPack5.ToLowerInvariant())
			return 12.99f;
		else if(productId.ToLowerInvariant() == ProductIdPack6.ToLowerInvariant())
			return 29.99f;
        else if(productId.ToLowerInvariant() == StarterPackProductId6.ToLowerInvariant())
            return 1.99f;
		else
			return 0;
		#else
		string priceString = PlayerPrefs.GetString(string.Format("IAPMpprice{0}", productId.ToUpperInvariant()), "");
		if(string.IsNullOrEmpty(priceString))
			return 0f;

		float price;
		if(float.TryParse(priceString, out price))
			return price;
		else
			return 0f;
		#endif
	}

	void SetProductPrice(string productId, string priceString)
	{
		float price;
		if(!float.TryParse(priceString, out price))
		{
			priceString = "";
		}

		PlayerPrefs.SetString(string.Format("IAPMpprice{0}", productId.ToUpperInvariant()), priceString);
	}



	public void PurchaseProduct(string productId)
	{
		if(!Utils.HasInternet())
		{
			string msg = GameLanguage.LocalizedText("Internet connection required to make purchases");

			if(OnPurchaseFailed != null)
				OnPurchaseFailed(msg);

			AlertDialogUtil.ShowAlert("", msg, "OK");
		}
		else
		{
			if(printIAPManagerDebugMsg)
				Debug.LogFormat("IAPManager -> PurchaseProduct -> productId: {0}", productId);
				
			restoringPurchases = false;
			currentPurchasingProductID = productId;

			#if UNITY_IOS
			StoreKitBinding.requestProductData(new string[]{ productId.ToUpperInvariant() });
			#elif UNITY_ANDROID
				GoogleIAB.purchaseProduct( productId.ToLowerInvariant());
			#endif
		}
	}

	public void RestorePurchases()
	{
		if(!Utils.HasInternet())
		{
			string msg = GameLanguage.LocalizedText("Internet connection required");

			if(OnRestorePurchasesFailed != null)
				OnRestorePurchasesFailed(msg);

			AlertDialogUtil.ShowAlert("", msg, "OK");
		}
		else
		{
			if(printIAPManagerDebugMsg)
				Debug.Log("IAPManager -> RestorePurchases");
			
			restoringPurchases = true;

			#if UNITY_IOS
				StoreKitBinding.restoreCompletedTransactions();
			#elif UNITY_ANDROID
				GoogleIAB.queryInventory(GetAllProductIds());
			#endif
		}
	}

	string[] GetAllProductIds()
	{
        return productIds;
	}

	bool IsSupportedProductId(string productId)
	{
		if(string.IsNullOrEmpty(productId))
			return false;
		
		var allProductIds = GetAllProductIds();
		foreach(string packProductId in allProductIds)
		{
			if(packProductId.ToLowerInvariant() == productId.ToLowerInvariant())
				return true;
		}

		return false;
	}

	void RefreshProductPrices()
	{
		if(printIAPManagerDebugMsg)
			Debug.Log("IAPManager -> RefreshProductPrices");

		#if UNITY_IOS
			StoreKitBinding.requestProductData(GetAllProductIds());
		#elif UNITY_ANDROID
			GoogleIAB.queryInventory(GetAllProductIds());
		#endif
	}

	bool ShouldRefreshProductInfo()
	{
		bool result;

		if(!PlayerPrefs.HasKey(refreshProductInfoPrefKey))
		{
			result = true;
		}
		else
		{
			System.DateTime lastRefreshed = System.DateTime.UtcNow.AddYears(-1);
			string dateString = PlayerPrefs.GetString(refreshProductInfoPrefKey);

			if(System.DateTime.TryParseExact(dateString, "s", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out lastRefreshed))
			{
				int mins = (int)((System.DateTime.UtcNow - lastRefreshed).TotalMinutes);

				if(mins <= GameServerSettings.SharedInstance.SurferSettings.RefreshProductInfoEveryMins)
					result = false;
				else
					result = true;
			}
			else
				result = true;
		}

		return result;
	}

	void UpdateLastProductInfoRefreshDate()
	{
		PlayerPrefs.SetString(refreshProductInfoPrefKey, System.DateTime.UtcNow.ToString("s"));
		PlayerPrefs.Save();
	}

	#region StoreKitBinding Event Handlers

	#if UNITY_IOS

	void ProductListReceivedEventHandler(List<StoreKitProduct> productList)
	{
		if(printIAPManagerDebugMsg)
			Debug.Log("IAPManager -> ProductListReceivedEventHandler");

		bool pricesRefreshed = false;
		for(int i = 0; i < productList.Count; i++)
		{
			string productIdentifier  = productList[i].productIdentifier;

			if(IsSupportedProductId(productIdentifier))
			{
				pricesRefreshed = true;
				SetProductPriceString(productIdentifier, productList[i].formattedPrice);
				SetProductPrice(productIdentifier, productList[i].price);

				if(printIAPManagerDebugMsg)
					Debug.LogFormat("IAPManager -> ProductListReceivedEventHandler -> productId: {0} formattedPrice: {1}", productIdentifier, productList[i].formattedPrice);

				if(!string.IsNullOrEmpty(currentPurchasingProductID) && productIdentifier.Equals(currentPurchasingProductID, System.StringComparison.InvariantCultureIgnoreCase))
				{
					currentPurchasingProductID = "";

					IAPProductPurchaseRecord.ClearPurchaseRecord();
					IAPProductPurchaseRecord.PurchaseProductId = productIdentifier;
					IAPProductPurchaseRecord.CurrencyString = productList[i].currencyCode;
					IAPProductPurchaseRecord.Price = GetProductPrice(productIdentifier);

					if(printIAPManagerDebugMsg)
						Debug.LogFormat("IAPManager -> ProductListReceivedEventHandler -> PurchaseProduct: {0}", productIdentifier.ToUpper());
					
					StoreKitBinding.purchaseProduct(productIdentifier.ToUpper(), 1);
				}
			}
		}

		if(pricesRefreshed)
		{
			UpdateLastProductInfoRefreshDate();

			if(OnProductPricesRefreshed != null)
				OnProductPricesRefreshed();
		}
	}

	void PurchaseSuccessfulEventHandler(StoreKitTransaction theTransaction)
	{
		string productIdentifier = theTransaction.productIdentifier;

		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> PurchaseSuccessfulEventHandler -> productIdentifier: {0}", productIdentifier);
		
		if(IsSupportedProductId(productIdentifier))
		{
			GameState.SharedInstance.RemoveAds();

			if(restoringPurchases)
			{
				restoringPurchases = false;

				if(OnRestorePurchasesSuccessful != null)
					OnRestorePurchasesSuccessful(productIdentifier);
			}
			else
			{
				IAPProductPurchaseRecord.VerifiedPurchaseProductId = productIdentifier;
				IAPProductPurchaseRecord.PurchaseTransactionIdentifier = theTransaction.transactionIdentifier;
				IAPProductPurchaseRecord.PurchaseTransactionData = theTransaction.transactionIdentifier;

				AnalyticsAndCrossPromoManager.SharedInstance.LogIAP();

				if(OnPurchaseSuccessful != null)
					OnPurchaseSuccessful(productIdentifier);
			}
		}
	}



	void PurchaseFailedEventHandler(string errorString)
	{
		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> PurchaseFailedEventHandler -> errorString: {0}", errorString);
		
		IAPProductPurchaseRecord.ClearPurchaseRecord();

		string msg = GameLanguage.LocalizedText("Purchase attempt unsuccessful");
		string errorMsg = string.IsNullOrEmpty(errorString) ? msg : string.Format("{0}. {1}", msg, errorString);

		if(OnPurchaseFailed != null)
			OnPurchaseFailed(errorMsg);

		AlertDialogUtil.ShowAlert("", errorMsg, "OK");
	}
	#endif

	#endregion

	void PurchaseCancelledEventHandler(string errorString)
	{
		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> PurchaseCancelledEventHandler -> errorString: {0}", errorString);
		
		IAPProductPurchaseRecord.ClearPurchaseRecord();

		string msg = GameLanguage.LocalizedText("Purchase cancelled");
		string errorMsg = string.IsNullOrEmpty(errorString) ? msg : string.Format("{0}. {1}", msg, errorString);
	
		if(OnPurchaseCancelled != null)
			OnPurchaseCancelled(errorMsg);

		AlertDialogUtil.ShowAlert("", errorMsg, "OK");
	}

	void RestoreTransactionsFinishedEventHandler()
	{
		if(printIAPManagerDebugMsg)
			Debug.Log("IAPManager -> RestoreTransactionsFinishedEventHandler");

		if(OnRestorePurchasesFinished != null)
			OnRestorePurchasesFinished();

		AlertDialogUtil.ShowAlert("", "Restore complete", "OK");
	}

	void RestoreTransactionsFailedEventHandler(string errorString)
	{
		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> RestoreTransactionsFailedEventHandler -> errorString: {0}", errorString);

		restoringPurchases = false;

		string msg = GameLanguage.LocalizedText("Restore failed");
		string errorMsg = string.IsNullOrEmpty(errorString) ? msg : string.Format("{0}. {1}", msg, errorString);

		if(OnRestorePurchasesFailed != null)
			OnRestorePurchasesFailed(errorMsg);

		AlertDialogUtil.ShowAlert("", errorMsg, "OK");
	}



	#if UNITY_ANDROID

	void BillingSupportedEventHandler()
	{
		if(printIAPManagerDebugMsg)
			Debug.Log("IAPManager -> BillingSupportedEventHandler");
	}

	void QueryInventorySucceededEventHandler(List<GooglePurchase> productsOwned, List<GoogleSkuInfo> productQueries)
	{
		if(printIAPManagerDebugMsg)
			Debug.Log("IAPManager -> QueryInventorySucceededEventHandler");
		
		// Refreshing prices
		bool pricesRefreshed = false;

		foreach(GoogleSkuInfo skuInfo in productQueries)
		{
			string productIdentifier = skuInfo.productId;

			if(IsSupportedProductId(productIdentifier))
			{
				if(printIAPManagerDebugMsg)
					Debug.LogFormat("IAPManager -> QueryInventorySucceededEventHandler -> productId: {0} price: {1}", productIdentifier, skuInfo.price);
				
				pricesRefreshed = true;
				SetProductPriceString(productIdentifier, skuInfo.price);
				SetProductPrice(productIdentifier, skuInfo.price);
			}
		}
			
		if(pricesRefreshed)
		{
			UpdateLastProductInfoRefreshDate();

			if(OnProductPricesRefreshed != null)
				OnProductPricesRefreshed();
		}


		// Restoring purchases
		if(restoringPurchases)
		{
			restoringPurchases = false;

			string[] productIDs = GetAllProductIds();

			foreach(GooglePurchase googlePurchase in productsOwned)
			{
				for (int i = 0; i < productIds.Length; i++)
				{
					if(googlePurchase.productId.Equals(productIDs[i], System.StringComparison.InvariantCultureIgnoreCase))
					{
						if(printIAPManagerDebugMsg)
							Debug.LogFormat("IAPManager -> QueryInventorySucceededEventHandler -> RestoringPurchaset -> productId: {0}", googlePurchase.productId);
						
						if(IsConsumableProduct(googlePurchase.productId))
						{
							GoogleIAB.consumeProduct(googlePurchase.productId.ToLowerInvariant());
						}

						// // quang don't need to restore android RestoreStarterPackPurchase(productIDs[i]);

						if(OnRestorePurchasesSuccessful != null)
							OnRestorePurchasesSuccessful(productIDs[i]);
					}
				}
			}
		}
	}

	void PurchaseSucceededEventHandler(GooglePurchase googlePurchase)
	{
		string[] productIDs = GetAllProductIds();

		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> PurchaseSucceededEventHandler -> productId: {0}", googlePurchase.productId);
		
		for (int i = 0; i < productIDs.Length; i++)
		{
			if(googlePurchase.productId.Equals(productIDs[i], System.StringComparison.InvariantCultureIgnoreCase))
			{
				if(IsConsumableProduct(googlePurchase.productId))
				{
					GoogleIAB.consumeProduct(googlePurchase.productId.ToLowerInvariant());
				}

				if(OnPurchaseSuccessful != null)
					OnPurchaseSuccessful(googlePurchase.productId);
			}
		}
	}


	void PurchaseFailedEventHandler(string message, int response)
	{
		if(printIAPManagerDebugMsg)
			Debug.LogFormat("IAPManager -> PurchaseFailedEventHandler -> Message {0}: {1}", response, message);
		
		if(OnPurchaseFailed != null)
			OnPurchaseFailed(message);
		
		if(!string.IsNullOrEmpty(message) && message.ToLowerInvariant().Contains("already owned"))
		{
			EtceteraAndroid.showAlert(
				"",
				string.Format("{0}: {1}", response, message),
				GameLanguage.LocalizedText("OK"));
		
		}
		else
		{
			Debug.LogFormat("IAPManager -> PurchaseFailedEventHandler -> errorString: {0}", message);
		
			IAPProductPurchaseRecord.ClearPurchaseRecord();

			string msg = GameLanguage.LocalizedText("Purchase attempt unsuccessful");

			EtceteraAndroid.showAlert("", 
							msg,
							GameLanguage.LocalizedText("OK"));
		}

	}

	#endif

}
