﻿using UnityEngine;
using System.Collections;
using TMPro;

public class FigetSpinnerManager : MonoBehaviour {

	public Camera mainCamera = null;
	public Transform fidgetSpinnerObject = null;
	public GameObject tutorialHand = null;
	public ParticleSystem coinEffect = null;
	public GameCenterPluginManager gameCenterPluginManager = null;

	public Renderer normalSpinnerRenderer;
	public Renderer blurredSpinnerRenderer;

	public BackButton backButton;

	public AudioSource reggaeMusic;
    public AudioSource inGameMusic;
    public AudioSource miniGameMusic;
	public AudioSource waveSounds;

	public AudioSource sfxSpinnerMove;
	public AudioSource sfxCashRegister;

	public TextMeshPro spinText = null;
	public TextMeshPro spinNumberText = null;

	private int spins = 0;

	private int spinGoalsReached = 0;
	private int spinGoal = 200;

	private bool updatedGamesPlayed = false;
	private bool showingTutorial = false;

	private float curRotation = 0;
	private float spinVelocity = 0;
	private float spinFriction = 400.0f;
	private float maxSpinVelocity = 4000.0f;

	private bool isSwiping = false;
	private bool playedSwipeSound = false;
	private Vector3 lastSwipePosition = Vector3.zero;
	private Vector3 nextSwipePosition = Vector3.zero;
	private float timeSwiping = 0.0f;

	private float lastDist = 0;
	private bool givenBigBonus = false;

	void Start () 
	{
		bool playedBefore = GameState.IsFirstFidgetSpinner();

		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();

		backButton.onBackButtonPress += SaveAudio;

		normalSpinnerRenderer.enabled = true;
		blurredSpinnerRenderer.enabled = false;

		if (playedBefore)
		{
			//do tutorial thing
			showingTutorial = true;
		}
		else
			tutorialHand.gameObject.SetActive(false);

		if (GlobalSettings.currentMenuMusicIndex == 0)
        {
			GlobalSettings.PlayMusicFromTimePoint(reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
        }
        else if (GlobalSettings.currentMenuMusicIndex == 1)
        {
			GlobalSettings.PlayMusicFromTimePoint(miniGameMusic, GlobalSettings.miniGameMusicPlayPosition);
        }
        else if (GlobalSettings.currentMenuMusicIndex == 2)
        {
			GlobalSettings.PlayMusicFromTimePoint(inGameMusic, GlobalSettings.inGameMusicPlayPosition);
        }

        if (!GlobalSettings.musicEnabled)
        	waveSounds.Stop();

		spinNumberText.text = spins.ToString();

		//UIHelpers.SetTransformRelativeToCorner( UIHelpers.GUICorner.CORNER_TOP_LEFT, 2.0f, 1.5f, spinText.transform, mainCamera);
		//UIHelpers.SetTransformRelativeToCorner( UIHelpers.GUICorner.CORNER_TOP_LEFT, 3.0f, 1.5f, spinNumberText.transform, mainCamera);

		spinText.gameObject.SetActive(false);
		spinNumberText.gameObject.SetActive(false);



	}

	void OnDisable()
	{
		if (backButton != null)
			backButton.onBackButtonPress -= SaveAudio;
	}
	
	void Update () 
	{
		//figure new spins
		curRotation += spinVelocity * Time.deltaTime;

		if (spinVelocity > -0.1 && spinVelocity < 0.1)
			spinVelocity = 0;
		else if (spinVelocity > 0)
			spinVelocity -= spinFriction * Time.deltaTime;
		else if (spinVelocity < 0)
			spinVelocity += spinFriction * Time.deltaTime;

		if (Mathf.Abs(spinVelocity) > maxSpinVelocity)
		{
			if (spinVelocity > 0) 
				spinVelocity = maxSpinVelocity;
			else 
				spinVelocity = -maxSpinVelocity;
		}

		if (spinVelocity != 0.0f)
		{
			Quaternion newRotation = Quaternion.AngleAxis(spinVelocity * Time.deltaTime, Vector3.forward) * fidgetSpinnerObject.rotation;
			if (!float.IsNaN(newRotation.x + newRotation.y + newRotation.z + newRotation.w))
				fidgetSpinnerObject.rotation = newRotation;
		}

			if (Mathf.Abs(spinVelocity) < 0.35f * maxSpinVelocity)
			{
				normalSpinnerRenderer.enabled = true;
				blurredSpinnerRenderer.enabled = false;
			}
			else if (Mathf.Abs(spinVelocity) < 0.5f * maxSpinVelocity)
			{
				normalSpinnerRenderer.enabled = true;
				blurredSpinnerRenderer.enabled = true;
			}
			else
			{
				normalSpinnerRenderer.enabled = false;
				blurredSpinnerRenderer.enabled = true;
			}
		


		if ( Mathf.Abs(curRotation) > 360.0f)
		{
			float sign = Mathf.Sign(curRotation);

			int newSpins = (int)(Mathf.Abs(curRotation) / 360.0f);
			curRotation = (Mathf.Abs(curRotation) % 360) * sign;

			if (spins == 0)
			{
				tutorialHand.gameObject.SetActive(false);
				spinText.gameObject.SetActive(true);
				spinNumberText.gameObject.SetActive(true);
			}

			spins += newSpins;
			if (spins > (spinGoalsReached + 1) * spinGoal)
			{
				coinEffect.Emit(5);
				GameState.SharedInstance.AddCash(5);
				spinGoalsReached ++;
				GlobalSettings.PlaySound(sfxCashRegister); 
			}

			if (spins > 1000 && !givenBigBonus)
			{
				gameCenterPluginManager.TrackAndSubmitAchivementFidgetSpinner();
				GameState.SharedInstance.AddCash(250);
				coinEffect.Emit(250);
				givenBigBonus = true;
			}

			spinNumberText.text = spins.ToString();
		}

		//detect swipe
		if (Input.GetMouseButton(0) || Input.touches.Length != 0)
		{
			#if UNITY_EDITOR || UNITY_STANDALONE
				Vector3 inputPosition = Input.mousePosition;
			#else
				Vector2 touchPos = Input.touches[0].position;
				Vector3 inputPosition = new Vector3(touchPos.x, touchPos.y, 0);
			#endif

			inputPosition = mainCamera.ScreenToWorldPoint( inputPosition);

			if (!isSwiping)
			{
				lastSwipePosition = inputPosition;
				nextSwipePosition = inputPosition;
				isSwiping = true;
				timeSwiping = 0;
				lastDist = 0;

				sfxSpinnerMove.pitch = 1.0f + Random.Range( -0.1f, 0.1f);
				GlobalSettings.PlaySound(sfxSpinnerMove);
					playedSwipeSound = true;
			}
			else
			{
				float dist = Vector3.Distance(lastSwipePosition, nextSwipePosition);

				nextSwipePosition = inputPosition;
				timeSwiping += Time.deltaTime;
				if (Mathf.Abs(spinVelocity) < 1f)
				{
					if (lastSwipePosition.x < nextSwipePosition.x)
						fidgetSpinnerObject.rotation = Quaternion.AngleAxis(-(lastDist - dist) * 10.0f, Vector3.forward) * fidgetSpinnerObject.rotation;
					else
						fidgetSpinnerObject.rotation = Quaternion.AngleAxis((lastDist - dist) * 10.0f, Vector3.forward) * fidgetSpinnerObject.rotation;

					lastDist = dist;
				}
			}
		}
		else if (isSwiping)
		{
			//Debug.Log("end swiping");

			float sign = (lastSwipePosition.x < nextSwipePosition.x) ? 1.0f : -1.0f;
			float addedVelocity = Vector3.Distance(lastSwipePosition, nextSwipePosition) / Mathf.Max( 0.001f, timeSwiping);
			spinVelocity += addedVelocity * sign * 10f;

			playedSwipeSound = false;
			timeSwiping = 0;
			lastDist = 0;
			lastSwipePosition = Vector3.zero;
			nextSwipePosition = Vector3.zero;
			isSwiping = false;
		}
	}

	public void SaveAudio()
	{
        if (GlobalSettings.currentMenuMusicIndex == 0)
        {
            GlobalSettings.reggaeMusicPlayPosition = reggaeMusic.time;
        }
        else if (GlobalSettings.currentMenuMusicIndex == 1)
        {
            GlobalSettings.miniGameMusicPlayPosition = miniGameMusic.time;
        }
        else if (GlobalSettings.currentMenuMusicIndex == 2)
        {
            GlobalSettings.inGameMusicPlayPosition = inGameMusic.time;
        }
	}
}
