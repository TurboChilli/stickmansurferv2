﻿using UnityEngine;
using System.Collections;

public class CarPlayer : MonoBehaviour {

    public CarGameController cgc;


    void OnTriggerEnter(Collider other)
    {
        // hit something!
        Debug.Log("***************** HIT:" + other);
        if (other.tag.Equals("EnemyCar"))
        {
            cgc.OnCrash();
        }
    }
}
