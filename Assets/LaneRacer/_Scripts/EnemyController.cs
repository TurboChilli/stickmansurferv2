﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public GameObject carEnemy;
    public GameObject truckEnemy;
    public GameObject motoEnemy;
    public enum Enemy
    {
        Car,
        Moto,
        Truck
    };

    public void SetRandomEnemy()
    {
        int randEnemy = Random.Range(0,3);
        if(randEnemy == 0)
        {
            SetEnemyType(Enemy.Truck);
        }
        else if(randEnemy == 1)
        {
            SetEnemyType(Enemy.Moto);
        }
        else
        {
            SetEnemyType(Enemy.Car);
        }
    }
    public void SetEnemyType(Enemy e)
    {
        carEnemy.SetActive(false);
        truckEnemy.SetActive(false);
        motoEnemy.SetActive(false);

        if(e == Enemy.Car)
        {
            carEnemy.SetActive(true);
        }
        else if(e == Enemy.Moto)
        {
            motoEnemy.SetActive(true);
        }
        else if(e == Enemy.Truck)
        {
            truckEnemy.SetActive(true);
        }
    }
	
}
