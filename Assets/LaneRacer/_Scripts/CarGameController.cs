﻿using UnityEngine;
using System.Collections;

public class CarGameController : MonoBehaviour {

    public GameObject playerCar;
    public GameObject terrain1Master;
    public GameObject enemy1Master;
    public GameObject crashedFrame;


    public AudioSource motorSound;
    public AudioSource screechSound;
    public AudioSource crashSound;
    public AudioSource errorSound;
    public AudioSource sfxFanAmbient;
    public AudioSource startSound;

    public GameObject coinMaster;

    public Camera carCamera;

    public GameObject menuTitle;
    public TextMesh lastScore;
    public TextMesh highScore;
    public GameObject tapToStartText;
    public TextMesh distanceText;

    public ParticleSystem consoleSmoke;
    public GameObject consoleFan;
    public GameObject errorScreen;

    GameCenterPluginManager gameCenter;

    bool leftLane = true;
    bool changingLanes = false;
    float rightLaneXPos = 5f;
    float leftLaneXPos = 1f;
    float laneChangeXVelocity = 8f;
    float laneWidth = 1.6f;
    float playerYVelocity = 0f;
    float playerYStartVelocity = 5f;
    float maxPlayerYVelocity = 10.75f;
    float maxMotorPitch = 2.6f;
    float terrainHeight = 10;
    GameObject[] terrainArray;
    GameObject[] enemyPool;
    GameObject[] coinPool;
    float startYPos = 0;
    float distanceCounter = 0;
    bool crashed = false;
    float startPitch = 0.8f;
    float maxPitch = 2f;

	private GameConsoleTimer gameConsoleTimer;

	// Use this for initialization
	void Start () {

		GameState.SharedInstance.HasPlayedGameConsole = true;

		if (Utils.IphoneXCheck())
		{
			distanceText.transform.position += Vector3.down * 0.5f;
			distanceText.transform.position += Vector3.right * 0.25f;
		}

		GameObject gameConsoleTimerObject = new GameObject("GameConsoleTimer");
		gameConsoleTimer = gameConsoleTimerObject.AddComponent<GameConsoleTimer>();
		gameConsoleTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.GameConsole));

        if (gameCenter == null)
            gameCenter = FindObjectOfType<GameCenterPluginManager>();
        
       // Time.timeScale = 0.1f;
        Application.targetFrameRate = 60;
        motorSound.pitch = startPitch;
        startYPos = playerCar.transform.position.y;
        leftLaneXPos = playerCar.transform.position.x;
        rightLaneXPos = leftLaneXPos + laneWidth;
        crashedFrame.SetActive(false);
        terrainArray = new GameObject[3];

        currentCosmetic = terrain1Master.GetComponent<CosmeticManager>();
        currentCosmetic.SetCosmeticItem(CosmeticManager.CosmeticItem.None);
       
        terrainArray[0] = GameObject.Instantiate(terrain1Master);
        terrainArray[1] = GameObject.Instantiate(terrain1Master);
        terrainArray[2] = GameObject.Instantiate(terrain1Master);

        currentCosmetic = terrainArray[0].GetComponent<CosmeticManager>();
        SetRandomCosmetic(currentCosmetic);

        currentCosmetic = terrainArray[1].GetComponent<CosmeticManager>();
        SetRandomCosmetic(currentCosmetic);

        currentCosmetic = terrainArray[2].GetComponent<CosmeticManager>();
        SetRandomCosmetic(currentCosmetic);

        terrainArray[0].transform.position = terrain1Master.transform.position;
        terrainArray[1].transform.position = new Vector3(terrainArray[0].transform.position.x, terrainArray[0].transform.position.y + terrainHeight, terrainArray[1].transform.position.z);
        terrainArray[2].transform.position = new Vector3(terrainArray[0].transform.position.x, terrainArray[1].transform.position.y + terrainHeight, terrainArray[2].transform.position.z);

        enemyPool = new GameObject[5];


        for (int i = 0; i < enemyPool.Length; i++)
        {
            
                enemyPool[i] = GameObject.Instantiate(enemy1Master);
           
                ec = enemyPool[i].GetComponent<EnemyController>();
                ec.SetRandomEnemy();

            //PositionEnemy(enemyPool[i], enemyReferencePointY);
           // enemyReferencePointY = enemyPool[i].transform.position.y;
        }
        //Time.timeScale = 0.2f;
        /*
        coinPool = new GameObject[10];
        for (int i = 0; i < coinPool.Length; i++)
        {
            coinPool[i] = GameObject.Instantiate(coinMaster);
            //coinMaster.transform.position = new Vector3(coinMaster.transform.position.x, coinMaster.transform.position.y, playerCar.transform.position.z);
        }
        */
        terrain1Master.SetActive(false);

        ShowMenu();

        errorScreen.SetActive(false);

        consoleSmoke.Stop();
        consoleSmoke.gameObject.SetActive(false);

        currentDistance = 0;
        UpdateDistance();

        playerYVelocity = playerYStartVelocity;

        UpgradeItem hasFan = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.RetroFan);
        bool hasFanTrue = false;
        if (hasFan != null)
            hasFanTrue = (hasFan.CurrentUpgradeStage != 0);

        if (hasFanTrue)
        {
            curGameTimer = GameServerSettings.SharedInstance.SurferSettings.GameConsoleMaxGameTime * 2.0f;
            consoleFan.SetActive(true);
            GlobalSettings.PlaySound(sfxFanAmbient);
        }
        else
        {
            curGameTimer = GameServerSettings.SharedInstance.SurferSettings.GameConsoleMaxGameTime;
            consoleFan.SetActive(false);
        }

        if (PlayerPrefs.HasKey("SS_CarGameHighscore"))
            highScore.text = string.Format("HIGH:{0}", PlayerPrefs.GetInt("SS_CarGameHighscore", 0));
        else
            highScore.gameObject.SetActive(false);

        GlobalSettings.PlaySound(startSound);
	}
    private float errorScreenTimer = 5.0f;

    float enemyReferencePointY = 0;
    float coinReferencePointY = 0;
    float crashTimer = 0;
    float crashDelay = 2;

    bool menuMode = false;
    bool gameErrored = false;


	// Update is called once per frame
	void Update () 
    {
        if (gameErrored)
        {
            if (errorScreenTimer > 0)
                errorScreenTimer -= Time.deltaTime;
            else
            {
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)foregroundCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                droidLoader.Show();
                }
                #endif
                SceneNavigator.NavigateTo(SceneType.Shack);
            }
            return;
        }

        if (crashed)
        {
            crashTimer += Time.deltaTime;
            if (crashTimer > crashDelay)
            {
                crashTimer = 0;
                ShowMenu();

            }

            return;
        }
            
        if (menuMode)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //screechSound.Play();
                ShowIngame();
            }
        }
        playerCar.transform.Translate(0, playerYVelocity * Time.deltaTime, 0f);
        if (!menuMode)
        {
            UpdateControls();
        }

        UpdateTerrain();

        if (!menuMode)
        {
            UpdateEnemies();
            //UpdateCoins();
        }

        UpdateCamera();

        if (!menuMode)
        {
            playerYVelocity += 0.3f * Time.deltaTime;
            if (playerYVelocity > maxPlayerYVelocity)
            {
                playerYVelocity = maxPlayerYVelocity;
            }

            UpdateDistance();


            motorSound.pitch += 0.05f * Time.deltaTime;
            if (motorSound.pitch > maxMotorPitch)
            {
                motorSound.pitch = maxMotorPitch;
            }
            //Debug.Log("Speed:" + playerYVelocity + " pitch:" + motorSound.pitch);
        }

        if (curGameTimer > 0)
        {
            curGameTimer -= Time.deltaTime;
            if (curGameTimer <= 0)
            {
                consoleSmoke.gameObject.SetActive(true);
                consoleSmoke.Play();
            }
        }

        if (menuMode)
        {
            curStartMessageTime -= Time.deltaTime;
            if (curStartMessageTime <= 0)
            {
                curStartMessageTime = startMessageTime;
                if (startMessageOn)
                {
                    startMessageOn = false;
                    tapToStartText.gameObject.SetActive(false);
                }
                else
                {
                    startMessageOn = true;
                    tapToStartText.gameObject.SetActive(true);
                }
            }
        }
       // Debug.Log("Speed;" + playerYVelocity);
	}

    float curStartMessageTime;
    float startMessageTime = 0.7f;
    bool startMessageOn = false;

    float curGameTimer = 200;
    GameObject tempTerrain;

    void ShowMenu()
    {
        menuTitle.SetActive(true);
        lastScore.gameObject.SetActive(false);
        highScore.gameObject.SetActive(true);
        crashedFrame.SetActive(false);
        //ResetGame();
        menuMode = true;
        crashed = false;

        for (int i = 0; i < enemyPool.Length; i++)
        {
            enemyPool[i].transform.position = new Vector3(enemyPool[i].transform.position.x,-999,enemyPool[i].transform.position.z);
        }
        /*
        for (int i = 0; i < coinPool.Length; i++)
        {
            coinPool[i].transform.position = new Vector3(coinPool[i].transform.position.x,-999,coinPool[i].transform.position.z);
        }
        */
        //motorSound.pitch = startPitch;
        tapToStartText.SetActive(true);

        Renderer rend = playerCar.transform.GetComponent<Renderer>();
        rend.enabled = true;

        playerCar.transform.rotation = Quaternion.Euler(0, 0, 0);
        playerCar.transform.position = new Vector3(leftLaneXPos, playerCar.transform.position.y, playerCar.transform.position.z);

        playerYVelocity = playerYStartVelocity;

        leftLane = true;
        changingLanes = false;
        GlobalSettings.PlaySound(motorSound);
        motorSound.pitch = startPitch;

        highScore.text = "HIGH:" + PlayerPrefs.GetInt("SS_CarGameHighscore", 0);

    }

    void ShowIngame()
    {
        enemyReferencePointY = playerCar.transform.position.y + terrainHeight * 2;
        for (int i = 0; i < enemyPool.Length; i++)
        {
            //enemyPool[i] = GameObject.Instantiate(enemy1Master);
            PositionEnemy(enemyPool[i], enemyReferencePointY);
            enemyReferencePointY = enemyPool[i].transform.position.y;
        }
        /*
        for (int i = 0; i < coinPool.Length; i++)
        {
            //enemyPool[i] = GameObject.Instantiate(enemy1Master);
            PositionCoin(coinPool[i], coinReferencePointY);
            coinReferencePointY = coinPool[i].transform.position.y;
        }
        */
        menuTitle.SetActive(false);
        lastScore.gameObject.SetActive(false);
        highScore.gameObject.SetActive(false);
        menuMode = false;
        tapToStartText.SetActive(false);

        currentDistance = 0;
        startYPos = playerCar.transform.position.y;
        UpdateDistance();


        playerYVelocity = playerYStartVelocity;

        if (curGameTimer <= 0)
        {
            errorScreen.SetActive(true);
            GlobalSettings.PlaySound(errorSound);
            gameErrored = true;
            distanceText.gameObject.SetActive(false);
            motorSound.Stop();
            screechSound.Stop();
            inputCooldown = 0.5f;
        }
    }


    float inputCooldown = 0;
    float currentDistance = 0;
    void UpdateDistance()
    {
        currentDistance = playerCar.transform.position.y - startYPos;
        //currentDistance *= 0.7f;
       // distance *= 2f;
            distanceText.text = "" + (int)currentDistance;
    }

    CosmeticManager currentCosmetic;

    void UpdateTerrain()
    {
        if (terrainArray[0].transform.position.y < playerCar.transform.position.y - terrainHeight)
        {
            // terrain 0 scrolled off screen so recyle it

            terrainArray[0].transform.position = new Vector3(terrainArray[0].transform.position.x, terrainArray[2].transform.position.y + terrainHeight, terrainArray[0].transform.position.z);


            tempTerrain = terrainArray[0];
            terrainArray[0] = terrainArray[1];
            terrainArray[1] = terrainArray[2];
            terrainArray[2] = tempTerrain;

            currentCosmetic = terrainArray[2].GetComponent<CosmeticManager>();
            SetRandomCosmetic(currentCosmetic);
        }
    }

    void SetRandomCosmetic(CosmeticManager cm)
    {
        /*
        if (currentDistance < 60)
        {
            int randVal = Random.Range(0, 22);
            if (randVal < 4)
            {
                cm.SetCosmeticItem(CosmeticManager.CosmeticItem.LeftLake);
            }
            else if (randVal < 8)
            {
                cm.SetCosmeticItem(CosmeticManager.CosmeticItem.RightLake);
            }
        }
        else*/ 

        int randVal = Random.Range(0, 30);
        if (randVal < 4)
        {
            cm.SetCosmeticItem(CosmeticManager.CosmeticItem.LeftHouse);
        }
        else if (randVal < 9)
        {
            cm.SetCosmeticItem(CosmeticManager.CosmeticItem.RightHouse);
        }
        else if (randVal < 11)
        {
            cm.SetCosmeticItem(CosmeticManager.CosmeticItem.LeftLake);
        }
        else if (randVal < 14)
        {
            cm.SetCosmeticItem(CosmeticManager.CosmeticItem.RightLake);
        }
        else if (randVal < 16)
        {
            cm.SetCosmeticItem(CosmeticManager.CosmeticItem.Sign);
        }


    }

    void UpdateControls()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!changingLanes)
            {
                ChangeLanes();
            }
        }

        if (changingLanes)
        {
            UpdateChangingLanes();
        }
    }

    void PositionEnemy(GameObject theEnemy, float yReferencePoint)
    {
        if (currentDistance < 60)
        {
            if (Random.Range(0, 2) == 0)
            {
                float distanceAheadY = Random.Range(terrainHeight / 1.5f, terrainHeight * 2);
                theEnemy.transform.position = new Vector3(leftLaneXPos, yReferencePoint + distanceAheadY, theEnemy.transform.position.z);
            }
            else
            {
                float distanceAheadY = Random.Range(terrainHeight / 1.5f, terrainHeight * 2);
                theEnemy.transform.position = new Vector3(rightLaneXPos, yReferencePoint + distanceAheadY, theEnemy.transform.position.z);
            }
        }
        else
        {
            if (Random.Range(0, 2) == 0)
            {
                float distanceAheadY = Random.Range(terrainHeight, terrainHeight * 2);
                theEnemy.transform.position = new Vector3(leftLaneXPos, yReferencePoint + distanceAheadY, theEnemy.transform.position.z);
            }
            else
            {
                float distanceAheadY = Random.Range(terrainHeight, terrainHeight * 2);
                theEnemy.transform.position = new Vector3(rightLaneXPos, yReferencePoint + distanceAheadY, theEnemy.transform.position.z);
            }
        }
    }

    void PositionCoin(GameObject theCoin, float yReferencePoint)
    {
        
        if (Random.Range(0, 2) == 0)
        {
            //float distanceAheadY = Random.Range(terrainHeight/2, terrainHeight);
            float distanceAheadY = 2;
            theCoin.transform.position = new Vector3(leftLaneXPos, yReferencePoint + distanceAheadY, theCoin.transform.position.z);
        }
        else
        {
            //float distanceAheadY = Random.Range(terrainHeight/2, terrainHeight);
            float distanceAheadY = 2;
            theCoin.transform.position = new Vector3(rightLaneXPos, yReferencePoint + distanceAheadY, theCoin.transform.position.z);
        }

    }

    EnemyController ec;
    GameObject enemyTemp;
    void UpdateEnemies()
    {
        if (enemyPool[0].transform.position.y < playerCar.transform.position.y - terrainHeight)
        {
            ec = enemyPool[0].GetComponent<EnemyController>();
            ec.SetRandomEnemy();

            PositionEnemy(enemyPool[0], enemyPool[4].transform.position.y);
            enemyTemp = enemyPool[0];

            enemyPool[0] = enemyPool[1];
            enemyPool[1] = enemyPool[2];
            enemyPool[2] = enemyPool[3];
            enemyPool[3] = enemyPool[4];
            enemyPool[4] = enemyTemp;

        }

        for(int i =0; i < enemyPool.Length; i++)
        {
            enemyPool[i].transform.position = new Vector3(enemyPool[i].transform.position.x, enemyPool[i].transform.position.y - 3f * Time.deltaTime, enemyPool[i].transform.position.z);

        }
    }

    void UpdateCoins()
    {
        if (coinPool[0].transform.position.y < playerCar.transform.position.y - terrainHeight)
        {
            
            PositionCoin(coinPool[0], coinPool[4].transform.position.y);
            enemyTemp = coinPool[0];

            coinPool[0] = coinPool[1];
            coinPool[1] = coinPool[2];
            coinPool[2] = coinPool[3];
            coinPool[3] = coinPool[4];
            coinPool[4] = coinPool[5];
            coinPool[5] = coinPool[6];
            coinPool[6] = coinPool[7];
            coinPool[7] = coinPool[8];
            coinPool[8] = coinPool[9];
            coinPool[9] = enemyTemp;

        }
        /*
        for(int i =0; i < coinPool.Length; i++)
        {
            coinPool[i].transform.position = new Vector3(coinPool[i].transform.position.x, coinPool[i].transform.position.y - 3f * Time.deltaTime, coinPool[i].transform.position.z);

        }
        */
    }

    void UpdateChangingLanes()
    {
        if (leftLane)
        {
            //Debug.Log("CHANGING FROM LEFT:" + playerCar.transform.position.x);
            // moving to right lane
            playerCar.transform.position = new Vector3(playerCar.transform.position.x + laneChangeXVelocity * Time.deltaTime, playerCar.transform.position.y, playerCar.transform.position.z);
            playerCar.transform.rotation = Quaternion.Euler(0, 0, -8);
            if (playerCar.transform.position.x >= rightLaneXPos)
            {
                
                playerCar.transform.position = new Vector3(rightLaneXPos, playerCar.transform.position.y, playerCar.transform.position.z);
                leftLane = false;
                changingLanes = false;
                OnChangLaneComplete();
            }
        }
        else
        {
            playerCar.transform.position = new Vector3(playerCar.transform.position.x - laneChangeXVelocity * Time.deltaTime, playerCar.transform.position.y, playerCar.transform.position.z);
            playerCar.transform.rotation = Quaternion.Euler(0, 0, 8);
            if (playerCar.transform.position.x <= leftLaneXPos)
            {
                playerCar.transform.position = new Vector3(leftLaneXPos, playerCar.transform.position.y, playerCar.transform.position.z);
                leftLane = true;
                changingLanes = false;
                OnChangLaneComplete();
            }
        }

            
    }

    public void OnCrash()
    {
        crashed = true;
        distanceText.text = distanceText.text;
        motorSound.Stop();
        GlobalSettings.PlaySound(crashSound);
        crashedFrame.SetActive(true);
        //playerCar.SetActive(false);
        Renderer rend = playerCar.transform.GetComponent<Renderer>();
        rend.enabled = false;

        GameState.SharedInstance.AddCash((int)currentDistance);
        gameCenter.SubmitHighScore( (int)currentDistance, LeaderboardType.CarGameBoard);

        if (PlayerPrefs.GetInt("SS_CarGameHighscore", 0) < (int)currentDistance)
            PlayerPrefs.SetInt("SS_CarGameHighscore", (int)currentDistance);
    }

    void OnChangLaneComplete()
    {
        playerCar.transform.rotation = Quaternion.Euler(0, 0, 0);
    }



    void ChangeLanes()
    {
        changingLanes = true;
        GlobalSettings.PlaySound(screechSound);
    }


    void UpdateCamera()
    {
        carCamera.transform.position = new Vector3(playerCar.transform.position.x, playerCar.transform.position.y + 2f, carCamera.transform.position.z);
        //Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, transform.position.y + 2f, Camera.main.transform.position.z);
    }
}
