﻿using UnityEngine;
using System.Collections;

public class CosmeticManager : MonoBehaviour {

    public GameObject[] cosmeticItemList;

    public enum CosmeticItem
    {
        LeftHouse,
        RightHouse,
        LeftLake,
        RightLake,
        Sign,
        None

    };

	// Use this for initialization
	void Start () {
	
	}
	
    public void SetCosmeticItem(CosmeticItem ci)
    {
        for (int i = 0; i < cosmeticItemList.Length; i++)
        {
            cosmeticItemList[i].SetActive(false);
        }

        int ciVal = (int)ci;
        if (ciVal < cosmeticItemList.Length)
        {
            cosmeticItemList[ciVal].SetActive(true);
        }

    }

}
