﻿using UnityEngine;
using System.Collections;

public class GameMenuController : MonoBehaviour 
{
    bool playCarGame = true;

    public GameObject carGameParent;
    public GameObject snowboardGameParent;

    public GameObject snowGameButton;
    public GameObject carGameButton;
    public GameObject fillBackground;

    public GameObject selectText;

    public GameObject smoke;
    public GameObject consoleFan;

    public AudioSource sfxFanAmbient;
    public AudioSource menuSound;

	public Camera foregroundCamera = null;
	public Camera snowboardCamera = null;
    public Camera lanerRacerCamera = null;

	// Use this for initialization
	void Awake () 
    {
        /*
        if(playCarGame)
        {
            carGameParent.SetActive(true);
            snowboardGameParent.SetActive(false);
        }
        else
        {
            carGameParent.SetActive(false);
            snowboardGameParent.SetActive(true);
        }
        */

        if (Utils.IphoneXCheck())
        {
			foregroundCamera.transform.position += Vector3.down * 0.45f;
			snowboardCamera.rect = new Rect(0.04f, 0.06f, 0.8f, 0.9f);
			lanerRacerCamera.rect = new Rect(0.04f, 0.06f, 0.8f, 0.9f);
		}
        carGameParent.SetActive(false);
        snowboardGameParent.SetActive(false);
        smoke.SetActive(false);

        UpgradeItem hasFan = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.RetroFan);
        bool hasFanTrue = false;
        if (hasFan != null)
            hasFanTrue = (hasFan.CurrentUpgradeStage != 0);

        if (hasFanTrue)
        {
            consoleFan.SetActive(true);
            GlobalSettings.PlaySound(sfxFanAmbient);
        }
        else
        {
            //curGameTimer = GameServerSettings.SharedInstance.SurferSettings.GameConsoleMaxGameTime;
            consoleFan.SetActive(false);
        }

	}
	
    void Start ()
    {
        GlobalSettings.PlaySound(menuSound);
    }

	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(UIHelpers.CheckButtonHit(snowGameButton.transform))
            {
                carGameParent.SetActive(false);
                snowboardGameParent.SetActive(true);
                snowGameButton.SetActive(false);
                carGameButton.SetActive(false);
                selectText.SetActive(false);
                fillBackground.SetActive(false);
            }
            else if(UIHelpers.CheckButtonHit(carGameButton.transform))
            {
                carGameParent.SetActive(true);
                snowboardGameParent.SetActive(false);
                snowGameButton.SetActive(false);
                carGameButton.SetActive(false);
                selectText.SetActive(false);
                fillBackground.SetActive(false);
            }
        }
	}
}
