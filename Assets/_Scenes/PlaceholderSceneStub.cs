﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlaceholderSceneStub : MonoBehaviour {

	private float coolDown = 0.5f;
	public string navigateToScene = "";

	void Start()
	{
		Time.timeScale = 1.0f;
	}

	void Update () 
	{
		if (Input.GetMouseButton(0) && coolDown < 0.0f)
		{
			if (navigateToScene == "")
				SceneNavigator.NavigateToPreviousScene();
			else
				SceneManager.LoadScene(navigateToScene);
			//////Debug.Log("Peace foo");
		}

		coolDown = coolDown - Time.deltaTime;
	}
}
