﻿using UnityEngine;
using System.Collections;

public class DroneBobbing : MonoBehaviour {

    private float markerSinCounter = 0;
    private float markerYSin = 0;
    private float markerYStartPos = 0;

    private bool bobbingEnabled = true;

    void Start()
    {
        markerYStartPos = transform.localPosition.y;
    }

    void Update () 
    {
        if(bobbingEnabled)
        {
            // WAVELENGTH
            markerSinCounter += Time.deltaTime * 4f;
            markerYSin = Mathf.Sin(markerSinCounter);

            // AMPLITUDE
            markerYSin /= 8f;
            transform.localPosition = new Vector3(transform.localPosition.x, markerYStartPos + markerYSin,transform.localPosition.z);
        }
    }
}
