﻿using UnityEngine;
using QuickTerrain2D; // *** do not forget include QuickTerrain2D namespace


//This is an exapmle of how to use custom generation scripts (WIP)
// *** STEP 1 
// implement ICustomGenerator interaface
public class CustomGenerator : MonoBehaviour, ICustomGenerator
{
    public GameObject jeepTutorial;
    QuickTerrain quick_terrain;
    Activity currentActivity;

    public NoiseList easyNoiseList;
    public NoiseList hardNoiseList;

    bool tutorialMode = false;
	float tutorialTimerCheck = 3f;

    public CarManager jeepCarManager;
    public CarManager quadBikeCarManager;

    void Awake()
    {
        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
            quadBikeCarManager.gameObject.SetActive(true);
            jeepCarManager.gameObject.SetActive(false);

        }
        else
        {
            quadBikeCarManager.gameObject.SetActive(false);
            jeepCarManager.gameObject.SetActive(true);
        }
    }

    void CheckForTutorial()
    {
        

        checkedForTutorial = true;
       // currentActivity = GameState.SharedInstance.GetCurrentActivity();

        jeepTutorial.SetActive(false);

        if (GlobalSettings.firstTimeJeepGame)
        {
            GlobalSettings.firstTimeJeepGame = false;
            jeepTutorial.SetActive(true);
            //////Debug.Log("SETTING TUTORIAL MODE TO TRUE");
            tutorialMode = true;
            quick_terrain.ReplaceNoise(easyNoiseList);

        }
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            jeepTutorial.SetActive(false);
        }

		if(tutorialTimerCheck >= 0)
		{
			tutorialTimerCheck -= Time.deltaTime;

			if(tutorialTimerCheck <= 0 && jeepTutorial != null)
			{
				if(jeepTutorial.activeSelf)
					jeepTutorial.SetActive(false);
			}
		}

		#if UNITY_EDITOR
        if(Input.GetKey(KeyCode.Space))
        {
        	if (Time.timeScale == 0)
            {   
                Time.timeScale = 1;
            }
            else
            {
                Time.timeScale = 0;
            }
        }
		#endif
    }

    public void InitiateGeneration(QuickTerrain _quick_terrain)
    {
       

        // *** STEP 2 
        // save quick_terrain reference
        quick_terrain = _quick_terrain;


        // *** STEP 3 
        // subscribe to events

        // This event sends before generation of a block starts
        quick_terrain.on_generation += MyPreGenerationHandler;

        // This events sends after generation of block finishes
        quick_terrain.on_generation_over += MyPostGenerationHandler;

        // This events sends after generation of an item finishes
        // This event is recommended to use when generated 
        // gameobjects do not have any custom scripts on them, 
        // othervise use implementation of IItemGenerationHandler instead
        quick_terrain.on_item_generated += MyItemGenerationHandler;

       
        // *** STEP 4 
        // drag and drop gameobject with this script to 
        // QuickTerrain 'generation sctipts' list.

        ////////Debug.Log("EEEEEEEEW");

       // quick_terrain.ReplaceNoise(easyNoiseList);
        //print("Custom generation sctipt initiated");
    }

    bool checkedForTutorial = false;
    void MyPreGenerationHandler(int index)
    {
        
        ////////Debug.Log("INDEX PRE GEN:" + index);
        if (!checkedForTutorial)
        {
            CheckForTutorial();
        }

        /*
        if (tutorialMode)
        {
            //////Debug.Log("tutorial mode is:" + tutorialMode);
            if(tutorialMode)
            {
                //////Debug.Log("LOADING EASY LIST HERE");
                quick_terrain.ReplaceNoise(easyNoiseList);
            }
            else
            {
                //////Debug.Log("LOADING HARD LIST HERE");
                quick_terrain.ReplaceNoise(hardNoiseList);
            }
        }
        */

        //print("After this procedure is over block with index " + index + " will be generated");
        //SampleModifyData[] mySampleData = new SampleModifyData[1];
        //mySampleData[0].amplitude = 10;
        //mySampleData[0].length = 33;
        //mySampleData[0].offset = 0;
        //mySampleData[0].smoothnes = 1;

        //NoiseList myNoiseList = new NoiseList();

        //quick_terrain.ReplaceNoise(
        //mySampleData[0].amplitude = 2f;
       // mySampleData[0].smoothnes = 1f;


        //quick_terrain.ModifyNoiseSampleAdd(mySampleData,0,0);

        //Usefull functions to use here:  
        //quick_terrain.RemoveItem()
        //quick_terrain.ReplaceNoise()
        //quick_terrain.AddItem()
        //quick_terrain.RemoveItem()
        //quick_terrain.ReplaceItems()
        //quick_terrain.ReplaceMeshMaterial()
        //quick_terrain.InsertCustomBlock()
        //quick_terrain.ReplaceMeshSetup()
        //quick_terrain.ModifyNoiseSampleAdd()
        //quick_terrain.ModifyNoiseSampleApply()
    }


    void MyPostGenerationHandler(Block block)
    {
        ////////Debug.Log("WWWWW WW W W W");
        //print("Block had generated");
        //print("Block name is " + block.name);
        //print("Block index is " + block.index);
        //print("Block tag is " + block.gameObject.tag);
        //print("Block width is " + block.width.ToString("F"));

        //  block.points - noise vertices that was used to generate block meshses;
    }


    void MyItemGenerationHandler(ItemGenerationData item_generation_data)
    {
        //print("Item just had generated!");
        //print("Item name is " + item_generation_data.item.name);
        //print("Item index is " + item_generation_data.index);
        //item_generation_data.block - block on which item is generated
        //print("Block name is " + item_generation_data.block.name);
    }
}
