﻿using UnityEngine;
using System.Collections;

public class waterSideUpDown : MonoBehaviour {

    private float markerSinCounter = 0;
    private float markerYSin = 0;
    private float markerYStartPos = 0;

    private bool bobbingEnabled = true;

    void Start()
    {
        markerYStartPos = transform.position.y;
    }

    void Update () 
    {
        if(bobbingEnabled)
        {
            // WAVELENGTH
            markerSinCounter += Time.deltaTime * 2f;
            markerYSin = Mathf.Sin(markerSinCounter);

            // AMPLITUDE
            markerYSin /= 2f;
            transform.position = new Vector3(transform.position.x, markerYStartPos + markerYSin,transform.position.z);
        }
    }
}
