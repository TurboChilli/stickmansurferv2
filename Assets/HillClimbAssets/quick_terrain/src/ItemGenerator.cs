﻿using UnityEngine;
using System.Collections;



namespace QuickTerrain2D
{
    [System.Serializable]
    public class ItemGenerator : ScriptableObject
    {
        public ItemSet itemset;

        void OnEnable()
        {
            if(itemset == null)
               itemset = new ItemSet();
            itemset.name = name;
        }
    }
}
