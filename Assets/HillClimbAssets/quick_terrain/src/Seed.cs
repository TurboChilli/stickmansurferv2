﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class Seed
    {
        public enum Type { master, constant, random };
        public int value;
        public Type type;

        public void initialize(int master)
        {
            if (type == Seed.Type.master)
                value = master;
            else if (type == Seed.Type.random)
                value = Random.Range(int.MinValue, int.MaxValue);
        }
    }
}
