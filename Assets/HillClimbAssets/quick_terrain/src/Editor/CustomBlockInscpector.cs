﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using QuickTerrain2D;


[CustomEditor(typeof(CustomBlock))]
public class CusotmBLockInspector : Editor
{
    private CustomBlock block;
    private Transform handle_transform;
    private Quaternion handle_rotation;

    private const float handle_size = 0.07f;
    private const float pick_size = 0.08f;

    private int selected_point = -1;

    Color[] point_color = {
		Color.white,
		Color.yellow,
		Color.cyan
	};

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        block = target as CustomBlock;

        EditorGUILayout.BeginHorizontal();
        if (EditorExtensions.button("add point", "add control point"))
        {
            Undo.RecordObject(block, "add point");
            block.add_arc();
            EditorUtility.SetDirty(block);
        }
        if (EditorExtensions.button("remove point", "renove control point"))
        {
            Undo.RecordObject(block, "remove point");
            block.remove_arc();
            EditorUtility.SetDirty(block);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("segments"), new GUIContent("segments"));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("type"), new GUIContent("insert type"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("override_height"), new GUIContent("override height"));

        EditorGUILayout.Space();

        SerializedProperty use_states = serializedObject.FindProperty("use_states");
        EditorGUILayout.PropertyField(use_states, new GUIContent("use states"));
        if (use_states.boolValue)
        {
            EditorGUILayout.BeginHorizontal();
            if (EditorExtensions.medium_button("fix 0", "fix block state at 0 value"))
                block.fix_state(0);

            if (block.has_both_states())
            {
                SerializedProperty state_preview = serializedObject.FindProperty("state_slider");
                float state = EditorGUILayout.Slider(block.state_slider, 0f, 1f);
                if (state != state_preview.floatValue)
                {
                    Vector3[] vs = block.interpolated_vartices(state);
                    SerializedProperty list = serializedObject.FindProperty("vs");
                    for (int i = 0; i < list.arraySize; i++)
                        list.GetArrayElementAtIndex(i).vector3Value = vs[i];
                    state_preview.floatValue = state;
                }
            }
            else
            {
                GUILayout.FlexibleSpace();
            }

            if (EditorExtensions.medium_button("fix 1", "fix block state at 1 value"))
                block.fix_state(1);
            EditorGUILayout.EndHorizontal();
        }

        if (block.blend_curve == null)
            block.blend_curve = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(1f, 1f));

        EditorGUILayout.PropertyField(serializedObject.FindProperty("insert_layer"), new GUIContent("insert layer"));
        if (block.type == CustomBlock.InsertType.addition || block.type == CustomBlock.InsertType.blend)
        {
            SerializedProperty curve = serializedObject.FindProperty("blend_curve");
            curve.animationCurveValue = EditorGUILayout.CurveField("blend curve", block.blend_curve, Color.green, new Rect(0, 0, 1f, 1f));
        }

        if (block.type == CustomBlock.InsertType.insert || block.type == CustomBlock.InsertType.blend)
        {
            EditorExtensions.ListDrawer(serializedObject.FindProperty("objects"), "objects",
            EditorExtensions.ListOptions.by_default ^ EditorExtensions.ListOptions.sorting);
        }


        if (selected_point >= 0 && selected_point < block.point_count)
            point_inspector();


        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        block = target as CustomBlock;
        handle_transform = block.transform;
        handle_rotation = Tools.pivotRotation == PivotRotation.Local ?
            handle_transform.rotation : Quaternion.identity;

        Vector3 p0 = point_scene(0);
        for (int i = 1; i < block.point_count; i += 3)
        {
            Vector3 p1 = point_scene(i);
            Vector3 p2 = point_scene(i + 1);
            Vector3 p3 = point_scene(i + 2);

            Handles.color = Color.gray;
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p2, p3);

            p0 = p3;
        }

    }


    private Vector3 point_scene(int i)
    {
        Vector3 p = handle_transform.TransformPoint(block.get_point(i));

        float size = HandleUtility.GetHandleSize(p);
        Color c = point_color[(int)block.get_point_mode(i)];
        if (i % 3 == 0)
        {
            size *= .9f;
            c -= new Color(.3f, .3f, .3f, 0f);
        }

        Handles.color = c;
        if (Handles.Button(p, handle_rotation, size * handle_size, size * pick_size, Handles.DotCap))
        {
            selected_point = i;
            Repaint();
        }

        if (selected_point == i)
        {
            EditorGUI.BeginChangeCheck();
            p = Handles.DoPositionHandle(p, handle_rotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(block, "Move Point");
                EditorUtility.SetDirty(block);
                block.set_point(i, handle_transform.InverseTransformPoint(p));
            }
        }
        return p;
    }


    private void point_inspector()
    {
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        GUILayout.Label("selected:");
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginHorizontal();
        
        Vector2 p = block.get_point(selected_point);
        EditorGUIUtility.labelWidth = 20;
        p.x = EditorGUILayout.FloatField("x", p.x);
        p.y = EditorGUILayout.FloatField("y", p.y);
        EditorGUIUtility.labelWidth = 0;
        
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(block, "Move Point");
            EditorUtility.SetDirty(block);
            block.set_point(selected_point, p);
        }

        EditorGUILayout.EndHorizontal();


        EditorGUI.BeginChangeCheck();

        EditorGUILayout.BeginHorizontal();
        CustomBlock.ControlPointMode mode = block.get_point_mode(selected_point);
        if(EditorExtensions.button("aligned", "aligned tangents"))
        {
            mode = CustomBlock.ControlPointMode.aligned;
        }
        if (EditorExtensions.button("mirrored", "mirrored tangents"))
        {
            mode = CustomBlock.ControlPointMode.mirrored;
        }
        if (EditorExtensions.button("broken", "broken tangents"))
        {
            mode = CustomBlock.ControlPointMode.broken;
        }
        EditorGUILayout.EndHorizontal();

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(block, "Change Point Mode");
            block.set_point_mode(selected_point, mode);
            EditorUtility.SetDirty(block);
        }
    }
}

