﻿using UnityEngine;
using System.Collections;
using UnityEditor;


namespace QuickTerrain2D
{
    [CustomEditor(typeof(MeshStructure))]
    public class MeshInfoInspector : Editor
    {
        MeshStructure m;
        bool need_save = false;
        void OnEnable()
        {
            m = (MeshStructure)target;
        }

        void OnDisable()
        {
            if (m != null && need_save)
            {
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(m);
                AssetDatabase.SaveAssets();
                need_save = false;
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorExtensions.ListDrawer(serializedObject.FindProperty("meshes"), "meshes", 
                EditorExtensions.ListOptions.by_default ^ 
                EditorExtensions.ListOptions.horizontal_layout);
            serializedObject.ApplyModifiedProperties();

            if (!unique_mesh_names())
                EditorGUILayout.HelpBox("Mesh names should be unique", MessageType.Error);

            if (GUI.changed)
            {
                need_save = true;
            }
        }

        public bool unique_mesh_names()
        {
            for (int i = 0; i < m.meshes.Count - 1; i++)
                for (int j = i + 1; j < m.meshes.Count; j++)
                    if (m.meshes[i].name == m.meshes[j].name)
                        return false;

            return true;
        }
    }
}