﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [CustomEditor(typeof(ItemGenerator))]

    public class ItemGeneratorInspector : Editor
    {
        ItemGenerator t;
        GameObject add_item;
        bool need_save = false;

        void OnEnable()
        {
            t = (ItemGenerator)target;
        }

        void OnDisable()
        {
            if (t != null && need_save)
            {
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(t);
                AssetDatabase.SaveAssets();
                need_save = false;
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("itemset"));
            serializedObject.ApplyModifiedProperties();
            if (GUI.changed)
                need_save = true;
        }
    }
}