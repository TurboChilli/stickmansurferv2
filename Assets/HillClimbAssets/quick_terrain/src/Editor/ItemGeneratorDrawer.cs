﻿using UnityEngine;
using System.Collections;
using UnityEditor;



namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(ItemGenerator))]
    public class ItemGeneratorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ItemGenerator item = property.objectReferenceValue as ItemGenerator;

            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            Rect content_position = EditorGUI.PrefixLabel(position, label);
            content_position.width *= 0.5f;
            EditorGUI.indentLevel = 0;

            if(item != null)
                EditorGUI.LabelField(content_position, new GUIContent(item.name));
            content_position.x += content_position.width;
            if(EditorExtensions.medium_button("edit", "edit asset"))
                Selection.activeObject = item;

            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }
    }
}
