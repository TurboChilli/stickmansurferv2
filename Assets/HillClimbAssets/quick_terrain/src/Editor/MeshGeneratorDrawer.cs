﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(MeshGenerator))]
    public class MeshGeneratorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);

            SerializedProperty type = property.FindPropertyRelative("type");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(property.FindPropertyRelative("name"), GUIContent.none);
            EditorGUILayout.PropertyField(type, GUIContent.none);
            EditorGUILayout.EndHorizontal();


            EditorGUILayout.PropertyField(property.FindPropertyRelative("mapping"), new GUIContent("texture mapping"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("material"));

            SerializedProperty tag = property.FindPropertyRelative("tag");
            tag.stringValue = EditorGUILayout.TagField("tag", tag.stringValue);

            SerializedProperty layer = property.FindPropertyRelative("layer");
            layer.intValue = EditorGUILayout.LayerField("layer", layer.intValue);

            SerializedProperty collider = property.FindPropertyRelative("collider_type");
            EditorGUILayout.PropertyField(collider, new GUIContent("collider"));
            if(collider.enumValueIndex != 0)
                EditorGUILayout.PropertyField(property.FindPropertyRelative("collider_material"), new GUIContent("collider material"));

            EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"), new GUIContent("offset"));
            
            SerializedProperty tile_size = property.FindPropertyRelative("tile_size");
            EditorGUILayout.PropertyField(tile_size, new GUIContent("tile size"));
            if (tile_size.floatValue < .1f)
                tile_size.floatValue = .1f;
            EditorGUILayout.PropertyField(property.FindPropertyRelative("thickness"), new GUIContent("thickness"));


            if(type.enumValueIndex == 1)
                EditorGUILayout.PropertyField(property.FindPropertyRelative("gap"), new GUIContent("gap"));
                
            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }
    }
}
