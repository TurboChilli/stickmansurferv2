﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


namespace QuickTerrain2D
{
    [CustomEditor(typeof(NoiseList))]
    public class NoiseEditor : Editor
    {
        NoiseList t;
        int master_seed;

        void OnEnable()
        {
            t = (NoiseList)target;
        }


        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("edit"))
                NoiseEditorWindow.ShowWindow(t, serializedObject);
        }
    }
}
