﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;


namespace QuickTerrain2D
{
    public class MakeNoise
    {
        [MenuItem("Tools/Quick Terrain 2D/Create Terrain")]
        static void CreateObject()
        {
            GameObject obj = new GameObject("QuickTerrain");
            Undo.RegisterCreatedObjectUndo(obj, "Created obj");
            QuickTerrain qt = obj.AddComponent<QuickTerrain>();
            qt.init();
        }

        [MenuItem("Tools/Quick Terrain 2D/Create Custom Block")]
        static void CreateCustomBLock()
        {
            GameObject obj = new GameObject("custom block");
            Undo.RegisterCreatedObjectUndo(obj, "Created obj");
            obj.AddComponent<CustomBlock>();
        }

        [MenuItem("Assets/Create/QuickTerrain2D/Noise")]
        public static void CreateNoise()
        {
            create_asset(ScriptableObject.CreateInstance<NoiseList>(), "noise");
        }

        [MenuItem("Assets/Create/QuickTerrain2D/Item generation rule")]
        public static void CreateItem()
        {
            create_asset(ScriptableObject.CreateInstance<ItemGenerator>(), "item generation rule");
        }

        [MenuItem("Assets/Create/QuickTerrain2D/Mesh Setup")]
        public static void CreateMeshInfo()
        {
            create_asset(ScriptableObject.CreateInstance<MeshStructure>(), "mesh_structure");
        }

        public static void create_asset(ScriptableObject asset, string name)
        {
            string path = "Assets";
            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                path = AssetDatabase.GetAssetPath(obj);
                if (File.Exists(path))
                {
                    path = Path.GetDirectoryName(path);
                }
                break;
            }

            path = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");

            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
        }
    }
}
