﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(NoiseSample))]
    public class NoiseSampleDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("seed", GUILayout.Width(50f));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("seed"), GUIContent.none);
            EditorGUILayout.EndHorizontal();
            SerializedProperty amplitude = property.FindPropertyRelative("_amplitude");
            SerializedProperty length = property.FindPropertyRelative("_length");
            SerializedProperty smothness = property.FindPropertyRelative("_smoothnes");
            EditorGUILayout.PropertyField(amplitude, new GUIContent("amplitude"));
            EditorGUILayout.PropertyField(length, new GUIContent("length"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"), new GUIContent("offset"));
            EditorGUILayout.PropertyField(smothness, new GUIContent("smoothnes"));
            if (amplitude.floatValue < 0f) amplitude.floatValue = 0f;
            if (length.floatValue < .01f) length.floatValue = .01f;
            if (smothness.floatValue < 0f) smothness.floatValue = 0f;
            else if (smothness.floatValue > 1f) smothness.floatValue = 1f;

            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }
    }
}
