﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(Seed))]
    public class SeedPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty value = property.FindPropertyRelative("value");
            SerializedProperty type = property.FindPropertyRelative("type");

            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            Rect content_position = EditorGUI.PrefixLabel(position, label);
            content_position.width *= 0.5f;
            EditorGUI.indentLevel = 0;

            if((Seed.Type)type.enumValueIndex == Seed.Type.constant)
                EditorGUI.PropertyField(content_position, value, GUIContent.none);
            content_position.x += content_position.width;
            EditorGUI.PropertyField(content_position, type, GUIContent.none);

            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }
    }
}
