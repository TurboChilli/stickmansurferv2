﻿using UnityEngine;
using System.Collections;
using UnityEditor;



namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(ItemSet))]
    public class ItemSetDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUI.indentLevel = 0;

            EditorExtensions.ListDrawer(property.FindPropertyRelative("items"), "items", 
                EditorExtensions.ListOptions.no_fold | EditorExtensions.ListOptions.by_default ^ EditorExtensions.ListOptions.sorting);

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(property.FindPropertyRelative("seed"), new GUIContent("seed"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("probability"), new GUIContent("probability"));
            SerializedProperty distance = property.FindPropertyRelative("distance");
            EditorGUILayout.PropertyField(distance, new GUIContent("distance"));
            if (distance.floatValue < .01f)
                distance.floatValue = .01f;
            EditorGUILayout.PropertyField(property.FindPropertyRelative("scatter"), new GUIContent("scatter"));

            SerializedProperty masking = property.FindPropertyRelative("masking");
            EditorGUILayout.PropertyField(masking, new GUIContent("masking"));
            if(masking.boolValue)
            {
                EditorGUI.indentLevel += 1;
                draw_range(property, "white_bounds", "white zone");
                draw_range(property, "black_bounds", "gap zone");
                EditorGUI.indentLevel -= 1;
            }

            EditorGUILayout.BeginHorizontal();
            SerializedProperty minx = property.FindPropertyRelative("min_x_constraint");
            EditorGUILayout.PropertyField(minx, new GUIContent("min x"));
            if (minx.boolValue)
                EditorGUILayout.PropertyField(property.FindPropertyRelative("min_x"), GUIContent.none);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            SerializedProperty maxx = property.FindPropertyRelative("max_x_constraint");
            EditorGUILayout.PropertyField(maxx, new GUIContent("max x"));
            if (maxx.boolValue)
                EditorGUILayout.PropertyField(property.FindPropertyRelative("max_x"), GUIContent.none);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(property.FindPropertyRelative("align_to_normal"), new GUIContent("angle of surface"));
            EditorGUILayout.PropertyField(property.FindPropertyRelative("block_as_parent"), new GUIContent("block as parent"));

            SerializedProperty offset = property.FindPropertyRelative("do_offset");
            EditorGUILayout.PropertyField(offset, new GUIContent("offset"));
            if(offset.boolValue)
            {
                EditorGUI.indentLevel += 1;
                draw_range(property, "offset_x", "x");
                draw_range(property, "offset_y", "y");
                draw_range(property, "offset_z", "z");
                EditorGUI.indentLevel -= 1;
            }

            SerializedProperty scale = property.FindPropertyRelative("do_scale");
            EditorGUILayout.PropertyField(scale, new GUIContent("scale"));
            if (scale.boolValue)
            {
                EditorGUI.indentLevel += 1;
                draw_range(property, "scale");
                EditorGUI.indentLevel -= 1;
            }

            SerializedProperty rotation = property.FindPropertyRelative("do_rotation");
            EditorGUILayout.PropertyField(rotation, new GUIContent("rotation"));
            if (rotation.boolValue)
            {
                EditorGUI.indentLevel += 1;
                draw_range(property, "rotation");
                EditorGUI.indentLevel -= 1;
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("filter tags");

            EditorGUI.indentLevel += 1;
            SerializedProperty list = property.FindPropertyRelative("tags");
            for (int i = 0; i < list.arraySize; i++)
            {
                EditorGUILayout.BeginHorizontal();
                SerializedProperty filter_tag = list.GetArrayElementAtIndex(i);
                filter_tag.stringValue = EditorGUILayout.TagField(filter_tag.stringValue);
                if (EditorExtensions.small_button("x", "remove"))
                {
                    int oldSize = list.arraySize;
                    list.DeleteArrayElementAtIndex(i);
                    if (list.arraySize == oldSize)
                        list.DeleteArrayElementAtIndex(i);
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (EditorExtensions.medium_button("add", "add element"))
            {
                list.arraySize += 1;
                list.GetArrayElementAtIndex(list.arraySize - 1).stringValue = "Untagged";
            }
            EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel -= 1;


         /*   EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("filter tags:");
            for (int i = 0; i < s.tags.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(s.tags[i]);
                if (GUILayout.Button("x"))
                {
                    s.tags.RemoveAt(i);
                    i--;
                }
                EditorGUILayout.EndHorizontal();

            }
            EditorGUILayout.BeginHorizontal();
            string tag = "Untagged";
            EditorGUILayout.LabelField("add tag");
            tag = EditorGUILayout.TagField(tag);
            if (tag != "Untagged")
                s.tags.Add(tag);
            EditorGUILayout.EndHorizontal();
  



            if (distance.floatValue < .01f)
                distance.floatValue = .01f;*/

            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }

        public void draw_range(SerializedProperty property, string property_name, string label = "")
        {
            SerializedProperty x, y;
            EditorGUILayout.BeginHorizontal();
            if(label != "")
                EditorGUILayout.LabelField(new GUIContent(label));
            x = property.FindPropertyRelative(property_name).FindPropertyRelative("x");
            y = property.FindPropertyRelative(property_name).FindPropertyRelative("y");
            EditorGUILayout.PropertyField(x, GUIContent.none);
            EditorGUILayout.PropertyField(y, GUIContent.none);
            if (x.floatValue < 0) x.floatValue = 0f;
            if (y.floatValue < 0) y.floatValue = 0f;
            if (x.floatValue > y.floatValue) y.floatValue = x.floatValue;
            EditorGUILayout.EndHorizontal();
        }
    }
}

