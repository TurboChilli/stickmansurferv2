﻿using UnityEngine;
using UnityEditor;



public static class EditorExtensions{

    [System.Flags]
    public enum ListOptions
    {
        none = 0,
        element_label = 1,
        removing = 2,
        no_fold = 4,
        sorting = 8,
        adding = 16,
        horizontal_layout = 32,
        by_default = removing | sorting | adding | horizontal_layout
    }

    public static void ListDrawer(SerializedProperty list, string label, EditorExtensions.ListOptions options = ListOptions.by_default)
    {
        EditorGUILayout.PropertyField(list, new GUIContent(label));
        EditorGUI.indentLevel += 1;
        if ((options & ListOptions.no_fold) != 0 || list.isExpanded)
        {
            for (int i = 0; i < list.arraySize; i++)
            {
                if(flag(ListOptions.horizontal_layout, options))
                    EditorGUILayout.BeginHorizontal();

                SerializedProperty element = list.GetArrayElementAtIndex(i);
                if (flag(ListOptions.element_label, options))
                    EditorGUILayout.PropertyField(element);
                else
                    EditorGUILayout.PropertyField(element, GUIContent.none);

                if (!flag(ListOptions.horizontal_layout, options))
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                }

                if (flag(ListOptions.sorting, options))
                {
                    if (GUILayout.Button(new GUIContent("\u21d3", "move down"), EditorStyles.miniButtonLeft, GUILayout.Width(20f)))
                        list.MoveArrayElement(i, i + 1);
                    if (GUILayout.Button(new GUIContent("\u21d1", "move up"), EditorStyles.miniButtonRight, GUILayout.Width(20f)))
                        list.MoveArrayElement(i, i - 1);
                }

                if (flag(ListOptions.removing, options))
                {
                    
                    if (EditorExtensions.small_button("x", "remove"))
                    {
                        int oldSize = list.arraySize;
                        list.DeleteArrayElementAtIndex(i);
                        if (list.arraySize == oldSize)
                            list.DeleteArrayElementAtIndex(i);
                    }
                }

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (EditorExtensions.medium_button("add", "add element"))
                list.arraySize += 1;
            EditorGUILayout.EndHorizontal();
        }
        EditorGUI.indentLevel -= 1;
    }

    public static void ListDrawer<T>(SerializedProperty list, string label, EditorExtensions.ListOptions options = ListOptions.by_default)
    {
        EditorGUILayout.PropertyField(list, new GUIContent(label));
        EditorGUI.indentLevel += 1;
        if ((options & ListOptions.no_fold) != 0 || list.isExpanded)
        {
            for (int i = 0; i < list.arraySize; i++)
            {
                if (flag(ListOptions.horizontal_layout, options))
                    EditorGUILayout.BeginHorizontal();

                SerializedProperty element = list.GetArrayElementAtIndex(i);

                if (element.objectReferenceValue == null)
                {
                    element.objectReferenceValue = EditorGUILayout.ObjectField(element.objectReferenceValue, typeof(T), true);
                }
                else
                {
                    if (flag(ListOptions.element_label, options))
                        EditorGUILayout.PropertyField(element);
                    else
                        EditorGUILayout.PropertyField(element, GUIContent.none);
                }

                if (!flag(ListOptions.horizontal_layout, options))
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                }

                if (flag(ListOptions.sorting, options))
                {
                    if (GUILayout.Button(new GUIContent("\u21d3", "move down"), EditorStyles.miniButtonLeft, GUILayout.Width(20f)))
                        list.MoveArrayElement(i, i + 1);
                    if (GUILayout.Button(new GUIContent("\u21d1", "move up"), EditorStyles.miniButtonRight, GUILayout.Width(20f)))
                        list.MoveArrayElement(i, i - 1);
                }

                if (flag(ListOptions.removing, options))
                {

                    if (EditorExtensions.small_button("x", "remove"))
                    {
                        int oldSize = list.arraySize;
                        list.DeleteArrayElementAtIndex(i);
                        if (list.arraySize == oldSize)
                            list.DeleteArrayElementAtIndex(i);
                    }
                }

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (EditorExtensions.medium_button("add", "add element"))
            {
                list.arraySize += 1;
                list.GetArrayElementAtIndex(list.arraySize - 1).objectReferenceValue = null;

            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUI.indentLevel -= 1;
    }


    public static bool small_button(string label, string tip)
    {
        return GUILayout.Button(new GUIContent(label, tip), EditorStyles.miniButton, GUILayout.Width(20f));
    }
    public static bool medium_button(string label, string tip)
    {
        return GUILayout.Button(new GUIContent(label, tip), EditorStyles.miniButton, GUILayout.Width(50f));
    }

    public static bool button(string label, string tip)
    {
        return GUILayout.Button(new GUIContent(label, tip), EditorStyles.miniButton);
    }

    public static bool flag(ListOptions option, ListOptions options)
    {
        return (option & options) != 0;
    }
}
