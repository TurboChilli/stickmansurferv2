﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace QuickTerrain2D
{
    [CustomPropertyDrawer(typeof(ItemInfo))]
    public class ItemDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty item = property.FindPropertyRelative("prefab");
            SerializedProperty weight = property.FindPropertyRelative("weight");

            int oldIndentLevel = EditorGUI.indentLevel;
            label = EditorGUI.BeginProperty(position, label, property);
            Rect content_position = EditorGUI.PrefixLabel(position, label);
            content_position.width *= 0.5f;
            EditorGUI.indentLevel = 0;

            EditorGUI.PropertyField(content_position, item, GUIContent.none);
            content_position.x += content_position.width;
            EditorGUI.PropertyField(content_position, weight, GUIContent.none);

            EditorGUI.indentLevel = oldIndentLevel;
            EditorGUI.EndProperty();
        }
    }
}


