﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    public class NoiseEditorWindow : EditorWindow
    {
        public static NoiseEditorWindow window;
        NoiseList list = null;
        SerializedObject serializedObject;
        Vector2 slide = new Vector2(0f, 100f);
        float scale = 5f;
        int master_seed = 0;
        static Texture2D tex;

        public static void ShowWindow(NoiseList noise_list, SerializedObject s)
        {
            if (window != null)
                window.Close();
            window = EditorWindow.GetWindow<NoiseEditorWindow>(noise_list.name);
            window.serializedObject = s;
            window.list = noise_list;
            window.wantsMouseMove = true;

            if (tex == null)
            {
                tex = new Texture2D(1, 1, TextureFormat.RGBA32, false);
                tex.SetPixel(0, 0, new Color32(70, 80, 90, 255));
                tex.Apply();
            }
        }

        public void OnDestroy()
        {
            window = null;
            if (list != null)
            {
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(list);
                AssetDatabase.SaveAssets();
            }
        }

        void OnGUI()
        {
            GUI.backgroundColor = new Color(.8f, .9f, .8f);
            GUI.DrawTexture(new Rect(0, 0, maxSize.x, maxSize.y), tex, ScaleMode.StretchToFill);

            float cell_size = 10f;
            float w = position.width / scale;
            int segments = 500;
            float cx = slide.x % (cell_size * scale);
            float cy = slide.y % (cell_size * scale);
            float center = position.height / 2f + cy;

            Handles.color = Color.gray;
            Handles.DrawLine(new Vector3(0f, center, 0f), new Vector3(position.width, center, 0f));

            int i;
            for (i = 0; i * scale < center; i++)
            {
                float y = i * scale * cell_size;
                Handles.DrawLine(new Vector3(0f, center + y, 0f), new Vector3(position.width, center + y, 0f));
                Handles.DrawLine(new Vector3(0f, center - y, 0f), new Vector3(position.width, center - y, 0f));
            }
            for (i = 0; i * scale < position.width; i++)
            {
                float x = i * cell_size * scale + cx;
                Handles.DrawLine(new Vector3(x, 0, 0f), new Vector3(x, position.height, 0f));
            }

            if (list != null)
            {
                float ta = 0f;
                foreach (NoiseSample ns in list.samples)
                    ta += ns.amplitude;
                float segment_size = w / (float)segments;
                int amount = segments + 1;
                Vector3[] vs = new Vector3[amount];
                center = position.height / 2f + slide.y;
                float dy = ta * scale + center;
                for (i = 0; i < amount; i++)
                {
                    float x = i * segment_size;
                    vs[i] = new Vector3(x * scale, -Noise.value_at(x - slide.x / scale, list.samples, list.slope) * scale + dy);
                }

                Handles.color = Color.white;
                Handles.DrawPolyLine(vs);
            }

            draw_windows();

            Event e = Event.current;
            if (e != null)
                if (e.button == 1)
                {
                    slide += e.delta;
                    Repaint();
                }
        }

        void draw_windows()
        {
            BeginWindows();
            int i = 0;
            for (i = 0; i < list.samples.Count; i++ )
            {
                Rect windowRect = new Rect(20 + 300 * i, 20, 100, 100);
                windowRect.position += slide;
                GUILayout.Window(i, windowRect, draw_sample, "window " + i.ToString());
            }

            Rect options = new Rect(10f, 10f, position.width - 20f, 20f);
            GUILayout.Window(i, options, draw_options, "options"); ;
            EndWindows();
        }

        void draw_options(int unusedWindowID)
        {
            int check = master_seed;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("slope"), new GUIContent("slope"));
            master_seed = EditorGUILayout.IntField("master seed", master_seed);
            if (GUILayout.Button("add sample"))
            {
                SerializedProperty samples = serializedObject.FindProperty("samples");
                samples.arraySize += 1;
            }
            scale = EditorGUILayout.Slider("preview scale", scale, 2f, 10f);
            if (GUILayout.Button("reset view"))
            {
                slide.x = 0f;
                slide.y = 100f;
            }
            EditorGUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties();

            if (check != master_seed)
                update_seed();
        }

        void draw_sample(int index)
        {

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("index " + index);
            bool removed = EditorExtensions.small_button("x", "remove");
            EditorGUILayout.EndHorizontal();
            if (removed)
            {
                serializedObject.FindProperty("samples").DeleteArrayElementAtIndex(index);
                serializedObject.ApplyModifiedProperties();
            }
            else
            {
                SerializedProperty sample = serializedObject.FindProperty("samples").GetArrayElementAtIndex(index);
                EditorGUILayout.PropertyField(sample, GUIContent.none);
                GUI.DragWindow();
            }
        }

        void update_seed()
        {
            foreach (NoiseSample n in list.samples)
                n.seed.initialize(master_seed);
        }
    }
}