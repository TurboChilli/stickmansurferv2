﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


namespace QuickTerrain2D
{
    [CustomEditor(typeof(QuickTerrain))]
    public class GoundGenInspector : Editor
    {
        QuickTerrain qt;

        string[] action_label_list;
        void OnEnable()
        {
            qt = (QuickTerrain)target;
            action_label_list = EventAction.all_actions();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            if (Application.isPlaying)
                return;


            GUILayout.BeginHorizontal();
            if (GUILayout.Button("update preview"))
                update_preview();
            if (GUILayout.Button("clear preview"))
                clear();
            GUILayout.EndHorizontal();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("agent"), new GUIContent("tracking transform"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("master_seed"), new GUIContent("master seed"));

            EditorGUILayout.PropertyField(serializedObject.FindProperty("blocks_total"), new GUIContent("active blocks"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("segments"), new GUIContent("segments"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("block_size"), new GUIContent("block width"));


            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("noise_list"), new GUIContent("noise"));
            if (qt.noise_list != null && EditorExtensions.medium_button("edit", "edit asset"))
                NoiseEditorWindow.ShowWindow(qt.noise_list, new SerializedObject(qt.noise_list));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("mesh_structure"), new GUIContent("meshes"));
            if (qt.mesh_structure != null && EditorExtensions.medium_button("edit", "edit asset"))
                Selection.activeObject = qt.mesh_structure;
            EditorGUILayout.EndHorizontal();


            EditorExtensions.ListDrawer<ItemGenerator>(serializedObject.FindProperty("item_generators"), "items");




            EditorGUILayout.Space();

            qt.block_fold = EditorGUILayout.Foldout(qt.block_fold, new GUIContent("events"));
            if (qt.block_fold)
                draw_event();

            EditorGUILayout.Space();

            EditorExtensions.ListDrawer(serializedObject.FindProperty("generation_scripts"), "generation sctipts");

            serializedObject.ApplyModifiedProperties();
            if (GUI.changed)
            {
                qt.OnValidate();
                EditorUtility.SetDirty(qt);
            }
        }


        void draw_event()
        {
            SerializedProperty event_list = serializedObject.FindProperty("actions");
            
            for(int i = 0; i<event_list.arraySize; i++)
            {
                SerializedProperty index_event = event_list.GetArrayElementAtIndex(i);
                IndexEvent event_object = qt.actions[i];
                if(event_object != null)
                {
                    EditorGUILayout.BeginHorizontal();
                    event_object.fold = EditorGUILayout.Foldout(event_object.fold, new GUIContent(event_object.get_label()));
                    EditorGUILayout.PropertyField(index_event.FindPropertyRelative("name"), GUIContent.none);
                    if (GUILayout.Button(new GUIContent("\u21d3", "move down"), EditorStyles.miniButtonLeft, GUILayout.Width(20f)))
                        event_list.MoveArrayElement(i, i + 1);
                    if (GUILayout.Button(new GUIContent("\u21d1", "move up"), EditorStyles.miniButtonRight, GUILayout.Width(20f)))
                        event_list.MoveArrayElement(i, i - 1);
                    if (EditorExtensions.small_button("x", "remove event"))
                    {
                        event_list.DeleteArrayElementAtIndex(i);
                        EditorGUILayout.EndHorizontal();
                        continue;
                    }
                    EditorGUILayout.EndHorizontal();

                    if (event_object.fold)
                    {
                        draw_event_patern(index_event);

                        EditorGUILayout.Space();
                        EditorGUILayout.PropertyField(index_event.FindPropertyRelative("seed"));
                        EditorGUILayout.PropertyField(index_event.FindPropertyRelative("probability"));
                        EditorGUILayout.Space();

                        draw_event_actions(index_event.FindPropertyRelative("actions"), event_object.actions);
                    }
                }
                else if(EditorExtensions.small_button("x", "remove event"))
                    event_list.DeleteArrayElementAtIndex(i);
            }
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (EditorExtensions.button("add event", "add event"))
            {
                event_list.arraySize++;
                serializedObject.ApplyModifiedProperties();
                int index = qt.actions.Count - 1;
                qt.actions[index] = new IndexEvent();
            }
            EditorGUILayout.EndHorizontal();
        }


        void clear()
        {
            qt.re_init();
            List<GameObject> children = new List<GameObject>();
            foreach (Transform child in qt.transform) 
                children.Add(child.gameObject);
            children.ForEach(child =>  Undo.DestroyObjectImmediate(child));
            EditorUtility.SetDirty(qt);
        }

        void update_preview()
        {
            clear();
            qt.StartGeneration();
            List<GameObject> children = new List<GameObject>();
            foreach (Transform child in qt.transform)
                children.Add(child.gameObject);
            children.ForEach(child => Undo.RegisterCreatedObjectUndo(child, "update_preview"));
            EditorUtility.SetDirty(qt);
        }

        void draw_event_actions(SerializedProperty list, List<EventAction> action_list)
        {
            for (int i = 0; i < action_list.Count; i++)
            {
                EventAction action_object = action_list[i];
 //               SerializedProperty action = list.GetArrayElementAtIndex(i);

                if (action_object != null)
                {
                    bool deleted = false;
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(action_object.name);

                    if (EditorExtensions.small_button("x", "remove action"))
                    {
                        deleted = true;
                        action_list.RemoveAt(i);
                    }
                    EditorGUILayout.EndHorizontal();

                    if (!deleted)
                    {
                        if (action_object is InsertBlockAction)
                        {
                            InsertBlockAction action = action_object as InsertBlockAction;
                            action.block = (CustomBlock)EditorGUILayout.ObjectField(" block", action.block, typeof(CustomBlock), true);
                            if(action.block != null && action.block.use_states)
                            {
                                action.random_state = EditorGUILayout.Toggle(" random state", action.random_state);
                                if (!action.random_state)
                                    action.state = EditorGUILayout.Slider(" state", action.state, 0f, 1f);
                            }
                        }
                        if (action_object is InsertRandomBlockAction)
                        {
                            draw_block_set(((InsertRandomBlockAction)action_object).blocks);
                        }
                        else if (action_object is AddItemAction)
                        {
                            ((AddItemAction)action_object).item = (ItemGenerator)EditorGUILayout.ObjectField("item", ((AddItemAction)action_object).item, typeof(ItemGenerator), false);
                        }
                        else if (action_object is RemoveItemAction)
                        {
                            ((RemoveItemAction)action_object).item = (ItemGenerator)EditorGUILayout.ObjectField("item", ((RemoveItemAction)action_object).item, typeof(ItemGenerator), false);
                        }
                        else if (action_object is ChangeNoiseAction)
                        {
                            ((ChangeNoiseAction)action_object).noise = (NoiseList)EditorGUILayout.ObjectField("noise", ((ChangeNoiseAction)action_object).noise, typeof(NoiseList), false);
                            ((ChangeNoiseAction)action_object).iterations = EditorGUILayout.IntField("iterations", ((ChangeNoiseAction)action_object).iterations);
                            if (((ChangeNoiseAction)action_object).iterations < 1)
                                ((ChangeNoiseAction)action_object).iterations = 1;
                        }
                        else if (action_object is ChangeMeshStructureAction)
                        {
                            ((ChangeMeshStructureAction)action_object).mesh_setup = (MeshStructure)EditorGUILayout.ObjectField("mesh structure", ((ChangeMeshStructureAction)action_object).mesh_setup, typeof(MeshStructure), false);
                        }
                        else if (action_object is ModifyNoiseSampleAction)
                        {
                            ModifyNoiseSampleAction ma = (ModifyNoiseSampleAction)action_object;
                            for (int z = 0; z < ma.data.Count; z++)
                            {
                                SampleModifyData d = ma.data[z];
                                EditorGUILayout.BeginHorizontal();
                                d.index = EditorGUILayout.IntField("index", d.index);
                                if (EditorExtensions.small_button("+", "add sample"))
                                {
                                    ma.add_sample_data();
                                }
                                if (ma.data.Count > 1)
                                {
                                    if (EditorExtensions.small_button("-", "remove sample"))
                                    {
                                        ma.data.RemoveAt(z);
                                        z--;
                                    }
                                }
                                EditorGUILayout.EndHorizontal();
                                d.amplitude = EditorGUILayout.FloatField("  amplitude", d.amplitude);
                                d.length = EditorGUILayout.FloatField("  length", d.length);
                                d.smoothnes = Mathf.Clamp(EditorGUILayout.FloatField("  smoothnes", d.smoothnes), 0f, 1f);
                            }
                            EditorGUILayout.Space();
                            ma.slope = EditorGUILayout.FloatField("slope", ma.slope);
                            ma.iterations = EditorGUILayout.IntField("itrations", ma.iterations);
                        }
                        else if (action_object is ReplaceMeshMaterialAction)
                        {
                            ((ReplaceMeshMaterialAction)action_object).material = (Material)EditorGUILayout.ObjectField("material", ((ReplaceMeshMaterialAction)action_object).material, typeof(Material), false);
                            ((ReplaceMeshMaterialAction)action_object).mesh_name = EditorGUILayout.TextField("mesh name", ((ReplaceMeshMaterialAction)action_object).mesh_name);
                        }
                        else if (action_object is ReplaceItemsAction)
                        {
                            draw_item_list(((ReplaceItemsAction)action_object).items, false);
                        }
                    }
                }
                else if (EditorExtensions.small_button("x", "action is missing"))
                    list.DeleteArrayElementAtIndex(i);



                EditorGUILayout.Space();
                EditorGUILayout.Space();

            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            int action_index = 0;
            action_index = EditorGUILayout.Popup(0, action_label_list);
            if (action_index != 0)
                action_list.Add(EventAction.CreateAction(action_label_list[action_index]));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        void draw_event_patern(SerializedProperty index_event)
        {
            SerializedProperty start = index_event.FindPropertyRelative("start");
            SerializedProperty step = index_event.FindPropertyRelative("step");
            SerializedProperty end = index_event.FindPropertyRelative("end");
            SerializedProperty type = index_event.FindPropertyRelative("type");
            EditorGUILayout.PropertyField(type, new GUIContent("pattern"));

            switch ((IndexEvent.EType)type.enumValueIndex)
            {
                case IndexEvent.EType.single:
                    EditorGUILayout.PropertyField(start, new GUIContent("index"));
                    if (start.intValue == 0) start.intValue = 1;
                    break;
                case IndexEvent.EType.repeat:
                    EditorGUILayout.PropertyField(step, new GUIContent("every"));
                    if (step.intValue < 0)
                        step.intValue = -step.intValue;
                    else if (step.intValue == 0)
                        step.intValue = 1;
                    break;
                case IndexEvent.EType.range:
                    EditorGUILayout.PropertyField(start, new GUIContent("start from"));
                    EditorGUILayout.PropertyField(end, new GUIContent("end at"));
                    EditorGUILayout.PropertyField(step, new GUIContent("every"));
                    if (step.intValue == 0)
                        step.intValue = 1;
                    if (step.intValue > 0 && end.intValue < start.intValue + step.intValue ||
                        step.intValue < 0 && end.intValue > start.intValue + step.intValue)
                        end.intValue = start.intValue + step.intValue;
                    break;
                case IndexEvent.EType.ray:
                    EditorGUILayout.PropertyField(start, new GUIContent("start from"));
                    EditorGUILayout.PropertyField(step, new GUIContent("evety"));
                    if (step.intValue == 0) step.intValue = 1;
                    break;
            }
        }


        public void draw_item_list(List<ItemGenerator> items, bool edit = true)
        {
            for (int i = 0; i < items.Count; i++)
            {
                ItemGenerator item = items[i];
                if (item == null)
                {
                    if (GUILayout.Button("item is missing"))
                    {
                        items.RemoveAt(i);
                        i--;
                    }
                    continue;
                }
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(item.name);
                if (edit)
                    if (GUILayout.Button("edit"))
                        Selection.activeObject = item;
                if (GUILayout.Button("x"))
                {
                    items.RemoveAt(i);
                    i--;
                }
                EditorGUILayout.EndHorizontal();
            }
            ItemGenerator new_item = null;
            new_item = (ItemGenerator)EditorGUILayout.ObjectField(new GUIContent("add item"), new_item, typeof(ItemGenerator), false);
            if (new_item != null)
                items.Add(new_item);
        }

        public void draw_block_set(CustomBlockSet set)
        {
            if (set.blocks != null)
            {
                for (int i = 0; i < set.blocks.Length; i++)
                {
                    CustomBlockInfo block = set.blocks[i];

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(block.block.name);
                    block.weight = EditorGUILayout.IntField(block.weight);
                    if (block.weight < 1) block.weight = 1;
                    if (GUILayout.Button("x"))
                    {
                        set.remove_item(i);
                        i--;
                    }
                    EditorGUILayout.EndHorizontal();

                    if (block.block != null && block.block.use_states)
                    {
                        block.random_state = EditorGUILayout.Toggle(" random state", block.random_state);
                        if (!block.random_state)
                            block.state = EditorGUILayout.Slider(" state", block.state, 0f, 1f);
                    }
                }
            }
            CustomBlock new_item = null;
            new_item = (CustomBlock)EditorGUILayout.ObjectField(new GUIContent("add prefab"), new_item, typeof(CustomBlock), true);
            if (new_item != null)
                set.add_item(new_item);

        }
    }
}

