﻿using UnityEngine;
using System.Collections;

public class TangentControl : MonoBehaviour {
    [HideInInspector]
    public bool left = false;
    public Transform point;
    [HideInInspector]
    public bool visible = true;
    void OnDrawGizmos()
    {
        if (visible)
        {
            Gizmos.color = left ? Color.yellow : Color.green;
            Gizmos.DrawSphere(transform.position, .2f);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (visible)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, point.position);
        }
    }
}
