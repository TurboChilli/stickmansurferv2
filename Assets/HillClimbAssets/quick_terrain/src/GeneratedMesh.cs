﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace QuickTerrain2D
{
    public class GeneratedMesh : MonoBehaviour
    {
        public Collider2D _collider;
        public PhysicsMaterial2D collider_material;
        public float left_seam;
        public float right_seam;

        public enum ColliderType { none, edge_top, edge_center, edge_bottom, edge_perimeter, polygon}
        public ColliderType collider_type;

        public void build_collider()
        {
            if (collider_type == ColliderType.none)
                return;
            if (_collider != null)
                DestroyImmediate(_collider);
            switch (collider_type)
            {
                case ColliderType.edge_top:
                    build_edge_top();
                    break;
                case ColliderType.edge_center:
                    build_edge_center();
                    break;
                case ColliderType.edge_bottom:
                    build_edge_bottom();
                    break;
                case ColliderType.edge_perimeter:
                    build_edge();
                    break;
                case ColliderType.polygon:
                    build_polygon();
                    break;
            }


        }

        void build_edge_bottom()
        {
            Vector2[] cv;
            Vector3[] vs = get_mesh_verices();
            int c = vs.Length / 2;
            cv = new Vector2[c];
            for (int i = 0; i < c; i++)
                cv[i] = vs[i + c];
            _collider = edge(cv);
        }

        EdgeCollider2D edge(Vector2[] cv)
        {
            EdgeCollider2D c = gameObject.AddComponent<EdgeCollider2D>();
            c.points = cv;
            if (collider_material != null)
                c.sharedMaterial = collider_material;
            return c;
        }

        void build_edge_top()
        {
            Vector2[] cv;
            Vector3[] vs = get_mesh_verices();
            int c = vs.Length / 2;
            cv = new Vector2[c];
            for (int i = 0; i < c; i++)
                cv[i] = vs[i];
            _collider = edge(cv);
        }

        void build_edge_center()
        {
            Vector2[] cv;
            Vector3[] vs = get_mesh_verices();
            int c = vs.Length / 2;
            cv = new Vector2[c];
            for (int i = 0; i < c; i++)
            {
                Vector3 p = (vs[vs.Length - i - 1] - vs[i]) / 2f;
                cv[i] = vs[i] + p;
            }
            _collider = edge(cv);
        }

        void build_edge()
        {
            Vector3[] vs = get_mesh_verices();
            Vector2[] cv = new Vector2[vs.Length + 1];
            for (int i = 0; i < vs.Length; i++)
                cv[i] = vs[i];
            cv[cv.Length - 1] = vs[0];
            _collider = edge(cv);
        }

        void build_polygon()
        {
            Vector2[] cv;
            Vector3[] vs = get_mesh_verices();
            cv = new Vector2[vs.Length];
            for (int i = 0; i < vs.Length; i++)
                cv[i] = vs[i];
            PolygonCollider2D c = gameObject.AddComponent<PolygonCollider2D>();
            c.points = cv;
            if (collider_material != null)
                c.sharedMaterial = collider_material;
            _collider = c;
        }

        Vector3[] get_mesh_verices()
        {
            return GetComponent<MeshFilter>().sharedMesh.vertices;
        }
    }
}