﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class CustomBlockSet
    {
#if UNITY_EDITOR
        public bool fold = true;
#endif

        public CustomBlockInfo[] blocks;
        public Seed seed;
        public float probability = 1;

        int tw;
        int[] weights;


        public CustomBlockSet()
        {
            seed = new Seed();
        }
        public CustomBlockInfo get_item(float x)
        {
            int _seed = seed.value + 1;
            float r = NoiseSample.normalized_value(x, _seed) * (float)tw;
            for (int i = 0; i < weights.Length; i++)
                if (weights[i] >= r)
                    return blocks[i];
            return blocks[blocks.Length - 1];
        }

        public void add_item(CustomBlock b)
        {
            if(blocks == null)
            {
                blocks = new CustomBlockInfo[1];
                blocks[0] = new CustomBlockInfo();
                blocks[0].block = b;
                return;
            }
            int count = blocks == null ? 0 : blocks.Length;
            CustomBlockInfo[] nb = new CustomBlockInfo[count + 1];
            for (int i = 0; i < blocks.Length; i++)
                nb[i] = blocks[i];

            nb[nb.Length - 1] = new CustomBlockInfo();
            nb[nb.Length - 1].block = b;
            blocks = nb;
        }

        public void remove_item(int index)
        {
            int count = blocks == null ? 0 : blocks.Length;
            if (count == 0)
                return;
            CustomBlockInfo[] nb = new CustomBlockInfo[count - 1];
            for (int i = 0, j = 0; i < nb.Length; i++, j++)
            {
                if (j == index) j++;
                nb[i] = blocks[j];
            }
            blocks = nb;
        }

        public void init(int master_seed)
        {

            seed.initialize(master_seed);
            CustomBlockInfo t;
            for (int i = 0; i < blocks.Length - 1; i++)
                for (int j = i + 1; j < blocks.Length; j++)
                {
                    if (blocks[i].weight > blocks[j].weight)
                    {
                        t = blocks[i];
                        blocks[i] = blocks[j];
                        blocks[j] = t;
                    }
                } 
            tw = 0;
            weights = new int[blocks.Length];
            for (int i = 0; i < blocks.Length; i++)
            {
                tw += blocks[i].weight;
                weights[i] = tw;
            }
        }

        public CustomBlockSet clone
        {
            get
            {
                CustomBlockSet c = new CustomBlockSet();
                c.seed = seed;
                c.probability = probability;
                c.blocks = new CustomBlockInfo[blocks.Length];
                for (int i = 0; i < blocks.Length; i++)
                    c.blocks[i] = blocks[i].clone;
                return c;
            }
        }
    }

    [System.Serializable]
    public class CustomBlockInfo
    {
        public CustomBlock block;
        public int weight = 1;
        public float state = 0f;
        public bool random_state = true;

        public CustomBlockInfo clone 
        {
            get
            {
                CustomBlockInfo i = new CustomBlockInfo();
                i.weight = weight;
                i.block = block;
                i.state = state;
                i.random_state = random_state;
                return i;
            }
        }
    }
}