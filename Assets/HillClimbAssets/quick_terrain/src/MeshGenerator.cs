﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace QuickTerrain2D
{
    [System.Serializable]
    public class MeshGenerator
    {
        public enum MeshType { basic, top, road }
        public enum Mapping { projection, path}

        public Mapping mapping = Mapping.projection;

        public string name = "unnamed";
        public MeshType type;
        public Material material;

        public GeneratedMesh.ColliderType collider_type;
        public PhysicsMaterial2D collider_material;
        public string tag = "Untagged";
        public int layer;
        public Vector3 offset;

        public float tile_size = 1f;
        public float thickness = 1f;
        public float gap = 10f;

        public MeshGenerator clone
        {
            get
            {
                MeshGenerator c = new MeshGenerator(name);
                c.mapping = mapping;
                c.type = type;
                c.material = material;
                c.collider_type = collider_type;
                c.collider_material = collider_material;
                c.tag = tag;
                c.layer = layer;
                c.offset = offset;
                c.tile_size = tile_size;
                c.thickness = thickness;
                c.gap = gap;
                return c;
            }
        }

        public MeshGenerator(string _name)
        {
            name = _name;
            tag = "Untagged";
            tile_size = 1f;
        }

        public GeneratedMesh build_mesh(Vector3[] line, Vector3 o, Block fix_block, GeneratedMesh fix_mesh, bool right)
        {
            Vector3[] vs;
            Vector2[] tcs;
            float left_seam, right_seam;


            float seam = fix_mesh == null ? 0f : right ? fix_mesh.right_seam : fix_mesh.left_seam;
            seam -= (int)seam;

            if (type == MeshType.top)
            {
                vs = top_block(line);
                tcs = projected_tcs(vs, o);
            }
            else if (type == MeshType.road)
            {
                vs = rope(line, o, fix_block, fix_mesh, right);
                tcs = right ? rope_tcs_left_to_right(vs, seam) : rope_tcs_right_to_left(vs, seam);
            }
            else
            {
                vs = bottom_block(line);
                if (mapping == Mapping.projection)
                    tcs = projected_tcs(vs, o);
                else
                    tcs = right ? rope_tcs_left_to_right(vs, seam) : rope_tcs_right_to_left(vs, seam);
            }

            left_seam = tcs[0].x;
            right_seam = tcs[tcs.Length / 2 - 1].x;

            Mesh msh = new Mesh();
            msh.name = "mesh";
            msh.vertices = vs;
            msh.triangles = triangles(vs);
            msh.uv = tcs;
            msh.RecalculateNormals();
            msh.RecalculateBounds();

            GameObject obj = new GameObject(name);
            obj.AddComponent<MeshFilter>().sharedMesh = msh;
            ////////Debug.Log("TAG IS" + tag);
            obj.tag = tag;
            obj.layer = layer;
            obj.transform.position += offset;
            MeshRenderer r = obj.AddComponent<MeshRenderer>();
            r.sharedMaterial = material;

            GeneratedMesh gmsh = obj.AddComponent<GeneratedMesh>();
            gmsh.collider_type = collider_type;
            gmsh.collider_material = collider_material;
            gmsh.build_collider();
            if (collider_type != GeneratedMesh.ColliderType.none)
            {
                Rigidbody2D rb = obj.AddComponent<Rigidbody2D>();
                rb.isKinematic = true;
            }
            gmsh.left_seam = left_seam;
            gmsh.right_seam = right_seam;
            gmsh.name = name;
            gmsh.hideFlags = HideFlags.HideInInspector;
            return gmsh;
        }

        Vector2[] projected_tcs(Vector3[] vs, Vector3 o)
        {
            Vector2[] uvs = new Vector2[vs.Length];
            int diminish = (int)((vs[0].x + o.x) / tile_size);
            for (int i = 0; i < vs.Length; i++)
                uvs[i] = new Vector2((vs[i].x + o.x) / tile_size  - diminish,  (vs[i].y + o.y) / tile_size);
            return uvs;
        }

      /*  Vector2[] path_tcs(Vector3[] vs, Vector3 o, float seam)
        {
            Vector2[] uvs = new Vector3[vs.Length];
            int c = vs.Length / 2;
            float aspect = 1f / tile_size;
            uvs[0] = new Vector2(seam, 1f);
            uvs[vs.Length - 1] = new Vector2(seam, 0f);
            for (int i = 1; i < c; i++)
            {
                u += aspect * (vs[i] - vs[i - 1]).magnitude;
                uvs[i] = new Vector2(u, 1f);
                uvs[vs.Length - i - 1] = new Vector2(seam, 0f);
            }
            return uvs;
        }*/

        Vector2[] rope_tcs_left_to_right(Vector3[] vs, float seam)
        {
            Vector2[] uvs = new Vector2[vs.Length];
            int c = vs.Length / 2;
            float aspect = 1f / tile_size;
            uvs[0] = new Vector2(seam, 1f);
            uvs[vs.Length - 1] = new Vector2(seam, 0f);
            for (int i = 1; i < c; i++)
            {
                seam += aspect * (vs[i] - vs[i - 1]).magnitude;
                uvs[i] = new Vector2(seam, 1f);
                uvs[vs.Length - i - 1] = new Vector2(seam, 0f);
            }
            return uvs;
        }

        Vector2[] rope_tcs_right_to_left(Vector3[] vs, float seam)
        {
            Vector2[] uvs = new Vector2[vs.Length];
            int c = vs.Length / 2;
            float aspect = 1f / tile_size;
            int i = c - 1;
            uvs[i] = new Vector2(seam, 1f);
            uvs[vs.Length - i - 1] = new Vector2(seam, 0f);

            for (i = c - 2; i >= 0; i--)
            {
                seam -= aspect * (vs[i] - vs[i + 1]).magnitude;
                uvs[i] = new Vector2(seam, 1f);
                uvs[vs.Length - i - 1] = new Vector2(seam, 0f);
            }
            return uvs;
        }

        Vector3[] bottom_block(Vector3[] line)
        {
            Vector3[] vs = new Vector3[line.Length * 2];
            for (int i = 0; i < line.Length; i++)
                vs[i] = line[i];
            for (int i = line.Length - 1; i >= 0; i--)
                vs[vs.Length - i - 1] = new Vector3(line[i].x, line[i].y  - thickness);
            return vs;
        }

        Vector3[] top_block(Vector3[] line)
        {
            Vector3[] res = new Vector3[line.Length * 2];
            for (int i = 0; i < line.Length; i++)
                res[i] = new Vector3(line[i].x, line[i].y + gap + thickness);
            for (int i = line.Length - 1; i >= 0; i--)
                res[res.Length - i - 1] = new Vector2(line[i].x, line[i].y + gap);
            return res;
        }

        Vector3 rope_vertex(Vector3 v, Vector3 left, Vector3 right, float d)
        {
            Vector3 p = right - left;
            p = new Vector2(-p.y, p.x).normalized * d;
            return v - p;
        }

       /* Vector3 rope_vertex(Vector2 p, Vector2 left, Vector2 right, float d) {
            Vector2 tangent = ((right - p).normalized + (p - left).normalized).normalized;
            Vector2 miter = new Vector2(tangent.y, -tangent.x);
            return p + miter;
        }*/

        bool should_be_connected(Vector3 p0, Vector3 p1)
        {
            float accuracy = .001f;
            return (p0 - p1).sqrMagnitude < accuracy;
        }

        Vector3[] rope(Vector3[] line, Vector3 o, Block fix_block, GeneratedMesh fix_mesh, bool right)
        {
            Vector3 fix_point;
            Vector3 od = fix_block != null ? fix_block.transform.position - o : new Vector3(0, 0, 0);
            Vector3[] vs = new Vector3[line.Length * 2];

            int i = 0;
            Vector3 p;
            bool connect = false;
            if (right)
                connect = fix_mesh == null ? false : should_be_connected(line[0], fix_block.points[fix_block.points.Length - 1] + od);
            else
                connect = fix_mesh == null ? false : should_be_connected(line[line.Length - 1], fix_block.points[0] + od);


            if (!right || !connect)
            {
                p = line[0];
                vs[0] = p;
                p.y -= thickness;
                vs[vs.Length - 1] = p;
            }
            else
            {
                fix_point = fix_block.points[fix_block.points.Length - 2] + od;
                vs[0] = line[0];
                vs[vs.Length - 1] = rope_vertex(line[0], fix_point, line[1], thickness);

                Mesh mesh = fix_mesh.GetComponent<MeshFilter>().sharedMesh;
                Vector3[] mvs = mesh.vertices;
                int index = mvs.Length / 2 - 1;
                mvs[index] = vs[0] - fix_block.transform.position + o;
                mvs[mvs.Length - index - 1] = vs[vs.Length - 1] - fix_block.transform.position + o;
                mesh.vertices = mvs;
                fix_mesh.build_collider();
            }

            for (i = 1; i < line.Length - 1; i++)
            {
                vs[i] = line[i];
                vs[vs.Length - i - 1] = rope_vertex(line[i], line[i - 1], line[i + 1], thickness);
            }

            if (right || !connect)
            {
                i = line.Length - 1;
                p = line[i];
                vs[i] = p;
                p.y -= thickness;
                vs[vs.Length - i - 1] = p;
            }
            else
            {
                int index = line.Length - 1;
                fix_point = fix_block.points[1] + od;
                vs[index] = line[index];
                vs[vs.Length - index - 1] = rope_vertex(line[index], line[index - 1], fix_point, thickness);

                Mesh mesh = fix_mesh.GetComponent<MeshFilter>().sharedMesh;
                Vector3[] mvs = mesh.vertices;
                mvs[0] = vs[index] - fix_block.transform.position + o;
                mvs[mvs.Length - 1] = vs[vs.Length - index - 1] - fix_block.transform.position + o;
                mesh.vertices = mvs;
                fix_mesh.build_collider();
            }

            return vs;
        }

        int[] triangles(Vector3[] vs)
        {
            Stack<int> ts = new Stack<int>();
            int l = vs.Length / 2;

            for (int i = 0; i < l; i++)
            {
                ts.Push(i);
                ts.Push(vs.Length - i - 2);
                ts.Push(i + 1);

                ts.Push(i);
                ts.Push(vs.Length - i - 1);
                ts.Push(vs.Length - i - 2);
            }
            return ts.ToArray();
        }
    }
}