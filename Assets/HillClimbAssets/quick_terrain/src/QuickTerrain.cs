﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class QuickTerrain : MonoBehaviour
    {

#if UNITY_EDITOR
        public bool items_fold = false;
        public bool block_fold = true;
#endif

        public Seed master_seed;
        public int blocks_total = 6;
        public int segments = 40;
        public float block_size = 20f;

        public Transform agent;
        public NoiseList noise_list;
        public MeshStructure mesh_structure;
        public List<IndexEvent> actions;
        public ItemGenerator[] item_generators;
        public GameObject[] generation_scripts;

        bool[] overriden_height;
        Stack<Noise>[] blended_noise;
        List<ItemSet>[] items;
        List<MeshGenerator>[] meshes;

        Queue active_blocks;
        Stack<Block> right_sleep;
        Stack<Block> left_sleep;
        float gen_distance;

        public delegate void BlockEvent(Block block);
        public delegate void ItemEvent(ItemGenerationData e);
        public delegate void PreGenerationEvent(int index);
        public event BlockEvent on_generation_over;
        public event ItemEvent on_item_generated;
        public event PreGenerationEvent on_generation;

        string insert_error = "Inserting allowed in on_generation event handler only.";
        CustomBlock inserted_block;
        Vector3[] custom_verts;
        float custom_size;
        bool insert_allowed = false;
        bool gen_phase = false;


        bool initial_generation;
        public int index { get; private set; }
        int side { get { return index >= 0 ? 1 : 0; } }

        void Awake()
        {
            imidiate_clear();
            StartGeneration();
            gen_distance = Mathf.Min(
                block_size * blocks_total / 3f,
                active_blocks.last.transform.position.x + active_blocks.last.width - active_blocks.first.transform.position.x);
        }


        /// <summary>
        /// Modifyes noise by assigning new values.
        /// </summary>
        /// <param name="data">List of sample data to modify</param>
        /// <param name="slope">Slope value</param>
        /// <param name="iterations">Amount of iterations required to interpolate old noise into new one</param>
        public void ModifyNoiseAssignment(SampleModifyData[] data, float slope = 0f, int iterations = 1)
        {
            int i = side;
            Noise old;
            Noise n;
            if (index >= 0)
            {
                old = active_blocks.last.noise;
                n = old.clone;
                n.slope_offset = Noise.calculate_slope_offset(active_blocks.last.right_handle, old.slope, old.slope_offset);
            }
            else
            {
                old = active_blocks.first.noise;
                n = old.clone;
                n.slope_offset = Noise.calculate_slope_offset(active_blocks.first.left_handle, old.slope, old.slope_offset);
            }
            n.slope = slope;


            for (int j = 0; j < data.Length; j++)
            {
                if (data[j].index < 0 || data[j].index >= n.samples.Length)
                    Debug.LogError("unacceptable sample index in 'modify noise' action");
                n.samples[data[j].index].amplitude = data[j].amplitude;
                n.samples[data[j].index].length = data[j].length;
                n.samples[data[j].index].smoothnes = data[j].smoothnes;
            }
            if (iterations > 0)
                blended_noise[i] = blend_noise(old, n, iterations);
        }

        /// <summary>
        /// Modifyes noise by adding new values.
        /// </summary>
        /// <param name="data">List of sample data to modify</param>
        /// <param name="slope">Slope value</param>
        /// <param name="iterations">Amount of iterations required to interpolate old noise into new one</param>
        public void ModifyNoiseSampleAdd(SampleModifyData[] data, float slope, int iterations = 1)
        {
            int i = side;
            Noise old;
            Noise n;
            if (index >= 0)
            {
                old = active_blocks.last.noise;
                n = old.clone;
                n.slope_offset = Noise.calculate_slope_offset(active_blocks.last.right_handle, old.slope, old.slope_offset);
            }
            else
            {
                old = active_blocks.first.noise;
                n = old.clone;
                n.slope_offset = Noise.calculate_slope_offset(active_blocks.first.left_handle, old.slope, old.slope_offset);
            }
            n.slope += slope;
            for (int j = 0; j < data.Length; j++)
            {
                if (data[j].index < 0 || data[j].index >= n.samples.Length)
                    Debug.LogError("unacceptable sample index in 'modify noise sample' action");
                n.samples[data[j].index].amplitude += data[j].amplitude;
                n.samples[data[j].index].length += data[j].length;
                n.samples[data[j].index].smoothnes += data[j].smoothnes;
            }
            if (iterations > 0)
                blended_noise[i] = blend_noise(old, n, iterations);
        }
        /// <summary>
        /// Raplaces old noiew with new one
        /// </summary>
        /// <param name="n">new noise</param>
        /// <param name="iterations">Amount of iterations required to interpolate old noise into new one</param>
        public void ReplaceNoise(NoiseList n, int iterations = 1)
        {
            int i = side;
            Noise old = index >= 0 ? active_blocks.last.noise : active_blocks.first.noise;
            Noise new_noise = new Noise();
            new_noise.samples = n.samples.ToArray();
            new_noise.slope = n.slope;
            new_noise.init(master_seed.value);

            if (iterations > 0)
                blended_noise[i] = blend_noise(old, new_noise, iterations);
        }
        /// <summary>
        /// Replaces mesh setup
        /// </summary>
        /// <param name="n">new mesh setup</param>
        public void ReplaceMeshSetup(MeshStructure n)
        {
            int i = side;
            meshes[i] = new List<MeshGenerator>();
            foreach (MeshGenerator m in n.meshes)
                meshes[i].Add(m.clone);
        }
        /// <summary>
        /// Adds new item into the generation
        /// </summary>
        /// <param name="item">new item</param>
        public void AddItem(ItemSet item)
        {
            ItemSet it = item.clone;
            it.init(master_seed.value, index);
            items[side].Add(it);
        }
        /// <summary>
        /// Removes old item collection and replaces it with new one
        /// </summary>
        /// <param name="it">Item collection</param>
        public void ReplaceItems(IEnumerable<ItemGenerator> it)
        {
            int i = side;
            items[i].Clear();
            foreach (ItemGenerator s in it)
            {
                ItemSet clone = s.itemset.clone;
                clone.init(master_seed.value, index);
                items[i].Add(clone);
            }
        }
        /// <summary>
        /// Removes item from generation
        /// </summary>
        /// <param name="item_name">Name of an item to remove</param>
        public void RemoveItem(string item_name)
        {
            int i = side, j = -1;
            foreach (ItemSet set in items[i])
            {
                j++;
                if (set.name == item_name)
                    break;
            }
            if (j >= 0 && j < items[i].Count)
            {
                if (gen_phase)
                    items[i][j].prepare_to_remove();
                else
                    items[i].RemoveAt(j);
            }
        }
        /// <summary>
        /// Replaces material of a mesh
        /// </summary>
        /// <param name="mesh_material">new material</param>
        /// <param name="mesh_name">name of a mesh</param>
        public void ReplaceMeshMaterial(Material mesh_material, string mesh_name)
        {
            int i = side;
            foreach (MeshGenerator m in meshes[i])
                if (m.name == mesh_name)
                    m.material = mesh_material;
        }

        void LateUpdate()
        {
            if (agent == null)
                return;
            if (!active_blocks.empty)
            {
                float x = agent.transform.position.x;
                if (x + gen_distance > active_blocks.last.transform.position.x + active_blocks.last.width)
                    add_block(true);
                else if (x - gen_distance < active_blocks.first.transform.position.x)
                    add_block(false);
            }
        }

        public void init()
        {
            /* if (item_generators == null)
                 item_generators = new List<ItemGenerator>();*/
            if (active_blocks == null)
                active_blocks = new Queue();
            if (actions == null)
                actions = new List<IndexEvent>();

            initial_generation = true;
            right_sleep = new Stack<Block>();
            left_sleep = new Stack<Block>();
        }

        Vector3[] get_verts(float sx, float w, int segs, Noise n)
        {
            float segment_size = w / (float)segs;
            int amount = segs + 1;
            Vector3[] vs = new Vector3[amount];
            for (int i = 0; i < amount; i++)
            {
                float x = sx + i * segment_size;
                vs[i] = new Vector3(x - sx, n.value_at(x));
            }
            return vs;
        }

        Vector3[] get_blended_verts(float sx, float w, int segs, Noise n1, Noise n2, bool right_to_left = false)
        {
            float segment_size = w / (float)segs;
            int amount = segs + 1;
            Vector3[] vs = new Vector3[amount];
            for (int i = 0; i < amount; i++)
            {
                float x = sx + i * segment_size;
                float t = (float)i / ((float)amount - 1);
                if (right_to_left) t = 1 - t;
                vs[i] = new Vector3(x - sx, n2.value_at(x, t, n1));
            }

            return vs;
        }

        Vector3[] custom_verts_addition(Vector3[] vertices, float handle, Noise n, AnimationCurve weight_map)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                float w = (float)i / (float)(vertices.Length - 1);
                w = Mathf.Clamp(weight_map.Evaluate(w), 0f, 1f);
                vertices[i].y = n.value_at(vertices[i].x + handle) + vertices[i].y * w;
            }
            return vertices;
        }

        Vector3[] custom_verts_blend(Vector3[] vertices, float handle, float h, Noise n, AnimationCurve weight_map)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                float w = (float)i / (float)(vertices.Length - 1);
                w = Mathf.Clamp(weight_map.Evaluate(w), 0f, 1f);
                float ym = n.value_at(vertices[i].x + handle);
                vertices[i].y = ym - (ym - (vertices[i].y + h)) * w;
            }
            return vertices;
        }

        Vector3[] custom_verts_insert(Vector3[] vertices, float h)
        {
            for (int i = 0; i < vertices.Length; i++)
                vertices[i].y += h;
            return vertices;
        }

        Vector3[] overriden_verts(Vector3 p0, Vector3 p1, int segment_count)
        {
            if (segment_count < 2) segment_count = 2;
            Vector3[] vs = new Vector3[segment_count + 1];
            vs[0] = p0;
            vs[vs.Length - 1] = p1;

            for (int i = 1; i < segment_count; i++)
            {
                float t = (float)i / (float)segment_count;
                float w = (1 - Mathf.Cos(t * Mathf.PI)) * .5f;
                vs[i] = new Vector3(
                    p0.x + (p1.x - p0.x) * t,
                    p0.y + (p1.y - p0.y) * w
                    );
            }
            return vs;
        }

        void generate_custom_block(int index)
        {
            foreach (IndexEvent e in actions)
                e.Check(index, this);
        }

        Vector3 calculate_offset(Vector2 x, Vector2 y, Vector2 z, float r)
        {
            float ox = x.x + (x.y - x.x) * r;
            float oy = y.x + (y.y - y.x) * r;
            float oz = z.x + (z.y - z.x) * r;
            return new Vector3(ox, oy, oz);
        }

        void generate_items(Block block)
        {
            float x0 = block.transform.localPosition.x;
            float x1 = x0 + block.width;
            int k = side;
            for (int i = 0; i < items[k].Count; i++)
            {
                if (items[k][i].wasted)
                {
                    items[k].RemoveAt(i);
                    i--;
                    continue;
                }

                ItemSet set = items[k][i];

                if (set.BlockToFilter(block.gameObject))
                    continue;
                GenericQueue<ItemInstantiationData> item_queue = new GenericQueue<ItemInstantiationData>();
                if (index >= 0)
                {
                    float n = (x0 - x0 % set.distance);
                    if (n < x0)
                        n += set.distance;
                    for (float x = n; x < x1; x += set.distance)
                    {
                        ItemInstantiationData data = set.generate(x, x0, x1, block, this);
                        if(data != null)
                            item_queue.push(data);
                    }
                }
                else
                {
                    float n = (x1 + Mathf.Abs(x1 % set.distance));
                    if (n > x1)
                        n -= set.distance;
                    for (float x = n; x > x0; x -= set.distance)
                    {
                        if (x == 0f) set.skip(x);
                        ItemInstantiationData data = set.generate(x, x0, x1, block, this);
                        if (data != null)
                            item_queue.push(data);
                    }
                }
                if(!item_queue.empty)
                {
                    if(Application.isPlaying && !initial_generation)
                        StartCoroutine(instatiate_items_coroutine(item_queue));
                    else
                        item_queue.each(instatiate_item);
                }
            }
        }

        void instatiate_item(ItemInstantiationData data)
        {
            GameObject obj = GameObject.Instantiate<GameObject>(data.item);
            obj.name = data.item.name;

            data.block.place_object(data.placement, obj, data.align_to_normal);
            if (data.do_offset)
                obj.transform.position += data.offset;

            if (data.do_scale)
                obj.transform.localScale *= data.scale;

            if (data.do_rotation)
                obj.transform.eulerAngles += new Vector3(0f, 0f, data.rotation);


            if (!data.block_as_parrent)
                obj.transform.parent = data.block.transform.parent;


            ItemGenerationData e = new ItemGenerationData();
            e.item = obj;
            e.block = data.block;
            e.sender = this;
            e.index = data.index;
            e.group_index = data.group_index;
            e.set = data.set;

            IItemGenerationHandler handler = obj.GetComponent(typeof(IItemGenerationHandler)) as IItemGenerationHandler;
            if (handler != null)
                handler.OnItemGenerated(e);
            if (on_item_generated != null)
                on_item_generated(e);
        }

        IEnumerator instatiate_items_coroutine(GenericQueue<ItemInstantiationData> item_queue)
        {
            foreach (ItemInstantiationData data in item_queue)
            {
                instatiate_item(data);
                yield return null;
            }
        }

        void instantiate_custom_items(Block block, float height)
        {
            if (inserted_block.objects == null)
                return;
            for (int i = 0; i < inserted_block.objects.Length; i++)
            {
                if (inserted_block.objects[i] != null)
                {
                    GameObject obj = Instantiate<GameObject>(inserted_block.objects[i]);
                    obj.transform.parent = block.transform;
                    obj.transform.localPosition = inserted_block.objects[i].transform.localPosition + new Vector3(0, height, 0);
                    obj.name = inserted_block.objects[i].name;
                }
            }
        }

        public void StartGeneration()
        {
            init();
            transform.localScale = new Vector3(1f, 1f, 1f);
            transform.eulerAngles = new Vector3();
            if (active_blocks.count > 0)
                return;

            if (noise_list == null)
            {
                Debug.LogError("Noise list is required");
                return;
            }

            if (mesh_structure == null)
            {
                Debug.LogError("Mesh structure is required");
                return;
            }

            overriden_height = new bool[2] { false, false };

            if (master_seed.type == Seed.Type.random)
                master_seed.value = Random.Range(int.MinValue, int.MaxValue);

            meshes = new List<MeshGenerator>[2];
            items = new List<ItemSet>[2];
            for (int i = 0; i < 2; i++)
            {
                items[i] = new List<ItemSet>();
                if (item_generators != null)
                {
                    foreach (ItemGenerator gen in item_generators)
                    {
                        if (gen == null)
                        {
                            Debug.LogWarning("item is null in quick terrain items");
                            continue;
                        }
                        ItemSet set = gen.itemset.clone;
                        set.init(master_seed.value, i == 0 ? -1 : 1);
                        items[i].Add(set);
                    }
                }

                meshes[i] = new List<MeshGenerator>();
                foreach (MeshGenerator m in mesh_structure.meshes)
                    meshes[i].Add(m.clone);
            }

            blended_noise = new Stack<Noise>[2];

            extract_generation_scripts();


            build_root_block();
            for (int i = 1; i < blocks_total; i++)
                add_block(i % 2 != 0);

            initial_generation = false;
        }

        void extract_generation_scripts()
        {
            if (generation_scripts == null)
                return;
            foreach (GameObject obj in generation_scripts)
            {
                Component[] all_components = obj.GetComponents(typeof(ICustomGenerator));
                for (int i = 0; i < all_components.Length; i++)
                {
                    if (all_components[i] is ICustomGenerator)
                    {
                        ICustomGenerator custom_generator = all_components[i] as ICustomGenerator;
                        custom_generator.InitiateGeneration(this);
                    }
                }
            }
        }


        Stack<Noise> blend_noise(Noise old_noise, Noise new_noise, int iterations)
        {
            float handle = index >= 0 ? active_blocks.last.right_handle : active_blocks.first.left_handle;
            float handle_step = index > 0 ? block_size : -block_size;
            float seam = handle + handle_step;
            Noise[] res = new Noise[iterations];
            int sample_count = Mathf.Max(old_noise.samples.Length, new_noise.samples.Length);
            Noise last_noise = null;
            Noise blended = null;
            for (int i = 1; i <= iterations; i++)
            {
                float t = (float)i / iterations;
                blended = new Noise();
                blended.slope = new_noise.slope;
                blended.slope_offset = new_noise.slope_offset;
                blended.samples = new NoiseSample[sample_count];
                for (int j = 0; j < sample_count; j++)
                {
                    NoiseSample n0 = old_noise.samples.Length > j ? old_noise.samples[j] : null;
                    NoiseSample n1 = new_noise.samples.Length > j ? new_noise.samples[j] : null;
                    float a0, a1, l0, l1, o, sm;

                    if (n0 != null && n1 != null)
                    {
                        a0 = n0.amplitude;
                        sm = n1.smoothnes;
                        a1 = n1.amplitude;
                        l0 = n0.length;
                        l1 = n1.length;
                        o = n0.offset;
                    }
                    else if (n0 == null)
                    {
                        o = a0 = 0f;
                        a1 = n1.amplitude;
                        l0 = l1 = n1.length;
                        sm = n1.smoothnes;
                    }
                    else
                    {
                        a0 = n0.amplitude;
                        a1 = 0f;
                        l0 = l1 = n0.length;
                        o = n0.offset;
                        sm = n0.smoothnes;
                    }

                    NoiseSample s = new NoiseSample();
                    s.amplitude = a0 + (a1 - a0) * t;
                    s.length = l0 + (l1 - l0) * t;
                    s.smoothnes = sm;
                    if (n1 == null)
                    {
                        s.offset = 0f;
                        s.seed.value = n0.seed.value;
                    }
                    else
                    {
                        float d0 = l0, d1 = l1;
                        if (last_noise != null)
                        {
                            d0 = last_noise.samples[j].length;
                            d1 = s.length;
                            o = last_noise.samples[j].offset;
                        }
                        s.offset = seam / d0 + o - seam / d1;
                        s.seed.value = n1.seed.value;
                    }
                    blended.samples[j] = s;
                }
                seam += handle_step;
                last_noise = blended;
                res[i - 1] = blended;
            }

            Stack<Noise> result = new Stack<Noise>();
            for (int i = res.Length - 1; i >= 0; i--)
                result.Push(res[i]);

            return result;
        }
        Block side_block(bool right)
        {
            return right ? active_blocks.last : active_blocks.first;
        }

        Block awake_block(bool right)
        {
            Block b;
            if (right && right_sleep.Count > 0)
            {
                b = right_sleep.Pop();
                b.gameObject.SetActive(true);
            }
            else if (!right && left_sleep.Count > 0)
            {
                b = left_sleep.Pop();
                b.gameObject.SetActive(true);
            }
            else b = null;
            return b;
        }

        void build_root_block()
        {
            index = 0;
            Noise noise = new Noise();
            noise = new Noise();
            noise.slope = noise_list.slope;
            noise.samples = noise_list.samples.ToArray();
            noise.init(master_seed.value);
            Vector3[] verts = get_verts(0f, block_size, segments, noise);
            Block block = build_block(right_block_postion(), verts);
            block.noise = noise;
            block.left_handle = 0f;
            block.right_handle = block_size;
            active_blocks.push(block);
        }

        void add_block(bool right)
        {
            Block block = awake_block(right);
            if (block == null)
            {
                gen_phase = true;
                int g_side = right ? 1 : 0;
                Block pro = side_block(right);
                float start_handle = pro.get_handle(right);
                float end_handle;
                index = pro.index + (right ? 1 : -1);
                Noise pro_noise = pro.noise;
                float starting_height = pro.get_height(right);


                insert_allowed = true;
                if (on_generation != null)
                    on_generation(index);
                if (inserted_block == null && index != 0)
                    generate_custom_block(index);
                insert_allowed = false;


                Vector3[] verts;
                if (inserted_block == null)
                {
                    start_handle = right ? pro.right_handle : pro.left_handle - block_size;

                    Noise noise_to_blend = null;
                    if (blended_noise[g_side] != null && blended_noise[g_side].Count > 0)
                        noise_to_blend = blended_noise[g_side].Pop();

                    verts = noise_to_blend == null ?
                        get_verts(start_handle, block_size, segments, pro_noise) :
                        get_blended_verts(start_handle, block_size, segments, noise_to_blend, pro_noise, !right);

                    block = build_block(next_block_position(right), verts, pro, right);
                    if (noise_to_blend != null)
                    {
                        block.noise = noise_to_blend;
                        block.secondary_noise = pro_noise;
                    }
                    else
                        block.noise = pro_noise;


                    block.left_handle = start_handle;
                    block.right_handle = start_handle + (right ? block_size : -block_size);

                }
                else if (inserted_block.type == CustomBlock.InsertType.addition)
                {
                    start_handle = right ? pro.right_handle : pro.left_handle - custom_size;
                    end_handle = start_handle + (right ? custom_size : -custom_size);

                    verts = custom_verts_addition(custom_verts, start_handle, pro_noise, inserted_block.blend_curve);
                    block = build_block(next_block_position(right), verts, pro, right);
                    block.noise = pro_noise;

                    overriden_height[g_side] = inserted_block.override_height;

                    block.left_handle = start_handle;
                    block.right_handle = end_handle;

                }
                else if (inserted_block.type == CustomBlock.InsertType.blend)
                {
                    start_handle = right ? pro.right_handle : pro.left_handle - custom_size;
                    end_handle = start_handle + (right ? custom_size : -custom_size);

                    verts = custom_verts_blend(custom_verts, start_handle, starting_height, pro_noise, inserted_block.blend_curve);
                    block = build_block(next_block_position(right), verts, pro, right);
                    block.noise = pro_noise;
                    instantiate_custom_items(block, starting_height);

                    overriden_height[g_side] = inserted_block.override_height;

                    block.left_handle = start_handle;
                    block.right_handle = end_handle;

                }
                else
                {
                    start_handle = right ? pro.right_handle : pro.left_handle;
                    verts = custom_verts_insert(custom_verts, starting_height);
                    block = build_block(next_block_position(right), verts, pro, right);
                    block.noise = pro_noise;
                    instantiate_custom_items(block, starting_height);
                    overriden_height[g_side] = inserted_block.override_height;
                    block.right_handle = block.left_handle = start_handle;
                }

                generate_items(block);

                reset_custom_block_data();
                gen_phase = false;
                if (on_generation_over != null)
                    on_generation_over(block);
            }

            if (right)
            {
                active_blocks.push(block);
                if (active_blocks.count > blocks_total)
                    deactivate_left();
            }
            else
            {
                active_blocks.unshift(block);
                if (active_blocks.count > blocks_total)
                    deactivate_right();
            }
        }

        Block build_block(Vector3 o, Vector3[] vs, Block fix = null, bool right = true)
        {
            int side_index = right ? 1 : 0;
            if (overriden_height[side_index])
            {
                o.y += right ?
                    fix.transform.position.y + fix.points[fix.points.Length - 1].y - (o.y + vs[0].y) :
                    fix.transform.position.y + fix.points[0].y - (o.y + vs[vs.Length - 1].y);
                overriden_height[side_index] = false;
            }


            GameObject obj = new GameObject("terrain_block " + index);

            Block b = obj.AddComponent<Block>();
            b.meshes = new List<GeneratedMesh>();
            foreach (MeshGenerator mi in meshes[side_index])
            {
                GeneratedMesh fix_mesh = fix == null ? null : fix.get_mesh(mi.name);
                GeneratedMesh m;
                if (inserted_block != null && mi.type == MeshGenerator.MeshType.top && !inserted_block.overrides_top)
                    m = mi.build_mesh(overriden_verts(vs[0], vs[vs.Length - 1], segments), o, fix, fix_mesh, right);
                else if (inserted_block != null && mi.type != MeshGenerator.MeshType.top && !inserted_block.overrides_basic)
                    m = mi.build_mesh(overriden_verts(vs[0], vs[vs.Length - 1], segments), o, fix, fix_mesh, right);
                else
                    m = mi.build_mesh(vs, o, fix, fix_mesh, right);
                m.transform.parent = b.transform;
                b.meshes.Add(m);
            }

            b.points = vs;
            b.index = index;
            b.custom = inserted_block != null;
            b.left_height = vs[0].y;
            b.right_height = vs[vs.Length - 1].y;



            b.transform.position = o;
            obj.transform.parent = transform;

            if (inserted_block != null)
                b.gameObject.tag = inserted_block.gameObject.tag;

            return b;
        }
        /// <summary>
        /// Inserts custom block at this generation iteration.
        /// Use this function while handling on_generation event.
        /// </summary>
        /// <param name="_custom_block">Custom block to add</param>
        public void InsertCustomBlock(CustomBlock _custom_block)
        {
            if (!insert_allowed)
            {
                Debug.LogError(insert_error);
                return;
            }
            inserted_block = _custom_block;
            if (inserted_block.use_states && inserted_block.has_both_states())
                inserted_block.interpolate_state(inserted_block.state_slider);
            custom_verts = inserted_block.calculate_vertices();
            custom_size = custom_verts[custom_verts.Length - 1].x - custom_verts[0].x;
        }

        public void InsertRandomBlock(CustomBlockSet blocks)
        {
            blocks.init(master_seed.value);
            CustomBlockInfo block_info = blocks.get_item(index);
            CustomBlock b = block_info.block;

            b.state_slider = block_info.random_state ?
                NoiseSample.normalized_value(index, master_seed.value + 1) :
                block_info.state;

            InsertCustomBlock(b);
        }

        void reset_custom_block_data()
        {
            inserted_block = null;
        }

        void deactivate_left()
        {
            Block b = active_blocks.shift();
            b.gameObject.SetActive(false);
            left_sleep.Push(b);
        }

        void deactivate_right()
        {
            Block b = active_blocks.pop();
            b.gameObject.SetActive(false);
            right_sleep.Push(b);
        }

        void imidiate_clear()
        {
            re_init();
            List<GameObject> children = new List<GameObject>();
            foreach (Transform child in transform) children.Add(child.gameObject);
            children.ForEach(child => DestroyImmediate(child));
        }

        public void re_init()
        {
            init();
            on_generation_over = null;
            on_generation = null;
            on_item_generated = null;
            meshes = null;
            items = null;
            reset_custom_block_data();
            active_blocks.nulify();
        }


        public void OnValidate()
        {
            if (segments < 1) segments = 1;
            if (block_size < .1f) block_size = .1f;
            if (blocks_total < 2) blocks_total = 2;

            if (master_seed.type == Seed.Type.master)
                master_seed.type = Seed.Type.constant;
        }

        Vector3 next_block_position(bool right)
        {
            return right ? right_block_postion() : left_block_postion();
        }

        Vector3 right_block_postion()
        {
            if (!active_blocks.empty)
            {
                Vector3 r = active_blocks.last.transform.position;
                r.x += active_blocks.last.width;
                return r;
            }
            return transform.position;
        }

        Vector3 left_block_postion()
        {
            Vector3 r;
            if (!active_blocks.empty)
                r = active_blocks.first.transform.position;
            else
                r = transform.position;
            r.x = inserted_block == null ? r.x - block_size : r.x - custom_size;
            return r;
        }


        public ItemSet GetItemRule(string rule_name, bool right)
        {
            int i = right ? 1 : -1;
            foreach (ItemSet set in items[i])
                if (set.name == rule_name)
                    return set;
            return null;
        }

        public bool is_right_side { get { return index >= 0; } }
    }
}