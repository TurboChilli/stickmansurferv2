﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class Queue 
    {
        public Block first = null;
        public Block last = null;
        public bool empty { get { return first == null; } }
        public int count { get { return n; } }
        public int n = 0;
        public Queue() { }

        public void push(Block block)
        {
            if(last == null)
                first = last = block;
            else
            {
                last.right = block;
                block.left = last;
                last = block;
            }
            n++;
        }

        public void unshift(Block block)
        {
            if (first == null)
                first = last = block;
            else
            {
                first.left = block;
                block.right = first;
                first = block;
            }
            n++;
        }


        public Block pop()
        {
            Block res = last;
            if (first != last)
            {
                last = last.left;
                n--;
            }
            else
            {
                first = last = null;
                n = 0;
            }
            return res;
        }

        public Block shift()
        {
            Block res = first;
            if (first != last)
            {
                first = first.right;
                n--;
            }
            else
            {
                first = last = null;
                n = 0;
            }
            return res;
        }

        public void nulify()
        {
            first = last = null;
            n = 0;
        }
    }
}