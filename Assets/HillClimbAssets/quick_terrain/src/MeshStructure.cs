﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class MeshStructure : ScriptableObject
    {
        [SerializeField] public List<MeshGenerator> meshes;

        void OnEnable()
        {
            if (meshes == null)
            {
                meshes = new List<MeshGenerator>();
                meshes.Add(new MeshGenerator("mesh 0"));
            }
        }
    }
}
