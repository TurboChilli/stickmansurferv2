﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class NoiseList : ScriptableObject
    {
        public List<NoiseSample> samples;
        public float slope = 0f;

        void OnEnable()
        {
            if (samples == null)
                samples = new List<NoiseSample>();
        }
    }
}

