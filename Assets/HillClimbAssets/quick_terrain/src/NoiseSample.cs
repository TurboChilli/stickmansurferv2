﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class NoiseSample
    {
        public Seed seed;

        [SerializeField] float _amplitude = 1f;
        [SerializeField] float _length = 1f;
        [SerializeField] float _smoothnes = 1f;

        public float offset;
        public float amplitude { get { return _amplitude; } set { _amplitude = value >= 0 ? value : 0f; } }
        public float length { get { return _length; } set { _length = value > .01f ? value : .01f; } }
        public float smoothnes { get { return _smoothnes; } set { _smoothnes = Mathf.Clamp(value, 0f, 1f); } }

        public NoiseSample() 
        {
            seed = new Seed();
            smoothnes = 1f;
        }

        public float evaluate(float x)
        {
            return NoiseSample.interpolated_uniform_value(x, seed.value, length, offset, smoothnes) * amplitude;
        }

        public static float normalized_value(float x, int seed){
            return (interpolated_uniform_value(x, seed, 1f) + 1) / 2f;
        }

        public static float interpolated_uniform_value(float x, int seed, float scale_factor, float offset = 0f, float smootnes = 1f)
        {
            float sx = x / scale_factor + offset;
            int ix = (int)sx;
            float w = sx - ix;
            if(sx >= 0)
                return cosine(uniform_value(ix, seed), uniform_value(ix + 1, seed), w, smootnes);
            else
                return cosine(uniform_value(ix, seed), uniform_value(ix - 1, seed), -w, smootnes);
        }

        public static float uniform_value(int x, int seed)
        {
            int n = x + seed * 57;
            n = (n << 13) ^ n;
            return (1f - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) /
                1073741824f);
        }

        public static float cosine(float a, float b, float w, float smoothnes)
        {
            float t = (1 - Mathf.Cos(w * Mathf.PI)) * .5f;
            return a + linear(w, t, smoothnes) * (b - a);
        }

        public static float linear(float a, float b, float w)
        {
            return a + w * (b - a);
        }

        public NoiseSample clone
        {
            get
            {
                NoiseSample n = new NoiseSample();
                n.seed = seed;
                n.amplitude = amplitude;
                n.length = length;
                n.offset = offset;
                n.smoothnes = smoothnes;
                return n;
            }
        }
    }
}
