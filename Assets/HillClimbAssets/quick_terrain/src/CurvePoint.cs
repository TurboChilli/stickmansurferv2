﻿using UnityEngine;
using System.Collections;

public class CurvePoint : MonoBehaviour {

    [HideInInspector]
    public Transform left_control;
    [HideInInspector]
    public Transform right_control;
    [HideInInspector]
    public bool lock_x_at_zero;

    public Vector3 left_tangent { 
        get 
        {
            return left_control.transform.position; 
        }

        set
        {
            left_control.transform.position = value;
        }
    }
    public Vector3 right_tangent { 
        get
        {
            return right_control.transform.position; 
        }

        set
        {
            right_control.transform.position = value;
        }
    }

    public void OnCreate(bool left_control_visible = true, bool right_control_visible = true)
    {
        lock_x_at_zero = !left_control_visible;

        Vector3 p = transform.position;
        left_control = new GameObject("left_tangent").transform;
        left_control.hideFlags = HideFlags.HideInHierarchy;
        left_control.transform.position = p;
        left_control.transform.parent = transform;
        TangentControl t = left_control.gameObject.AddComponent<TangentControl>();
        t.left = true;
        t.point = transform;
        t.visible = left_control_visible;
        

        right_control = new GameObject("right_tangent").transform;
        right_control.hideFlags = HideFlags.HideInHierarchy;
        right_control.transform.position = p;
        right_control.transform.parent = transform;
        t = right_control.gameObject.AddComponent<TangentControl>();
        t.left = false;
        t.point = transform;
        t.visible = right_control_visible;
    }
    
    void OnDrawGizmos()
    {
        if (lock_x_at_zero)
        {
            Vector3 p = transform.localPosition;
            p.x = 0f;
            transform.localPosition = p;
        }
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(transform.position, .2f);
    }

    public CurvePointState build_state()
    {
        CurvePointState state = new CurvePointState();
        state.position = transform.localPosition;
        state.left_tangent = left_control.localPosition;
        state.right_tangent = right_control.localPosition;
        return state;
    }

}

[System.Serializable]
public class CurvePointState {
    public Vector3 position;
    public Vector3 left_tangent;
    public Vector3 right_tangent;
}
