﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{
    //NOTE : Generic type classes are not serializeable in Unity!
    public class GenericQueue<T> : IEnumerable, IEnumerator
    {
        public ItemQueueNode<T> first;
        public ItemQueueNode<T> last;


        ItemQueueNode<T> current;

        public bool empty { get { return first == null; } }
        public int count { get; private set; }
        public GenericQueue()
        {
            count = 0;
            current = first = last = null;
        }

        public void push(T item)
        {
            //if (item == null)
                //////Debug.Log("Ahtung!!");
            ItemQueueNode<T> node = new ItemQueueNode<T>(item);
            if(last == null)
                first = last = node;
            else
            {
                last.next = node;
                node.prev = last;
                last = node;
            }
            count++;
        }

        public void unshift(T item)
        {
            ItemQueueNode<T> node = new ItemQueueNode<T>(item);
            if (first == null)
                first = last = node;
            else
            {
                first.prev = node;
                node.next = first;
                first = node;
            }
            count++;
        }

        public T pop()
        {
            T data = last.data;
            if (first != last)
            {
                last = last.prev;
                last.next = null;
                count--;
            }
            else
            {
                first = last = null;
                count = 0;
            }
            return  data;
        }

        public T shift()
        {
            T data = first.data;
            if (first != last)
            {
                first = first.next;
                first.prev = null;
                count--;
            }
            else
            {
                first = last = null;
                count = 0;
            }
            return data;
        }

        public void nulify()
        {
            current = first = last = null;
            count = 0;
        }


        public IEnumerator GetEnumerator()
        {
            return this;
        }

        public bool MoveNext()
        {
            current = current == null ? first: current.next;
            if(current == null)
            {
                Reset();
                return false;
            }
            return true;
        }

        public void Reset()
        {
            current = null;
        }

        public object Current
        {
            get
            {
                return current.data;
            }
        }

        public void each(System.Action<T> callback)
        {
            ItemQueueNode<T> node = first;
            while(node != null)
            {
                callback(node.data);
                node = node.next;
            }
        }
    }

    public class ItemQueueNode<T>
    {
        public ItemQueueNode<T> next;
        public ItemQueueNode<T> prev;

        public T data;

        public ItemQueueNode(T _data)
        {
            data = _data;
        }
    }
}