﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class EventAction
    {
        public enum Type { 
            add_action,
            insert_block, 
            insert_random_block,
            add_item, 
            remove_item, 
            replace_item_list,
            replace_noise, 
            modify_noise_add,
            modify_noise_assign,
            reaplce_mesh_material,
            replace_mesh_setup
        }
        public string name;


        public EventAction() {}
        virtual public void Action(QuickTerrain qt) {}

        public static EventAction CreateAction(Type action_type)
        {
            EventAction action=null;
            if(action_type == Type.insert_block)
                action = new InsertBlockAction();
            else if (action_type == Type.insert_random_block)
                action = new InsertRandomBlockAction();
            else if(action_type == Type.add_item)
                action = new AddItemAction();
            else if (action_type == Type.remove_item)
                action = new RemoveItemAction();
            else if (action_type == Type.replace_noise)
                action = new ChangeNoiseAction();
            else if (action_type == Type.replace_mesh_setup)
                action = new ChangeMeshStructureAction();
            else if (action_type == Type.reaplce_mesh_material)
                action = new ReplaceMeshMaterialAction();
            else if (action_type == Type.modify_noise_add)
                action = new ModifyNoiseSampleAction();
            else if (action_type == Type.modify_noise_assign)
                action = new ModifyNoiseSampleApplyAction();
            else if (action_type == Type.replace_item_list)
                action = new ReplaceItemsAction();
            return action;
        }

        public static EventAction CreateAction(string action_name)
        {
            action_name = action_name.Replace(" ", "_");
            return CreateAction((EventAction.Type)System.Enum.Parse(typeof(EventAction.Type), action_name));
        }

        public static string[] all_actions()
        {
            string[] names = System.Enum.GetNames(typeof(EventAction.Type));
            for (int i = 0; i < names.Length; i++ )
                    names[i] = names[i].Replace("_", " ");
            return names;
        }

       
    }
}