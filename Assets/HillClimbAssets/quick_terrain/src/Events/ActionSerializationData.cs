﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ActionSerializationData
    {
        public List<InsertBlockAction> insert_block = new List<InsertBlockAction>();
        public List<InsertRandomBlockAction> random_block = new List<InsertRandomBlockAction>();
        public List<AddItemAction> add_item = new List<AddItemAction>();
        public List<RemoveItemAction> remove_item = new List<RemoveItemAction>();
        public List<ChangeNoiseAction> replace_noise = new List<ChangeNoiseAction>();
        public List<ChangeMeshStructureAction> change_meshes = new List<ChangeMeshStructureAction>();
        public List<ReplaceMeshMaterialAction> replace_mesh_material = new List<ReplaceMeshMaterialAction>();
        public List<ModifyNoiseSampleAction> noise_sample_add = new List<ModifyNoiseSampleAction>();
        public List<ModifyNoiseSampleApplyAction> noise_sample_apply = new List<ModifyNoiseSampleApplyAction>();
        public List<ReplaceItemsAction> replace_items = new List<ReplaceItemsAction>();


        public ActionSerializationData() { }


        public void prepare_to_serialization(List<EventAction> actions)
        {
            if (actions == null) return;

            insert_block.Clear();
            random_block.Clear();
            add_item.Clear();
            remove_item.Clear();
            replace_noise.Clear();
            change_meshes.Clear();
            replace_mesh_material.Clear();
            noise_sample_add.Clear();
            noise_sample_apply.Clear();
            replace_items.Clear();


            foreach(EventAction action in actions)
            {
                if(action is InsertBlockAction)
                    insert_block.Add(action as InsertBlockAction);

                else if(action is InsertRandomBlockAction)
                    random_block.Add(action as InsertRandomBlockAction);

                else if(action is AddItemAction)
                    add_item.Add(action as AddItemAction);

                else if(action is RemoveItemAction)
                    remove_item.Add(action as RemoveItemAction);

                else if(action is ChangeNoiseAction)
                    replace_noise.Add(action as ChangeNoiseAction);

                else if(action is ChangeMeshStructureAction)
                    change_meshes.Add(action as ChangeMeshStructureAction);

                else if(action is ReplaceMeshMaterialAction)
                    replace_mesh_material.Add(action as ReplaceMeshMaterialAction);

                else if(action is ModifyNoiseSampleApplyAction)
                    noise_sample_apply.Add(action as ModifyNoiseSampleApplyAction);

                else if(action is ModifyNoiseSampleAction)
                    noise_sample_add.Add(action as ModifyNoiseSampleAction);

                else if(action is ReplaceItemsAction)
                    replace_items.Add(action as ReplaceItemsAction);

                else
                    Debug.LogError("action type is missing in 'custom action serialization'");

            }
        }

        public List<EventAction> get_data()
        {
            List<EventAction> result = new List<EventAction>();

            foreach (EventAction action in insert_block)
                result.Add(action);

            foreach (EventAction action in random_block)
                result.Add(action);

            foreach (EventAction action in add_item)
                result.Add(action);

            foreach (EventAction action in remove_item)
                result.Add(action);

            foreach (EventAction action in replace_noise)
                result.Add(action);

            foreach (EventAction action in change_meshes)
                result.Add(action);

            foreach (EventAction action in replace_mesh_material)
                result.Add(action);

            foreach (EventAction action in noise_sample_add)
                result.Add(action);

            foreach (EventAction action in noise_sample_apply)
                result.Add(action);
            
            foreach (EventAction action in replace_items)
                result.Add(action);


            return result;
        }

    }
}