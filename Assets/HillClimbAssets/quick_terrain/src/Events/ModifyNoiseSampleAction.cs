﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class ModifyNoiseSampleAction : EventAction
    {
        public List<SampleModifyData> data;
        public float slope = 0f;
        public int iterations = 1;

        public ModifyNoiseSampleAction()
            : base()
        {
            name = "modify noise (addition)";
            data = new List<SampleModifyData>();
            data.Add(new SampleModifyData());
        }



        override public void Action(QuickTerrain qt)
        {
            qt.ModifyNoiseSampleAdd(data.ToArray(), slope, iterations);
        }

        virtual public void add_sample_data()
        {
            SampleModifyData d = new SampleModifyData();
            d.smoothnes = 0f;
            d.index = data.Count;
            data.Add(d);
        }
    }

    ///<summary>
    ///Defines which values should be modified in specific noise sample
    ///</summary>
    [System.Serializable]
    public class SampleModifyData
    {
        public int index = 0;
        public float amplitude = 0f;
        public float length = 0f;
        public float smoothnes = 0f;

        public SampleModifyData()
        {

        }
    }
}