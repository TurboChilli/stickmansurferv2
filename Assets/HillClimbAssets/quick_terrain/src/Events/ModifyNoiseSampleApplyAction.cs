﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class ModifyNoiseSampleApplyAction : ModifyNoiseSampleAction
    {
        public ModifyNoiseSampleApplyAction()
            : base()
        {
            name = "modify noise (assign)";
        }

        override public void Action(QuickTerrain qt)
        {
            qt.ModifyNoiseAssignment(data.ToArray(), slope, iterations);
        }

        override public void add_sample_data()
        {
            SampleModifyData d = new SampleModifyData();
            d.smoothnes = 1f;
            d.index = data.Count;
            data.Add(d);
        }
    }
}