﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ReplaceMeshMaterialAction : EventAction
    {
        public Material material;
        public string mesh_name;

        public ReplaceMeshMaterialAction()
            : base()
        {
            name = "replace mesh material";
        }

        override public void Action(QuickTerrain qt)
        {
            if (material == null)
                Debug.LogError("material is not set in '" + name + "' action");
            qt.ReplaceMeshMaterial(material, mesh_name);
        }
    }

}
