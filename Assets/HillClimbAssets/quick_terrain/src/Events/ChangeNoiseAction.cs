﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ChangeNoiseAction : EventAction
    {
        public NoiseList noise;
        public int iterations = 3;

        public ChangeNoiseAction()
            : base()
        {
            name = "replace noise";
        }

        override public void Action(QuickTerrain qt)
        {
            if (noise == null)
                Debug.LogError("noise is not set in '" + name + "' action");
            qt.ReplaceNoise(noise, iterations);
        }

    }
}