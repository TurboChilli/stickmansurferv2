﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class ChangeMeshStructureAction : EventAction
    {
        public MeshStructure mesh_setup;

        public ChangeMeshStructureAction()
            : base()
        {
            name = "replace mesh setup";
        }

        override public void Action(QuickTerrain qt)
        {
            if (mesh_setup == null)
                Debug.LogError("mesh setup is not set in '" + name + "' action");
            qt.ReplaceMeshSetup(mesh_setup);
        }
    }
}