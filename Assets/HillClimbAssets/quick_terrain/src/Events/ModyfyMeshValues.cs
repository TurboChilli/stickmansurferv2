﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ModyfyMeshValues : EventAction
    {
        public string mesh_name;
        public string field_name;
        public string value;


        override public void Action(QuickTerrain qt)
        {
        }
    }

}
