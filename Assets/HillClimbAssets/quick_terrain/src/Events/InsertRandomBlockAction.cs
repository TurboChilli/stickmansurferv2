﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class InsertRandomBlockAction : EventAction
    {
        public CustomBlockSet blocks;

        public InsertRandomBlockAction()
            : base()
        {
            name = "insert random block";
            blocks = new CustomBlockSet();
        }

        override public void Action(QuickTerrain qt)
        {
            if (blocks.blocks.Length == 0)
                Debug.LogError("cusotm blocks are not set in '" + name + "' action");
            qt.InsertRandomBlock(blocks);
        }
    }
}