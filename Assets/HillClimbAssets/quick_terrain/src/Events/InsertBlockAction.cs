﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class InsertBlockAction : EventAction
    {
        public CustomBlock block;
        public float state;
        public bool random_state;

        public InsertBlockAction()
            : base()
        {
            name = "insert block";
            state = 0f;
            random_state = true;

        }

        override public void Action(QuickTerrain qt)
        {
            if (block == null)
                Debug.LogError("cusotm block is not set in '" + name + "' action");

            block.state_slider = random_state ?
                NoiseSample.normalized_value(qt.index, qt.master_seed.value + 1) :
                state;

            qt.InsertCustomBlock(block);
        }
    }
}