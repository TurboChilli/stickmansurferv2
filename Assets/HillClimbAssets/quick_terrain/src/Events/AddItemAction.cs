﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class AddItemAction : EventAction
    {
        public ItemGenerator item;

        public AddItemAction()
            : base()
        {
            name = "add item";
        }

        override public void Action(QuickTerrain qt)
        {
            if (item == null)
                Debug.LogError("item is not set in '" + name + "' action");
            qt.AddItem(item.itemset);
        }
    }
}