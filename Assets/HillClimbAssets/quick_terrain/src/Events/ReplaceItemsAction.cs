﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ReplaceItemsAction : EventAction
    {
        public List<ItemGenerator> items;

        public ReplaceItemsAction()
            : base()
        {
            name = "replace item list";
            items = new List<ItemGenerator>();
        }

        override public void Action(QuickTerrain qt)
        {
            if (items == null)
                Debug.LogError("items are not set in '" + name + "' action");
            qt.ReplaceItems(items);
        }
    }
}
