﻿using UnityEngine;
using System.Collections;


namespace QuickTerrain2D
{
    [System.Serializable]
    public class RemoveItemAction : EventAction
    {
        public ItemGenerator item;

        public RemoveItemAction()
            : base()
        {
            name = "remove item";
        }

        override public void Action(QuickTerrain qt)
        {
            if (item == null)
                Debug.LogError("item is not set in '" + name + "' action");
            qt.RemoveItem(item.itemset.name);
        }
    }
}