﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class IndexEvent : ISerializationCallbackReceiver
    {
        public enum EType { single, repeat, range, ray }
        public bool fold = true;
        public List<EventAction> actions;
        public EType type = EType.single; 
        public int start = 1;
        public int end = 0;
        public int step = 1;
        [Range (0f,1f)]
        public float probability = 1f;
        public Seed seed;
        public string name;

        public ActionSerializationData serialization_data;

        public IndexEvent()
        {
            name = "unnamed";
            seed = new Seed();
            actions = new List<EventAction>();
            serialization_data = new ActionSerializationData();
        }


        public void Check(int _index, QuickTerrain qt)
        {
            switch (type)
            {
                case EType.single:
                    if (single_predicate(_index))
                    {
                        do_stuff(qt);
                    }
                    break;
                case EType.repeat:
                    if (repeat_predicate(_index))
                    {
                        seed.initialize(qt.master_seed.value);
                        if (NoiseSample.normalized_value((float)_index, seed.value) < probability)
                            do_stuff(qt);
                    }
                    break;
                case EType.range:
                    if (range_predicate(_index))
                    {
                        seed.initialize(qt.master_seed.value);
                        if (NoiseSample.normalized_value((float)_index, seed.value) < probability)
                            do_stuff(qt);
                    }
                    break;
                case EType.ray:
                    if (ray_predicate(_index))
                    {
                        seed.initialize(qt.master_seed.value);
                        if (NoiseSample.normalized_value((float)_index, seed.value) < probability)
                            do_stuff(qt);
                    }
                    break;
            }
        }


        bool single_predicate(int i)
        {
            return start == i;
        }

        bool repeat_predicate(int i)
        {
            return i % step == 0;
        }

        bool range_predicate(int i)
        {
            return (step > 0 && i >= start && i <= end || 
                    step < 0 && i <= start && i >= end) && 
                    (i - start) % step == 0;
        }

        bool range_wasted(int i)
        {
            return step > 0 && (i >= end || i <= start) || 
                   step < 0 && (i <= end || i >= start);
        }

        bool ray_predicate(int i)
        {
            return (step > 0 && i >= start || 
                    step < 0 && i <= start) && 
                    (i - start) % step == 0;
        }

        void do_stuff(QuickTerrain qt)
        {
            foreach (EventAction a in actions)
                a.Action(qt);
        }

        public string get_label()
        {
            string s = "_";
            switch (type)
            {
                case EType.single:
                    s = start.ToString();
                    break;
                case EType.repeat:
                    s = ": " + step.ToString();
                    break;
                case EType.range:
                    s = start.ToString() + " -> " + end.ToString() + " : " + step.ToString();
                    break;
                case EType.ray:
                    s = start.ToString() + " -> : " + step.ToString();
                    break;
            }
            return s;
        }


        public void OnAfterDeserialize()
        {
            actions = serialization_data.get_data();
        }
        public void OnBeforeSerialize()
        {
            serialization_data.prepare_to_serialization(actions);
        }
    }
}