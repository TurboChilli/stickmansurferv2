﻿using UnityEngine;
using System.Collections;

namespace QuickTerrain2D
{

    public interface ICustomGenerator { 
        /// <summary>
        /// subscribe to QuickTerrain events here, so you can see generation modicications in the editor mode
        /// </summary>
        /// <param name="quick_terrain">Generation object</param>
        void InitiateGeneration(QuickTerrain quick_terrain);
    }
    public interface IItemGenerationHandler { 
        /// <summary>
        /// This metod is colled when item is generated.
        /// </summary>
        /// <param name="e">useful data about item and generatinon</param>
        void OnItemGenerated(ItemGenerationData e); 
    }

    public class ItemGenerationData
    {
        /// <summary>
        /// QuickTerrain in which generated the item.
        /// </summary>
        public QuickTerrain sender;  
        
        /// <summary>
        /// ItemSet in which item was generated
        /// </summary>
        public ItemSet set;    
       
        /// <summary>
        /// The item
        /// </summary>
        public GameObject item;

        /// <summary>
        /// Block in which item was added
        /// </summary>
        public Block block;

        /// <summary>
        /// Index number of an item
        /// </summary>
        public int index;

        /// <summary>
        /// Index number of an item in the masking group.
        /// </summary>
        public int group_index;
    }
}
