﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class ItemSet
    {
        public ItemInfo[] items;
        public Seed seed;
        [Range(0, 1)]
        public float probability;
        public float distance;
        public float scatter;
        public string name = "set";
        public bool align_to_normal = false;
        public bool block_as_parent = true;
        public string[] tags;

        public bool max_x_constraint;
        public float max_x = 0f;
        public bool min_x_constraint;
        public float min_x = 0f;


        public bool masking;
        public Vector3 white_bounds;
        public Vector3 black_bounds;

        public bool do_offset;
        public Vector2 offset_x;
        public Vector2 offset_y;
        public Vector2 offset_z;

        public bool do_scale;
        public Vector2 scale = new Vector2(1f, 1f);

        public bool do_rotation;
        public Vector2 rotation;

        float eXpand_right = float.MinValue / 2f;
        float eXpand_left = float.MaxValue / 2f;

        Vector2 white_zone;
        int side = 0;
        int group_index = 0;
        int index = 0;

        float group_counter = 0;
        int tw;
        int[] weights;


        public bool wasted { get; private set; }


        public ItemSet clone
        {
            get
            {
                ItemSet set = new ItemSet();
                set.items = new ItemInfo[items.Length];
                for (int i = 0; i < items.Length; i++)
                    set.items[i] = items[i].clone;
                set.seed = seed;
                set.probability = probability;
                set.distance = distance;
                set.scatter = scatter;
                set.name = name;
                set.align_to_normal = align_to_normal;
                set.block_as_parent = block_as_parent;
                set.tags = tags;
                set.masking = masking;
                set.white_bounds = white_bounds;
                set.black_bounds = black_bounds;
                set.max_x_constraint = max_x_constraint;
                set.min_x_constraint = min_x_constraint;
                set.max_x = max_x;
                set.min_x = min_x;
                set.do_offset = do_offset;
                set.offset_x = offset_x;
                set.offset_y = offset_y;
                set.offset_z = offset_z;
                set.do_scale = do_scale;
                set.scale = scale;
                set.do_rotation = do_rotation;
                set.rotation = rotation;
                return set;
            }
        }

        public ItemSet()
        {
            items = new ItemInfo[1];
            distance = 1f;
            probability = .5f;
        }

        public ItemInfo get_item(float x)
        {
            int _seed = seed.value + 1;
            float r = NoiseSample.normalized_value(x, _seed) * (float)tw;

            for (int i = 0; i < weights.Length; i++)
                if (weights[i] >= r)
                    return items[i];
            return items[items.Length - 1];
        }

        public ItemInstantiationData generate(float x, float x0, float x1, Block block, QuickTerrain qt)
        {

            float cx = x + scatter * NoiseSample.interpolated_uniform_value(x, seed.value + 1, 1f);
            if (cx < x0 || cx > x1)
                cx = x;

            if (should_be_generated(cx))
            {
                ItemInfo item = get_item(x);
                ItemInstantiationData data = new ItemInstantiationData();

                data.item = item.prefab;
                data.block = block;
                data.do_offset = do_offset;
                data.do_rotation = do_rotation;
                data.do_scale = do_scale;
                data.align_to_normal = align_to_normal;
                data.placement = (cx - x0) / block.width;
                data.block_as_parrent = block_as_parent;


                if(do_offset)
                {
                    float r = NoiseSample.normalized_value(x, seed.value + 2);
                    data.offset = new Vector3(
                        offset_x.x + (offset_x.y - offset_x.x) * r,
                        offset_y.x + (offset_y.y - offset_y.x) * r,
                        offset_z.x + (offset_z.y - offset_z.x) * r);
                }

                if (do_scale)
                {
                    float r = NoiseSample.normalized_value(x, seed.value + 3);
                    data.scale = scale.x + (scale.y - scale.x) * r;
                }
                if (do_rotation)
                {
                    float r = NoiseSample.normalized_value(x, seed.value + 3);
                    data.rotation = rotation.x + (rotation.y - rotation.x) * r;
                }

                group_counter++;
                if (cx > eXpand_right) eXpand_right = cx;
                else if (cx < eXpand_left) eXpand_left = cx;

                index += side;
                if (masking)
                    group_index += side;

                data.index = index;
                data.group_index = group_index;
                data.set = this;
        
                return data;
            }
            return null;
        }


        public bool should_be_generated(float x)
        {
            if (!in_range(x))
                return false;

            if (masking && !in_white_zone(x))
            {
                if (x > 0)
                {
                    while (x > white_zone.y)
                    {
                        white_zone.x = white_zone.y + black_bounds.x + (black_bounds.y - black_bounds.x) * NoiseSample.normalized_value(white_zone.y, seed.value + 1);
                        white_zone.y = white_zone.x + white_bounds.x + (white_bounds.y - white_bounds.x) * NoiseSample.normalized_value(white_zone.x, seed.value + 2);
                    }
                }
                else if (x < 0)
                {
                    while (x < white_zone.x)
                    {
                        white_zone.y = white_zone.x - (black_bounds.x + (black_bounds.y - black_bounds.x) * NoiseSample.normalized_value(white_zone.x, seed.value + 1));
                        white_zone.x = white_zone.y - (white_bounds.x + (white_bounds.y - white_bounds.x) * NoiseSample.normalized_value(white_zone.y, seed.value + 2));
                    }
                }
                group_index = 0;
                return false;
            }

            return (NoiseSample.interpolated_uniform_value(x, seed.value, 1f) + 1) / 2f <= probability;
        }

        public bool BlockToFilter(GameObject obj)
        {
            if (tags == null)
                return false;


            bool succes = false;
            foreach (string tag in tags)
            {
                succes |= obj.CompareTag(tag);
                if (succes)
                    break;
            }
            return succes;
        }


        bool in_range(float x)
        {
            return (!min_x_constraint || x >= min_x)
                && (!max_x_constraint || x <= max_x);
        }

        public bool in_white_zone(float x)
        {
            return x >= white_zone.x && x <= white_zone.y;
        }

        public void init(int master_seed, int _side)
        {
            wasted = false;

            side = _side >= 0 ? 1 : -1;
            seed.initialize(master_seed);

            white_zone.x = (black_bounds.y - black_bounds.x) * NoiseSample.normalized_value(0f, seed.value + 1);
            white_zone.y = white_zone.x + white_bounds.x + (white_bounds.y - white_bounds.x) * NoiseSample.normalized_value(white_zone.x, seed.value + 2);

            eXpand_right = float.MinValue;
            eXpand_left = float.MaxValue;

            int i = 0;
            ItemInfo t;
            for (i = 0; i < items.Length - 1; i++)
                for (int j = i + 1; j < items.Length; j++)
                {
                    if (items[i].weight > items[j].weight)
                    {
                        t = items[i];
                        items[i] = items[j];
                        items[j] = t;
                    }
                }

            tw = 0;
            weights = new int[items.Length];
            i = 0;
            foreach (ItemInfo item in items)
            {
                tw += item.weight;
                weights[i] = tw;
                i++;
            }
        }

        public void skip(float x)
        {
            if (x > eXpand_right) eXpand_right = x;
            else if (x < eXpand_left) eXpand_left = x;
        }

        public void prepare_to_remove()
        {
            wasted = true;
        }
    }



    [System.Serializable]
    public class ItemInfo
    {
        public GameObject prefab;
        public int weight = 1;

        public ItemInfo() { }

        public ItemInfo clone
        {
            get
            {
                ItemInfo item = new ItemInfo();
                item.weight = weight;
                item.prefab = prefab;
                return item;
            }
        }
    }

    public class ItemInstantiationData
    {
        public GameObject item;
        public Block block;
        public ItemSet set;
        public float placement, scale, rotation;
        public bool do_offset, do_scale, do_rotation, align_to_normal, block_as_parrent;
        public Vector3 offset;
        public int index, group_index;


        public ItemInstantiationData() {}
    }

}
