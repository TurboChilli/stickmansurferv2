﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    public class Block : MonoBehaviour
    {
        [HideInInspector]
        public Vector3[] points;
        [HideInInspector]
        public float left_handle;
        [HideInInspector]
        public float right_handle;
        [HideInInspector]
        public List<GeneratedMesh> meshes;
        public int index = 0;

        public Block left;
        public Block right;

        [HideInInspector]
        public Noise noise;
        [HideInInspector]
        public Noise secondary_noise;

        [HideInInspector]
        public bool custom = false;


        public float width { get { return points[points.Length - 1].x - points[0].x; } }
        [HideInInspector]
        public float left_height;
        [HideInInspector]
        public float right_height;

        public bool height_overridden { get { return left_height != right_height && custom; } }

        public GeneratedMesh get_mesh(string mesh_name)
        {
            foreach (GeneratedMesh gm in meshes)
                if (gm.name == mesh_name)
                    return gm;
            return null;
        }

        public float get_handle(bool right)
        {
            return right ? right_handle : left_handle;
        }

        public float get_height(bool right)
        {
            return right ? right_height : left_height;
        }

        public float right_seam(string mesh_name)
        {
            GeneratedMesh mesh = get_mesh(mesh_name);
            return mesh != null ? mesh.right_seam : 0f;
        }

        public float left_seam(string mesh_name)
        {
            GeneratedMesh mesh = get_mesh(mesh_name);
            return mesh != null ? mesh.left_seam : 0f;
        }

        public void place_object(float placement, GameObject obj, bool align_to_normal = false)
        {
            if (placement < 0 || placement > 1)
            {
                Debug.LogError("placement cant be less than 0 or larger than 1. placement = " + placement);
                return;
            }
            obj.transform.parent = transform;
            bool blended = secondary_noise != null;
            float x = width * placement;
            float ox = x + left_handle;
            float y;
            if (custom) 
            {
                if(placement <= .0001)
                {    
                    y = points[0].y;
                    if (align_to_normal)
                    {
                        Vector3 v = points[1] - points[0];
                        obj.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg);
                    }
                }
                else if(placement >= .9999)
                {
                    y = points[points.Length - 1].y;
                    if (align_to_normal)
                    {
                        Vector3 v = points[points.Length - 1] - points[points.Length - 2];
                        obj.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg);
                    }
                }
                else{
                    int i = (int)((points.Length - 1) * placement);
                    while (x < points[i].x || x > points[i + 1].x)
                        if (x < points[i].x) i--;
                        else i++;
                    y = Mathf.Lerp(points[i].y, points[i + 1].y, (x - points[i].x) / (points[i + 1].x - points[i].x));
                    if (align_to_normal)
                    {
                        Vector3 v = points[i + 1] - points[i];
                        obj.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg);
                    }
                }
            }
            else
            {
                if (index < 0) placement = 1 - placement;
                y = blended ? secondary_noise.value_at(ox, placement, noise) : noise.value_at(ox);
                if (align_to_normal)
                {
                    float angle = blended ? secondary_noise.angle_at(ox, placement, noise, index > 0 ? width : -width) : noise.angle_at(ox);
                    obj.transform.eulerAngles = new Vector3(0f, 0f, angle);
                }
            }
            obj.transform.localPosition = new Vector3(x, y, 0f);
        }
    }
}