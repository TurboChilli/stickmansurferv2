﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    [System.Serializable]
    public class Noise 
    {

        public float slope = 0f;
        public Vector2 slope_offset;
        public NoiseSample[] samples;


        public Noise()
        {
            slope_offset = new Vector2();
        }

        public float value_at(float x)
        {
            float y = 0f;
            foreach (NoiseSample n in samples)
                y += n.evaluate(x) + n.amplitude;
            y += (x + slope_offset.x) * slope + slope_offset.y;
            return y;
        }

        public float value_at(float x, float t, Noise second)
        {
            t = (1 - Mathf.Cos(t * Mathf.PI)) * .5f;
            float v = value_at(x);
            return v + (second.value_at(x) - v) * t;
        }

        public float angle_at(float x, float t, Noise second, float width, float accuracy = .01f)
        {
            float lx = x + accuracy;
            float rx = x - accuracy;
            float dt = accuracy / width;
            Vector2 n = new Vector2(lx, value_at(lx, t + dt, second)) - new Vector2(rx, value_at(rx, t - dt, second));
            return Mathf.Atan2(n.y, n.x) * Mathf.Rad2Deg;
        }

        public float angle_at(float x, float accuracy = .01f)
        {
            float lx = x + accuracy;
            float rx = x - accuracy;
            Vector2 n = new Vector2(lx, value_at(lx)) - new Vector2(rx, value_at(rx));
            return Mathf.Atan2(n.y, n.x) * Mathf.Rad2Deg;
        }

        public Noise clone
        {
            get
            {
                Noise c = new Noise();
                c.samples = new NoiseSample[samples.Length];
                c.slope = slope;
                c.slope_offset = slope_offset;
                for (int i = 0; i < samples.Length; i++)
                    c.samples[i] = samples[i].clone;
                return c;
            }
        }

        public void add_sample(NoiseSample s)
        {
            if (samples == null)
            {
                samples = new NoiseSample[1];
                samples[0] = s;
                return;
            }
            NoiseSample[] new_array = new NoiseSample[samples.Length + 1];
            for (int i = 0; i < samples.Length; i++)
                new_array[i] = samples[i];
            new_array[new_array.Length - 1] = s;
            samples = new_array;
        }

        public void remove_item(int index)
        {
            int count = samples == null ? 0 : samples.Length;
            if (count == 0)
                return;
            NoiseSample[] new_array = new NoiseSample[count - 1];
            for (int i = 0, j = 0; i < new_array.Length; i++, j++)
            {
                if (j == index) j++;
                new_array[i] = samples[j];
            }
            samples = new_array;
        }

        public void init(int master_seed)
        {
            foreach (NoiseSample n in samples)
                n.seed.initialize(master_seed);
        }

        public static float value_at(float x, List<NoiseSample> samples, float slope){
            float y = 0f;
            foreach (NoiseSample n in samples)
                y += n.evaluate(x) + n.amplitude;
            y += x * slope;
            return y;
        }

        public static Vector2 calculate_slope_offset(float handle, float old_slope, Vector2 old_offset)
        {
            return new Vector2(handle, (handle + old_offset.x) * old_slope + old_offset.y);
        }
    }


}
