﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace QuickTerrain2D
{
    public class CustomBlock : MonoBehaviour
    {
        public enum ControlPointMode
        {
            broken,
            aligned,
            mirrored
        }


        [SerializeField]
        Vector3[] vs;
        [SerializeField]
        Vector3[] vs0;
        [SerializeField]
        Vector3[] vs1;
        [SerializeField]
        ControlPointMode[] modes;

        public int segments = 20;
        public int point_count { get { return vs.Length; } }
        public int arcs { get { return (vs.Length - 1) / 3; } }
        public enum InsertType { insert, addition, blend }
        public enum InsertLayer { both, basic_only, top_only }
        public InsertType type = InsertType.insert;

        public bool override_height = true;
        public InsertLayer insert_layer;
        public bool overrides_top { get { return type == InsertType.insert || insert_layer != InsertLayer.basic_only; } }
        public bool overrides_basic { get { return type == InsertType.insert || insert_layer != InsertLayer.top_only; } }

        public AnimationCurve blend_curve;

        public bool use_states = false;
        [Range(0, 1)]
        public float state_slider;
        
        public GameObject[] objects;

        bool[] states = new bool[2];

        public Vector3 evaluate_global(float t)
        {
            int i;
            if (t >= 1f)
            {
                t = 1f;
                i = vs.Length - 4;
            }
            else
            {
                t = Mathf.Clamp01(t) * arcs;
                i = (int)t;
                t -= i;
                i *= 3;
            }
            return transform.TransformPoint(bezier(vs[i], vs[i + 1], vs[i + 2], vs[i + 3], t));
        }

        public Vector3 evaluate(float t)
        {
            int i;
            if (t >= 1f)
            {
                t = 1f;
                i = vs.Length - 4;
            }
            else
            {
                t = Mathf.Clamp01(t) * arcs;
                i = (int)t;
                t -= i;
                i *= 3;
            }
            return bezier(vs[i], vs[i + 1], vs[i + 2], vs[i + 3], t);
        }

        Vector3 bezier(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            t = Mathf.Clamp01(t);
            float w = 1f - t;
            float ww = w * w;
            float tt = t * t;
            return ww * w * p0 + 3f * ww * t * p1 + 3f * w * tt * p2 + tt * t * p3;
        }

        public void add_arc()
        {
            Vector3 p = vs[vs.Length - 1];
            System.Array.Resize(ref vs, vs.Length + 3);
            p.x += 5f;
            vs[vs.Length - 3] = p;
            p.x += 10f;
            vs[vs.Length - 2] = p;
            p.x += 5f;
            vs[vs.Length - 1] = p;


            System.Array.Resize(ref modes, modes.Length + 1);
            modes[modes.Length - 1] = ControlPointMode.aligned;
            apply_mode(vs.Length - 4);
            vs0 = vs1 = null;
            states[0] = states[1] = false;
        }

        public void set_point(int i, Vector3 point)
        {
            if (i % 3 == 0)
            {
                if (i == 0)
                    point.x = 0;

                Vector3 delta = point - vs[i];
                if (i > 0)
                    vs[i - 1] += delta;
                if (i + 1 < vs.Length)
                    vs[i + 1] += delta;
            }
            
            vs[i] = point;


            apply_mode(i);
        }

        public Vector3 get_point(int i)
        {
            return vs[i];
        }


        public ControlPointMode get_point_mode(int index)
        {
            return modes[(index + 1) / 3];
        }

        public void set_point_mode(int i, ControlPointMode mode)
        {
            modes[(i + 1) / 3] = mode;
            apply_mode(i);
        }

        void apply_mode(int i)
        {
            int modeIndex = (i + 1) / 3;

            ControlPointMode mode = modes[modeIndex];
            if (mode == ControlPointMode.broken || modeIndex == 0 || modeIndex == modes.Length - 1)
            {
                return;
            }

            int control = modeIndex * 3;
            int fixedIndex, enforcedIndex;
            if (i <= control)
            {
                fixedIndex = control - 1;
                enforcedIndex = control + 1;
            }
            else
            {
                fixedIndex = control + 1;
                enforcedIndex = control - 1;
            }

            Vector3 middle = vs[control];
            Vector3 enforcedTangent = middle - vs[fixedIndex];

            if (enforcedIndex > 0 && enforcedIndex < vs.Length)
            {
                if (mode == ControlPointMode.aligned)
                {
                    enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, vs[enforcedIndex]);
                }
                vs[enforcedIndex] = middle + enforcedTangent;
            }
        }

        public void remove_arc()
        {
            if (vs.Length == 4) return;
            System.Array.Resize(ref vs, vs.Length - 3);
            System.Array.Resize(ref modes, modes.Length - 1);
            vs0 = vs1 = null;
            states[0] = states[1] = false;
        }

        void Reset()
        {
            vs = new Vector3[] {
			    new Vector3(0f, 0f, 0f),
			    new Vector3(5f, 0f, 0f),
			    new Vector3(15f, 0f, 0f),
			    new Vector3(20f, 0f, 0f) };
            
            modes = new ControlPointMode[] {
                ControlPointMode.aligned,
                ControlPointMode.aligned };

            segments = 30;
            vs0 = null;
            vs1 = null;
        }


        public void fix_state(int state)
        {
            state_slider = state;
            Vector3[] fix = new Vector3[vs.Length];

            for (int i = 0; i < vs.Length; i++)
                fix[i] = vs[i];

            if (state == 0)
            {
                vs0 = fix;
                states[0] = true;
            }
            else if (state == 1)
            {
                vs1 = fix;
                states[1] = true;
            }
        }

        public void interpolate_state(float t)
        {
            if (!has_both_states())
                return;
            if (t < 0 || t > 1)
                return;

            for (int i = 0; i < vs.Length; i++)
                vs[i] = vs0[i] + (vs1[i] - vs0[i]) * t;
        }


        public Vector3[] interpolated_vartices(float t)
        {
            if (!has_both_states())
                return null;
            if (t < 0 || t > 1)
                return null;

            Vector3[] ps = new Vector3[vs.Length];

            for (int i = 0; i < vs.Length; i++)
                ps[i] = vs0[i] + (vs1[i] - vs0[i]) * t;

            return ps;
        }

        public bool has_both_states()
        {
            return states[0] && states[1];
        }



        Vector3[] calculate_verices_global()
        {
            Vector3[] ps = new Vector3[segments * arcs + 1];
            for (int i = 0; i < ps.Length; i++)
                ps[i] = evaluate_global((i) / ((float)ps.Length - 1));
            return ps;
        }

        public Vector3[] calculate_vertices()
        {
            Vector3[] ps = new Vector3[segments * arcs + 1];
            for (int i = 0; i < ps.Length; i++)
                ps[i] = evaluate((i) / ((float)ps.Length - 1));
            return ps;
        }


        void OnDrawGizmos()
        {
            Vector3[] ps = calculate_verices_global();
            if (ps == null)
                return;
            float width = ps[ps.Length - 1].x - ps[0].x;
            float l = .5f;
            float x0 = 0f;
            float x1 = l;
            while (true)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(new Vector3(x0, 0f) + transform.position, new Vector3(x1, 0f) + transform.position);
                x0 = x1 + l / 2f;
                x1 = x0 + l;
                if (x1 > width)
                    x1 = width;
                if (x0 >= width)
                    break;
            }

            for (int i = 0; i < ps.Length - 1; i++)
            {
                bool valid = ps[i + 1].x >= ps[i].x;
                float t = (float)(i + 1) / (float)ps.Length;
                Color c = type == InsertType.insert ? Color.yellow : Color.Lerp(Color.green, Color.yellow, blend_curve.Evaluate(t));
                Gizmos.color = valid ? c : Color.red;
                Gizmos.DrawLine(ps[i], ps[i + 1]);
                Gizmos.color = valid ? c : Color.red;
                Gizmos.DrawSphere(ps[i], .05f);
            }
        }

        void OnValidate()
        {
            if (segments < 2) segments = 2;
        }
    }
}
