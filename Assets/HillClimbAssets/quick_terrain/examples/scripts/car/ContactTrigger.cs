﻿using UnityEngine;
using System.Collections;

public class ContactTrigger : MonoBehaviour {

    public string check_tag;
    public bool contact { get { return overlaps > 0; } }
    int overlaps = 0;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(check_tag))
            overlaps++;
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(check_tag))
            overlaps--;
    }
}
