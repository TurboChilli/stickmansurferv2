﻿using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour {

    public float moment_of_inertia_ratio = 1f;
    public Transform center_of_mass;
    Rigidbody2D body;
    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        float default_inertia = body.inertia;
        body.centerOfMass = center_of_mass.localPosition;
        body.inertia = default_inertia * moment_of_inertia_ratio;

    }
}
