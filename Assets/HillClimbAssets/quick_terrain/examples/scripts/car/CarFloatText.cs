﻿using UnityEngine;
using System.Collections;

public class CarFloatText : MonoBehaviour {

    bool floatingText = false;
	// Use this for initialization
	void Start () {
        Reset();
	}
	
    public void Reset()
    {
        floatingText = false;
        gameObject.SetActive(false);
    }
    public void StartFloat(Vector3 startPos)
    {
        Reset();
        transform.position = startPos;
        gameObject.SetActive(true);
        floatingText = true;
    }

    float floatingTimer = 0;
    float floatingDelay = 1;
    float yFloatVelocity = 5f;
	// Update is called once per frame
	void Update () 
    {
        if (floatingText)
        {
            floatingTimer += Time.deltaTime;
            if (floatingTimer > floatingDelay)
            {
                floatingTimer = 0;
                Reset();
            }
            transform.position = new Vector3(transform.position.x, transform.position.y + yFloatVelocity * Time.deltaTime, transform.position.z);
        }

	}
}
