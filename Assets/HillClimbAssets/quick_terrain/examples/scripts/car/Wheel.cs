﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wheel : MonoBehaviour {

    public AudioSource engineSound;
    public bool drive = true;
    public Rigidbody2D car;
    public float frequency;
    public ParticleSystem particlesz;
    [Range(0f,1f)]


    public float trigger_additional_radius = .04f;

    ContactTrigger waterTrigger;
    ContactTrigger trigger;
    Rigidbody2D body;
    float tor = 0f;
    //private float max_tor = 2000f;
    private float[] torqueLevels = {1500f, 1800f, 2200f, 2400f, 2700f, 3000f};

    private float[] shockDampeningLevels = {0.1f, 0.15f, 0.20f, 0.25f, 0.29f, 0.32f};
    private float[] shockWheelBounceLevels = {1, 0.7f, 0.6f, 0.5f, 0.3f, 0.1f};
    private float[] wheelFrictionLevels = { 1.3f, 1.75f, 2f, 2.5f,2.8f, 3f };
    //private float wheelFrictionlevel = 2;

    // default values
    private float max_tor = 2200f;
    private float damping_ratio = 0.26f;
    private float a = 40f;
    private float friction = 2f;
    private float bounciness = 0f;
    private float maxPitch = 1;
    WheelJoint2D j;

    private Vector3 lastCarPos;

    CircleCollider2D wheelCollider;
    PhysicsMaterial2D wheelPhysics;

	void Start () {
        wheelCollider = GetComponentInChildren<CircleCollider2D>();
       
        lastCarPos = car.transform.position;

        UpgradeItem jeepEngine = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepEngine);
        int engineStage = jeepEngine.CurrentUpgradeStage;

        UpgradeItem jeepWheels = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepWheels);
        int wheelsStage = jeepWheels.CurrentUpgradeStage;

        int globalSetting = 4;

        // wheel grip 0-4
        this.wheelCollider.sharedMaterial.friction = wheelFrictionLevels[wheelsStage];

        // engine power 0-4
        max_tor = torqueLevels[engineStage];

        if (GameState.SharedInstance.CurrentJeep == JeepType.Black)
        {
            //engineSound = jeepEngineSound;
           // trailBikeSound.Stop();
            max_tor += 1000f;
            this.wheelCollider.sharedMaterial.friction = wheelFrictionLevels[wheelsStage] + 0.5f;
        }
        else if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
            //engineSound = trailBikeSound;
            //jeepEngineSound.Stop();
            maxPitch = 1.6f;
            max_tor += 2000f;
            this.wheelCollider.sharedMaterial.friction = wheelFrictionLevels[wheelsStage] + 0.7f;
        }
        else
        {
            //engineSound = jeepEngineSound;
            //trailBikeSound.Stop();
        }
        // shock absorber settings 0-4
        damping_ratio = shockDampeningLevels[globalSetting];
        this.wheelCollider.sharedMaterial.bounciness = 0f;//shockWheelBounceLevels[globalSetting];

        // apply defaults
        //this.wheelCollider.sharedMaterial.friction = friction;
        //this.wheelCollider.sharedMaterial.bounciness = bounciness;
        //damping_ratio = damping_ratio;

        body = GetComponent<Rigidbody2D>();
        create_wheel_joint();
        create_contact_trigger();
        //create_water_trigger();
	}

    void create_wheel_joint()
    {
        j = car.gameObject.AddComponent<WheelJoint2D>();
        j.anchor = car.transform.InverseTransformPoint(transform.position);
        JointSuspension2D js2d = new JointSuspension2D();
        js2d.dampingRatio = damping_ratio;
        js2d.frequency = frequency;
        js2d.angle = 90;
        j.suspension = js2d;
        j.connectedBody = body;
    }
    void create_contact_trigger()
    {
        GameObject obj = new GameObject("contact_trigger");
        CircleCollider2D col = obj.AddComponent<CircleCollider2D>();
        col.radius = GetComponent<CircleCollider2D>().radius + trigger_additional_radius;
        col.isTrigger = true;
        trigger = obj.AddComponent<ContactTrigger>();
        trigger.check_tag = "road";
        obj.transform.position = transform.position;
        obj.transform.parent = transform;
    }

    void create_water_trigger()
    {
        GameObject obj = new GameObject("contact_water_trigger");
        CircleCollider2D col = obj.AddComponent<CircleCollider2D>();
        col.radius = GetComponent<CircleCollider2D>().radius + trigger_additional_radius;
        col.isTrigger = true;
        waterTrigger = obj.AddComponent<ContactTrigger>();
        waterTrigger.check_tag = "JeepWater";
        obj.transform.position = transform.position;
        obj.transform.parent = transform;
    }
	
    Color orangeSand = new Color(0.8f, 0.7f, 0.2f);
    Color blueWater = new Color(0.3f, 0.8f, 1f);
	void Update () {

        float lastFrameDistance = Vector3.Distance(lastCarPos, car.transform.position);
        float currentCarSpeed = (1f / Time.deltaTime) * lastFrameDistance;
        ////////Debug.Log("CAR SPEED:" + currentCarSpeed);
        lastCarPos = car.transform.position;

        if (inWater)
        {
            ////////Debug.Log("IN WATERRRRR");
            particlesz.startColor = blueWater;
            particlesz.startSize = 0.2f;
        }
        else
        {
            particlesz.startSize = 0.1f;
            particlesz.startColor = orangeSand;
        }

        if (!drive)
        {
            if(engineSound != null)
                engineSound.pitch = 0.3f;
            particlesz.Stop();
            return;
        }
            

        int sign = 0;
        if (Input.GetMouseButton(0))
            //sign = Input.mousePosition.x > Screen.width / 2 ? 1 : -1;
            sign = 1;
        else if(Input.GetKey(KeyCode.RightArrow))
            sign = 1;
        else if (Input.GetKey(KeyCode.LeftArrow))
            sign = -1;
        if (sign != 0 && on_road())
        {
            tor -= a * sign;
            if (tor < -max_tor)
                tor = -max_tor;
            else if (tor > max_tor)
                tor = max_tor;
            body.angularVelocity = tor;

            particlesz.Play();

        }
        else
        {
            particlesz.Stop();
            tor = body.angularVelocity;
        }

        if (engineSound != null)
        {
            if (on_road())
            {
                float torqueVariant = Mathf.Abs(tor / max_tor);
                ////////Debug.Log("torque variant: " + torqueVariant);
                if (sign == 0)
                {
                    engineSound.pitch -= Time.deltaTime;
                    engineSound.pitch = Mathf.Max(0.5f, engineSound.pitch);
                    engineSound.volume = Mathf.Max(0.7f, torqueVariant);
                }
                else
                {
                    engineSound.pitch += Time.deltaTime * 3f;
                    engineSound.pitch = Mathf.Min(maxPitch, engineSound.pitch);
                    engineSound.volume = 1f;//Mathf.Max(0.9f, torqueVariant);
                }


            }
            else
            {
                if (sign > 0)
                {
                    

                    engineSound.pitch += Time.deltaTime * 2f;
                    engineSound.pitch = Mathf.Min(maxPitch, engineSound.pitch);
                    engineSound.volume = 1;
                }
                else
                {
                    
                    engineSound.pitch -= Time.deltaTime;
                    engineSound.pitch = Mathf.Max(0.5f, engineSound.pitch);
                    engineSound.volume = 1;
                }


            }
            //engineSound.pitch = Mathf.Min(2f, engineSound.pitch * 1.3f);

        }

        //

	}

    public bool GetIsInWater()
    {
        return inWater;
    }

    bool inWater = false;
    void OnTriggerEnter2D(Collider2D other) 
    { 
        if (other.tag.Equals("JeepWater"))
        {
            ////////Debug.Log("enter" + other.tag);
            inWater = true;
        }


    }

    void OnTriggerExit2D(Collider2D other) 
    { 
        if (other.tag.Equals("JeepWater"))
        {
            ////////Debug.Log("exited" + other.tag);
            inWater = false;
        }


    }
    

    public bool on_road()
    {
        //return onRoad;
        return trigger.contact;
    }

   
}
