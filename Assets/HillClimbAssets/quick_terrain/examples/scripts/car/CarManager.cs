﻿using UnityEngine;
using System.Collections;
using TMPro;
using QuickTerrain2D;

public class CarManager : MonoBehaviour 
{
    private Vector3 startPos;
    private float timeElapsed = 0;
    public GameObject chopper;
    public AudioSource collectCoinSound;
    public AudioSource waterSound;
    public AudioSource flipSound;
    public AudioSource clickSound;
    public AudioSource soundMusic;
    public AudioSource engineSound;
    public AudioSource chopperSound;

    public GameCenterPluginManager gcpm = null;

    public Car theCar;
    public SpriteRenderer theCarRenderer;
	public SpriteRenderer backWheelRenderer;
    public SpriteRenderer frontWheelRenderer;
    public Sprite defaultCarSprite;
    public Sprite defaultWheelSprite;
    public Sprite blackCarSprite;
    public Sprite blackWheelSprite;

    public CoinBar coinBar;
    public TextMesh distanceText;
    public TextMeshPro fuelText;
    public TextMeshPro endText;
    public GameObject distanceMarker;
    public TextMeshPro distanceMarkerText;

    public TextMeshPro jeepTutorialText;

    public GameObject pauseButton;
    public FillBar fuelBar;
    public Wheel rearWheel;
    public Wheel frontWheel;
    public AnimatingObject cosmeticObject;
    public AnimatingObject emotionObject;

    private float distanceTravelled = 0;
    private float prevFrameDistance = 0;
    private Vector3 previousPosition;
    private float deadTimer = -999;
    private float deadDelay = 0.5f;
    private float lockRotation = 0;
    private float carRotationDegrees = 0;

    private bool hasPlacedDistanceMarker = false;

    public GameObject goalObject;
    public GameObject pausePanel;
    public GameObject playButton;
    public GameObject shackButton;

    public TextMeshPro goalDescription;

    public GameObject surfboardWhite;
    public GameObject surfboardGold;
    public GameObject surfboardOrange;

    private Vector3 activeGoalPosition;
    private Vector3 inactiveGoalPosition;
    private float timeToMoveGoal = 0.5f;
    private float timeToWait = 2.0f;
    private float curTimeToMoveGoal;
    private float curTimeToWait;
    private bool activeGoal = false;
    private bool showingGoal = false;

    private Emotion curEmotion = Emotion.Determined;
    private float emotionTimer = 3.0f;
    private float curEmotionTimer;
    private Emotion defaultEmotion = Emotion.Determined;

    public Camera mainCamera;

    public ParticleSystem coinParticles;
    bool flipping = false;
    //private bool flippingCar = false;
	private float distanceToDestination
	{ 
		get  { return Application.isEditor ? 280f : 280f; }
	}
   // private float maxTime = 30;
    private Color darkRe2d = new Color(0.8f, 0f, 0f);
    private bool outOfFuel = false; 
    private float flipYPosTop = 0;
    private Rigidbody2D carRigid;

    private float startFuel = 0;

    public TMP_Text floatTextMeshPro; 
    private CarFloatText floatText;

    public GameObject surfboard;

    public enum RescueState
    {
        ChopperComingToRescue,
        ChopperPickingUp,
        ChopperReleased,
        None
    };

    private float[] fuelStages = {40,60,90,120,150,180};
        
    private RescueState currentRescueState = RescueState.None;
	GameCenterPluginManager gameCenterPluginManager = null;
	int flipCountReward = 100;

	JeepTimer jeepTimer;
    void Awake()
    {
        //GameState.SharedInstance.CurrentJeep = JeepType.QuadBike;
    }
	void Start () 
	{
		jeepTimer = new JeepTimer();
		jeepTimer.InitializeTimer(TimerManager.SharedInstance.GetTimeInSecondsAndIncrementToNextInterval(TimerManager.TimerID.Jeep));
			
		flipCountReward = GameServerSettings.SharedInstance.SurferSettings.JeepFlipCountReward;

		if(gameCenterPluginManager == null)
			gameCenterPluginManager = FindObjectOfType<GameCenterPluginManager>();

		if(gameCenterPluginManager != null)
			gameCenterPluginManager.TrackAndSubmitAchievementFourWheelEnthusiast();

        //GameState.SharedInstance.CurrentJeep = JeepType.QuadBike;

        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
           // do nothing
        }
        else  if (GameState.SharedInstance.CurrentJeep == JeepType.Black)
        {
            theCarRenderer.sprite = blackCarSprite;
            frontWheelRenderer.sprite = blackWheelSprite;
            backWheelRenderer.sprite = blackWheelSprite;
        }
        else
        {
            theCarRenderer.sprite = defaultCarSprite;
            frontWheelRenderer.sprite = defaultWheelSprite;
            backWheelRenderer.sprite = defaultWheelSprite;
        }

        surfboardWhite.SetActive(false);
        surfboardGold.SetActive(false);
        surfboardOrange.SetActive(false);

		endText.gameObject.SetActive(false);

        //GameState.SharedInstance.CurrentVehicle = Vehicle.GoldSurfboard;

        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {

        }
        else if (GameState.SharedInstance.CurrentVehicle == Vehicle.Thruster)
        {
            surfboardWhite.SetActive(true);
        }
        else if (GameState.SharedInstance.CurrentVehicle == Vehicle.GoldSurfboard)
        {
            surfboardGold.SetActive(true);
        }
        else
        {
            surfboardOrange.SetActive(true);
        }

        playLocalScale = playButton.transform.localScale;
		if (gcpm == null)
			gcpm = FindObjectOfType<GameCenterPluginManager>();

        floatText = floatTextMeshPro.GetComponentInChildren<CarFloatText>();
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 3, 0.9f, coinBar.transform, Camera.main);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 1, 1, pauseButton.transform, Camera.main);
        UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_RIGHT, 4.4f, 1, fuelBar.transform, Camera.main);
        //UIHelpers.SetTransformRelativeToCorner(UIHelpers.GUICorner.CORNER_TOP_LEFT, 1, 2.5f, distanceText.transform, Camera.main);

		activeGoalPosition = goalObject.transform.localPosition;
		inactiveGoalPosition = activeGoalPosition + Vector3.up * 10.0f;
		goalObject.transform.localPosition = inactiveGoalPosition;

        UpgradeItem jeepFuel = GameState.SharedInstance.Upgrades.UpgradeItemByType(UpgradeItemType.JeepFuel);
        int fuelStage = jeepFuel.CurrentUpgradeStage;
        startFuel = fuelStages[fuelStage];
		#if UNITY_EDITOR
		//////Debug.Log("SETTING - FuelStage:" + fuelStage + " FuelTimer: " + fuelStages[fuelStage]);
		#endif

        fuelText.gameObject.SetActive(false);
        coinBar.coinText.text = "" + coinsInGame;
        carRigid = theCar.gameObject.GetComponent<Rigidbody2D>();
        //Application.targetFrameRate = 60;
        Time.timeScale = 1f;
        //Application.targetFrameRate = 60;
		endText.gameObject.SetActive(false);
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("Respawn");
        if (spawns != null && spawns.Length > 0)
        {
            if (spawns.Length > 1)
                Debug.LogWarning("There should be only one spawn point. Found points: " + spawns.Length);
            transform.position = spawns[0].transform.position;
        }

		Vector3 localPosition = Vector3.zero;
		Quaternion localRotation = Quaternion.identity;
		Vector3 localScale = Vector3.one;



        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
            emotionObject.transform.localScale = new Vector3(0.6f, 0.6f, 1f);
            cosmeticObject.transform.localScale = new Vector3(0.6f, 0.6f, 1f);

        }

        //Cosmetics woo
        Cosmetic curCosmetic = GameState.SharedInstance.CurrentCosmetic;
        if (curCosmetic != Cosmetic.None)
        {
            cosmeticObject.gameObject.SetActive(true);
            cosmeticObject.DrawFrame((int)curCosmetic);

            CosmeticitemManager.LoadJeepCosmeticItem(curCosmetic,
                ref localPosition,
                ref localRotation,
                ref localScale);

            cosmeticObject.transform.localPosition = localPosition;
            cosmeticObject.transform.localRotation = localRotation;
            if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
            {
               // localScale = new Vector3(0.6f, 0.6f, 1f);
            }
            cosmeticObject.transform.localScale = localScale;
        }
        else
            cosmeticObject.gameObject.SetActive(false);

       
        emotionObject.gameObject.SetActive(true);
        emotionObject.DrawFrame((int)curEmotion);

        localPosition = Vector3.zero;
        localRotation = Quaternion.identity;
        localScale = Vector3.one;

       

        CosmeticitemManager.LoadJeepEmotion(ref localPosition,
            ref localRotation,
            ref localScale);

        emotionObject.transform.localPosition = localPosition;
        emotionObject.transform.localRotation = localRotation;
        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
           // localScale = new Vector3(0.6f, 0.6f, 1f);
        }
        emotionObject.transform.localScale = localScale;

        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
            //emotionObject.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
           // cosmeticObject.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
            cosmeticObject.transform.localPosition = new Vector3(cosmeticObject.transform.localPosition.x + 0.6f, cosmeticObject.transform.localPosition.y + 0.1f, cosmeticObject.transform.localPosition.z);
            emotionObject.transform.localPosition = new Vector3(emotionObject.transform.localPosition.x + 0.6f, emotionObject.transform.localPosition.y + 0.1f, emotionObject.transform.localPosition.z);
        }


        startPos = theCar.transform.position;

        //place the distance marker
		float furthestXPos = startPos.x + GameState.SharedInstance.HighestJeepDistance;
		if (GameState.SharedInstance.HighestJeepDistance < 100.0f)
		{
			hasPlacedDistanceMarker = true;
			distanceMarker.SetActive(false);
		}

        if (GameState.SharedInstance.HighestJeepDistance > 0)
        {
            //jeepTutorialText.gameObject.SetActive(false);
        }
        else
        {
            fuelText.gameObject.SetActive(true);
			fuelText.text = GameLanguage.LocalizedText("HOLD TO ACCELERATE");
            // first time in jeep
            ////////Debug.Log("FIRST TIME PLAYING JEEP GAME!");
        }

		RaycastHit2D rayHit = Physics2D.Raycast( new Vector2(furthestXPos, 1000), Vector2.down, 2000);
		if (rayHit.point != Vector2.zero)
		{
			distanceMarker.transform.position = new Vector3(rayHit.point.x, rayHit.point.y, distanceMarker.transform.position.z);
			hasPlacedDistanceMarker = true;
			distanceMarkerText.text = string.Format("{0}m", Mathf.FloorToInt(GameState.SharedInstance.HighestJeepDistance));
			distanceMarkerText.gameObject.SetActive(true);
        }

        if (GlobalSettings.musicEnabled)
        {
            soundMusic.Play();
        }

        if (!GlobalSettings.soundEnabled)
        {
            engineSound.Stop();
            chopperSound.Stop();
            waterSound.Stop();
        }
        pausePanel.SetActive(false);
	}

	void SetEmotion(Emotion newEmotion, float customTime = 0.0f)
	{
		curEmotion = newEmotion;
		emotionObject.DrawFrame((int)curEmotion);

		if (customTime != 0.0f)
			curEmotionTimer = customTime;
		else
			curEmotionTimer = emotionTimer;
		

	}

    void OnChopperReleased()
    {
		currentRescueState = RescueState.ChopperReleased;

        carRigid.MoveRotation(0);
		#if UNITY_EDITOR
		//////Debug.Log("COMPLETED FLIPPING CAR");
        #endif

        foreach (Rigidbody2D rb in theCar.gameObject.GetComponentsInChildren<Rigidbody2D>())
        {
            rb.isKinematic = false;
            //rb.inertia = 0;
            //rb.freezeRotation = false;
            //rb.inertia = 0;
            // rb.IsAwake = false;
        }

    }

    void OnChopperPickingUp()
    {
        currentRescueState = RescueState.ChopperPickingUp;
        //////Debug.Log("ChopperPickingUp");
        //flippingCar = true;
        flipYPosTop = theCar.transform.position.y + 7f;
        //theCar.transform.Rotate(0, 0, -250f * Time.deltaTime);
        foreach (Rigidbody2D rb in theCar.gameObject.GetComponentsInChildren<Rigidbody2D>())
        {
            rb.isKinematic = true;
            //rb.freezeRotation = true;
           // rb.IsAwake = false;
        }
    }
        
    int coinsInGame = 0;
    float chopperContactY = 0;
    float ropeLength = 5f;
    bool flippping = false;
    float carZPos = 14.3f;
    public void AddCoin(int numberOfCoins = 1)
    {
		GoalManager.SharedInstance.UpdateCoins(numberOfCoins, Vehicle.None);
        coinsInGame += numberOfCoins;
        coinBar.coinText.text = "" + coinsInGame;
        GlobalSettings.PlaySound(collectCoinSound);
    }

    bool fallingAfterReset = false;
    float prevCarRotationDegrees = 0;
    bool clockwiseRotation = false;
    bool gamePaused = false;
    bool lastFlipWasClockwise = false;
    Vector3 playLocalScale = new Vector3(0,0,0);
    //bool chopperComingToRescue = false;
    bool musicWasPlaying = false;
    bool isGameOver = false;

	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(gamePaused == false)
			{
				PauseGame();
			}
			else
			{
                ResumeGame();
			}
		}
		#endif 
	}

    void ResumeGame()
    {
        pausePanel.SetActive(false);
        gamePaused = false;
        GlobalSettings.PlaySound(clickSound);
        if (musicWasPlaying)
        {
            soundMusic.UnPause();
        }
        if (GlobalSettings.soundEnabled)
        {
            engineSound.UnPause();
            chopperSound.UnPause();
            waterSound.UnPause();
        }
        Time.timeScale = 1;
    }

	void PauseGame()
	{
		//playButton.transform.localScale = playLocalScale;
		pausePanel.SetActive(true);
		gamePaused = true;
		GlobalSettings.PlaySound(clickSound);
		if (soundMusic.isPlaying)
		{
			musicWasPlaying = true;
			soundMusic.Pause();
		}
		engineSound.Pause();
		chopperSound.Pause();
		waterSound.Pause();
		Time.timeScale = 0;
	}

    void Update()
    {
		CheckAndroidBackButton();

        if (Input.GetMouseButtonDown(0))
        {
			#if UNITY_EDITOR
				//////Debug.Log("CHECKING BUTTON HIT HERE");
            #endif

            if (UIHelpers.CheckButtonHit(pauseButton.transform, Camera.main))
            {
				PauseGame();
            }
            else if (UIHelpers.CheckButtonHit(playButton.transform, Camera.main))
            {
                //playButton.transform.localScale = playLocalScale * 0.7f;
                ResumeGame();
            }
            else if (UIHelpers.CheckButtonHit(shackButton.transform, Camera.main))
            {
                //pausePanel.SetActive(false);
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
                SceneNavigator.NavigateTo(SceneType.Menu);
                GlobalSettings.PlaySound(clickSound);
                Time.timeScale = 1;
            }
        }

        if (gamePaused)
        {
            return;
        }

        if (curEmotionTimer > 0.0f)
        {
			curEmotionTimer -= Time.deltaTime;
			if (curEmotionTimer <= 0)
			{
				curEmotion = defaultEmotion;
				emotionObject.DrawFrame((int)curEmotion);
			}

        }

    	if (!hasPlacedDistanceMarker)
    	{
			float furthestXPos = startPos.x + GameState.SharedInstance.HighestJeepDistance;
			RaycastHit2D rayHit = Physics2D.Raycast( new Vector2(furthestXPos, 1000), Vector2.down, 2000);
			if (rayHit.point != Vector2.zero)
			{
				distanceMarker.transform.position = new Vector3(rayHit.point.x, rayHit.point.y, distanceMarker.transform.position.z);
				hasPlacedDistanceMarker = true;
				distanceMarkerText.text = string.Format("{0}m", Mathf.FloorToInt(GameState.SharedInstance.HighestJeepDistance));
				distanceMarkerText.gameObject.SetActive(true);
	        }
        }

        /*
        GoalManager.SharedInstance.UpdateGoals();

		if (GoalManager.SharedInstance.completedGoalsWaiting &&
			GoalManager.SharedInstance.completedGoalIndexs.Count != 0 &&
			!activeGoal)
        {
			showingGoal = true;
			activeGoal = true;

        	GoalManager goalManager = GoalManager.SharedInstance;
			int doneGoalIndex = goalManager.completedGoalIndexs[ goalManager.completedGoalIndexs.Count - 1];
			Goal doneGoal = goalManager.goals[goalManager.completedGoalIndexs[doneGoalIndex]];

			goalDescription.text = GoalText.Get(doneGoal);
			curTimeToMoveGoal = timeToMoveGoal;
			curTimeToWait = timeToWait;
        }
        */

        if (activeGoal)
        {
        	if (showingGoal)
        	{
				float lerpValue = curTimeToMoveGoal / timeToMoveGoal;
				curTimeToMoveGoal -= Time.deltaTime;
				if (curTimeToMoveGoal <= 0)
				{
					lerpValue = 0;
					showingGoal = false;
					curTimeToMoveGoal = timeToMoveGoal;
				}
				goalObject.transform.localPosition = Vector3.Lerp(inactiveGoalPosition, activeGoalPosition, 1.0f - lerpValue);
        	}
        	else
        	{
				if (curTimeToWait > 0)
					curTimeToWait -= Time.deltaTime;
				else
				{
					float lerpValue = curTimeToMoveGoal / timeToMoveGoal;
					curTimeToMoveGoal -= Time.deltaTime;
					if (curTimeToMoveGoal <= 0)
					{
						lerpValue = 0;
						showingGoal = false;
					}
					goalObject.transform.localPosition = Vector3.Lerp(inactiveGoalPosition, activeGoalPosition, lerpValue);
				}
        	}

        }

        if (transform.position.z != carZPos)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, carZPos);
        }

        carRotationDegrees = theCar.transform.rotation.eulerAngles.z;

        if (carRotationDegrees > prevCarRotationDegrees)
        {
            clockwiseRotation = false;
        }
        else
        {
            clockwiseRotation = true;
        }

        ////////Debug.Log("ROTATION CLOCKWISE:" + clockwiseRotation);

        prevCarRotationDegrees = carRotationDegrees;
        if (gameOverMode)
        {
            if(!isGameOver)
            {
                isGameOver = true;
                //GameState.SharedInstance.AddJeepGamePlayed();
            }

            UpdateGameOver();
            return;
        }
        if (currentRescueState == RescueState.ChopperReleased)
        {
            chopper.transform.position = new Vector3(chopper.transform.position.x, chopper.transform.position.y + 20f * Time.deltaTime, chopper.transform.position.z);

            if (rearWheel.on_road() || frontWheel.on_road())
            {
                OnRescueVehicleHitGround();
            }
        }
        if (currentRescueState == RescueState.ChopperComingToRescue)
        {
            if (chopper.transform.position.y < chopperContactY)
            {
                //////Debug.Log("Chopper rescue here 1");

                OnChopperPickingUp();
            }
            else
            {
                
                chopper.transform.position = new Vector3(chopper.transform.position.x, chopper.transform.position.y - 10f * Time.deltaTime, chopper.transform.position.z);
                ////////Debug.Log("Chopper rescue here 2 ypos:" + chopper.transform.position.y);
            }
            // OnFlippingCar();
        }

        if (currentRescueState == RescueState.ChopperPickingUp)
        {
            carRigid.MovePosition(new Vector2(theCar.transform.position.x, theCar.transform.position.y + 5f * Time.deltaTime));
            chopper.transform.position = new Vector3(theCar.transform.position.x, theCar.transform.position.y + ropeLength, chopper.transform.position.z);

            ////////Debug.Log("CORRECTING ROTATION:" + carRigid.rotation);
            //theCar.transform.Rotate(0, 0, 250f * Time.deltaTime);
            carRigid.MoveRotation(carRotationDegrees += 150f * Time.deltaTime);
            if (carRotationDegrees < 30)
            {
                carRigid.freezeRotation = true;
                //carRigid.freezeRotation = false;
                OnChopperReleased();    

                lockRotation = carRotationDegrees;
            }

        }
        
        timeElapsed += Time.deltaTime;
        ////////Debug.Log("PREVX: " + previousPosition.x + " CURX:" + theCar.transform.position.x);
        if (previousPosition.x == theCar.transform.position.x)
        {
            if (carRotationDegrees > 90 && carRotationDegrees < 270)
            {
                if (currentRescueState == RescueState.None)
                {
                    deadTimer += Time.deltaTime;
                    if (deadTimer > deadDelay)
                    {
                        // Flip car back over
                        //OnDead();

                        OnChopperComingToRescue();
                    }
                }
            }
        }
        else
        {
            deadTimer = 0;
            if (IsInAir())
            {
                if (carRotationDegrees > 160 && carRotationDegrees < 220)
                {
                    if (!flipping)
                    {
                        OnFlip();
                    }
                }
                else if (carRotationDegrees < 20 || carRotationDegrees > 340)
                {
                    flipping = false;
                }
            }
            else
            {
                flipping = false;
                if (flipCountThisAir > 0)
                {
                    OnFlipLand();
                }

            }
               
        }

        if (outOfFuel)
        {
            if (previousPosition.x > theCar.transform.position.x)
            {
                OnDead();
                //OnOutOfFuel();
            }
        }
        if ((timeElapsed > startFuel) && (currentRescueState == RescueState.None))
        {
            ////////Debug.Log("ERE 1");
            OnOutOfFuel();
        }
        if (theCar.transform.position.x > startPos.x + distanceToDestination)
        {
            //OnWin();

        }
        distanceTravelled = theCar.transform.position.x - startPos.x;
        ////////Debug.Log("Travelled Distance:" + travelledDistance);

        previousPosition = theCar.transform.position;
        UpdateDistance();
        UpdateFuel();

        if (CheckIsInWater())
        {
            if (!waterSound.isPlaying)
            {
                GlobalSettings.PlaySound(waterSound);
            }
        }
        else
        {
            if (waterSound.isPlaying)
            {
                waterSound.Stop();
            }
        }

        expressionTimer += Time.deltaTime;
        if (expressionTimer >= expressionDelay)
        {
            expressionTimer = 0;
            expressionDelay = Random.Range(3, 10);
            SetEmotion(Emotion.Content);
        }

    }

    float expressionTimer = 0;
    float expressionDelay = 6;
           
    bool CheckIsInWater()
    {
        if( frontWheel.GetIsInWater() || rearWheel.GetIsInWater() )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    int flipCountThisAir = 0;
    bool cancelledFlip = false;

    float timeLastFlip = 0;
    float minTimeBetweenFlips = 0.75f;
    void OnFlip()
    {

        timeLastFlip = Time.time;
        flipping = true;


        flipCountThisAir++;

        #if UNITY_EDITOR
        //////Debug.Log("GO FLIPPING!");
        #endif
    }
		
    int flipCountGame = 0;
    void OnFlipLand()
    {
        if (carRotationDegrees < 120 || carRotationDegrees > 240)
        {
            GlobalSettings.PlaySound(flipSound);
            flipCountGame += flipCountThisAir;

            floatTextMeshPro.text = "" + GameLanguage.LocalizedText("FLIP") + "<size=-26>x</size>" + flipCountGame + " <size=-26>\n+" + (flipCountReward * flipCountThisAir) + "</size>";
            floatText.StartFloat(new Vector3(theCar.transform.position.x, theCar.transform.position.y + 1f, floatTextMeshPro.transform.position.z));
            coinParticles.transform.position = new Vector3(theCar.transform.position.x, theCar.transform.position.y + 1f, floatTextMeshPro.transform.position.z);
            coinParticles.Emit(flipCountReward);
            AddCoin(flipCountReward * flipCountThisAir);

            flipCountThisAir = 0;
            flipping = false;

			#if UNITY_EDITOR
            //////Debug.Log("FLIPPED LANDED");
            #endif

			SetEmotion(Emotion.Content);

            flipCountReward += 50;
        }
    }

    void UpdateDistance()
    {
		distanceText.text = string.Format("{0}m",  Mathf.FloorToInt(distanceTravelled));

		if (GameState.SharedInstance.HighestJeepDistance < distanceTravelled)
			GameState.SharedInstance.HighestJeepDistance = distanceTravelled;
    	/*
        float distanceRemaining = distanceToDestination - distanceTravelled;
        distanceRemaining = Mathf.Max(distanceRemaining,0);
        distanceText.text = "BEACH: " + (int)distanceRemaining + "m";
        */ 
    }
    bool redTextMode = false;

    float gameFinishedTimer = 0;
    float gameFinishedDelay = 3;
    bool gameOverMode = false;
    bool playerDidWin = false;

    void UpdateGameOver()
    {
        gameFinishedTimer += Time.deltaTime;
        if (gameFinishedTimer > gameFinishedDelay)
        {
            GameState.SharedInstance.AddCash(coinsInGame);
            gameFinishedTimer = 0;
            GameState.SharedInstance.AddJeepGamesPlayed();
            if(playerDidWin)
				SceneNavigator.NavigateTo(SceneType.Menu);
            else
                SceneNavigator.NavigateTo(SceneType.Menu);
        }
    }

    void OnRescueVehicleHitGround()
    {
        currentRescueState = RescueState.None;
        carRigid.freezeRotation = false;
        flipCountThisAir = 0;
        flipping = false;
    }

    void OnChopperComingToRescue()
    {
		SetEmotion(Emotion.Mad);

        currentRescueState = RescueState.ChopperComingToRescue;
        chopperContactY = theCar.transform.position.y + ropeLength;
        chopper.transform.position = new Vector3(theCar.transform.position.x, chopperContactY + 12f, chopper.transform.position.z);
    }


    bool IsInAir()
    {
        if( (!frontWheel.on_road()) && (!rearWheel.on_road()) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnDead()
    {
        playerDidWin = false;
        gameOverMode = true;
        frontWheel.drive = false;
        rearWheel.drive = false;
		gcpm.SubmitHighScore( Mathf.FloorToInt(distanceTravelled), LeaderboardType.JeepLeaderBoard);

		endText.gameObject.SetActive(true);

		AnalyticsAndCrossPromoManager.SharedInstance.LogJeepGamePlayed( coinsInGame, 0, distanceTravelled);
		AnalyticsAndCrossPromoManager.SharedInstance.NotifyEndJeepGame();
    }


    void OnOutOfFuel()
    {
        fuelText.gameObject.SetActive(true);

		fuelText.text = GameLanguage.LocalizedText("OUT OF FUEL");

        SetEmotion(Emotion.Content);
        frontWheel.drive = false;
        rearWheel.drive = false;
        gameOverMode = true;
		gcpm.SubmitHighScore( Mathf.FloorToInt(distanceTravelled), LeaderboardType.JeepLeaderBoard);

		endText.gameObject.SetActive(true);

		AnalyticsAndCrossPromoManager.SharedInstance.LogJeepGamePlayed( coinsInGame, 0, distanceTravelled);
		AnalyticsAndCrossPromoManager.SharedInstance.NotifyEndJeepGame();
    }


    void UpdateFuel()
    {
        if(playerDidWin)
            return;
        
        float fuelRemaining = startFuel - timeElapsed;
        if ((fuelRemaining < 10f) && (!redTextMode))
        {
			#if UNITY_EDITOR
            //////Debug.Log("RED TEXT HERE");
            #endif

            //fuelText.color = darkRed;
            redTextMode = true;
        }
        fuelRemaining = Mathf.Max(fuelRemaining,0);
        //fuelText.text = "FUEL: " + (int)fuelRemaining + "l"; 

        float percentFuel = 1f - (fuelRemaining / startFuel);
        fuelBar.SetHorizBarAtValue(percentFuel);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            if(gamePaused == false)
            {
                PauseGame();
            }
        }
    }

}
