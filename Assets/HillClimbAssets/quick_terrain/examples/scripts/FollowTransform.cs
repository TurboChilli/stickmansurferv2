﻿using UnityEngine;
using System.Collections;

public class FollowTransform : MonoBehaviour {

    public Transform tracking_transform_jeep;
    public Transform tracking_transform_quad;
    Transform tracking_transform;

    public bool rotation = false;
	
    void Awake()
    {
        if (GameState.SharedInstance.CurrentJeep == JeepType.QuadBike)
        {
            tracking_transform = tracking_transform_quad;

        }
        else
        {
            tracking_transform = tracking_transform_jeep;
        }
    }

	void LateUpdate () {
        Vector3 p = tracking_transform.position;
        p.z = transform.position.z;
        transform.position = p;
        if (rotation)
            transform.eulerAngles = tracking_transform.eulerAngles;
	}
}
