﻿using UnityEngine;
using QuickTerrain2D; // *** do not forget include QuickTerrain2D namespace


//This is an exapmle of how to use custom generation scripts (WIP)
// *** STEP 1 
// implement ICustomGenerator interaface
public class CustomGenerationExample : MonoBehaviour, ICustomGenerator
{

    QuickTerrain quick_terrain;

    public void InitiateGeneration(QuickTerrain _quick_terrain)
    {
        // *** STEP 2 
        // save quick_terrain reference
        quick_terrain = _quick_terrain;


        // *** STEP 3 
        // subscribe to events

        // This event sends before generation of a block starts
        quick_terrain.on_generation += MyPreGenerationHandler;

        // This events sends after generation of block finishes
        quick_terrain.on_generation_over += MyPostGenerationHandler;

        // This events sends after generation of an item finishes
        // This event is recommended to use when generated 
        // gameobjects do not have any custom scripts on them, 
        // othervise use implementation of IItemGenerationHandler instead
        quick_terrain.on_item_generated += MyItemGenerationHandler;


        // *** STEP 4 
        // drag and drop gameobject with this script to 
        // QuickTerrain 'generation sctipts' list.




        //print("Custom generation sctipt initiated");
    }


    void MyPreGenerationHandler(int index)
    {

        //print("After this procedure is over block with index " + index + " will be generated");

        //Usefull functions to use here:  
        //quick_terrain.RemoveItem()
        //quick_terrain.ReplaceNoise()
        //quick_terrain.AddItem()
        //quick_terrain.RemoveItem()
        //quick_terrain.ReplaceItems()
        //quick_terrain.ReplaceMeshMaterial()
        //quick_terrain.InsertCustomBlock()
        //quick_terrain.ReplaceMeshSetup()
        //quick_terrain.ModifyNoiseSampleAdd()
        //quick_terrain.ModifyNoiseSampleApply()
    }


    void MyPostGenerationHandler(Block block)
    {

        //print("Block had generated");
        //print("Block name is " + block.name);
        //print("Block index is " + block.index);
        //print("Block tag is " + block.gameObject.tag);
        //print("Block width is " + block.width.ToString("F"));

        //  block.points - noise vertices that was used to generate block meshses;
    }


    void MyItemGenerationHandler(ItemGenerationData item_generation_data)
    {
        //print("Item just had generated!");
        //print("Item name is " + item_generation_data.item.name);
        //print("Item index is " + item_generation_data.index);
        //item_generation_data.block - block on which item is generated
        //print("Block name is " + item_generation_data.block.name);
    }
}
