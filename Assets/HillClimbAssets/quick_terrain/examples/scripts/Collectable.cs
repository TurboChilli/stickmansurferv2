﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {

    private CarManager theManager;
	private SpriteRenderer renderer;

	private float fade_speed = 30f;
	int jeepCoinValue = 5;

    void Start()
    {
		jeepCoinValue = GameServerSettings.SharedInstance.SurferSettings.JeepCoinValue;

        theManager = FindObjectOfType<CarManager>();
		renderer = GetComponent<SpriteRenderer>();
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponent<Collider2D>().enabled = false;
            StartCoroutine(disapear());

            if (this.CompareTag("Coin"))
            {
				theManager.AddCoin(jeepCoinValue);
            }

        }
    }

    IEnumerator disapear()
    {
        if (renderer != null)
        {
            while (renderer.color.a > 0)
            {
                Color c = renderer.color;
                c.a -= fade_speed * Time.deltaTime;
                renderer.color = c;
                yield return new WaitForEndOfFrame();
            }
        }

        Destroy(gameObject);
    }
}
