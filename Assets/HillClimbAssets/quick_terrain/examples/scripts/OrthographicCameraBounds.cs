﻿using UnityEngine;
using System.Collections;

public class OrthographicCameraBounds : MonoBehaviour
{

    Camera cam;
    public Color32 color = new Color32(249, 134, 97, 255);

    void OnDrawGizmos()
    {
        if (cam == null)
            cam = GetComponent<Camera>();
        float verticalHeightSeen = cam.orthographicSize * 2.0f;
        Gizmos.color = color;
        Gizmos.DrawWireCube(transform.position, new Vector3((verticalHeightSeen * cam.aspect), verticalHeightSeen, 0));
    }
}
