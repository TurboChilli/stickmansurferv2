﻿using System;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class TranslationImporterController : MonoBehaviour 
{
	internal class TranslationTerm
	{
		internal Dictionary<SystemLanguage, string> TranslatedTerms = new Dictionary<SystemLanguage, string>
		{
			{ SystemLanguage.ChineseSimplified, "" },
			{ SystemLanguage.ChineseTraditional, "" },
			{ SystemLanguage.French, "" },
			{ SystemLanguage.German, "" },
			{ SystemLanguage.Indonesian, "" },
			{ SystemLanguage.Italian, "" },
			{ SystemLanguage.Japanese, "" },
			{ SystemLanguage.Korean, "" },
			{ SystemLanguage.Portuguese, "" },
			{ SystemLanguage.Russian, "" },
			{ SystemLanguage.Spanish, "" }
		};

		string englishTerm;
		internal string EnglishTerm
		{
			get { return englishTerm.Replace("\\N", "\\n"); }
			set  { englishTerm = value; }
		}

		internal TranslationTerm(string term)
		{
			EnglishTerm = term
				.Trim().TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
		}

		string TermStringForLanguage(SystemLanguage language, string[] translatedTerms)
		{
			int index = -1;

			if(language == SystemLanguage.English)
				index = 0;
			if(language == SystemLanguage.Spanish)
				index = 1;
			else if(language == SystemLanguage.ChineseSimplified)
				index = 2;
			else if(language == SystemLanguage.ChineseTraditional)
				index = 3;
			else if(language == SystemLanguage.French)
				index = 4;
			else if(language == SystemLanguage.Japanese)
				index = 5;
			else if(language == SystemLanguage.German)
				index = 6;
			else if(language == SystemLanguage.Indonesian)
				index = 7;
			else if(language == SystemLanguage.Portuguese)
				index = 8;
			else if(language == SystemLanguage.Italian)
				index = 9;
			else if(language == SystemLanguage.Korean)
				index = 10;
			else if(language == SystemLanguage.Russian)
				index = 11;

			if(index == -1 || index >= translatedTerms.Length)
				return "";
			if(index == 0)
				return EnglishTerm;
			else
				return translatedTerms[index];
		}

		void SetTranslatedTermForLanguage(SystemLanguage language, string translatedTerm)
		{
			if(TranslatedTerms.ContainsKey(language))
			{
				string term = translatedTerm.Trim().TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });

				if(!string.IsNullOrEmpty(term) && term.Length > 0)
					TranslatedTerms[language] = term;
			}
		}

		internal void SetTranslatedTerm(string[] translatedTerms)
		{
			SetTranslatedTermForLanguage(SystemLanguage.Spanish, TermStringForLanguage(SystemLanguage.Spanish, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.ChineseSimplified, TermStringForLanguage(SystemLanguage.ChineseSimplified, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.ChineseTraditional, TermStringForLanguage(SystemLanguage.ChineseTraditional, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.French, TermStringForLanguage(SystemLanguage.French, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Japanese, TermStringForLanguage(SystemLanguage.Japanese, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.German, TermStringForLanguage(SystemLanguage.German, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Indonesian, TermStringForLanguage(SystemLanguage.Indonesian, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Portuguese, TermStringForLanguage(SystemLanguage.Portuguese, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Italian, TermStringForLanguage(SystemLanguage.Italian, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Korean, TermStringForLanguage(SystemLanguage.Korean, translatedTerms));
			SetTranslatedTermForLanguage(SystemLanguage.Russian, TermStringForLanguage(SystemLanguage.Russian, translatedTerms));
		}

		internal string GetTranslatedTerm(SystemLanguage language)
		{
			if(TranslatedTerms.ContainsKey(language))
			{
				string result = TranslatedTerms[language];

				#if UNITY_EDITOR
				if(string.IsNullOrEmpty(result) || result.Trim().Length == 0)
				{
					Debug.LogErrorFormat("TranslationImporter -> Term '{0}' empty for language '{1}'", EnglishTerm, language);
				}
				#endif

				return result;
			}
			else
			{
				#if UNITY_EDITOR
				Debug.LogErrorFormat("TranslationImporter -> NO LANGUAGE '{0}' FOR TERM '{1}'", language, EnglishTerm);
				#endif

				return "";
			}
		}
	}



	string translationsFolderPath;
	const string translationCSV = "CSV_Translations.tsv";
	const string generatedGameLanguageFileName = "GeneratedLanguageDictionary.cs";
	string generatedCharacterSetFileNameFormat = "_GeneratedCharacterSet{0}.txt";

	List<TranslationTerm> translationTerms = new List<TranslationTerm>();

	void Start()
	{
		translationsFolderPath = Path.Combine(Application.dataPath, "TranslationImporter");
		ImportTranslationTextFromCSV();
	}



	public void ImportTranslationTextFromCSV()
	{	
		// READ CSV AND LOAD INTO DATA STRUCTURE
		string csvFilePath = Path.Combine(translationsFolderPath, translationCSV);

		using (StreamReader sr = new StreamReader(csvFilePath, Encoding.UTF8))
		{
			string csvLine;
			while ((csvLine = sr.ReadLine()) != null)
			{
				if(!string.IsNullOrEmpty(csvLine))
				{
					csvLine = csvLine.Trim();
					string[] separatedTerms = csvLine.Split(new char[] { '|' });

					if(separatedTerms.Length == 12 && separatedTerms[0].Trim().Length > 0)
					{
						string englishTerm = separatedTerms[0]
							.Trim().TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });

						TranslationTerm translationTerm = translationTerms.FirstOrDefault(x => x.EnglishTerm.ToUpperInvariant() == englishTerm.ToUpperInvariant());

						if(translationTerm == null)
						{
							translationTerm = new TranslationTerm(englishTerm);
							translationTerms.Add(translationTerm);
						}

						translationTerm.SetTranslatedTerm(separatedTerms);
					}
				}
			}
		}

		// REMOVE ANY EMPTY TRANSLATION TERMS
		RemoveAnyEmptyTermsAndDuplicateKeys();

		// WRITE TRANSLATION TERMS LIST OBJECT TO FILE
		WriteTranslatedTermsToCodeFormattedFile();


		// WRITE CHARACTER SET OUT TO FILE FOR CREATION OF FONT ATLAS.
		WriteTranslatedTermsToCharacterSetForFontAtlas();
	}

	void RemoveAnyEmptyTermsAndDuplicateKeys()
	{
		if(translationTerms == null || translationTerms.Count == 0)
			return;

		translationTerms.RemoveAll(x => string.IsNullOrEmpty(x.EnglishTerm));
	}
		
	void WriteTranslatedTermsToCodeFormattedFile()
	{
		if(translationTerms == null || translationTerms.Count == 0)
			return;

		StringBuilder sb = new StringBuilder();

		sb.AppendLine("using System;");
		sb.AppendLine("using System.Collections;");
		sb.AppendLine("using System.Collections.Generic;");
		sb.AppendLine("using UnityEngine;");

		sb.Append("\n\n");

		sb.AppendLine("public class GeneratedLanguageDictionary");
		sb.AppendLine("{");
		sb.Append("\n");

		sb.AppendLine("\tpublic static Dictionary<string, Dictionary<SystemLanguage, string>> GeneratedLcalizedTexts = new Dictionary<string, Dictionary<SystemLanguage, string>>(StringComparer.InvariantCultureIgnoreCase)");
		sb.AppendLine("\t{\n");

		foreach(TranslationTerm translationTerm in translationTerms)
		{
			sb.AppendFormat("\t\t{{ \"{0}\", \n", translationTerm.EnglishTerm);
			sb.AppendLine("\t\t\tnew Dictionary<SystemLanguage, string> {");
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Spanish, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Spanish));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.ChineseSimplified, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.ChineseSimplified));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.ChineseTraditional, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.ChineseTraditional));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.French, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.French));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Japanese, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Japanese));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.German, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.German));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Indonesian, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Indonesian));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Portuguese, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Portuguese));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Italian, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Italian));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Korean, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Korean));
			sb.AppendFormat("\t\t\t\t{{ SystemLanguage.Russian, \"{0}\" }}, \n", translationTerm.GetTranslatedTerm(SystemLanguage.Russian));
			sb.AppendLine("\t\t\t}");
			sb.AppendLine("\t\t},");

			sb.AppendLine("");
			sb.AppendLine("");
		}

		sb.AppendLine("\t};\n\n");
		sb.AppendLine("} // end class");
			
		string codeFormattedFile = Path.Combine(translationsFolderPath, generatedGameLanguageFileName);

		WriteTextToFile(codeFormattedFile, sb.ToString());

		Debug.Log("DONE SAVING TRANSLATIONS TO CODE FORMATTED FILE");
	}

	void WriteTranslatedTermsToCharacterSetForFontAtlas()
	{
		// ADD TERMS FROM DEFAULT GameLanguage.cs. REQUIRED FOR FONT ATLAS GENERATION
		Dictionary<string, Dictionary<SystemLanguage, string>> defaultLocalisedTexts = GameLanguage.GetDefaultLocalizedTexts();
		foreach(var defaultLocalisedText in defaultLocalisedTexts)
		{
			string [] separatedTerms = new string[]
			{
				defaultLocalisedText.Key,
				defaultLocalisedText.Value[SystemLanguage.Spanish],
				defaultLocalisedText.Value[SystemLanguage.ChineseSimplified],
				defaultLocalisedText.Value[SystemLanguage.ChineseTraditional],
				defaultLocalisedText.Value[SystemLanguage.French],
				defaultLocalisedText.Value[SystemLanguage.Japanese],
				defaultLocalisedText.Value[SystemLanguage.German],
				defaultLocalisedText.Value[SystemLanguage.Indonesian],
				defaultLocalisedText.Value[SystemLanguage.Portuguese],
				defaultLocalisedText.Value[SystemLanguage.Italian],
				defaultLocalisedText.Value[SystemLanguage.Korean],
				defaultLocalisedText.Value[SystemLanguage.Russian]
			};

			if(separatedTerms.Length == 12 && separatedTerms[0].Trim().Length > 0)
			{
				string englishTerm = separatedTerms[0]
					.Trim().TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });

				TranslationTerm translationTerm = translationTerms.FirstOrDefault(x => x.EnglishTerm.ToUpperInvariant() == englishTerm.ToUpperInvariant());

				if(translationTerm == null)
				{
					translationTerm = new TranslationTerm(englishTerm);
					translationTerms.Add(translationTerm);
				}

				translationTerm.SetTranslatedTerm(separatedTerms);
			}
		}


		// WRITE OUT TO FONT ATLAS
		if(translationTerms == null || translationTerms.Count == 0)
			return;

		string commonCharacterSet = "! \"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
			
		StringBuilder sbEnglishAndCatchAll = new StringBuilder(commonCharacterSet);
		StringBuilder sbChineseSimplified = new StringBuilder(commonCharacterSet);
		StringBuilder sbChineseTraditional = new StringBuilder(commonCharacterSet);
		StringBuilder sbJapanese = new StringBuilder(commonCharacterSet);
		StringBuilder sbKorean = new StringBuilder(commonCharacterSet);
		StringBuilder sbRussian = new StringBuilder(commonCharacterSet);

		foreach(TranslationTerm translationTerm in translationTerms)
		{
			sbChineseSimplified.Append(translationTerm.GetTranslatedTerm(SystemLanguage.ChineseSimplified));
			sbChineseTraditional.Append(translationTerm.GetTranslatedTerm(SystemLanguage.ChineseTraditional));
			sbJapanese.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Japanese));
			sbKorean.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Korean));
			sbRussian.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Russian));

			// For all alphanumeric and accents combine into EnglishCatchAll font atlas
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Spanish));
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.French));
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.German));
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Italian));
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Portuguese));
			sbEnglishAndCatchAll.Append(translationTerm.GetTranslatedTerm(SystemLanguage.Indonesian));
		}

		// WRITE CHARACTER SET TO FILES
		generatedCharacterSetFileNameFormat = Path.Combine(translationsFolderPath, generatedCharacterSetFileNameFormat);

		string chineseSimplifiedFileName = string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.ChineseSimplified));
		string uniqueCatchAllChars = new string(sbChineseSimplified.ToString().Distinct().ToArray());
		WriteTextToFile(chineseSimplifiedFileName, uniqueCatchAllChars);



		string chineseTraditionalFileName = string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.ChineseTraditional));
		uniqueCatchAllChars = new string(sbChineseTraditional.ToString().Distinct().ToArray());
		WriteTextToFile(chineseTraditionalFileName, uniqueCatchAllChars);



		string japaneseFileName = string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.Japanese));
		uniqueCatchAllChars = new string(sbJapanese.ToString().Distinct().ToArray());
		WriteTextToFile(japaneseFileName, uniqueCatchAllChars);



		string koreanFileName = string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.Korean));
		uniqueCatchAllChars = new string(sbKorean.ToString().Distinct().ToArray());
		WriteTextToFile(koreanFileName, uniqueCatchAllChars);



		string russianFileName = string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.Russian));
		uniqueCatchAllChars = new string(sbRussian.ToString().Distinct().ToArray());
		WriteTextToFile(russianFileName, uniqueCatchAllChars);



		string englishAndCatchAllFileName = 
			string.Format(generatedCharacterSetFileNameFormat, Enum.GetName(typeof(SystemLanguage), SystemLanguage.English))
			.Replace("English", "EnglishAndCatchAll");
		
		uniqueCatchAllChars = new string(sbEnglishAndCatchAll.ToString().Distinct().ToArray());
		WriteTextToFile(englishAndCatchAllFileName, uniqueCatchAllChars);
			
		Debug.Log("DONE SAVING TRANSLATIONS TO CHARACTER SET ATLAS FILES");
	}


	void WriteTextToFile(string filePath, string text)
	{
		using (StreamWriter newFile = new StreamWriter(filePath, false, Encoding.UTF8))
		{
			newFile.Write(text);
		}
	}

};
