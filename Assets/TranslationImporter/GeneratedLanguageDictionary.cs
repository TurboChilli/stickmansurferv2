﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GeneratedLanguageDictionary
{

	public static Dictionary<string, Dictionary<SystemLanguage, string>> GeneratedLcalizedTexts = new Dictionary<string, Dictionary<SystemLanguage, string>>(StringComparer.InvariantCultureIgnoreCase)
	{

		{ "GO<size=-26>>></size>", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMENZAR<size=-26>>></size>" }, 
				{ SystemLanguage.ChineseSimplified, "进入游戏<size=-26>>></size>" }, 
				{ SystemLanguage.ChineseTraditional, "開始<size=-26>>></size>" }, 
				{ SystemLanguage.French, "ALLER<size=-26>>></size>" }, 
				{ SystemLanguage.Japanese, "プレイ<size=-26>>></size>" }, 
				{ SystemLanguage.German, "LOS<size=-26>>></size>" }, 
				{ SystemLanguage.Indonesian, "MULAI<size=-26>>></size>" }, 
				{ SystemLanguage.Portuguese, "VÁ!<size=-26>>></size>" }, 
				{ SystemLanguage.Italian, "VIA<size=-26>>></size>" }, 
				{ SystemLanguage.Korean, "시작<size=-26>>></size>" }, 
				{ SystemLanguage.Russian, "Начать<size=-26>>></size>" }, 
			}
		},


		{ "PLAY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JUGAR" }, 
				{ SystemLanguage.ChineseSimplified, "开始游戏" }, 
				{ SystemLanguage.ChineseTraditional, "遊戲" }, 
				{ SystemLanguage.French, "JOUER" }, 
				{ SystemLanguage.Japanese, "プレイ" }, 
				{ SystemLanguage.German, "SPIELEN" }, 
				{ SystemLanguage.Indonesian, "MAIN" }, 
				{ SystemLanguage.Portuguese, "JOGAR" }, 
				{ SystemLanguage.Italian, "GIOCA" }, 
				{ SystemLanguage.Korean, "재생하기" }, 
				{ SystemLanguage.Russian, "ИГРАТЬ" }, 
			}
		},


		{ "START", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EMPEZAR" }, 
				{ SystemLanguage.ChineseSimplified, "开始" }, 
				{ SystemLanguage.ChineseTraditional, "開始" }, 
				{ SystemLanguage.French, "DÉBUT" }, 
				{ SystemLanguage.Japanese, "開始" }, 
				{ SystemLanguage.German, "STARTEN" }, 
				{ SystemLanguage.Indonesian, "MULAI" }, 
				{ SystemLanguage.Portuguese, "INICIAR" }, 
				{ SystemLanguage.Italian, "INIZIA" }, 
				{ SystemLanguage.Korean, "출발하기" }, 
				{ SystemLanguage.Russian, "СТАРТ" }, 
			}
		},


		{ "READY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PREPÁRATE" }, 
				{ SystemLanguage.ChineseSimplified, "预备" }, 
				{ SystemLanguage.ChineseTraditional, "請準備" }, 
				{ SystemLanguage.French, "PRÉPAREZ-VOUS" }, 
				{ SystemLanguage.Japanese, "レディー" }, 
				{ SystemLanguage.German, "MACH DICH BEREIT" }, 
				{ SystemLanguage.Indonesian, "BERSIAP" }, 
				{ SystemLanguage.Portuguese, "ATENÇÃO" }, 
				{ SystemLanguage.Italian, "PRONTI" }, 
				{ SystemLanguage.Korean, "준비하시고" }, 
				{ SystemLanguage.Russian, "ВНИМАНИЕ" }, 
			}
		},


		{ "GO!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡VE!" }, 
				{ SystemLanguage.ChineseSimplified, "开始！" }, 
				{ SystemLanguage.ChineseTraditional, "開始！" }, 
				{ SystemLanguage.French, "GO !" }, 
				{ SystemLanguage.Japanese, "ゴー！" }, 
				{ SystemLanguage.German, "LOS!" }, 
				{ SystemLanguage.Indonesian, "PERGI !" }, 
				{ SystemLanguage.Portuguese, "VÁ!" }, 
				{ SystemLanguage.Italian, "VIA!" }, 
				{ SystemLanguage.Korean, "시작!" }, 
				{ SystemLanguage.Russian, "МАРШ!" }, 
			}
		},


		{ "OPTIONS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "OPCIONES" }, 
				{ SystemLanguage.ChineseSimplified, "选项" }, 
				{ SystemLanguage.ChineseTraditional, "選項" }, 
				{ SystemLanguage.French, "OPTIONS" }, 
				{ SystemLanguage.Japanese, "オプション" }, 
				{ SystemLanguage.German, "OPTIONEN" }, 
				{ SystemLanguage.Indonesian, "PILIHAN" }, 
				{ SystemLanguage.Portuguese, "OPÇÕES" }, 
				{ SystemLanguage.Italian, "OPZIONI" }, 
				{ SystemLanguage.Korean, "옵션" }, 
				{ SystemLanguage.Russian, "МЕНЮ" }, 
			}
		},


		{ "SETTINGS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "AJUSTES" }, 
				{ SystemLanguage.ChineseSimplified, "设置" }, 
				{ SystemLanguage.ChineseTraditional, "設置" }, 
				{ SystemLanguage.French, "PARAMÈTRES" }, 
				{ SystemLanguage.Japanese, "設定" }, 
				{ SystemLanguage.German, "EINSTELLUNGEN" }, 
				{ SystemLanguage.Indonesian, "PENGATURAN" }, 
				{ SystemLanguage.Portuguese, "CONFIGURAÇÕES" }, 
				{ SystemLanguage.Italian, "IMPOSTAZIONI" }, 
				{ SystemLanguage.Korean, "설정" }, 
				{ SystemLanguage.Russian, "НАСТРОЙКИ" }, 
			}
		},


		{ "MUSIC", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MÚSICA" }, 
				{ SystemLanguage.ChineseSimplified, "音乐" }, 
				{ SystemLanguage.ChineseTraditional, "音樂" }, 
				{ SystemLanguage.French, "MUSIQUE" }, 
				{ SystemLanguage.Japanese, "音楽" }, 
				{ SystemLanguage.German, "MUSIK" }, 
				{ SystemLanguage.Indonesian, "MUSIK" }, 
				{ SystemLanguage.Portuguese, "MÚSICA" }, 
				{ SystemLanguage.Italian, "MUSICA" }, 
				{ SystemLanguage.Korean, "음악" }, 
				{ SystemLanguage.Russian, "МУЗЫКА" }, 
			}
		},


		{ "MUSIC ON", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MÚSICA ACTIVADA" }, 
				{ SystemLanguage.ChineseSimplified, "开启音乐" }, 
				{ SystemLanguage.ChineseTraditional, "開啟音樂" }, 
				{ SystemLanguage.French, "MUSIQUE ON" }, 
				{ SystemLanguage.Japanese, "音楽オン" }, 
				{ SystemLanguage.German, "MUSIK AN" }, 
				{ SystemLanguage.Indonesian, "MUSIK MENYALA" }, 
				{ SystemLanguage.Portuguese, "MÚSICA LIGADA" }, 
				{ SystemLanguage.Italian, "ACCENDI MUSICA" }, 
				{ SystemLanguage.Korean, "음악 켜기" }, 
				{ SystemLanguage.Russian, "МУЗЫКА ВКЛ." }, 
			}
		},


		{ "MUSIC OFF", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MÚSICA DESACTIVADA" }, 
				{ SystemLanguage.ChineseSimplified, "关闭音乐" }, 
				{ SystemLanguage.ChineseTraditional, "關閉音樂" }, 
				{ SystemLanguage.French, "MUSIQUE OFF" }, 
				{ SystemLanguage.Japanese, "音楽オフ" }, 
				{ SystemLanguage.German, "MUSIK AUS" }, 
				{ SystemLanguage.Indonesian, "MUSIK MATI" }, 
				{ SystemLanguage.Portuguese, "MÚSICA DESLIGADA" }, 
				{ SystemLanguage.Italian, "SPEGNI MUSICA" }, 
				{ SystemLanguage.Korean, "음악 끄기" }, 
				{ SystemLanguage.Russian, "МУЗЫКА ОТКЛ." }, 
			}
		},


		{ "SOUND EFFECT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EFECTO DE SONIDO" }, 
				{ SystemLanguage.ChineseSimplified, "声效" }, 
				{ SystemLanguage.ChineseTraditional, "音效" }, 
				{ SystemLanguage.French, "EFFET SONORE" }, 
				{ SystemLanguage.Japanese, "音響効果" }, 
				{ SystemLanguage.German, "SOUNDEFFEKT" }, 
				{ SystemLanguage.Indonesian, "EFEK SUARA" }, 
				{ SystemLanguage.Portuguese, "EFEITOS SONOROS" }, 
				{ SystemLanguage.Italian, "EFFETTI SONORI" }, 
				{ SystemLanguage.Korean, "음향 효과" }, 
				{ SystemLanguage.Russian, "ЗВУКОВЫЕ ЭФФЕКТЫ" }, 
			}
		},


		{ "SOUND FX", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SONIDO FX" }, 
				{ SystemLanguage.ChineseSimplified, "音效" }, 
				{ SystemLanguage.ChineseTraditional, "聲音效果" }, 
				{ SystemLanguage.French, "SON FX" }, 
				{ SystemLanguage.Japanese, "FX" }, 
				{ SystemLanguage.German, "SOUND FX" }, 
				{ SystemLanguage.Indonesian, "SUARA FX" }, 
				{ SystemLanguage.Portuguese, "SOM FX" }, 
				{ SystemLanguage.Italian, "AUDIO FX" }, 
				{ SystemLanguage.Korean, "음향 FX" }, 
				{ SystemLanguage.Russian, "ЗВУК FX" }, 
			}
		},


		{ "ALERTS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ALERTAS" }, 
				{ SystemLanguage.ChineseSimplified, "警报" }, 
				{ SystemLanguage.ChineseTraditional, "警示" }, 
				{ SystemLanguage.French, "ALERTES" }, 
				{ SystemLanguage.Japanese, "アラート" }, 
				{ SystemLanguage.German, "WARNSIGNALE" }, 
				{ SystemLanguage.Indonesian, "WASPADA" }, 
				{ SystemLanguage.Portuguese, "ALERTAS" }, 
				{ SystemLanguage.Italian, "AVVISI" }, 
				{ SystemLanguage.Korean, "알리미" }, 
				{ SystemLanguage.Russian, "СИГНАЛЫ" }, 
			}
		},


		{ "NOTIFICATIONS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "NOTIFICACIONES" }, 
				{ SystemLanguage.ChineseSimplified, "通知" }, 
				{ SystemLanguage.ChineseTraditional, "通知" }, 
				{ SystemLanguage.French, "NOTIFICATIONS" }, 
				{ SystemLanguage.Japanese, "通知" }, 
				{ SystemLanguage.German, "BENACHRICHTIGUNGEN" }, 
				{ SystemLanguage.Indonesian, "PEMBERITAHUAN" }, 
				{ SystemLanguage.Portuguese, "NOTIFICAÇÕES" }, 
				{ SystemLanguage.Italian, "NOTIFICHE" }, 
				{ SystemLanguage.Korean, "공고" }, 
				{ SystemLanguage.Russian, "УВЕДОМЛЕНИЯ" }, 
			}
		},


		{ "LANGUAGE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "IDIOMA" }, 
				{ SystemLanguage.ChineseSimplified, "语言" }, 
				{ SystemLanguage.ChineseTraditional, "語言" }, 
				{ SystemLanguage.French, "LANGUE" }, 
				{ SystemLanguage.Japanese, "言語" }, 
				{ SystemLanguage.German, "SPRACHE" }, 
				{ SystemLanguage.Indonesian, "BAHASA" }, 
				{ SystemLanguage.Portuguese, "IDIOMA" }, 
				{ SystemLanguage.Italian, "LINGUA" }, 
				{ SystemLanguage.Korean, "언어" }, 
				{ SystemLanguage.Russian, "ЯЗЫК" }, 
			}
		},


		{ "RESTORE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RESTAURAR" }, 
				{ SystemLanguage.ChineseSimplified, "恢复" }, 
				{ SystemLanguage.ChineseTraditional, "復原" }, 
				{ SystemLanguage.French, "RESTAURER" }, 
				{ SystemLanguage.Japanese, "リストア" }, 
				{ SystemLanguage.German, "WIEDERHERSTELLEN" }, 
				{ SystemLanguage.Indonesian, "MENGEMBALIKAN" }, 
				{ SystemLanguage.Portuguese, "RESTAURAR" }, 
				{ SystemLanguage.Italian, "RIPRISTINA" }, 
				{ SystemLanguage.Korean, "복원하기" }, 
				{ SystemLanguage.Russian, "ВОССТАНОВИТЬ" }, 
			}
		},


		{ "LOADING", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CARGANDO" }, 
				{ SystemLanguage.ChineseSimplified, "加载中" }, 
				{ SystemLanguage.ChineseTraditional, "加載中" }, 
				{ SystemLanguage.French, "CHARGEMENT" }, 
				{ SystemLanguage.Japanese, "読み込み中" }, 
				{ SystemLanguage.German, "LADEN" }, 
				{ SystemLanguage.Indonesian, "MEMUAT" }, 
				{ SystemLanguage.Portuguese, "CARREGANDO" }, 
				{ SystemLanguage.Italian, "IN CARICAMENTO" }, 
				{ SystemLanguage.Korean, "로딩하는 중" }, 
				{ SystemLanguage.Russian, "ИДЕТ ЗАГРУЗКА" }, 
			}
		},


		{ "SAVE ME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SÁLVAME" }, 
				{ SystemLanguage.ChineseSimplified, "救救我" }, 
				{ SystemLanguage.ChineseTraditional, "保存" }, 
				{ SystemLanguage.French, "SAUVEGARDER-MOI" }, 
				{ SystemLanguage.Japanese, "セーブ" }, 
				{ SystemLanguage.German, "SPEICHERN" }, 
				{ SystemLanguage.Indonesian, "SELAMATKAN SAYA" }, 
				{ SystemLanguage.Portuguese, "SALVE-ME" }, 
				{ SystemLanguage.Italian, "SALVAMI" }, 
				{ SystemLanguage.Korean, "나를 구해주세요" }, 
				{ SystemLanguage.Russian, "СОХРАНИТЕ МЕНЯ" }, 
			}
		},


		{ "RETRY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VOLVERLO A INTENTAR" }, 
				{ SystemLanguage.ChineseSimplified, "重试" }, 
				{ SystemLanguage.ChineseTraditional, "重試" }, 
				{ SystemLanguage.French, "RÉESSAYEZ" }, 
				{ SystemLanguage.Japanese, "リトライ" }, 
				{ SystemLanguage.German, "ERNEUT VERSUCHEN" }, 
				{ SystemLanguage.Indonesian, "MENCOBA LAGI" }, 
				{ SystemLanguage.Portuguese, "TENTE DE NOVO" }, 
				{ SystemLanguage.Italian, "RIPROVA" }, 
				{ SystemLanguage.Korean, "재시도하기" }, 
				{ SystemLanguage.Russian, "ПОВТОРИТЬ ПОПЫТКУ" }, 
			}
		},


		{ "OPEN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ABRIR" }, 
				{ SystemLanguage.ChineseSimplified, "打开" }, 
				{ SystemLanguage.ChineseTraditional, "打開" }, 
				{ SystemLanguage.French, "OUVERT" }, 
				{ SystemLanguage.Japanese, "開く" }, 
				{ SystemLanguage.German, "ÖFFNEN" }, 
				{ SystemLanguage.Indonesian, "BUKA" }, 
				{ SystemLanguage.Portuguese, "ABRIR" }, 
				{ SystemLanguage.Italian, "APRI" }, 
				{ SystemLanguage.Korean, "열기" }, 
				{ SystemLanguage.Russian, "ОТКРЫТЬ" }, 
			}
		},


		{ "PAUSE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PAUSA" }, 
				{ SystemLanguage.ChineseSimplified, "暂停" }, 
				{ SystemLanguage.ChineseTraditional, "暫停" }, 
				{ SystemLanguage.French, "PAUSE" }, 
				{ SystemLanguage.Japanese, "ポーズ" }, 
				{ SystemLanguage.German, "PAUSE" }, 
				{ SystemLanguage.Indonesian, "BERHENTI SEBENTAR" }, 
				{ SystemLanguage.Portuguese, "PAUSA" }, 
				{ SystemLanguage.Italian, "PAUSA" }, 
				{ SystemLanguage.Korean, "중지하기" }, 
				{ SystemLanguage.Russian, "ПАУЗА" }, 
			}
		},


		{ "PAUSED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PAUSADO" }, 
				{ SystemLanguage.ChineseSimplified, "已暂停" }, 
				{ SystemLanguage.ChineseTraditional, "已暫停" }, 
				{ SystemLanguage.French, "EN PAUSE" }, 
				{ SystemLanguage.Japanese, "ポーズ中" }, 
				{ SystemLanguage.German, "PAUSIERT" }, 
				{ SystemLanguage.Indonesian, "BERHENTI" }, 
				{ SystemLanguage.Portuguese, "PAUSADO" }, 
				{ SystemLanguage.Italian, "IN PAUSA" }, 
				{ SystemLanguage.Korean, "중지됨" }, 
				{ SystemLanguage.Russian, "ПРИОСТАНОВЛЕНО" }, 
			}
		},


		{ "RE-FUEL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LLENAR COMBUSTIBLE" }, 
				{ SystemLanguage.ChineseSimplified, "加油" }, 
				{ SystemLanguage.ChineseTraditional, "加油" }, 
				{ SystemLanguage.French, "RAVITAILLER" }, 
				{ SystemLanguage.Japanese, "給油" }, 
				{ SystemLanguage.German, "ERNEUT TANKEN" }, 
				{ SystemLanguage.Indonesian, "ISI BENSIN" }, 
				{ SystemLanguage.Portuguese, "ABASTEÇA" }, 
				{ SystemLanguage.Italian, "FAI IL PIENO" }, 
				{ SystemLanguage.Korean, "연료 다시 채우기" }, 
				{ SystemLanguage.Russian, "ДОЗАПРАВИТЬ" }, 
			}
		},


		{ "FILL UP NOW", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LLENAR AHORA" }, 
				{ SystemLanguage.ChineseSimplified, "立即加油" }, 
				{ SystemLanguage.ChineseTraditional, "馬上加油" }, 
				{ SystemLanguage.French, "RAVITAILLEZ MAINTENANT" }, 
				{ SystemLanguage.Japanese, "今すぐ給油" }, 
				{ SystemLanguage.German, "JETZT AUFFÜLLEN" }, 
				{ SystemLanguage.Indonesian, "ISI BENSIN SEKARANG" }, 
				{ SystemLanguage.Portuguese, "ABASTEÇA AGORA" }, 
				{ SystemLanguage.Italian, "FAI IL PIENO ADESSO" }, 
				{ SystemLanguage.Korean, "지금 연료를 채우기" }, 
				{ SystemLanguage.Russian, "ДОЗАПРАВИТЬ СЕЙЧАС" }, 
			}
		},


		{ "HEAD WEAR", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBREROS" }, 
				{ SystemLanguage.ChineseSimplified, "头饰" }, 
				{ SystemLanguage.ChineseTraditional, "頭飾" }, 
				{ SystemLanguage.French, "COUVRE-CHEF" }, 
				{ SystemLanguage.Japanese, "かぶり物" }, 
				{ SystemLanguage.German, "KOPFBEDECKUNG" }, 
				{ SystemLanguage.Indonesian, "HIASAN KEPALA" }, 
				{ SystemLanguage.Portuguese, "APARÊNCIA DO CABEÇALHO" }, 
				{ SystemLanguage.Italian, "DA METTERE IN TESTA" }, 
				{ SystemLanguage.Korean, "머리 장식용품" }, 
				{ SystemLanguage.Russian, "ГОЛОВНЫЕ УБОРЫ" }, 
			}
		},


		{ "BASEBALL CAP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "GORRA DE BEÍSBOL" }, 
				{ SystemLanguage.ChineseSimplified, "棒球帽" }, 
				{ SystemLanguage.ChineseTraditional, "棒球帽" }, 
				{ SystemLanguage.French, "CASQUETTE DE BASEBALL" }, 
				{ SystemLanguage.Japanese, "野球帽" }, 
				{ SystemLanguage.German, "BASEBALLMÜTZE" }, 
				{ SystemLanguage.Indonesian, "TOPI BASEBALL" }, 
				{ SystemLanguage.Portuguese, "BONÉ DE BASQUETE" }, 
				{ SystemLanguage.Italian, "BERRETTO DA BASEBALL" }, 
				{ SystemLanguage.Korean, "야구 모자" }, 
				{ SystemLanguage.Russian, "БЕЙСБОЛКА" }, 
			}
		},


		{ "MULLET", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MULLET" }, 
				{ SystemLanguage.ChineseSimplified, "胭脂鱼发型" }, 
				{ SystemLanguage.ChineseTraditional, "搖滾發型" }, 
				{ SystemLanguage.French, "MULET" }, 
				{ SystemLanguage.Japanese, "襟足" }, 
				{ SystemLanguage.German, "MULLET" }, 
				{ SystemLanguage.Indonesian, "MULLET" }, 
				{ SystemLanguage.Portuguese, "LOIRÃO" }, 
				{ SystemLanguage.Italian, "CAPELLI A TRIGLIA" }, 
				{ SystemLanguage.Korean, "멀릿헤어스타일" }, 
				{ SystemLanguage.Russian, "РЫБИЙ ХВОСТ" }, 
			}
		},


		{ "DISGUISE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DISFRAZ" }, 
				{ SystemLanguage.ChineseSimplified, "化装" }, 
				{ SystemLanguage.ChineseTraditional, "偽裝" }, 
				{ SystemLanguage.French, "DÉGUISEMENT" }, 
				{ SystemLanguage.Japanese, "変装" }, 
				{ SystemLanguage.German, "VERKLEIDUNG" }, 
				{ SystemLanguage.Indonesian, "PENYAMARAN" }, 
				{ SystemLanguage.Portuguese, "DISFARCE" }, 
				{ SystemLanguage.Italian, "TRAVESTIMENTO" }, 
				{ SystemLanguage.Korean, "변장" }, 
				{ SystemLanguage.Russian, "НЕВИДИМКА" }, 
			}
		},


		{ "TOP HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO DE COPA" }, 
				{ SystemLanguage.ChineseSimplified, "高顶礼帽" }, 
				{ SystemLanguage.ChineseTraditional, "禮帽" }, 
				{ SystemLanguage.French, "CHAPEAU HAUT DE FORME" }, 
				{ SystemLanguage.Japanese, "シルクハット" }, 
				{ SystemLanguage.German, "ZYLINDERHUT" }, 
				{ SystemLanguage.Indonesian, "TOPI TINGGI" }, 
				{ SystemLanguage.Portuguese, "CARTOLA" }, 
				{ SystemLanguage.Italian, "CAPPELLO A CILINDRO" }, 
				{ SystemLanguage.Korean, "톱 햇" }, 
				{ SystemLanguage.Russian, "ЦИЛИНДР" }, 
			}
		},


		{ "PAPER BAG", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "BOLSA DE PAPEL" }, 
				{ SystemLanguage.ChineseSimplified, "纸袋" }, 
				{ SystemLanguage.ChineseTraditional, "紙袋" }, 
				{ SystemLanguage.French, "SAC EN PAPIER" }, 
				{ SystemLanguage.Japanese, "紙袋" }, 
				{ SystemLanguage.German, "PAPIERMÜTZE" }, 
				{ SystemLanguage.Indonesian, "TAS KERTAS" }, 
				{ SystemLanguage.Portuguese, "SACOLA" }, 
				{ SystemLanguage.Italian, "SACCHETTO DI CARTA" }, 
				{ SystemLanguage.Korean, "종이 봉지" }, 
				{ SystemLanguage.Russian, "БУМАЖНЫЙ ПАКЕТ" }, 
			}
		},


		{ "MOHAWK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MOHAWK" }, 
				{ SystemLanguage.ChineseSimplified, "莫霍克" }, 
				{ SystemLanguage.ChineseTraditional, "莫霍克發型" }, 
				{ SystemLanguage.French, "MOHAWK" }, 
				{ SystemLanguage.Japanese, "モヒカン" }, 
				{ SystemLanguage.German, "MOWHAWK" }, 
				{ SystemLanguage.Indonesian, "MOHAWK (gaya rambut)" }, 
				{ SystemLanguage.Portuguese, "CABELO MOICANO" }, 
				{ SystemLanguage.Italian, "CAPELLI ALLA MOICANA" }, 
				{ SystemLanguage.Korean, "모호크 (헤어 스타일)" }, 
				{ SystemLanguage.Russian, "ИРОКЕЗ (прическа)" }, 
			}
		},


		{ "PUNK HAIR", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUNK" }, 
				{ SystemLanguage.ChineseSimplified, "朋克发型" }, 
				{ SystemLanguage.ChineseTraditional, "龐克頭" }, 
				{ SystemLanguage.French, "CHEVEUX PUNK" }, 
				{ SystemLanguage.Japanese, "パンク" }, 
				{ SystemLanguage.German, "PUNKHAARE" }, 
				{ SystemLanguage.Indonesian, "RAMBUT PUNK" }, 
				{ SystemLanguage.Portuguese, "CABELO PUNK" }, 
				{ SystemLanguage.Italian, "CAPELLI DA PUNK" }, 
				{ SystemLanguage.Korean, "펑크 머리" }, 
				{ SystemLanguage.Russian, "ПАНК" }, 
			}
		},


		{ "PIRATE HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO PIRATA" }, 
				{ SystemLanguage.ChineseSimplified, "海盗帽" }, 
				{ SystemLanguage.ChineseTraditional, "海盜帽" }, 
				{ SystemLanguage.French, "CHAPEAU DE PIRATE" }, 
				{ SystemLanguage.Japanese, "海賊" }, 
				{ SystemLanguage.German, "PIRATENHUT" }, 
				{ SystemLanguage.Indonesian, "TOPI BAJAK LAUT" }, 
				{ SystemLanguage.Portuguese, "CHAPÉU DE PIRATA" }, 
				{ SystemLanguage.Italian, "CAPPELLO DA PIRATA" }, 
				{ SystemLanguage.Korean, "해적 모자" }, 
				{ SystemLanguage.Russian, "ПИРАТСКАЯ ШЛЯПА" }, 
			}
		},


		{ "PUMPKIN HEAD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CABEZA DE CALABAZA" }, 
				{ SystemLanguage.ChineseSimplified, "南瓜头" }, 
				{ SystemLanguage.ChineseTraditional, "南瓜頭" }, 
				{ SystemLanguage.French, "TÊTE DE CITROUILLE" }, 
				{ SystemLanguage.Japanese, "かぼちゃ頭" }, 
				{ SystemLanguage.German, "KÜRBISKOPF" }, 
				{ SystemLanguage.Indonesian, "KEPALA LABU" }, 
				{ SystemLanguage.Portuguese, "CABEÇA DE ABÓBORA" }, 
				{ SystemLanguage.Italian, "TESTA DI ZUCCA" }, 
				{ SystemLanguage.Korean, "호박 머리" }, 
				{ SystemLanguage.Russian, "БОЛВАНКА" }, 
			}
		},


		{ "COWBOY HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO DE BAQUERO" }, 
				{ SystemLanguage.ChineseSimplified, "牛仔帽" }, 
				{ SystemLanguage.ChineseTraditional, "牛仔帽" }, 
				{ SystemLanguage.French, "CHAPEAU DE COWBOY" }, 
				{ SystemLanguage.Japanese, "カウボーイハット" }, 
				{ SystemLanguage.German, "COWBOYHUT" }, 
				{ SystemLanguage.Indonesian, "TOPI KOBOI" }, 
				{ SystemLanguage.Portuguese, "CHAPÉU DE CAUBÓI" }, 
				{ SystemLanguage.Italian, "CAPPELLO DA COWBOY" }, 
				{ SystemLanguage.Korean, "카우보이 모자" }, 
				{ SystemLanguage.Russian, "КОВБОЙСКАЯ ШЛЯПА" }, 
			}
		},


		{ "RASTACAP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JAMAICA" }, 
				{ SystemLanguage.ChineseSimplified, "牙买加" }, 
				{ SystemLanguage.ChineseTraditional, "牙買加帽子" }, 
				{ SystemLanguage.French, "JAMAÏQUE" }, 
				{ SystemLanguage.Japanese, "ジャマイカ" }, 
				{ SystemLanguage.German, "JAMAIKA" }, 
				{ SystemLanguage.Indonesian, "JAMAIKA" }, 
				{ SystemLanguage.Portuguese, "JAMAICA" }, 
				{ SystemLanguage.Italian, "JAMAICA" }, 
				{ SystemLanguage.Korean, "자메이카" }, 
				{ SystemLanguage.Russian, "ЯМАЙКА" }, 
			}
		},


		{ "EMO", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EMO" }, 
				{ SystemLanguage.ChineseSimplified, "自杀式情绪摇滚" }, 
				{ SystemLanguage.ChineseTraditional, "情緒發" }, 
				{ SystemLanguage.French, "EMO" }, 
				{ SystemLanguage.Japanese, "前髪ウェーブ" }, 
				{ SystemLanguage.German, "EMO" }, 
				{ SystemLanguage.Indonesian, "EMO" }, 
				{ SystemLanguage.Portuguese, "EMO" }, 
				{ SystemLanguage.Italian, "EMO" }, 
				{ SystemLanguage.Korean, "EMO" }, 
				{ SystemLanguage.Russian, "СУИЦИДАЛЬНЫЙ ЭМО" }, 
			}
		},


		{ "JEEP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TODOTERRENO" }, 
				{ SystemLanguage.ChineseSimplified, "吉普" }, 
				{ SystemLanguage.ChineseTraditional, "吉普車" }, 
				{ SystemLanguage.French, "JEEP" }, 
				{ SystemLanguage.Japanese, "ジープ" }, 
				{ SystemLanguage.German, "JEEP" }, 
				{ SystemLanguage.Indonesian, "MOBIL JIP" }, 
				{ SystemLanguage.Portuguese, "JIPE" }, 
				{ SystemLanguage.Italian, "JEEP" }, 
				{ SystemLanguage.Korean, "지프" }, 
				{ SystemLanguage.Russian, "ДЖИП" }, 
			}
		},


		{ "CAR", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "AUTOMÓVIL" }, 
				{ SystemLanguage.ChineseSimplified, "汽车" }, 
				{ SystemLanguage.ChineseTraditional, "車" }, 
				{ SystemLanguage.French, "VOITURE" }, 
				{ SystemLanguage.Japanese, "車" }, 
				{ SystemLanguage.German, "AUTO" }, 
				{ SystemLanguage.Indonesian, "MOBIL" }, 
				{ SystemLanguage.Portuguese, "CARRO" }, 
				{ SystemLanguage.Italian, "AUTOMOBILE" }, 
				{ SystemLanguage.Korean, "차" }, 
				{ SystemLanguage.Russian, "МАШИНА" }, 
			}
		},


		{ "FUEL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMBUSTIBLE" }, 
				{ SystemLanguage.ChineseSimplified, "汽油" }, 
				{ SystemLanguage.ChineseTraditional, "汽油" }, 
				{ SystemLanguage.French, "CARBURANT" }, 
				{ SystemLanguage.Japanese, "燃料" }, 
				{ SystemLanguage.German, "BENZIN" }, 
				{ SystemLanguage.Indonesian, "BENSIN" }, 
				{ SystemLanguage.Portuguese, "GASOLINA" }, 
				{ SystemLanguage.Italian, "BENZINA" }, 
				{ SystemLanguage.Korean, "연료" }, 
				{ SystemLanguage.Russian, "ТОПЛИВО" }, 
			}
		},


		{ "ENGINE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MOTOR" }, 
				{ SystemLanguage.ChineseSimplified, "发动机" }, 
				{ SystemLanguage.ChineseTraditional, "引擎" }, 
				{ SystemLanguage.French, "MOTEUR" }, 
				{ SystemLanguage.Japanese, "エンジン" }, 
				{ SystemLanguage.German, "MOTOR" }, 
				{ SystemLanguage.Indonesian, "MESIN" }, 
				{ SystemLanguage.Portuguese, "MOTOR" }, 
				{ SystemLanguage.Italian, "MOTORE" }, 
				{ SystemLanguage.Korean, "엔진" }, 
				{ SystemLanguage.Russian, "ДВИГАТЕЛЬ" }, 
			}
		},


		{ "WHEELS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RUEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "车轮" }, 
				{ SystemLanguage.ChineseTraditional, "輪胎" }, 
				{ SystemLanguage.French, "ROUES" }, 
				{ SystemLanguage.Japanese, "車輪" }, 
				{ SystemLanguage.German, "REIFEN" }, 
				{ SystemLanguage.Indonesian, "RODA" }, 
				{ SystemLanguage.Portuguese, "RODAS" }, 
				{ SystemLanguage.Italian, "PNEUMATICI" }, 
				{ SystemLanguage.Korean, "바퀴" }, 
				{ SystemLanguage.Russian, "КОЛЕСА" }, 
			}
		},


		{ "SKATEBOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PATINETA" }, 
				{ SystemLanguage.ChineseSimplified, "滑板" }, 
				{ SystemLanguage.ChineseTraditional, "滑板" }, 
				{ SystemLanguage.French, "SKATEBOARD" }, 
				{ SystemLanguage.Japanese, "スケートボード" }, 
				{ SystemLanguage.German, "SKATEBOARD" }, 
				{ SystemLanguage.Indonesian, "PAPAN LUNCUR" }, 
				{ SystemLanguage.Portuguese, "SKATE" }, 
				{ SystemLanguage.Italian, "FARE SKATEBOARD" }, 
				{ SystemLanguage.Korean, "스케이트보드" }, 
				{ SystemLanguage.Russian, "СКЕЙТБОРД" }, 
			}
		},


		{ "SKATE NOW", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PATINAR AHORA" }, 
				{ SystemLanguage.ChineseSimplified, "立即滑行" }, 
				{ SystemLanguage.ChineseTraditional, "開始滑行" }, 
				{ SystemLanguage.French, "SKATE MAINTENANT" }, 
				{ SystemLanguage.Japanese, "スケートボード開始" }, 
				{ SystemLanguage.German, "JETZT SKATEN" }, 
				{ SystemLanguage.Indonesian, "MELUNCUR SEKARANG" }, 
				{ SystemLanguage.Portuguese, "USE O SKATE AGORA" }, 
				{ SystemLanguage.Italian, "FAI SKATEBOARD ORA" }, 
				{ SystemLanguage.Korean, "지금 스케이트타기" }, 
				{ SystemLanguage.Russian, "КАТАТЬСЯ СЕЙЧАС" }, 
			}
		},


		{ "EXTRA TIME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TIEMPO EXTRA" }, 
				{ SystemLanguage.ChineseSimplified, "额外时间" }, 
				{ SystemLanguage.ChineseTraditional, "額外時間" }, 
				{ SystemLanguage.French, "TEMPS SUPPLÉMENTAIRE" }, 
				{ SystemLanguage.Japanese, "追加タイム" }, 
				{ SystemLanguage.German, "EXTRA ZEIT" }, 
				{ SystemLanguage.Indonesian, "TAMBAHAN WAKTU" }, 
				{ SystemLanguage.Portuguese, "TEMPO EXTRA" }, 
				{ SystemLanguage.Italian, "TEMPO EXTRA" }, 
				{ SystemLanguage.Korean, "연장 시간" }, 
				{ SystemLanguage.Russian, "ДОПОЛНИТЕЛЬНОЕ ВРЕМЯ" }, 
			}
		},


		{ "COINS x2", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MONEDAS x2" }, 
				{ SystemLanguage.ChineseSimplified, "金币 x2" }, 
				{ SystemLanguage.ChineseTraditional, "金幣x 2" }, 
				{ SystemLanguage.French, "PIÈCES X2" }, 
				{ SystemLanguage.Japanese, "コイン2倍" }, 
				{ SystemLanguage.German, "MÜNZEN x2" }, 
				{ SystemLanguage.Indonesian, "KOIN DIKALIKAN 2" }, 
				{ SystemLanguage.Portuguese, "MOEDAS x2" }, 
				{ SystemLanguage.Italian, "MONETE x2" }, 
				{ SystemLanguage.Korean, "동전 x2" }, 
				{ SystemLanguage.Russian, "МОНЕТЫ х2" }, 
			}
		},


		{ "COINS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MONEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "金币" }, 
				{ SystemLanguage.ChineseTraditional, "金幣" }, 
				{ SystemLanguage.French, "PIÈCES" }, 
				{ SystemLanguage.Japanese, "コイン倍" }, 
				{ SystemLanguage.German, "MÜNZEN" }, 
				{ SystemLanguage.Indonesian, "KOIN DIKALIKAN" }, 
				{ SystemLanguage.Portuguese, "MOEDAS" }, 
				{ SystemLanguage.Italian, "MONETE" }, 
				{ SystemLanguage.Korean, "동전" }, 
				{ SystemLanguage.Russian, "МОНЕТЫ" }, 
			}
		},


		{ "COINS x2 DURATION", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DURACIÓN DE LAS MONEDAS X2" }, 
				{ SystemLanguage.ChineseSimplified, "金币 x2 持续时间" }, 
				{ SystemLanguage.ChineseTraditional, "金幣x 2時效" }, 
				{ SystemLanguage.French, "PIÈCES X2 DURÉE" }, 
				{ SystemLanguage.Japanese, "コイン2倍の時間" }, 
				{ SystemLanguage.German, "MÜNZEN x2 DAUER" }, 
				{ SystemLanguage.Indonesian, "KOIN DIKALIKAN 2 DURASI" }, 
				{ SystemLanguage.Portuguese, "MOEDAS x2 DURAÇÃO" }, 
				{ SystemLanguage.Italian, "DURATA MONETE x2" }, 
				{ SystemLanguage.Korean, "동전 x2 지속시간" }, 
				{ SystemLanguage.Russian, "ДЛИТЕЛЬНОСТЬ МОНЕТЫ х2" }, 
			}
		},


		{ "POWER DURATION", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DURACIÓN DE PODER" }, 
				{ SystemLanguage.ChineseSimplified, "动力滑行时间" }, 
				{ SystemLanguage.ChineseTraditional, "能力時效" }, 
				{ SystemLanguage.French, "DURÉE DE PUISSANCE" }, 
				{ SystemLanguage.Japanese, "パワー持続時間" }, 
				{ SystemLanguage.German, "ENERGIEDAUER" }, 
				{ SystemLanguage.Indonesian, "DURASI KEKUATAN" }, 
				{ SystemLanguage.Portuguese, "DURAÇÃO DA ENERGIA" }, 
				{ SystemLanguage.Italian, "DURATA ALIMENTAZIONE" }, 
				{ SystemLanguage.Korean, "전원 지속시간" }, 
				{ SystemLanguage.Russian, "ДЛИТЕЛЬНОСТЬ МОЩНОСТИ" }, 
			}
		},


		{ "CHILI", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CHILE" }, 
				{ SystemLanguage.ChineseSimplified, "辣椒" }, 
				{ SystemLanguage.ChineseTraditional, "辣椒" }, 
				{ SystemLanguage.French, "CHILI" }, 
				{ SystemLanguage.Japanese, "唐辛子" }, 
				{ SystemLanguage.German, "CHILI" }, 
				{ SystemLanguage.Indonesian, "PEDAS" }, 
				{ SystemLanguage.Portuguese, "CHILI" }, 
				{ SystemLanguage.Italian, "PEPERONCINO" }, 
				{ SystemLanguage.Korean, "쌀쌀한" }, 
				{ SystemLanguage.Russian, "ЧИЛИ" }, 
			}
		},


		{ "JET SKI", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MOTO ACUÁTICA" }, 
				{ SystemLanguage.ChineseSimplified, "水上摩托艇" }, 
				{ SystemLanguage.ChineseTraditional, "水上摩托" }, 
				{ SystemLanguage.French, "JET-SKI" }, 
				{ SystemLanguage.Japanese, "ジェットスキー" }, 
				{ SystemLanguage.German, "JETSKI" }, 
				{ SystemLanguage.Indonesian, "JET SKI" }, 
				{ SystemLanguage.Portuguese, "JET SKI" }, 
				{ SystemLanguage.Italian, "MOTO D'ACQUA" }, 
				{ SystemLanguage.Korean, "제트 스키" }, 
				{ SystemLanguage.Russian, "ГИДРОЦИКЛ" }, 
			}
		},


		{ "BODYBOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "BODYBOARD" }, 
				{ SystemLanguage.ChineseSimplified, "俯伏冲浪板" }, 
				{ SystemLanguage.ChineseTraditional, "滑浪板" }, 
				{ SystemLanguage.French, "BODYBOARD" }, 
				{ SystemLanguage.Japanese, "ボディーボード" }, 
				{ SystemLanguage.German, "WELLENBRETT" }, 
				{ SystemLanguage.Indonesian, "BODYBOARD" }, 
				{ SystemLanguage.Portuguese, "PRANCHA" }, 
				{ SystemLanguage.Italian, "TAVOLA DA BODYBOARD" }, 
				{ SystemLanguage.Korean, "바디보드" }, 
				{ SystemLanguage.Russian, "ДОСКА ДЛЯ БУГИ-СЕРФИНГА" }, 
			}
		},


		{ "TUBE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COLCHONETA" }, 
				{ SystemLanguage.ChineseSimplified, "冲浪管" }, 
				{ SystemLanguage.ChineseTraditional, "泳圈" }, 
				{ SystemLanguage.French, "TUBE" }, 
				{ SystemLanguage.Japanese, "浮き輪" }, 
				{ SystemLanguage.German, "POOLBOOT" }, 
				{ SystemLanguage.Indonesian, "BAN" }, 
				{ SystemLanguage.Portuguese, "BÓIA" }, 
				{ SystemLanguage.Italian, "CANOTTO" }, 
				{ SystemLanguage.Korean, "튜브" }, 
				{ SystemLanguage.Russian, "НАДУВНОЙ КРУГ" }, 
			}
		},


		{ "SPEED BOAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LANCHA" }, 
				{ SystemLanguage.ChineseSimplified, "快艇" }, 
				{ SystemLanguage.ChineseTraditional, "快艇" }, 
				{ SystemLanguage.French, "SPEED BOAT" }, 
				{ SystemLanguage.Japanese, "スピードボート" }, 
				{ SystemLanguage.German, "SCHNELLBOOT" }, 
				{ SystemLanguage.Indonesian, "KAPAL CEPAT" }, 
				{ SystemLanguage.Portuguese, "LANCHA" }, 
				{ SystemLanguage.Italian, "MOTOSCAFO" }, 
				{ SystemLanguage.Korean, "스피드 보트" }, 
				{ SystemLanguage.Russian, "МОТОРНАЯ ЛОДКА" }, 
			}
		},


		{ "LONGBOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TABLA LARGA" }, 
				{ SystemLanguage.ChineseSimplified, "长板" }, 
				{ SystemLanguage.ChineseTraditional, "長沖浪板" }, 
				{ SystemLanguage.French, "LONG BOARD" }, 
				{ SystemLanguage.Japanese, "ロングボード" }, 
				{ SystemLanguage.German, "LONGBOARD" }, 
				{ SystemLanguage.Indonesian, "PAPAN LUNCUR PANJANG" }, 
				{ SystemLanguage.Portuguese, "PRANCHA LONGA" }, 
				{ SystemLanguage.Italian, "LONGBOARD DA SURF" }, 
				{ SystemLanguage.Korean, "긴 보드" }, 
				{ SystemLanguage.Russian, "ДЛИННАЯ ДОСКА" }, 
			}
		},


		{ "SURFBOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TABLA DE SURF" }, 
				{ SystemLanguage.ChineseSimplified, "冲浪板" }, 
				{ SystemLanguage.ChineseTraditional, "沖浪板" }, 
				{ SystemLanguage.French, "PLANCHE DE SURF" }, 
				{ SystemLanguage.Japanese, "サーフボード" }, 
				{ SystemLanguage.German, "SURFBOARD" }, 
				{ SystemLanguage.Indonesian, "PAPAN LUNCUR" }, 
				{ SystemLanguage.Portuguese, "PRANCHA DE SURF" }, 
				{ SystemLanguage.Italian, "TAVOLA DA SURF" }, 
				{ SystemLanguage.Korean, "서프 보드" }, 
				{ SystemLanguage.Russian, "ДОСКА ДЛЯ СЕРФИНГА" }, 
			}
		},


		{ "MOTORCYCLE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MOTOCICLETA" }, 
				{ SystemLanguage.ChineseSimplified, "摩托车" }, 
				{ SystemLanguage.ChineseTraditional, "摩托車" }, 
				{ SystemLanguage.French, "MOTO" }, 
				{ SystemLanguage.Japanese, "バイク" }, 
				{ SystemLanguage.German, "MOTORRAD" }, 
				{ SystemLanguage.Indonesian, "PERAHU MOTOR" }, 
				{ SystemLanguage.Portuguese, "BIKE COM MOTOR" }, 
				{ SystemLanguage.Italian, "MOTOCICLETTA" }, 
				{ SystemLanguage.Korean, "모터바이크" }, 
				{ SystemLanguage.Russian, "МОТОЦИКЛ" }, 
			}
		},


		{ "PLANE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "AVIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "英式飞机" }, 
				{ SystemLanguage.ChineseTraditional, "飛機" }, 
				{ SystemLanguage.French, "AVION" }, 
				{ SystemLanguage.Japanese, "飛行機" }, 
				{ SystemLanguage.German, "FLUGZEUG" }, 
				{ SystemLanguage.Indonesian, "PESAWAT" }, 
				{ SystemLanguage.Portuguese, "AVIÃO" }, 
				{ SystemLanguage.Italian, "AEREO" }, 
				{ SystemLanguage.Korean, "비행기" }, 
				{ SystemLanguage.Russian, "АЭРОПЛАН" }, 
			}
		},


		{ "GOLD BOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Tabla Dorada" }, 
				{ SystemLanguage.ChineseSimplified, "黄金冲浪板" }, 
				{ SystemLanguage.ChineseTraditional, "黃金沖浪板" }, 
				{ SystemLanguage.French, "Planche d'or" }, 
				{ SystemLanguage.Japanese, "黄金のボード" }, 
				{ SystemLanguage.German, "Gold-Surfbrett" }, 
				{ SystemLanguage.Indonesian, "PAPAN LUNCUR EMAS" }, 
				{ SystemLanguage.Portuguese, "Prancha de ouro" }, 
				{ SystemLanguage.Italian, "Albo d'oro" }, 
				{ SystemLanguage.Korean, "골드 보드" }, 
				{ SystemLanguage.Russian, "Золотая доска" }, 
			}
		},


		{ "ACHIEVEMENTS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LOGROS" }, 
				{ SystemLanguage.ChineseSimplified, "成绩" }, 
				{ SystemLanguage.ChineseTraditional, "成就榜" }, 
				{ SystemLanguage.French, "RÉALISATIONS" }, 
				{ SystemLanguage.Japanese, "成果" }, 
				{ SystemLanguage.German, "ERFOLGE" }, 
				{ SystemLanguage.Indonesian, "PENCAPAIAN" }, 
				{ SystemLanguage.Portuguese, "RESULTADOS" }, 
				{ SystemLanguage.Italian, "RAGGIUNGIMENTI" }, 
				{ SystemLanguage.Korean, "업적" }, 
				{ SystemLanguage.Russian, "ДОСТИЖЕНИЯ" }, 
			}
		},


		{ "LEADERBOARD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TABLA DE CLASIFICACIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "积分排行榜" }, 
				{ SystemLanguage.ChineseTraditional, "排行榜" }, 
				{ SystemLanguage.French, "LEADERBOARD" }, 
				{ SystemLanguage.Japanese, "スコアボード" }, 
				{ SystemLanguage.German, "RANGLISTE" }, 
				{ SystemLanguage.Indonesian, "PAPAN NILAI" }, 
				{ SystemLanguage.Portuguese, "PLACAR" }, 
				{ SystemLanguage.Italian, "CLASSIFICA" }, 
				{ SystemLanguage.Korean, "순위표" }, 
				{ SystemLanguage.Russian, "ТАБЛИЦА ЛИДЕРОВ" }, 
			}
		},


		{ "HIGH SCORE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUNTUACIÓN ALTA" }, 
				{ SystemLanguage.ChineseSimplified, "高分" }, 
				{ SystemLanguage.ChineseTraditional, "最高得分" }, 
				{ SystemLanguage.French, "MEILLEURES SCORES" }, 
				{ SystemLanguage.Japanese, "ハイスコア" }, 
				{ SystemLanguage.German, "HIGHSCORE" }, 
				{ SystemLanguage.Indonesian, "NILAI TERTINGGI" }, 
				{ SystemLanguage.Portuguese, "ALTA PONTUAÇÃO" }, 
				{ SystemLanguage.Italian, "PUNTEGGIO ALTO" }, 
				{ SystemLanguage.Korean, "높은 점수" }, 
				{ SystemLanguage.Russian, "ВЫСОКИЙ РЕЗУЛЬТАТ" }, 
			}
		},


		{ "SCORE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUNTUACIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "得分" }, 
				{ SystemLanguage.ChineseTraditional, "分數" }, 
				{ SystemLanguage.French, "SCORE" }, 
				{ SystemLanguage.Japanese, "スコア" }, 
				{ SystemLanguage.German, "PUNKTZAHL" }, 
				{ SystemLanguage.Indonesian, "NILAI" }, 
				{ SystemLanguage.Portuguese, "PONTUAÇÃO" }, 
				{ SystemLanguage.Italian, "PUNTEGGIO" }, 
				{ SystemLanguage.Korean, "점수" }, 
				{ SystemLanguage.Russian, "РЕЗУЛЬТАТ" }, 
			}
		},


		{ "COMPETITION", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPETICIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "竞赛" }, 
				{ SystemLanguage.ChineseTraditional, "比賽" }, 
				{ SystemLanguage.French, "COMPETITION" }, 
				{ SystemLanguage.Japanese, "競争" }, 
				{ SystemLanguage.German, "WETTBEWERB" }, 
				{ SystemLanguage.Indonesian, "KOMPETISI" }, 
				{ SystemLanguage.Portuguese, "COMPETIÇÃO" }, 
				{ SystemLanguage.Italian, "COMPETIZIONE" }, 
				{ SystemLanguage.Korean, "경쟁" }, 
				{ SystemLanguage.Russian, "СОСТЯЗАНИЕ" }, 
			}
		},


		{ "WATCH VIDEOS AND WIN!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VE VIDEOS Y GANA" }, 
				{ SystemLanguage.ChineseSimplified, "观看视频，赢得胜利" }, 
				{ SystemLanguage.ChineseTraditional, "觀看視頻和贏取" }, 
				{ SystemLanguage.French, "REGARDER DES VIDÉOS ET GAGNEZ" }, 
				{ SystemLanguage.Japanese, "動画を見て勝利をその手に" }, 
				{ SystemLanguage.German, "SCHAUE DIR VIDEOS AN UND GEWINNE" }, 
				{ SystemLanguage.Indonesian, "LIHAT VIDEO DAN MENANG" }, 
				{ SystemLanguage.Portuguese, "ASSISTA OS VÍDEOS E VENÇA" }, 
				{ SystemLanguage.Italian, "GUARDA I VIDEO E VINCI" }, 
				{ SystemLanguage.Korean, "동영상을 보고 이기세요" }, 
				{ SystemLanguage.Russian, "ПОСМОТРЕТЬ ВИДЕО И ВЫИГРАТЬ" }, 
			}
		},


		{ "WATCH VIDEO", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VER VIDEO" }, 
				{ SystemLanguage.ChineseSimplified, "观看视频" }, 
				{ SystemLanguage.ChineseTraditional, "觀看視頻" }, 
				{ SystemLanguage.French, "REGARDER LA VIDÉO" }, 
				{ SystemLanguage.Japanese, "動画を見る" }, 
				{ SystemLanguage.German, "VIDEO ANSCHAUEN" }, 
				{ SystemLanguage.Indonesian, "LIHAT VIDEO" }, 
				{ SystemLanguage.Portuguese, "ASSISTA O VÍDEO" }, 
				{ SystemLanguage.Italian, "GUARDA IL VIDEO" }, 
				{ SystemLanguage.Korean, "동영상을 보세요" }, 
				{ SystemLanguage.Russian, "ПОСМОТРЕТЬ ВИДЕО" }, 
			}
		},


		{ "TAP TO PLAY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TOCA PARA REPRODUCIRLO" }, 
				{ SystemLanguage.ChineseSimplified, "点击开始游戏" }, 
				{ SystemLanguage.ChineseTraditional, "點擊進行播放" }, 
				{ SystemLanguage.French, "APPUYEZ SUR PLAY" }, 
				{ SystemLanguage.Japanese, "タップで再生" }, 
				{ SystemLanguage.German, "TIPPEN ZUM SPIELEN" }, 
				{ SystemLanguage.Indonesian, "KETUK UNTUK BERMAIN" }, 
				{ SystemLanguage.Portuguese, "TOQUE PARA JOGAR" }, 
				{ SystemLanguage.Italian, "TOCCA PER GIOCARE" }, 
				{ SystemLanguage.Korean, "재생하려면 툭하고 누르세요" }, 
				{ SystemLanguage.Russian, "ПРИКОСНИТЕСЬ, ЧТОБЫ ИГРАТЬ" }, 
			}
		},


		{ "PICK A SHELL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ELIGE UNA CONCHA" }, 
				{ SystemLanguage.ChineseSimplified, "挑选一个贝壳" }, 
				{ SystemLanguage.ChineseTraditional, "挑選一個貝殼" }, 
				{ SystemLanguage.French, "CHOISIR UNE COQUILLE" }, 
				{ SystemLanguage.Japanese, "貝を拾う" }, 
				{ SystemLanguage.German, "NIMM EINE MUSCHEL" }, 
				{ SystemLanguage.Indonesian, "MENGAMBIL KERANG" }, 
				{ SystemLanguage.Portuguese, "ESCOLHA UMA CONCHA" }, 
				{ SystemLanguage.Italian, "RACCOGLI UNA CONCHIGLIA" }, 
				{ SystemLanguage.Korean, "조가비 선택하기" }, 
				{ SystemLanguage.Russian, "ВЫБРАТЬ РАКУШКУ" }, 
			}
		},


		{ "YOU WIN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "GANASTE" }, 
				{ SystemLanguage.ChineseSimplified, "你获胜啦" }, 
				{ SystemLanguage.ChineseTraditional, "你贏了" }, 
				{ SystemLanguage.French, "VOUS GAGNEZ" }, 
				{ SystemLanguage.Japanese, "勝利" }, 
				{ SystemLanguage.German, "DU GEWINNST" }, 
				{ SystemLanguage.Indonesian, "KAMU MENANG" }, 
				{ SystemLanguage.Portuguese, "VOCÊ GANHA" }, 
				{ SystemLanguage.Italian, "VINCI" }, 
				{ SystemLanguage.Korean, "당신이 승리를 거두다" }, 
				{ SystemLanguage.Russian, "ВЫ ВЫИГРЫВАЕТЕ" }, 
			}
		},


		{ "YOU'VE WON", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "HAS GANADO" }, 
				{ SystemLanguage.ChineseSimplified, "你已经获胜" }, 
				{ SystemLanguage.ChineseTraditional, "你已贏取" }, 
				{ SystemLanguage.French, "VOUS AVEZ GAGNÉ" }, 
				{ SystemLanguage.Japanese, "勝利しました" }, 
				{ SystemLanguage.German, "DU HAST GEWONNEN" }, 
				{ SystemLanguage.Indonesian, "KAMU SUDAH MENANG" }, 
				{ SystemLanguage.Portuguese, "VOCÊ GANHOU" }, 
				{ SystemLanguage.Italian, "HAI VINTO" }, 
				{ SystemLanguage.Korean, "당신은 승리를 거두었습니다" }, 
				{ SystemLanguage.Russian, "ВЫ ВЫИГРАЛИ" }, 
			}
		},


		{ "TREASURE CHEST", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COFRE DEL TESORO" }, 
				{ SystemLanguage.ChineseSimplified, "宝箱" }, 
				{ SystemLanguage.ChineseTraditional, "百寶箱" }, 
				{ SystemLanguage.French, "COFFRE AU TRÉSOR" }, 
				{ SystemLanguage.Japanese, "宝箱" }, 
				{ SystemLanguage.German, "SCHATZTRUHE" }, 
				{ SystemLanguage.Indonesian, "PETI HARTA KARUN" }, 
				{ SystemLanguage.Portuguese, "BAÚ DE TESOURO" }, 
				{ SystemLanguage.Italian, "FORZIERE DEL TESORO" }, 
				{ SystemLanguage.Korean, "보물 상자" }, 
				{ SystemLanguage.Russian, "СУНДУК С СОКРОВИЩАМИ" }, 
			}
		},


		{ "JEEP REFUELED AND READY TO ROCK THE DUNES!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMBUSTIBLE DEL TODOTERRENO LLENO Y LISTO PARA CONDUCIR" }, 
				{ SystemLanguage.ChineseSimplified, "加满油的吉普，随时启动" }, 
				{ SystemLanguage.ChineseTraditional, "吉普車已加滿油，可準備啟程" }, 
				{ SystemLanguage.French, "RAVITAILLEMENT JEEP COMPLET, PRÊT À CONDUIRE" }, 
				{ SystemLanguage.Japanese, "ジープの燃料は満タン＆走行の準備完了" }, 
				{ SystemLanguage.German, "JEEP TANK IST VOLL, BEREIT ZUM FAHREN" }, 
				{ SystemLanguage.Indonesian, "BENSIN MOBIL JIP PENUH, SIAP DIKENDARAI" }, 
				{ SystemLanguage.Portuguese, "TANQUE DO JIPE CHEIO, PRONTO PARA DIRIGIR" }, 
				{ SystemLanguage.Italian, "JEEP CON IL PIENO DI BENZINA PRONTA A PARTIRE" }, 
				{ SystemLanguage.Korean, "지프에 연료가 가득차고 드라이브할 준비가 됨" }, 
				{ SystemLanguage.Russian, "ДЖИП ЗАПРАВЛЕН И ГОТОВ К ПОЕЗДКЕ" }, 
			}
		},


		{ "HALF PIPE IS UNLOCKED SO GET YER SKATE ON!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PARQUE DE PATINAJE DESBLOQUEADO, ¡VE A PATINAR!" }, 
				{ SystemLanguage.ChineseSimplified, "停放的滑板未上锁，开始滑行吧！" }, 
				{ SystemLanguage.ChineseTraditional, "滑板公園已解鎖，去滑行吧！" }, 
				{ SystemLanguage.French, "SKATE PARK DÉBLOQUÉ, ALLEZ FAIRE DU SKATE !" }, 
				{ SystemLanguage.Japanese, "スケートボードの公園がアンロックされました、早速プレイしましょう！" }, 
				{ SystemLanguage.German, "SKATEBOARD-PARK FREIGESCHALTET, GEHE SKATEN!" }, 
				{ SystemLanguage.Indonesian, "TAMAN SKATEBOARD TERBUKA, MULAI MELUNCUR" }, 
				{ SystemLanguage.Portuguese, "PARQUE DE SKATE DESBLOQUEADO, VÁ ANDAR DE SKATE!" }, 
				{ SystemLanguage.Italian, "PARCO PER GLI SKATEBOARD SBLOCCATO, VAI A FARE SKATEBOARD!" }, 
				{ SystemLanguage.Korean, "스케이트보드 공원 잠금해제됨, 스케이트타러 가기!" }, 
				{ SystemLanguage.Russian, "ПЛОЩАДКА ДЛЯ СКЕЙТБОРДА РАЗБЛОКИРОВАНА, КАТАЙТЕСЬ!" }, 
			}
		},


		{ "TREASURE CHEST HAS BEEN UNLOCKED! CLAIM YOUR REWARD!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COFRE DEL TESORO DESBLOQUEADO. RECLAMA TU RECOMPENSA." }, 
				{ SystemLanguage.ChineseSimplified, "宝箱未上锁。上前获取奖励吧" }, 
				{ SystemLanguage.ChineseTraditional, "百寶箱已解鎖。去領取獎勵吧。" }, 
				{ SystemLanguage.French, "COFFRE AU TRÉSOR DÉVERROUILLÉ. ALLEZ RÉCLAMEZ RÉCOMPENSE." }, 
				{ SystemLanguage.Japanese, "宝箱がアンロックされました、早速中身を確認してみましょう" }, 
				{ SystemLanguage.German, "SCHATZTRUHE FREIGESCHALTET, HOL DIR DEINE BELOHNUNG." }, 
				{ SystemLanguage.Indonesian, "PETI HARTA KARUN TERBUKA, MULAI KLAIM HADIAH" }, 
				{ SystemLanguage.Portuguese, "BAÚ DO TESOURO DESBLOQUEADO. VÁ PEGAR SUA RECOMPENSA" }, 
				{ SystemLanguage.Italian, "FORZIERE DEL TESORO SBLOCCATO. VAI A RICHIEDERE IL PREMIO." }, 
				{ SystemLanguage.Korean, "보물 상자 잠금해제됨. 청구 보상하러 가기." }, 
				{ SystemLanguage.Russian, "СУНДУК С СОКРОВИЩАМИ РАЗБЛОКИРОВАН, ПОТРЕБУЙТЕ ВОЗНАГРАЖДЕНИЕ." }, 
			}
		},


		{ "ENERGY DRINKS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "BEBIDAS ENERGÉTICAS" }, 
				{ SystemLanguage.ChineseSimplified, "能量饮料" }, 
				{ SystemLanguage.ChineseTraditional, "能量飲品" }, 
				{ SystemLanguage.French, "BOISSONS ÉNERGISANTES" }, 
				{ SystemLanguage.Japanese, "エナジードリンク" }, 
				{ SystemLanguage.German, "ENERGYDRINS" }, 
				{ SystemLanguage.Indonesian, "MINUMAN BERENERGI" }, 
				{ SystemLanguage.Portuguese, "BEBIDAS ENERGÉTICAS" }, 
				{ SystemLanguage.Italian, "BEVANDE ENERGETICHE" }, 
				{ SystemLanguage.Korean, "에너지 음료" }, 
				{ SystemLanguage.Russian, "ЭНЕРГЕТИЧЕСКИЕ НАПИТКИ" }, 
			}
		},


		{ "ENERGY DRINK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "BEBIDA ENERGÉTICA" }, 
				{ SystemLanguage.ChineseSimplified, "能量饮料" }, 
				{ SystemLanguage.ChineseTraditional, "能量飲料" }, 
				{ SystemLanguage.French, "BOISSON ÉNERGISANTE" }, 
				{ SystemLanguage.Japanese, "エナジードリンク" }, 
				{ SystemLanguage.German, "ENERGYDRINK" }, 
				{ SystemLanguage.Indonesian, "MINUMAN ENERGI" }, 
				{ SystemLanguage.Portuguese, "BEBIDA ENERGÉTICA" }, 
				{ SystemLanguage.Italian, "BEVANDA ENERGETICA" }, 
				{ SystemLanguage.Korean, "에너지 드링크" }, 
				{ SystemLanguage.Russian, "ЭНЕРГЕТИЧЕСКИЙ НАПИТОК" }, 
			}
		},


		{ "BEST VALUE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MEJOR VALOR" }, 
				{ SystemLanguage.ChineseSimplified, "超值优惠" }, 
				{ SystemLanguage.ChineseTraditional, "超值" }, 
				{ SystemLanguage.French, "MEILLEURE VALEUR" }, 
				{ SystemLanguage.Japanese, "お得価格" }, 
				{ SystemLanguage.German, "BESTER NUTZEN" }, 
				{ SystemLanguage.Indonesian, "PENAWARAN TERBAIK" }, 
				{ SystemLanguage.Portuguese, "MELHOR VALOR" }, 
				{ SystemLanguage.Italian, "RISULTATO MIGLIORE" }, 
				{ SystemLanguage.Korean, "최고 가치" }, 
				{ SystemLanguage.Russian, "ЛУЧШЕЕ ЗНАЧЕНИЕ" }, 
			}
		},


		{ "SALE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "OFERTA" }, 
				{ SystemLanguage.ChineseSimplified, "出售" }, 
				{ SystemLanguage.ChineseTraditional, "促銷" }, 
				{ SystemLanguage.French, "VENTE" }, 
				{ SystemLanguage.Japanese, "セール" }, 
				{ SystemLanguage.German, "VERKAUF" }, 
				{ SystemLanguage.Indonesian, "DISKON" }, 
				{ SystemLanguage.Portuguese, "VENDA" }, 
				{ SystemLanguage.Italian, "VENDITA" }, 
				{ SystemLanguage.Korean, "판매" }, 
				{ SystemLanguage.Russian, "РАСПРОДАЖА" }, 
			}
		},


		{ "LIMITED OFFER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "OFERTA LIMITADA" }, 
				{ SystemLanguage.ChineseSimplified, "限量折扣" }, 
				{ SystemLanguage.ChineseTraditional, "限量優惠" }, 
				{ SystemLanguage.French, "OFFRE LIMITÉE" }, 
				{ SystemLanguage.Japanese, "期間限定" }, 
				{ SystemLanguage.German, "BEGRENZTES ANGEBOT" }, 
				{ SystemLanguage.Indonesian, "PENAWARAN TERBATAS" }, 
				{ SystemLanguage.Portuguese, "OFERTA LIMITADA" }, 
				{ SystemLanguage.Italian, "OFFERTA LIMITATA" }, 
				{ SystemLanguage.Korean, "한정 제공" }, 
				{ SystemLanguage.Russian, "ОГРАНИЧЕННОЕ ПРЕДЛОЖЕНИЕ" }, 
			}
		},


		{ "THANK YOU", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "GRACIAS" }, 
				{ SystemLanguage.ChineseSimplified, "谢谢你" }, 
				{ SystemLanguage.ChineseTraditional, "謝謝" }, 
				{ SystemLanguage.French, "MERCI" }, 
				{ SystemLanguage.Japanese, "ありがとうございます" }, 
				{ SystemLanguage.German, "DANKE DIR" }, 
				{ SystemLanguage.Indonesian, "TERIMA KASIH" }, 
				{ SystemLanguage.Portuguese, "OBRIGADO" }, 
				{ SystemLanguage.Italian, "GRAZIE" }, 
				{ SystemLanguage.Korean, "고맙습니다" }, 
				{ SystemLanguage.Russian, "СПАСИБО" }, 
			}
		},


		{ "PURCHASE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPRA" }, 
				{ SystemLanguage.ChineseSimplified, "购" }, 
				{ SystemLanguage.ChineseTraditional, "買" }, 
				{ SystemLanguage.French, "ACHAT" }, 
				{ SystemLanguage.Japanese, "購入" }, 
				{ SystemLanguage.German, "KAUF" }, 
				{ SystemLanguage.Indonesian, "MEMBELI" }, 
				{ SystemLanguage.Portuguese, "COMPRAR" }, 
				{ SystemLanguage.Italian, "ACQUISTO" }, 
				{ SystemLanguage.Korean, "구매가" }, 
				{ SystemLanguage.Russian, "ПОКУПКА" }, 
			}
		},


		{ "PURCHASE COMPLETE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPRA COMPLETADA" }, 
				{ SystemLanguage.ChineseSimplified, "购买已完成" }, 
				{ SystemLanguage.ChineseTraditional, "購買完成" }, 
				{ SystemLanguage.French, "ACHAT COMPLET" }, 
				{ SystemLanguage.Japanese, "購入が完了しました" }, 
				{ SystemLanguage.German, "KAUF ABGESCHLOSSEN" }, 
				{ SystemLanguage.Indonesian, "PEMBAYARAN SELESAI" }, 
				{ SystemLanguage.Portuguese, "COMPRA COMPLETA" }, 
				{ SystemLanguage.Italian, "ACQUISTO COMPLETATO" }, 
				{ SystemLanguage.Korean, "구매가 완료됨" }, 
				{ SystemLanguage.Russian, "ПОКУПКА ЗАВЕРШЕНА" }, 
			}
		},


		{ "PURCHASE COMPLETED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPRA COMPLETADA" }, 
				{ SystemLanguage.ChineseSimplified, "购买已完成" }, 
				{ SystemLanguage.ChineseTraditional, "購買完成" }, 
				{ SystemLanguage.French, "ACHAT COMPLET" }, 
				{ SystemLanguage.Japanese, "購入が完了しました" }, 
				{ SystemLanguage.German, "KAUF ABGESCHLOSSEN" }, 
				{ SystemLanguage.Indonesian, "PEMBAYARAN SELESAI" }, 
				{ SystemLanguage.Portuguese, "COMPRA COMPLETA" }, 
				{ SystemLanguage.Italian, "ACQUISTO COMPLETATO" }, 
				{ SystemLanguage.Korean, "구매가 완료됨" }, 
				{ SystemLanguage.Russian, "ПОКУПКА ЗАВЕРШЕНА" }, 
			}
		},


		{ "PURCHASE FAILED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPRA FALLIDA" }, 
				{ SystemLanguage.ChineseSimplified, "购买失败" }, 
				{ SystemLanguage.ChineseTraditional, "購買失敗" }, 
				{ SystemLanguage.French, "ACHAT RATÉ" }, 
				{ SystemLanguage.Japanese, "購入失敗" }, 
				{ SystemLanguage.German, "KAUF GESCHEITERT" }, 
				{ SystemLanguage.Indonesian, "PEMBAYARAN GAGAL" }, 
				{ SystemLanguage.Portuguese, "FALHA NA COMPRA" }, 
				{ SystemLanguage.Italian, "ACQUISTO FALLITO" }, 
				{ SystemLanguage.Korean, "구매 실패" }, 
				{ SystemLanguage.Russian, "ПОКУПКА НЕ УДАЛАСЬ" }, 
			}
		},


		{ "NOT ENOUGH COINS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "NO TIENES SUFICIENTES MONEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "金币不足" }, 
				{ SystemLanguage.ChineseTraditional, "不夠金幣" }, 
				{ SystemLanguage.French, "PAS ASSEZ DE PIÈCES" }, 
				{ SystemLanguage.Japanese, "コインが足りません" }, 
				{ SystemLanguage.German, "NICHT GENUG MÜNZEN" }, 
				{ SystemLanguage.Indonesian, "KOIN TIDAK CUKUP" }, 
				{ SystemLanguage.Portuguese, "MOEDAS INSUFICIENTES" }, 
				{ SystemLanguage.Italian, "MONETE NON SUFFICIENTI" }, 
				{ SystemLanguage.Korean, "동전이 충분하지 않습니다" }, 
				{ SystemLanguage.Russian, "НЕДОСТАТОЧНО МОНЕТ" }, 
			}
		},


		{ "COIN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MONEDA" }, 
				{ SystemLanguage.ChineseSimplified, "金币" }, 
				{ SystemLanguage.ChineseTraditional, "金幣" }, 
				{ SystemLanguage.French, "PIÈCE" }, 
				{ SystemLanguage.Japanese, "コイン" }, 
				{ SystemLanguage.German, "MÜNZE" }, 
				{ SystemLanguage.Indonesian, "KOIN" }, 
				{ SystemLanguage.Portuguese, "MOEDA" }, 
				{ SystemLanguage.Italian, "MONETA" }, 
				{ SystemLanguage.Korean, "동전" }, 
				{ SystemLanguage.Russian, "МОНЕТА" }, 
			}
		},


		{ "GET THE MISSING", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "OBTÉN LAS MONEDAS RESTANTES" }, 
				{ SystemLanguage.ChineseSimplified, "获取丢失的金币" }, 
				{ SystemLanguage.ChineseTraditional, "獲取丟失的金幣" }, 
				{ SystemLanguage.French, "OBTENIR LES PIÈCES MANQUANTES" }, 
				{ SystemLanguage.Japanese, "コインを獲得" }, 
				{ SystemLanguage.German, "HOL DIR DIE FEHLENDEN MÜNZEN" }, 
				{ SystemLanguage.Indonesian, "DAPATKAN KOIN YANG HILANG" }, 
				{ SystemLanguage.Portuguese, "PEGUE AS MOEDAS QUE FALTAM" }, 
				{ SystemLanguage.Italian, "OTTIENI LE MONETE MANCANTI" }, 
				{ SystemLanguage.Korean, "누락된 동전을 얻기" }, 
				{ SystemLanguage.Russian, "ПОЛУЧИТЬ НЕДОСТАЮЩИЕ МОНЕТЫ" }, 
			}
		},


		{ "EQUIP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EQUIPAR" }, 
				{ SystemLanguage.ChineseSimplified, "装备" }, 
				{ SystemLanguage.ChineseTraditional, "裝備" }, 
				{ SystemLanguage.French, "ÉQUIPEZ" }, 
				{ SystemLanguage.Japanese, "装着する" }, 
				{ SystemLanguage.German, "AUSSTATTUNG" }, 
				{ SystemLanguage.Indonesian, "MELENGKAPI" }, 
				{ SystemLanguage.Portuguese, "VISTA-SE" }, 
				{ SystemLanguage.Italian, "INDOSSA" }, 
				{ SystemLanguage.Korean, "갖추기" }, 
				{ SystemLanguage.Russian, "ОДЕТЬ" }, 
			}
		},


		{ "SELECT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SELECCIONAR" }, 
				{ SystemLanguage.ChineseSimplified, "选择" }, 
				{ SystemLanguage.ChineseTraditional, "選擇" }, 
				{ SystemLanguage.French, "SÉLECTIONNEZ" }, 
				{ SystemLanguage.Japanese, "選択" }, 
				{ SystemLanguage.German, "AUSWÄHLEN" }, 
				{ SystemLanguage.Indonesian, "MEMILIH" }, 
				{ SystemLanguage.Portuguese, "ESCOLHA" }, 
				{ SystemLanguage.Italian, "SELEZIONA" }, 
				{ SystemLanguage.Korean, "고르기" }, 
				{ SystemLanguage.Russian, "ВЫБРАТЬ" }, 
			}
		},


		{ "SHARK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TIBURÓN" }, 
				{ SystemLanguage.ChineseSimplified, "鲨鱼" }, 
				{ SystemLanguage.ChineseTraditional, "鯊魚" }, 
				{ SystemLanguage.French, "REQUIN" }, 
				{ SystemLanguage.Japanese, "鮫" }, 
				{ SystemLanguage.German, "HAI" }, 
				{ SystemLanguage.Indonesian, "HIU" }, 
				{ SystemLanguage.Portuguese, "TUBARÃO" }, 
				{ SystemLanguage.Italian, "SQUALO" }, 
				{ SystemLanguage.Korean, "상어" }, 
				{ SystemLanguage.Russian, "АКУЛА" }, 
			}
		},


		{ "GOAL COMPLETED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "META COMPLETADA" }, 
				{ SystemLanguage.ChineseSimplified, "目标已完成" }, 
				{ SystemLanguage.ChineseTraditional, "完成目標" }, 
				{ SystemLanguage.French, "OBJECTIF TERMINÉ" }, 
				{ SystemLanguage.Japanese, "ゴールしました" }, 
				{ SystemLanguage.German, "ZIEL ERREICHT" }, 
				{ SystemLanguage.Indonesian, "TARGET TERCAPAI" }, 
				{ SystemLanguage.Portuguese, "OBJETIVO ATINGIDO" }, 
				{ SystemLanguage.Italian, "OBIETTIVO RAGGIUNTO" }, 
				{ SystemLanguage.Korean, "목표 완료됨" }, 
				{ SystemLanguage.Russian, "ЦЕЛЬ ДОСТИГНУТА" }, 
			}
		},


		{ "DOUBLE COINS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DOBLE MONEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "双倍金币" }, 
				{ SystemLanguage.ChineseTraditional, "倍增金幣" }, 
				{ SystemLanguage.French, "DOUBLER LES PIÈCES" }, 
				{ SystemLanguage.Japanese, "ダブルコイン" }, 
				{ SystemLanguage.German, "DOPPELTE MÜNZEN" }, 
				{ SystemLanguage.Indonesian, "KOIN GANDA" }, 
				{ SystemLanguage.Portuguese, "MOEDAS EM DOBRO" }, 
				{ SystemLanguage.Italian, "RADDOPPIA LE MONETE" }, 
				{ SystemLanguage.Korean, "동전 두배로 늘리기" }, 
				{ SystemLanguage.Russian, "УДВОИТЬ МОНЕТЫ" }, 
			}
		},


		{ "MULTIPLY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MULTIPLICAR" }, 
				{ SystemLanguage.ChineseSimplified, "增殖" }, 
				{ SystemLanguage.ChineseTraditional, "加倍" }, 
				{ SystemLanguage.French, "MULTIPLIER" }, 
				{ SystemLanguage.Japanese, "倍増" }, 
				{ SystemLanguage.German, "VERVIELFACHEN" }, 
				{ SystemLanguage.Indonesian, "MEMPERBANYAK" }, 
				{ SystemLanguage.Portuguese, "MULTIPLICAR" }, 
				{ SystemLanguage.Italian, "MOLTIPLICA" }, 
				{ SystemLanguage.Korean, "곱하기" }, 
				{ SystemLanguage.Russian, "ПРИУМНОЖИТЬ" }, 
			}
		},


		{ "CURRENT GOALS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "METAS ACTUALES" }, 
				{ SystemLanguage.ChineseSimplified, "当前目标" }, 
				{ SystemLanguage.ChineseTraditional, "目前的目標" }, 
				{ SystemLanguage.French, "OBJECTIFS ACTUELS" }, 
				{ SystemLanguage.Japanese, "達成すべきゴール" }, 
				{ SystemLanguage.German, "AKTUELLE ZIELE" }, 
				{ SystemLanguage.Indonesian, "TARGET SAAT INI" }, 
				{ SystemLanguage.Portuguese, "OBJETIVOS ATUAIS" }, 
				{ SystemLanguage.Italian, "MISSIONI IN CORSO" }, 
				{ SystemLanguage.Korean, "현재 목표" }, 
				{ SystemLanguage.Russian, "ТЕКУЩИЕ ЦЕЛИ" }, 
			}
		},


		{ "DONE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LISTO" }, 
				{ SystemLanguage.ChineseSimplified, "已完成" }, 
				{ SystemLanguage.ChineseTraditional, "已完成" }, 
				{ SystemLanguage.French, "TERMINÉ" }, 
				{ SystemLanguage.Japanese, "完了" }, 
				{ SystemLanguage.German, "FERTIG" }, 
				{ SystemLanguage.Indonesian, "SELESAI" }, 
				{ SystemLanguage.Portuguese, "FEITO" }, 
				{ SystemLanguage.Italian, "FATTO" }, 
				{ SystemLanguage.Korean, "완료" }, 
				{ SystemLanguage.Russian, "ГОТОВО" }, 
			}
		},


		{ "COMPLETED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPLETADO" }, 
				{ SystemLanguage.ChineseSimplified, "已达成" }, 
				{ SystemLanguage.ChineseTraditional, "已完成" }, 
				{ SystemLanguage.French, "PASSEZ L’OBJECTIF" }, 
				{ SystemLanguage.Japanese, "完了" }, 
				{ SystemLanguage.German, "ABGESCHLOSSEN" }, 
				{ SystemLanguage.Indonesian, "TERCAPAI" }, 
				{ SystemLanguage.Portuguese, "COMPLETO" }, 
				{ SystemLanguage.Italian, "COMPLETATO" }, 
				{ SystemLanguage.Korean, "완료됨" }, 
				{ SystemLanguage.Russian, "ЗАВЕРШЕНО" }, 
			}
		},


		{ "SKIP GOAL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SALTAR META" }, 
				{ SystemLanguage.ChineseSimplified, "跳跃目标" }, 
				{ SystemLanguage.ChineseTraditional, "略過目標" }, 
				{ SystemLanguage.French, "PASSEZ L’OBJECTIF" }, 
				{ SystemLanguage.Japanese, "ゴールをスキップ" }, 
				{ SystemLanguage.German, "ZIELE ÜBERSPRINGEN" }, 
				{ SystemLanguage.Indonesian, "LEWATI TARGET" }, 
				{ SystemLanguage.Portuguese, "PULAR OBJETIVO" }, 
				{ SystemLanguage.Italian, "SALTA MISSIONE" }, 
				{ SystemLanguage.Korean, "목표 건너뛰기" }, 
				{ SystemLanguage.Russian, "ПРОПУСТИТЬ ЦЕЛЬ" }, 
			}
		},


		{ "PLAY A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JUGAR UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "玩1个游戏" }, 
				{ SystemLanguage.ChineseTraditional, "進行1個遊戲" }, 
				{ SystemLanguage.French, "JOUER À UN JEU" }, 
				{ SystemLanguage.Japanese, "ゲームをプレイ" }, 
				{ SystemLanguage.German, "SPIELE EIN SPIEL" }, 
				{ SystemLanguage.Indonesian, "BERMAIN  SEBUAH PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "JOGAR UM JOGO" }, 
				{ SystemLanguage.Italian, "INIZIA A GIOCARE" }, 
				{ SystemLanguage.Korean, "게임을 하기" }, 
				{ SystemLanguage.Russian, "СЫГРАТЬ ОДНУ ИГРУ" }, 
			}
		},


		{ "PIER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MUELLE" }, 
				{ SystemLanguage.ChineseSimplified, "桥墩" }, 
				{ SystemLanguage.ChineseTraditional, "碼頭" }, 
				{ SystemLanguage.French, "JETÉE" }, 
				{ SystemLanguage.Japanese, "桟橋" }, 
				{ SystemLanguage.German, "HOLZSTEG" }, 
				{ SystemLanguage.Indonesian, "DERMAGA" }, 
				{ SystemLanguage.Portuguese, "PIER" }, 
				{ SystemLanguage.Italian, "MOLO" }, 
				{ SystemLanguage.Korean, "나무 교각" }, 
				{ SystemLanguage.Russian, "ПРИЧАЛ" }, 
			}
		},


		{ "RAIL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RIEL" }, 
				{ SystemLanguage.ChineseSimplified, "轨道" }, 
				{ SystemLanguage.ChineseTraditional, "軌道" }, 
				{ SystemLanguage.French, "RAIL" }, 
				{ SystemLanguage.Japanese, "レール" }, 
				{ SystemLanguage.German, "BAHN" }, 
				{ SystemLanguage.Indonesian, "LINTASAN" }, 
				{ SystemLanguage.Portuguese, "PISTA" }, 
				{ SystemLanguage.Italian, "CORRIMANO" }, 
				{ SystemLanguage.Korean, "레일" }, 
				{ SystemLanguage.Russian, "РЕЛЬС" }, 
			}
		},


		{ "BRIDGE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUENTE" }, 
				{ SystemLanguage.ChineseSimplified, "桥" }, 
				{ SystemLanguage.ChineseTraditional, "橋梁" }, 
				{ SystemLanguage.French, "PONT" }, 
				{ SystemLanguage.Japanese, "橋" }, 
				{ SystemLanguage.German, "BRÜCKE" }, 
				{ SystemLanguage.Indonesian, "JEMBATAN" }, 
				{ SystemLanguage.Portuguese, "PONTE" }, 
				{ SystemLanguage.Italian, "PONTE" }, 
				{ SystemLanguage.Korean, "다리" }, 
				{ SystemLanguage.Russian, "МОСТ" }, 
			}
		},


		{ "SHIP WRECK BRIDGE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUENTE DE NAUFRAGIO" }, 
				{ SystemLanguage.ChineseSimplified, "沉船桥" }, 
				{ SystemLanguage.ChineseTraditional, "沈船橋" }, 
				{ SystemLanguage.French, "PONT D’ÉPAVE DE BATEAU" }, 
				{ SystemLanguage.Japanese, "廃船の橋" }, 
				{ SystemLanguage.German, "SCHIFFSWRACKBRÜCKE" }, 
				{ SystemLanguage.Indonesian, "JEMBATAN KAPAL RUSAK" }, 
				{ SystemLanguage.Portuguese, "PONTE NAUFRAGADA" }, 
				{ SystemLanguage.Italian, "PONTE DEL RELITTO" }, 
				{ SystemLanguage.Korean, "난파선 다리" }, 
				{ SystemLanguage.Russian, "МОСТ С КОРАБЛЕКРУШЕНИЯМИ" }, 
			}
		},


		{ "MAIN BEACH", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PLAYA" }, 
				{ SystemLanguage.ChineseSimplified, "海滩" }, 
				{ SystemLanguage.ChineseTraditional, "海灘" }, 
				{ SystemLanguage.French, "PLAGE" }, 
				{ SystemLanguage.Japanese, "ビーチ" }, 
				{ SystemLanguage.German, "STRAND" }, 
				{ SystemLanguage.Indonesian, "PANTAI" }, 
				{ SystemLanguage.Portuguese, "PRAIA" }, 
				{ SystemLanguage.Italian, "SPIAGGIA" }, 
				{ SystemLanguage.Korean, "바닷가" }, 
				{ SystemLanguage.Russian, "ПЛЯЖ" }, 
			}
		},


		{ "WAVE PARK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PARQUE ACUÁTICO" }, 
				{ SystemLanguage.ChineseSimplified, "水上公园" }, 
				{ SystemLanguage.ChineseTraditional, "水上樂園" }, 
				{ SystemLanguage.French, "PARC AQUATIQUE" }, 
				{ SystemLanguage.Japanese, "ウォーターパーク" }, 
				{ SystemLanguage.German, "WASSERPARK" }, 
				{ SystemLanguage.Indonesian, "TAMAN OMBAK" }, 
				{ SystemLanguage.Portuguese, "PARQUE DE ONDAS" }, 
				{ SystemLanguage.Italian, "PARCO ACQUATICO" }, 
				{ SystemLanguage.Korean, "물결 공원" }, 
				{ SystemLanguage.Russian, "ВОДНЫЙ ПАРК" }, 
			}
		},


		{ "TIKI BAY", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "BAHÍA TIKI" }, 
				{ SystemLanguage.ChineseSimplified, "提基湾" }, 
				{ SystemLanguage.ChineseTraditional, "提基海灣" }, 
				{ SystemLanguage.French, "BAIE DE TIKI" }, 
				{ SystemLanguage.Japanese, "ハワイアンベイ" }, 
				{ SystemLanguage.German, "TIKI BAY" }, 
				{ SystemLanguage.Indonesian, "TIKI BAY" }, 
				{ SystemLanguage.Portuguese, "BAÍA TIKI" }, 
				{ SystemLanguage.Italian, "BAIA DI TIKI" }, 
				{ SystemLanguage.Korean, "티키 베이" }, 
				{ SystemLanguage.Russian, "БУХТА ТИКИ" }, 
			}
		},


		{ "CAVE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CUEVA" }, 
				{ SystemLanguage.ChineseSimplified, "洞穴" }, 
				{ SystemLanguage.ChineseTraditional, "洞穴" }, 
				{ SystemLanguage.French, "CAVE" }, 
				{ SystemLanguage.Japanese, "洞窟" }, 
				{ SystemLanguage.German, "HÖHLE" }, 
				{ SystemLanguage.Indonesian, "GOA" }, 
				{ SystemLanguage.Portuguese, "CAVERNA" }, 
				{ SystemLanguage.Italian, "CAVERNA" }, 
				{ SystemLanguage.Korean, "동굴" }, 
				{ SystemLanguage.Russian, "ПЕЩЕРА" }, 
			}
		},


		{ "SHIP WRECK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "NAUFRAGIO" }, 
				{ SystemLanguage.ChineseSimplified, "沉船" }, 
				{ SystemLanguage.ChineseTraditional, "沈船" }, 
				{ SystemLanguage.French, "ÉPAVE DE NAVIRE" }, 
				{ SystemLanguage.Japanese, "廃船" }, 
				{ SystemLanguage.German, "SCHIFFSWRACK" }, 
				{ SystemLanguage.Indonesian, "KECELAKAAN KAPAL" }, 
				{ SystemLanguage.Portuguese, "PONTE NAUFRAGADA" }, 
				{ SystemLanguage.Italian, "RELITTO" }, 
				{ SystemLanguage.Korean, "난파선" }, 
				{ SystemLanguage.Russian, "КОРАБЛЕКРУШЕНИЕ" }, 
			}
		},


		{ "HARBOR", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PUERTO" }, 
				{ SystemLanguage.ChineseSimplified, "港口" }, 
				{ SystemLanguage.ChineseTraditional, "港口" }, 
				{ SystemLanguage.French, "PORT" }, 
				{ SystemLanguage.Japanese, "湾" }, 
				{ SystemLanguage.German, "HAFEN" }, 
				{ SystemLanguage.Indonesian, "PELABUHAN" }, 
				{ SystemLanguage.Portuguese, "PORTO" }, 
				{ SystemLanguage.Italian, "DARSENA" }, 
				{ SystemLanguage.Korean, "항구" }, 
				{ SystemLanguage.Russian, "ГАВАНЬ" }, 
			}
		},


		{ "RESTORE PURCHASES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REESTABLECER COMPRA" }, 
				{ SystemLanguage.ChineseSimplified, "恢复购买" }, 
				{ SystemLanguage.ChineseTraditional, "復原購買" }, 
				{ SystemLanguage.French, "RESTAURER LES ACHATS" }, 
				{ SystemLanguage.Japanese, "購入をリストア" }, 
				{ SystemLanguage.German, "KÄUFE WIEDERHERSTELLEN" }, 
				{ SystemLanguage.Indonesian, "PEMBAYARAN DIKEMBALIKAN" }, 
				{ SystemLanguage.Portuguese, "RESTAURAR COMPRAS" }, 
				{ SystemLanguage.Italian, "RIPRISTINA ACQUISTI" }, 
				{ SystemLanguage.Korean, "구매 복원하기" }, 
				{ SystemLanguage.Russian, "ВОССТАНОВИТЬ ПОКУПКИ" }, 
			}
		},


		{ "GOALS COMPLETE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "METAS COMPLETAS" }, 
				{ SystemLanguage.ChineseSimplified, "已完成目标" }, 
				{ SystemLanguage.ChineseTraditional, "目標完成" }, 
				{ SystemLanguage.French, "BUTS COMPLETS" }, 
				{ SystemLanguage.Japanese, "ゴール達成" }, 
				{ SystemLanguage.German, "ZIELE ABGESCHLOSSEN" }, 
				{ SystemLanguage.Indonesian, "TARGET BERHASIL" }, 
				{ SystemLanguage.Portuguese, "OBJETIVOS COMPLETOS" }, 
				{ SystemLanguage.Italian, "MISSIONI COMPLETATE" }, 
				{ SystemLanguage.Korean, "목표 왼료됨" }, 
				{ SystemLanguage.Russian, "ЦЕЛИ ДОСТИГНУТЫ" }, 
			}
		},


		{ "CONTACTING SERVER...", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONTACTAR CON EL SERVIDOR" }, 
				{ SystemLanguage.ChineseSimplified, "连接服务" }, 
				{ SystemLanguage.ChineseTraditional, "正在連接伺服器" }, 
				{ SystemLanguage.French, "CONTACTANT LE SERVEUR" }, 
				{ SystemLanguage.Japanese, "サーバーの応答を待っています" }, 
				{ SystemLanguage.German, "KONTAKTIERE SERVER" }, 
				{ SystemLanguage.Indonesian, "MENGHUBUNGI SERVER" }, 
				{ SystemLanguage.Portuguese, "CONTATANDO SERVIDOR" }, 
				{ SystemLanguage.Italian, "CONTATTANDO IL SERVER" }, 
				{ SystemLanguage.Korean, "서버에 접속중입니다" }, 
				{ SystemLanguage.Russian, "ИДЕТ ПОДКЛЮЧЕНИЕ К СЕРВЕРУ" }, 
			}
		},


		{ "CONTACTING SERVER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONTACTAR CON EL SERVIDOR" }, 
				{ SystemLanguage.ChineseSimplified, "连接服务" }, 
				{ SystemLanguage.ChineseTraditional, "正在連接伺服器" }, 
				{ SystemLanguage.French, "CONTACTANT LE SERVEUR" }, 
				{ SystemLanguage.Japanese, "サーバーの応答を待っています" }, 
				{ SystemLanguage.German, "KONTAKTIERE SERVER" }, 
				{ SystemLanguage.Indonesian, "MENGHUBUNGI SERVER" }, 
				{ SystemLanguage.Portuguese, "CONTATANDO SERVIDOR" }, 
				{ SystemLanguage.Italian, "CONTATTANDO IL SERVER" }, 
				{ SystemLanguage.Korean, "서버에 접속중입니다" }, 
				{ SystemLanguage.Russian, "ИДЕТ ПОДКЛЮЧЕНИЕ К СЕРВЕРУ" }, 
			}
		},


		{ "INTERNET CONNECTION REQUIRED TO MAKE PURCHASES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Se requiere conexión a internet para realizar la compra" }, 
				{ SystemLanguage.ChineseSimplified, "互联网连接要求购买" }, 
				{ SystemLanguage.ChineseTraditional, "購買時需要連接網絡" }, 
				{ SystemLanguage.French, "Une connexion Internet requise pour faire des achats" }, 
				{ SystemLanguage.Japanese, "購入にはインターネットが必要です" }, 
				{ SystemLanguage.German, "Internet-Verbindung nötig für Käufe" }, 
				{ SystemLanguage.Indonesian, "Dibutuhkan koneksi interney untuk melakukan  pembayaran" }, 
				{ SystemLanguage.Portuguese, "Conexão com Internet é necessária para realizar compras" }, 
				{ SystemLanguage.Italian, "Richiesta connessione internet per effettuare gli acquisti" }, 
				{ SystemLanguage.Korean, "구매를 위해서 인터넷 연결이 필요합니다" }, 
				{ SystemLanguage.Russian, "Чтобы делать покупки, необходимо подключение к интернету" }, 
			}
		},


		{ "PURCHASE ATTEMPT UNSUCCESSFUL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Intento de compra fallido" }, 
				{ SystemLanguage.ChineseSimplified, "购买尝试失败" }, 
				{ SystemLanguage.ChineseTraditional, "購買不成功" }, 
				{ SystemLanguage.French, "Tentative d'achat échouée" }, 
				{ SystemLanguage.Japanese, "購入が完了しました" }, 
				{ SystemLanguage.German, "Kaufversuch fehlgeschlagen" }, 
				{ SystemLanguage.Indonesian, "Upaya pembayaran gagal" }, 
				{ SystemLanguage.Portuguese, "Tentativa de compra mal sucedida" }, 
				{ SystemLanguage.Italian, "Tentativo di acquisto non riuscito" }, 
				{ SystemLanguage.Korean, "구매 시도가 실패했습니다" }, 
				{ SystemLanguage.Russian, "Покупка не удалась" }, 
			}
		},


		{ "PURCHASE CANCELLED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Compra cancelada" }, 
				{ SystemLanguage.ChineseSimplified, "购买已取消" }, 
				{ SystemLanguage.ChineseTraditional, "取消購買" }, 
				{ SystemLanguage.French, "Achat annulé" }, 
				{ SystemLanguage.Japanese, "購入が中止されました" }, 
				{ SystemLanguage.German, "Kauf abgebrochen" }, 
				{ SystemLanguage.Indonesian, "PEMBAYARAN BATAL" }, 
				{ SystemLanguage.Portuguese, "Compra cancelada" }, 
				{ SystemLanguage.Italian, "Acquisto annullato" }, 
				{ SystemLanguage.Korean, "구매가 취소되었습니다" }, 
				{ SystemLanguage.Russian, "Покупка отменена" }, 
			}
		},


		{ "RESTORE COMPLETE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Recuperar por completo" }, 
				{ SystemLanguage.ChineseSimplified, "恢复完成" }, 
				{ SystemLanguage.ChineseTraditional, "復原成功" }, 
				{ SystemLanguage.French, "restauration complète" }, 
				{ SystemLanguage.Japanese, "リストア完了" }, 
				{ SystemLanguage.German, "Wiederherstellung vollendet" }, 
				{ SystemLanguage.Indonesian, "PENGEMBALIAN KOMPLIT" }, 
				{ SystemLanguage.Portuguese, "Restauração completada" }, 
				{ SystemLanguage.Italian, "Ripristino completo" }, 
				{ SystemLanguage.Korean, "복원을 왼료했습니다" }, 
				{ SystemLanguage.Russian, "Восстановление завершено" }, 
			}
		},


		{ "RESTORE FAILED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Recuperación fallida" }, 
				{ SystemLanguage.ChineseSimplified, "恢复失败" }, 
				{ SystemLanguage.ChineseTraditional, "復原失敗" }, 
				{ SystemLanguage.French, "échec de la restauration" }, 
				{ SystemLanguage.Japanese, "リストア失敗" }, 
				{ SystemLanguage.German, "Wiederherstellung fehlgeschlagen" }, 
				{ SystemLanguage.Indonesian, "PENGEMBALIAN GAGAL" }, 
				{ SystemLanguage.Portuguese, "Falha na restauração" }, 
				{ SystemLanguage.Italian, "Ripristino fallito" }, 
				{ SystemLanguage.Korean, "복원에 실패했습니다" }, 
				{ SystemLanguage.Russian, "Сбой восстановления" }, 
			}
		},


		{ "UPGRADE COST", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ACTUALIZAR COSTES" }, 
				{ SystemLanguage.ChineseSimplified, "升级话费" }, 
				{ SystemLanguage.ChineseTraditional, "升級費用" }, 
				{ SystemLanguage.French, "AMÉLIORATION DU COÛT" }, 
				{ SystemLanguage.Japanese, "アップグレード費用" }, 
				{ SystemLanguage.German, "UPGRADE-KOSTEN" }, 
				{ SystemLanguage.Indonesian, "BIAYA UPGRADE" }, 
				{ SystemLanguage.Portuguese, "VALOR DO UPGRADE" }, 
				{ SystemLanguage.Italian, "COSTO AGGIORNAMENTO" }, 
				{ SystemLanguage.Korean, "업그레이드 비용" }, 
				{ SystemLanguage.Russian, "СТОИМОСТЬ ОБНОВЛЕНИЯ" }, 
			}
		},


		{ "MAGNET DURATION", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DURACIÓN DE LA MONEDA MAGNÉTICA" }, 
				{ SystemLanguage.ChineseSimplified, "磁铁持续时间" }, 
				{ SystemLanguage.ChineseTraditional, "磁铁持续时间" }, 
				{ SystemLanguage.French, "DURÉE D'AIMANT" }, 
				{ SystemLanguage.Japanese, "磁石の効果持続時間" }, 
				{ SystemLanguage.German, "MAGNETDAUER" }, 
				{ SystemLanguage.Indonesian, "DURASI MAGNET" }, 
				{ SystemLanguage.Portuguese, "DURAÇÃO DO MAGNETO" }, 
				{ SystemLanguage.Italian, "DURATA CALAMITA" }, 
				{ SystemLanguage.Korean, "자석 지속기간" }, 
				{ SystemLanguage.Russian, "СРОК ДЕЙСТВИЯ МАГНИТА" }, 
			}
		},


		{ "EXTENDS DURATION OF COIN MAGNET.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EXTENDER DURACIÓN DE LAS MONEDAS MAGNÉTICAS" }, 
				{ SystemLanguage.ChineseSimplified, "延长金币磁铁的持续时间" }, 
				{ SystemLanguage.ChineseTraditional, "延長金幣磁鐵時效" }, 
				{ SystemLanguage.French, "ETEND LA DURÉE DE L'AIMANT DES PIÈCES ." }, 
				{ SystemLanguage.Japanese, "コイン磁石の持続時間を伸ばす" }, 
				{ SystemLanguage.German, "ERWEITERT DAUER DES MÜNZMAGNETEN." }, 
				{ SystemLanguage.Indonesian, "MEMPERPANJANG DURASI MAGNET KOIN" }, 
				{ SystemLanguage.Portuguese, "ESTENDE DURAÇÃO DA MOEDA DE MAGNETO" }, 
				{ SystemLanguage.Italian, "AUMENTA LA DURATA DELLA MONETA CALAMITA" }, 
				{ SystemLanguage.Korean, "동전 자석의 지속 기간을 연장합니다." }, 
				{ SystemLanguage.Russian, "ПРОДЛЕВАЕТ СРОК ДЕЙСТВИЯ МАГНИТА МОНЕТ." }, 
			}
		},


		{ "DOUBLE COINS FOREVER.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DOBLE DE MONEDAS PARA SIEMPRE" }, 
				{ SystemLanguage.ChineseSimplified, "永久性双倍金币" }, 
				{ SystemLanguage.ChineseTraditional, "永遠雙倍賺取金幣" }, 
				{ SystemLanguage.French, "DOUBLAGE DES PIÈCES POUR TOUJOURS." }, 
				{ SystemLanguage.Japanese, "永続コイン2倍化" }, 
				{ SystemLanguage.German, "DOPPELTE MÜNZEN FÜR IMMER." }, 
				{ SystemLanguage.Indonesian, "KOIN GANDA SELAMANYA" }, 
				{ SystemLanguage.Portuguese, "MOEDAS EM DOBRO PARA SEMPRE" }, 
				{ SystemLanguage.Italian, "RADDOPPIA LE MONETE PER SEMPRE" }, 
				{ SystemLanguage.Korean, "동전을 두배로 영구히 증가하기" }, 
				{ SystemLanguage.Russian, "УДВОИТЬ КОЛИЧЕСТВО МОНЕТ НАВСЕГДА." }, 
			}
		},


		{ "COIN X2", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Monedas x 2" }, 
				{ SystemLanguage.ChineseSimplified, "金币 x2" }, 
				{ SystemLanguage.ChineseTraditional, "金幣x2" }, 
				{ SystemLanguage.French, "Coin x2" }, 
				{ SystemLanguage.Japanese, "コイン×2" }, 
				{ SystemLanguage.German, "Münzen x2" }, 
				{ SystemLanguage.Indonesian, "Koin x2" }, 
				{ SystemLanguage.Portuguese, "Moedas x2" }, 
				{ SystemLanguage.Italian, "Moneta x2" }, 
				{ SystemLanguage.Korean, "동전  x2" }, 
				{ SystemLanguage.Russian, "Монеты х 2" }, 
			}
		},


		{ "SCORE X2", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Puntuación x 2" }, 
				{ SystemLanguage.ChineseSimplified, "得分 x2" }, 
				{ SystemLanguage.ChineseTraditional, "分數x2" }, 
				{ SystemLanguage.French, "Score x2" }, 
				{ SystemLanguage.Japanese, "スコア×2" }, 
				{ SystemLanguage.German, "Punktzahl x2" }, 
				{ SystemLanguage.Indonesian, "Nilai x2" }, 
				{ SystemLanguage.Portuguese, "Placar x2" }, 
				{ SystemLanguage.Italian, "Punteggio x2" }, 
				{ SystemLanguage.Korean, "점수  x2" }, 
				{ SystemLanguage.Russian, "Очки х 2" }, 
			}
		},


		{ "THRUSTER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TABLA DE SURF PROFESIONAL" }, 
				{ SystemLanguage.ChineseSimplified, "推进器" }, 
				{ SystemLanguage.ChineseTraditional, "推進器沖浪板" }, 
				{ SystemLanguage.French, "Poussoir" }, 
				{ SystemLanguage.Japanese, "プロ用サーフボード" }, 
				{ SystemLanguage.German, "Thruster" }, 
				{ SystemLanguage.Indonesian, "PAPAN LUNCUR PROFESIONAL" }, 
				{ SystemLanguage.Portuguese, "PRANCHA DE SURF PROFISSIONAL" }, 
				{ SystemLanguage.Italian, "Propulsore" }, 
				{ SystemLanguage.Korean, "투러스터 보드" }, 
				{ SystemLanguage.Russian, "Ускоритель" }, 
			}
		},


		{ "SURF BOARDS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Tablas de Surf" }, 
				{ SystemLanguage.ChineseSimplified, "冲浪板" }, 
				{ SystemLanguage.ChineseTraditional, "沖浪板" }, 
				{ SystemLanguage.French, "PLANCHES DE SURF" }, 
				{ SystemLanguage.Japanese, "サーフボード" }, 
				{ SystemLanguage.German, "SURFBOARDS" }, 
				{ SystemLanguage.Indonesian, "PAPAN - PAPAN LUNCUR" }, 
				{ SystemLanguage.Portuguese, "PRANCHAS DE SURF" }, 
				{ SystemLanguage.Italian, "TAVOLE DA SURF" }, 
				{ SystemLanguage.Korean, "서핑 보드" }, 
				{ SystemLanguage.Russian, "ДОСКИ ДЛЯ СЕРФИНГА" }, 
			}
		},


		{ "STICKSON", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "STICKSON (HOMBRECITO ADHESIVO)" }, 
				{ SystemLanguage.ChineseSimplified, "火柴人之子" }, 
				{ SystemLanguage.ChineseTraditional, "火柴人之子" }, 
				{ SystemLanguage.French, "STICKSON" }, 
				{ SystemLanguage.Japanese, "スティックマン" }, 
				{ SystemLanguage.German, "STICKSON" }, 
				{ SystemLanguage.Indonesian, "STICKSON" }, 
				{ SystemLanguage.Portuguese, "STICKSON" }, 
				{ SystemLanguage.Italian, "OMINO STILIZZATO" }, 
				{ SystemLanguage.Korean, "스틱선" }, 
				{ SystemLanguage.Russian, "СТИКСОН" }, 
			}
		},


		{ "SESSION DURATION", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DURACIÓN DE LA SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "游戏时间" }, 
				{ SystemLanguage.ChineseTraditional, "遊戲時限" }, 
				{ SystemLanguage.French, "DURÉE DE LA SESSION" }, 
				{ SystemLanguage.Japanese, "各セッションの長さ" }, 
				{ SystemLanguage.German, "SPIELDAUER" }, 
				{ SystemLanguage.Indonesian, "DURASI PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "DURAÇÃO DA SESSÃO" }, 
				{ SystemLanguage.Italian, "DURATA SESSIONE" }, 
				{ SystemLanguage.Korean, "게임세션 지속시간" }, 
				{ SystemLanguage.Russian, "ДЛИТЕЛЬНОСТЬ СЕАНСА" }, 
			}
		},


		{ "Cactus Bay", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "La bahía de los cáctus" }, 
				{ SystemLanguage.ChineseSimplified, "仙人掌海湾" }, 
				{ SystemLanguage.ChineseTraditional, "仙人掌海灣" }, 
				{ SystemLanguage.French, "Baie de Cactus" }, 
				{ SystemLanguage.Japanese, "サボテンベイ" }, 
				{ SystemLanguage.German, "Kaktus-Bucht" }, 
				{ SystemLanguage.Indonesian, "PANTAI KAKTUS" }, 
				{ SystemLanguage.Portuguese, "BAÍA DO CACTUS" }, 
				{ SystemLanguage.Italian, "Baia dei Cactus" }, 
				{ SystemLanguage.Korean, "선인장 베이" }, 
				{ SystemLanguage.Russian, "Кактусовая бухта" }, 
			}
		},


		{ "CREDITS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CRÉDITOS" }, 
				{ SystemLanguage.ChineseSimplified, "鸣谢" }, 
				{ SystemLanguage.ChineseTraditional, "幕後功臣" }, 
				{ SystemLanguage.French, "CRÉDITS" }, 
				{ SystemLanguage.Japanese, "クレジット" }, 
				{ SystemLanguage.German, "MACHER" }, 
				{ SystemLanguage.Indonesian, "KREDIT" }, 
				{ SystemLanguage.Portuguese, "CRÉDITOS" }, 
				{ SystemLanguage.Italian, "RINGRAZIAMENTI" }, 
				{ SystemLanguage.Korean, "크레디트" }, 
				{ SystemLanguage.Russian, "БЛАГОДАРНОСТИ" }, 
			}
		},


		{ "SHACK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Cabaña en la Playa" }, 
				{ SystemLanguage.ChineseSimplified, "棚屋" }, 
				{ SystemLanguage.ChineseTraditional, "窩棚" }, 
				{ SystemLanguage.French, "CABANE" }, 
				{ SystemLanguage.Japanese, "掘建て小屋" }, 
				{ SystemLanguage.German, "BARACKE" }, 
				{ SystemLanguage.Indonesian, "GUBUK" }, 
				{ SystemLanguage.Portuguese, "BARRACA" }, 
				{ SystemLanguage.Italian, "BARACCA" }, 
				{ SystemLanguage.Korean, "바닥가 오두막집" }, 
				{ SystemLanguage.Russian, "ХИЖИНА" }, 
			}
		},


		{ "NICE!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡BIEN!" }, 
				{ SystemLanguage.ChineseSimplified, "好棒！" }, 
				{ SystemLanguage.ChineseTraditional, "很好！" }, 
				{ SystemLanguage.French, "JOLI!" }, 
				{ SystemLanguage.Japanese, "いいね！" }, 
				{ SystemLanguage.German, "SCHÖN!" }, 
				{ SystemLanguage.Indonesian, "BAGUS !" }, 
				{ SystemLanguage.Portuguese, "BOM!" }, 
				{ SystemLanguage.Italian, "CARINO!" }, 
				{ SystemLanguage.Korean, "좋아요!" }, 
				{ SystemLanguage.Russian, "ПРЕКРАСНО!" }, 
			}
		},


		{ "AWESOME!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡INCREÍBLE!" }, 
				{ SystemLanguage.ChineseSimplified, "太厉害了！" }, 
				{ SystemLanguage.ChineseTraditional, "真棒！" }, 
				{ SystemLanguage.French, "GÉNIAL!" }, 
				{ SystemLanguage.Japanese, "すごい！" }, 
				{ SystemLanguage.German, "GROßARTIG!" }, 
				{ SystemLanguage.Indonesian, "KEREN !" }, 
				{ SystemLanguage.Portuguese, "IMPRESSIONANTE!" }, 
				{ SystemLanguage.Italian, "MERAVIGLIOSO!" }, 
				{ SystemLanguage.Korean, "멋져요!" }, 
				{ SystemLanguage.Russian, "ПОТРЯСАЮЩЕ!" }, 
			}
		},


		{ "GET BUSY!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡PONTE LAS PILAS!" }, 
				{ SystemLanguage.ChineseSimplified, "忙起来！" }, 
				{ SystemLanguage.ChineseTraditional, "動起來！" }, 
				{ SystemLanguage.French, "ÊTRE OCCUPÉ!" }, 
				{ SystemLanguage.Japanese, "急いで！" }, 
				{ SystemLanguage.German, "MACH ETWAS!" }, 
				{ SystemLanguage.Indonesian, "SIBUK !" }, 
				{ SystemLanguage.Portuguese, "VÁ!" }, 
				{ SystemLanguage.Italian, "DATTI DA FARE!" }, 
				{ SystemLanguage.Korean, "바빠지다!" }, 
				{ SystemLanguage.Russian, "ЗА ДЕЛО!" }, 
			}
		},


		{ "TIME'S UP!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡SE ACABÓ EL TIEMPO!" }, 
				{ SystemLanguage.ChineseSimplified, "时间到了！" }, 
				{ SystemLanguage.ChineseTraditional, "時間到！" }, 
				{ SystemLanguage.French, "LE TEMPS EST ÉCOULÉ!" }, 
				{ SystemLanguage.Japanese, "タイムアップ！" }, 
				{ SystemLanguage.German, "ZEIT IST UM!" }, 
				{ SystemLanguage.Indonesian, "WAKTU HABIS !" }, 
				{ SystemLanguage.Portuguese, "ACABOU O TEMPO!" }, 
				{ SystemLanguage.Italian, "IL TEMPO È FINITO!" }, 
				{ SystemLanguage.Korean, "시간이 다 되었습니다!" }, 
				{ SystemLanguage.Russian, "ВРЕМЯ ВЫШЛО!" }, 
			}
		},


		{ "PLAY {0} GAMES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JUEGA {0} JUEGOS" }, 
				{ SystemLanguage.ChineseSimplified, "玩 {0} 款游戏" }, 
				{ SystemLanguage.ChineseTraditional, "完成{0}個遊戲" }, 
				{ SystemLanguage.French, "JOUER {0} JEUX" }, 
				{ SystemLanguage.Japanese, "{0} 回ゲームをプレイ" }, 
				{ SystemLanguage.German, "SPIEL {0} SPIELE" }, 
				{ SystemLanguage.Indonesian, "BERMAIN {0} PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "JOGUE {0} JOGOS" }, 
				{ SystemLanguage.Italian, "GIOCA {0} GIOCHI" }, 
				{ SystemLanguage.Korean, "{0} 게임을 하다" }, 
				{ SystemLanguage.Russian, "ИГРАТЬ {0} РАЗ" }, 
			}
		},


		{ "REACH {0} SCORE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONSIGUE {0} DE PUNTUACIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "获取 {0} 得分" }, 
				{ SystemLanguage.ChineseTraditional, "分數達到{0}" }, 
				{ SystemLanguage.French, "ATTEINDTRE {0} UN SCORE" }, 
				{ SystemLanguage.Japanese, "{0}点を獲得" }, 
				{ SystemLanguage.German, "ERREICHE {0} PUNKTZAHL" }, 
				{ SystemLanguage.Indonesian, "MENCAPAI NILAI {0}" }, 
				{ SystemLanguage.Portuguese, "ATINJA O PLACAR {0}" }, 
				{ SystemLanguage.Italian, "RAGGIUNGI IL PUNTEGGIO DI {0}" }, 
				{ SystemLanguage.Korean, "{0} 점수에 도달하다" }, 
				{ SystemLanguage.Russian, "ДОСТИЧЬ РЕЗУЛЬТАТА {0}" }, 
			}
		},


		{ "REACH x{0} SCORE MULTIPLIER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONSIGUE x{0} MULTIPLICACIÓN DE PUNTUACIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "获取 x{0} 分数倍增器" }, 
				{ SystemLanguage.ChineseTraditional, "達到x{0}分數倍增器" }, 
				{ SystemLanguage.French, "ATTEINDTRE x {0} UN SCORE MULTIPLIER" }, 
				{ SystemLanguage.Japanese, "スコア{0}倍に到達" }, 
				{ SystemLanguage.German, "ERREICHE x{0} PUNKTZAHLMULTIPLIZIERER" }, 
				{ SystemLanguage.Indonesian, "MENCAPAI NILAI DIKALIKAN {0}" }, 
				{ SystemLanguage.Portuguese, "ATINJA O PLACAR MULTIPLICADOR x{0}" }, 
				{ SystemLanguage.Italian, "RAGGIUNGI x{0} NEL MOLTIPLICATORE DI PUNTEGGIO" }, 
				{ SystemLanguage.Korean, "x{0} 점수 곱셈기에 도달하다" }, 
				{ SystemLanguage.Russian, "ДОСТИЧЬ КОЭФФИЦИЕНТА ОЧКОВ х{0}" }, 
			}
		},


		{ "COLLECT {0} COINS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COLECCIONA {0} MONEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "收集 {0} 金币" }, 
				{ SystemLanguage.ChineseTraditional, "收集{0}金幣" }, 
				{ SystemLanguage.French, "COLLECTER {0} PIÈCES" }, 
				{ SystemLanguage.Japanese, "コインを{0}枚獲得" }, 
				{ SystemLanguage.German, "SAMMLE {0} MÜNZEN" }, 
				{ SystemLanguage.Indonesian, "KUMPULKAN {0} KOIN" }, 
				{ SystemLanguage.Portuguese, "COLETE {0} MOEDAS" }, 
				{ SystemLanguage.Italian, "RACCOGLI {0} MONETE" }, 
				{ SystemLanguage.Korean, "{0} 개의 동전을 수집하다" }, 
				{ SystemLanguage.Russian, "СОБРАТЬ {0} МОНЕТ" }, 
			}
		},


		{ "COLLECT {0} COINS WITH THE {1}", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COLECCIONA {0} MONEDAS CON EL/LA {1}" }, 
				{ SystemLanguage.ChineseSimplified, "{0}使用 {1} 收集 {0} 金币" }, 
				{ SystemLanguage.ChineseTraditional, "使用{1}收集{0}金幣" }, 
				{ SystemLanguage.French, "COLLECTER {0} PIÈCES AVEC LA {1}" }, 
				{ SystemLanguage.Japanese, "{1}で{0}枚のコインを獲得" }, 
				{ SystemLanguage.German, "SAMMLE {0} MÜNZEN MIT DEM {1}" }, 
				{ SystemLanguage.Indonesian, "KUMPULKAN {0} KOIN DENGAN {1}" }, 
				{ SystemLanguage.Portuguese, "COLETE {0} MOEDAS COM O {1}" }, 
				{ SystemLanguage.Italian, "RACCOGLI {0} MONETE IN {1}" }, 
				{ SystemLanguage.Korean, "{1}로 {0}개의 동전을 수집하다" }, 
				{ SystemLanguage.Russian, "СОБРАТЬ {0} МОНЕТ НА {1}" }, 
			}
		},


		{ "VISIT {0} {1} TIMES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VISITA {0} {1} VECES" }, 
				{ SystemLanguage.ChineseSimplified, "游览 {0} {1}次" }, 
				{ SystemLanguage.ChineseTraditional, "探訪{0} {1} 次" }, 
				{ SystemLanguage.French, "VISITER {0} {1} FOIS" }, 
				{ SystemLanguage.Japanese, "{0}を{1}回訪れる" }, 
				{ SystemLanguage.German, "BESUCHE {0} {1} MAL" }, 
				{ SystemLanguage.Indonesian, "KUNJUNGI {0} {1} KALI" }, 
				{ SystemLanguage.Portuguese, "VISITE {0} {1} VEZES" }, 
				{ SystemLanguage.Italian, "VAI IN {0} {1} VOLTE" }, 
				{ SystemLanguage.Korean, "{0} 를 {1} 번 방문하다" }, 
				{ SystemLanguage.Russian, "ПОСЕТИТЬ {0} {1} РАЗ" }, 
			}
		},


		{ "VISIT {0}", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VISITA {0}" }, 
				{ SystemLanguage.ChineseSimplified, "游览 {0}" }, 
				{ SystemLanguage.ChineseTraditional, "探訪{0}" }, 
				{ SystemLanguage.French, "VISITER {0}" }, 
				{ SystemLanguage.Japanese, "{0}を訪れる" }, 
				{ SystemLanguage.German, "BESUCHE {0}" }, 
				{ SystemLanguage.Indonesian, "KUNJUNGI {0}" }, 
				{ SystemLanguage.Portuguese, "VISITE {0}" }, 
				{ SystemLanguage.Italian, "VAI IN {0}" }, 
				{ SystemLanguage.Korean, "{0}를 방문하다" }, 
				{ SystemLanguage.Russian, "ПОСЕТИТЬ {0}" }, 
			}
		},


		{ "PICK UP THE {0} {1} TIMES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RECOGE EL/LA {0} {1} VECES" }, 
				{ SystemLanguage.ChineseSimplified, "取 {0} {1} 次" }, 
				{ SystemLanguage.ChineseTraditional, "選擇{0} {1} 次" }, 
				{ SystemLanguage.French, "RAMASSER {0} {1} FOIS" }, 
				{ SystemLanguage.Japanese, "{0}を{1}回選択する" }, 
				{ SystemLanguage.German, "HEBE DAS {0} {1} MAL AUF" }, 
				{ SystemLanguage.Indonesian, "MENGAMBIL {0} {1} KALI" }, 
				{ SystemLanguage.Portuguese, "PEGUE O {0} {1} VEZES" }, 
				{ SystemLanguage.Italian, "PRENDI LA {0} {1} VOLTE" }, 
				{ SystemLanguage.Korean, "{0}를 {1} 번 픽업하다" }, 
				{ SystemLanguage.Russian, "ВЫБРАТЬ {0} {1} РАЗ" }, 
			}
		},


		{ "PICK UP THE {0}", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RECOGE EL/LA {0}" }, 
				{ SystemLanguage.ChineseSimplified, "取 {0}" }, 
				{ SystemLanguage.ChineseTraditional, "選擇{0} 次" }, 
				{ SystemLanguage.French, "RAMASSER LA {0}" }, 
				{ SystemLanguage.Japanese, "{0}を選択する" }, 
				{ SystemLanguage.German, "HEBE DAS {0} AUF" }, 
				{ SystemLanguage.Indonesian, "MENGAMBIL {0}" }, 
				{ SystemLanguage.Portuguese, "PEGUE O {0}" }, 
				{ SystemLanguage.Italian, "PRENDI LA {0}" }, 
				{ SystemLanguage.Korean, "{0}를 픽업하다" }, 
				{ SystemLanguage.Russian, "ВЫБРАТЬ {0}" }, 
			}
		},


		{ "PASS THROUGH {0} GATES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PASA A TRAVÉS DE {0} PUERTAS" }, 
				{ SystemLanguage.ChineseSimplified, "穿过 {0} 阀门" }, 
				{ SystemLanguage.ChineseTraditional, "通過{0}個閘門" }, 
				{ SystemLanguage.French, "TRAVERSER {0} BARRIÈRES" }, 
				{ SystemLanguage.Japanese, "{0}個のゲートを通過" }, 
				{ SystemLanguage.German, "DURCHQUERE {0} TORE" }, 
				{ SystemLanguage.Indonesian, "LEWATI {0} JEMBATAN" }, 
				{ SystemLanguage.Portuguese, "ATRAVESSE {0} PORTÕES" }, 
				{ SystemLanguage.Italian, "ATTRAVERSA {0} PASSAGGI" }, 
				{ SystemLanguage.Korean, "{0}개의 게이트를 통과해 가다" }, 
				{ SystemLanguage.Russian, "ПРОЙТИ ЧЕРЕЗ {0} ВОРОТ" }, 
			}
		},


		{ "SURF {0}M", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SURF {0}M" }, 
				{ SystemLanguage.ChineseSimplified, "冲浪{0}米" }, 
				{ SystemLanguage.ChineseTraditional, "沖浪{0}米" }, 
				{ SystemLanguage.French, "SURF {0}M" }, 
				{ SystemLanguage.Japanese, "{0}mサーフィンで前進" }, 
				{ SystemLanguage.German, "SURFE {0} M" }, 
				{ SystemLanguage.Indonesian, "BERSELANCAR {0}M" }, 
				{ SystemLanguage.Portuguese, "SURFADOS {0}M" }, 
				{ SystemLanguage.Italian, "FAI SURF PER {0} M" }, 
				{ SystemLanguage.Korean, "{0}M 서핑하기" }, 
				{ SystemLanguage.Russian, "ПРОКАТИСЬ  {0}M" }, 
			}
		},


		{ "GRIND ONCE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DESLIZARSE UNA VEZ" }, 
				{ SystemLanguage.ChineseSimplified, "研磨1次" }, 
				{ SystemLanguage.ChineseTraditional, "研磨1次" }, 
				{ SystemLanguage.French, "GRINCER UNE FOIS" }, 
				{ SystemLanguage.Japanese, "一回グラインド" }, 
				{ SystemLanguage.German, "GRINDE EINMAL" }, 
				{ SystemLanguage.Indonesian, "MELUNCUR SATU KALI" }, 
				{ SystemLanguage.Portuguese, "UM DESLIZE" }, 
				{ SystemLanguage.Italian, "SCIVOLA UNA VOLTA" }, 
				{ SystemLanguage.Korean, "1번 미끄러지듯 타기" }, 
				{ SystemLanguage.Russian, "СДЕЛАЙ ГРАЙНД 1 РАЗ" }, 
			}
		},


		{ "MAGNET", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "IMÁN" }, 
				{ SystemLanguage.ChineseSimplified, "磁铁" }, 
				{ SystemLanguage.ChineseTraditional, "磁鐵" }, 
				{ SystemLanguage.French, "AIMANT" }, 
				{ SystemLanguage.Japanese, "マグネット" }, 
				{ SystemLanguage.German, "MAGNET" }, 
				{ SystemLanguage.Indonesian, "MAGNET" }, 
				{ SystemLanguage.Portuguese, "ÍMÃ" }, 
				{ SystemLanguage.Italian, "CALAMITA" }, 
				{ SystemLanguage.Korean, "자석" }, 
				{ SystemLanguage.Russian, "МАГНИТ" }, 
			}
		},


		{ "LEVEL UP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SUBIR DE NIVEL" }, 
				{ SystemLanguage.ChineseSimplified, "升级" }, 
				{ SystemLanguage.ChineseTraditional, "升級" }, 
				{ SystemLanguage.French, "NIVEAU SUPÉRIEUR" }, 
				{ SystemLanguage.Japanese, "レベルアップ" }, 
				{ SystemLanguage.German, "NÄCHSTES LEVEL" }, 
				{ SystemLanguage.Indonesian, "LEVEL NAIK" }, 
				{ SystemLanguage.Portuguese, "SUBIU DE NÍVEL" }, 
				{ SystemLanguage.Italian, "AUMENTA DI LIVELLO" }, 
				{ SystemLanguage.Korean, "레벨 업" }, 
				{ SystemLanguage.Russian, "НОВЫЙ УРОВЕНЬ" }, 
			}
		},


		{ "LEVEL {0}\nREQUIRED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "NIVEL {0}\n requerido" }, 
				{ SystemLanguage.ChineseSimplified, "需要等级{0}" }, 
				{ SystemLanguage.ChineseTraditional, "需要等級{0}" }, 
				{ SystemLanguage.French, "NIVEAU {0}\nREQUIS" }, 
				{ SystemLanguage.Japanese, "レベル{0} が必要です" }, 
				{ SystemLanguage.German, "LEVEL {0}\nNÖTIG" }, 
				{ SystemLanguage.Indonesian, "WAJIB\nLEVEL {0}" }, 
				{ SystemLanguage.Portuguese, "NÍVEL {0}\nNECESSÁRIO" }, 
				{ SystemLanguage.Italian, "LIVELLO {0}\nRICHIESTO" }, 
				{ SystemLanguage.Korean, "레벨 {0}\n필요함" }, 
				{ SystemLanguage.Russian, "НУЖЕН {0}\n УРОВЕНЬ" }, 
			}
		},


		{ "OUT OF FUEL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡SIN GASOLINA!" }, 
				{ SystemLanguage.ChineseSimplified, "没有汽油！" }, 
				{ SystemLanguage.ChineseTraditional, "沒有汽油！" }, 
				{ SystemLanguage.French, "EN PANNE D'ESSENCE!" }, 
				{ SystemLanguage.Japanese, "燃料切れ！" }, 
				{ SystemLanguage.German, "KEIN SPRIT MEHR!" }, 
				{ SystemLanguage.Indonesian, "KEHABISAN BENSIN" }, 
				{ SystemLanguage.Portuguese, "SEM COMBUSTÍVEL!" }, 
				{ SystemLanguage.Italian, "CARBURANTE ESAURITO!" }, 
				{ SystemLanguage.Korean, "연료 고갈" }, 
				{ SystemLanguage.Russian, "БЕНЗИН КОНЧИЛСЯ!" }, 
			}
		},


		{ "HOLD TO ACCELERATE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PRESIONAR PARA ACELERAR" }, 
				{ SystemLanguage.ChineseSimplified, "按着保持加速" }, 
				{ SystemLanguage.ChineseTraditional, "按著保持加速" }, 
				{ SystemLanguage.French, "TENEZ POUR ACCÉLÉRER" }, 
				{ SystemLanguage.Japanese, "長押しで加速" }, 
				{ SystemLanguage.German, "ZUM BESCHLEUNIGEN GEDRÜCKT HALTEN" }, 
				{ SystemLanguage.Indonesian, "TAHAN UNTUK PERCEPATAN" }, 
				{ SystemLanguage.Portuguese, "SEGURE PARA ACELERAR" }, 
				{ SystemLanguage.Italian, "TIENI PREMUTO PER ACCELERARE" }, 
				{ SystemLanguage.Korean, "속도 올리기 위해 꾹 누르기" }, 
				{ SystemLanguage.Russian, "НАЖИМАЙ ДЛЯ УСКОРЕНИЯ" }, 
			}
		},


		{ "THANKS FOR PLAYING!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¡GRACIAS POR JUGAR!" }, 
				{ SystemLanguage.ChineseSimplified, "谢谢参与游戏！" }, 
				{ SystemLanguage.ChineseTraditional, "謝謝參與遊戲！" }, 
				{ SystemLanguage.French, "MERCI D'AVOIR JOUÉ!" }, 
				{ SystemLanguage.Japanese, "プレイありがとうございます！" }, 
				{ SystemLanguage.German, "DANKE FÜR'S SPIELEN!" }, 
				{ SystemLanguage.Indonesian, "TERIMAKASIH SUDAH BERMAIN!" }, 
				{ SystemLanguage.Portuguese, "OBRIGADO POR JOGAR!" }, 
				{ SystemLanguage.Italian, "GRAZIE PER AVER GIOCATO!" }, 
				{ SystemLanguage.Korean, "플레이해주셔서 감사합니다!" }, 
				{ SystemLanguage.Russian, "СПАСИБО ЗА ИГРУ!" }, 
			}
		},


		{ "HOLD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MANTENER PRESIONADO" }, 
				{ SystemLanguage.ChineseSimplified, "按着保持加速" }, 
				{ SystemLanguage.ChineseTraditional, "按著保持加速" }, 
				{ SystemLanguage.French, "TENIR" }, 
				{ SystemLanguage.Japanese, "長押しで加速" }, 
				{ SystemLanguage.German, "GEDRÜCKT HALTEN" }, 
				{ SystemLanguage.Indonesian, "TAHAN" }, 
				{ SystemLanguage.Portuguese, "SEGURE" }, 
				{ SystemLanguage.Italian, "TIENI PREMUTO" }, 
				{ SystemLanguage.Korean, "꾹 누르기" }, 
				{ SystemLanguage.Russian, "ДЕРЖИ ПАЛЕЦ" }, 
			}
		},


		{ "SAVE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "GUARDAR" }, 
				{ SystemLanguage.ChineseSimplified, "节省" }, 
				{ SystemLanguage.ChineseTraditional, "節省" }, 
				{ SystemLanguage.French, "SAUVEGARDER" }, 
				{ SystemLanguage.Japanese, "節約" }, 
				{ SystemLanguage.German, "SPEICHERN" }, 
				{ SystemLanguage.Indonesian, "SIMPAN" }, 
				{ SystemLanguage.Portuguese, "SALVAR" }, 
				{ SystemLanguage.Italian, "RISPARMIA" }, 
				{ SystemLanguage.Korean, "모으기" }, 
				{ SystemLanguage.Russian, "СЭКОНОМИТЬ" }, 
			}
		},


		{ "LIMITED TIME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TIEMPO LIMITADO" }, 
				{ SystemLanguage.ChineseSimplified, "时间有限" }, 
				{ SystemLanguage.ChineseTraditional, "時間有限" }, 
				{ SystemLanguage.French, "TEMPS LIMITÉ" }, 
				{ SystemLanguage.Japanese, "期間限定" }, 
				{ SystemLanguage.German, "ZEITLICH BEGRENZT" }, 
				{ SystemLanguage.Indonesian, "WAKTU TERBATAS" }, 
				{ SystemLanguage.Portuguese, "TEMPO LIMITADO" }, 
				{ SystemLanguage.Italian, "TEMPO LIMITATO" }, 
				{ SystemLanguage.Korean, "제한된 시간" }, 
				{ SystemLanguage.Russian, "ОГРАНИЧЕННОЕ ВРЕМЯ" }, 
			}
		},


		{ "OFFER EXPIRES IN {0}", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LA OFERTA EXPIRA EN {0}" }, 
				{ SystemLanguage.ChineseSimplified, "优惠将在{0}过期" }, 
				{ SystemLanguage.ChineseTraditional, "優惠將在{0}過期" }, 
				{ SystemLanguage.French, "OFFRE EXPIRE DANS {0}" }, 
				{ SystemLanguage.Japanese, "期間終了まで{0}" }, 
				{ SystemLanguage.German, "ANGEBOT ENDET IN {0}" }, 
				{ SystemLanguage.Indonesian, "PENAWARAN BERAKHIR DALAM {0}" }, 
				{ SystemLanguage.Portuguese, "OFERTA EXPIRA EM {0}" }, 
				{ SystemLanguage.Italian, "L'OFFERTA TERMINA IN {0}" }, 
				{ SystemLanguage.Korean, "오퍼는 {0}에 만료됩니다" }, 
				{ SystemLanguage.Russian, "ПРЕДЛОЖЕНИЕ ИСТЕКАЕТ ЧЕРЕЗ  {0}" }, 
			}
		},


		{ "REACH : {0}M", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ALCANZAR: {0}M" }, 
				{ SystemLanguage.ChineseSimplified, "到达 : {0}米" }, 
				{ SystemLanguage.ChineseTraditional, "到達 : {0}米" }, 
				{ SystemLanguage.French, "ATTEINDRE: {0} M" }, 
				{ SystemLanguage.Japanese, "到達：{0}m" }, 
				{ SystemLanguage.German, "ERREICHE : {0}M" }, 
				{ SystemLanguage.Indonesian, "MENDAPATKAN : {0}M" }, 
				{ SystemLanguage.Portuguese, "ALCANCE : {0}M" }, 
				{ SystemLanguage.Italian, "RAGGIUNGI : {0}M" }, 
				{ SystemLanguage.Korean, "도달하기: {0}M" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ  {0}M" }, 
			}
		},


		{ "REACH {0}M IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ALCANZA: {0}M EN EL JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏中到达 : {0}米" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲中到達 : {0}米" }, 
				{ SystemLanguage.French, "ATTEINDRE {0}M DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイで{0}m到達" }, 
				{ SystemLanguage.German, "ERREICHE IN EINEM SPIEL {0}M" }, 
				{ SystemLanguage.Indonesian, "MENDAPAT {0}M DIDALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "ALCANCE {0}M EM UM JOGO" }, 
				{ SystemLanguage.Italian, "PERCORRI {0}M IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {0}M 도달하기" }, 
				{ SystemLanguage.Russian, "ПРОКАТИСЬ  {0}M ЗА ОДНУ ИГРУ" }, 
			}
		},


		{ "SURF {0}M WITH THE {1} IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SURF {0}M CON EL/LA {1} EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里使用{1}冲浪{0}米" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏使用{1}沖浪{0}米" }, 
				{ SystemLanguage.French, "SURFER {0}M AVEC LE {1} DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "{1}を使って、1プレイで{0}m到達" }, 
				{ SystemLanguage.German, "SURFE IN EINEM SPIEL MIT DER {1} {0} M" }, 
				{ SystemLanguage.Indonesian, "BERSELANCAR {0}M MENGGUNAKAN {1} DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "SURFE {0}M COM {1} EM UM JOGO" }, 
				{ SystemLanguage.Italian, "FAI SURF PER {0}M CON {1} IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {1}로 {0}M 서핑하기" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ {0}M С {1} В ИГРЕ" }, 
			}
		},


		{ "SURF {0}M WITH THE {1}", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SURF {0}M CON EL/LA {1}" }, 
				{ SystemLanguage.ChineseSimplified, "使用{1}冲浪{0}米" }, 
				{ SystemLanguage.ChineseTraditional, "使用{1}沖浪{0}米" }, 
				{ SystemLanguage.French, "SURFER {0}M AVEC LA {1}" }, 
				{ SystemLanguage.Japanese, "{1}を使って、{0}m到達" }, 
				{ SystemLanguage.German, "SURFE MIT DER {1} {0} M" }, 
				{ SystemLanguage.Indonesian, "BERSELANCAR {0}M MENGGUNAKAN {1}" }, 
				{ SystemLanguage.Portuguese, "SURFE {0}M COM {1}" }, 
				{ SystemLanguage.Italian, "FAI SURF PER {0}M CON {1}" }, 
				{ SystemLanguage.Korean, "{1}로{0}M 서핑하기" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ {0}M С {1}" }, 
			}
		},


		{ "LET'S SKATE!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VAMOS A PATINAR" }, 
				{ SystemLanguage.ChineseSimplified, "我们一起滑板" }, 
				{ SystemLanguage.ChineseTraditional, "我們一起滑板" }, 
				{ SystemLanguage.French, "ALLONS PATINER" }, 
				{ SystemLanguage.Japanese, "スケボー開始" }, 
				{ SystemLanguage.German, "LASS UNS SKATEN" }, 
				{ SystemLanguage.Indonesian, "AYO BERMAIN SKATE" }, 
				{ SystemLanguage.Portuguese, "VAMOS ANDAR DE SKATE" }, 
				{ SystemLanguage.Italian, "FACCIAMO UN PO' DI SKATEBOARD" }, 
				{ SystemLanguage.Korean, "스케이트를 탑시다" }, 
				{ SystemLanguage.Russian, "ДАВАЙ КАТАТЬСЯ!" }, 
			}
		},


		{ "COLLECT S.U.R.F {0} TIMES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REUNE LAS LETRAS S.U.R.F. {0} VECES" }, 
				{ SystemLanguage.ChineseSimplified, "手机S.U.R.F {0} 次" }, 
				{ SystemLanguage.ChineseTraditional, "手機S.U.R.F {0} 次" }, 
				{ SystemLanguage.French, "COLLECTER S.U.R.F {0} TIMES" }, 
				{ SystemLanguage.Japanese, "「S.U.R.F」の文字を{0}回獲得" }, 
				{ SystemLanguage.German, "SAMMLE {0} MAL S.U.R.F." }, 
				{ SystemLanguage.Indonesian, "MENGUMPULKAN S.U.R.F {0} KALI" }, 
				{ SystemLanguage.Portuguese, "COLECIONE S.U.R.F {0} VEZES" }, 
				{ SystemLanguage.Italian, "RACCOGLI S.U.R.F {0} VOLTE" }, 
				{ SystemLanguage.Korean, "S.U.R.F {0} 번 수집하기" }, 
				{ SystemLanguage.Russian, "СОБЕРИ БУКВЫ S.U.R.F  {0}  РАЗ" }, 
			}
		},


		{ "COLLECT S.U.R.F", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REUNE LAS LETRAS S.U.R.F." }, 
				{ SystemLanguage.ChineseSimplified, "收集S.U.R.F" }, 
				{ SystemLanguage.ChineseTraditional, "收集S.U.R.F" }, 
				{ SystemLanguage.French, "COLLECTER S.U.R.F" }, 
				{ SystemLanguage.Japanese, "「S.U.R.F」の文字を獲得" }, 
				{ SystemLanguage.German, "SAMMLE S.U.R.F." }, 
				{ SystemLanguage.Indonesian, "MENGUMPULKAN S.U.R.F" }, 
				{ SystemLanguage.Portuguese, "COLECIONE S.U.R.F" }, 
				{ SystemLanguage.Italian, "RACCOGLI S.U.R.F" }, 
				{ SystemLanguage.Korean, "S.U.R.F 수집하기" }, 
				{ SystemLanguage.Russian, "СОБЕРИ БУКВЫ S.U.R.F" }, 
			}
		},


		{ "SURF {0}M ON THE {1} IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SURF {0}M EN EL/LA  {1} EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里的{1}冲浪{0}米" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏的{1}沖浪{0}米" }, 
				{ SystemLanguage.French, "SURFER {0}M SUR LE {1} DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "{1}にて、1プレイで、サーフィン{0}m到達" }, 
				{ SystemLanguage.German, "SURFE {0} M AUF DEM {1} IN EINEM SPIEL" }, 
				{ SystemLanguage.Indonesian, "BERSELANCAR {0}M DI {1} DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "SURFE {0}M NO {1} EM UM JOGO" }, 
				{ SystemLanguage.Italian, "FAI SURF PER {0}M SU {1} IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {1} 에서 {0}M 서핑하기" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ {0}M НА {1} В ИГРЕ" }, 
			}
		},


		{ "REACH {0} SCORE IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ALCANZA {0} PUNTOS EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里达到{0}分" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏達到{0}分" }, 
				{ SystemLanguage.French, "ATTEINDRE {0} SCORE D'UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイで、{0}スコア到達" }, 
				{ SystemLanguage.German, "ERREICHE IN EINEM SPIEL EINEN SCORE VON {0}" }, 
				{ SystemLanguage.Indonesian, "MENDAPATKAN NILAI {0} DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "ALCANCE {0} DE PONTUAÇÃO EM UM JOGO" }, 
				{ SystemLanguage.Italian, "RAGGIUNGI UN PUNTEGGIO DI {0} IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {0} 스코어 도달하기" }, 
				{ SystemLanguage.Russian, "НАБЕРИ {0} ОЧКОВ В ИГРЕ" }, 
			}
		},


		{ "COLLECT {0} COINS IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COLECCIONA {0} MONEDAS EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里收集{0}金币" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏收集{0}金幣" }, 
				{ SystemLanguage.French, "COLLECTER {0} PIÈCES DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイでコインを{0}枚獲得" }, 
				{ SystemLanguage.German, "SAMMLE {0} MÜNZEN IN EINEM SPIEL" }, 
				{ SystemLanguage.Indonesian, "MENGUMPULKAN {0} KOIN DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "COLECIONE {0} MOEDAS EM UM JOGO" }, 
				{ SystemLanguage.Italian, "RACCOGLI {0} MONETE IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {0}개의 동전 수집하기" }, 
				{ SystemLanguage.Russian, "СОБЕРИ {0} МОНЕТ В ИГРЕ" }, 
			}
		},


		{ "VISIT {0} {1} TIMES IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VISITA {0} {1} VECES EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里探访{0} {1}次" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏探訪{0} {1}次" }, 
				{ SystemLanguage.French, "VISITER {0} {1} FOIS DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイで{0}を{1}回訪問" }, 
				{ SystemLanguage.German, "BESUCHE {0} {1} MAL IN EINEM SPIEL" }, 
				{ SystemLanguage.Indonesian, "MENGUNJUNGI {0} {1} KALI DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "VISITE {0} {1} VEZES EM UM JOGO" }, 
				{ SystemLanguage.Italian, "VAI IN {0} {1} VOLTE IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {0} {1}번 방문하기" }, 
				{ SystemLanguage.Russian, "ПОСЕТИ {0} {1} РАЗ В ИГРЕ" }, 
			}
		},


		{ "PICK UP THE {0} {1} TIMES IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "RECOGE EL/LA {0} {1} VECES EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里选择{0} {1}次" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏選擇{0} {1}次" }, 
				{ SystemLanguage.French, "RAMASSER {0} {1} FOIS DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイで{0}を{1}回獲得" }, 
				{ SystemLanguage.German, "HEBE IN EINEM SPIEL DAS {0} {1} MAL AUF" }, 
				{ SystemLanguage.Indonesian, "MENGAMBIL {0} {1} KALI DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "PEGUE O {0} {1} VEZES EM UM JOGO" }, 
				{ SystemLanguage.Italian, "RACCOGLI IL/LA {0} {1} VOLTE IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서{0} {1}번 픽업하기" }, 
				{ SystemLanguage.Russian, "СОБЕРИ {0} {1} РАЗ ЗА ОДНУ ИГРУ" }, 
			}
		},


		{ "GRIND {0} TIMES IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DESLÍZATE {0} VECES EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里研磨{0}次" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏研磨{0}次" }, 
				{ SystemLanguage.French, "GRINCER {0} FOIS DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイで{0}回グラインド" }, 
				{ SystemLanguage.German, "GRINDE {0} MAL IN EINEM SPIEL" }, 
				{ SystemLanguage.Indonesian, "MELUNCUR {0} KALI DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "DESLIZE {0} VEZES EM UM JOGO" }, 
				{ SystemLanguage.Italian, "SCIVOLA {0} VOLTE IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서{0} 번 미끄러지듯이 타기" }, 
				{ SystemLanguage.Russian, "СДЕЛАЙ ГРАЙНД {0} РАЗ В ИГРЕ" }, 
			}
		},


		{ "GRIND {0} TIMES", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DESLÍZATE {0} VECES" }, 
				{ SystemLanguage.ChineseSimplified, "研磨{0}次" }, 
				{ SystemLanguage.ChineseTraditional, "研磨{0}次" }, 
				{ SystemLanguage.French, "GRINCER {0} FOIS" }, 
				{ SystemLanguage.Japanese, "{0}回グラインド" }, 
				{ SystemLanguage.German, "GRINDE {0} MAL" }, 
				{ SystemLanguage.Indonesian, "MELUNCUR {0} KALI" }, 
				{ SystemLanguage.Portuguese, "DESLIZE {0} VEZES" }, 
				{ SystemLanguage.Italian, "SCIVOLA {0} VOLTE" }, 
				{ SystemLanguage.Korean, "{0} 번 미끄러지듯이 타기" }, 
				{ SystemLanguage.Russian, "СДЕЛАЙ ГРАЙНД {0} РАЗ" }, 
			}
		},


		{ "PASS THROUGH {0} GATES IN A GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ATRAVESAR UNA PUERTA" }, 
				{ SystemLanguage.ChineseSimplified, "通过闸门" }, 
				{ SystemLanguage.ChineseTraditional, "通過閘門" }, 
				{ SystemLanguage.French, "TRAVERSER UNE BARRIÈRE" }, 
				{ SystemLanguage.Japanese, "ゲートを1回通過" }, 
				{ SystemLanguage.German, "FAHRE DURCH EIN TOR" }, 
				{ SystemLanguage.Indonesian, "MELEWATI SEBUAH JEMBATAN" }, 
				{ SystemLanguage.Portuguese, "PASSE PELO PORTAL" }, 
				{ SystemLanguage.Italian, "PASSA ATTRAVERSO UN TRAGUARDO" }, 
				{ SystemLanguage.Korean, "게이트 사이 통과하기" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ ЧЕРЕЗ ВОРОТА" }, 
			}
		},


		{ "PASS THROUGH A GATE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ATRAVESAR {0} PUERTAS EN UN JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "在游戏里通过{0}个闸门" }, 
				{ SystemLanguage.ChineseTraditional, "在遊戲裏通過{0}個閘門" }, 
				{ SystemLanguage.French, "TRAVERSER {0} BARRIÈRES DANS UN JEU" }, 
				{ SystemLanguage.Japanese, "1プレイでゲートを{0}回通過" }, 
				{ SystemLanguage.German, "FAHRE IN EINEM SPIEL {0} MAL DURCH EIN TOR" }, 
				{ SystemLanguage.Indonesian, "MELEWATI {0} JEMBATAN DALAM PERMAINAN" }, 
				{ SystemLanguage.Portuguese, "PASSE POR {0} PORTAIS EM UM JOGO" }, 
				{ SystemLanguage.Italian, "PASSA ATTRAVERSO {0} TRAGUARDI IN UNA PARTITA" }, 
				{ SystemLanguage.Korean, "게임에서 {0} 게이트를 통과하기" }, 
				{ SystemLanguage.Russian, "ПРОЙДИ ЧЕРЕЗ {0} ВОРОТ В ИГРЕ" }, 
			}
		},


		{ "NEXT SKATE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PRÓXIMO PATINAJE" }, 
				{ SystemLanguage.ChineseSimplified, "下一次滑板" }, 
				{ SystemLanguage.ChineseTraditional, "下一次滑板" }, 
				{ SystemLanguage.French, "PATINAGE SUIVANT" }, 
				{ SystemLanguage.Japanese, "次のプレイ可能まで" }, 
				{ SystemLanguage.German, "BEIM NÄCHSTEN SKATEN" }, 
				{ SystemLanguage.Indonesian, "WAKTU MELUNCUR SELANJUTNYA" }, 
				{ SystemLanguage.Portuguese, "PRÓXIMO SKATE" }, 
				{ SystemLanguage.Italian, "PROSSIMA POSSIBILITÀ DI FARE SKATEBOARD" }, 
				{ SystemLanguage.Korean, "다음차례 스케이트타기" }, 
				{ SystemLanguage.Russian, "СЛЕДУЮЩИЙ ЗАЕЗД" }, 
			}
		},


		{ "PRO JEEP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JEEP PROFESIONAL" }, 
				{ SystemLanguage.ChineseSimplified, "专业吉普车" }, 
				{ SystemLanguage.ChineseTraditional, "專業吉普車" }, 
				{ SystemLanguage.French, "JEEP PRO" }, 
				{ SystemLanguage.Japanese, "プロ仕様ジープ" }, 
				{ SystemLanguage.German, "PROFESSIONELLER JEEP" }, 
				{ SystemLanguage.Indonesian, "PROFESSIONAL JEEP" }, 
				{ SystemLanguage.Portuguese, "JEEP PROFISSIONAL" }, 
				{ SystemLanguage.Italian, "JEEP PROFESSIONALE" }, 
				{ SystemLanguage.Korean, "프로 지프" }, 
				{ SystemLanguage.Russian, "КРУТОЙ ДЖИП" }, 
			}
		},


		{ "FLIP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DAR VUELTAS" }, 
				{ SystemLanguage.ChineseSimplified, "翻转" }, 
				{ SystemLanguage.ChineseTraditional, "翻轉" }, 
				{ SystemLanguage.French, "RETOURNER" }, 
				{ SystemLanguage.Japanese, "横転" }, 
				{ SystemLanguage.German, "ÜBERSCHLAGEN" }, 
				{ SystemLanguage.Indonesian, "TERBALIK" }, 
				{ SystemLanguage.Portuguese, "VIRAR" }, 
				{ SystemLanguage.Italian, "CAPOVOLGERE" }, 
				{ SystemLanguage.Korean, "뒤집기" }, 
				{ SystemLanguage.Russian, "КУВЫРОК" }, 
			}
		},


		{ "INTERNET CONNECTION REQUIRED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SE REQUIERE CONEXIÓN A INTERNET" }, 
				{ SystemLanguage.ChineseSimplified, "需要网络连接" }, 
				{ SystemLanguage.ChineseTraditional, "需要網絡連接" }, 
				{ SystemLanguage.French, "CONNEXION INTERNET REQUISE" }, 
				{ SystemLanguage.Japanese, "インターネット接続が必要です" }, 
				{ SystemLanguage.German, "INTERNETVERBINDUNG NOTWENDIG" }, 
				{ SystemLanguage.Indonesian, "DIBUTUHKAN KONEKSI INTERNET" }, 
				{ SystemLanguage.Portuguese, "É NECESSÁRIO CONEXÃO COM A INTERNET" }, 
				{ SystemLanguage.Italian, "RICHIESTA CONNESSIONE INTERNET" }, 
				{ SystemLanguage.Korean, "인터넷 연결이 필요합니다" }, 
				{ SystemLanguage.Russian, "НУЖЕН ИНТЕРНЕТ" }, 
			}
		},


		{ "PRESS\n<size=-26>TO GO UP</size>", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PULSAR\n<size=-26>PARA SUBIR A</size>" }, 
				{ SystemLanguage.ChineseSimplified, "手指按着屏幕\n<size=-26>往上</size>" }, 
				{ SystemLanguage.ChineseTraditional, "手指按著屏幕\n<size=-26>往上</size>" }, 
				{ SystemLanguage.French, "APPUYER\n<size=-26>POUR ALLER VERS LE HAUT</size>" }, 
				{ SystemLanguage.Japanese, "タップして\n<size=-26>上へ</size>" }, 
				{ SystemLanguage.German, "DRÜCKE\n<size=-26>UM NACH OBEN ZU GELANGEN</size>" }, 
				{ SystemLanguage.Indonesian, "TEKAN\n<size=-26>UNTUK KE ATAS</size>" }, 
				{ SystemLanguage.Portuguese, "PRESSIONE\n<size=-26>PARA SUBIR</size>" }, 
				{ SystemLanguage.Italian, "PREMI\n<size=-26>PER SALIRE</size>" }, 
				{ SystemLanguage.Korean, "누르기\n<size=-26>위로 가기 위해</size>" }, 
				{ SystemLanguage.Russian, "НАЖМИ\n<size=-26>ЧТОБЫ ПОДНЯТЬСЯ</size>" }, 
			}
		},


		{ "RELEASE\n<size=-26>TO GO DOWN</size>", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "LIBERAR\n<size=-26>PARA BAJAR A</size>" }, 
				{ SystemLanguage.ChineseSimplified, "手指离开屏幕\n<size=-26>往下</size>" }, 
				{ SystemLanguage.ChineseTraditional, "手指離開屏幕\n<size=-26>往下</size>" }, 
				{ SystemLanguage.French, "APPUYER\n<size=-26>POUR ALLER EN BAS</size>" }, 
				{ SystemLanguage.Japanese, "指を離して\n<size=-26>下へ</size>" }, 
				{ SystemLanguage.German, "DRÜCKE\n<size=-26>UM NACH UNTEN ZU GELANGEN</size>" }, 
				{ SystemLanguage.Indonesian, "LEPASKAN\n<size=-26>UNTUK KEBAWAH</size>" }, 
				{ SystemLanguage.Portuguese, "SOLTE\n<size=-26>PARA DESCER</size>" }, 
				{ SystemLanguage.Italian, "RILASCIA\n<size=-26>PER SCENDERE</size>" }, 
				{ SystemLanguage.Korean, "놓기\n<size=-26>아래로 가기 위해</size>" }, 
				{ SystemLanguage.Russian, "УБЕРИ ПАЛЕЦ\n<size=-26>ЧТОБЫ СПУСТИТЬСЯ</size>" }, 
			}
		},


		{ "Unable to load image", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "No se puede cargar imagen" }, 
				{ SystemLanguage.ChineseSimplified, "无法加载图片" }, 
				{ SystemLanguage.ChineseTraditional, "無法加載圖片" }, 
				{ SystemLanguage.French, "Impossible de charger l'image" }, 
				{ SystemLanguage.Japanese, "画像を読み込めません" }, 
				{ SystemLanguage.German, "Kann Bild nicht laden" }, 
				{ SystemLanguage.Indonesian, "Tidak bisa memuat gambar" }, 
				{ SystemLanguage.Portuguese, "Não foi possível carregar a imagem" }, 
				{ SystemLanguage.Italian, "Non è possibile caricare l'immagine" }, 
				{ SystemLanguage.Korean, "이미지를 로드할 수 없습니다" }, 
				{ SystemLanguage.Russian, "Невозможно загрузить картинку" }, 
			}
		},


		{ "SHOP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TIENDA" }, 
				{ SystemLanguage.ChineseSimplified, "商店" }, 
				{ SystemLanguage.ChineseTraditional, "商店" }, 
				{ SystemLanguage.French, "BOUTIQUE" }, 
				{ SystemLanguage.Japanese, "ショップ" }, 
				{ SystemLanguage.German, "SHOP" }, 
				{ SystemLanguage.Indonesian, "TOKO" }, 
				{ SystemLanguage.Portuguese, "COMPRAR" }, 
				{ SystemLanguage.Italian, "NEGOZIO" }, 
				{ SystemLanguage.Korean, "숍" }, 
				{ SystemLanguage.Russian, "МАГАЗИН" }, 
			}
		},


		{ "LOG IN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIAR SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "登录" }, 
				{ SystemLanguage.ChineseTraditional, "登錄" }, 
				{ SystemLanguage.French, "S'IDENTIFIER" }, 
				{ SystemLanguage.Japanese, "ログイン" }, 
				{ SystemLanguage.German, "EINLOGGEN" }, 
				{ SystemLanguage.Indonesian, "MASUK" }, 
				{ SystemLanguage.Portuguese, "ENTRAR" }, 
				{ SystemLanguage.Italian, "ACCESSO" }, 
				{ SystemLanguage.Korean, "로그인" }, 
				{ SystemLanguage.Russian, "АВТОРИЗОВАТЬСЯ" }, 
			}
		},


		{ "LOG OUT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CERRAR SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "登出" }, 
				{ SystemLanguage.ChineseTraditional, "登出" }, 
				{ SystemLanguage.French, "SE DÉCONNECTER" }, 
				{ SystemLanguage.Japanese, "ログアウト" }, 
				{ SystemLanguage.German, "AUSLOGGEN" }, 
				{ SystemLanguage.Indonesian, "KELUAR" }, 
				{ SystemLanguage.Portuguese, "SAIR" }, 
				{ SystemLanguage.Italian, "DISCONNETTERSI" }, 
				{ SystemLanguage.Korean, "로그 아웃" }, 
				{ SystemLanguage.Russian, "ВЫЙТИ" }, 
			}
		},


		{ "LOG IN / RESTORE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIAR SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "登录" }, 
				{ SystemLanguage.ChineseTraditional, "登錄" }, 
				{ SystemLanguage.French, "S'IDENTIFIER" }, 
				{ SystemLanguage.Japanese, "ログイン" }, 
				{ SystemLanguage.German, "EINLOGGEN" }, 
				{ SystemLanguage.Indonesian, "MASUK" }, 
				{ SystemLanguage.Portuguese, "ENTRAR" }, 
				{ SystemLanguage.Italian, "ACCESSO" }, 
				{ SystemLanguage.Korean, "로그인" }, 
				{ SystemLanguage.Russian, "АВТОРИЗОВАТЬСЯ" }, 
			}
		},


		{ "GAME DATA RESTORED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Los datos restaurados" }, 
				{ SystemLanguage.ChineseSimplified, "数据中还原" }, 
				{ SystemLanguage.ChineseTraditional, "數據中還原" }, 
				{ SystemLanguage.French, "RESTAURER DES DONNÉES" }, 
				{ SystemLanguage.Japanese, "復元されたデータ" }, 
				{ SystemLanguage.German, "wiederherzustellenden Daten" }, 
				{ SystemLanguage.Indonesian, "DATA dipulihkan" }, 
				{ SystemLanguage.Portuguese, "dados restaurados" }, 
				{ SystemLanguage.Italian, "dati ripristinati" }, 
				{ SystemLanguage.Korean, "데이터를 복원" }, 
				{ SystemLanguage.Russian, "ДАННЫЕ ВОССТАНОВЛЕНА" }, 
			}
		},


		{ "SHARE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "COMPARTIR" }, 
				{ SystemLanguage.ChineseSimplified, "分享" }, 
				{ SystemLanguage.ChineseTraditional, "分享" }, 
				{ SystemLanguage.French, "PARTAGER" }, 
				{ SystemLanguage.Japanese, "シェア" }, 
				{ SystemLanguage.German, "AKTIE" }, 
				{ SystemLanguage.Indonesian, "BAGIKAN" }, 
				{ SystemLanguage.Portuguese, "COMPARTILHAR" }, 
				{ SystemLanguage.Italian, "CONDIVIDERE" }, 
				{ SystemLanguage.Korean, "몫" }, 
				{ SystemLanguage.Russian, "ДОЛЯ" }, 
			}
		},


		{ "EMAIL OR PASSWORD IS INCORRECT.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CORREO ELECTRÓNICO O LA CONTRASEÑA SON INCORRECTOS." }, 
				{ SystemLanguage.ChineseSimplified, "电子邮件或密码不正确。" }, 
				{ SystemLanguage.ChineseTraditional, "電子郵件或密碼不正確。" }, 
				{ SystemLanguage.French, "E-MAIL OU MOT DE PASSE INCORRECT." }, 
				{ SystemLanguage.Japanese, "メールアドレスまたはパスワードが正しくありません。" }, 
				{ SystemLanguage.German, "EMAIL ODER PASSWORT IST FALSCH." }, 
				{ SystemLanguage.Indonesian, "EMAIL ATAU PASSWORD SALAH." }, 
				{ SystemLanguage.Portuguese, "E-MAIL OU SENHA ESTÁ INCORRETO." }, 
				{ SystemLanguage.Italian, "Email o la password non è corretta." }, 
				{ SystemLanguage.Korean, "이메일 또는 비밀번호가 잘못되었습니다." }, 
				{ SystemLanguage.Russian, "EMAIL ИЛИ ПАРОЛЬ УКАЗАН НЕВЕРНО." }, 
			}
		},


		{ "EMAIL ALREADY REGISTERED. LOG IN REQUIRED.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CORREO ELECTRÓNICO YA REGISTRADO. NECESARIO INICIAR SESIÓN." }, 
				{ SystemLanguage.ChineseSimplified, "EMAIL已注册。 LOG所需。" }, 
				{ SystemLanguage.ChineseTraditional, "EMAIL已註冊。 LOG所需。" }, 
				{ SystemLanguage.French, "EMAIL déjà enregistré. PSEUDO REQUIS." }, 
				{ SystemLanguage.Japanese, "EMAILは既に登録されています。 REQUIREDにログインします。" }, 
				{ SystemLanguage.German, "EMAIL SCHON REGISTRIERT. ANMELDUNG ERFORDERLICH." }, 
				{ SystemLanguage.Indonesian, "EMAIL SUDAH TERDAFTAR. LOGIN DI BUTUHKAN." }, 
				{ SystemLanguage.Portuguese, "E-MAIL JÁ REGISTRADO. LOG in necessário." }, 
				{ SystemLanguage.Italian, "EMAIL GIÀ REGISTRATA. ACCESSO RICHIESTO." }, 
				{ SystemLanguage.Korean, "EMAIL은 (는) 이미 등록. 필수 로그인합니다." }, 
				{ SystemLanguage.Russian, "E-MAIL УЖЕ ЗАРЕГИСТРИРОВАНЫ. Войти в систему." }, 
			}
		},


		{ "EMAIL AND PASSWORD IS REQUIRED.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Correo electrónico y contraseña que se requiere." }, 
				{ SystemLanguage.ChineseSimplified, "电子邮件地址和密码是必需的。" }, 
				{ SystemLanguage.ChineseTraditional, "電子郵件地址和密碼是必需的。" }, 
				{ SystemLanguage.French, "EMAIL ET MOT DE PASSE EST REQUISE." }, 
				{ SystemLanguage.Japanese, "メールアドレスとパスワードが必要です。" }, 
				{ SystemLanguage.German, "E-MAIL UND Kennwort erforderlich." }, 
				{ SystemLanguage.Indonesian, "EMAIL DAN PASSWORD DIPERLUKAN." }, 
				{ SystemLanguage.Portuguese, "-Mail e senha é necessária." }, 
				{ SystemLanguage.Italian, "Mail e la password è necessaria." }, 
				{ SystemLanguage.Korean, "이메일과 비밀번호가 필요합니다." }, 
				{ SystemLanguage.Russian, "Электронный адрес и пароль не требуется." }, 
			}
		},


		{ "EMAIL ALREADY REGISTERED. PLEASE LOG IN.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CORREO ELECTRÓNICO YA REGISTRADO. POR FAVOR INICIAR SESIÓN." }, 
				{ SystemLanguage.ChineseSimplified, "EMAIL已注册。请登录。" }, 
				{ SystemLanguage.ChineseTraditional, "EMAIL已註冊。請登錄。" }, 
				{ SystemLanguage.French, "EMAIL déjà enregistré. S'IL VOUS PLAÎT IDENTIFIER." }, 
				{ SystemLanguage.Japanese, "EMAILは既に登録されています。ログインしてください。" }, 
				{ SystemLanguage.German, "EMAIL SCHON REGISTRIERT. BITTE LOGGEN SIE SICH EIN." }, 
				{ SystemLanguage.Indonesian, "EMAIL SUDAH TERDAFTAR. SILAHKAN MASUK." }, 
				{ SystemLanguage.Portuguese, "E-MAIL JÁ REGISTRADO. POR FAVOR ENTRE." }, 
				{ SystemLanguage.Italian, "EMAIL GIÀ REGISTRATA. ACCEDETE, PER FAVORE." }, 
				{ SystemLanguage.Korean, "EMAIL은 (는) 이미 등록. 로그인 해주세요." }, 
				{ SystemLanguage.Russian, "E-MAIL УЖЕ ЗАРЕГИСТРИРОВАНЫ. ПОЖАЛУЙСТА, ВОЙДИТЕ." }, 
			}
		},


		{ "FACEBOOK LOGIN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EL MAPA" }, 
				{ SystemLanguage.ChineseSimplified, "FACEBOOK登入" }, 
				{ SystemLanguage.ChineseTraditional, "FACEBOOK登入" }, 
				{ SystemLanguage.French, "IDENTIFIANT FACEBOOK" }, 
				{ SystemLanguage.Japanese, "フェイスブックログイン" }, 
				{ SystemLanguage.German, "FACEBOOK ANMELDUNG" }, 
				{ SystemLanguage.Indonesian, "FACEBOOK LOGIN" }, 
				{ SystemLanguage.Portuguese, "LOGIN NO FACEBOOK" }, 
				{ SystemLanguage.Italian, "ACCESSO A FACEBOOK" }, 
				{ SystemLanguage.Korean, "페이스 북 로그인" }, 
				{ SystemLanguage.Russian, "ЛОГИН В ФЕЙСБУК" }, 
			}
		},


		{ "LOGIN CANCELLED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIO DE SESIÓN CANCELADA" }, 
				{ SystemLanguage.ChineseSimplified, "登录已取消" }, 
				{ SystemLanguage.ChineseTraditional, "登錄已取消" }, 
				{ SystemLanguage.French, "LOGIN ANNULÉ" }, 
				{ SystemLanguage.Japanese, "LOGIN CANCELLED" }, 
				{ SystemLanguage.German, "LOGIN ABGESAGT" }, 
				{ SystemLanguage.Indonesian, "Masuk DIBATALKAN" }, 
				{ SystemLanguage.Portuguese, "LOGIN CANCELADO" }, 
				{ SystemLanguage.Italian, "LOGIN ANNULLATO" }, 
				{ SystemLanguage.Korean, "로그인 취소됨" }, 
				{ SystemLanguage.Russian, "ВХОД ОТМЕНЕН" }, 
			}
		},


		{ "LOGIN SUCCESS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIO DE SESIÓN EXITOSO" }, 
				{ SystemLanguage.ChineseSimplified, "成功登录" }, 
				{ SystemLanguage.ChineseTraditional, "成功登錄" }, 
				{ SystemLanguage.French, "SUCCÈS LOGIN" }, 
				{ SystemLanguage.Japanese, "ログイン成功" }, 
				{ SystemLanguage.German, "LOGIN ERFOLG" }, 
				{ SystemLanguage.Indonesian, "SUKSES Masuk" }, 
				{ SystemLanguage.Portuguese, "SUCESSO LOGIN" }, 
				{ SystemLanguage.Italian, "SUCCESSO LOGIN" }, 
				{ SystemLanguage.Korean, "로그인 성공" }, 
				{ SystemLanguage.Russian, "ВХОД УСПЕХ" }, 
			}
		},


		{ "NO RESPONSE FROM INTERNET", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "No hay respuesta del INTERNET" }, 
				{ SystemLanguage.ChineseSimplified, "无响应从互联网上" }, 
				{ SystemLanguage.ChineseTraditional, "無響應從互聯網上" }, 
				{ SystemLanguage.French, "PAS DE REPONSE DE L'INTERNET" }, 
				{ SystemLanguage.Japanese, "インターネットからの応答がありません" }, 
				{ SystemLanguage.German, "KEINE REAKTION VON INTERNET" }, 
				{ SystemLanguage.Indonesian, "NO RESPON DARI INTERNET" }, 
				{ SystemLanguage.Portuguese, "SEM RESPOSTA DE INTERNET" }, 
				{ SystemLanguage.Italian, "Nessuna risposta dal INTERNET" }, 
				{ SystemLanguage.Korean, "인터넷에서 응답이 없습니다" }, 
				{ SystemLanguage.Russian, "Нет ответа от ИНТЕРНЕТ" }, 
			}
		},


		{ "LOGIN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIAR SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "登录" }, 
				{ SystemLanguage.ChineseTraditional, "登錄" }, 
				{ SystemLanguage.French, "S'IDENTIFIER" }, 
				{ SystemLanguage.Japanese, "ログイン" }, 
				{ SystemLanguage.German, "ANMELDUNG" }, 
				{ SystemLanguage.Indonesian, "MASUK" }, 
				{ SystemLanguage.Portuguese, "ENTRAR" }, 
				{ SystemLanguage.Italian, "ACCESSO" }, 
				{ SystemLanguage.Korean, "로그인" }, 
				{ SystemLanguage.Russian, "АВТОРИЗОВАТЬСЯ" }, 
			}
		},


		{ "LOGOUT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CERRAR SESIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "登出" }, 
				{ SystemLanguage.ChineseTraditional, "登出" }, 
				{ SystemLanguage.French, "SE DÉCONNECTER" }, 
				{ SystemLanguage.Japanese, "ログアウト" }, 
				{ SystemLanguage.German, "AUSLOGGEN" }, 
				{ SystemLanguage.Indonesian, "KELUAR" }, 
				{ SystemLanguage.Portuguese, "SAIR" }, 
				{ SystemLanguage.Italian, "DISCONNETTERSI" }, 
				{ SystemLanguage.Korean, "로그 아웃" }, 
				{ SystemLanguage.Russian, "ВЫЙТИ" }, 
			}
		},


		{ "REGISTER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REGISTRO" }, 
				{ SystemLanguage.ChineseSimplified, "寄存器" }, 
				{ SystemLanguage.ChineseTraditional, "寄存器" }, 
				{ SystemLanguage.French, "REGISTRE" }, 
				{ SystemLanguage.Japanese, "登録" }, 
				{ SystemLanguage.German, "NEU REGISTRIEREN" }, 
				{ SystemLanguage.Indonesian, "DAFTAR" }, 
				{ SystemLanguage.Portuguese, "REGISTRO" }, 
				{ SystemLanguage.Italian, "REGISTRO" }, 
				{ SystemLanguage.Korean, "레지스터" }, 
				{ SystemLanguage.Russian, "РЕГИСТР" }, 
			}
		},


		{ "REGISTER USER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REGISTRO DE USUARIO" }, 
				{ SystemLanguage.ChineseSimplified, "注册用户" }, 
				{ SystemLanguage.ChineseTraditional, "註冊用戶" }, 
				{ SystemLanguage.French, "REGISTRE UTILISATEUR" }, 
				{ SystemLanguage.Japanese, "ユーザーを登録" }, 
				{ SystemLanguage.German, "Registrieren USER" }, 
				{ SystemLanguage.Indonesian, "DAFTAR PENGGUNA" }, 
				{ SystemLanguage.Portuguese, "REGISTRO DE USUÁRIO" }, 
				{ SystemLanguage.Italian, "REGISTRO UTENTE" }, 
				{ SystemLanguage.Korean, "사용자 등록" }, 
				{ SystemLanguage.Russian, "РЕЕСТР USER" }, 
			}
		},


		{ "EMAIL:", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CORREO ELECTRÓNICO:" }, 
				{ SystemLanguage.ChineseSimplified, "电子邮件：" }, 
				{ SystemLanguage.ChineseTraditional, "電子郵件：" }, 
				{ SystemLanguage.French, "EMAIL:" }, 
				{ SystemLanguage.Japanese, "Eメール：" }, 
				{ SystemLanguage.German, "EMAIL:" }, 
				{ SystemLanguage.Indonesian, "E-MAIL:" }, 
				{ SystemLanguage.Portuguese, "O EMAIL:" }, 
				{ SystemLanguage.Italian, "E-MAIL:" }, 
				{ SystemLanguage.Korean, "이메일:" }, 
				{ SystemLanguage.Russian, "ЭЛ. АДРЕС:" }, 
			}
		},


		{ "PASSWORD:", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONTRASEÑA:" }, 
				{ SystemLanguage.ChineseSimplified, "密码：" }, 
				{ SystemLanguage.ChineseTraditional, "密碼：" }, 
				{ SystemLanguage.French, "MOT DE PASSE:" }, 
				{ SystemLanguage.Japanese, "パスワード：" }, 
				{ SystemLanguage.German, "PASSWORT:" }, 
				{ SystemLanguage.Indonesian, "KATA SANDI:" }, 
				{ SystemLanguage.Portuguese, "SENHA:" }, 
				{ SystemLanguage.Italian, "PAROLA D'ORDINE:" }, 
				{ SystemLanguage.Korean, "암호:" }, 
				{ SystemLanguage.Russian, "ПАРОЛЬ:" }, 
			}
		},


		{ "OR", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "O" }, 
				{ SystemLanguage.ChineseSimplified, "要么" }, 
				{ SystemLanguage.ChineseTraditional, "要么" }, 
				{ SystemLanguage.French, "OU" }, 
				{ SystemLanguage.Japanese, "OR" }, 
				{ SystemLanguage.German, "ODER" }, 
				{ SystemLanguage.Indonesian, "ATAU" }, 
				{ SystemLanguage.Portuguese, "OU" }, 
				{ SystemLanguage.Italian, "O" }, 
				{ SystemLanguage.Korean, "또는" }, 
				{ SystemLanguage.Russian, "ИЛИ" }, 
			}
		},


		{ "LOGIN USING FACEBOOK", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Sesión con FACEBOOK" }, 
				{ SystemLanguage.ChineseSimplified, "登录使用Facebook" }, 
				{ SystemLanguage.ChineseTraditional, "登錄使用Facebook" }, 
				{ SystemLanguage.French, "LOGIN Facebook" }, 
				{ SystemLanguage.Japanese, "FACEBOOKを使用してログイン" }, 
				{ SystemLanguage.German, "LOGIN über Facebook" }, 
				{ SystemLanguage.Indonesian, "Masuk MENGGUNAKAN FACEBOOK" }, 
				{ SystemLanguage.Portuguese, "Entrar usando o Facebook" }, 
				{ SystemLanguage.Italian, "Accedi usando FACEBOOK" }, 
				{ SystemLanguage.Korean, "페이스 북을 사용 LOGIN" }, 
				{ SystemLanguage.Russian, "ВХОД помощи" }, 
			}
		},


		{ "LOGIN TO BACKUP AND RESTORE GAME DATA", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Iniciar la sesión para respaldar y restaurar datos del juego" }, 
				{ SystemLanguage.ChineseSimplified, "LOGIN备份和恢复游戏数据" }, 
				{ SystemLanguage.ChineseTraditional, "LOGIN備份和恢復遊戲數據" }, 
				{ SystemLanguage.French, "LOGIN pour sauvegarder les données de JEU" }, 
				{ SystemLanguage.Japanese, "BACKUPにログインし、ゲームデータを復元" }, 
				{ SystemLanguage.German, "ZU SICHERN LOGIN und GAME DATA RESTORE" }, 
				{ SystemLanguage.Indonesian, "LOGIN Backup dan Restore PERTANDINGAN DATA" }, 
				{ SystemLanguage.Portuguese, "Entre para backup e restauração JOGO DE DADOS" }, 
				{ SystemLanguage.Italian, "ACCESSO PER backup e ripristino dati di gioco" }, 
				{ SystemLanguage.Korean, "BACKUP에 로그인하여 게임 데이터를 복원" }, 
				{ SystemLanguage.Russian, "Вход в систему ДО РЕЗЕРВИРОВАНИЮ игровых данных" }, 
			}
		},


		{ "LOGGED IN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONECTADO" }, 
				{ SystemLanguage.ChineseSimplified, "登录" }, 
				{ SystemLanguage.ChineseTraditional, "登錄" }, 
				{ SystemLanguage.French, "CONNECTÉ" }, 
				{ SystemLanguage.Japanese, "ログインして" }, 
				{ SystemLanguage.German, "EINGELOGGT" }, 
				{ SystemLanguage.Indonesian, "login" }, 
				{ SystemLanguage.Portuguese, "LOGADO" }, 
				{ SystemLanguage.Italian, "CONNESSO" }, 
				{ SystemLanguage.Korean, "로그인" }, 
				{ SystemLanguage.Russian, "вошли в систему" }, 
			}
		},


		{ "SUCCESS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ÉXITO" }, 
				{ SystemLanguage.ChineseSimplified, "成功" }, 
				{ SystemLanguage.ChineseTraditional, "成功" }, 
				{ SystemLanguage.French, "LE SUCCÈS" }, 
				{ SystemLanguage.Japanese, "成功" }, 
				{ SystemLanguage.German, "ERFOLG" }, 
				{ SystemLanguage.Indonesian, "KEBERHASILAN" }, 
				{ SystemLanguage.Portuguese, "SUCESSO" }, 
				{ SystemLanguage.Italian, "SUCCESSO" }, 
				{ SystemLanguage.Korean, "성공" }, 
				{ SystemLanguage.Russian, "УСПЕХ" }, 
			}
		},


		{ "USER LOGGED OUT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Usuario se desconectó" }, 
				{ SystemLanguage.ChineseSimplified, "用户退出" }, 
				{ SystemLanguage.ChineseTraditional, "用戶退出" }, 
				{ SystemLanguage.French, "USER DÉCONNECTÉ" }, 
				{ SystemLanguage.Japanese, "USERログアウトし" }, 
				{ SystemLanguage.German, "USER ABGEMELDET" }, 
				{ SystemLanguage.Indonesian, "PENGGUNA log out" }, 
				{ SystemLanguage.Portuguese, "Usuário conectado OUT" }, 
				{ SystemLanguage.Italian, "Utente si è disconnesso" }, 
				{ SystemLanguage.Korean, "사용자가 로그 아웃" }, 
				{ SystemLanguage.Russian, "USER логаут" }, 
			}
		},


		{ "FAILED", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "HA FALLADO" }, 
				{ SystemLanguage.ChineseSimplified, "失败" }, 
				{ SystemLanguage.ChineseTraditional, "失敗" }, 
				{ SystemLanguage.French, "ÉCHOUÉ" }, 
				{ SystemLanguage.Japanese, "FAILED" }, 
				{ SystemLanguage.German, "GESCHEITERT" }, 
				{ SystemLanguage.Indonesian, "GAGAL" }, 
				{ SystemLanguage.Portuguese, "FALHOU" }, 
				{ SystemLanguage.Italian, "FALLITO" }, 
				{ SystemLanguage.Korean, "실패한" }, 
				{ SystemLanguage.Russian, "НЕ СМОГЛИ" }, 
			}
		},


		{ "UNABLE TO CONTACT SERVER. PLEASE TRY AGAIN LATER.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INCAPAZ DE CONTACTAR CON EL SERVIDOR. POR FAVOR, INTÉNTELO DE NUEVO MÁS TARDE." }, 
				{ SystemLanguage.ChineseSimplified, "无法连接服务器。请稍后再试。" }, 
				{ SystemLanguage.ChineseTraditional, "無法連接服務器。請稍後再試。" }, 
				{ SystemLanguage.French, "IMPOSSIBLE DE CONTACTER LE SERVEUR. VEUILLEZ RÉESSAYER PLUS TARD." }, 
				{ SystemLanguage.Japanese, "サーバに接続できません。後ほどおかけ直しください。" }, 
				{ SystemLanguage.German, "UNABLE Server zu kontaktieren. Bitte versuchen Sie es erneut." }, 
				{ SystemLanguage.Indonesian, "TIDAK DAPAT MENGHUBUNGI SERVER. Silakan coba lagi nanti." }, 
				{ SystemLanguage.Portuguese, "INCAPAZ DE CONTACTAR O SERVIDOR. POR FAVOR, TENTE NOVAMENTE MAIS TARDE." }, 
				{ SystemLanguage.Italian, "NON RIESCO A CONTATTARE IL SERVER. PER FAVORE RIPROVA PIÙ TARDI." }, 
				{ SystemLanguage.Korean, "서버에 연결할 수 없습니다. 나중에 다시 시도 해주십시오." }, 
				{ SystemLanguage.Russian, "Не удается подключиться к серверу. ПОЖАЛУЙСТА, ПОВТОРИТЕ ПОПЫТКУ ПОЗЖЕ." }, 
			}
		},


		{ "INVALID EMAIL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "EMAIL INVÁLIDO" }, 
				{ SystemLanguage.ChineseSimplified, "不合规电邮" }, 
				{ SystemLanguage.ChineseTraditional, "不合規電郵" }, 
				{ SystemLanguage.French, "EMAIL INVALIDE" }, 
				{ SystemLanguage.Japanese, "無効なEメール" }, 
				{ SystemLanguage.German, "UNGÜLTIGE E-MAIL" }, 
				{ SystemLanguage.Indonesian, "EMAIL TIDAK VALID" }, 
				{ SystemLanguage.Portuguese, "E-MAIL INVÁLIDO" }, 
				{ SystemLanguage.Italian, "E-MAIL NON VALIDO" }, 
				{ SystemLanguage.Korean, "잘못된 이메일" }, 
				{ SystemLanguage.Russian, "НЕВЕРНЫЙ АДРЕС ЭЛЕКТРОННОЙ ПОЧТЫ" }, 
			}
		},


		{ "YOU MUST ENTER A VALID EMAIL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Debe ingresar un correo electrónico válida" }, 
				{ SystemLanguage.ChineseSimplified, "您必须输入一个有效的电子邮件" }, 
				{ SystemLanguage.ChineseTraditional, "您必須輸入一個有效的電子郵件" }, 
				{ SystemLanguage.French, "VOUS DEVEZ ENTRER A VALID EMAIL" }, 
				{ SystemLanguage.Japanese, "あなたは有効なメールアドレスを入力しなければなりません。" }, 
				{ SystemLanguage.German, "Sie müssen eine gültige E-MAIL ENTER" }, 
				{ SystemLanguage.Indonesian, "ANDA HARUS ENTER A EMAIL VALID" }, 
				{ SystemLanguage.Portuguese, "Você deve digitar um e-mail válido" }, 
				{ SystemLanguage.Italian, "È necessario inserire un indirizzo email valido" }, 
				{ SystemLanguage.Korean, "유효한 이메일 주소를 입력해야" }, 
				{ SystemLanguage.Russian, "Вы должны ввести правильный электронный адрес" }, 
			}
		},


		{ "INVALID PASSWORD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONTRASEÑA INVALIDA" }, 
				{ SystemLanguage.ChineseSimplified, "无效的密码" }, 
				{ SystemLanguage.ChineseTraditional, "無效的密碼" }, 
				{ SystemLanguage.French, "MOT DE PASSE INCORRECT" }, 
				{ SystemLanguage.Japanese, "無効なパスワード" }, 
				{ SystemLanguage.German, "UNGÜLTIGES PASSWORT" }, 
				{ SystemLanguage.Indonesian, "KATA SANDI SALAH" }, 
				{ SystemLanguage.Portuguese, "SENHA INVÁLIDA" }, 
				{ SystemLanguage.Italian, "PASSWORD NON VALIDA" }, 
				{ SystemLanguage.Korean, "유효하지 않은 비밀번호" }, 
				{ SystemLanguage.Russian, "НЕВЕРНЫЙ ПАРОЛЬ" }, 
			}
		},


		{ "YOU MUST ENTER A PASSWORD AT LEAST 5 CHARACTERS LONG", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Debe introducir una clave de al menos 5 caracteres" }, 
				{ SystemLanguage.ChineseSimplified, "您必须输入密码至少5个字符长" }, 
				{ SystemLanguage.ChineseTraditional, "您必須輸入密碼至少5個字符長" }, 
				{ SystemLanguage.French, "VOUS DEVEZ ENTRER UN MOT DE PASSE AU MOINS 5 PERSONNAGES LONG" }, 
				{ SystemLanguage.Japanese, "あなたは、少なくとも5文字の長パスワードを入力する必要があります" }, 
				{ SystemLanguage.German, "Sie müssen ein Passwort mindestens 5 Zeichen eingeben" }, 
				{ SystemLanguage.Indonesian, "ANDA HARUS ENTER A PASSWORD SETIDAKNYA 5 KARAKTER LONG" }, 
				{ SystemLanguage.Portuguese, "Você deve digitar uma senha com pelo menos 5 caracteres" }, 
				{ SystemLanguage.Italian, "È necessario inserire una password di almeno 5 caratteri" }, 
				{ SystemLanguage.Korean, "적어도 5 자 암호를 입력해야" }, 
				{ SystemLanguage.Russian, "Вы должны ввести пароль на странице не менее 5 символов" }, 
			}
		},


		{ "NEW USER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "NUEVO USUARIO" }, 
				{ SystemLanguage.ChineseSimplified, "新用户" }, 
				{ SystemLanguage.ChineseTraditional, "新用戶" }, 
				{ SystemLanguage.French, "NOUVEL UTILISATEUR" }, 
				{ SystemLanguage.Japanese, "新しいユーザー" }, 
				{ SystemLanguage.German, "NEUER BENUTZER" }, 
				{ SystemLanguage.Indonesian, "PENGGUNA BARU" }, 
				{ SystemLanguage.Portuguese, "NOVO USUÁRIO" }, 
				{ SystemLanguage.Italian, "NUOVO UTENTE" }, 
				{ SystemLanguage.Korean, "새로운 사용자" }, 
				{ SystemLanguage.Russian, "НОВЫЙ ПОЛЬЗОВАТЕЛЬ" }, 
			}
		},


		{ "LOGIN USER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "INICIO DE SESIÓN DE USUARIO" }, 
				{ SystemLanguage.ChineseSimplified, "登录用户" }, 
				{ SystemLanguage.ChineseTraditional, "登錄用戶" }, 
				{ SystemLanguage.French, "USER LOGIN" }, 
				{ SystemLanguage.Japanese, "ログインユーザ" }, 
				{ SystemLanguage.German, "LOGIN USER" }, 
				{ SystemLanguage.Indonesian, "USER LOGIN" }, 
				{ SystemLanguage.Portuguese, "USUÁRIO LOGIN" }, 
				{ SystemLanguage.Italian, "LOGIN" }, 
				{ SystemLanguage.Korean, "로그인 USER" }, 
				{ SystemLanguage.Russian, "ВХОД ПОЛЬЗОВАТЕЛЯ" }, 
			}
		},


		{ "FORGOT PASSWORD?", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¿SE TE OLVIDÓ TU CONTRASEÑA?" }, 
				{ SystemLanguage.ChineseSimplified, "忘记密码？" }, 
				{ SystemLanguage.ChineseTraditional, "忘記密碼？" }, 
				{ SystemLanguage.French, "MOT DE PASSE OUBLIÉ?" }, 
				{ SystemLanguage.Japanese, "パスワードをお忘れですか？" }, 
				{ SystemLanguage.German, "PASSWORT VERGESSEN?" }, 
				{ SystemLanguage.Indonesian, "LUPA KATA SANDI?" }, 
				{ SystemLanguage.Portuguese, "ESQUECEU A SENHA?" }, 
				{ SystemLanguage.Italian, "HA DIMENTICATO LA PASSWORD?" }, 
				{ SystemLanguage.Korean, "비밀번호를 잊으 셨나요?" }, 
				{ SystemLanguage.Russian, "ЗАБЫЛИ ПАРОЛЬ?" }, 
			}
		},


		{ "CONTACT US", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CONTÁCTENOS" }, 
				{ SystemLanguage.ChineseSimplified, "联系我们" }, 
				{ SystemLanguage.ChineseTraditional, "聯繫我們" }, 
				{ SystemLanguage.French, "CONTACTEZ NOUS" }, 
				{ SystemLanguage.Japanese, "お問い合わせ" }, 
				{ SystemLanguage.German, "KONTAKTIERE UNS" }, 
				{ SystemLanguage.Indonesian, "HUBUNGI KAMI" }, 
				{ SystemLanguage.Portuguese, "ENTRE EM CONTATO CONOSCO" }, 
				{ SystemLanguage.Italian, "CONTATTACI" }, 
				{ SystemLanguage.Korean, "접촉" }, 
				{ SystemLanguage.Russian, "СВЯЖИТЕСЬ С НАМИ" }, 
			}
		},


		{ "SEND EMAIL TO CONTACT@TURBOCHILLI.COM TO RESET PASSWORD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Enviar correo electrónico al CONTACT@TURBOCHILLI.COM para restablecer la contraseña" }, 
				{ SystemLanguage.ChineseSimplified, "发送电子邮件至CONTACT@TURBOCHILLI.COM 重置密码" }, 
				{ SystemLanguage.ChineseTraditional, "發送電子郵件至CONTACT@TURBOCHILLI.COM 重置密碼" }, 
				{ SystemLanguage.French, "Envoyer un email à CONTACT@TURBOCHILLI.COM POUR RÉINITIALISER MOT DE PASSE" }, 
				{ SystemLanguage.Japanese, "パスワードをリセットするにCONTACT@TURBOCHILLI.COMにメールを送ります" }, 
				{ SystemLanguage.German, "E-Mails an CONTACT@TURBOCHILLI.COM Passwort zurücksetzen" }, 
				{ SystemLanguage.Indonesian, "KIRIM EMAIL KE CONTACT@TURBOCHILLI.COM UNTUK RESET PASSWORD" }, 
				{ SystemLanguage.Portuguese, "Envie um email para CONTACT@TURBOCHILLI.COM para redefinir a senha" }, 
				{ SystemLanguage.Italian, "Invia email a CONTACT@TURBOCHILLI.COM per reimpostare la password" }, 
				{ SystemLanguage.Korean, "비밀번호를 재설정 할 CONTACT@TURBOCHILLI.COM에게 이메일을 보내" }, 
				{ SystemLanguage.Russian, "Отправьте сообщение по адресу CONTACT@TURBOCHILLI.COM~~pobj для сброса пароля" }, 
			}
		},


		{ "GAME COOLDOWN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "JUEGO DE ENFRIAMIENTO" }, 
				{ SystemLanguage.ChineseSimplified, "时间冷却" }, 
				{ SystemLanguage.ChineseTraditional, "時間冷卻" }, 
				{ SystemLanguage.French, "JEU REFROIDIR" }, 
				{ SystemLanguage.Japanese, "冷却期間" }, 
				{ SystemLanguage.German, "Zeit zum Abkühlen" }, 
				{ SystemLanguage.Indonesian, "Waktu untuk menenangkan diri" }, 
				{ SystemLanguage.Portuguese, "JOGO arrefecer" }, 
				{ SystemLanguage.Italian, "Gioco, divertente GIÙ" }, 
				{ SystemLanguage.Korean, "게임은 냉각" }, 
				{ SystemLanguage.Russian, "ВРЕМЯ остывать" }, 
			}
		},


		{ "PLAY NOW", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "REPRODUCIR AHORA" }, 
				{ SystemLanguage.ChineseSimplified, "现在播放" }, 
				{ SystemLanguage.ChineseTraditional, "現在播放" }, 
				{ SystemLanguage.French, "JOUE MAINTENANT" }, 
				{ SystemLanguage.Japanese, "スキップ" }, 
				{ SystemLanguage.German, "JETZT SPIELEN" }, 
				{ SystemLanguage.Indonesian, "MAIN SEKARANG" }, 
				{ SystemLanguage.Portuguese, "JOGUE AGORA" }, 
				{ SystemLanguage.Italian, "RIPRODUCI ORA" }, 
				{ SystemLanguage.Korean, "지금 플레이" }, 
				{ SystemLanguage.Russian, "ИГРАТЬ СЕЙЧАС" }, 
			}
		},


		{ "HOLD TO SPIN!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "MANTENER A GIRAR!" }, 
				{ SystemLanguage.ChineseSimplified, "保持旋转" }, 
				{ SystemLanguage.ChineseTraditional, "保持旋轉" }, 
				{ SystemLanguage.French, "TENIR À TOURNER" }, 
				{ SystemLanguage.Japanese, "スピンして" }, 
				{ SystemLanguage.German, "DRÜCKEN SIE DREHEN" }, 
				{ SystemLanguage.Indonesian, "TAHAN untuk memutar" }, 
				{ SystemLanguage.Portuguese, "MANTER a girar!" }, 
				{ SystemLanguage.Italian, "ATTESA PER RUOTARE" }, 
				{ SystemLanguage.Korean, "스핀 잡아!" }, 
				{ SystemLanguage.Russian, "ДЕРЖАТЬ крутиться!" }, 
			}
		},


		{ "MORE SPINS!", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Más vueltas!" }, 
				{ SystemLanguage.ChineseSimplified, "更多旋转！" }, 
				{ SystemLanguage.ChineseTraditional, "更多旋轉！" }, 
				{ SystemLanguage.French, "PLUS TOURNER!" }, 
				{ SystemLanguage.Japanese, "もっとスピン!" }, 
				{ SystemLanguage.German, "MEHR DREHEN!" }, 
				{ SystemLanguage.Indonesian, "LEBIH PUTAR!" }, 
				{ SystemLanguage.Portuguese, "MAIS gira!" }, 
				{ SystemLanguage.Italian, "Le più rotazioni!" }, 
				{ SystemLanguage.Korean, "추가로 회전 급강하!" }, 
				{ SystemLanguage.Russian, "БОЛЬШЕ раскручивает!" }, 
			}
		},


		{ "Rate Us", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CALIFICA EL JUEGO" }, 
				{ SystemLanguage.ChineseSimplified, "游戏反馈" }, 
				{ SystemLanguage.ChineseTraditional, "遊戲反饋" }, 
				{ SystemLanguage.French, "TAUX DE JEU" }, 
				{ SystemLanguage.Japanese, "ゲームのフィードバック" }, 
				{ SystemLanguage.German, "RATE SPIEL" }, 
				{ SystemLanguage.Indonesian, "umpan balik" }, 
				{ SystemLanguage.Portuguese, "TAXA DE JOGO" }, 
				{ SystemLanguage.Italian, "TASSO DI GIOCO" }, 
				{ SystemLanguage.Korean, "게임 피드백" }, 
				{ SystemLanguage.Russian, "обратная связь игры" }, 
			}
		},


		{ "Cancel", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "Cancelar" }, 
				{ SystemLanguage.ChineseSimplified, "取消" }, 
				{ SystemLanguage.ChineseTraditional, "取消" }, 
				{ SystemLanguage.French, "Annuler" }, 
				{ SystemLanguage.Japanese, "キャンセル" }, 
				{ SystemLanguage.German, "Stornieren" }, 
				{ SystemLanguage.Indonesian, "Membatalkan" }, 
				{ SystemLanguage.Portuguese, "Cancelar" }, 
				{ SystemLanguage.Italian, "Annulla" }, 
				{ SystemLanguage.Korean, "취소" }, 
				{ SystemLanguage.Russian, "Отмена" }, 
			}
		},


		{ "Having fun? Rate the game so we can bring you more cool updates.", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "¿Divirtiéndose? tasa de juego por lo que puede traer cambios más fresco." }, 
				{ SystemLanguage.ChineseSimplified, "正玩得开心？率的游戏，所以我们可以带来更多的清凉更新。" }, 
				{ SystemLanguage.ChineseTraditional, "正玩得開心？率的遊戲，所以我們可以帶來更多的清涼更新。" }, 
				{ SystemLanguage.French, "S'amuser? jeu de rythme afin que nous puissions apporter des mises à jour plus fraîches." }, 
				{ SystemLanguage.Japanese, "楽しんでますか？レートゲームので、私たちはより多くのクールなアップデートをもたらすことができます。" }, 
				{ SystemLanguage.German, "Spaß haben? Rate-Spiel, so dass wir mehr coole Updates bringen." }, 
				{ SystemLanguage.Indonesian, "Bersenang-senang? tingkat permainan sehingga kita dapat membawa pembaruan lebih keren." }, 
				{ SystemLanguage.Portuguese, "Se divertindo? jogo de taxa para que possamos trazer atualizações mais legal." }, 
				{ SystemLanguage.Italian, "Divertirsi? tasso di gioco in modo che possiamo portare gli aggiornamenti più cool." }, 
				{ SystemLanguage.Korean, "재미? 속도의 게임은 그래서 우리는 더 멋진 업데이트를 가져올 수 있습니다." }, 
				{ SystemLanguage.Russian, "Веселиться? игра скорость так что мы можем принести более прохладный обновления." }, 
			}
		},


		{ "SPINS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "ROTACIONES" }, 
				{ SystemLanguage.ChineseSimplified, "纺" }, 
				{ SystemLanguage.ChineseTraditional, "紡" }, 
				{ SystemLanguage.French, "PIROUETTES" }, 
				{ SystemLanguage.Japanese, "スピン" }, 
				{ SystemLanguage.German, "ROTATIONEN" }, 
				{ SystemLanguage.Indonesian, "rotasi" }, 
				{ SystemLanguage.Portuguese, "ROTACIONA" }, 
				{ SystemLanguage.Italian, "ROTAZIONI" }, 
				{ SystemLanguage.Korean, "회전 급강하" }, 
				{ SystemLanguage.Russian, "раскручивает" }, 
			}
		},


		{ "DUCK HEAD", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "cabeza de pato" }, 
				{ SystemLanguage.ChineseSimplified, "鸭头" }, 
				{ SystemLanguage.ChineseTraditional, "鴨頭" }, 
				{ SystemLanguage.French, "TETE DE CANARD" }, 
				{ SystemLanguage.Japanese, "アヒル" }, 
				{ SystemLanguage.German, "ENTE" }, 
				{ SystemLanguage.Indonesian, "BEBEK" }, 
				{ SystemLanguage.Portuguese, "cabeça de pato" }, 
				{ SystemLanguage.Italian, "ANATRA" }, 
				{ SystemLanguage.Korean, "오리" }, 
				{ SystemLanguage.Russian, "Утка ГОЛОВА" }, 
			}
		},


		{ "PINEAPPLE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PIÑA" }, 
				{ SystemLanguage.ChineseSimplified, "菠萝" }, 
				{ SystemLanguage.ChineseTraditional, "菠蘿" }, 
				{ SystemLanguage.French, "ANANAS" }, 
				{ SystemLanguage.Japanese, "パイナップル" }, 
				{ SystemLanguage.German, "ANANAS" }, 
				{ SystemLanguage.Indonesian, "NANAS" }, 
				{ SystemLanguage.Portuguese, "ABACAXI" }, 
				{ SystemLanguage.Italian, "ANANAS" }, 
				{ SystemLanguage.Korean, "파인애플" }, 
				{ SystemLanguage.Russian, "АНАНАС" }, 
			}
		},


		{ "SNORKEL", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "TUBO RESPIRADOR" }, 
				{ SystemLanguage.ChineseSimplified, "潜水" }, 
				{ SystemLanguage.ChineseTraditional, "潛水" }, 
				{ SystemLanguage.French, "TUBA" }, 
				{ SystemLanguage.Japanese, "スノーケル" }, 
				{ SystemLanguage.German, "SCHNORCHEL" }, 
				{ SystemLanguage.Indonesian, "SNORKEL" }, 
				{ SystemLanguage.Portuguese, "SNORKEL" }, 
				{ SystemLanguage.Italian, "BOCCAGLIO" }, 
				{ SystemLanguage.Korean, "스노클" }, 
				{ SystemLanguage.Russian, "ДАЙВИНГ" }, 
			}
		},


		{ "SWIMMING CAP", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "GORRA DE NATACIÓN" }, 
				{ SystemLanguage.ChineseSimplified, "游泳帽" }, 
				{ SystemLanguage.ChineseTraditional, "游泳帽" }, 
				{ SystemLanguage.French, "BONNET DE BAIN" }, 
				{ SystemLanguage.Japanese, "水泳帽" }, 
				{ SystemLanguage.German, "SCHWIMMHAUBE" }, 
				{ SystemLanguage.Indonesian, "TOPI BERENANG" }, 
				{ SystemLanguage.Portuguese, "touca de natação" }, 
				{ SystemLanguage.Italian, "CUFFIA DA NUOTO" }, 
				{ SystemLanguage.Korean, "수영 모자" }, 
				{ SystemLanguage.Russian, "ПЛАВАТЕЛЬНАЯ ШАПОЧКА" }, 
			}
		},


		{ "OCTOPUS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "PULPO" }, 
				{ SystemLanguage.ChineseSimplified, "章鱼" }, 
				{ SystemLanguage.ChineseTraditional, "章魚" }, 
				{ SystemLanguage.French, "POULPE" }, 
				{ SystemLanguage.Japanese, "たこ" }, 
				{ SystemLanguage.German, "TINTENFISCH" }, 
				{ SystemLanguage.Indonesian, "GURITA" }, 
				{ SystemLanguage.Portuguese, "POLVO" }, 
				{ SystemLanguage.Italian, "POLPO" }, 
				{ SystemLanguage.Korean, "문어" }, 
				{ SystemLanguage.Russian, "ОСЬМИНОГ" }, 
			}
		},


		{ "CAPTAINS HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO CAPITANES" }, 
				{ SystemLanguage.ChineseSimplified, "队长" }, 
				{ SystemLanguage.ChineseTraditional, "隊長" }, 
				{ SystemLanguage.French, "CAPITAINE" }, 
				{ SystemLanguage.Japanese, "キャプテン" }, 
				{ SystemLanguage.German, "KAPITÄN" }, 
				{ SystemLanguage.Indonesian, "KAPTEN" }, 
				{ SystemLanguage.Portuguese, "CAPITÃO" }, 
				{ SystemLanguage.Italian, "CAPITANO" }, 
				{ SystemLanguage.Korean, "선장 모자" }, 
				{ SystemLanguage.Russian, "КАПИТАНЫ HAT" }, 
			}
		},


		{ "ELF", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "DUENDE" }, 
				{ SystemLanguage.ChineseSimplified, "小精灵" }, 
				{ SystemLanguage.ChineseTraditional, "小精靈" }, 
				{ SystemLanguage.French, "ELFE" }, 
				{ SystemLanguage.Japanese, "エルフ" }, 
				{ SystemLanguage.German, "ELF" }, 
				{ SystemLanguage.Indonesian, "PERI" }, 
				{ SystemLanguage.Portuguese, "DUENDE" }, 
				{ SystemLanguage.Italian, "ELFO" }, 
				{ SystemLanguage.Korean, "꼬마 요정" }, 
				{ SystemLanguage.Russian, "эльф" }, 
			}
		},


		{ "PAPER HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO DE PAPEL" }, 
				{ SystemLanguage.ChineseSimplified, "纸帽" }, 
				{ SystemLanguage.ChineseTraditional, "紙帽" }, 
				{ SystemLanguage.French, "PAPIER" }, 
				{ SystemLanguage.Japanese, "紙" }, 
				{ SystemLanguage.German, "PAPIERHUT" }, 
				{ SystemLanguage.Indonesian, "PAPER HAT" }, 
				{ SystemLanguage.Portuguese, "chapéu de papel" }, 
				{ SystemLanguage.Italian, "CARTA" }, 
				{ SystemLanguage.Korean, "용지 HAT" }, 
				{ SystemLanguage.Russian, "БУМАГА" }, 
			}
		},


		{ "QUAD BIKE", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "CUATRO RUEDAS" }, 
				{ SystemLanguage.ChineseSimplified, "四轮摩托车" }, 
				{ SystemLanguage.ChineseTraditional, "四輪摩托車" }, 
				{ SystemLanguage.French, "HORS ROUTE" }, 
				{ SystemLanguage.Japanese, "四輪" }, 
				{ SystemLanguage.German, "QUAD-BIKE" }, 
				{ SystemLanguage.Indonesian, "QUAD BIKE" }, 
				{ SystemLanguage.Portuguese, "QUADRICÍCLO" }, 
				{ SystemLanguage.Italian, "FUORI STRADA" }, 
				{ SystemLanguage.Korean, "오프로드" }, 
				{ SystemLanguage.Russian, "КВАДРОЦИКЛ" }, 
			}
		},


		{ "WINDSURFER", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VIENTO NAVEGAR" }, 
				{ SystemLanguage.ChineseSimplified, "风帆冲浪" }, 
				{ SystemLanguage.ChineseTraditional, "風帆衝浪" }, 
				{ SystemLanguage.French, "DE LA PLANCHE A VOILE" }, 
				{ SystemLanguage.Japanese, "ウインドサーフィン" }, 
				{ SystemLanguage.German, "WIND SURFEN" }, 
				{ SystemLanguage.Indonesian, "SELANCAR ANGIN" }, 
				{ SystemLanguage.Portuguese, "SURF VENTO" }, 
				{ SystemLanguage.Italian, "SURF VENTO" }, 
				{ SystemLanguage.Korean, "바람" }, 
				{ SystemLanguage.Russian, "ВЕТЕР" }, 
			}
		},


		{ "RETRO GAME", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "juego retro" }, 
				{ SystemLanguage.ChineseSimplified, "复古游戏" }, 
				{ SystemLanguage.ChineseTraditional, "復古遊戲" }, 
				{ SystemLanguage.French, "JEU RETRO" }, 
				{ SystemLanguage.Japanese, "レトロゲーム" }, 
				{ SystemLanguage.German, "PERIODE SPIEL" }, 
				{ SystemLanguage.Indonesian, "PERTANDINGAN RETRO" }, 
				{ SystemLanguage.Portuguese, "JOGO RETRO" }, 
				{ SystemLanguage.Italian, "GIOCO RETRO" }, 
				{ SystemLanguage.Korean, "기간 게임" }, 
				{ SystemLanguage.Russian, "РЕТРО ИГРЫ" }, 
			}
		},


		{ "COOLING FAN", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "VENTILADOR" }, 
				{ SystemLanguage.ChineseSimplified, "冷风扇" }, 
				{ SystemLanguage.ChineseTraditional, "冷風扇" }, 
				{ SystemLanguage.French, "VENTILATEUR" }, 
				{ SystemLanguage.Japanese, "冷却ファン" }, 
				{ SystemLanguage.German, "LÜFTER" }, 
				{ SystemLanguage.Indonesian, "KIPAS PENDINGIN" }, 
				{ SystemLanguage.Portuguese, "VENTOINHA" }, 
				{ SystemLanguage.Italian, "VENTILATORE" }, 
				{ SystemLanguage.Korean, "냉각 팬" }, 
				{ SystemLanguage.Russian, "ВЕНТИЛЯТОР" }, 
			}
		},


		{ "NEKO EARS", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "OREJAS DE GATO" }, 
				{ SystemLanguage.ChineseSimplified, "猫耳" }, 
				{ SystemLanguage.ChineseTraditional, "貓耳" }, 
				{ SystemLanguage.French, "OREILLES DE CHAT" }, 
				{ SystemLanguage.Japanese, "猫耳" }, 
				{ SystemLanguage.German, "KATZENOHREN" }, 
				{ SystemLanguage.Indonesian, "TELINGA CAT" }, 
				{ SystemLanguage.Portuguese, "ORELHAS DE GATO" }, 
				{ SystemLanguage.Italian, "orecchie di gatto" }, 
				{ SystemLanguage.Korean, "모자고양이 귀" }, 
				{ SystemLanguage.Russian, "KOT УШИ" }, 
			}
		},


		{ "CAT HAT", 
			new Dictionary<SystemLanguage, string> {
				{ SystemLanguage.Spanish, "SOMBRERO GATO" }, 
				{ SystemLanguage.ChineseSimplified, "猫帽子" }, 
				{ SystemLanguage.ChineseTraditional, "貓帽子" }, 
				{ SystemLanguage.French, "CHAPEAU CHAT" }, 
				{ SystemLanguage.Japanese, "帽子ネコ" }, 
				{ SystemLanguage.German, "KATZE HUT" }, 
				{ SystemLanguage.Indonesian, "KUCING TOPI" }, 
				{ SystemLanguage.Portuguese, "CHAPÉU GATO" }, 
				{ SystemLanguage.Italian, "CAPPELLO GATTO" }, 
				{ SystemLanguage.Korean, "고양이" }, 
				{ SystemLanguage.Russian, "ШАПКА КОТ" }, 
			}
		},


	};


} // end class
