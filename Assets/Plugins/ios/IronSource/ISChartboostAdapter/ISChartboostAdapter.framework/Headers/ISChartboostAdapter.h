//
//  Copyright (c) 2015 IronSource. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

//System Frameworks For Chartboost Adapter
static NSString * const ChartboostAdapterVersion     = @"4.1.6";
static NSString *  GitHash = @"549038e";

@import Foundation;
@import CoreGraphics;
@import StoreKit;
@import UIKit;
@import WebKit;
@import AVFoundation;

@interface ISChartboostAdapter : ISBaseAdapter

@end
