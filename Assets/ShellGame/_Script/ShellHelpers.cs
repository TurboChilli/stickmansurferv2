﻿using UnityEngine;
using System.Collections;

public class ShellHelpers {

    public enum GUICorner
    {
        CORNER_TOP_LEFT,
        CORNER_TOP_RIGHT,
        CORNER_BOTTOM_LEFT,
        CORNER_BOTTOM_RIGHT
    };



    public static bool CheckButtonHit(Transform buttonCheck, Camera theCamera)
	{
		Vector2 touchPos = Input.mousePosition;
		RaycastHit hit;
      

        if (Physics.Raycast (theCamera.ScreenPointToRay(touchPos), out hit) )
        { 
            if(hit.transform.Equals(buttonCheck))
            {   
                return true;
            }
        }
        return false;
	}



	public static bool CheckButtonHitWithName(Transform buttonCheck, string buttonName)
	{
		Vector2 touchPos = Input.mousePosition;
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay(touchPos), out hit))
        { 	
        if(hit.transform.Equals(buttonCheck) && 
		string.Equals(hit.transform.gameObject.name, buttonName, System.StringComparison.InvariantCultureIgnoreCase))
	        {
	        return true;
	        }
        }
    return false;
	}



    public static void SetTransformRelativeToCorner(GUICorner theCorner, float xPaddingScreenPixels,
        float yPaddingScreenPixels, Transform theTransform, Camera theCamera)
    {
        SetTransformRelativeToCorner(theCorner, xPaddingScreenPixels, yPaddingScreenPixels, theTransform, theCamera, 0);
    }



    public static void SetTransformRelativeToCorner(GUICorner theCorner, float xPaddingScreenPixels,
        float yPaddingScreenPixels, Transform theTransform, Camera theCamera, float positionXOffSet)
    {
        //Screenspace is defined in pixels. The bottom-left of the screen is (0,0); the right-top is (pixelWidth,pixelHeight).
        Vector3 transPos = new Vector3(0,0,0);
        float prevZPos = theTransform.position.z;

        if(theCorner == GUICorner.CORNER_TOP_LEFT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (0, theCamera.pixelHeight, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet + xPaddingScreenPixels, transPos.y - yPaddingScreenPixels, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_TOP_RIGHT)
        {
            ////////Debug.Log("SetTransformRelativeToTopRight");
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth ,theCamera.pixelHeight, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet - xPaddingScreenPixels, transPos.y - yPaddingScreenPixels, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_BOTTOM_LEFT)
        {
            transPos = theCamera.ViewportToWorldPoint(new Vector3(0, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet + xPaddingScreenPixels, transPos.y + yPaddingScreenPixels, transPos.z);
        }
        else if(theCorner == GUICorner.CORNER_BOTTOM_RIGHT)
        {
            transPos = theCamera.ScreenToWorldPoint (new Vector3 (theCamera.pixelWidth, 0, theCamera.nearClipPlane));
            transPos = new Vector3(transPos.x + positionXOffSet - xPaddingScreenPixels, transPos.y + yPaddingScreenPixels, transPos.z);
        }

        theTransform.position = new Vector3(transPos.x, transPos.y, prevZPos); 
    }
}
