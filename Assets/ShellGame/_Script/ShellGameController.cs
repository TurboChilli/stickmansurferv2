﻿using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class ShellGameController : MonoBehaviour {

    private Vector3 leftAnchor = new Vector3 (0, 0, 0);
    private Vector3 middleAnchor = new Vector3 (0, 0, 0);
    private Vector3 rightAnchor = new Vector3 (0, 0, 0);

    private Vector3 startPos1 = new Vector3 (0, 0, 0);
    private Vector3 startPos2 = new Vector3 (0, 0, 0);
    private Vector3 startPos3 = new Vector3 (0, 0, 0);

    private Shell[] shells = new Shell[3];

    public ShellImageShadow shell1Shadow;
    public ShellImageShadow shell2Shadow;
    public ShellImageShadow shell3Shadow;

    public ParticleSystem coinParticles;
    public ParticleSystem canParticles;

    public Camera mainCamera;
    public Camera uiCamera;
    public Camera shellCamera;

    public GameObject background;

    public Shell shell1;
    public Shell shell2;
    public Shell shell3;

    private Shell losingShell1;
    private Shell losingShell2;

    public GameObject shellImage1;
    public GameObject shellImage2;
    public GameObject shellImage3;

    public GameObject prize1;
    public GameObject prize2;
    public GameObject prize3;

    public GameObject prizeImage1;
    public GameObject prizeImage2;
    public GameObject prizeImage3;
    public GameObject prizeImage4;
    public GameObject prizeImage5;
	public GameObject prizeImage6;
	public GameObject prizeImage7;

    List<GameObject> prizePoolArray = new List<GameObject>();

    private GameObject winningPrize;
    public GameObject prizeDestination;

    public TextMeshPro prize1Text;
    public TextMeshPro prize2Text;
    public TextMeshPro prize3Text;
    public TextMeshPro prize4Text;
	public TextMeshPro prize5Text;
	public TextMeshPro prize6Text;
    public TextMeshPro prize7Text;

    public TextMeshPro outputText;
    public TextMeshPro prizePanelText;
    private string prizeTextTemp = string.Empty;

    public NumericDisplayBar energyDrinkBar;

    public GameObject winPanel;
    private Vector3 winPanelDestination;
    private bool isMovingPanel = true;

    public AudioSource shellSwishSound;
    public AudioSource clickSound;
    public AudioSource prizeWinSound;

    //private Vector3 prizeDestination;

    private int totalSwaps;

    int swapCount = 0;
    bool isShellClicked = false;
    bool hasWonEnergyDrinks;

    Shell selectedShell = null;
    Shell selectedWinningShell = null;
    GameObject winningShell = null;

    private float curTimeToMoveAssets = 0.0f;
    private float timeToMoveAssets = 1.0f;

    private bool movingAssets = false;
    private bool isSwapFinished = false;

    private Vector3 panelActivePosition;
    private Vector3 panelInactivePosition;

    private Vector3 prizeActivePosition;
    private Vector3 prizeInactivePosition;

    private Vector3 origPrizeScale;
    private Vector3 addedPrizeScale;

    private Quaternion origPrizeRotation;

    private Random random;

    public enum GamePlayState 
    { 
        WaitingToStart,
        RevealAllPrizes,
        PauseAllShellsAtTop,
        LowerAllShells,
        Swapping, 
        SwappingComplete,
        PostGameSelectedReveal,
        ShowingGameResults,
        GameWin,
        GameLose, 
        GameOver
    };
    GamePlayState currGameState = GamePlayState.WaitingToStart;

    public enum Swaps
    {
        Swap_1_2 = 0,
        Swap_1_3 = 1,
        Swap_2_3 = 2
    };

    public enum WinningShell
    {
        WinningShell1 = 0,
        WinningShell2 = 1,
        WinningShell3 = 2
    }

    public enum DifficultyLevel
    {
        Easy,
        Medium,
        Hard
    };

    public enum Prize
    {
        FiftyCoins,
        OneHundredCoins,
        FiveHundredCoins,
        OneThousandCoins,
        TwoThousandCoins,
        EnergyDrinkOne,
        EnergyDrinkTwo
    };
    Prize currPrizeEnum;

	private int[] PrizeAmounts 
    {
    	get 
    	{
			if (_PrizeAmounts == null)
    		{
				_PrizeAmounts = new int[7];

				_PrizeAmounts[0] = GameServerSettings.SharedInstance.SurferSettings.ShellGameReward50;
				_PrizeAmounts[1] = GameServerSettings.SharedInstance.SurferSettings.ShellGameReward100;
				_PrizeAmounts[2] = GameServerSettings.SharedInstance.SurferSettings.ShellGameReward500;
				_PrizeAmounts[3] = GameServerSettings.SharedInstance.SurferSettings.ShellGameReward1000;
				_PrizeAmounts[4] = GameServerSettings.SharedInstance.SurferSettings.ShellGameReward2000;
				_PrizeAmounts[5] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardE1;
				_PrizeAmounts[6] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardE2;

			}
			return _PrizeAmounts;	
    	}
    }



    private float[] PrizePercentages
    {
    	get 
    	{
	    	if (_prizePercentages == null)
	    	{
	    		_prizePercentages = new float[7];

				_prizePercentages[0] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentage50;
				_prizePercentages[1] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentage100;
				_prizePercentages[2] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentage500;
				_prizePercentages[3] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentage1000;
				_prizePercentages[4] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentage2000;
				_prizePercentages[5] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentageE1;
				_prizePercentages[6] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardPercentageE2;
	    	}
			return _prizePercentages;
		}
	}

    private float[] _prizePercentages = null;
    private int[] _PrizeAmounts = null; 

    public AudioSource reggaeMusic;
    public AudioSource waveSounds;

    // Use this for initialization
    void Start () 
    {  
        if (GlobalSettings.musicEnabled)
            GlobalSettings.PlayMusicFromTimePoint(reggaeMusic, GlobalSettings.reggaeMusicPlayPosition);
        else 
        {
            reggaeMusic.Stop();
            waveSounds.Stop();
        }

        //Application.targetFrameRate = 60;

        prizeImage1.SetActive(false);
        prizeImage2.SetActive(false);
        prizeImage3.SetActive(false);
        prizeImage4.SetActive(false);
		prizeImage5.SetActive(false);
		prizeImage6.SetActive(false);
        prizeImage7.SetActive(false);

        energyDrinkBar.gameObject.SetActive(false);

        if(GameState.IsFirstShellGame())
        {
            SetDifficulty(DifficultyLevel.Easy);
            totalSwaps = GameState.SharedInstance.NumberOfSwapsOnShellgame;

            prizePoolArray.Add(prizeImage1);
            prizePoolArray.Add(prizeImage2);
            prizePoolArray.Add(prizeImage7);

            RandomizeAndActivatePrizes();
        }
        else
        {
            SetDifficulty(DifficultyLevel.Hard);
            totalSwaps = GameState.SharedInstance.NumberOfSwapsOnShellgame;

            int[] prizes = new int[3] 
            {
				RandomReward(),
				RandomReward(),
				RandomReward()
            };

            while (prizes[1] == prizes[0])
				prizes[1] = (prizes[1] + 1) % PrizeAmounts.Length;
			while (prizes[2] == prizes[0] || prizes[2] == prizes[1])
				prizes[2] = (prizes[2] + 1) % PrizeAmounts.Length;

			for (int i = 0; i < 3; i++)
			{
				switch (prizes[i])
				{
					case 0: prizePoolArray.Add(prizeImage1);  break;
					case 1: prizePoolArray.Add(prizeImage2);  break;
					case 2: prizePoolArray.Add(prizeImage3);  break;
					case 3: prizePoolArray.Add(prizeImage4);  break;
					case 4: prizePoolArray.Add(prizeImage5);  break;
					case 5: prizePoolArray.Add(prizeImage6);  break;
					case 6: prizePoolArray.Add(prizeImage7);  break;
				}
			}
		
            RandomizeAndActivatePrizes();
        }

		int prizePercent1 =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[0]);
		int prizePercent2 =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[1]);
		int prizePercent3 =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[2]);
		int prizePercent4 =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[3]);
		int prizePercent5 =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[4]);

		prize1Text.text = ((prizePercent1 > PrizeAmounts[0]) ? prizePercent1 : PrizeAmounts[0]).ToString();
		prize2Text.text = ((prizePercent1 > PrizeAmounts[1]) ? prizePercent2 : PrizeAmounts[1]).ToString();
		prize3Text.text = ((prizePercent1 > PrizeAmounts[2]) ? prizePercent3 : PrizeAmounts[2]).ToString();
		prize4Text.text = ((prizePercent1 > PrizeAmounts[3]) ? prizePercent4 : PrizeAmounts[3]).ToString();
		prize5Text.text = ((prizePercent1 > PrizeAmounts[4]) ? prizePercent5 : PrizeAmounts[4]).ToString();
		prize6Text.text = PrizeAmounts[5].ToString();
		prize7Text.text = (GameState.IsFirstShellGame()) ?
							GameServerSettings.SharedInstance.SurferSettings.FirstEnergyDrinkPrize.ToString() :
							PrizeAmounts[6].ToString();

        hasWonEnergyDrinks = false;

        //Initialize position of anchor points
        //Left anchor point
        leftAnchor.x = shell1.transform.position.x + ((shell2.transform.position.x - shell1.transform.position.x) / 2);
        leftAnchor.y = shell1.transform.position.y;
        leftAnchor.z = shell1.transform.position.z;

        //Middle anchor point
        middleAnchor.x = shell2.transform.position.x;
        middleAnchor.y = shell2.transform.position.y;
        middleAnchor.z = shell2.transform.position.z;

        //Right anchor point
        rightAnchor.x = shell2.transform.position.x + ((shell3.transform.position.x - shell2.transform.position.x) / 2);
        rightAnchor.y = shell2.transform.position.y;
        rightAnchor.z = shell2.transform.position.z;

        //Initialize our start positons with their logical positions
        startPos1 = shell1.transform.position;
        startPos2 = shell2.transform.position;
        startPos3 = shell3.transform.position;

        //Linking our 3 shells to the array slots
        shells[0] = shell1;
        shells[1] = shell2;
        shells[2] = shell3;

        //Assign each shell an ID
        shell1.shellId = 0;
        shell2.shellId = 1;
        shell3.shellId = 2;

        ShellReset();

        shell1.CurrentRevealState = Shell.RevealState.RevealMovingUp;
        shell2.CurrentRevealState = Shell.RevealState.RevealMovingUp;
        shell3.CurrentRevealState = Shell.RevealState.RevealMovingUp;
    }        

    public int RandomReward()
    {
		int[] weights = GetRandomWeights();
		int randValue = Random.Range(0, weights[weights.Length - 1] + 1);

		for (int i = 0 ; i < weights.Length; i++)
		{
			if (randValue <= weights[i])
			{
				return i;
			}
		}
		return 0;
    }

    public int[] GetRandomWeights()
    {
    	int[] randomWeights = new int[7];

		randomWeights[0] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeight50;
		randomWeights[1] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeight100 + randomWeights[0];
		randomWeights[2] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeight500 + randomWeights[1];
		randomWeights[3] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeight1000 + randomWeights[2];
		randomWeights[4] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeight2000 + randomWeights[3];
		randomWeights[5] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeightE1 + randomWeights[4];
		randomWeights[6] = GameServerSettings.SharedInstance.SurferSettings.ShellGameRewardWeightE2 + randomWeights[5];


		return randomWeights;
    }


	void CheckAndroidBackButton()
	{
		#if UNITY_ANDROID || UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape))
		{
            #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
            #endif
            redirectToMenuSoon = true;
		}
		#endif 
	}


    // Update is called once per frame
    void Update () 
    {
        if (redirectToMenuSoon)
        {
            redirectTimer += Time.deltaTime;
            if (redirectTimer > redirectDelay)
            {
                redirectTimer = 0;
                SceneNavigator.NavigateTo(SceneType.Menu);
                redirectToMenuSoon = false;
            }
        }
		CheckAndroidBackButton();

        // start game, move all shells to the top
        if (currGameState == GamePlayState.WaitingToStart) 
        {
            currGameState = GamePlayState.RevealAllPrizes;
        }
        if (currGameState == GamePlayState.RevealAllPrizes)
        {
            shell1.UpdatePosition ();
            shell2.UpdatePosition ();
            shell3.UpdatePosition ();

            if (shell1.CurrentRevealState == Shell.RevealState.RevealPauseAtTop
                && shell2.CurrentRevealState == Shell.RevealState.RevealPauseAtTop
                && shell3.CurrentRevealState == Shell.RevealState.RevealPauseAtTop)
            {
                currGameState = GamePlayState.PauseAllShellsAtTop;
				outputText.text = GameLanguage.LocalizedText("TAP TO PLAY");
            }
        }
        // shells pause at top, in ready to move down state
        else if (currGameState == GamePlayState.PauseAllShellsAtTop)
        {
            ////////Debug.Log("2 CURR GAME STATE: " + currGameState);
            if (Input.GetMouseButtonDown (0)) 
            {
                clickSound.Play();
                currGameState = GamePlayState.LowerAllShells;
                shell1.CurrentRevealState = Shell.RevealState.RevealMoveBackDown;
                shell2.CurrentRevealState = Shell.RevealState.RevealMoveBackDown;
                shell3.CurrentRevealState = Shell.RevealState.RevealMoveBackDown;
                outputText.text = string.Empty;
            }
        }
        // lower all the shells and swap them
        else if (currGameState == GamePlayState.LowerAllShells)
        {
            shell1.UpdatePosition ();
            shell2.UpdatePosition ();
            shell3.UpdatePosition ();

            if (shell1.CurrentRevealState == Shell.RevealState.RevealPauseAtBottom
                && shell2.CurrentRevealState == Shell.RevealState.RevealPauseAtBottom
                && shell3.CurrentRevealState == Shell.RevealState.RevealPauseAtBottom)
            {
                currGameState = GamePlayState.Swapping;
            }
        }
        else if (currGameState == GamePlayState.Swapping) 
        {
            ////////Debug.Log("3 CURR GAME STATE: " + currGameState);
            MoveShells();
        } 
        // click on a shell to move it upwards after shell shuffle is complete
        else if (currGameState == GamePlayState.SwappingComplete) 
        {
            if (Input.GetMouseButtonDown (0)) 
            {
                if (ShellHelpers.CheckButtonHit (shellImage1.transform, shellCamera))
                {                    
                    clickSound.Play();
                    isShellClicked = true;

                    selectedShell = shell1;  
                    losingShell1 = shell2;
                    losingShell2 = shell3;

                    SetActivateWinResults(prize1);
                }
                if (ShellHelpers.CheckButtonHit (shellImage2.transform, shellCamera))
                {                   
                    clickSound.Play();
                    isShellClicked = true;

                    selectedShell = shell2;
                    losingShell1 = shell1;
                    losingShell2 = shell3;

                    SetActivateWinResults(prize2);
                }
                if (ShellHelpers.CheckButtonHit (shellImage3.transform, shellCamera))
                {
                    clickSound.Play();
                    isShellClicked = true;

                    selectedShell = shell3;
                    losingShell1 = shell1;
                    losingShell2 = shell2;

                    SetActivateWinResults(prize3);
                }
                if(isShellClicked)
                {
                    selectedShell.CurrentRevealState = Shell.RevealState.PostSwap;
                    outputText.text = "";
                    currGameState = GamePlayState.PostGameSelectedReveal;
                    InitWinAssetPositions();
                }

				GameState.SharedInstance.AddShellGamePlayed();
            }
        } 
        else if (currGameState == GamePlayState.PostGameSelectedReveal) 
        {  
            if (selectedShell.CurrentRevealState == Shell.RevealState.RevealPauseAtTop)
            {
                losingShell1.CurrentRevealState = Shell.RevealState.PostSwap;
                losingShell2.CurrentRevealState = Shell.RevealState.PostSwap;

                prizeWinSound.Play();
                currGameState = GamePlayState.GameOver;

                if((currPrizeEnum == Prize.FiftyCoins || currPrizeEnum == Prize.OneHundredCoins) 
                    && GameState.SharedInstance.NumberOfSwapsOnShellgame != ShellGameGlobals.easyAmountOfSwaps)
                {
                    GameState.SharedInstance.MinusSwapFromShellGame();
                }
                else
                {
                    GameState.SharedInstance.AddSwapToShellGame();
                }
            }

            float lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / timeToMoveAssets);

            curTimeToMoveAssets -= Time.deltaTime;
            if (movingAssets && curTimeToMoveAssets <= 0)
            {
                lerpAmount = 0;
                movingAssets = false;
                // play particles here
				if (currPrizeEnum == Prize.EnergyDrinkOne || currPrizeEnum == Prize.EnergyDrinkTwo)
                {
                    
                    canParticles.transform.position = new Vector3(prizeInactivePosition.x, prizeInactivePosition.y, prizeInactivePosition.z);
                    canParticles.Emit(30);
                }
                else
                {
                    ////////Debug.Log("SETTING COIN PARTICLES POSITION TO:" + prizeInactivePosition);
                    coinParticles.transform.position = new Vector3(prizeInactivePosition.x, prizeInactivePosition.y, prizeInactivePosition.z);
                    coinParticles.Emit(50);
                }

            }
             
            if(movingAssets)
            {
                //winPanel.transform.position = Vector3.Lerp(panelActivePosition, panelInactivePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
                winningPrize.transform.position = Vector3.Lerp(prizeActivePosition, prizeInactivePosition, 1.0f - Mathf.Pow(lerpAmount, 3.0f));
                winningPrize.transform.localScale = Vector3.Lerp(origPrizeScale, addedPrizeScale, 1.0f - Mathf.Pow(lerpAmount, 2.0f));
                //Quaternion jitterRotation = Quaternion.AngleAxis((Mathf.Sin(Time.time * 60.0f) * 4.0f), Vector3.forward );
                //winningPrize.transform.rotation = Quaternion.Slerp(origPrizeRotation, jitterRotation, Mathf.Pow(lerpAmount, 2.0f));
            }

            selectedShell.UpdatePosition();
        }
        // If swapping follow the shell
        if (currGameState == GamePlayState.Swapping || currGameState == GamePlayState.SwappingComplete)
        {
            shellImage1.transform.position = new Vector3 (shell1.transform.position.x, shell1.transform.position.y, shellImage1.transform.position.z);
            shellImage2.transform.position = new Vector3 (shell2.transform.position.x, shell2.transform.position.y, shellImage2.transform.position.z);
            shellImage3.transform.position = new Vector3 (shell3.transform.position.x, shell3.transform.position.y, shellImage3.transform.position.z);

            prize1.transform.position = new Vector3 (shell1.transform.position.x, shell1.transform.position.y + 2.2f, shell1.transform.position.z);
            prize2.transform.position = new Vector3 (shell2.transform.position.x, shell2.transform.position.y + 2.2f, shell2.transform.position.z);
            prize3.transform.position = new Vector3 (shell3.transform.position.x, shell3.transform.position.y + 2.2f, shell3.transform.position.z);
        }
        // Move shell images to their logical positions
        if (currGameState != GamePlayState.WaitingToStart)
        {
            shellImage1.transform.position = new Vector3 (shell1.transform.position.x, shell1.transform.position.y, shell1.transform.position.z);
            shellImage2.transform.position = new Vector3 (shell2.transform.position.x, shell2.transform.position.y, shell2.transform.position.z);
            shellImage3.transform.position = new Vector3 (shell3.transform.position.x, shell3.transform.position.y, shell3.transform.position.z);
        }
        if (currGameState == GamePlayState.GameOver)
        {  
            losingShell1.UpdatePosition();
            losingShell2.UpdatePosition();

            if (Input.GetMouseButtonDown (0)) 
            {    
                clickSound.Play();
                #if UNITY_ANDROID
                AndroidLoadingPanel droidLoader = (AndroidLoadingPanel)mainCamera.GetComponentInChildren( typeof(AndroidLoadingPanel), true );

                if(droidLoader != null)
                {
                    droidLoader.Show();
                }
                #endif
				redirectToMenuSoon = true;
                
                
            }
        }

        shell1Shadow.UpdateShadow();
        shell2Shadow.UpdateShadow();
        shell3Shadow.UpdateShadow();
    }

    bool redirectToMenuSoon = false;
    float redirectTimer = 0;
    float redirectDelay = 0.4f;

    public void ResetGame()
    {
        ShellReset();
        currGameState = GamePlayState.WaitingToStart;
		outputText.text = GameLanguage.LocalizedText("TAP TO PLAY");
        swapCount = 0;
    }

    public void ShellReset()
    {
        shells[0] = shell1;
        shells[1] = shell2;
        shells[2] = shell3;

        shell1.transform.position = startPos1;
        shell2.transform.position = startPos2;
        shell3.transform.position = startPos3;

        shellImage1.transform.position = new Vector3 (shell1.transform.position.x, shell1.transform.position.y, shell1.transform.position.z);
        shellImage2.transform.position = new Vector3 (shell2.transform.position.x, shell2.transform.position.y, shell2.transform.position.z);
        shellImage3.transform.position = new Vector3 (shell3.transform.position.x, shell3.transform.position.y, shell3.transform.position.z);

        // Randomize winning shell placement
        int randomShell = Random.Range (0, 3);

        if (randomShell == (int)WinningShell.WinningShell1)
        {
            winningShell = shellImage1;
            selectedWinningShell = shell1;
        }
        else if (randomShell == (int)WinningShell.WinningShell2)
        {
            winningShell = shellImage2;
            selectedWinningShell = shell2;
        }
        else if (randomShell == (int)WinningShell.WinningShell3)
        {
            winningShell = shellImage3;
            selectedWinningShell = shell3;
        }
        shells[0].PreRevealZPosition = selectedWinningShell.transform.position.z;
        shells[1].PreRevealZPosition = selectedWinningShell.transform.position.z;
        shells[2].PreRevealZPosition = selectedWinningShell.transform.position.z;
    }

    public void StartRandomSwap()
    {
        int randomIndex = Random.Range (0, 3);
        Swaps swapEnum = (Swaps)randomIndex;

        int randomDirection = Random.Range (0, 2);
        bool clockwise = false;

        if (randomDirection == 0)
        {
            clockwise = false;
        }
        else if (randomDirection == 1)
        {
            clockwise = true;
        }
        if (swapEnum == Swaps.Swap_1_2) 
        {
            StartSwapShells (0, 1, clockwise);
        }
        else if (swapEnum == Swaps.Swap_1_3) 
        {
            StartSwapShells (0, 2, clockwise);
        }
        else if (swapEnum == Swaps.Swap_2_3) 
        {
            StartSwapShells (1, 2, clockwise);
        }
        swapCount++;
    }

    public void StartSwapShells(int shellPos1, int shellPos2, bool clockwise)
    {
        // Set dest points for shells 1 and 2
        if (shellPos1 == 0 && shellPos2 == 1) 
        {
            // set current anchor point to be between 1 and 2
            shells[0].Anchor = leftAnchor;
            shells[1].Anchor = leftAnchor;

            // marking if shell is moving to a destination point with X pos to the right of this shells starting point
            // used only for low fps collision detection system
            shells[0].SetMovingRight(true);
            shells[1].SetMovingRight(false);
            // set the rotation direction
            shells[0].Clockwise = clockwise;
            shells[1].Clockwise = clockwise;

            // set dest point for shell 1 (will be position 2)
            shells[0].Destination = startPos2;
            // set dest point for shell 2 (will be position 1)
            shells[1].Destination = startPos1;

            shells[0].Moving = true;
            shells[1].Moving = true;

            // swap shells
            Shell tempShell = shells[0];
            shells[0] = shells [1];
            shells[1] = tempShell;
        }
        // Set dest points for shells 2 and 3
        if (shellPos1 == 1 && shellPos2 == 2) 
        {
            // set current anchor point to be between 2 and 3
            shells[1].Anchor = rightAnchor;
            shells[2].Anchor = rightAnchor;

            // marking if shell is moving to a destination point with X pos to the right of this shells starting point
            // used only for low fps collision detection system
            shells[1].SetMovingRight(true);
            shells[2].SetMovingRight(false);
            // set the rotation direction
            shells[1].Clockwise = clockwise;
            shells[2].Clockwise = clockwise;

            // set dest point for shell 2 (will be position 3)
            shells[1].Destination = startPos3;
            // set dest point for shell 3 (will be position 2)
            shells[2].Destination = startPos2;

            shells[1].Moving = true;
            shells[2].Moving = true;

            // swap shells
            Shell tempShell = shells[1];
            shells[1] = shells [2];
            shells[2] = tempShell;
        }
        // Set destination point for shells 1 and 3
        if (shellPos1 == 0 && shellPos2 == 2) 
        {
            // set current anchor point to be between 1 and 3
            shells[0].Anchor = middleAnchor;
            shells[2].Anchor = middleAnchor;

            // marking if shell is moving to a destination point with X pos to the right of this shells starting point
            // used only for low fps collision detection system
            shells[0].SetMovingRight(true);
            shells[2].SetMovingRight(false);

            // set the rotation direction
            shells[0].Clockwise = clockwise;
            shells[2].Clockwise = clockwise;

            // set dest point for shell 1 (will be position 3)
            shells[0].Destination = startPos3;
            // set dest point for shell 3 (will be position 1)
            shells[2].Destination = startPos1;

            shells[0].Moving = true;
            shells[2].Moving = true;

            // swap shells
            Shell tempShell = shells[0];
            shells[0] = shells[2];
            shells[2] = tempShell;
        }
        // switch current game state to swapping
        currGameState = GamePlayState.Swapping;

    }

    public void MoveShells()
    {
        for (int i = 0; i < shells.Length; i++) 
        {
            shells[i].UpdatePosition();
        }
        bool allShellsFinishedMoving = true;
        for (int i = 0; i < shells.Length; i++) 
        {
            if (shells[i].Moving)
            {
                allShellsFinishedMoving = false;
            }
        }
        if (allShellsFinishedMoving) 
        {
            if (swapCount < totalSwaps) 
            {
                StartRandomSwap ();
                shellSwishSound.Play();
            } 
            else 
            {
                currGameState = GamePlayState.SwappingComplete;
				outputText.text = GameLanguage.LocalizedText("PICK A SHELL");
            }
        }
    }

    public void SetDifficulty(DifficultyLevel level)
    {
        if (level == DifficultyLevel.Easy)
        {
            //totalSwaps = ShellGameGlobals.easyAmountOfSwaps;
            shell1.CurrentSwapSpeed = Shell.SwapSpeed.Slow;
            shell2.CurrentSwapSpeed = Shell.SwapSpeed.Slow;
            shell3.CurrentSwapSpeed = Shell.SwapSpeed.Slow;
        }
        else if (level == DifficultyLevel.Medium)
        {
            //totalSwaps = ShellGameGlobals.mediumAmountOfSwaps;
            shell1.CurrentSwapSpeed = Shell.SwapSpeed.Medium;
            shell2.CurrentSwapSpeed = Shell.SwapSpeed.Medium;
            shell3.CurrentSwapSpeed = Shell.SwapSpeed.Medium;
        }
        else if (level == DifficultyLevel.Hard)
        {
            //totalSwaps = ShellGameGlobals.hardAmountOfSwaps;
            shell1.CurrentSwapSpeed = Shell.SwapSpeed.Fast;
            shell2.CurrentSwapSpeed = Shell.SwapSpeed.Fast;
            shell3.CurrentSwapSpeed = Shell.SwapSpeed.Fast;
        }
    }

    public void ShufflePrizePool(List<GameObject> prizes)
    {
        for (int i = 0; i < prizes.Count; i++)
        {
            GameObject tmp = prizes[i];
            int randIndex = Random.Range(i, prizes.Count);
            prizes[i] = prizes[randIndex];
            prizes[randIndex] = tmp;
        }
    }

    public void RandomizeAndActivatePrizes()
    {
        ShufflePrizePool(prizePoolArray);

        prize1 = prizePoolArray[0];
        prize2 = prizePoolArray[1];
        prize3 = prizePoolArray[2];

        prize1.SetActive(true);
        prize2.SetActive(true);
        prize3.SetActive(true);

        prize1.transform.position = new Vector3 (shell1.transform.position.x, shell1.transform.position.y + 6, shell1.transform.position.z);
        prize2.transform.position = new Vector3 (shell2.transform.position.x, shell2.transform.position.y + 6, shell2.transform.position.z);
        prize3.transform.position = new Vector3 (shell3.transform.position.x, shell3.transform.position.y + 6, shell3.transform.position.z); 
    }

    public void SetActivateWinResults(GameObject currPrize)
    {
        if(currPrize == prizeImage1)
        {            
            winningPrize = prizeImage1;
            currPrizeEnum = Prize.FiftyCoins;
            //ShowHidePrizeAmounts(currPrizeEnum);


			int prizePercent =  GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[0]);
			GameState.SharedInstance.AddCash((prizePercent > PrizeAmounts[0]) ? prizePercent : PrizeAmounts[0]);
        }
        else if(currPrize == prizeImage2)
        {
            winningPrize = prizeImage2;
            currPrizeEnum = Prize.OneHundredCoins;

			int prizePercent = GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[1]);
			GameState.SharedInstance.AddCash((prizePercent > PrizeAmounts[1]) ? prizePercent : PrizeAmounts[1]);
        }
        else if(currPrize == prizeImage3)
        {
            winningPrize = prizeImage3;
            currPrizeEnum = Prize.FiveHundredCoins;


			int prizePercent = GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[2]);
			GameState.SharedInstance.AddCash((prizePercent > PrizeAmounts[2]) ? prizePercent : PrizeAmounts[2]);

        }
        else if(currPrize == prizeImage4)
        {
            winningPrize = prizeImage4;
            currPrizeEnum = Prize.OneThousandCoins;
           // ShowHidePrizeAmounts(currPrizeEnum);


			int prizePercent = GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[3]);
			GameState.SharedInstance.AddCash((prizePercent > PrizeAmounts[3]) ? prizePercent : PrizeAmounts[3]);
        }
        else if (currPrize == prizeImage5)
        {
			winningPrize = prizeImage5;
            currPrizeEnum = Prize.TwoThousandCoins;
           // ShowHidePrizeAmounts(currPrizeEnum);

			int prizePercent = GameState.SharedInstance.GetNicePercentageCostOfNextMinUpgradeItem(PrizePercentages[4]);
			GameState.SharedInstance.AddCash((prizePercent > PrizeAmounts[4]) ? prizePercent : PrizeAmounts[4]);
        }
		else if (currPrize == prizeImage6)
        {
			winningPrize = prizeImage6;
            currPrizeEnum = Prize.EnergyDrinkOne;
            // ShowHidePrizeAmounts(currPrizeEnum);
			hasWonEnergyDrinks = true;
           
			GameState.SharedInstance.AddEnergyDrink(PrizeAmounts[5]);
			energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink, true);

        }
		else if (currPrize == prizeImage7)
        {
			winningPrize = prizeImage7;
            currPrizeEnum = Prize.EnergyDrinkTwo;
           // ShowHidePrizeAmounts(currPrizeEnum);
			hasWonEnergyDrinks = true;

            energyDrinkBar.gameObject.SetActive(true);

            if (GameState.IsFirstShellGame())
            {
				GameState.SharedInstance.AddEnergyDrink(GameServerSettings.SharedInstance.SurferSettings.FirstEnergyDrinkPrize);
			}	
			else
				GameState.SharedInstance.AddEnergyDrink(PrizeAmounts[6]);
			

            energyDrinkBar.DisplayValue(GameState.SharedInstance.EnergyDrink, true);
        }

    }

    public void ShowHidePrizeAmounts(Prize currPrizeEnum)
    {
        if(currPrizeEnum == Prize.FiftyCoins)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[0], GameLanguage.LocalizedText("COINS"));
            prize1Text.text = "";
        }
        else if(currPrizeEnum == Prize.OneHundredCoins)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[1], GameLanguage.LocalizedText("COINS"));
            prize2Text.text = "";
        }
        else if(currPrizeEnum == Prize.FiveHundredCoins)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[2], GameLanguage.LocalizedText("COINS"));
            prize3Text.text = "";
        }
        else if(currPrizeEnum == Prize.OneThousandCoins)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[3], GameLanguage.LocalizedText("COINS"));
            prize4Text.text = "";
        }
		else if(currPrizeEnum == Prize.OneThousandCoins)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[4], GameLanguage.LocalizedText("COINS"));
            prize5Text.text = "";
        }
        else if(currPrizeEnum == Prize.EnergyDrinkOne)
        {
			prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[5], GameLanguage.LocalizedText("ENERGY DRINKS"));
            prize6Text.text = "";  
        }
		else if(currPrizeEnum == Prize.EnergyDrinkTwo)
        {
			if (GameState.IsFirstShellGame())
				prizePanelText.text = string.Format("{0} {1}", GameServerSettings.SharedInstance.SurferSettings.FirstEnergyDrinkPrize, GameLanguage.LocalizedText("ENERGY DRINKS"));
			else
				prizePanelText.text = string.Format("{0} {1}", PrizeAmounts[6], GameLanguage.LocalizedText("ENERGY DRINKS"));

            prize7Text.text = "";  
        }
    }

    public void InitWinAssetPositions()
    {
        movingAssets = true;

        curTimeToMoveAssets = timeToMoveAssets;
        origPrizeRotation = prize1.transform.rotation;

        winPanelDestination = new Vector3(50, -184, 40);

        panelActivePosition = winPanel.transform.position; 
        panelInactivePosition = panelActivePosition + Vector3.back * 40;

        prizeActivePosition = winningPrize.transform.position;
        prizeInactivePosition = new Vector3(prizeActivePosition.x, prizeDestination.transform.position.y, prizeDestination.transform.position.z);

        //prizePanelText.transform.position = new Vector3(prizeActivePosition.x, prizePanelText.transform.position.y, prizePanelText.transform.position.z);

        origPrizeScale = prize1.transform.localScale;
        addedPrizeScale = prize1.transform.localScale + Vector3.one * 4;
    }
}
