﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
        GenerateDifficultyLevel();
	}
	
    // Random generate game difficulty level
    public void GenerateDifficultyLevel()
    {
        int levelOfDifficulty = Random.Range (0, 3);
        //Application.targetFrameRate = 60;
       
        ShellGameController.DifficultyLevel currLevel = ShellGameController.DifficultyLevel.Easy;

        switch (levelOfDifficulty)
        {
            case 0:
                currLevel = ShellGameController.DifficultyLevel.Easy;
                break;
            case 1:
                currLevel = ShellGameController.DifficultyLevel.Medium;
                break;
            case 2:
                currLevel = ShellGameController.DifficultyLevel.Hard;
                break;
        }

        ShellGameGlobals.currentDifficultyLevel = currLevel;
    }
}
