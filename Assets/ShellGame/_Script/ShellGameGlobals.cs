﻿using UnityEngine;
using System.Collections;

public class ShellGameGlobals : MonoBehaviour {

    public static ShellGameController.DifficultyLevel currentDifficultyLevel = ShellGameController.DifficultyLevel.Medium;
    public static float easySpeed = 700;
    public static float mediumSpeed = 800;
    public static float hardSpeed = 900;

    public static int easyAmountOfSwaps = 6;
    public static int mediumAmountOfSwaps = 8;
    public static int hardAmountOfSwaps = 10;

    public static float upDownMovementSpeed = 10;
    public static float gameResultPanelSlideSpeed = 800;
    public static float shellPauseTopPos = -11;
    public static float panelPauseRightPos = -5;
}
    