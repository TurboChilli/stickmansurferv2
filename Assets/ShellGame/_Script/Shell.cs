﻿using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour {

    private Vector3 destinationPoint;
    private Vector3 anchorPoint;
    private bool isMovingGame = false;
    private bool isMovingUp = false;
    public int shellId = -1;
    private bool clockwise = false;
    private bool movingLeftToRight = false;
    private float preRevealZPosition = 0;
    public ShellGameController shellGameController;
    public ShellImageShadow shellShadow;

    private float curTimeToMoveAssets = 0.0f;
    private float timeToMoveAssets = 1.0f;
    private Vector3 shellActivePosition;
    private Vector3 shellInactivePosition;
    private bool movingAssets = false;
    float lerpAmount = 0;
    private bool isSwapFinished = false;


    private Vector3 localScaleShellShadow = new Vector3(0,0,0);
    private Vector3 localScaleShellShadowLarger = new Vector3(0,0,0);


    public enum RevealState
    {
        RevealMovingUp,
        RevealPauseAtTop,
        RevealMoveBackDown,
        RevealPauseAtBottom, 
        RevealNone,
        PostSwap
    }
    private RevealState currentRevealState = RevealState.RevealNone;

    public enum SwapSpeed
    {
        Slow,
        Medium,
        Fast
    }
    private SwapSpeed currentSwapSpeed = SwapSpeed.Slow;

    public RevealState CurrentRevealState
    {
        get
        {
            return currentRevealState;
        }
        set
        {
            curTimeToMoveAssets = timeToMoveAssets;
            currentRevealState = value;
        }
    }

    public SwapSpeed CurrentSwapSpeed
    {
        get
        {
            return currentSwapSpeed;
        }
        set
        {
            currentSwapSpeed = value;
        }

    }

    public float PreRevealZPosition
    {
        get
        {
            return preRevealZPosition;
        }
        set
        {
            preRevealZPosition = value;
        }
    }

    public Vector3 Destination
    {
        get 
        {
            return destinationPoint;
        }
        set
        {
            this.destinationPoint = value;
        }
    }

    public bool Clockwise
    {
        get 
        {
            return clockwise;
        }
        set
        {
            this.clockwise = value;
        }
    }

    public Vector3 Anchor
    {
        get 
        {
            return anchorPoint;
        }
        set
        {
            this.anchorPoint = value;
        }
    }

    public void SetMovingRight(bool right)
    {
        movingLeftToRight = right;
    }

    public bool Moving
    {
        get 
        {
            return isMovingGame;
        }
        set
        {
            this.isMovingGame = value;
        }
    }

    public bool MovingUp
    {
        get 
        {
            return isMovingUp;
        }
        set
        {
            this.isMovingUp = value;
        }
    }

    private float GetSwapSpeedValue()
    {
        float swapSpeedValue = 0;
        if (currentSwapSpeed == SwapSpeed.Slow)
        {
            swapSpeedValue = ShellGameGlobals.easySpeed;
        }
        else if (currentSwapSpeed == SwapSpeed.Medium)
        {
            swapSpeedValue = ShellGameGlobals.mediumSpeed;
        }
        else if (currentSwapSpeed == SwapSpeed.Fast)
        {
            swapSpeedValue = ShellGameGlobals.hardSpeed;
        }
        return swapSpeedValue;
    }


    // Use this for initialization
    void Start () {
       
        localScaleShellShadow = shellShadow.transform.localScale;
        localScaleShellShadowLarger = new Vector3(localScaleShellShadow.x * 1.1f, localScaleShellShadow.y * 1.1f, shellShadow.transform.localScale.z);

        shellActivePosition = this.transform.position;
        shellInactivePosition = shellActivePosition + Vector3.back * 10;

        movingAssets = true;
    }

    // UpdatePosition is called once per frame
    public void UpdatePosition () 
    {
        float rotationSpeed = GetSwapSpeedValue();

        if (Clockwise == true)
        {
            rotationSpeed = -rotationSpeed;
        }
        else if (Clockwise == false)
        {
            rotationSpeed = rotationSpeed;
        }
        // Moving shells around
        if (isMovingGame)       
        {
            if(isMovingGame)
            {
                // Move this shell to destination
                this.transform.RotateAround (anchorPoint, new Vector3 (0, 0, 1), rotationSpeed * Time.deltaTime);
            }
            if(Clockwise)
            {
                if (movingLeftToRight && transform.position.y < destinationPoint.y)
                {
                    // Snap to position
                    this.transform.position = destinationPoint;
                    isMovingGame = false;
                }
                else if (!movingLeftToRight && transform.position.y > destinationPoint.y)
                {
                    // Snap to position
                    this.transform.position = destinationPoint;
                    isMovingGame = false;
                }
            }               
            else
            {
                if (movingLeftToRight && transform.position.y > destinationPoint.y)
                {
                    // Snap to position
                    this.transform.position = destinationPoint;
                    isMovingGame = false;
                }
                else if (!movingLeftToRight && transform.position.y < destinationPoint.y)
                {
                    // Snap to position
                    this.transform.position = destinationPoint;
                    isMovingGame = false;
                }
            }
        } 
        //lerp shell upwards
        else if (currentRevealState == RevealState.RevealMovingUp)
        {            
            ////////Debug.Log("REVEAL MOVING UP HERE");
            lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / timeToMoveAssets);

            curTimeToMoveAssets -= Time.deltaTime;
            if (movingAssets && curTimeToMoveAssets <= 0)
            {
                lerpAmount = 0;
                movingAssets = false;
            }

            this.transform.position = Vector3.Lerp(shellActivePosition, shellInactivePosition, Mathf.Pow(1.0f - lerpAmount, 0.5f));


            if (transform.position.z == shellInactivePosition.z)
            {
                currentRevealState = RevealState.RevealPauseAtTop;

                shellActivePosition = this.transform.position;
                shellInactivePosition = shellActivePosition + Vector3.forward * 10;
                movingAssets = true;
                curTimeToMoveAssets = timeToMoveAssets;
            }

            shellShadow.transform.localScale = Vector3.Lerp(localScaleShellShadow, localScaleShellShadowLarger, 1-lerpAmount);
        }
        //lerp shell downwards
        else if (currentRevealState == RevealState.RevealMoveBackDown)
        {
            lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / timeToMoveAssets);

            curTimeToMoveAssets -= Time.deltaTime;
            if (movingAssets && curTimeToMoveAssets <= 0)
            {
                lerpAmount = 0;
                movingAssets = false;
                ////////Debug.Log("FINISHED MOVING DOWN SHELL shellActivePosition: " + shellActivePosition);
            }

            this.transform.position = Vector3.Lerp(shellActivePosition, shellInactivePosition, Mathf.Pow(1.0f - lerpAmount, 2.5f));


            if (transform.position.z == shellInactivePosition.z)
            {
                currentRevealState = RevealState.RevealPauseAtBottom;
            }

            shellShadow.transform.localScale = Vector3.Lerp(localScaleShellShadowLarger,localScaleShellShadow , 1-lerpAmount);

            //shellShadow.transform.localScale = new Vector3(0.9541942f, 0.6517228f, 1f);
        }
        else if (currentRevealState == RevealState.PostSwap)
        {
            shellActivePosition = destinationPoint;
            shellInactivePosition = shellActivePosition + Vector3.back * 10;

            lerpAmount = Mathf.Min(1.0f, curTimeToMoveAssets / timeToMoveAssets);

            curTimeToMoveAssets -= Time.deltaTime;
            if (movingAssets && curTimeToMoveAssets <= 0)
            {
                lerpAmount = 0;
                movingAssets = false;
            }

            this.transform.position = Vector3.Lerp(shellActivePosition, shellInactivePosition, Mathf.Pow(1.0f - lerpAmount, 0.5f));

            if (transform.position.z == shellInactivePosition.z)
            {
                currentRevealState = RevealState.RevealPauseAtTop;
            }
        }
    }
}
