﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class SpotlightEffect : MonoBehaviour {

	public Transform marker = null;
	public bool effectOn = true;
	public float cullRadius = 4.0f;

	public Shader spotlightCutout;
	public Shader spotlightCutoutAndroid;

	private bool isAnimating = false;
	private float amimMaxCullRadius = 100.0f;
	private float animCullRadius;
	private float timeToAnimate = 1.0f;
	private float curTimeToAnimate;

	private MeshRenderer meshRenderer = null;
	private Material meshMat = null;

	void Start()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		meshMat = meshRenderer.sharedMaterial;

		#if UNITY_ANDROID
			meshMat.shader = spotlightCutoutAndroid;
		#else
			meshMat.shader = spotlightCutout;
		#endif


	}

	public void ToggleEffect(bool effect, float newCullRadius = 1.0f, bool animating = false, float newTimeToAnimate = 1.0f)
	{
		effectOn = effect;
		if (meshRenderer == null)
			meshRenderer = GetComponent<MeshRenderer>();
		if (meshMat == null)
			meshMat = meshRenderer.sharedMaterial;

		if (!effectOn)
			meshRenderer.enabled = false;
		else
			meshRenderer.enabled = true;

		cullRadius = newCullRadius;

		if (animating)
		{
	
			amimMaxCullRadius = newCullRadius * 100.0f;
			cullRadius = amimMaxCullRadius;
			animCullRadius = newCullRadius;

			timeToAnimate = newTimeToAnimate;
			curTimeToAnimate = timeToAnimate;

			isAnimating = true;

			meshMat.SetVector("_CullPosition", new Vector3( marker.position.x, marker.position.y, transform.position.z));
			meshMat.SetFloat("_CullRadius", cullRadius);

		}
	}	

	void Update () 
	{
		if (effectOn && meshMat != null)
		{
			if (isAnimating)
			{
				if (curTimeToAnimate >= 0)
				{
					float lerpAmount = curTimeToAnimate / timeToAnimate;
					cullRadius = Mathf.Lerp(amimMaxCullRadius, animCullRadius, 1.0f - Mathf.Pow( lerpAmount, 2.0f) ); 

					curTimeToAnimate -= Time.deltaTime;
				}
				else
				{
					cullRadius = animCullRadius;
					isAnimating = false;
				}
			}

			meshMat.SetVector("_CullPosition", new Vector4( marker.position.x, marker.position.y, transform.position.z, 1));
			meshMat.SetFloat("_CullRadius", cullRadius);
		}
		else if (meshMat == null)
		{
			meshRenderer = GetComponent<MeshRenderer>();
			meshMat = meshRenderer.sharedMaterial;
		}
	}
}
