﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

 Shader "Unlit/Transparent ZWrite Crop" 
 {
 Properties {
     _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     _Color ("Color", Color) = (1,1,1,1)
     _VerticalCrop ("Vertical Crop", Range(0.0, 1.0)) = 0.0
     _HorizontalCrop ("Horizontal Crop", Range(0.0, 1.0)) = 0.0
 }
 SubShader {
     Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}

     Blend SrcAlpha OneMinusSrcAlpha 
     ZWrite On
  
     Pass
     {
     	CGPROGRAM

     	#pragma vertex vert
     	#pragma fragment frag
     	#include "UnityCG.cginc"

     	struct inputVert
     	{
     		float4 vertex : POSITION;
     		float4 color : COLOR;
     		float2 texcoord : TEXCOORD0;
     	};

     	struct vert2frag
     	{
     		fixed4 color : COLOR;
     		float4 vertex : SV_POSITION;
     		float2 texcoord : TEXCOORD0;
     	};

     	sampler2D _MainTex;
     	uniform float4 _MainTex_ST;
     	uniform float4 _Color;
     	uniform float _VerticalCrop;
     	uniform float _HorizontalCrop;

     	vert2frag vert (inputVert i)
     	{
     		vert2frag output;
     		output.vertex = UnityObjectToClipPos(i.vertex);
     		output.color = i.color;
     		output.texcoord = TRANSFORM_TEX(i.texcoord, _MainTex);
     		return output;
     	}

     	fixed4 frag (vert2frag i) : SV_TARGET
     	{
     		fixed4 sample = tex2D(_MainTex, i.texcoord);
     		float cropCheck = ceil(i.texcoord.x - _HorizontalCrop) * ceil(i.texcoord.y - _VerticalCrop);

     		fixed4 returnVal = sample * i.color * _Color;
     		returnVal.w = lerp(returnVal.w, 0.0, 1.0 - cropCheck);

     		return returnVal;
     	}
     	       
        ENDCG
     }
   }
 }