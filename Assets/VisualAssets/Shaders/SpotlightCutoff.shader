// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Spotlight Cutout" 
{
	Properties 
	{
		_Color ("Color", Color) = (0.5,0.5,0.5,0.5)
		_CullPosition ("Cull Position", Vector) = (0.0, 0.0, 0.0)
		_CullRadius ("Cull Radius", Float) = 1.0
		_XScale ("Cull X Scale", Range(0.1, 5.0)) = 1.0
		_YScale ("Cull Y Scale", Range(0.1, 5.0)) = 1.0
		_InvFade ("Softening Factor", Range(0.01, 5.0)) = 1.0
		_Cutoff ("OutsideCutoff", Range(0.0, 1.0)) = 0.5
	}

	Category 
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha

		Cull Off Lighting Off ZWrite Off

		SubShader 
		{
			Pass 
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				uniform float4 _Color;
				uniform float4 _CullPosition;
				uniform float _CullRadius;
				uniform float _InvFade;
				uniform float _XScale;
				uniform float _YScale;
				uniform float _Cutoff;

				struct appdata_t 
				{
					float4 vertex : POSITION;
					float4 color : COLOR;
				};

				struct v2f {
					float4 vertex : SV_POSITION;
					float4 color : COLOR;
					float4 worldCoord : TEXCOORD0;
				};

				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.color = v.color;
					o.worldCoord = mul(unity_ObjectToWorld, v.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					float dist = distance(i.worldCoord.xyz, _CullPosition);
					float cutoffDistance = _CullRadius * _Cutoff;

					float lerpValue = min((max(0, dist) - cutoffDistance) / (_CullRadius - cutoffDistance), 1.0);

					//for new IOS
					//lerpValue = max( 0, pow(lerpValue, _InvFade));

					return float4(_Color.x, _Color.y, _Color.z, _Color.w * lerpValue);
				}
				ENDCG 
			}
		}	
	}
}
