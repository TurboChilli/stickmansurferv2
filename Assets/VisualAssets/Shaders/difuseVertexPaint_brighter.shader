Shader "DifusePaintBrighter" {
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		//_BlendTex ("Alpha Blended (RGBA) ", 2D) = "white" {}
	}
	SubShader
	{
		BindChannels
		{
			Bind "vertex", vertex
			Bind "Color", color
			Bind "texcoord", texcoord
		}
		Cull Off
		Tags { "RenderType"="Opaque" }
		LOD 200
		Pass
		{
			Color[color]
			SetTexture[_MainTex]
			{
				Combine texture*previous DOUBLE
			}
			SetTexture [_BrightTex] {

                Combine previous +- texture DOUBLE

            }

		}
		/*Pass
		{
			Color[color]
		}*/
	} 
	//FallBack "Diffuse"
}
