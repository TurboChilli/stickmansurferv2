﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "TurboChilli/Unlit/Transparent Backfacing"
{
    Properties
    {
        _MainTex("Texture",        2D) = "white" {}
        _Alpha  ("Alpha", Range(0, 1)) = 0.75 
        _Color  ("TintColor", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags
        {
            "Queue"                = "Transparent"
            "RenderType"           = "Transparent"
            "IgnoreProjector"      = "true"
            "ForceNoShadowCasting" = "true"
        }
        
        Name      "FORWARD"
        Cull      Front
        ZTest     LEqual
        Lighting  Off
        Fog       { Mode Off }
        LOD 200
        
        Blend SrcAlpha OneMinusSrcAlpha
        
        Pass
        {
            CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4    _MainTex_ST;
			float _Alpha;
			float4 _Color;

			struct vertexInput
			{
				float4 vertex   : POSITION; 
				float4 texcoord : TEXCOORD0;
			};


			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				half2 uv   : TEXCOORD0;
			};


			fragmentInput vert(vertexInput input)
			{
				fragmentInput output;
				output.pos = UnityObjectToClipPos( input.vertex );
				output.uv  = input.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;

				return output;
			}

			fixed4 frag(fragmentInput input) : COLOR
			{
				fixed4 colr = fixed4(tex2D(_MainTex, input.uv));
				colr.a      = _Alpha;
				colr.rgb   *= _Color;
				return colr;
			}

			ENDCG
        }
    } 
    FallBack "Unlit/Transparent Cutout"
}