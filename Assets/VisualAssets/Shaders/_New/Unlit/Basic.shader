﻿Shader "TurboChilli/Unlit/Basic"
{
    Properties
    {
        _MainTex("Texture",  2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "Queue"                = "Geometry"
            "RenderType"           = "Opaque"
            "IgnoreProjector"      = "true"
            "ForceNoShadowCasting" = "true"
        }
        Name     "FORWARD"
        Cull     Back
        ZTest    LEqual
        Lighting Off
        Fog      { Mode Off }
        LOD 200
        
        Pass
        {
        	SetTexture [_MainTex]
        	{
                Combine texture
        	}
        }
    } 
    FallBack "Unlit/Texture"
}