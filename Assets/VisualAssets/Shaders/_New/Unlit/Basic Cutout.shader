﻿Shader "TurboChilli/Unlit/Transparent Cutout"
{
    Properties
    {
        _MainTex("Texture",               2D) = "white" {}
        _Cutoff ("Alpha cutoff", Range (0,1)) = 0.5
    }
    SubShader
    {
        Tags
        {
            "Queue"                = "Transparent"
            "RenderType"           = "Transparent"
            "IgnoreProjector"      = "true"
            "ForceNoShadowCasting" = "true"
        }
        
        Name      "FORWARD"
        Cull      Back
        ZTest     LEqual
        Lighting  Off
        Fog       { Mode Off }
        LOD 200
        
        AlphaTest Greater [_Cutoff]
        Blend     SrcAlpha OneMinusSrcAlpha
        
        Pass
        {
	        SetTexture [_MainTex]
	        {
	            Combine texture
	        }
        }
    } 
    FallBack "Unlit/Transparent Cutout"
}