Shader "DifusePaintTrans" {
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		BindChannels
		{
			Bind "vertex", vertex
			Bind "Color", color
			Bind "texcoord", texcoord
		}
		Cull Off
		Tags { "Queue"="Transparent" }
		LOD 200
		Pass
		{
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
			AlphaTest Greater 0.3
			ColorMask RGB
			
			Color[color]
			SetTexture[_MainTex]
			{
				Combine texture*primary DOUBLE
			}
		}
		/*Pass
		{
			Color[color]
		}*/
	} 
	FallBack "Diffuse"
}
/*
Shader "DifusePaintTrans" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 200
Cull Off
CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff

sampler2D _MainTex;
fixed4 _Color;

struct Input {
	float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "Transparent/VertexLit"
}*/
/*
Shader "DifusePaintTrans" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}

SubShader {
    Tags {"RenderType"="Transparent" "Queue"="Transparent"}
    Cull Off
    // Render into depth buffer only
    Pass {
    	AlphaTest Greater 0
        ColorMask 0
    }
    // Render normally
    Pass {
        ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
        AlphaTest Greater 0
        ColorMask RGB
        Material {
            Diffuse [_Color]
            Ambient [_Color]
        }
        Lighting On
        SetTexture [_MainTex] {
            Combine texture * primary DOUBLE, texture * primary
        }
    }
}
}*/

/*Shader "Transparent/Unlit2" {
    Properties {
        _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
        _Alpha ("Alpha", Range (0,1)) = 1
    }

    SubShader {
        Tags { "IgnoreProjector"="True" "RenderType"="TransparentCutout" "Queue"="Transparent"}
        Lighting off

        // Render both front and back facing polygons.
        Cull Off

        // first pass:
        //   render any pixels that are more than [_Alpha] opaque
        Pass {  
        	 Blend SrcAlpha OneMinusSrcAlpha
        	 AlphaTest Greater 0.1
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata_t {
                    float4 vertex : POSITION;
                    float4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                struct v2f {
                    float4 vertex : POSITION;
                    float4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;
                float _Alpha;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                    o.color = v.color;
                    o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }

                half4 frag (v2f i) : COLOR
                {
                    half4 col = tex2D(_MainTex, i.texcoord) * i.color;
                    col.a *= _Alpha;
                    return col;
                }
            ENDCG
        }

        // Second pass:
        //   render the semitransparent details.
        Pass {
            Tags { "RequireOption" = "SoftVegetation" "Queue"="Transparent"}

            // Dont write to the depth buffer
            ZWrite off

            // Set up alpha blending
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata_t {
                    float4 vertex : POSITION;
                    float4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                struct v2f {
                    float4 vertex : POSITION;
                    float4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;
                float _Alpha;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                    o.color = v.color;
                    o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                    return o;
                }

                half4 frag (v2f i) : COLOR
                {
                    half4 col = tex2D(_MainTex, i.texcoord) * i.color;
                    col.a *= _Alpha;
                    return col;
                }
            ENDCG
        }
    }
	FallBack "Diffuse"
   }*/
