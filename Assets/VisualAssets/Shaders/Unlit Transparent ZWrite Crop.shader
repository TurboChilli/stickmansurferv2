﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

 Shader "Unlit/Transparent ZWrite" {
 Properties {
     _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     _Color ("Color", Color) = (1,1,1,1)
 }
 SubShader {
     Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}

     Blend SrcAlpha OneMinusSrcAlpha 
     ZWrite On
  
     Pass
     {
     	CGPROGRAM

     	#pragma vertex vert
     	#pragma fragment frag
     	#include "UnityCG.cginc"

     	struct inputVert
     	{
     		float4 vertex : POSITION;
     		float4 color : COLOR;
     		float2 texcoord : TEXCOORD0;
     	};

     	struct vert2frag
     	{
     		fixed4 color : COLOR;
     		float4 vertex : SV_POSITION;
     		float2 texcoord : TEXCOORD0;
     	};

     	sampler2D _MainTex;
     	uniform float4 _MainTex_ST;
     	uniform float4 _Color;

     	vert2frag vert (inputVert i)
     	{
     		vert2frag output;
     		output.vertex = UnityObjectToClipPos(i.vertex);
     		output.color = i.color;
     		output.texcoord = TRANSFORM_TEX(i.texcoord, _MainTex);
     		return output;
     	}

     	fixed4 frag (vert2frag i) : SV_TARGET
     	{
     		return tex2D(_MainTex, i.texcoord) * i.color * _Color;
     	}
     	       
        ENDCG
     }
   }
 }