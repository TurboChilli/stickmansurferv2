Shader "DifuseCullOff" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Cull Off
		Tags { "RenderType"="Opaque" }
		Pass
		{
			SetTexture[_MainTex]
			
		}
	} 
	FallBack "Diffuse"
}
