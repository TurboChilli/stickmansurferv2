﻿Shader "TurboChilli/ScanLines" {
	Properties 
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_LineColor("Line Color", color) = (1.0, 1.0, 1.0, 1.0)
		_LineBlend("Line Blend", float) = 0.5
		_LineVertFreq("Line Vertical Frequency", float) = 1
		_VerticalOffset("Line Vertical Offset", float) = 1
	}
	
	SubShader 
	{
		CGINCLUDE
		#include "UnityCG.cginc"

		uniform sampler2D _MainTex;
		uniform float4 _LineColor;
		uniform float _LineBlend;
		uniform float _LineVertFreq;
		uniform float _VerticalOffset;

		float4 frag(v2f_img i) : COLOR
		{			
			//orig color	
			float4 base = tex2D(_MainTex, i.uv);
			
			fixed scanline = cos(i.uv.y * _LineVertFreq + _VerticalOffset);
			
			return lerp(base, _LineColor * scanline, _LineBlend);
		}
		ENDCG

		Pass 
		{
		  ZTest Always Cull Off ZWrite Off
		  Fog { Mode off }      
		
		  CGPROGRAM
		  #pragma fragmentoption ARB_precision_hint_fastest 
		  #pragma vertex vert_img
		  #pragma fragment frag
		  ENDCG
		}
	} 
}
