﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;


[ExecuteInEditMode]
public class ScanLines : ImageEffectBase 
{
	public Color LineColor = Color.white;
	
	[Range(0.0f, 1.0f)]
	public float LineBlend = 0.2f;
	
	public float VerticalSpeed = 0.1f;
	private float timer;
	
	public float VertLineFreq = 1000.0f;

	void OnRenderImage (RenderTexture source, RenderTexture destination) 
	{
		if (LineBlend == 0)
		{
			Graphics.Blit(source, destination);
			return;
		}
		
		timer += Time.deltaTime * VerticalSpeed;
		
		material.SetColor("_LineColor", LineColor);
		material.SetFloat("_LineBlend", LineBlend);
		material.SetFloat("_LineVertFreq", VertLineFreq);
		material.SetFloat("_VerticalOffset", timer);
		Graphics.Blit(source, destination, material);			
	}
}
