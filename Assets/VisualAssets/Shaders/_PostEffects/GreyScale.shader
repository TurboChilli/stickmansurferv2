﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "TurboChilli/GreyScale" {

 Properties {
     _MainTex ("Texture", 2D) = "white" { }
 }
 SubShader {
     Pass {
 
	 CGPROGRAM
	 #pragma vertex vert
	 #pragma fragment frag
	 
	 #include "UnityCG.cginc"
	 
	 uniform sampler2D _MainTex;
	 uniform float4 _MainTex_ST;

  	struct inputVert
 	{
 		float4 vertex : POSITION;
 		float2 texcoord : TEXCOORD0;
 	};

	 struct v2f {
	     float4  pos : SV_POSITION;
	     float2  uv : TEXCOORD0;
	 };

	 v2f vert (inputVert v)
	 {
	     v2f o;
	     o.pos = UnityObjectToClipPos (v.vertex);
	     o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
	     return o;
	 }
	 
	 fixed4 frag (v2f i) : COLOR
	 {
	     fixed4 sample = tex2D (_MainTex, i.uv);

	     float luminence = dot(sample.rgb, float3(0.3, 0.59, 0.11));
	     sample = fixed4(luminence, luminence, luminence, sample.a);

	     return sample;
	 }
	 ENDCG
 
     }
 }
 Fallback "VertexLit"
 } 
