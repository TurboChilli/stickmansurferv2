﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader  "Custom/FadeShader"
{
Properties
    {
    _MainTex("First Texture",         2D) = "white" {}
    _AltTex ("Second Texture",        2D) = "white" {}
    _AltTex2 ("Third Texture",        2D) = "white" {}
    _Ammount("Lerp Weight",  Range(0, 1)) = 0
    }
    SubShader
    {
   Tags
   {
            "Queue"           = "Transparent"
            "RenderType"      = "Transparent"
   "IgnoreProjector" = "true"
            "LightMode"       = "Always"
   }

Name      "FORWARD"
        Cull      Back
        ZTest     LEqual
        Lighting  Off
Blend     SrcAlpha OneMinusSrcAlpha
        Fog       { Mode Off }
        LOD 200
    
        Pass
        {
            CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"
            
            uniform sampler2D _MainTex;
            uniform sampler2D _AltTex;
            uniform sampler2D _AltTex2;
uniform float4    _MainTex_ST;
            fixed  _Ammount;


struct vertexInput
{
float4 vertex   : POSITION; 
float4 texcoord : TEXCOORD0;
} ;


struct fragmentInput
{
float4 pos : SV_POSITION;
half2 uv   : TEXCOORD0;
} ;


fragmentInput vert(vertexInput input)
{
fragmentInput output;
output.pos = UnityObjectToClipPos( input.vertex );
output.uv =  input.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;

return output;
}


            fixed4 frag(v2f_img input) : COLOR
            {
            // Decide which texture to use based on Ammount
                fixed4 colr;
                if(_Ammount == 0) // For Ammount of 0, use the first texture
                {
                 colr = tex2D(_MainTex, input.uv);
                }
                else if(_Ammount == 1) // For Ammount of 1, use the second texture
               {
                colr = tex2D(_AltTex, input.uv);
                }
               else if(_Ammount == 2) // For Ammount of 1, use the second texture
               {
                colr = tex2D(_AltTex2, input.uv);
               }
               else // For Ammount between 0 and 1, blend between textures;
               {
                colr = lerp(tex2D(_MainTex, input.uv), tex2D(_AltTex, input.uv), _Ammount);
               }
                
                // Return chosen texture
                return colr;
            }
            ENDCG
        }
    }
    Fallback "Unlit/Transparent Cutout"
}